import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import ReactTooltip from "react-tooltip";

import GridTable from "./../Common/GridTable";
import PrevHistPopup from "./popups/PrevHistPopup";
import ChgReasonPopup from "./popups/ChgReasonPopup";

import * as fieldActions from "../../store/modules/field";
import { FormattedMessage } from "react-intl";

import "./style.scss";

class FieldHistList extends Component {
  state = {
    prevHistPopup: false,
    rollbackPopup: false,
    targetHistSeq: ""
  };

  //이전이력 필드조회
  onClickPrevHist = async fieldHist => {
    try {
      const { FieldActions } = this.props;
      const res = await FieldActions.getPrevFields(fieldHist.fieldHistSeq);
      this.setState(
        {
          prevData: {
            fieldHist,
            fields: res.data
          }
        },
        () => this.openPopup("prevHistPopup")
      );
    } catch (err) {
      console.log(err);
    }
  };

  /** 롤백 버튼 */
  onClickRollback = ({ fieldHistSeq }) => {
    this.setState(
      {
        targetHistSeq: fieldHistSeq
      },
      () => this.openPopup("rollbackPopup")
    );
  };
  /** 이력 롤백 */
  rollbackHistory = async fieldChgReason => {
    try {
      const { FieldActions } = this.props;
      const targetHistSeq = this.state.targetHistSeq;
      await FieldActions.rollback({ targetHistSeq, fieldChgReason });

      FieldActions.getFieldHists();
      FieldActions.getFields();

      this.closePopup("rollbackPopup");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 팝업제어 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** 이력 조회 */
  getFieldHists = () => {
    const { FieldActions } = this.props;
    FieldActions.getFieldHists();
  };

  componentDidMount() {
    this.getFieldHists();
  }

  render() {
    const {
      onClickPrevHist,
      onClickRollback,
      closePopup,
      rollbackHistory
    } = this;
    const { fieldHists } = this.props;
    const { prevHistPopup, rollbackPopup, prevData } = this.state;

    const columns = [
      {
        Header: "No.",
        accessor: "fieldHistSeq",
        width: 50,
        filterable: false,
        style: { textAlign: "center" }
      },
      {
        Header: (
          <FormattedMessage id="change.datetime">
            {text => text}
          </FormattedMessage>
        ),
        accessor: "regDateFormatted",
        sortable: true,
        filterable: true,
        width: 120,
        style: { textAlign: "center" }
      },
      {
        Header: (
          <FormattedMessage id="change.modifier">
            {text => text}
          </FormattedMessage>
        ),
        accessor: "regUsrNm",
        width: 80,
        filterable: true,
        style: { textAlign: "center" }
      },
      {
        Header: (
          <FormattedMessage id="reason.change">{text => text}</FormattedMessage>
        ),
        accessor: "fieldChgReason",
        filterable: true,
        Cell: props => {
          const tooltipId = `fieldChgTooltip_${props.original.fieldHistSeq}`;
          return (
            <React.Fragment>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
              <ReactTooltip
                className="tooltipClass"
                id={tooltipId}
                place="left"
                type="dark"
                effect="float"
              >
                {props.value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      },
      {
        Header: " Actions",
        accessor: "action1",
        width: 70,
        style: { textAlign: "center" },
        Cell: row => {
          return row.original.isLatest === "N" ? (
            <React.Fragment>
              <button
                type="button"
                className="btn"
                style={{
                  height: "28px",
                  width: "28px",
                  padding: "0",
                  marginRight: "1px"
                }}
                onClick={() => {
                  onClickPrevHist(row.original);
                }}
              >
                <i className="fas fa-search"></i>
              </button>
              <button
                type="button"
                className="btn"
                style={{ height: "28px", width: "28px", padding: "0" }}
                onClick={() => {
                  onClickRollback(row.original);
                }}
              >
                <i className="fas fa-redo"></i>
              </button>
            </React.Fragment>
          ) : (
            ""
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <GridTable
          columns={columns}
          data={fieldHists}
          filterable={false}
          sortable
          defaultSorted={[{ id: "regDateFormatted", desc: true }]}
          resizable={false}
          className="-highlight"
          onSortedChange={data => console.log(data)}
          getTrProps={(state, rowInfo, column) => {
            if (rowInfo && rowInfo.original.isLatest === "Y") {
              return {
                style: {
                  backgroundColor: "#282a35"
                }
              };
            } else {
              return {};
            }
          }}
        />
        {prevHistPopup && (
          <PrevHistPopup
            data={prevData}
            onClose={() => closePopup("prevHistPopup")}
          />
        )}
        {rollbackPopup && (
          <ChgReasonPopup
            onClose={() => closePopup("rollbackPopup")}
            onConfirm={rollbackHistory}
            message={
              <FormattedMessage id="roll.back.hist">
                {text => text}
              </FormattedMessage>
            }
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    fieldHists: state.field.get("fieldHists")
  }),
  dispatch => ({
    FieldActions: bindActionCreators(fieldActions, dispatch)
  })
)(FieldHistList);
