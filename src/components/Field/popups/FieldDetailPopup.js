import React, { Component } from "react";
import { connect } from "react-redux";
import { Map } from "immutable";
import { Textbox, Textarea, Select } from "react-inputs-validation";
import Draggable from "react-draggable";
import PropTypes from "prop-types";

import ConfirmPopup from "./ConfirmPopup";
import IntlMessages from "util/IntlMessages";

import * as api from "./../../../store/api/field";

const initData = Map({
  fieldChgReason: "",
  field: Map({
    keyFieldYn: "N",
    fieldGroupCode: "",
    fieldType: "",
    field: "",
    fieldId: "",
    fieldDesc: ""
  })
});

class FieldDetailPopup extends Component {
  state = {
    fieldHist: this.props.data,
    validation: {
      validate: false,
      hasErrFieldGroupcd: this.props.isUpdate ? false : true,
      hasErrFieldType: this.props.isUpdate ? false : true,
      hasErrField: this.props.isUpdate ? false : true,
      hasErrFieldId: this.props.isUpdate ? false : true,
      hasErrFieldChgReason: true
    },
    confirmPopup: false,
    position: {
      x: 0,
      y: 0
    }
  };

  /** 확인버튼 */
  onClickConfirm = () => {
    if (this.isValidate()) {
      this.openPopup("confirmPopup");
    }
  };

  /** input onChange */
  onChange = (value, e) => {
    const { name } = e.target;
    this.setState({
      fieldHist: this.state.fieldHist.setIn(["field", name], value)
    });
  };

  onChangeCheck = e => {
    const { name, checked } = e.target;
    this.setState({
      fieldHist: this.state.fieldHist.setIn(
        ["field", name],
        checked ? "Y" : "N"
      )
    });
  };
  onChangeFieldHist = (value, e) => {
    const { name } = e.target;
    this.setState({
      fieldHist: this.state.fieldHist.set(name, value)
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    //validate를 true로 설정 시 모든 form의 유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);

    const {
      hasErrField,
      hasErrFieldId,
      hasErrFieldGroupcd,
      hasErrFieldType,
      hasErrFieldChgReason
    } = this.state.validation;

    return (
      !hasErrField &&
      !hasErrFieldId &&
      !hasErrFieldGroupcd &&
      !hasErrFieldType &&
      !hasErrFieldChgReason
    );
  };

  /** 팝업제어 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** set for draggable */
  setPopupPosition = () => {
    const elWidth = document.getElementById("fieldDetailPopup").offsetWidth;
    const elHeight = document.getElementById("fieldDetailPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };
  /** drag */
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  componentDidMount = () => {
    this.setPopupPosition();
  };

  /**
   * props setting
   */
  static defaultProps = {
    type: [],
    groupcd: [],
    onConfirm: () =>
      console.log("FieldDetailPopup :: onConfirm is not defined"),
    onClose: () => console.log("FieldDetailPopup :: onClose is not defined"),
    data: initData,
    title: <IntlMessages id="field.detail" />,
    isUpdate: false
  };
  static propTypes = {
    type: PropTypes.array,
    groupcd: PropTypes.array,
    onConfirm: PropTypes.func,
    onClose: PropTypes.func,
    data: PropTypes.instanceOf(Map),
    title: PropTypes.object,
    isUpdate: PropTypes.bool
  };

  render() {
    const {
      onChangeFieldHist,
      onChange,
      onChangeCheck,
      handleDrag,
      onClickConfirm,
      closePopup
    } = this;
    const { type, groupcd, onConfirm, title, onClose, isUpdate } = this.props;
    const { position, fieldHist, confirmPopup } = this.state;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={handleDrag}
        >
          <div className="popup" id="fieldDetailPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>{title}</h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="key.field.use" />
                    </th>
                    <td>
                      <div className="checkbox">
                        <label>
                          <input
                            type="checkbox"
                            name="keyFieldYn"
                            tabIndex="1"
                            onChange={onChangeCheck}
                            checked={
                              fieldHist.getIn(["field", "keyFieldYn"]) === "Y"
                                ? true
                                : false
                            }
                          />
                          <div className="icon" />
                        </label>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="field.group" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Select
                        id="fieldGroupCode"
                        name="fieldGroupCode"
                        tabIndex="2"
                        value={fieldHist.getIn(["field", "fieldGroupCode"])}
                        onChange={(value, e) => {
                          const { fieldHist } = this.state;
                          this.setState({
                            fieldHist: fieldHist.setIn(
                              ["field", "fieldGroupCode"],
                              value
                            )
                          });
                        }}
                        onBlur={() => {}}
                        optionList={groupcd}
                        validate={validate}
                        validationOption={{
                          locale: this.props.language
                        }}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFieldGroupcd: res,
                              validate: false
                            }
                          });
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="data.type" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Select
                        id="fieldType"
                        name="fieldType"
                        tabIndex="3"
                        value={fieldHist.getIn(["field", "fieldType"])}
                        onChange={(value, e) => {
                          const { fieldHist } = this.state;
                          this.setState({
                            fieldHist: fieldHist.setIn(
                              ["field", "fieldType"],
                              value
                            )
                          });
                        }}
                        onBlur={() => {}}
                        optionList={type}
                        validate={validate}
                        validationOption={{
                          locale: this.props.language
                        }}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFieldType: res,
                              validate: false
                            }
                          });
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="field.id" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="fieldId"
                        name="fieldId"
                        type="text"
                        tabIndex="4"
                        value={fieldHist.getIn(["field", "fieldId"])}
                        onChange={onChange}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFieldId: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          reg: /^[0-9]+$/,
                          regMsg: <IntlMessages id="number.only" />,
                          locale: this.props.language,
                          customFunc: value => {
                            try {
                              const res = api.isDuplicateFieldId(
                                value,
                                isUpdate
                                  ? fieldHist.getIn(["field", "fieldSeq"])
                                  : ""
                              );
                              if (res.status === 200) {
                                if (res.responseText === "true") {
                                  return <IntlMessages id="no.dup.value" />;
                                }
                              } else {
                                return <IntlMessages id="occur.error" />;
                              }
                            } catch (e) {
                              return <IntlMessages id="occur.error" />;
                            }
                            return true;
                          }
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="field" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      {isUpdate ? (
                        <div style={{ padding: "0 10px" }}>
                          {fieldHist.getIn(["field", "field"])}
                        </div>
                      ) : (
                        <Textbox
                          id="field"
                          name="field"
                          type="text"
                          tabIndex="4"
                          value={fieldHist.getIn(["field", "field"])}
                          onChange={onChange}
                          onBlur={() => {}}
                          validate={validate}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrField: res,
                                validate: false
                              }
                            });
                          }}
                          validationOption={{
                            reg: /^[a-zA-Z0-9{}[\]/?.,;:|)*~`!^\-_+<>@#$%&\\=('"]+$/,
                            regMsg: <IntlMessages id="no.korean" />,
                            locale: this.props.language,
                            customFunc: value => {
                              try {
                                const res = api.isDuplicate(value);
                                if (res.status === 200) {
                                  if (res.responseText === "true") {
                                    return <IntlMessages id="no.dup.value" />;
                                  }
                                }
                              } catch (e) {
                                return <IntlMessages id="occur.error" />;
                              }
                              return true;
                            }
                          }}
                        />
                      )}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="field.desc" />
                    </th>
                    <td>
                      <Textarea
                        id="fieldDesc"
                        name="fieldDesc"
                        type="text"
                        tabIndex="6"
                        maxLength="4000"
                        value={fieldHist.getIn(["field", "fieldDesc"])}
                        onChange={onChange}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="change-reason">
                <label>
                  <IntlMessages id="reason.change.reg" />
                  <span className="font--red">*</span>
                </label>
                <Textarea
                  id="fieldChgReason"
                  name="fieldChgReason"
                  type="text"
                  tabIndex="7"
                  maxLength="4000"
                  value={fieldHist.get("fieldChgReason")}
                  onChange={onChangeFieldHist}
                  onBlur={() => {}}
                  validate={validate}
                  validationOption={{locale: this.props.language}}
                  validationCallback={res => {
                    this.setState({
                      validation: {
                        ...this.state.validation,
                        hasErrFieldChgReason: res,
                        validate: false
                      }
                    });
                  }}
                  customStyleInput={{ width: "100%", resize: "none" }}
                />
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={onClickConfirm}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
        {confirmPopup && (
          <ConfirmPopup
            isPip={true}
            message={<IntlMessages id="do.save" />}
            onConfirm={() => {
              onConfirm(fieldHist.toJS());
              closePopup("confirmPopup");
            }}
            onClose={() => closePopup("confirmPopup")}
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({})
)(FieldDetailPopup);
