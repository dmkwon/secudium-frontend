import React, { Component } from "react";
import { Textarea } from "react-inputs-validation";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";

export default class ChgReasonPopup extends Component {
  state = {
    chgReason: "",
    validation: {
      validate: false,
      hasErrChgReason: true
    }
  };

  onClickConfirm = () => {
    if (this.isValidate()) {
      this.props.onConfirm(this.state.chgReason);
    }
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);
    const { hasErrChgReason } = this.state.validation;
    return !hasErrChgReason;
  };

  onChangeTextarea = (value, e) => {
    const { name } = e.target;
    this.setState({ [name]: value });
  };

  /**
   * props setting
   */
  static defaultProps = {
    onClose: () => console.log("ChgReasonPopup :: onClose is not defined"),
    onConfirm: () => console.log("ChgReasonPopup :: onConfirm is not defined"),
    message: "ConfirmPopup :: message is not defined"
  };

  static propTypes = {
    onClose: PropTypes.func,
    onConfirm: PropTypes.func,
    message: PropTypes.string
  };

  render() {
    const { onChangeTextarea, onClickConfirm } = this;
    const { onClose, message } = this.props;
    const { chgReason } = this.state;
    const { validate } = this.state.validation;

    return (
      <div>
        <div className="popup popup--alert" style={{ width: "400px" }}>
          <div className="popup__header">
            <h5>
              <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
            </h5>
            <button className="btn btn-close" onClick={onClose} />
          </div>
          <div className="popup__body">
            {message}
            <div className="change-reason">
              <label>
                <FormattedMessage id="reason.change">
                  {text => text}
                </FormattedMessage>
                <span className="font--red">*</span>
              </label>
              <FormattedMessage id="enter.change.reason">
                {placeholder => (
                  <Textarea
                    name="chgReason"
                    placeholder={placeholder}
                    type="text"
                    tabIndex="1"
                    maxLength="4000"
                    value={chgReason}
                    onChange={onChangeTextarea}
                    onBlur={() => {}}
                    validate={validate}
                    validationOption={{locale: this.props.language}}
                    validationCallback={res => {
                      this.setState({
                        validation: {
                          ...this.state.validation,
                          hasErrChgReason: res,
                          validate: false
                        }
                      });
                    }}
                    customStyleInput={{
                      width: "100%",
                      resize: "none",
                      height: "80px",
                      marginTop: "15px"
                    }}
                  />
                )}
              </FormattedMessage>
            </div>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={onClose}>
              <FormattedMessage id="cancel"> {text => text} </FormattedMessage>
            </button>
            <button className="btn btn--dark" onClick={onClickConfirm}>
              <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
            </button>
          </div>
        </div>
        <div className="dim" />
      </div>
    );
  }
}
