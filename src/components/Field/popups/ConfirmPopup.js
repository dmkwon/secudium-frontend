import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";

/**
 * 확인팝업(확인, 취소)
 * @param {message} 본문 메세지
 * @param {callback} 확인버튼 클릭 시 콜백
 * @param {onClose} 팝업닫기 콜백
 * @param {isPip} 팝업 위에 표시되는 팝업인지 유무
 */
const ConfirmPopup = ({ message, onConfirm, onClose, isPip }) => {
  return (
    <div>
      <div
        className="popup popup--alert"
        style={{ width: "400px", zIndex: "13" }}
      >
        <div className="popup__header">
          <h5>
            <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
          </h5>
          <button className="btn btn-close" onClick={onClose} />
        </div>
        <div className="popup__body">{message}</div>
        <div className="popup__footer">
          <button className="btn btn--white" onClick={onClose}>
            <FormattedMessage id="cancel"> {text => text} </FormattedMessage>
          </button>
          <button className="btn btn--dark" onClick={onConfirm}>
            <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
          </button>
        </div>
      </div>
      <div className={isPip ? "innerDim" : "dim"} />
    </div>
  );
};

ConfirmPopup.defaultProps = {
  message: "ConfirmPopup :: message is not defined",
  onConfirm: () => console.log("ConfirmPopup :: onConfirm is not defined"),
  conClose: () => console.log("confirmPopup :: is not defined"),
  isPip: false
};

ConfirmPopup.propTypes = {
  message: PropTypes.string,
  onConfirm: PropTypes.func,
  onClose: PropTypes.func
};

export default ConfirmPopup;
