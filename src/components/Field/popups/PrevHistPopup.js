import React, { Component } from "react";
import GridTable from "./../../Common/GridTable";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

export default class PrevHistPopup extends Component {
  state = {
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount() {
    this.setPopupPosition();
  }

  setPopupPosition = () => {
    const elWidth = document.getElementById("prevHistPopup").offsetWidth;
    const elHeight = document.getElementById("prevHistPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  render() {
    const { data, onClose } = this.props;
    const { fields, fieldHist } = data;
    const { x, y } = this.state.position;
    const columns = [
      {
        Header: (
          <IntlMessages id="key.field"/>
        ),
        accessor: "keyFieldYn",
        style: { textAlign: "center" },
        width: 60,
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  disabled
                  checked={row.original.keyFieldYn === "Y" ? true : false}
                />
                <div className="icon" style={{ cursor: "default" }} />
              </label>
            </div>
          );
        }
      },
      {
        Header: (
          <IntlMessages id="field.group"/>
        ),
        accessor: "fieldGroupCodeString",
        filterable: true,
        style: { textAlign: "center" },
        width: 80
      },
      {
        Header: "id",
        accessor: "fieldId",
        filterable: true,
        style: { textAlign: "center" },
        width: 60
      },
      {
        Header: <IntlMessages id="type"/>,
        accessor: "fieldTypeString",
        style: { textAlign: "center" },
        filterable: true,
        width: 70
      },
      {
        Header: (
          <IntlMessages id="field"/>
        ),
        accessor: "field",
        style: { textAlign: "center" },
        filterable: true
      },
      {
        Header: (
          <IntlMessages id="field.desc"/>
        ),
        accessor: "fieldDesc",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: (
          <IntlMessages id="modifier"/>
        ),
        accessor: "modUsrNm",
        style: { textAlign: "center" },
        filterable: true,
        width: 80
      },
      {
        Header: (
          <IntlMessages id="mod.datetime"/>
        ),
        accessor: "modDateFormatted",
        style: { textAlign: "center" },
        filterable: true,
        width: 120
      }
    ];

    return (
      <React.Fragment>
        <div
          className="popup"
          style={{ width: "1000px", transform: `translate(${x}px, ${y}px)` }}
          id="prevHistPopup"
        >
          <div className="popup__header">
            <h5>
              <FormattedMessage id="change.hist.info">
                {text => text}
              </FormattedMessage>
            </h5>
            <button className="btn btn-close" onClick={onClose} />
          </div>
          <div
            className=""
            style={{
              backgroundColor: "#2f323c",
              padding: "10px",
              overflowY: "auto"
            }}
          >
            <label>
              <FormattedMessage id="reason.change">
                {text => text}
              </FormattedMessage>
            </label>
            <div
              style={{ fontSize: "12px" }}
            >{`${fieldHist.regDateFormatted} ${fieldHist.regUsrNm}`}</div>
            <textarea
              className="form-control"
              style={{
                width: "100%",
                height: "80px",
                resize: "none",
                marginBottom: "10px"
              }}
              value={fieldHist.fieldChgReason}
              readOnly
            />
            <GridTable
              columns={columns}
              data={fields}
              sortable
              defaultSorted={[{ id: "fieldId", desc: false }]}
              filterable={false}
              resizable={false}
              style={{
                height: "480px" // This will force the table body to overflow and scroll, since there is not enough room
              }}
              className="-highlight"
            />
          </div>
          <div className="popup__footer">
            <button className="btn btn--dark" onClick={onClose}>
              <FormattedMessage id="close"> {text => text} </FormattedMessage>
            </button>
          </div>
        </div>
        <div className="dim" />
      </React.Fragment>
    );
  }
}
