import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Map } from "immutable";
import ReactTooltip from "react-tooltip";

import GridTable from "./../Common/GridTable";
import FieldDetailPopup from "./popups/FieldDetailPopup";
import ChgReasonPopup from "./popups/ChgReasonPopup";
import * as fieldActions from "../../store/modules/field";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class FieldList extends Component {
  state = {
    updatePopup: false,
    deletePopup: false
  };

  /** 수정,삭제 버튼 */
  onClickUpdate = field => {
    const fieldHist = Map({
      field,
      fieldChgReason: ""
    });
    this.setState({ fieldHist }, () => this.openPopup("updatePopup"));
  };
  onClickDelete = field => {
    const fieldHist = Map({
      field,
      fieldChgReason: ""
    });
    this.setState({ fieldHist }, () => this.openPopup("deletePopup"));
  };

  /** 팝업제어관련 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** 필드 수정 */
  updateField = async fieldHist => {
    try {
      const { FieldActions } = this.props;
      await FieldActions.updateFieldHist(fieldHist);

      await FieldActions.getFieldHists();
      await FieldActions.getFields();

      this.closePopup("updatePopup");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 필드 삭제 */
  deleteField = async fieldChgReason => {
    try {
      const { FieldActions } = this.props;
      const fieldHist = this.state.fieldHist
        .set("fieldChgReason", fieldChgReason)
        .toJS();
      await FieldActions.deleteFieldHist(fieldHist);

      await FieldActions.getFieldHists();
      await FieldActions.getFields();

      this.closePopup("deletePopup");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 필드조회 */
  getFields = () => {
    const { FieldActions } = this.props;
    try {
      FieldActions.getFields();
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    this.getFields();
  }

  /** props setting */
  static defaultProps = {
    type: [],
    groupcd: []
  };
  static propTypes = {
    type: PropTypes.array,
    groupcd: PropTypes.array
  };

  render() {
    const {
      onClickUpdate,
      onClickDelete,
      closePopup,
      updateField,
      deleteField
    } = this;
    const { fieldHist, updatePopup, deletePopup } = this.state;
    const { fields, type, groupcd } = this.props;

    const columns = [
      {
        Header: <IntlMessages id="key.field" />,
        accessor: "keyFieldYn",
        style: { textAlign: "center" },
        width: 60,
        Cell: data => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  disabled
                  checked={data.original.keyFieldYn === "Y" ? true : false}
                />
                <div className="icon" style={{ cursor: "default" }} />
              </label>
            </div>
          );
        }
      },
      {
        Header: <IntlMessages id="field.group" />,
        accessor: "fieldGroupCodeString",
        filterable: true,
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: "ID",
        id: "fieldId",
        accessor: d => Number(d.fieldId),
        filterable: true,
        style: { textAlign: "center" },
        width: 60
      },
      {
        Header: <IntlMessages id="type" />,
        accessor: "fieldTypeString",
        style: { textAlign: "center" },
        filterable: true,
        width: 80
      },
      {
        Header: <IntlMessages id="field" />,
        accessor: "field",
        style: { textAlign: "center" },
        filterable: true
      },
      {
        Header: <IntlMessages id="field.desc" />,
        accessor: "fieldDesc",
        style: { textAlign: "left" },
        filterable: true,
        Cell: props => {
          const tooltipId = `fieldDescTooltip_${props.original.fieldSeq}`;
          return (
            <React.Fragment>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
              <ReactTooltip
                className="tooltipClass"
                id={tooltipId}
                place="bottom"
                type="dark"
                effect="float"
              >
                {props.value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "modUsrNm",
        style: { textAlign: "center" },
        filterable: true,
        width: 80
      },
      {
        Header: <IntlMessages id="mod.datetime" />,
        accessor: "modDateFormatted",
        style: { textAlign: "center" },
        filterable: true,
        width: 120
      },
      {
        Header: " Actions",
        accessor: "action1",
        width: 70,
        style: { textAlign: "center" },
        Cell: data => {
          return (
            <React.Fragment>
              <button
                type="button"
                className="btn btn-icon"
                onClick={() => onClickDelete(data.original)}
              >
                <img
                  id="ipDelete"
                  src="/images/common/icon_delete.png"
                  alt="icon_delete"
                />
              </button>
              <button
                type="button"
                className="btn btn-icon"
                onClick={() => onClickUpdate(data.original)}
              >
                <img
                  id="ipUpdate"
                  src="/images/common/icon_edit.png"
                  alt="icon_edit"
                />
              </button>
            </React.Fragment>
          );
        }
      }
    ];
    return (
      <React.Fragment>
        <GridTable
          columns={columns}
          data={fields}
          sortable
          defaultSorted={[{ id: "fieldId", desc: false }]}
          filterable={false}
          resizable={false}
          className="-highlight"
        />
        {updatePopup && (
          <FieldDetailPopup
            isUpdate
            type={type}
            groupcd={groupcd}
            data={fieldHist}
            onClose={() => closePopup("updatePopup")}
            title={<IntlMessages id="field.edit" />}
            onConfirm={updateField}
          />
        )}
        {deletePopup && (
          <ChgReasonPopup
            onClose={() => closePopup("deletePopup")}
            onConfirm={deleteField}
            message={<IntlMessages id="delete.field" />}
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    fields: state.field.get("fields")
  }),
  dispatch => ({
    FieldActions: bindActionCreators(fieldActions, dispatch)
  })
)(FieldList);
