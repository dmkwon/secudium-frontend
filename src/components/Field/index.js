import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getCodeList } from "./../../store/api/common";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";

import FieldHistList from "./FieldHistList";
import FieldList from "./FieldList";
import FieldDetailPopup from "./popups/FieldDetailPopup";

import Loading from "./../Common/Loading";

import * as fieldActions from "../../store/modules/field";
import IntlMessages from "util/IntlMessages";

class FieldMgmtComponent extends Component {
  state = {
    insertPopup: false,
    type: [],
    groupcd: []
  };

  /** 팝업제어관련 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** 필드추가(이력추가) */
  insertField = async fieldHist => {
    try {
      const { FieldActions } = this.props;
      await FieldActions.insertFieldHist(fieldHist);
      FieldActions.getFieldHists();
      FieldActions.getFields();

      this.closePopup("insertPopup");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 그룹 및 타입 코드 조회 */
  getCodes = async () => {
    try {
      const groupcd = await getCodeList("FIELD_GROUP");
      const type = await getCodeList("FIELD_TYPE");
      this.setState({
        groupcd: groupcd.data,
        type: type.data
      });
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    this.getCodes();
  }

  render() {
    const { openPopup, closePopup, insertField } = this;
    const { loading } = this.props;
    const { insertPopup, type, groupcd } = this.state;

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper field-mgmt">
          <LeftMenu />
          <div
            className="component-right"
            style={{ flex: 1, marginRight: "20px" }}
          >
            <div
              className="component rule-list"
              style={{ marginBottom: 0, height: "100%" }}
            >
              <div
                className="component__title"
                style={{ paddingBottom: "10px", marginBottom: 0 }}
              >
                <span>
                  <IntlMessages id="field.list" />
                </span>
                <div className="btns">
                  <button
                    className="btn btn--blue"
                    onClick={() => openPopup("insertPopup")}
                  >
                    <IntlMessages id="new.reg" />
                  </button>
                  {insertPopup && (
                    <FieldDetailPopup
                      type={type}
                      groupcd={groupcd}
                      onClose={() => closePopup("insertPopup")}
                      title={<IntlMessages id="field.reg" />}
                      onConfirm={insertField}
                    />
                  )}
                </div>
              </div>
              <FieldList groupcd={this.state.groupcd} type={this.state.type} />
            </div>
          </div>
          <div
            className="component component-tree vender-tree"
            style={{
              width: "500px",
              marginRight: 0,
              maxHeight: "970px",
              height: "calc(100vh - 110px)"
            }}
          >
            <div className="component__title" style={{ borderBottom: 0 }}>
              <IntlMessages id="change.hist" />
            </div>
            <div className="component__box">
              <FieldHistList />
            </div>
          </div>
        </div>
        {loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    loading: state.field.get("loading")
  }),
  dispatch => ({
    FieldActions: bindActionCreators(fieldActions, dispatch)
  })
)(FieldMgmtComponent);
