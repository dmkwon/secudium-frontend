import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import jwtService from "services/jwtService";
import history from "./../../history";

import { defaultUserInfo } from "./../../constants";
import IntlMessages from "util/IntlMessages";

import * as commonActions from "../../store/modules/common";

import { store } from "../../AppContainer";
import { openCommonAlert } from "../../store/modules/common";

import "./style.scss";

class LoginComponent extends Component {
  state = {
    ...defaultUserInfo,
    alertPopup: false,
    alertMsg: ""
  };

  onChange = e => {
    const { name, value } = e.target;
    this.setState({
      ...this.state,
      [name]: value
    });
  };

  onKeyDown = e => {
    if (e.keyCode === 13) {
      this.signIn();
    }
  };

  signIn = async () => {
    const { id, passwd } = this.state;
    const { CommonActions, language } = this.props;

    jwtService
      .signInWithUserNameAndPassword(id, passwd)
      .then(res => {
        //로그인 성공처리
        jwtService
          .getCurrentUser()
          .then(res => {
            CommonActions.setLanguage(language);
            history.push("/");
          })
          .catch(err => {
            //사용자 정보 가져오기 실패
            if (err.response !== undefined) {
              store.dispatch(openCommonAlert(err.response.data));
            } else {
              store.dispatch(openCommonAlert());
            }
          });
      })
      .catch(err => {
        //로그인 실패처리
        if (err.response !== undefined) {
          store.dispatch(openCommonAlert(err.response.data));
        } else {
          store.dispatch(openCommonAlert());
        }
      });
  };

  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  render() {
    const { onChange, signIn, onKeyDown } = this;
    const { id, passwd } = this.state;
    return (
      <div className="login">
        <div className="login__logo" />
        <div className="login__form">
          <input
            type="text"
            className="form-control"
            placeholder="User ID"
            name="id"
            value={id}
            onChange={onChange}
            onKeyDown={onKeyDown}
          />
          <input
            type="password"
            className="form-control"
            placeholder="Password"
            name="passwd"
            value={passwd}
            onChange={onChange}
            onKeyDown={onKeyDown}
          />
          <button className="btn btn-login" onClick={signIn}>
            <IntlMessages id="login" />
          </button>
        </div>
      </div>
    );
    //}
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    CommonActions: bindActionCreators(commonActions, dispatch)
  })
)(LoginComponent);
