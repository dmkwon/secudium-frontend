import { ENGINE_OP_HEADER, isDebug } from "./const/enginOptionConfig";

// const testUserHead_01 = `##Name=정규식설정정보
// ##Delimit=5C74405C74
// ##Description=보안이벤트를 표준화하기 위한 설정정보
// ##Owner=홍길동
// ##Field,Count=4
// ##Field,1,Name=장비종류
// ##Field,1,Type=String
// ##Field,2,Name=Prefix
// ##Field,2,Type= String
// ##Field,3,Name=RegEx
// ##Field,3,Type=String
// ##Field,4,Name=크기`

// const testUserHead_02 = `##Name=정규식설정정보##Delimit=5C74405C74##Description=보안이벤트를 표준화하기 위한 설정정보##Owner=홍길동##Field,Count=4##Field,1,Name=장비종류##Field,1,Type=String##Field,2,Name=Prefix##Field,2,Type= String##Field,3,Name=RegEx##Field,3,Type=String##Field,4,Name=크기`

// https://stackoverflow.com/questions/3745666/how-to-convert-from-hex-to-ascii-in-javascript
const hex2a = hexx => {
  var hex = hexx.toString();
  hex = hex.trim().replace(/0x/gi, ""); // 요구 사항 0x200x20처럼 한글자씩 ASCII 표현되고, 0x 모두 제거 하고 처리함.
  var str = "";
  for (var i = 0; i < hex.length && hex.substr(i, 2) !== "00"; i += 2)
    str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
  return str;
};

const hasUserDefinedHeader = userDefineHdr =>
  userDefineHdr != null && userDefineHdr.length > 0;

let tempArr = [];
let isAttachHeader = false;
const dataParser = (data, selectedNodeObj) => {
  tempArr = data.indexOf("\n") === -1 ? [data] : data.split("\n");

  let dataArr = null;
  let header = null;
  let userHeader = null;

  let isReadOnly = false;
  isAttachHeader = false;
  let delimit = null;
  let fieldCount = 1; // 필드가 정의되지 않은 한라인의 데이터를 하나의 필드 데이터로 봄.
  let userDefinedHeader = selectedNodeObj.userDefineHdr;

  isDebug && console.log(">>>> dataParser(start): ", tempArr);

  if (data.length >= 0) {
    // [1] 사용자 헤더가 있는 경우, 사용자 헤더를 우선으로 함
    if (hasUserDefinedHeader(userDefinedHeader)) {
      userHeader = [];
      let arr = selectedNodeObj.userDefineHdr.split("##");
      //isDebug && console.log("#### userdefinedHdr [IN]", arr);

      for (let i = 0; i < arr.length; i++) {
        let data = arr[i].trim();
        if (data.length > 0) {
          let h = data.split("=");

          // header 배열을 사용해도 되지만,
          // 매번 사용할 때마다 값을 비교해서 찾는 것보다는 한번 파싱할때 담아두고 사용하는 것이 효율적이라고 판단됨
          if (h[0].toLowerCase() === ENGINE_OP_HEADER.delimit.toLowerCase()) {
            delimit = hex2a(h[1]);
            isDebug && console.log("userdefined delimit=", delimit);
          }
          if (
            h[0].toLowerCase() === ENGINE_OP_HEADER["field,count"].toLowerCase()
          ) {
            fieldCount = h[1];
          }
          if (h[0].toLowerCase() === ENGINE_OP_HEADER.readonly.toLowerCase()) {
            isReadOnly = h[1].toLowerCase() === "false" ? false : true;
          }

          if (
            h[0].toLowerCase() === ENGINE_OP_HEADER.attachheader.toLowerCase()
          ) {
            isAttachHeader = h[1].toLowerCase() === "false" ? false : true;
          }

          userHeader.push(h);
        }
      }
      isDebug && console.log("#### userdefinedHdr [OUT]", userHeader);
    }

    // [2] 데이터에서
    //     헤더와 데이터를 분리해서 필요한 부분 추출
    //     헤더는 상단에 있음으로 첫줄에 헤더 구분자 ##이 없으면 헤더는 없다고 판단
    if (tempArr[0].indexOf("##") === -1 && !userHeader) {
      // [2.1] data는 존재하지만 헤더가 없는 경우, 하나의 필드를 data로 만들고 string 타입 사용.
      isDebug && console.log("#### DATA without Header [in]", tempArr);
      dataArr = [];
      header = [
        ["Field,1,Name", "data"],
        ["Field,1,Type", "String"]
      ];
      for (let i = 0; i < tempArr.length; i++) {
        let data = tempArr[i]; // 이슈: #36984 trim() 사용시에 tab도 삭제됨으로 tab 포함하는 경우 데이터가 잘림
        if (data.length > 0) {
          // 빈 라인 삭제를 위해서
          dataArr.push({ crudType: "R", data_0: data }); // UI 수정에서 사용할 CRUD 타입을 추가 (C 생성, R 읽기, U 변경, D 삭제 )
        }
      }
    } else {
      // [2.2] data도 있고 헤더도 있는 경우
      isDebug && console.log("#### DATA with Header [in]", tempArr);
      //console.log("#### DATA with Header [in]", tempArr);
      dataArr = [];
      header = [];
      for (let i = 0; i < tempArr.length; i++) {
        let data = tempArr[i]; // 이슈: #36984 trim() 사용시에 tab도 삭제됨으로 tab 포함하는 경우 데이터가 잘림
        let idx = data.indexOf("##");
        if (idx !== -1) {
          // [2.2.1] header
          let h = data.substring("##".length, data.length).split("=");
          h[2] = "R"; // crud type
          h[3] = i; // idx
          if (h[0].toLowerCase() === ENGINE_OP_HEADER.delimit.toLowerCase()) {
            delimit = delimit ? delimit : hex2a(h[1]);
            isDebug && console.log("dataParser, delimit=", delimit);
          }
          if (
            h[0].toLowerCase() === ENGINE_OP_HEADER["field,count"].toLowerCase()
          ) {
            fieldCount = fieldCount ? fieldCount : h[1];
            //isDebug && console.log("dataParser, fieldCount=", fieldCount);
          }
          if (h[0].toLowerCase() === ENGINE_OP_HEADER.readonly.toLowerCase()) {
            isReadOnly = isReadOnly
              ? isReadOnly
              : h[1].toLowerCase() === "false"
              ? false
              : true;
          }

          if (
            h[0].toLowerCase() === ENGINE_OP_HEADER.attachheader.toLowerCase()
          ) {
            isAttachHeader = h[1].toLowerCase() === "false" ? false : true;
          }
          header.push(h);
        } else {
          // [2.2.2] data
          if (data.length > 0) {
            // 빈 라인 삭제를 위해서
            if (delimit != null && delimit !== "") {
              // 2.2.1 구분자가 있는 경우
              let v = data.split(delimit);
              let va = { crudType: "R" };
              let idx = 0;
              isDebug && console.log("tempArr=", { data, delimit }, v);
              v.map(item => {
                va[`data_${idx}`] = item === "undefined" ? "" : item; // NodeInfo.js와 FieldGrid.js는 이 코드에 맞춰야함.
                idx++;
                return null;
              });

              if ((isAttachHeader && i > 0) || !isAttachHeader) {
                dataArr.push(va);
              }
            } else {
              // 2.2.2 구분자 없는 경우
              if ((isAttachHeader && i > 0) || !isAttachHeader) {
                dataArr.push({ crudType: "R", data_0: data });
              }
              //isDebug && console.log("dataParser, error-데이터 간의 구분자인 delimit 없음")
            }
          }
        }
      }
    }
    isDebug &&
      console.log(">>>> dataParser(end):", dataArr, header, userHeader);
    return { dataArr, header, userHeader, fieldCount, delimit, isReadOnly };
  } else {
    // 데이터로 없고, 헤더가 없는 경우 처리를 위해서
    if (!userHeader) {
      header = [
        ["Field,1,Name", "data"],
        ["Field,1,Type", "String"]
      ]; // 이슈: #36970
    }
    isDebug &&
      console.log(">>>> dataParser(end):", dataArr, header, userHeader);
    return {
      dataArr: null,
      header: header,
      userHeader: null,
      fieldCount: 0,
      delimit: null,
      isReadOnly: false
    };
  }
};

const checkHeader = (data, org) => {
  let i = 0;
  for (; i < data.length; i++) {
    if (data[i][2] !== "R") {
      isDebug && console.log("checkHeader true", data[i]);
      return true;
    }
  }
  isDebug && console.log("checkHeader", false);
  return false;
};

const checkNode = (data, hdrCnt) => {
  let i = 0;
  for (; i < data.length; i++) {
    if (
      data[i].crudType !== "R" && // original, read만 되었지 변경 사항은 없음
      !(
        data[i].crudType === "C" &&
        accumulatorRowFieldDataLen(data[i], hdrCnt) === 0
      )
    ) {
      isDebug && console.log("checkNode true", data[i]);
      return true;
    }
  }
  isDebug && console.log("checkNode", false);
  return false;
};

const accumulatorRowFieldDataLen = (rowDataArray, hdrCnt) => {
  let idx;
  let fieldName;
  let accumulator = 0;
  if (hdrCnt === 0) {
    fieldName = `data_${0}`;
    accumulator = accumulator + rowDataArray[fieldName].length;
  } else {
    for (idx = 0; idx < hdrCnt; idx++) {
      fieldName = `data_${idx}`;
      accumulator = accumulator + rowDataArray[fieldName].length;
    }
  }
  isDebug &&
    console.log(
      "accumulatorRowFieldDataLen",
      rowDataArray,
      hdrCnt,
      accumulator
    );
  return accumulator;
};

const makeSaveData = (headerCount, delimit, header, nodeData) => {
  let needToSave = false;
  let data = "";

  // 실제 헤더가 없는 경우 그리드로 표현하기 위해서 임시로 추가된 경우 제외
  let hdrCnt = headerCount === 1 && header[0][1] === "data" ? 0 : headerCount;

  isDebug && console.log("makeSaveData", hdrCnt, headerCount, delimit);
  isDebug && console.log("makeSaveData", header, nodeData);

  // header나 data 수정된 경우, 새로 저장함
  // 우선 변경 사항이 있는지 확인하고, 있으면 저장할 데이터를 생성하고 없으면 만들지 않음.
  if ((hdrCnt > 0 && checkHeader(header)) || checkNode(nodeData, hdrCnt)) {
    needToSave = true;
    // header
    hdrCnt > 0 &&
      header.map(h => {
        if (h[3] === "D") {
          // skip
        } else {
          data = `${data}##${h[0]}=${h[1]}\n`;
        }
        return null;
      });

    if (isAttachHeader) {
      data = `${tempArr[0]}\n`;
    }

    // data
    nodeData &&
      nodeData.length > 0 &&
      nodeData.map(d => {
        let idx = 0;
        let fieldName = `data_${idx}`;
        let temp = d[fieldName]; // 첫번째 필드 데이터 index = 0
        if (temp === undefined) temp = "";

        if (d.crudType === "D") {
          // skip
        } else if (
          d.crudType === "C" &&
          accumulatorRowFieldDataLen(d, hdrCnt) === 0
        ) {
          // skip
        } else {
          if (hdrCnt === 0) {
            // header 없는 경우, 파싱시에 임으로 data_0에 count정보 0으로 만들어서 필요한 처리
            data = `${data}${temp}\n`;
          } else {
            // header가 있는 경우
            data = `${data}${temp}`; // 두번째 필드 데이터 index = 1
            for (idx = 1; idx < hdrCnt; idx++) {
              fieldName = `data_${idx}`;
              temp = d[fieldName];
              if (temp === undefined) temp = "";
              data = `${data}${delimit}${temp}`;
            }
            data = `${data}\n`;
          }
        }
        return null;
      });
  }

  isDebug && console.log("makeSaveData", needToSave, data);
  return { needToSave, data };
};

export { dataParser, makeSaveData };
