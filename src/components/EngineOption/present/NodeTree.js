import React, { Component } from 'react';
import Tree, { TreeNode } from 'rc-tree';

import 'react-contexify/dist/ReactContexify.min.css';
import '../style.scss';

const getArrayLen = arr => (arr != null ? Object.keys(arr).length : 0);

class NodeTree extends Component {
  componentDidMount() {}

  handleSeleteNode(selectedKeys, info) {
    let _selectedNode = info.node.props.targetData;

    if (_selectedNode.hasSubNodes && _selectedNode.subNode[0] === undefined) {
      info.node.props.getChildNodeDataCB(
        _selectedNode,
        info.node.props.zkNodeMgmt
      );
    } else {
      info.node.props.getDataCB(_selectedNode, info.node.props.zkNodeMgmt);
    }
  }

  makeKey = nd => {
    if (nd.fullPath === undefined) {
      return nd.nodePath;
    } else {
      return nd.fullPath;
    }
  };

  render() {
    const { nodeTree, getChildNodeData, getData, zkNodeMgmt } = this.props;

    const loop = data => {
      return data.map((nd, i) => {
        if (getArrayLen(nd.subNode) > 0) {
          return (
            <TreeNode
              title={nd.nodePath}
              key={this.makeKey(nd)}
              targetData={nd}
              getChildNodeDataCB={getChildNodeData}
              getDataCB={getData}
              zkNodeMgmt={zkNodeMgmt}>
              {loop(nd.subNode)}
            </TreeNode>
          );
        }

        return (
          <TreeNode
            title={nd.nodePath}
            key={this.makeKey(nd)}
            targetData={nd}
            getChildNodeDataCB={getChildNodeData}
            getDataCB={getData}
            zkNodeMgmt={zkNodeMgmt}
            isLeaf={!nd.hasSubNodes}
          />
        );
      });
    };

    const treeNodes = loop(nodeTree);
    return (
      <Tree
        // defaultExpandAll
        selectable={true}
        showIcon={true}
        onSelect={this.handleSeleteNode}>
        {nodeTree !== undefined && getArrayLen(nodeTree) && treeNodes}
      </Tree>
    );
  }
}

export default NodeTree;
