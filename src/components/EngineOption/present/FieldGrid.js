import React, { Component } from "react";
import { connect } from "react-redux";

import GridTable from "../../Common/GridTable";
import ReactTooltip from "react-tooltip";

import { isDebug } from "../const/enginOptionConfig";

import "../style.scss";
import {
  enginOptConfirmPopup,
  enginOptPopupType
} from "../const/popupResource";
import EnterableConfirmPopup from "../popups/EnterableConfirmPopup";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class FieldGrid extends Component {
  state = {
    currentNodeData: null,

    inputRef: null,
    startPosition: 0,

    deletePopupResource: null
  };

  selectStateOption = (o = null) => {
    let stateSlect = document.getElementById("filterGrid-state-select");
    if (o == null) {
      o = stateSlect.value;
    }

    stateSlect.value = o;
  };

  handleChange = (e, cell, value) => {
    e.preventDefault();
    //isDebug && console.log("handleChange, cell=", cell);
    //isDebug && console.log("handleChange, targetValue=", value);
    this.currentNodeData[cell.index][cell.column.id] = value;
    this.props.handleUpdateChangedData(this.currentNodeData);
  };

  // input에 tab 추가 후에 바로 위치 조정하면, 원하는대로 위치 조정이 안됨
  // 값 설정이 완료 되면, cb으로 받아서 위치 조정하도록 수정함
  setPositon = () => {
    const { inputRef, startPosition } = this.state;

    if (inputRef) {
      inputRef.selectionStart = inputRef.selectionEnd = startPosition + 1;
      this.setState({
        inputRef: null,
        startPosition: startPosition
      });
    }
  };

  handleKeyDown = (e, cell) => {
    if (e.keyCode === 9) {
      // tab 0x09
      e.preventDefault();

      let value = cell.inputRef.value;
      let start = cell.inputRef.selectionStart; // 현재 포커스를 기준으로 삽입하기 위해서
      let end = cell.inputRef.selectionEnd;

      isDebug &&
        console.log("handleKeyDown, cell=", cell, start, end, `[${value}]`);

      value = value.slice(0, start) + "\t" + value.slice(end);

      this.setState({
        inputRef: cell.inputRef,
        startPosition: start
      });
      this.currentNodeData[cell.index][cell.column.id] = value;
      this.props.handleUpdateChangedData(this.currentNodeData, () => {
        this.setPositon();
      });
    }
  };

  handleEdit = cell => {
    this.currentNodeData[cell.index].crudType = "U";
    this.props.handleUpdateChangedData(this.currentNodeData);
    isDebug &&
      console.log("handleEdit", cell, this.currentNodeData[cell.index]);
  };

  confirmDelete = cell => {
    let resource = enginOptConfirmPopup(
      enginOptPopupType.delRow,
      this.closeConfirmPopup,
      this.handleDelete
    );

    resource.target = cell;
    this.openConfirmPopup(resource);
  };

  /* one/two 버튼의 confirm 팝업 하나로 처리 */
  openConfirmPopup = resource => {
    this.setState({
      deletePopupResource: resource
    });
  };

  closeConfirmPopup = () => {
    this.setState({
      deletePopupResource: null
    });
  };

  handleDelete = resource => {
    let cell = resource.target;

    if (cell.original.crudType === "C") {
      this.currentNodeData.shift();
      this.props.handleUpdateChangedData(this.currentNodeData);
    } else {
      this.currentNodeData[cell.index].crudType = "D";
      this.props.handleUpdateChangedData(this.currentNodeData);
    }

    isDebug &&
      console.log("handleDelete", cell, this.currentNodeData[cell.index]);
  };

  handleRevert = cell => {
    isDebug &&
      console.log(
        "handleRevert",
        cell.original,
        this.currentNodeData[cell.index],
        this.props.nodeDataOrg[cell.index]
      );

    // 되돌리기는 완전 초기상태로 돌리도록
    this.currentNodeData[cell.index] = {
      ...this.props.nodeDataOrg[cell.index]
    };
    this.props.handleUpdateChangedData(this.currentNodeData);
  };

  // handleSearch= (e) => {
  //   isDebug && console.log("handleSearch", e)
  // }

  // handleSearchUndo= (e) => {
  //   isDebug && console.log("handleSearchUndo", e)
  // }

  handleAddRow = () => {
    // if (this.currentNodeData != null && this.currentNodeData.length > 0) {
    let rowData = { crudType: "C" };
    let idx = 0;
    let count = this.props.fieldCount;
    for (; idx < count; idx++) {
      // 필드 개수 만큼
      rowData[`data_${idx}`] = "";
    }
    this.currentNodeData.unshift(rowData); // 요구사항: 최상 위에 새로 추가될 row 위치 시키는 것

    this.props.handleUpdateChangedData(this.currentNodeData);
    isDebug &&
      console.log(
        "handleAddRow:",
        this.currentNodeData[0],
        this.currentNodeData[1]
      );
    // }
  };

  renderEditable = cell => {
    let crudType = cell.original.crudType;

    switch (crudType) {
      case "C":
      case "U":
        //isDebug && console.log("renderEditable: ", cell)
        return (
          <FormattedMessage id="enter.new.data">
            {placeholder => (
              <input
                ref={input => {
                  cell.inputRef = input;
                }}
                style={{ width: "-webkit-fill-available", textAlign: "left" }}
                placeholder={placeholder}
                value={this.currentNodeData[cell.index][cell.column.id]}
                onChange={e => this.handleChange(e, cell, e.target.value)}
                onKeyDown={e => this.handleKeyDown(e, cell)}
                className="form-control"
              />
            )}
          </FormattedMessage>
        );
      case "R":
      default:
        const tooltipId = `enginOptTooltip_${cell.original.no}_${cell.column.id}`;
        return (
          <React.Fragment>
            <div data-tip data-for={tooltipId}>
              {cell.original[cell.column.id]}
            </div>
            <ReactTooltip
              id={tooltipId}
              className="tooltipClass"
              place="right"
              type="dark"
              effect="float"
              event="mouseover"
              globalEventOff="mouseout"
            >
              {cell.original[cell.column.id]}
            </ReactTooltip>
          </React.Fragment>
        );
      // (
      //   <div data-tip={cell.original[cell.column.id]} data-for="tooltips"
      //       style={{paddingLeft: "5px", paddingRight: "5px"}}>
      //     {cell.original[cell.column.id]}
      //   </div>
      // )
    }
  };

  getActionBtn = cell => {
    let crudType = cell.original.crudType;

    switch (crudType) {
      case "C":
        return (
          <div>
            <button
              id="eoDelete"
              className="btn btn-icon"
              onClick={() => this.confirmDelete(cell)}
            >
              <img src="/images/common/icon_delete.png" alt="icon_delete" />
            </button>
          </div>
        );

      case "U":
      case "D":
        return (
          <div>
            <button
              id="eoDelete"
              className="btn btn-icon"
              onClick={() => this.confirmDelete(cell)}
            >
              <img src="/images/common/icon_delete.png" alt="icon_delete" />
            </button>
            <button
              className="btn btn-icon btn-rollback"
              onClick={() => this.handleRevert(cell)}
            >
              <img src="/images/common/icon_rollback.png" alt="icon_rollback" />
            </button>
            {/* <button id="eoUndo" className="btn btn-icon" onClick={() => this.handleRevert(cell)}>
            <i className="fas fa-undo"/>
          </button> */}
          </div>
        );

      case "R":
      default:
        return (
          <div>
            <button
              id="eoDelete"
              className="btn btn-icon"
              onClick={() => this.confirmDelete(cell)}
            >
              <img src="/images/common/icon_delete.png" alt="icon_delete" />
            </button>
            <button
              id="eoEdit"
              className="btn btn-icon"
              onClick={() => this.handleEdit(cell)}
            >
              <img src="/images/common/icon_edit.png" alt="icon_edit" />
            </button>
          </div>
        );
    }
  };

  makeData = (data, header) => {
    data.map((d, i) => {
      d["no"] = i;
      d["action"] = "";
    });

    //isDebug && console.log(data);
    return data;
  };

  makeColumns = (header, isReadOnly) => {
    let columns = [];
    let idx = 0;

    columns.push({
      Header: "No.",
      accessor: "no",
      width: 50,
      style: { textAlign: "center" },
      sortable: true,
      Cell: ({ value }) => value + 1 // cell.original.no + 1
    });

    header.map((h, i) => {
      columns.push({
        Header: h[0],
        accessor: `data_${idx}`,
        style: { textAlign: "left" },
        sortable: true,
        filterable: true,
        resizable: false,
        Cell: this.renderEditable
      });
      idx++;
    });

    // react-table: Custom Filtering 예제 참조
    !isReadOnly &&
      columns.push({
        Header: (
          <IntlMessages id="change.status" />
        ),
        accessor: "crudType",
        width: 120,
        style: { textAlign: "center" },
        filterable: true,
        Cell: ({ value }) => {
          switch (value) {
            case "R":
            default:
              return "";
            case "U":
              return (
                <FormattedMessage id="change">{text => text}</FormattedMessage>
              );
            case "D":
              return (
                <FormattedMessage id="delete">{text => text}</FormattedMessage>
              );
            case "C":
              return (
                <FormattedMessage id="create">{text => text}</FormattedMessage>
              );
          }
        },
        Filter: ({ filter, onChange }) => (
          <select
            id="filterGrid-state-select"
            onChange={event => onChange(event.target.value)}
            style={{ width: "100%", textAlign: "center" }}
            value={filter ? filter.value : "all"}
          >
            <option value="all" style={{ textAlign: "center" }}>
              {this.props.language === "en" ? "All" : "모두"}
            </option>
            <option value="all-without-del" style={{ textAlign: "center" }}>
              {this.props.language === "en"
                ? "All(Not delete)"
                : "모두(삭제제외)"}
            </option>
            <option value="new" style={{ textAlign: "center" }}>
              {this.props.language === "en" ? "Create" : "생성"}
            </option>
            <option value="edit" style={{ textAlign: "center" }}>
              {this.props.language === "en" ? "Change" : "변경"}
            </option>
            <option value="delet" style={{ textAlign: "center" }}>
              {this.props.language === "en" ? "Delete" : "삭제"}
            </option>
          </select>
        ),
        filterMethod: (filter, row) => {
          switch (filter.value) {
            case "all":
            default:
              return true;
            case "all-without-del":
              return row[filter.id] !== "D"; // return true: 모두
            case "new":
              return row[filter.id] === "C";
            case "edit":
              return row[filter.id] === "U";
            case "delet":
              return row[filter.id] === "D";
          }
        }
      });

    !isReadOnly &&
      columns.push({
        Header: "Actions",
        accessor: "action",
        width: 60,
        style: { textAlign: "right" },
        Cell: this.getActionBtn
      });

    return columns;
  };

  chHeaderData = header => {
    let fieldArr = [];
    header.map((item, i) => {
      if (item[0].toLowerCase().indexOf("field") === -1) {
        // field 시작하지 않는 것은 버린다
      } else {
        let temp = item[0].split(",");
        if (temp[2] === "Name") {
          fieldArr[temp[1]] = [];
          fieldArr[temp[1]].push(item[1]);
        } else if (temp[2] === "Type") {
          // Type
          fieldArr[temp[1]].push(item[1]);
        }
      }
    });
    //isDebug && console.log("chHeaderData, ", fieldArr);
    return fieldArr;
  };

  render() {
    const { header, isReadOnly } = this.props;

    //isDebug && console.log(header, this.currentNodeData);
    //isDebug && console.log("isReadOnly : ", isReadOnly);

    let chHdr = this.chHeaderData(header);

    // grid에서 사용하는 데이터는 따로 복사해서 사용함
    this.currentNodeData = this.props.nodeData.slice();

    return (
      <div>
        <div className="component__title">
          {!isReadOnly && (
            <div className="btns">
              <button
                className="btn btn--blue"
                onClick={() => this.handleAddRow()}
              >
                <i className="fas fa-plus" />
                <FormattedMessage id="add.row">{text => text}</FormattedMessage>
              </button>
            </div>
          )}
        </div>
        <GridTable
          columns={this.makeColumns(chHdr, isReadOnly)}
          data={this.makeData(this.currentNodeData, chHdr)}
          pageSizeOptions={[10, 15, 20, 50, 100]}
          defaultFilterMethod={(filter, row) => {
            // 검색을 위한 function 정의
            // isDebug && console.log("filter", filter, row);
            if (
              row._original.action !== "D" &&
              row[filter.id] &&
              row[filter.id].indexOf(filter.value) > -1
            )
              return true;
          }}
        />

        {this.state.deletePopupResource != null && (
          <EnterableConfirmPopup resource={this.state.deletePopupResource} />
        )}

        {/* <ReactTooltip id="tooltips" effect="float" place="bottom"/> */}
      </div>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({})
)(FieldGrid);
