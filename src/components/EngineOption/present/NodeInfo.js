import React, { Component } from "react";
import {
  isDebug,
  ENGINE_OP_HEADER,
  EDITABLE_HEADER
} from "../const/enginOptionConfig";
import { FormattedMessage } from "react-intl";

class NodeInfo extends Component {
  state = {
    currentHeader: null
  };

  handleEdit = data => {
    let idx = data[3];
    this.currentHeader[idx][2] = "U";
    this.props.handleUpdateChangedHeader(this.currentHeader);
  };

  handleRevert = data => {
    let idx = data[3];
    isDebug &&
      console.log(
        "handleRevert",
        this.currentHeader[idx],
        this.props.headerOrg[idx]
      );

    this.currentHeader[idx] = { ...this.props.headerOrg[idx] };
    this.props.handleUpdateChangedHeader(this.currentHeader);
  };

  handleChange = (e, data, idx, value) => {
    e.preventDefault();
    this.currentHeader[idx][1] = value;
    this.props.handleUpdateChangedHeader(this.currentHeader);
  };

  makeTD = (data, idx) => {
    let crudType = data[2];
    switch (crudType) {
      case "U":
        return (
          <td>
            <input
              style={{ width: "-webkit-fill-available", textAlign: "left" }}
              value={data[1]}
              onChange={e => this.handleChange(e, data, idx, e.target.value)}
              className="form-control"
            />
          </td>
        );

      case "R":
      default:
        return <td>{data[1]}</td>;
    }
  };

  makeBtn = (data, idx) => {
    let crudType = data[2];

    switch (crudType) {
      case "U":
        return (
          <div>
            <button
              id="eoUndo"
              className="btn btn-icon"
              onClick={() => this.handleRevert(data)}
            >
              <i className="fas fa-undo" />
            </button>
          </div>
        );

      case "R":
      default:
        return (
          <div>
            <button
              id="eoEdit"
              className="btn btn-icon"
              onClick={() => this.handleEdit(data)}
            >
              <i className="fas fa-edit" />
            </button>
          </div>
        );
    }
  };

  rederNoHeader = () => {
    return (
      <tr>
        <th>
          <FormattedMessage id="node.header"> {text => text} </FormattedMessage>
        </th>
        <td>
          <FormattedMessage id="no.info.set"> {text => text} </FormattedMessage>
        </td>
        <td />
      </tr>
    );
  };

  render = () => {
    const { header, isReadOnly } = this.props;

    // 필터로 시작하는 정보는 제거 표시되지 않는다.
    // const chHeaderData = (header) => {
    //   let fieldArr = header.filter(item =>
    //     (item[0].toLowerCase().indexOf("field") == -1 || item[0].toLowerCase().indexOf("field,count") !== -1))
    //   return fieldArr;
    // }

    const check = h => {
      return (
        h[0].toLowerCase().indexOf("field") === -1 ||
        h[0].toLowerCase().indexOf("field,count") !== -1
      );
    };

    const makeTR = (h, i) => {
      if (check(h) && h[1]) {
        return (
          <tr key={h[0] + "_" + i}>
            <th>{ENGINE_OP_HEADER[h[0].toLowerCase()]}</th>
            {!isReadOnly ? this.makeTD(h, i) : <td>{h[1]}</td>}
            {!isReadOnly && EDITABLE_HEADER[h[0].toLowerCase()] ? (
              <td>{this.makeBtn(h, i)}</td>
            ) : (
              <td />
            )}
          </tr>
        );
      }
    };

    // header는 [][]로 구성되면, 0 - name, 1 - value, 2, - crudtype, 3 -index
    this.currentHeader = header.slice();
    //isDebug && console.log("NodeInfo: ", header, this.currentHeader, isReadOnly)

    return (
      <table className="table table--dark table--info security-event">
        <colgroup>
          <col width="150px" />
          <col />
          <col width="100px" />
        </colgroup>
        <tbody>
          {this.currentHeader && this.currentHeader.length > 0
            ? this.currentHeader.map((h, i) => makeTR(h, i))
            : this.rederNoHeader()}
        </tbody>
      </table>
    );
  };
}

export default NodeInfo;
