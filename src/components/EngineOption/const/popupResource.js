import React from "react";

import { FormattedMessage } from "react-intl";

const ONE_BTN_CONFIRM = confirmCB => [
  {
    name: <FormattedMessage id="confirm"> {text => text} </FormattedMessage>,
    CB: confirmCB
  }
];

const TWO_BTN_CONFIRM = (cancelCB, confirmCB) => [
  {
    name: <FormattedMessage id="cancel"> {text => text} </FormattedMessage>,
    CB: cancelCB
  },
  {
    name: <FormattedMessage id="confirm"> {text => text} </FormattedMessage>,
    CB: confirmCB
  }
];

const enginOptPopupType = {
  saveToZookeeper: "SAVE_TO_ZOOKEEPER",
  noChangeddata: "NO_CHANGED_DATA",
  doneSave: "DONE_SAVE",
  delRow: "DELETE_ROW"
};

const enginOptConfirmPopup = (type, cancelCB = null, confirmCB = null) => {
  switch (type) {
    case "SAVE_TO_ZOOKEEPER":
      return {
        title: (
          <FormattedMessage id="zookeeper.apply">
            {text => text}
          </FormattedMessage>
        ),
        message: (
          <FormattedMessage id="apply.zookeeper.content">
            {text => text}
          </FormattedMessage>
        ),
        hasChangeReason: false,
        btns: TWO_BTN_CONFIRM(cancelCB, confirmCB)
      };

    case "NO_CHANGED_DATA":
      return {
        title: (
          <FormattedMessage id="zookeeper.apply">
            {text => text}
          </FormattedMessage>
        ),
        message: (
          <FormattedMessage id="no.change.made">
            {text => text}
          </FormattedMessage>
        ),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    case "DONE_SAVE":
      return {
        title: (
          <FormattedMessage id="zookeeper.apply">
            {text => text}
          </FormattedMessage>
        ),
        message: (
          <FormattedMessage id="apply.zookeeper.complete">
            {text => text}
          </FormattedMessage>
        ),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    case "DELETE_ROW":
      return {
        title: (
          <FormattedMessage id="row.delete"> {text => text} </FormattedMessage>
        ),
        message: (
          <FormattedMessage id="delete.select.column">
            {text => text}
          </FormattedMessage>
        ),
        hasChangeReason: false,
        btns: TWO_BTN_CONFIRM(cancelCB, confirmCB)
      };
    default:
      return;
  }
};

export { enginOptPopupType, enginOptConfirmPopup };
