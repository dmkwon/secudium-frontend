const isDebug = false;

const MAX_READ_ROWCOUNT = 100; // paging 단위
const INIT_NODE_DEPTH = 3; // 초기 읽어올 node depth

const ENGINE_OP_HEADER = {
  name: "Name",
  delimit: "Delimit",
  description: "Description",
  owner: "Owner",
  readonly: "ReadOnly",
  writeselfnode: "WriteSelfNode",
  attachheader: "AttachHeader",
  "field,count": "Field,Count"
};

const EDITABLE_HEADER = {
  name: true,
  delimit: true,
  description: true,
  owner: true,
  readonly: false,
  writeselfnode: false,
  attachheader: false,
  "field,count": false
};

export {
  isDebug,
  MAX_READ_ROWCOUNT,
  INIT_NODE_DEPTH,
  ENGINE_OP_HEADER,
  EDITABLE_HEADER
};
