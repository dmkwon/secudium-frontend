import React, { Component } from 'react'
import Draggable from 'react-draggable'
import { isDebug } from '../../FilterMgmt/const/filterConfig';

export default class EnterableConfirmPopup extends Component {
  state = {
    position: {
      x: 0,
      y: 0
    }
  }

  componentDidMount () {
    const elWidth = document.getElementById('commonConfirmPopup').offsetWidth
    const elHeight = document.getElementById('commonConfirmPopup').offsetHeight

    this.setState({
      position : { x : Math.floor(elWidth / 2), y : Math.floor(elHeight / 2 * -1) }
    })

    this.confirmBtn.focus();
  }

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    })
  }

  handleKeyDown= (e) => {
    isDebug && console.log("key down", e, e.keyCode)
    if (e.keyCode === 13) {
      e.preventDefault();
      const { resource } = this.props
      resource.btns[0].CB();
      resource.btns[1].CB(resource, this.state.changeReason);
    }
  }

  render() {
    const { resource } = this.props // title, message, hasChangeReason, btns 으로 구성된 object 

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={this.state.position}
          onDrag={this.handleDrag}
        >

        <div className="popup popup--alert" id="commonConfirmPopup">
          <div className="popup__header" style={{cursor: "move"}}>
            <h5>{resource.title}</h5>
          </div>
          <div className="popup__body">
            {resource.message}
          </div>

          <div className="popup__footer">
              <button key={"btn-key-1"} className="btn btn--white" 
                  onClick={() => {resource.btns[0].CB()}} >
                {resource.btns[0].name}
              </button>
              <button key={"btn-key-2"} className="btn btn--dark" 
                  ref={(btn) => this.confirmBtn = btn}
                  onKeyDown={(e) => this.handleKeyDown(e)}
                  onClick={() => {
                    resource.btns[0].CB();
                    resource.btns[1].CB(resource, this.state.changeReason);
                  }}>
                { resource.btns[1].name }
              </button>
          </div>
        </div>
        </Draggable>
      </React.Fragment>
    )
  }
}