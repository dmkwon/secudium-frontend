import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Dropdown from "../../Common/Dropdown";

import * as zkOptionActions from "./../../../store/modules/engineoption";
import { createZkOptions } from "./../../../store/api/engineoption";

import { FormattedMessage } from "react-intl";

import "./style.scss";

class OptionSettingPopup extends Component {
  state = {
    optionType: "Normalizer",
    optionTypes: ["Normalizer", "Forwarder", "Anlyzer"]
    // optionNm: "",
    // zkNodeCd: "",
    // zkNodePath: "",
    // exprnClascd: "",
    // optionDesc: "",
    // useYn: "",
    // regUsrId: "",
    // regDate: "",
    // modUsrId: "",
    // modDate: ""
  };

  onChangeInput = e => {
    const { name, value } = e.target;
    this.setState({
      ...this.state,
      [name]: value
    });
  };

  onClickSave = async e => {
    const engineOption = this.state;

    try {
      await createZkOptions(engineOption);
      this.setState({
        optionNm: "11",
        zkNodeCd: "N",
        zkNodePath: "11",
        exprnClascd: "B",
        optionDesc: "11",
        useYn: "Y"
      });
    } catch (e) {
      console.warn(e);
    }
  };

  handleChangeForDropdown = option => {
    this.setState({
      optionType: option.value
    });
  };

  render() {
    const {
      optionType,
      optionTypes,
      optionNm,
      zkNodePath,
      exprnClascd,
      optionDesc
    } = this.state;
    const { closeAllPopup } = this.props;
    const { onClickSave, onChangeInput } = this;

    return (
      <React.Fragment>
        <div className="popup" id="optionSettingPopup">
          <div className="popup__header">
            <h5>
              <FormattedMessage id="option.create">
                {text => text}
              </FormattedMessage>
            </h5>
            {/* <FormattedMessage id="option.edit"> { text => text } </FormattedMessage> */}
            <button className="btn btn-close" onClick={closeAllPopup} />
          </div>
          <div className="popup__body">
            <div className="option-wrapper">
              <div className="form-group">
                <label>
                  <FormattedMessage id="option.label">
                    {text => text}
                  </FormattedMessage>
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="optionNm"
                  onChange={onChangeInput}
                  value={optionNm}
                />
              </div>

              <div className="form-group">
                <label>
                  <FormattedMessage id="engine.class">
                    {text => text}
                  </FormattedMessage>
                </label>
                <Dropdown
                  name="engineType"
                  value={optionType}
                  options={optionTypes}
                  handleChange={this.handleChangeForDropdown}
                />
              </div>

              <div className="form-group">
                <label>
                  <FormattedMessage id="engine.class">
                    {text => text}
                  </FormattedMessage>
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="zkNodePath"
                  onChange={onChangeInput}
                  value={zkNodePath}
                />
              </div>

              <div className="form-group">
                <label>
                  <FormattedMessage id="value.type">
                    {text => text}
                  </FormattedMessage>
                  :
                </label>
                <div className="radio-wrapper">
                  <div className="radio">
                    <label>
                      <input type="radio" name="valueType" />
                      <div className="icon" />
                      <span>True / False</span>
                    </label>
                  </div>
                  <div className="log">
                    <label>True</label>
                    <input type="text" className="form-control" />
                    <label>False</label>
                    <input
                      type="text"
                      className="form-control"
                      style={{ marginRight: 0 }}
                    />
                  </div>

                  <div className="radio">
                    <label>
                      <input
                        type="radio"
                        name="exprnClascd"
                        onChange={onChangeInput}
                        value={exprnClascd}
                      />
                      <div className="icon"></div>
                      <span>
                        <FormattedMessage id="text.single">
                          {text => text}
                        </FormattedMessage>
                      </span>
                    </label>
                  </div>

                  <div className="radio">
                    <label>
                      <input
                        type="radio"
                        name="exprnClascd"
                        onChange={onChangeInput}
                        value={exprnClascd}
                      />
                      <div className="icon"></div>
                      <span>
                        <FormattedMessage id="multiline.text">
                          {text => text}
                        </FormattedMessage>
                      </span>
                    </label>
                  </div>
                </div>
              </div>

              <div className="form-group">
                <label>
                  <FormattedMessage id="desc">{text => text}</FormattedMessage>
                </label>
                <textarea
                  className="form-control"
                  name="optionDesc"
                  onChange={onChangeInput}
                  value={optionDesc}
                ></textarea>
              </div>
            </div>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={closeAllPopup}>
              <FormattedMessage id="cancel"> {text => text} </FormattedMessage>
            </button>
            <button className="btn btn--dark" onClick={onClickSave}>
              <FormattedMessage id="save"> {text => text} </FormattedMessage>
            </button>
          </div>
        </div>
        <div className="dim" onClick={closeAllPopup} />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    engineOption: state.engineoption.get("engineOption")
  }),
  dispatch => ({
    ZkOptionActions: bindActionCreators(zkOptionActions, dispatch)
  })
)(OptionSettingPopup);
