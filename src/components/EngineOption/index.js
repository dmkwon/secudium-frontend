import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import Loading from "../Common/Loading";

import NodeTree from "./present/NodeTree";
import FieldGrid from "./present/FieldGrid";
import NodeInfo from "./present/NodeInfo";

import { dataParser, makeSaveData } from "./dataParser";

import { action as zkNodeMgmtActions } from "../../store/modules/enginOptionMgmt";

import ConfirmPopup from "../Common/Popups/CommonConfirmPopup";
import { enginOptPopupType, enginOptConfirmPopup } from "./const/popupResource";

import "./style.scss";
import { isDebug } from "./const/enginOptionConfig";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class EngineOptionComponent extends Component {
  state = {
    nodeListOrg: null,
    nodeList: null, // 노드 트리를 구성할 리스트

    baseNodePath: "",
    selectedNodeObj: null, // 트리 중에 선택된 노드
    selectedNodePath: "", // 선택된 노드의 경로

    // 선택된 노드의 헤더와 그 데이터
    selectedNodeUserHdr: null, // 파싱해서 리스트로 만들어진, 사용자 헤더
    selectedNodeHdr: null, // 노드에 포함된 헤더 정보
    selectedNodeHdrOrg: null, // 변경이 가능합으로 오리지널 백본을 만듦
    selectedNodeData: null, // 변경된 현재상태의 데이터
    selectedNodeDataOrg: null, // 수정되기 전 원본 데이터

    fieldCount: 0,
    delimit: null,
    isReadOnly: false,

    isShowConfirmPopup: false,
    confirmPopupResource: null,

    isLoading: false,

    dataToSave: null, // 주키퍼에 저장을 위해서 변환된 데이터(임시 저장)

    searchKeyWord: ""
  };

  componentDidMount() {
    this.getInitData(); // 초기 3depth 노드 정보를 읽어옴.
  }

  // 화면에 진입해서 초기 데이터 가져오는 함수
  getInitData = async () => {
    const { zkNodeMgmt } = this.props;
    this.setState({ isLoading: true });

    try {
      let initData = await zkNodeMgmt.getInitData();
      let nodeTreeData = this.constMakeNodeData(initData[1].data, null);
      if (initData[1].status === 200) {
        this.setState({
          baseNodePath: initData[0].data.rootPath,
          nodeList: nodeTreeData,
          nodeListOrg: nodeTreeData
        });

        this.setState({
          isLoading: false,
          selectedNodeObj: null,
          selectedNodePath: (
            <FormattedMessage id="no.select"> {text => text} </FormattedMessage>
          )
        });
      }
      isDebug && console.log("getInitData", initData, nodeTreeData);
    } catch (e) {
      this.setState({ isLoading: false });
      isDebug && console.log("getInitData", e);
    }
  };

  constMakeNodeData = (data, pPath) => {
    this.putTreePath(data, pPath);
    return data;
  };

  // tree 구조에서 위치: [0,0,2]의 의미는 1depth 첫번째, 2depth 첫번째, 3depth 세번째
  putTreePath = (data, pPath, pTreePath = undefined) => {
    //isDebug && console.log("putTreePath", data, pTreePath)
    data.sort(function(a, b) {
      // 오름차순
      return a.nodePath < b.nodePath ? -1 : a.nodePath > b.nodePath ? 1 : 0;
    });

    data.map((nd, i) => {
      if (pTreePath == null || nd.treePath === undefined) {
        if (pTreePath == null && pTreePath === undefined) {
          nd.treePath = []; // 생성
        } else {
          nd.treePath = pTreePath.slice(); // 부모 트리 경로 복사
        }
      }
      nd.treePath.push(i);
      nd.fullPath = (pPath != null ? pPath + "/" : "") + nd.nodePath;

      if (
        nd.subNode != null &&
        nd.subNode[0] != null &&
        nd.subNode[0] !== undefined
      ) {
        this.putTreePath(nd.subNode, nd.fullPath, nd.treePath);
      }
      return null;
    });
  };

  // sub 노드들을 가져오면, 기존 노드 리스트에 끼워 넣는다. path는 배열의 위치 정보를 의미함.
  insetChildNode = (chNode, target) => {
    const { nodeList } = this.state;
    let treePath = target.treePath;

    //isDebug && console.log("0. treePath = ", treePath);
    let fNode = nodeList;
    treePath.map((idx, i) => {
      fNode = fNode[idx]; // 계증적으로 선택된 node를 가진다.
      //isDebug && console.log("1. path = ", idx, i, fNode.fullPath, target.fullPath);
      if (fNode.fullPath === target.fullPath) {
        fNode.subNode = this.constMakeNodeData(chNode, target.fullPath);
        //isDebug && console.log("2", fNode.subNode, nodeList);
        return true;
      } else {
        fNode = fNode.subNode;
      }
      return null;
    });
  };

  // 3단 이상의 경우, 데이터가 없는 노드의 서브 디렉토리 가져오는 함수
  getChildNodeData = async (target, zkNodeMgmt) => {
    this.setState({
      isLoading: true,
      selectedNodePath: target.fullPath,
      selectedNodeObj: target
    });

    try {
      let nodeData = await zkNodeMgmt.getChildNode({ path: target.fullPath });
      this.insetChildNode(nodeData.data, target);
      isDebug && console.log("getChildNodeData", nodeData);
    } catch (e) {
      isDebug && console.log("getChildNodeData", e);
    } finally {
      this.setState({ isLoading: false });
    }
  };

  // utility - 레퍼런스 복사로 오리지널 유지 되지 않아 함수로 분리함
  deepCopyDataArray = dataArray => {
    let dataOrg = [];
    dataArray.map(obj => {
      let data = { ...obj }; // object 복사
      dataOrg.push(data);
      return null;
    });
    return dataOrg;
  };

  // utility - 레퍼런스 복사로 오리지널 유지 되지 않아 함수로 분리함
  deepCopHeaderArray = haderArray => {
    let headerOrg = [];
    haderArray.map(obj => {
      let data = [...obj]; // array 복사
      headerOrg.push(data);
      return null;
    });
    return headerOrg;
  };

  // 노드에 데이터가 포함되어 있으면, 이를 가져오는 함수
  getData = async (target, zkNodeMgmt) => {
    this.setState({
      isLoading: true,
      selectedNodePath: target.fullPath,
      selectedNodeObj: target
    });

    try {
      // path에 맞는 데이터를 요청하고 받아 옴
      let nodeData = await zkNodeMgmt.getNodeData({ path: target.fullPath });
      isDebug && console.log("getData, zookeeper부터 가져옴 데이터:", nodeData);

      if (nodeData.status === 200) {
        if (typeof nodeData.data !== "string") {
          isDebug &&
            console.error("response data is not string:", typeof nodeData.data);
          nodeData.data = nodeData.data.toString(); // #36977 숫자만 한줄 들어간 경우 UI 표시 안됨
        }
        // 데이터를 헤더에 따라 파싱함.
        let tem = dataParser(nodeData.data, this.state.selectedNodeObj);
        isDebug && console.log("getData, 헤더로 파싱한:", tem);

        this.setState({
          selectedNodeData: tem.dataArr ? tem.dataArr : [],
          selectedNodeDataOrg: tem.dataArr
            ? this.deepCopyDataArray(tem.dataArr)
            : [],
          selectedNodeHdr: tem.header,
          selectedNodeHdrOrg: tem.header
            ? this.deepCopHeaderArray(tem.header)
            : tem.header,
          selectedNodeUserHdr: tem.userHeader,
          fieldCount: tem.fieldCount,
          delimit: tem.delimit,
          isReadOnly: tem.isReadOnly
        });
        isDebug &&
          console.log(
            "getData, UI에 표시된 데이터, 헤더",
            this.state.selectedNodeData,
            this.getHeader()
          );
      }
    } catch (e) {
      //isDebug && console.log("getData", e);
    } finally {
      this.setState({ isLoading: false });
    }
  };

  saveDataToZp = () => {
    const {
      fieldCount,
      selectedNodeHdr,
      selectedNodeData,
      delimit
    } = this.state;

    let re = makeSaveData(
      fieldCount,
      delimit,
      selectedNodeHdr,
      selectedNodeData
    );
    isDebug && console.log("re ======== > ", selectedNodeHdr);
    isDebug && console.log("re ======== > ", selectedNodeData);
    isDebug && console.log("re ======== > ", re);

    // 저장할 데이터가 있다
    if (re.needToSave) {
      this.setState({ dataToSave: re.data });
      this.openConfirmPopup(
        enginOptConfirmPopup(
          enginOptPopupType.saveToZookeeper,
          this.closeConfirmPopup,
          this.saveChangedDataToZP
        )
      );
      // 저장할 데이터가 없다
    } else {
      this.openConfirmPopup(
        enginOptConfirmPopup(
          enginOptPopupType.noChangeddata,
          null,
          this.closeConfirmPopup
        )
      );
    }
  };

  saveChangedDataToZP = async () => {
    this.setState({ isLoading: true });

    try {
      let redata = await this.props.zkNodeMgmt.putNodeData({
        nodePath: this.state.selectedNodeObj.fullPath,
        data: this.state.dataToSave
      });

      isDebug && console.log("saveChangedDataToZP", redata);
      let tem = dataParser(this.state.dataToSave, this.state.selectedNodeObj);

      this.setState({
        selectedNodeData: tem.dataArr ? tem.dataArr : [],
        selectedNodeDataOrg: tem.dataArr
          ? this.deepCopyDataArray(tem.dataArr)
          : tem.dataArr,
        selectedNodeHdr: tem.header,
        selectedNodeHdrOrg: tem.header
          ? this.deepCopHeaderArray(tem.header)
          : tem.header,
        selectedNodeUserHdr: tem.userHeader,
        fieldCount: tem.fieldCount,
        delimit: tem.delimit,
        isReadOnly: tem.isReadOnly,
        dataToSave: null
      });

      this.openConfirmPopup(
        enginOptConfirmPopup(
          enginOptPopupType.doneSave,
          null,
          this.closeConfirmPopup
        )
      );
      isDebug && console.log("putData", redata);
    } catch (e) {
      isDebug && console.log("putData", e);
    } finally {
      this.setState({ isLoading: false });
    }
  };

  handleUpdateChangedData(data, cb = null) {
    this.setState({ selectedNodeData: data }, cb);
  }

  handleUpdateChangedHeader(data) {
    this.setState({ selectedNodeHdr: data });
  }

  /* one/two 버튼의 confirm 팝업 하나로 처리 */
  openConfirmPopup = resource => {
    isDebug && console.log("openConfirmPopup", resource);
    this.setState({
      confirmPopupResource: resource
    });
  };

  closeConfirmPopup = () => {
    isDebug && console.log("closeConfirmPopup");
    this.setState({
      confirmPopupResource: null
    });
  };

  noDataRender = () => {
    return (
      <table className="table table--dark table--info">
        <tbody>
          <tr>
            <td style={{ textAlign: "center", border: "0px", padding: "70px" }}>
              <FormattedMessage id="no.result">{text => text}</FormattedMessage>
              <p />
              <FormattedMessage id="select.zookeeper.node">
                {text => text}
              </FormattedMessage>
            </td>
          </tr>
        </tbody>
      </table>
    );
  };

  // 노드 트리에서 노드명으로 검색 가능하도록 구현 추가
  onChangeKeyWord = e => {
    // 검색 키워드 입력
    e.preventDefault();

    this.setSearchKeyWord(e);
  };

  setSearchKeyWord = e => {
    // e가 null이면, reset이 됨
    this.setState({
      searchKeyWord: e && e.target ? e.target.value : ""
    });
  };

  // 검색어 초기화 시에 새로 데이터를 가져오는 것은 구현이 쉽다
  // 지금 아래 구현한 것은 가져온 데이터를 최대한 활용하는 방식이다.
  // 만약 새로 받아오는 방식으로 변경하고자 하면, getInitData() 호출하고 nodeListOrg 사용하지 않으면 된다
  //   1. 노드 트리 복사본을 만들고, 이를 조작하다.
  //   2. 1번은 오리지널 복사본에 영향을 주면 안된다(딥복사 사용)
  //   3. 되돌리기 버튼이나 검색어가 없는 경우는 기존에 받아온 오리지널 데이터로 복구한다.
  searchNodeName = (nodeArray, keyword) => {
    nodeArray.map((node, i) => {
      //isDebug && console.log("searchNodeName 001 ::",  keyword, node.nodePath, node.subNode, nodeArray);
      if (node.subNode && node.subNode.length > 0) {
        this.searchNodeName(node.subNode, keyword);
      }

      // 검색어와 연관이 없는 노드는 리스트에서 제거
      let nodeName = node.nodePath.toLowerCase();
      if (nodeName.indexOf(keyword) === -1) {
        //nodeArray.splice(i, 1); // 바로 삭제했을 때 map index가 밀림 > 마킹만 함
        node.isDelete = true;
        //isDebug && console.log(" >> searchNodeName 002 :: 삭제, ", nodeName, nodeArray);
      } else {
        node.isDelete = false;
      }
    });
  };

  // tree 형태 하위의 모든 객체까지 deep copy
  deepCopyNodeTree = (array, copiedTree) => {
    array.map((item, i) => {
      let tempObj = { ...item }; // 객체를 딥 복사해서
      copiedTree.push(tempObj);

      if (item.subNode && item.subNode.length > 0) {
        tempObj.subNode = [];
        this.deepCopyNodeTree(item.subNode, tempObj.subNode);
      }
    });
  };

  getTreeWithoutIsDeleteNode = array => {
    let filterded = array.filter(node => {
      if (node.subNode && node.subNode.length > 0) {
        node.subNode = this.getTreeWithoutIsDeleteNode(node.subNode);
      }

      // 서브가 있거나, 자신도 검색어에 맞는 node는 남긴다.
      if ((node.subNode && node.subNode.length > 0) || !node.isDelete) {
        return true;
        // 서브도 없고 "그리고" 자신도 검색어가 맞지 않으면 삭제한다.
      } else {
        return false;
      }
    });

    return filterded;
  };

  // 키워드 검색
  onClickSearchBtn = () => {
    // 원본 유지를 위해서 복사.
    let copyNodeTree = [];
    this.deepCopyNodeTree(this.state.nodeListOrg, copyNodeTree);
    isDebug &&
      console.log("onClickSearchBtn 001", this.state.nodeListOrg, copyNodeTree);

    // 키워드에 맞춰 트리 다시 구성
    let searchedNodes;
    let keyword = this.state.searchKeyWord
      ? this.state.searchKeyWord.trim()
      : "";
    if (keyword.length > 0) {
      this.searchNodeName(copyNodeTree, keyword.toLowerCase());
      searchedNodes = this.getTreeWithoutIsDeleteNode(copyNodeTree);
    } else {
      searchedNodes = this.state.nodeListOrg; // 키워드 없이 검색 누른 경우 원복
    }

    isDebug &&
      console.log(
        "onClickSearchBtn 002",
        this.state.nodeListOrg,
        keyword,
        searchedNodes
      );

    this.setState({
      nodeList: searchedNodes
    });
  };

  onClickResetBtn = () => {
    // 키워드 리셋하고 전체 가져오기
    this.setSearchKeyWord(null);

    this.setState({
      nodeList: this.state.nodeListOrg
    });
  };

  onKeyDown = e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.onClickSearchBtn();
    }
  };

  // 헤더는 사용자 정의 헤더, 파일(주피터에 저장된)의 헤더가 있을 수 있다
  // 사용자 정의 헤더가 우선 적용된다
  getHeader = () => {
    const { selectedNodeHdr, selectedNodeUserHdr } = this.state;

    let header;
    if (selectedNodeUserHdr != null && selectedNodeUserHdr.length > 0) {
      header = selectedNodeUserHdr;
    } else if (selectedNodeHdr != null && selectedNodeHdr.length > 0) {
      header = selectedNodeHdr;
    } else {
      return null;
    }
    return header;
  };

  dataRender = () => {
    const {
      selectedNodeData,
      selectedNodeDataOrg,
      fieldCount,
      selectedNodeUserHdr,
      isReadOnly
    } = this.state;

    return (
      <div style={{ paddingTop: "30px" }}>
        <div className="component__title">
          <span>
            <FormattedMessage id="node.info"> {text => text} </FormattedMessage>
          </span>
          <div className="btns">
            {!isReadOnly && (
              <button
                className="btn btn--blue"
                onClick={() => this.saveDataToZp()}
              >
                <FormattedMessage id="zookeeper.apply">
                  {text => text}
                </FormattedMessage>
              </button>
            )}
          </div>
        </div>
        <div className="table-wrapper">
          <NodeInfo
            header={this.getHeader()}
            headerOrg={this.state.selectedNodeHdrOrg}
            // isReadOnly와 사용자 헤더가 있는 경우 : 변경 불가
            isReadOnly={
              isReadOnly ||
              (selectedNodeUserHdr != null && selectedNodeUserHdr.length > 0)
            }
            handleUpdateChangedHeader={this.handleUpdateChangedHeader.bind(
              this
            )}
          />
        </div>

        <FieldGrid
          nodeData={selectedNodeData}
          nodeDataOrg={selectedNodeDataOrg}
          fieldCount={fieldCount}
          isReadOnly={isReadOnly}
          header={this.getHeader()}
          openConfirmPopup={this.openConfirmPopup}
          closeConfirmPopup={this.closeConfirmPopup}
          handleUpdateChangedData={this.handleUpdateChangedData.bind(this)}
        />
      </div>
    );
  };

  render() {
    const {
      nodeList,
      selectedNodeObj,
      selectedNodePath,
      selectedNodeData,
      confirmPopupResource
    } = this.state;
    const { zkNodeMgmt } = this.props;

    const getUserHeader = () => {
      if (selectedNodeObj != null && selectedNodeObj !== undefined) {
        if (
          selectedNodeObj.userDefineHdr !== "" &&
          selectedNodeObj.userDefineHdr != null
        ) {
          return selectedNodeObj.userDefineHdr;
        }
      }
      return <IntlMessages id="no.info.set" />;
    };

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper enginOpt-mgmt">
          <LeftMenu />

          <div className="component">
            <div className="section__title">
              <FormattedMessage id="engine.option">
                {text => text}
              </FormattedMessage>
            </div>

            <div className="component-content">
              {/* 트리 */}
              <div className="component enginOpt-tree">
                <div className="search-bar">
                  <FormattedMessage id="node.name">
                    {placeholder => (
                      <input
                        type="text"
                        className="form-control"
                        placeholder={placeholder}
                        style={{ width: "230px", marginRight: "5px" }}
                        value={this.state.searchKeyWord}
                        onKeyDown={this.onKeyDown}
                        onChange={this.onChangeKeyWord}
                      />
                    )}
                  </FormattedMessage>
                  <button
                    className="btn btn-icon small btn--go"
                    onClick={() => this.onClickSearchBtn()}
                  />
                  <button
                    className="btn btn-icon small btn--enginOpt"
                    onClick={() => this.onClickResetBtn()}
                  />
                </div>
                {/* 정보 */}
                <div className="enginOpt-tree__wrap">
                  {nodeList != null && (
                    <NodeTree
                      nodeTree={nodeList}
                      getChildNodeData={this.getChildNodeData}
                      getData={this.getData}
                      zkNodeMgmt={zkNodeMgmt}
                    />
                  )}
                </div>
              </div>

              <div className="enginOpt-result">
                <div className="table-wrapper">
                  <table className="table table--dark table--info security-event">
                    <colgroup>
                      <col width="150px" />
                      <col />
                    </colgroup>
                    <tbody>
                      <tr>
                        <th>
                          <FormattedMessage id="node.path">
                            {text => text}
                          </FormattedMessage>
                        </th>
                        <td>{selectedNodePath}</td>
                      </tr>
                      <tr>
                        <th>
                          <FormattedMessage id="custom.header">
                            {text => text}
                          </FormattedMessage>
                        </th>
                        <td>{getUserHeader()}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                {selectedNodeData == null
                  ? this.noDataRender()
                  : this.dataRender()}
              </div>
            </div>
          </div>

          {/*
          {isShowOptionSettingPopup && (
            <OptionSettingPopup closeAllPopup={this.closeAllPopup} />
          )}
          {isShowOptionSettingPopup && (
            <UpdateEngineOptionPopup closeAllPopup={this.closeAllPopup} />
          )}
          */}

          {confirmPopupResource != null && (
            <ConfirmPopup resource={confirmPopupResource} />
          )}

          {/* 로딩 컴포넌트 */}
          {this.state.isLoading && <Loading />}
        </div>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    //nodeList: state.zpNodeMgmt.get("nodeList"),
    //baseNodePath: state.zpNodeMgmt.get("baseNodePath"),
    //selectedNodePath: state.zpNodeMgmt.get("selectedNodePath")
    isSuccess: state.zpNodeMgmt.get("isSuccess")
  }),
  dispatch => ({
    zkNodeMgmt: bindActionCreators(zkNodeMgmtActions, dispatch)
  })
)(EngineOptionComponent);
