import React, { Component } from "react";
import GridTable from "./../Common/GridTable";
import FilterTree from "./../Common/FilterTree";
import * as ruleActions from "../../store/modules/rule";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import AlertPopup from "./popups/AlertPopup";
import ChgReasonPopup from "./popups/ChgReasonPopup";
import IntlMessages from "util/IntlMessages";

class RuleHistory extends Component {
  state = {
    selected: 0,
    rollbackData: {
      ruleHistSeq: "",
      ruleSeq: ""
    },
    rollbackPopup: false,
    alertPopup: false
  };

  /** 롤백 클릭 */
  onClickRollback = ({ ruleHistSeq, ruleSeq }, e) => {
    e.stopPropagation();
    this.setState(
      {
        rollbackData: { ruleHistSeq, ruleSeq }
      },
      () => this.openPopup("rollbackPopup")
    );
  };
  /** 롤백 */
  rollbackRule = async ruleChgReason => {
    const { RuleActions } = this.props;
    const { ruleHistSeq, ruleSeq } = this.state.rollbackData;
    try {
      await RuleActions.rollbackRule({ ruleHistSeq, ruleSeq, ruleChgReason });
      this.setState({ selected: 0, deletePopup: false }, () =>
        this.openPopup("alertPopup")
      );
    } catch (err) {
      console.warn(err);
    }
  };

  /** 요일 체크 */
  getWeekChecked = (week, day) => week.ruleCommitWeek.indexOf(day) !== -1;
  /** 시간 단위 콜론 처리 */
  setTimeColon = time =>
    time ? `${time.substr(0, 2)}:${time.substr(2, 2)}` : "";

  /** 룰 변경이력 조회 */
  getRuleHistory = ({ ruleHistSeq }) => {
    const { RuleActions } = this.props;
    try {
      RuleActions.getRuleHistory(ruleHistSeq);
    } catch (err) {
      console.warn(err);
    }
  };

  /** 팝업제어 */
  closePopup = popupName => this.setState({ [popupName]: false });
  openPopup = popupName => this.setState({ [popupName]: true });

  /** props setting */
  static propTypes = {
    isShow: PropTypes.bool,
    codes: PropTypes.objectOf(PropTypes.array),
    onCancel: PropTypes.func,
    ruleHist: PropTypes.object,
    ruleHists: PropTypes.arrayOf(PropTypes.object)
  };
  static defaultProps = {
    isShow: false,
    codes: {
      week: [],
      ruleType: [],
      thrsldOprtr: []
    },
    onCancel: () => console.log("RuleInfo :: onCnacel is not defined"),
    ruleHist: {
      ruleChgHistSeq: "",
      ruleSeq: "",
      ruleNm: "",
      ruleType: "",
      ruleCommitStrDt: "",
      ruleCommitEndDt: "",
      ruleCommitStrTime: "",
      ruleCommitEndTime: "",
      ruleCommitWeek: "",
      thrsldGroupField: "",
      thrsldGroupPeriod: "",
      thrsldGroupCnt: "",
      thrsldDstnctField: "",
      thrsldDstnctCnt: "",
      thrsldOprtrPeriod: "",
      thrsldOprtr: "",
      detectCndtn: "",
      droolsCode: "",
      relshpRule: "",
      cntrlTargt: "",
      custmNm: "",
      ruleDesc: "",
      commitYn: "",
      ruleChgReason: "",
      filters: []
    },
    ruleHists: []
  };

  render() {
    const {
      onClickRollback,
      getWeekChecked,
      getRuleHistory,
      closePopup,
      rollbackRule,
      setTimeColon
    } = this;
    const { rollbackPopup, alertPopup } = this.state;
    const { ruleHist, ruleHists, isShow, codes, onCancel } = this.props;

    const columns = [
      {
        Header: "No",
        accessor: "ruleHistSeq",
        width: 50,
        filterable: false,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="change.datetime" />,
        accessor: "regDateFormatted",
        sortable: true,
        width: 120,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="change.modifier" />,
        accessor: "regUsrNm",
        width: 70,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="reason.change" />,
        accessor: "ruleChgReason",
        width: 120
      },
      {
        Header: " Actions",
        accessor: "action1",
        width: 50,
        style: { textAlign: "center" },
        Cell: row => {
          return row.original.isLatest === "N" ? (
            <button
              type="button"
              className="btn"
              style={{ height: "25px", width: "40px", padding: "0" }}
              onClick={e => {
                onClickRollback(row.original, e);
              }}
            >
              <i className="fas fa-redo"></i>
            </button>
          ) : (
            ""
          );
        }
      }
    ];

    return (
      <div
        className="component filter-detail"
        style={{ display: isShow ? "block" : "none" }}
      >
        <div className="top">
          <div className="history">
            <GridTable
              columns={columns}
              data={ruleHists}
              filterable={false}
              sortable={false}
              resizable={false}
              className="-highlight"
              defaultSorted={[{ id: "regDateFormatted", desc: true }]}
              getTrProps={(state, rowInfo, column) => {
                if (rowInfo) {
                  return {
                    onClick: (e, handleOriginal) => {
                      this.setState({
                        selected: rowInfo.index
                      });
                      getRuleHistory(rowInfo.original);
                      if (handleOriginal) {
                        handleOriginal();
                      }
                    },
                    style: {
                      backgroundColor:
                        rowInfo.index === this.state.selected
                          ? "#282a35"
                          : "#2f323c",
                      cursor: "pointer"
                    }
                  };
                } else {
                  return {
                    style: {
                      cursor: "pointer"
                    }
                  };
                }
              }}
            />
          </div>

          <div className="left" style={{ width: "480px" }}>
            <div className="filter-info">
              <div className="component__title">
                <span className="filter-title">
                  <IntlMessages id="basic.info" />
                </span>
              </div>
              <div
                className="table-wrapper"
                style={{ maxHeight: 560, overflowY: "auto" }}
              >
                <table className="table table--dark table--info security-event">
                  <colgroup>
                    <col width="130px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <IntlMessages id="rule.name" />
                      </th>
                      <td colSpan="3">{ruleHist.ruleNm}</td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="rule.type" />
                      </th>
                      <td>{ruleHist.ruleTypeString}</td>
                      <th>
                        <IntlMessages id="apply.yn" />
                      </th>
                      <td>
                        <div className="checkbox checkbox--dark">
                          <label>
                            <input
                              type="checkbox"
                              name="commitYn"
                              checked={ruleHist.commitYn === "Y" ? true : false}
                              disabled={true}
                            />
                            <div className="icon" />
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="period" />
                      </th>
                      <td colSpan="3">
                        {(ruleHist.ruleCommitStrDt ||
                          ruleHist.ruleCommitEndDt) &&
                          `${ruleHist.ruleCommitStrDt ||
                            ""}~${ruleHist.ruleCommitEndDt || ""}`}
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="day.of.the.week" />
                      </th>
                      <td colSpan="3" style={{ paddingRight: 0 }}>
                        <div className="checkbox__wrap">
                          {codes.week.map((day, index) => {
                            return (
                              <div
                                className="checkbox checkbox--dark"
                                key={index}
                              >
                                <label>
                                  <input
                                    type="checkbox"
                                    checked={getWeekChecked(ruleHist, day.id)}
                                    disabled={true}
                                  />
                                  <div className="icon" />
                                  <span>{day.name}</span>
                                </label>
                              </div>
                            );
                          })}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="time" />
                      </th>
                      <td>
                        {(ruleHist.ruleCommitStrTime ||
                          ruleHist.ruleCommitEndTime) &&
                          `${setTimeColon(
                            ruleHist.ruleCommitStrTime
                          )}~${setTimeColon(ruleHist.ruleCommitEndTime)}`}
                      </td>
                      <th>
                        <IntlMessages id="rule.group.code" />
                      </th>
                      <td>{ruleHist.ruleGroupCodeString}</td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="refer.rule" />
                      </th>
                      <td>{ruleHist.relshpRule}</td>
                      <th>Attack Code</th>
                      <td>{ruleHist.attackCodeString}</td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="control.target" />
                      </th>
                      <td>{ruleHist.cntrlTargt}</td>
                      <th>Severity Code</th>
                      <td>{ruleHist.severityCodeString}</td>
                    </tr>
                    <tr>
                      <th>Company</th>
                      <td>{ruleHist.custmNm}</td>
                      <th>
                        <IntlMessages id="report.autoyn" />
                      </th>
                      <td>{ruleHist.autoReportYn}</td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="desc" />
                      </th>
                      <td colSpan="3">{ruleHist.ruleDesc}</td>
                    </tr>
                    <tr>
                      <th>Comment</th>
                      <td colSpan="3">{ruleHist.comment}</td>
                    </tr>
                    <tr>
                      <th>Take Action Description</th>
                      <td colSpan="3">{ruleHist.takeActionDescription}</td>
                    </tr>
                    <tr>
                      <th>Take B Action Description</th>
                      <td colSpan="3">{ruleHist.takeBActionDescription}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="change-reason" style={{ marginTop: "20px" }}>
              <div className="component__title">
                <span>
                  <IntlMessages id="reason.change" />
                </span>
              </div>
              <div
                className="form-control change-reason-form"
                style={{ height: "200px", color: "white" }}
              >
                {ruleHist.ruleChgReason}
              </div>
            </div>
          </div>

          <div className="right">
            <div className="filter-condition" style={{ height: "55%" }}>
              {ruleHist.ruleType !== "1100" && (
                <FilterTree
                  disabled={true}
                  data={ruleHist.filters}
                  btnVisibled={false}
                />
              )}
              {ruleHist.ruleType === "1100" && (
                <React.Fragment>
                  <span className="icon-and" />
                  <div className="component__title">
                    <span>
                      <IntlMessages id="drools.editor" />
                    </span>
                  </div>
                  <div
                    className="component__box"
                    style={{ height: "400px", padding: "15px" }}
                  >
                    <pre
                      className="form-control change-reason-form"
                      style={{
                        height: "400px",
                        margin: "0",
                        overflowY: "auto",
                        color: "white"
                      }}
                    >
                      {ruleHist.droolsCode}
                    </pre>
                  </div>
                </React.Fragment>
              )}
            </div>
            {ruleHist.ruleType === "0100" && (
              <div>
                <div className="component__title">
                  <span>
                    <IntlMessages id="threshold.info" />
                  </span>
                </div>
                <table className="table table--dark table--info security-event">
                  <tbody>
                    <tr>
                      <th rowSpan="2">Group by Fields</th>
                      <td colSpan="4">{ruleHist.thrsldGroupField}</td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="duration" />
                      </th>
                      <td>{ruleHist.thrsldGroupPeriod}</td>
                      <th>Count</th>
                      <td>{ruleHist.thrsldGroupCnt}</td>
                    </tr>
                    <tr>
                      <th rowSpan="2">Distinct Fields</th>
                      <td colSpan="4">{ruleHist.thrsldDstnctField}</td>
                    </tr>
                    <tr>
                      <th>Count</th>
                      <td colSpan="3">{ruleHist.thrsldDstnctCnt}</td>
                    </tr>
                    <tr>
                      <th>Aggregation</th>
                      <th>
                        <IntlMessages id="duration" />
                      </th>
                      <td>{ruleHist.thrsldOprtrPeriod}</td>
                      <th>Operation</th>
                      <td style={{ overflow: "unset" }}>
                        {ruleHist.thrsldOprtrString}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            )}
          </div>
        </div>
        <div className="bottom">
          <div className="btns">
            <button className="btn btn--dark" onClick={onCancel}>
              <IntlMessages id="cancel" />
            </button>
          </div>
        </div>
        {rollbackPopup && (
          <ChgReasonPopup
            onConfirm={rollbackRule}
            message={<IntlMessages id="back.select.hist" />}
            onClose={() => closePopup("rollbackPopup")}
          />
        )}
        {alertPopup && (
          <AlertPopup
            message={<IntlMessages id="is.roll.back" />}
            onConfirm={onCancel}
            onClose={() => closePopup("alertPopup")}
          />
        )}
      </div>
    );
  }
}

export default connect(
  state => ({
    ruleHist: state.rule.get("ruleHist")
  }),
  dispatch => ({
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(RuleHistory);
