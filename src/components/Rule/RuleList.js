import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import IntlMessages from "util/IntlMessages";
import { FormattedMessage } from "react-intl";

import PropTypes from "prop-types";

import RuleGrid from "./RuleGrid";
import ThreatTaxoTree from "../Common/ThreatTaxoTree";

import * as ruleActions from "../../store/modules/rule";

import ChgReasonPopup from "./popups/ChgReasonPopup";
import TaxoSelectPopup from "./popups/TaxoSelectPopup";
import AlertPopup from "./popups/AlertPopup";

class RuleList extends Component {
  state = {
    keyword: "",
    selectedKey: "0",
    selectedRules: [],

    deletePopup: false,
    deleteBinPopup: false,
    revertPopup: false,
    alertPopup: false,
    taxoPopup: false,
    ruleDistPopup: false,
    distAlertPopup: false
  };

  /** 위협체계 트리 선택 콜백 */
  setSelectedTaxo = (selectedKeys, e) => {
    let selectedKey = "";
    if (selectedKeys.length > 0) {
      selectedKey = selectedKeys[0].split("-");
      selectedKey = selectedKey.length > 1 ? selectedKey[1] : selectedKey[0];
    } else {
      selectedKey = "0";
    }
    this.setState({ selectedKey }, () => this.getRules());
  };

  /** 선택된 룰 정보를 받아온다 */
  setSelectRules = selectedRules => {
    this.setState({ selectedRules });
  };

  /** 필터 */
  onChangeInput = e => this.setState({ [e.target.name]: e.target.value });
  /** 필터 enter 조회 */
  onKeydownInput = e => {
    if (e.keyCode === 13) {
      this.onClickSearch();
    }
  };

  /** 룰 조회 */
  getRules = () => {
    const { RuleActions } = this.props;
    const { selectedKey, keyword } = this.state;
    try {
      if (selectedKey === "0") {
        RuleActions.getRules({ keyword });
      } else {
        RuleActions.getRulesByThreatClasSeq({ selectedKey, keyword });
      }
    } catch (err) {
      console.warn(err);
    }
  };

  /** 조회버튼 클릭 */
  onClickSearch = () => this.getRules();
  /** 필터 리셋버튼 클릭 */
  onClickReset = () => this.setState({ keyword: "" }, () => this.getRules());

  /** 룰 삭제 클릭(휴지통이동) */
  onClickDelete = rule => {
    this.setState(
      {
        selectedRules: [rule]
      },
      () => this.openPopup("deletePopup")
    );
  };
  /** 선택삭제 클릭(휴지통이동) */
  onClickDeleteSelected = () => {
    const { selectedRules } = this.state;
    if (Array.isArray(selectedRules) && selectedRules.length > 0) {
      this.openPopup("deletePopup");
    } else {
      this.showAlert();
    }
  };
  /** 룰 삭제(휴지통이동) */
  deleteRule = async ruleChgReason => {
    const { RuleActions } = this.props;
    const data = { ruleChgReason, rules: this.state.selectedRules };
    try {
      await RuleActions.deleteRule(data);
      this.getRules();
      this.closePopup("deletePopup");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 휴지통복구 클릭 */
  onClickRevert = ({ ruleSeq }) => {
    this.setState(
      {
        selectedRules: ruleSeq
      },
      () => this.openPopup("revertPopup")
    );
  };
  /** 룰 복원(휴지통->미분류) */
  revertRule = async ruleChgReason => {
    const { RuleActions } = this.props;
    const rule = { ruleChgReason, ruleSeq: this.state.selectedRules };

    try {
      await RuleActions.revertRule(rule);
      this.getRules();
      this.closePopup("revertPopup");
    } catch (error) {
      console.warn(error);
    }
  };

  /** 영구삭제 클릭(set use_yn = 'N') */
  onClickDeleteFromBin = rule => {
    this.setState(
      {
        selectedRules: [rule]
      },
      () => this.openPopup("deleteBinPopup")
    );
  };
  /** 영구삭제 선택 클릭 */
  onClickDeleteFromBinSelected = () => {
    const { selectedRules } = this.state;
    if (Array.isArray(selectedRules) && selectedRules.length > 0) {
      this.openPopup("deleteBinPopup");
    } else {
      this.showAlert();
    }
  };
  /** 영구삭제 */
  deleteRuleFromBin = async ruleChgReason => {
    const { RuleActions } = this.props;
    const rules = { ruleChgReason, rules: this.state.selectedRules };
    try {
      await RuleActions.deleteRuleFromBin(rules);
      this.getRules();
      this.closePopup("deleteBinPopup");
    } catch (error) {
      console.warn(error);
    }
  };

  /** 룰 분류체계 등록 */
  getCheckedTaxo = async (checkedKeys, e) => {
    const threatClasSeqs = checkedKeys.checked.map(key => key.split("-")[1]);
    const rules = this.state.selectedRules;

    try {
      const { RuleActions } = this.props;
      await RuleActions.setThreatTaxo({ rules, threatClasSeqs });
      await this.getRules();
      this.closePopup("taxoPopup");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 룰 수정(화면전환) */
  editRule = ruleSeq => this.props.onMoveDetail(ruleSeq);

  /** 신규 룰 등록(화면전환) */
  onClickInsert = () => this.props.onDetail();

  /** 분류체계 등록 버튼 */
  onClickTaxo = () => {
    if (this.state.selectedRules.length > 0) {
      this.openPopup("taxoPopup");
    } else {
      this.showAlert();
    }
  };

  /** 룰 배포 */
  distRules = async dstrbtCont => {
    const { RuleActions } = this.props;
    try {
      await RuleActions.distRules({ dstrbtCont });
      this.closePopup("ruleDistPopup");
      this.openPopup("distAlertPopup");
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    this.getRules();
  }

  /** show alert popup */
  showAlert = () => this.openPopup("alertPopup");
  /** 팝업 제어 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** props setting */
  static propTypes = {
    onDetail: PropTypes.func
  };
  static defaultProps = {
    onDetail: () => console.log("RuleList :: onDetail is not defined")
  };

  render() {
    const {
      onChangeInput,
      onKeydownInput,
      closePopup,
      onClickDeleteFromBin,
      onClickSearch,
      onClickReset,
      onClickDeleteFromBinSelected,
      onClickInsert,
      onClickDelete,
      onClickRevert,
      onClickDeleteSelected,
      setSelectRules,
      deleteRuleFromBin,
      deleteRule,
      revertRule,
      distRules,
      getCheckedTaxo,
      onClickTaxo,
      setSelectedTaxo
    } = this;
    const { rules, onDetail } = this.props;
    const {
      keyword,
      selectedKey,
      deletePopup,
      taxoPopup,
      alertPopup,
      revertPopup,
      deleteBinPopup,
      ruleDistPopup,
      distAlertPopup
    } = this.state;

    return (
      <div className="component">
        <div className="section__title">
          <IntlMessages id="rule.manage" />
        </div>
        <div className="component-content">
          <div className="component filter-tree">
            <div className="filter-tree__wrap">
              <ThreatTaxoTree onSelect={setSelectedTaxo} />
            </div>
          </div>
          <div className="filter-result">
            <div className="component__title">
              <div className="search-bar">
                <FormattedMessage id="rule.name.or.rule.type">
                  {placeholder => (
                    <input
                      type="text"
                      className="form-control"
                      name="keyword"
                      placeholder={placeholder}
                      value={keyword}
                      onChange={onChangeInput}
                      onKeyDown={onKeydownInput}
                    />
                  )}
                </FormattedMessage>
                <div className="binder" />
                <button
                  className="btn btn-icon small btn--go"
                  onClick={onClickSearch}
                />
                <button
                  className="btn btn-icon small btn--filter"
                  onClick={onClickReset}
                />
              </div>
              <div className="btns">
                <button
                  className="btn btn--dark"
                  onClick={() => this.openPopup("ruleDistPopup")}
                >
                  <IntlMessages id="rule.deploy" />
                </button>
                {this.state.selectedKey !== "2" && (
                  <button
                    className="btn btn--dark"
                    onClick={onClickTaxo}
                  >
                    <IntlMessages id="class.system.create" />
                  </button>
                )}
                <button
                  className="btn btn--dark"
                  onClick={
                    this.state.selectedKey !== "2"
                      ? onClickDeleteSelected
                      : onClickDeleteFromBinSelected
                  }
                >
                  <IntlMessages id="multi.delete" />
                </button>
                <button className="btn btn--blue" onClick={onClickInsert}>
                  <IntlMessages id="new.reg" />
                </button>
              </div>
            </div>
            <RuleGrid
              data={rules}
              onSelect={setSelectRules}
              onDelete={onClickDelete}
              onDeleteFromBin={onClickDeleteFromBin}
              onRevert={onClickRevert}
              onDetail={onDetail}
              selectedKey={selectedKey}
              //reloadData={getRules}
            />
          </div>
        </div>

        {deletePopup && (
          <ChgReasonPopup
            onConfirm={deleteRule}
            message={<FormattedMessage id="move.rule.bin" />}
            onClose={() => closePopup("deletePopup")}
          />
        )}
        {revertPopup && (
          <ChgReasonPopup
            onConfirm={revertRule}
            message={<FormattedMessage id="recover.rule.bin" />}
            onClose={() => closePopup("revertPopup")}
          />
        )}
        {deleteBinPopup && (
          <ChgReasonPopup
            onConfirm={deleteRuleFromBin}
            message={<FormattedMessage id="delete.rule.bin" />}
            onClose={() => closePopup("deleteBinPopup")}
          />
        )}
        {alertPopup && (
          <AlertPopup
            message={<FormattedMessage id="no.rule" />}
            onConfirm={() => closePopup("alertPopup")}
            onClose={() => closePopup("alertPopup")}
          />
        )}
        {taxoPopup && (
          <TaxoSelectPopup
            onConfirm={getCheckedTaxo}
            onClose={() => closePopup("taxoPopup")}
          />
        )}
        {ruleDistPopup && (
          <ChgReasonPopup
            onConfirm={distRules}
            message={<FormattedMessage id="deploy.all.rule" />}
            textareaTitle={<IntlMessages id="desc" />}
            onClose={() => closePopup("ruleDistPopup")}
          />
        )}
        {distAlertPopup && (
          <AlertPopup
            message={<FormattedMessage id="complete.deploy" />}
            onConfirm={() => closePopup("distAlertPopup")}
            onClose={() => closePopup("distAlertPopup")}
          />
        )}
      </div>
    );
  }
}

export default connect(
  state => ({
    rules: state.rule.get("rules")
  }),
  dispatch => ({
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(RuleList);
