import React, { Component } from "react";
import IntlMessages from "util/IntlMessages";

import { getTaxo } from "./../../store/api/threattaxonomy";
import * as util from "./../../utils";

const TAXO_COUNT = 4;

export default class TaxonomyTable extends Component {
  state = {
    taxo: []
  };
  getTaxonomyInfo = async ruleSeq => {
    if (!util.isValid(ruleSeq)) {
      return;
    }
    const res = await getTaxo(ruleSeq);
    this.setState({ taxo: res.data });
  };

  componentDidMount() {
    this.getTaxonomyInfo(this.props.ruleSeq);
  }

  render() {
    return (
      <div
        style={{
          height: "250px"
          // , overflowY: "auto"
        }}
      >
        <table className="table table--dark table--info security-event">
          <thead>
            <tr>
              <th>
                <IntlMessages id="stage" />
              </th>
              <th>
                <IntlMessages id="tactical" />
              </th>
              <th>
                <IntlMessages id="tech" />
              </th>
              <th>
                <IntlMessages id="step" />
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.taxo.map((item, index) => {
              const taxoArr = item.thrtTpPath
                ? item.thrtTpPath.split(" > ")
                : [];
              taxoArr.push(item.thrtNm);
              if (taxoArr.length < TAXO_COUNT) {
                for (let i = taxoArr.length; i < TAXO_COUNT; i++) {
                  taxoArr.push("");
                }
              }
              return (
                <tr key={index}>
                  {taxoArr.map((taxoNm, idx) => (
                    <td key={idx}>{taxoNm}</td>
                  ))}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
