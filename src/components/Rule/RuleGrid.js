import React, { Component } from "react";
import GridTable from "./../Common/GridTable";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import equal from "fast-deep-equal";
import ReactTooltip from "react-tooltip";
import PropTypes from "prop-types";
import IntlMessages from "util/IntlMessages";
import * as ruleActions from "./../../store/modules/rule";

import { exportObjectToJson } from "./../FilterMgmt/const/jsonUtils";

import "./style.scss";

class RuleGrid extends Component {
  state = {
    rules: [],
    hdrChkbox: false
  };

  /** 익스포트 클릭 */
  onClickExport = async rule => {
    const { RuleActions } = this.props;
    try {
      const res = await RuleActions.getRule(rule.ruleSeq);
      exportObjectToJson(res.data);
    } catch (err) {
      console.warn(err);
    }
  };

  /** 개별삭제(휴지통) */
  onClickDelete = rule => this.props.onDelete(rule);
  /** 수정 */
  onClickUpdate = rule => this.props.onDetail(rule);
  /** 휴지통룰 복구(휴지통 -> 미분류) */
  onClickRevert = rule => this.props.onRevert(rule);
  /** 휴지통룰 삭제(useyn -> n) */
  onClickDeleteFromBin = rule => this.props.onDeleteFromBin(rule);

  /** 헤더 체크박스  */
  onChangeHdrchkbox = e => this.setCheckbox(e.target.checked);

  /** 전체 체크박스 설정(헤더제외) */
  setCheckbox = checked => {
    const data = this.state.rules.map(rule => {
      if (rule["threatClasTypeCode"] !== "D") {
        rule["checked"] = checked;
      } else {
        if (this.props.selectedKey === "2") {
          rule["checked"] = checked;
        }
      }
      return rule;
    });
    this.setState({ hdrChkbox: checked, data });
    this.onSelectCallbackHandler(data);
  };

  /** 체크박스 */
  onChangeChkbox = (ruleIndex, e) => {
    let hdrChkbox = this.state.hdrChkbox;
    const data = this.state.rules.map((rule, index) => {
      if (ruleIndex === index) {
        rule["checked"] = e.target.checked;
      }
      //하나라도 체크가 풀릴경우 헤더체크박스도 체크가 풀린다
      hdrChkbox = hdrChkbox && rule["checked"];
      return rule;
    });
    this.setState({ data, hdrChkbox });
    this.onSelectCallbackHandler(data);
  };

  //헤더 체크상태가 변할때마다 체크된 rule 정보를 반환한다
  onSelectCallbackHandler = data => {
    this.props.onSelect(data.filter(rule => rule["checked"]));
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!equal(nextProps.rules, this.state.rules)) {
      this.setState({ rules: nextProps.rules, hdrChkbox: false }); //rerendering시 헤더체크 해제
    }
  }

  getDisabled(code) {
    if (code === "D") {
      if (this.props.selectedKey === "2") {
        //휴지통일 경우
        return false;
      } else {
        return true;
      }
    }
  }

  /** props setting */
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
    onSelect: PropTypes.func,
    onDelete: PropTypes.func,
    onDleeteFromBin: PropTypes.func,
    onRever: PropTypes.func,
    selectedKey: PropTypes.string
  };
  static defaultProps = {
    data: [],
    onSelect: () => console.log("RuleGrid :: onSelect is not defined"),
    onDelete: () => console.log("RuleGrid :: onDelete is not defined"),
    onDeleteFromBin: () =>
      console.log("RuleGrid :: onDeleteFromBin is not defined"),
    onRevert: () => console.log("RuleGrid :: onRevert is not defined"),
    selectedKey: "0"
  };

  render() {
    const {
      onClickExport,
      onClickDelete,
      onClickUpdate,
      onChangeHdrchkbox,
      onChangeChkbox,
      onClickRevert,
      onClickDeleteFromBin
    } = this;
    const { rules, hdrChkbox } = this.state;

    const columns = [
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={onChangeHdrchkbox}
                checked={hdrChkbox}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={e => onChangeChkbox(row.index, e)}
                  checked={row.original.checked || false}
                  disabled={this.getDisabled(row.original.threatClasTypeCode)}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        sortable: false,
        width: 60
      },
      {
        Header: "No.",
        accessor: "ruleSeq",
        width: 60,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="rule.name" />,
        accessor: "ruleNm",
        width: 250,
        style: { textAlign: "center" },
        Cell: ({ value, original }) => {
          const tooltipId = `ruleNmTooltip_${original.ruleSeq}`;
          return (
            <React.Fragment>
              <span
                className="link-style"
                data-tip
                data-for={tooltipId}
                onClick={() => {
                  if (original.threatClasTypeCode !== "D") {
                    this.props.onDetail(original, true); //조회일 경우 disabled
                  }
                }}
              >
                {value}
              </span>
              <ReactTooltip
                id={tooltipId}
                className="tooltipDesc"
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={200}
                event="mouseover"
                globalEventOff="mouseout"
              >
                {value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      },
      {
        Header: <IntlMessages id="rule.type" />,
        accessor: "ruleType",
        width: 70,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="apply.yn" />,
        accessor: "commitYn",
        width: 70,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="threat.class" />,
        accessor: "threatPath",
        width: 540,
        style: { textAlign: "left" }
      },
      {
        Header: <IntlMessages id="threat.class.type.code" />,
        accessor: "threatClasTypeCode",
        show: false
      },
      {
        Header: " Actions",
        accessor: "actions",
        width: 150,
        style: { textAlign: "center" },
        Cell: data => {
          const style = { height: "25px", width: "40px", padding: "0" };
          return (
            <React.Fragment>
              {data.original.threatClasTypeCode !== "D" && (
                <button
                  type="button"
                  className="btn btn-icon"
                  style={{ ...style }}
                  onClick={() => onClickExport(data.original)}
                >
                  <img src="/images/common/icon_export.png" alt="icon_export" />
                </button>
              )}
              {data.original.threatClasTypeCode !== "D" ? (
                <button
                  type="button"
                  className="btn btn-icon"
                  style={{ ...style }}
                  onClick={() => onClickDelete(data.original)}
                >
                  <img
                    id="ipDelete"
                    src="/images/common/icon_delete.png"
                    alt="icon_delete"
                  />
                </button>
              ) : (
                <button
                  type="button"
                  className="btn btn-icon"
                  style={{ ...style }}
                  onClick={() => onClickDeleteFromBin(data.original)}
                >
                  <img
                    id="ipDelete"
                    src="/images/common/icon_delete.png"
                    alt="icon_delete"
                  />
                </button>
              )}
              {data.original.threatClasTypeCode !== "D" ? (
                <button
                  type="button"
                  className="btn btn-icon"
                  style={{ ...style }}
                  onClick={() => onClickUpdate(data.original)}
                >
                  <img
                    id="ipUpdate"
                    src="/images/common/icon_edit.png"
                    alt="icon_edit"
                  />
                </button>
              ) : (
                <button
                  type="button"
                  className="btn"
                  style={{ ...style }}
                  onClick={e => onClickRevert(data.original)}
                >
                  <i className="fas fa-redo"></i>
                </button>
              )}
            </React.Fragment>
          );
        }
      }
    ];

    return <GridTable columns={columns} data={rules} resizable={false} />;
  }
}

export default connect(
  state => ({
    rules: state.rule.get("rules")
  }),
  dispatch => ({
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(RuleGrid);
