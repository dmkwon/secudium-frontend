import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import jwtService from "services/jwtService";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import Loading from "../Common/Loading";

import RuleList from "./RuleList";
import RuleDetail from "./RuleDetail";

import * as ruleActions from "../../store/modules/rule";
import { action as filterActions } from "../../store/modules/filterMgmtListAction";
import { getCodeList } from "./../../store/api/common";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class RuleMgmtComponent extends Component {
  state = {
    ruleList: true,
    ruleDetail: false,
    disabled: false,
    ruleData: {},
    codes: {
      ruleType: [],
      thrsldOprtr: [],
      week: []
    }
  };

  showRuleList = () => {
    this.setState({
      ruleList: true,
      ruleDetail: false,
      ruleData: null
    });
  };

  showRuleDetail = async (targetRule, disabled = false) => {
    const { RuleActions } = this.props;
    if (targetRule) {
      try {
        const rule = await RuleActions.getRule(targetRule.ruleSeq);
        const ruleHist = await RuleActions.getLatestRuleHistory(
          targetRule.ruleSeq
        );
        const ruleHists = await RuleActions.getRuleHistories(
          targetRule.ruleSeq
        );

        this.setState({
          ruleList: false,
          ruleDetail: true,
          ruleData: {
            rule: rule.data,
            ruleHist: ruleHist.data,
            ruleHists: ruleHists.data
          },
          disabled
        });
      } catch (err) {
        console.log(err);
      }
    } else {
      await RuleActions.initRuleState();
      this.setState({
        ruleList: false,
        ruleDetail: true,
        ruleData: {},
        disabled
      });
    }
  };

  /** code setting */
  getCodes = async () => {
    try {
      const ruleType = await getCodeList("RULE_TYPE");
      const thrsldOprtr = await getCodeList("THRSLD_OPRTR");
      const week = await getCodeList("WEEK");

      const severityCode = await getCodeList("SEVE");
      const ruleGroupCode = await getCodeList("RRGC");
      const attackCode = await getCodeList("ATTK");

      this.setState({
        codes: {
          ruleType: ruleType.data,
          thrsldOprtr: thrsldOprtr.data,
          week: week.data,
          severityCode: [
            { id: "", name: <IntlMessages id="select.options" /> },
            ...severityCode.data
          ],
          ruleGroupCode: [
            { id: "", name: <IntlMessages id="select.options" /> },
            ...ruleGroupCode.data
          ],
          attackCode: [
            { id: "", name: <IntlMessages id="select.options" /> },
            ...attackCode.data
          ]
        }
      });
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    jwtService.on("onChangeLang", () => {
      this.getCodes();
    });

    this.props.FilterActions.getInitData({}, {});
    this.getCodes();
  }

  render() {
    const { showRuleDetail, showRuleList } = this;
    const { ruleList, ruleDetail, disabled, codes, ruleData } = this.state;
    const { loading } = this.props;

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper rule-mgmt">
          <LeftMenu />
          {ruleList && <RuleList onDetail={showRuleDetail} />}
          {ruleDetail && (
            <RuleDetail
              onList={showRuleList}
              data={ruleData}
              disabled={disabled}
              codes={codes}
            />
          )}
          {loading && <Loading />}
        </div>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    loading: state.rule.get("loading")
  }),
  dispatch => ({
    RuleActions: bindActionCreators(ruleActions, dispatch),
    FilterActions: bindActionCreators(filterActions, dispatch)
  })
)(RuleMgmtComponent);
