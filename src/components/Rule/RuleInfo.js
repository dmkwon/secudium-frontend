import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import DatePicker from "react-datepicker";
import { Textbox, Select, Textarea } from "react-inputs-validation";
import IntlMessages from "util/IntlMessages";

import PropTypes from "prop-types";

import FilterTree from "./../Common/FilterTree";
import TaxonomyTable from "./TaxonomyTable";
import ThrsldFieldPopup from "./popups/ThrsldFieldPopup";
import AlertPopup from "./popups/AlertPopup";

import * as ruleActions from "../../store/modules/rule";
import { checkJsonFile } from "./../FilterMgmt/const/utils";
import * as util from "./../../utils";

class RuleInfo extends Component {
  state = {
    rule: this.props.rule,
    disabled: this.props.disabled,
    groupBy: [], //임계치 groupby 필드
    distinct: [], //임계치 distinct 필드
    validation: {
      //validate
      validate: false,
      hasErrRuleNm: this.props.rule ? false : true,
      hasErrRuleChgReason: true
    },
    thrsldFieldPopup: false,
    alertPopup: false,
    alertMsg: ""
  };

  /** onChange input */
  onChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      rule: {
        ...this.state.rule,
        [name]: value
      }
    });
  };

  /** onChange datepicker */
  onChangeDatePicker = (name, date) => {
    this.setState({
      rule: {
        ...this.state.rule,
        [name]: `${date.getFullYear()}-${("0" + (date.getMonth() + 1)).slice(
          -2
        )}-${("0" + date.getDate()).slice(-2)}`
      }
    });
  };

  /** onChange timePicker */
  onChangeTimePicker = (name, time) => {
    this.setState({
      rule: {
        ...this.state.rule,
        [name]: `${("0" + time.getHours()).slice(-2)}${(
          "0" + time.getMinutes()
        ).slice(-2)}`
      }
    });
  };

  /** 4 digits time to Date */
  timeToDate = time => {
    //HH:mm format
    if (/^[0-9]{4}/.test(time)) {
      const date = new Date();
      date.setHours(time.substr(0, 2));
      date.setMinutes(time.substr(2, 2));
      return date;
    } else {
      return null;
    }
  };

  /** onChange week checkbox */
  onChangeWeek = e => {
    const { name, checked } = e.target;
    let week = this.state.rule.ruleCommitWeek
      ? this.state.rule.ruleCommitWeek.split(",")
      : []; //1,2,3,4,5... to [1,2,3,4,5...];
    if (checked) {
      week.push(name);
      week.sort();
    } else {
      week = week.filter(day => day !== name);
    }

    this.setState({
      rule: {
        ...this.state.rule,
        ruleCommitWeek: week.join(",")
      }
    });
  };

  getWeekChecked = day => this.state.rule.ruleCommitWeek.indexOf(day) !== -1;

  /** commitYn check */
  onChangeCheck = e => {
    const { name, checked } = e.target;
    this.setState({
      rule: {
        ...this.state.rule,
        [name]: checked ? "Y" : "N"
      }
    });
  };

  /** filtertree callback */
  onChangeFilter = filters => {
    this.setState({
      rule: {
        ...this.state.rule,
        filters
      }
    });
  };

  /** import json data */
  importJosnFile = e => {
    const importJsonFile = e.target.files[0];

    if (checkJsonFile(importJsonFile.name)) {
      const reader = new FileReader();
      reader.onload = e => {
        const rule = JSON.parse(e.target.result);
        this.setState({ rule });
      };
      reader.readAsText(importJsonFile);
    }
  };

  /** 룰타입 변경에 따른 값 초기화 */
  onChangeRuleType = (ruleType, e) => {
    let ruleTypeParam;
    if (ruleType === "1000") {
      //임계치,상관 제외
      ruleTypeParam = {
        thrsldGroupField: "",
        thrsldGroupPeriod: "",
        thrsldGroupCnt: "",
        thrsldDstnctField: "",
        thrsldDstnctCnt: "",
        thrsldOprtrPeriod: "",
        thrsldOprtr: "",
        droolsCode: ""
      };
    } else if (ruleType === "0100") {
      //상관 제외
      ruleTypeParam = {
        droolsCode: ""
      };
    } else if (ruleType === "1100") {
      //임계치, 단순 제외
      ruleTypeParam = {
        thrsldGroupField: "",
        thrsldGroupPeriod: "",
        thrsldGroupCnt: "",
        thrsldDstnctField: "",
        thrsldDstnctCnt: "",
        thrsldOprtrPeriod: "",
        thrsldOprtr: "",
        detectCndtn: ""
      };
    }

    this.setState({
      rule: {
        ...this.state.rule,
        ...ruleTypeParam,
        ruleType
      }
    });
  };

  /** validate 토글 */
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  /** form validation */
  isValidate = () => {
    //validate를 true로 설정 시 모든 form의 유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);
    const { hasErrRuleNm, hasErrRuleChgReason } = this.state.validation;
    return !hasErrRuleNm && !hasErrRuleChgReason;
  };

  /** 저장 */
  onSaveRule = async () => {
    if (this.isValidate()) {
      const { RuleActions } = this.props;
      try {
        if (util.isValid(this.state.rule.ruleSeq)) {
          await RuleActions.updateRule(this.state.rule);
        } else {
          await RuleActions.insertRule(this.state.rule);
        }
        this.showAlert(<IntlMessages id="do.save" />);
      } catch (err) {
        console.warn(err);
        //alert처리
      }
    }
  };

  onDeleteRule = async () => {
    if (this.isValidate()) {
      const { RuleActions } = this.props;
      const rule = this.state.rule;
      const rules = { ruleChgReason: rule.ruleChgReason, rules: [rule] };
      try {
        await RuleActions.deleteRule(rules);
        this.showAlert(<IntlMessages id="do.delete" />);
      } catch (error) {
        console.warn(error);
      }
    }
  };

  /** 임계치필드 팝업 선택 */
  onFieldSelect = ({ thrsldGroupField, thrsldDstnctField }) => {
    this.setState({
      rule: {
        ...this.state.rule,
        thrsldGroupField,
        thrsldDstnctField
      }
    });
  };

  showAlert = alertMsg => this.setState({ alertPopup: true, alertMsg });

  /** 팝업 제어 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** props setting */
  static propTypes = {
    isShow: PropTypes.bool,
    disabled: PropTypes.bool,
    onCancel: PropTypes.func,
    codes: PropTypes.objectOf(PropTypes.array),
    rule: PropTypes.object
  };
  static defaultProps = {
    isShow: false,
    disabled: false,
    onCancel: () => console.log("RuleInfo :: onCnacel is not defined"),
    codes: {
      week: [],
      ruleType: [],
      thrsldOprtr: [],
      attackCode: [],
      ruleGroupCode: [],
      severityCode: []
    },
    rule: {
      ruleSeq: "",
      ruleNm: "",
      ruleType: "1000", //단순룰
      ruleCommitStrDt: "",
      ruleCommitEndDt: "",
      ruleCommitStrTime: "",
      ruleCommitEndTime: "",
      ruleCommitWeek: "",
      thrsldGroupField: "",
      thrsldGroupPeriod: "",
      thrsldGroupCnt: "",
      thrsldDstnctField: "",
      thrsldDstnctCnt: "",
      thrsldOprtrPeriod: "",
      thrsldOprtr: "",
      detectCndtn: "",
      droolsCode: "",
      relshpRule: "",
      cntrlTargt: "",
      custmNm: "",
      ruleDesc: "",
      attackCode: "",
      ruleGroupCode: "",
      severityCode: "",
      autoReportYn: " ",
      comment: "",
      takeActionDescription: "",
      takeBActionDescription: "",
      commitYn: "N",
      ruleChgReason: "",
      filters: []
    }
  };

  render() {
    const {
      onChangeInput,
      onChangeCheck,
      onChangeWeek,
      onChangeFilter,
      onChangeRuleType,
      onChangeDatePicker,
      onChangeTimePicker,
      timeToDate,
      importJosnFile,
      openPopup,
      closePopup,
      onFieldSelect,
      onSaveRule,
      getWeekChecked,
      onDeleteRule
    } = this;
    const {
      rule,
      thrsldFieldPopup,
      disabled,
      alertPopup,
      alertMsg
    } = this.state;
    const { validate } = this.state.validation;
    const { isShow, onCancel, codes } = this.props;

    return (
      <div
        className="component filter-detail"
        style={{ display: isShow ? "block" : "none" }}
      >
        <div className="top" style={{ overflowY: "auto" }}>
          <div className="left" style={{ width: "580px" }}>
            <div className="filter-info">
              <div className="component__title">
                <span className="filter-title">
                  <IntlMessages id="basic.info" />
                </span>
                {disabled || (
                  <div className="file-import">
                    <input
                      id="importBtn"
                      type="file"
                      className="form-control"
                      onChange={importJosnFile}
                    />
                    <label
                      htmlFor="importBtn"
                      className="btn btn-icon btn--file"
                    />
                  </div>
                )}
              </div>
              <div className="table-wrapper">
                <table className="table table--dark table--info security-event">
                  <colgroup>
                    <col width="100px" />
                    <col width="240px" />
                    <col width="100px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <span className="font--red">*</span>
                        <IntlMessages id="rule.name" />
                      </th>
                      <td colSpan="3">
                        <Textbox
                          id="ruleNm"
                          name="ruleNm"
                          type="text"
                          tabIndex="1"
                          classNameInput="form-control"
                          maxLength="200"
                          value={rule.ruleNm}
                          onChange={onChangeInput}
                          onBlur={() => {}}
                          disabled={disabled}
                          validate={validate}
                          validationOption={{locale: this.props.language}}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrRuleNm: res,
                                validate: false
                              }
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <span className="font--red">*</span>
                        <IntlMessages id="rule.type" />
                      </th>
                      <td style={{ overflow: "unset" }}>
                        <Select
                          id="ruleType"
                          name="ruleType"
                          tabIndex="2"
                          customStyleInput={{ height: "24px" }}
                          value={rule.ruleType}
                          onChange={onChangeRuleType}
                          optionList={codes.ruleType}
                          disabled={disabled}
                        />
                      </td>
                      <th>
                        <IntlMessages id="apply.yn" />
                      </th>
                      <td>
                        <div className="checkbox checkbox--dark">
                          <label>
                            <input
                              type="checkbox"
                              name="commitYn"
                              tabIndex="3"
                              checked={rule.commitYn === "Y" ? true : false}
                              onChange={onChangeCheck}
                              disabled={disabled}
                            />
                            <div className="icon" />
                          </label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="period" />
                      </th>
                      <td colSpan="3">
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={
                            rule.ruleCommitStrDt
                              ? new Date(rule.ruleCommitStrDt)
                              : null
                          }
                          onChange={date =>
                            onChangeDatePicker("ruleCommitStrDt", date)
                          }
                          startDate={
                            rule.ruleCommitStrDt
                              ? new Date(rule.ruleCommitStrDt)
                              : null
                          }
                          endDate={
                            rule.ruleCommitEndDt
                              ? new Date(rule.ruleCommitEndDt)
                              : null
                          }
                          disabled={disabled}
                          dropdownMode="select"
                          selectsStart
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                        />
                        ~
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={
                            rule.ruleCommitEndDt
                              ? new Date(rule.ruleCommitEndDt)
                              : null
                          }
                          onChange={date =>
                            onChangeDatePicker("ruleCommitEndDt", date)
                          }
                          startDate={
                            rule.ruleCommitStrDt
                              ? new Date(rule.ruleCommitStrDt)
                              : null
                          }
                          endDate={
                            rule.ruleCommitEndDt
                              ? new Date(rule.ruleCommitEndDt)
                              : null
                          }
                          disabled={disabled}
                          dropdownMode="select"
                          selectsStart
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="day.of.the.week" />
                      </th>
                      <td colSpan="3" style={{ paddingRight: 0 }}>
                        <div className="checkbox__wrap">
                          {codes.week.map((day, index) => {
                            return (
                              <div
                                className="checkbox checkbox--dark"
                                key={index}
                              >
                                <label>
                                  <input
                                    type="checkbox"
                                    name={day.id}
                                    id={day.id}
                                    checked={getWeekChecked(day.id)}
                                    onChange={onChangeWeek}
                                    disabled={disabled}
                                  />
                                  <div className="icon" />
                                  <span>{day.name}</span>
                                </label>
                              </div>
                            );
                          })}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="time" />
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          selected={timeToDate(rule.ruleCommitStrTime)}
                          onChange={time =>
                            onChangeTimePicker("ruleCommitStrTime", time)
                          }
                          showTimeSelect
                          showTimeSelectOnly
                          dateFormat="HH:mm"
                          disabled={disabled}
                        />
                        ~
                        <DatePicker
                          className="form-control"
                          selected={timeToDate(rule.ruleCommitEndTime)}
                          onChange={time =>
                            onChangeTimePicker("ruleCommitEndTime", time)
                          }
                          showTimeSelect
                          showTimeSelectOnly
                          dateFormat="HH:mm"
                          disabled={disabled}
                        />
                      </td>
                      <th>Rule Group Code</th>
                      <td>
                        <Select
                          name="ruleGroupCode"
                          tabIndex="4"
                          customStyleInput={{ height: "24px" }}
                          value={rule.ruleGroupCode}
                          onChange={(ruleGroupCode, e) => {
                            this.setState({
                              rule: {
                                ...this.state.rule,
                                ruleGroupCode
                              }
                            });
                          }}
                          optionList={codes.ruleGroupCode}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="refer.rule" />
                      </th>
                      <td>
                        <Textbox
                          id="relshpRule"
                          name="relshpRule"
                          type="text"
                          tabIndex="5"
                          maxLength="4000"
                          classNameInput="form-control"
                          value={rule.relshpRule}
                          onChange={onChangeInput}
                          disabled={disabled}
                        />
                      </td>
                      <th>Attack Code</th>
                      <td>
                        <Select
                          id="attackCode"
                          name="attackCode"
                          tabIndex="6"
                          customStyleInput={{ height: "24px" }}
                          value={rule.attackCode}
                          onChange={(attackCode, e) => {
                            this.setState({
                              rule: {
                                ...this.state.rule,
                                attackCode
                              }
                            });
                          }}
                          optionList={codes.attackCode}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="control.target" />
                      </th>
                      <td>
                        <Textbox
                          id="cntrlTargt"
                          name="cntrlTargt"
                          type="text"
                          tabIndex="7"
                          maxLength="4000"
                          classNameInput="form-control"
                          value={rule.cntrlTargt}
                          onChange={onChangeInput}
                          disabled={disabled}
                        />
                      </td>
                      <th>Severity Code</th>
                      <td>
                        <Select
                          id="severityCode"
                          name="severityCode"
                          tabIndex="8"
                          customStyleInput={{ height: "24px" }}
                          value={rule.severityCode}
                          onChange={(severityCode, e) => {
                            this.setState({
                              rule: {
                                ...this.state.rule,
                                severityCode
                              }
                            });
                          }}
                          optionList={codes.severityCode}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>Company</th>
                      <td>
                        <Textbox
                          id="custmNm"
                          name="custmNm"
                          type="text"
                          tabIndex="9"
                          maxLength="4000"
                          classNameInput="form-control"
                          value={rule.custmNm}
                          onChange={onChangeInput}
                          disabled={disabled}
                        />
                      </td>
                      <th>
                        <IntlMessages id="report.autoyn" />
                      </th>
                      <td>
                        <Select
                          id="autoReportYn"
                          name="autoReportYn"
                          tabIndex="10"
                          customStyleInput={{ height: "24px" }}
                          value={rule.autoReportYn}
                          onChange={(autoReportYn, e) => {
                            this.setState({
                              rule: {
                                ...this.state.rule,
                                autoReportYn
                              }
                            });
                          }}
                          optionList={[
                            {
                              id: " ",
                              name: <IntlMessages id="select.options" />
                            },
                            { id: "Y", name: "Y" },
                            { id: "N", name: "N" }
                          ]}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="desc" />
                      </th>
                      <td colSpan="3">
                        <Textarea
                          classNameInput="form-control"
                          id="ruleDesc"
                          name="ruleDesc"
                          tabIndex="11"
                          value={rule.ruleDesc}
                          onChange={onChangeInput}
                          maxLenth="4000"
                          customStyleInput={{ height: "60px" }}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>Comment</th>
                      <td colSpan="3">
                        <Textarea
                          classNameInput="form-control"
                          id="comment"
                          name="comment"
                          tabIndex="12"
                          value={rule.comment}
                          onChange={onChangeInput}
                          maxLenth="1000"
                          customStyleInput={{ height: "60px" }}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>Take Action Description</th>
                      <td colSpan="3">
                        <Textarea
                          classNameInput="form-control"
                          id="takeActionDescription"
                          name="takeActionDescription"
                          tabIndex="13"
                          value={rule.takeActionDescription}
                          onChange={onChangeInput}
                          maxLenth="4000"
                          customStyleInput={{ height: "60px" }}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>Take B Action Description</th>
                      <td colSpan="3">
                        <Textarea
                          classNameInput="form-control"
                          id="takeBActionDescription"
                          name="takeBActionDescription"
                          tabIndex="14"
                          value={rule.takeBActionDescription}
                          onChange={onChangeInput}
                          maxLenth="4000"
                          customStyleInput={{ height: "60px" }}
                          disabled={disabled}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="change-reason" style={{ marginTop: "20px" }}>
              <div className="component__title">
                <span>
                  <span className="font--red">*</span>
                  <IntlMessages id="reason.change" />
                </span>
              </div>
              <Textarea
                classNameInput="form-control"
                id="ruleChgReason"
                tabIndex="15"
                maxLength="4000"
                name="ruleChgReason"
                value={rule.ruleChgReason}
                onChange={onChangeInput}
                onBlur={() => {}}
                validate={validate}
                validationOption={{locale: this.props.language}}
                validationCallback={res => {
                  this.setState({
                    validation: {
                      ...this.state.validation,
                      hasErrRuleChgReason: res,
                      validate: false
                    }
                  });
                }}
              />
            </div>
          </div>

          <div className="right">
            <div className="filter-condition" style={{ height: "55%" }}>
              {rule.ruleType !== "1100" && (
                <FilterTree
                  data={rule.filters}
                  disabled={disabled}
                  onChange={onChangeFilter}
                />
              )}
              {rule.ruleType === "1100" && (
                <React.Fragment>
                  <span className="icon-and" />
                  <div className="component__title">
                    <span>
                      <IntlMessages id="drools.editor" />
                    </span>
                  </div>
                  <div
                    className="component__box"
                    style={{ height: "400px", padding: "15px" }}
                  >
                    <Textarea
                      classNameInput="form-control"
                      id="droolsCode"
                      disabled={disabled}
                      name="droolsCode"
                      value={rule.droolsCode}
                      onChange={onChangeInput}
                      customStyleInput={{ height: "400px" }}
                    />
                  </div>
                </React.Fragment>
              )}
            </div>
            {rule.ruleType === "0100" && (
              <div>
                <div className="component__title" style={{ marginTop: "10px" }}>
                  <span>
                    <IntlMessages id="threshold.info" />
                  </span>
                  <div className="btns btns--left">
                    <button
                      className="btn btn--dark"
                      disabled={disabled}
                      onClick={() => openPopup("thrsldFieldPopup")}
                    >
                      <IntlMessages id="threshold.field.select" />
                    </button>
                  </div>
                </div>
                <table className="table table--dark table--info security-event">
                  <tbody>
                    <tr>
                      <th rowSpan="2">Group by Fields</th>
                      <td colSpan="4">{rule.thrsldGroupField}</td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="duration" />
                      </th>
                      <td>
                        <Textbox
                          id="thrsldGroupPeriod"
                          name="thrsldGroupPeriod"
                          value={rule.thrsldGroupPeriod}
                          onChange={onChangeInput}
                          classNameInput="form-control"
                          disabled={disabled}
                          validationOption={{
                            type: "number",
                            locale: this.props.language
                          }}
                        />
                      </td>
                      <th>Count</th>
                      <td>
                        <Textbox
                          classNameInput="form-control"
                          id="thrsldGroupCnt"
                          disabled={disabled}
                          value={rule.thrsldGroupCnt}
                          name="thrsldGroupCnt"
                          onChange={onChangeInput}
                          validationOption={{
                            type: "number",
                            locale: this.props.language
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th rowSpan="2">Distinct Fields</th>
                      <td colSpan="4">{rule.thrsldDstnctField}</td>
                    </tr>
                    <tr>
                      <th>Count</th>
                      <td colSpan="3">
                        <Textbox
                          classNameInput="form-control"
                          disabled={disabled}
                          id="thrsldDstnctCnt"
                          name="thrsldDstnctCnt"
                          value={rule.thrsldDstnctCnt}
                          onChange={onChangeInput}
                          validationOption={{
                            type: "number",
                            locale: this.props.language
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>Aggregation</th>
                      <th>
                        <IntlMessages id="duration" />
                      </th>
                      <td>
                        <Textbox
                          classNameInput="form-control"
                          disabled={disabled}
                          id="thrsldOprtrPeriod"
                          name="thrsldOprtrPeriod"
                          value={rule.thrsldOprtrPeriod}
                          onChange={onChangeInput}
                          validationOption={{
                            type: "number",
                            locale: this.props.language
                          }}
                        />
                      </td>
                      <th>Operation</th>
                      <td style={{ overflow: "unset" }}>
                        <Select
                          id="thrsldOprtr"
                          name="thrsldOprtr"
                          disabled={disabled}
                          customStyleInput={{ height: "24px" }}
                          value={rule.thrsldOprtr}
                          onChange={(thrsldOprtr, e) =>
                            this.setState({
                              rule: { ...this.state.rule, thrsldOprtr }
                            })
                          }
                          onBlur={() => {}}
                          optionList={codes.thrsldOprtr}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
                {thrsldFieldPopup && (
                  <ThrsldFieldPopup
                    onFieldSelect={onFieldSelect}
                    groupBy={
                      rule.thrsldGroupField !== ""
                        ? rule.thrsldGroupField.split(",")
                        : []
                    }
                    distinct={
                      rule.thrsldDstnctField !== ""
                        ? rule.thrsldDstnctField.split(",")
                        : []
                    }
                    onClose={() => closePopup("thrsldFieldPopup")}
                  />
                )}
              </div>
            )}
            <div style={{ marginTop: "20px" }}>
              <div
                className="component__title"
                style={{ marginBottom: "10px" }}
              >
                <span>
                  <IntlMessages id="threat.class" />
                </span>
              </div>
              <TaxonomyTable ruleSeq={rule.ruleSeq} />
            </div>
          </div>
        </div>
        <div className="bottom">
          <div className="btns">
            <button className="btn btn--dark" onClick={onCancel}>
              <IntlMessages id="cancel" />
            </button>
            {util.isValid(this.props.rule.ruleSeq) && (
              <button className="btn btn--grey" onClick={onDeleteRule}>
                <IntlMessages id="delete" />
              </button>
            )}
            {disabled ? (
              <button
                className="btn btn--blue"
                onClick={() => this.setState({ disabled: false })}
              >
                <IntlMessages id="edit" />
              </button>
            ) : (
              <button className="btn btn--blue" onClick={onSaveRule}>
                <IntlMessages id="save" />
              </button>
            )}
          </div>
        </div>
        {alertPopup && (
          <AlertPopup
            message={alertMsg}
            onConfirm={onCancel}
            onClose={onCancel}
          />
        )}
      </div>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(RuleInfo);
