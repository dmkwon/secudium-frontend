import React, { Component } from "react";
import IntlMessages from "util/IntlMessages";

import RuleInfo from "./RuleInfo";
import RuleHistory from "./RuleHistory";

export default class RuleDetail extends Component {
  state = {
    isInfo: true
  };

  setIsInfo = isInfo => this.setState({ isInfo });

  render() {
    const { isInfo } = this.state;
    const { onList, codes, data, disabled } = this.props;

    return (
      <div className="component-filter-detail">
        <ul className="component-tab">
          <li
            className={isInfo ? "active" : ""}
            onClick={() => this.setIsInfo(true)}
          >
            <IntlMessages id="rule.info" />
          </li>
          <li
            className={!isInfo ? "active" : ""}
            onClick={() => this.setIsInfo(false)}
          >
            <IntlMessages id="change.hist" />
          </li>
        </ul>
        <RuleInfo
          isShow={isInfo}
          codes={codes}
          rule={data.rule}
          onCancel={onList}
          disabled={disabled}
        />
        <RuleHistory
          isShow={!isInfo}
          codes={codes}
          ruleHist={data.ruleHist}
          ruleHists={data.ruleHists}
          onCancel={onList}
        />
      </div>
    );
  }
}
