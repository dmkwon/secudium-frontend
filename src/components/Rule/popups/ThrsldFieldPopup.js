import React, { Component } from "react";

import GridTable from "../../Common/GridTable";

import FieldGroupTree from "./FieldGroupTree";
import { getFields } from "./../../../store/api/field";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

export default class ThrsldFieldPopup extends Component {
  state = {
    selectedField: [],
    fields: [],
    hdrChkbox: false,
    groupBy: this.props.groupBy || [],
    distinct: this.props.distinct || []
  };

  onSelectTree = (selectKeys, e) => {
    const key = selectKeys[0];
    this.getFields(key);
  };

  /** 헤더 체크박스  */
  onChangeHdrchkbox = e => {
    this.setCheckbox(e.target.checked);
  };

  /** 전체 체크박스 설정(헤더제외) */
  setCheckbox = checked => {
    const fields = this.state.fields.map(field => {
      field["checked"] = checked;
      return field;
    });
    this.setState({ hdrChkbox: checked, fields });
    //this.onSelectCallbackHandler(data);
  };

  /** 체크박스 */
  onChangeChkbox = (fieldIndex, e) => {
    let hdrChkbox = this.state.hdrChkbox;
    const fields = this.state.fields.map((field, index) => {
      if (fieldIndex === index) {
        field["checked"] = e.target.checked;
      }
      //하나라도 체크가 풀릴경우 헤더체크박스도 체크가 풀린다
      hdrChkbox = hdrChkbox && field["checked"];
      return field;
    });
    this.setState({ fields, hdrChkbox });
    // this.onSelectCallbackHandler(data);
  };

  //그리드 페이지 변경시 콜백(체크박스 해제처리)
  onPageChange = () => {
    //this.setCheckbox(false);
  };

  onClickAddGroupBy = () => {
    let groupBy = this.state.fields.filter((field, index) => field["checked"]);
    groupBy = groupBy.map(group => `${group.fieldId}:${group.field}`);
    groupBy = groupBy.filter(field => this.state.groupBy.indexOf(field) === -1);
    if (groupBy.length > 0) {
      this.setState({ groupBy: [...this.state.groupBy, ...groupBy] });
    }
    console.log(this.state);
    this.setCheckbox(false);
  };

  onClickAddDistinct = () => {
    let distinct = this.state.fields.filter((field, index) => field["checked"]);
    distinct = distinct.map(dist => `${dist.fieldId}:${dist.field}`);
    distinct = distinct.filter(
      field => this.state.distinct.indexOf(field) === -1
    );
    distinct = distinct.filter(
      field => this.state.groupBy.indexOf(field) === -1
    ); //groupby 중복제외

    if (distinct.length > 0) {
      this.setState({ distinct: [...this.state.distinct, ...distinct] });
    }

    console.log(this.state);

    this.setCheckbox(false);
  };

  onSave = () => {
    const thrsldGroupField = this.state.groupBy.join(",");
    const thrsldDstnctField = this.state.distinct.join(",");
    this.props.onFieldSelect({ thrsldGroupField, thrsldDstnctField });
    this.props.onClose();
  };

  deleteGroupByField = field => {
    const groupBy = this.state.groupBy.filter(
      groupBy => field.indexOf(groupBy) === -1
    );
    this.setState({ groupBy });
  };

  deleteDistinctField = field => {
    const distinct = this.state.distinct.filter(
      distinct => field.indexOf(distinct) === -1
    );
    this.setState({ distinct });
  };

  componentDidMount() {
    this.getFields();
  }

  getFields = async key => {
    const res = await getFields(
      key && key !== "0" ? `searchfieldGroupcd=${key}` : ""
    );
    this.setState({ fields: res.data });
  };

  render() {
    const {
      onSelectTree,
      onChangeHdrchkbox,
      onChangeChkbox,
      onPageChange,
      onClickAddGroupBy,
      onClickAddDistinct,
      onSave,
      deleteGroupByField,
      deleteDistinctField
    } = this;
    const { fields, hdrChkbox, groupBy, distinct } = this.state;
    const { onClose } = this.props;

    const columns = [
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={onChangeHdrchkbox}
                checked={hdrChkbox}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={e => onChangeChkbox(row.index, e)}
                  checked={row.original.checked || false}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        sortable: false,
        width: 60
      },
      {
        Header: "No",
        accessor: "fieldSeq",
        width: 60,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="field.type" />,
        accessor: "fieldType",
        width: 60,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="field" />,
        accessor: "field",
        width: 120,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="desc" />,
        accessor: "fieldDesc",
        width: 120,
        style: { textAlign: "left" }
      },
      {
        Header: <IntlMessages id="field.group" />,
        accessor: "fieldGroupCode",
        width: 60,
        style: { textAlign: "center" }
      }
    ];

    return (
      <React.Fragment>
        <div className="popup popup--dark popup--filter" id="ThrsldFieldPopup">
          <div className="popup__header">
            <h5>
              <IntlMessages id="threshold.field.select" />
            </h5>
            <button className="btn btn-close" onClick={onClose} />
          </div>
          <div className="popup__body">
            <div className="flex-box">
              <div className="component filter-tree">
                <div className="filter-tree__wrap">
                  <FieldGroupTree onSelect={onSelectTree} />
                </div>
              </div>

              <div className="filter-table">
                <div className="search-bar">
                  <input type="text" className="form-control" />
                  <div className="binder" />
                  <button className="btn btn-icon small btn--go" />
                  <button className="btn btn-icon small btn--filter" />
                </div>

                <GridTable
                  data={fields}
                  columns={columns}
                  filterable={false}
                  sortable
                  resizable={false}
                  onPageChange={onPageChange}
                />
              </div>

              <div className="btns">
                <button
                  onClick={onClickAddGroupBy}
                  className="btn btn--blue btn-icon customer-wrapper__btn"
                  style={{ marginTop: "20px" }}
                />
                <button
                  onClick={onClickAddDistinct}
                  className="btn btn--blue btn-icon customer-wrapper__btn"
                  style={{ marginTop: "110px" }}
                />
              </div>

              <div className="filter-condition" style={{ minWidth: "120px" }}>
                <div className="component__title">
                  <span>
                    <IntlMessages id="selected.item.groupby" />
                  </span>
                </div>
                <div className="component__box" style={{ marginBottom: 20 }}>
                  <div
                    className="filter-tree__wrap"
                    style={{ height: "200px", color: "#fff", padding: 10 }}
                  >
                    {groupBy.map((field, index) => (
                      <p key={index} style={{ marginBottom: 0 }}>
                        <span style={{ verticalAlign: "middle" }}>{field}</span>
                        <button
                          className="btn btn--dark-close"
                          style={{ backgroundSize: "cover" }}
                          onClick={() => deleteGroupByField(field)}
                        />
                      </p>
                    ))}
                  </div>
                </div>

                <div className="component__title">
                  <span>
                    <IntlMessages id="selected.item.distinct" />
                  </span>
                </div>
                <div className="component__box">
                  <div
                    className="filter-tree__wrap"
                    style={{ height: "200px", color: "#fff", padding: 10 }}
                  >
                    {distinct.map((field, index) => (
                      <p key={index} style={{ marginBottom: 0 }}>
                        <span style={{ verticalAlign: "middle" }}>{field}</span>
                        <button
                          className="btn btn--dark-close"
                          style={{ backgroundSize: "cover" }}
                          onClick={() => deleteDistinctField(field)}
                        />
                      </p>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={onClose}>
              <IntlMessages id="cancel" />
            </button>
            <button className="btn btn--dark" onClick={onSave}>
              <IntlMessages id="confirm" />
            </button>
          </div>
        </div>
        <div className="dim" />
      </React.Fragment>
    );
  }
}
