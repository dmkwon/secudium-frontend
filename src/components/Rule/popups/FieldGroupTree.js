import React, { Component } from "react";
import Tree, { TreeNode } from "rc-tree";
import IntlMessages from "util/IntlMessages";

import * as api from "./../../../store/api/common";
import "rc-tree/assets/index.css";

export default class FieldGroupTree extends Component {
  state = {
    treeData: [{ key: 0, name: <IntlMessages id="all" /> }]
  };

  componentDidMount() {
    this.initTreeData();
  }

  initTreeData = async () => {
    try {
      const res = await api.getCodeList("FIELD_GROUP");
      const data = res.data.map(el => {
        return { name: el.name, key: el.id };
      });
      const treeData = [
        { key: 0, name: <IntlMessages id="all" />, children: data }
      ];
      this.setState({ treeData });
    } catch (error) {
      console.log(error);
    }
  };

  static defaultProps = {
    onSelect: (selectedKeys, e) =>
      console.log("threatTaxonomyTree ::: onSelect is not Defined.", {
        selectedKeys,
        e
      })
  };

  render() {
    const treeCls = `myCls${(this.state.useIcon && " customIcon") || ""}`;

    const loop = data => {
      return data.map(item => {
        if (item.children) {
          return (
            <TreeNode title={item.name} key={item.key}>
              {loop(item.children)}
            </TreeNode>
          );
        } else {
          return (
            <TreeNode
              title={item.name}
              key={item.key}
              isLeaf={item.isLeaf}
              disabled={item.key === "0-0-0-0"}
            />
          );
        }
      });
    };
    const { onSelect } = this.props;

    return (
      <Tree
        className={treeCls}
        onSelect={onSelect}
        defaultExpandedKeys={["0"]}
        defaultSelectedKeys={["0"]}
        selectable
      >
        {loop(this.state.treeData)}
      </Tree>
    );
  }
}
