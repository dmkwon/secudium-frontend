import React from "react";
import Draggable from "react-draggable";

import ThreatTaxoTree from "./../../Common/ThreatTaxoTree";
import IntlMessages from "util/IntlMessages";

export default class TaxoSelectPopup extends React.Component {
  state = {
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("TaxoSelectPopup").offsetWidth;
    const elHeight = document.getElementById("TaxoSelectPopup").offsetHeight;
    this.setState({
      position: { x: elWidth / 2, y: (elHeight / 2) * -1 - 200 }
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  onCheck = (checkedKeys, e) => this.setState({ checkedKeys });
  onConfirm = () => {
    this.props.onConfirm(this.state.checkedKeys);
  };

  render() {
    const { onCheck, onConfirm } = this;
    const { position } = this.state;
    const { onClose } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--dark popup--filter"
            style={{ width: "350px" }}
            id="TaxoSelectPopup"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <IntlMessages id="class.system.select" />
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div
              className="popup__body"
              style={{ color: "#fff", maxHeight: 500, overflowY: "auto" }}
            >
              <ThreatTaxoTree
                selectable={false}
                checkable
                onCheck={onCheck}
                arrowColor={"#2f323c"}
              />
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--blue" onClick={onConfirm}>
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}
