import React, { Component } from "react";
import { Textarea } from "react-inputs-validation";
import IntlMessages from "util/IntlMessages";

export default class DeletePopup extends Component {
  state = {
    ruleChgReason: "",
    validation: {
      validate: false,
      hasErrRuleChgReason: true
    }
  };

  onClick = () => {
    if (this.isValidate()) {
      this.props.onClickConfirm(this.state.ruleChgReason);
    }
  };

  onChange = (ruleChgReason, e) => this.setState({ ruleChgReason });

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    //validate를 true로 설정 시 모든 form의 유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);
    const { hasErrRuleChgReason } = this.state.validation;
    return !hasErrRuleChgReason;
  };

  render() {
    const { onClick, onChange } = this;
    const { onClose, message } = this.props;
    const { ruleChgReason } = this.state;
    const { validate } = this.state.validation;

    return (
      <div>
        <div className="popup popup--alert" style={{ width: "400px" }}>
          <div className="popup__header">
            <h5>
              <IntlMessages id="confirm" />
            </h5>
            <button className="btn btn-close" onClick={onClose} />
          </div>
          <div className="popup__body" style={{ textAlign: "left" }}>
            {message}
            <div className="change-reason">
              <label style={{ margin: "0" }}>
                <span className="font--red">*</span>
                <IntlMessages id="reason.change" />
              </label>
              <Textarea
                name="ruleChgReason"
                type="text"
                tabIndex="1"
                maxLength="4000"
                value={ruleChgReason}
                onChange={onChange}
                onBlur={() => {}}
                validate={validate}
                validationOption={{locale: this.props.language}}
                validationCallback={res => {
                  this.setState({
                    validation: {
                      ...this.state.validation,
                      hasErrRuleChgReason: res,
                      validate: false
                    }
                  });
                }}
                customStyleInput={{
                  width: "100%",
                  resize: "none",
                  height: "80px",
                  marginTop: "15px"
                }}
              />
            </div>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={onClose}>
              <IntlMessages id="cancel" />
            </button>
            <button className="btn btn--dark" onClick={onClick}>
              <IntlMessages id="confirm" />
            </button>
          </div>
        </div>
        <div className="dim" />
      </div>
    );
  }
}
