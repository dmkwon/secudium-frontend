import React from "react";
import IntlMessages from "util/IntlMessages";

const AlertPopup = ({ message, onConfirm, onClose }) => {
  return (
    <div>
      <div
        className="popup popup--alert"
        style={{ minWidth: "350px", zIndex: "13" }}
      >
        <div className="popup__header">
          <h5>
            <IntlMessages id="alarm" />
          </h5>
          <button className="btn btn-close" onClick={onClose} />
        </div>
        <div className="popup__body">{message}</div>
        <div className="popup__footer">
          <button className="btn btn--dark" onClick={onConfirm}>
            <IntlMessages id="confirm" />
          </button>
        </div>
      </div>
      <div className="innerDim" />
    </div>
  );
};

export default AlertPopup;
