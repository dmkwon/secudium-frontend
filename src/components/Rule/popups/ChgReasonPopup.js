import React, { Component } from "react";
import { Textarea } from "react-inputs-validation";
import PropTypes from "prop-types";
import IntlMessages from "util/IntlMessages";

export default class ChgReasonPopup extends Component {
  state = {
    chgReason: "",
    validation: {
      validate: false,
      hasErrChgReason: true
    }
  };

  onClickConfirm = () => {
    if (this.isValidate()) {
      this.props.onConfirm(this.state.chgReason);
    }
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    //validate를 true로 설정 시 모든 form의 유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);
    const { hasErrChgReason } = this.state.validation;
    return !hasErrChgReason;
  };

  onChangeTextarea = (value, e) => {
    const { name } = e.target;
    this.setState({ [name]: value });
  };

  /**
   * props setting
   */
  static defaultProps = {
    onClose: () => console.log("ChgReasonPopup :: onClose is not defined"),
    onConfirm: () => console.log("ChgReasonPopup :: onConfirm is not defined"),
    message: "ConfirmPopup :: message is not defined",
    textareaTitle: <IntlMessages id="reason.change" />
  };

  static propTypes = {
    onClose: PropTypes.func,
    onConfirm: PropTypes.func,
    message: PropTypes.string,
    textareaTitle: PropTypes.string
  };

  render() {
    const { onChangeTextarea, onClickConfirm } = this;
    const { onClose, message, textareaTitle } = this.props;
    const { chgReason } = this.state;
    const { validate } = this.state.validation;

    return (
      <div>
        <div className="popup popup--alert" style={{ width: "400px" }}>
          <div className="popup__header">
            <h5>
              <IntlMessages id="confirm" />
            </h5>
            <button className="btn btn-close" onClick={onClose} />
          </div>
          <div className="popup__body">
            {message}
            <div className="change-reason">
              <label>
                {textareaTitle}
                <span className="font--red">*</span>
              </label>
              <Textarea
                name="chgReason"
                type="text"
                tabIndex="1"
                maxLength="4000"
                value={chgReason}
                onChange={onChangeTextarea}
                onBlur={() => {}}
                validate={validate}
                validationOption={{locale: this.props.language}}
                validationCallback={res => {
                  this.setState({
                    validation: {
                      ...this.state.validation,
                      hasErrChgReason: res,
                      validate: false
                    }
                  });
                }}
                customStyleInput={{
                  width: "100%",
                  resize: "none",
                  height: "80px",
                  marginTop: "15px"
                }}
              />
            </div>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={onClose}>
              <IntlMessages id="cancel" />
            </button>
            <button className="btn btn--dark" onClick={onClickConfirm}>
              <IntlMessages id="confirm" />
            </button>
          </div>
        </div>
        <div className="dim" />
      </div>
    );
  }
}
