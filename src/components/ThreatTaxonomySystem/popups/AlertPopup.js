import React, { Component } from "react";
import IntlMessages from "util/IntlMessages";

export default class AlertPopup extends Component {
  render() {
    const { resource, onClose } = this.props;

    return (
      <React.Fragment>
        <div className="popup popup--alert">
          <div className="popup__header">
            <h5>{resource.title}</h5>
          </div>
          <div className="popup__body">{resource.message}</div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={onClose}>
              <IntlMessages id="cancel" />
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
