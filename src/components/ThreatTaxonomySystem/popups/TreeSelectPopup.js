import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import TaxonomyTreeForPopup from "../TaxonomyTreeForPopup";

import * as threatTaxonomyActions from "../../../store/modules/threattaxonomy";
import * as api from "../../../store/api/threattaxonomy";
import { List, Map } from "immutable";
import Draggable from "react-draggable";

import IntlMessages from "util/IntlMessages";

const initTreeData = List([
  Map({
    key: "0",
    name: <IntlMessages id="all" />,
    seq: "0",
    hasSubNode: "Y",
    children: List([])
  })
]);

class TreeSelectPopup extends React.Component {
  state = {
    position: {
      x: 0,
      y: 0
    },
    treeData: initTreeData,
    selectedKeys: [],
    expandedKeys: ["0"]
  };

  componentDidMount = () => {
    this.initNode();

    const elWidth = document.getElementById("TreeSelectPopup").offsetWidth;
    const elHeight = document.getElementById("TreeSelectPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1) - 200
      }
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  onExpand = async (expandedKeys, e) => {
    const selectedKeys = [];
    selectedKeys.push(e.node.props.eventKey);

    this.setState({
      expandedKeys
    });
    if (expandedKeys.length === 0) {
      return;
    }
  };

  //신규노드
  getNewNode = (key, name, seq, hasSubNode) =>
    Map({ key, name, seq, hasSubNode, children: List([]) });
  getSelectedKey = () => this.state.selectedKeys[0];
  getExpandedKey = () => this.state.expandedKeys[0];

  loadData = async treeNode => {
    const targetKey = treeNode.props.eventKey;
    const expandedKey = this.getExpandedKey();
    const selectedKey = this.state.expandedKeys[
      this.state.expandedKeys.length - 1
    ];

    if (targetKey === "0") {
      return;
    }

    let thrtSeq = targetKey.split("/")[1].split("-")[1];
    try {
      const res = await api.getSubNodes(thrtSeq);
      for (let i = 0; i < res.data.length; i++) {
        let nodeKey = `${res.data[i].tpThrtSeq}-${res.data[i].thrtSeq}`,
          hasSubNode = res.data[i].hasSubNode;
        this.addSubNode(
          res.data[i].thrtNm,
          nodeKey,
          hasSubNode,
          selectedKey,
          expandedKey
        );
      }
      return Promise.resolve();
    } catch (error) {
      console.log(error);
    }
  };

  initNode = async () => {
    try {
      const res = await api.getSubNodes(0);
      for (let i = 0; i < res.data.length; i++) {
        let nodeKey = `${res.data[i].tpThrtSeq}-${res.data[i].thrtSeq}`;
        this.addInitNode(res.data[i].thrtNm, nodeKey, res.data[i].hasSubNode);
      }
      return Promise.resolve();
    } catch (error) {
      console.log(error);
    }
  };

  addInitNode = (nodeName, nodeKey, hasSubNode) => {
    const selectedKey = "0",
      selector = this.makeSelector(selectedKey.split("-")),
      nodeSeq = this.getChildrenCnt(selector),
      key = `${selectedKey}-${nodeSeq}/${nodeKey}`;

    this.setState({
      treeData: this.state.treeData.setIn(
        [...selector, nodeSeq],
        this.getNewNode(key, nodeName, nodeKey, hasSubNode, false)
      ) // (key, name, seq, hasSubNode)
    });
  };

  addSubNode = (nodeName, nodeKey, hasSubNode, selectedKey, expandedKey) => {
    const exNodeKey = selectedKey.split("/")[0]; // nodeKey 분리.
    const selector = this.makeSelector(exNodeKey.split("-"));

    const nodeSeq = this.getChildrenCnt(selector);
    const key = `${exNodeKey}-${nodeSeq}/${nodeKey}`; // 하위  selectedKey + nodeKey(thrtSeq)

    this.setState({
      treeData: this.state.treeData.setIn(
        [...selector, nodeSeq],
        this.getNewNode(key, nodeName, nodeKey, hasSubNode, false)
      ),
      expandedKeys: [...this.state.expandedKeys, expandedKey]
    });
  };

  makeSelector = (keys, selectParent = false) => {
    let selector = [];
    for (let i = 0, length = keys.length; i < length; i++) {
      if (i === length - 1 && selectParent) {
        break;
      }
      const key = Number(keys[i]);
      selector.push(key);
      selector.push("children");
    }
    return selector;
  };

  getChildrenCnt = selector => {
    const target = this.state.treeData.getIn([...selector]);
    return target ? target.size : 0;
  };

  onSelectTreePopup = async (selectedKeys, e) => {
    const { setStateSelectKey, parentState } = this.props;

    if (selectedKeys.length === 0) {
      return;
    }

    if (selectedKeys[0] === "0") {
      this.setState({
        selectedKeys,
        selectedNode: e.node.props
      });
      setStateSelectKey(selectedKeys, e);
      return;
    }

    const thrtSeq = selectedKeys[0].split("/")[1].split("-")[1];
    if (thrtSeq === "2" && parentState.updateMod === false) {
      return;
    } else {
      this.setState({
        selectedKeys,
        selectedNode: e.node.props
      });
      setStateSelectKey(selectedKeys, e);
    }
  };

  render() {
    const { position } = this.state;
    const { onClose, onConfirm } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--dark popup--filter"
            style={{ width: "350px" }}
            id="TreeSelectPopup"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <IntlMessages id="class.system.popup" />
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div
              className="popup__body"
              style={{ color: "#fff", maxHeight: 500, overflowY: "auto" }}
            >
              <TaxonomyTreeForPopup
                onSelect={this.onSelectTreePopup}
                onExpand={this.onExpand}
                loadData={this.loadData}
                state={this.state}
              />
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--blue" onClick={onConfirm}>
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    taxoData: state.threattaxonomy.get("taxoData")
  }),
  dispatch => ({
    ThreatTaxonomyActions: bindActionCreators(threatTaxonomyActions, dispatch)
  })
)(TreeSelectPopup);
