import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as threatTaxonomyActions from "../../../store/modules/threattaxonomy";

import Draggable from "react-draggable";
import FilterTree from "../../Common/FilterTree";
import * as ruleActions from "../../../store/modules/rule";
import TaxonomyTable from "./../../Rule/TaxonomyTable";

import { getCodeList } from "../../../store/api/common";

import IntlMessages from "util/IntlMessages";

class RuleInfoPopup extends React.Component {
  state = {
    ruleInfo: this.props.ruleInfo,
    selected: 0,
    ruleType: this.props.ruleType,
    thrsldOprtr: [],
    week: [],
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    this.setCodes();
    const elWidth = document.getElementById("RuleInfoPopup").offsetWidth;
    const elHeight = document.getElementById("RuleInfoPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  setCodes = async () => {
    try {
      const thrsldOprtr = await getCodeList("THRSLD_OPRTR");
      const week = await getCodeList("WEEK");
      this.setState({
        thrsldOprtr: thrsldOprtr.data,
        week: week.data
      });
    } catch (err) {
      console.warn(err);
    }
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  getWeekChecked = day =>
    this.state.ruleInfo.ruleCommitWeek.indexOf(day) !== -1;

  /** 4 digits time to Date */
  timeToDate = time => {
    //HH:mm format
    if (/^[0-9]{4}/.test(time)) {
      const date = new Date();
      date.setHours(time.substring(0, 2));
      date.setMinutes(time.substring(2, 2));
      return date;
    } else {
      return null;
    }
  };

  openPopup = popupName => this.setState({ [popupName]: true });

  render() {
    const { position } = this.state;
    const { onClose } = this.props;
    const { getWeekChecked } = this;
    const { ruleInfo, week } = this.state;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--dark"
            style={{ width: "1000px" }}
            id="RuleInfoPopup"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <IntlMessages id="rule.info" />
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div
              className="popup__body"
              style={{ maxHeight: 750, overflowY: "auto" }}
            >
              <div
                className="component filter-detail"
                style={{ display: "block" }}
              >
                <div
                  className="top"
                  style={{ overflow: "hidden", height: "auto" }}
                >
                  <div className="left" style={{ width: "475px" }}>
                    <div
                      className="filter-info"
                      style={{
                        display: "flex",
                        flexDirection: "column",
                        height: "100%"
                      }}
                    >
                      <div className="component__title">
                        <span>
                          <IntlMessages id="basic.info" />
                        </span>
                      </div>
                      <div className="table-wrapper" stlye={{ flex: 1 }}>
                        <table
                          className="table table--dark table--info security-event"
                          style={{ height: "100%" }}
                        >
                          <colgroup>
                            <col width="130px" />
                            <col />
                          </colgroup>
                          <tbody>
                            <tr style={{ borderTop: "1px solid #484b54" }}>
                              <th>
                                <IntlMessages id="rule.name" />
                              </th>
                              <td colSpan="3">{ruleInfo.ruleNm}</td>
                            </tr>
                            <tr>
                              <th>
                                <IntlMessages id="rule.type" />
                              </th>
                              <td>{ruleInfo.ruleTypeString}</td>
                              <th>
                                <IntlMessages id="apply.yn" />
                              </th>
                              <td>
                                <div className="checkbox checkbox--dark">
                                  <label>
                                    <input
                                      type="checkbox"
                                      name="commitYn"
                                      value={ruleInfo.commitYn}
                                      checked={
                                        ruleInfo.commitYn === "Y" ? true : false
                                      }
                                      disabled={false}
                                      readOnly={true}
                                    />
                                    <div className="icon" />
                                  </label>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <IntlMessages id="period" />
                              </th>
                              <td colSpan="3">
                                {(ruleInfo.ruleCommitStrDt ||
                                  ruleInfo.ruleCommitEndDt) &&
                                  `${ruleInfo.ruleCommitStrDt ||
                                    ""} ~ ${ruleInfo.ruleCommitEndDt || ""}`}
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <IntlMessages id="day.of.the.week" />
                              </th>
                              <td colSpan="3" style={{ paddingRight: 0 }}>
                                <div className="checkbox__wrap">
                                  {week.map((day, index) => {
                                    return (
                                      <div
                                        className="checkbox checkbox--dark"
                                        key={index}
                                      >
                                        <label>
                                          <input
                                            type="checkbox"
                                            checked={getWeekChecked(day.id)}
                                            readOnly={true}
                                          />
                                          <div className="icon" />
                                          <span>{day.name}</span>
                                        </label>
                                      </div>
                                    );
                                  })}
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <th>
                                <IntlMessages id="time" />
                              </th>
                              <td>
                                {ruleInfo.ruleCommitStrTime &&
                                  `${ruleInfo.ruleCommitStrTime.substring(
                                    0,
                                    2
                                  )}:${ruleInfo.ruleCommitStrTime.substring(
                                    2,
                                    4
                                  )} ~
                                   ${ruleInfo.ruleCommitEndTime.substring(
                                     0,
                                     2
                                   )}:${ruleInfo.ruleCommitEndTime.substring(
                                    2,
                                    4
                                  )}`}
                              </td>
                              <th>
                                <IntlMessages id="rule.group.code" />
                              </th>
                              <td>{ruleInfo.ruleGroupCodeString}</td>
                            </tr>
                            <tr>
                              <th>
                                <IntlMessages id="refer.rule" />
                              </th>
                              <td>{ruleInfo.relshprule}</td>
                              <th>Attack Code</th>
                              <td>{ruleInfo.attackCodeString}</td>
                            </tr>
                            <tr>
                              <th>
                                <IntlMessages id="control.target" />
                              </th>
                              <td>{ruleInfo.cntrlTargt}</td>
                              <th>Severity Code</th>
                              <td>{ruleInfo.severityCodeString}</td>
                            </tr>
                            <tr>
                              <th>Company</th>
                              <td>{ruleInfo.custmNm}</td>
                              <th>
                                <IntlMessages id="report.autoyn" />
                              </th>
                              <td>{ruleInfo.autoReportYn}</td>
                            </tr>
                            <tr>
                              <th>
                                <IntlMessages id="desc" />
                              </th>
                              <td colSpan="3" style={{ height: "100%" }}>
                                {ruleInfo.ruleDesc}
                              </td>
                            </tr>
                            <tr>
                              <th>Comment</th>
                              <td colSpan="3" style={{ height: "100%" }}>
                                {ruleInfo.comment}
                              </td>
                            </tr>
                            <tr>
                              <th>Take Action Description</th>
                              <td colSpan="3" style={{ height: "100%" }}>
                                {ruleInfo.takeActionDescription}
                              </td>
                            </tr>
                            <tr>
                              <th>Take B Action Description</th>
                              <td colSpan="3" style={{ height: "100%" }}>
                                {ruleInfo.takeBActionDescription}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                  <div className="right">
                    <div className="filter-condition">
                      {ruleInfo.ruleType !== "1100" && (
                        <div
                          className="filter-condition__box"
                          style={{ height: "auto" }}
                        >
                          <FilterTree
                            disabled={true}
                            data={ruleInfo.filters}
                            btnVisibled={false}
                          />
                        </div>
                      )}

                      {ruleInfo.ruleType === "1100" && (
                        <React.Fragment>
                          <span className="icon-and" />
                          <div className="component__title">
                            <span>
                              <IntlMessages id="drools.editor" />
                            </span>
                          </div>
                          <div className="component__box">
                            <pre
                              className="form-control change-reason-form"
                              style={{
                                height: "400px",
                                margin: "0",
                                overflowY: "auto"
                              }}
                            >
                              {ruleInfo.droolsCode}
                            </pre>
                          </div>
                        </React.Fragment>
                      )}

                      {ruleInfo.ruleType === "0100" && (
                        <div style={{ display: "block", marginTop: 20 }}>
                          <div className="component__title">
                            <span>
                              <IntlMessages id="threshold.info" />
                            </span>
                          </div>
                          <table className="table table--dark security-event">
                            <tbody>
                              <tr>
                                <th rowSpan="2">Group by Fields</th>
                                <td
                                  colSpan="4"
                                  style={{ borderTop: "1px solid #484b54" }}
                                >
                                  {ruleInfo.thrsldGroupField}
                                </td>
                              </tr>
                              <tr>
                                <th>
                                  <IntlMessages id="duration" />
                                </th>
                                <td>{ruleInfo.thrsldGroupPeriod}</td>
                                <th>Count</th>
                                <td>{ruleInfo.thrsldGroupCnt}</td>
                              </tr>
                              <tr>
                                <th rowSpan="2">Distinct Fields</th>
                                <td colSpan="4">
                                  {ruleInfo.thrsldDstnctField}
                                </td>
                              </tr>
                              <tr>
                                <th>Count</th>
                                <td colSpan="3">{ruleInfo.thrsldDstnctCnt}</td>
                              </tr>
                              <tr>
                                <th>Aggregation</th>
                                <th>
                                  <IntlMessages id="duration" />
                                </th>
                                <td>{ruleInfo.thrsldOprtrPeriod}</td>
                                <th>Operation</th>
                                <td style={{ overflow: "unset" }}>
                                  {ruleInfo.thrsldOprtr}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      )}
                    </div>
                  </div>
                </div>

                {/* <div className="bottom" style={{  display: "inline-block", width : "1000px" }} > */}
                <div className="bottom">
                  <div style={{ marginTop: "20px" }}>
                    <div className="component__title">
                      <span>
                        <IntlMessages id="threat.class" />
                      </span>
                    </div>
                    <TaxonomyTable ruleSeq={ruleInfo.ruleSeq} />
                  </div>
                </div>
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--blue" onClick={onClose}>
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    rule: state.rule.get("rule")
  }),
  dispatch => ({
    ThreatTaxonomyActions: bindActionCreators(threatTaxonomyActions, dispatch),
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(RuleInfoPopup);
