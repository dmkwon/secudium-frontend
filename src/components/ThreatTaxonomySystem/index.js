import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as threatTaxonomyActions from "../../store/modules/threattaxonomy";
import * as ruleActions from "../../store/modules/rule";
import * as api from "../../store/api/threattaxonomy";
import jwtService from "services/jwtService";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import Loading from "../Common/Loading";
import { isValid } from "./../../utils";

import ThreatTaxoTree from "./TaxonomyTree";
import TaxonomyInfo from "./TaxonomyInfo";
import TaxonomyRule from "./TaxonomyRule";
import ConfirmPopup from "./popups/ConfirmPopup";
import AlertPopup from "./popups/AlertPopup";

import "./style.scss";
import { List, Map, fromJS } from "immutable";
import IntlMessages from "util/IntlMessages";

const initTreeData = List([
  Map({
    key: "0",
    name: <IntlMessages id="all" />,
    seq: "0",
    hasSubNode: "Y",
    children: List([])
  })
]);

class ThreatTaxonomySystemComponent extends Component {
  state = {
    validate: false,
    validateMsg: false,
    validateReq: false,
    isDisabled: true,
    disableIsrt: false,
    disableUpdt: true,
    disableDelt: true,
    tpPath: "",
    taxoName: "",
    vulName: "",
    cve: "",
    cpe: "",
    resDtl: "",
    hasSubNode: "",
    originTaxoData: {}, // 업데이트 시, 상위분류 체계 변동 시 변경대상 데이터 저장.
    originTaxoKey: {}, // 업데이트 시, 상위분류 체계 변동 시 원본 셀렉트 키 저장.
    originSubNodes: [], // 상위체계 위치 변동 시, 원본노드 하위체크
    targetSubNodes: [], // 상위체계 위치 변동 시, 대상노드 하위체크
    insertKey: "", // 등록 시 대상노드 (선택) 키 저장,
    updateKey: "", // 수정 시 대상노드 (선택) 키 저장
    insertMod: false,
    updateMod: false,
    deleteMod: false,
    showConfirm: false,
    showAlert: false,
    showRuleInfo: false,
    popupRescrs: {},
    treeData: initTreeData,
    selectedKeys: ["0"],
    expandedKeys: ["0"],

    selectedNode: {}
  };

  componentDidMount() {
    jwtService.on("onChangeLang", () => {
      this.onSelect(this.state.selectedKeys, this.state.selectedNode);
    });

    this.initNode();
  }

  showPopup = (type, title, msg) => {
    if (type === "ALERT") {
      this.setState({
        showAlert: true,
        popupRescrs: {
          title: title,
          message: msg
        }
      });
    } else if (type === "CONFIRM") {
      this.setState({
        showConfirm: true,
        popupRescrs: {
          title: title,
          message: msg
        }
      });
    }
  };

  onSelect = async (selectedKeys, e) => {
    const { ThreatTaxonomyActions, RuleActions, taxoData } = this.props;
    this.setState({
      selectedKeys,
      selectedNode: e
    });
    if (selectedKeys[0] === "0") {
      await ThreatTaxonomyActions.getNodeData(0);
      ThreatTaxonomyActions.clearRuleData();
      const keyword = "";
      const ruleTotal = await RuleActions.getRules({ keyword });
      await ThreatTaxonomyActions.getRuleTotal(ruleTotal.data);

      this.setState({
        taxoName: "",
        vulName: "",
        cve: "",
        cpe: "",
        resDtl: "",
        insertMod: false,
        updateMod: false,
        disableIsrt: false,
        disableUpdt: true,
        disableDelt: true
      });
      return;
    } else if (selectedKeys.length === 0) {
      ThreatTaxonomyActions.clearRuleData();
      this.setState({
        taxoName: "",
        vulName: "",
        cve: "",
        cpe: "",
        resDtl: "",
        disableIsrt: true,
        disableUpdt: true,
        disableDelt: true,
        isDisabled: true
      });
      return;
    } else {
      const thrtSeq = selectedKeys[0].split("/")[1].split("-")[1];
      const resThreat = await ThreatTaxonomyActions.getNodeData(thrtSeq);
      const resData = resThreat.data;
      const resRule = await ThreatTaxonomyActions.getRule(thrtSeq);
      let isrt = false,
        updt = false,
        delt = false;

      if (resData.thrtSeq === 1) {
        isrt = false;
        updt = true;
        delt = true;
      } else if (resData.thrtSeq === 2) {
        isrt = true;
        updt = true;
        delt = true;
      } else if (resData.hasSubNode === "Y") {
        isrt = false;
        updt = false;
        delt = true;
      } else if (resRule.data.length > 0) {
        isrt = false;
        updt = false;
        delt = true;
      } else if (resData.thrtLv === 4) {
        isrt = true;
        updt = false;
        delt = false;
      }

      this.setState({
        tpPath: taxoData.thrtTpPath,
        taxoName: resThreat.data.thrtNm,
        vulName: resThreat.data.vuln,
        cve: resThreat.data.cve,
        cpe: resThreat.data.cpe,
        resDtl: resThreat.data.resDtl,
        hasSubNode: resThreat.data.hasSubNode,
        insertMod: false,
        updateMod: false,
        isDisabled: true,
        disableIsrt: isrt,
        disableUpdt: updt,
        disableDelt: delt,
        validate: false,
        validateMsg: false,
        validateReq: false
      });
    }
  };

  onChangeTaxoInfo = (v, name) => {
    this.setState({ [name]: v });
  };

  onInsertMod = async () => {
    const { ThreatTaxonomyActions } = this.props;

    if (isValid(this.state.taxoName)) {
      ThreatTaxonomyActions.clearRuleData();
      this.setState({
        taxoName: "",
        vulName: "",
        cve: "",
        cpe: "",
        resDtl: "",
        hasSubNode: "",
        insertKey: this.getSelectedKey(),
        insertMod: true,
        updateMod: false,
        deleteMod: false,
        isDisabled: false,
        disableIsrt: false,
        disableUpdt: true,
        disableDelt: true
      });
    } else {
      await ThreatTaxonomyActions.getNodeData(0);
      ThreatTaxonomyActions.clearRuleData();
      this.setState({
        taxoName: "",
        vulName: "",
        cve: "",
        cpe: "",
        resDtl: "",
        hasSubNode: "",
        insertKey: this.getSelectedKey(),
        insertMod: true,
        updateMod: false,
        deleteMod: false,
        isDisabled: false,
        disableIsrt: false,
        disableUpdt: true,
        disableDelt: true
      });
    }
  };

  /** 수정모드 진입 */
  onUpdateMod = async () => {
    if (isValid(this.state.taxoName)) {
      const selectedKey = this.getSelectedKey();
      const thrtSeq = selectedKey.split("/")[1].split("-")[1];
      const res = await api.getNodeData(thrtSeq);
      this.setState({
        insertMod: false,
        updateMod: true,
        deleteMod: false,
        isDisabled: false,
        disableIsrt: true,
        disableUpdt: false,
        disableDelt: true,
        originTaxoData: res.data,
        originTaxoKey: selectedKey,
        updateKey: selectedKey
      });
    }
  };

  onDeleteMod = () => {
    this.setState({ deleteMod: true, insertMod: false, updateMod: false });
    this.showPopup(
      "CONFIRM",
      <IntlMessages id="delete" />,
      <IntlMessages id="do.delete" />
    );
  };

  onSave = () => {
    const { taxoData } = this.props;
    if (this.state.insertMod) {
      if (!isValid(this.state.taxoName)) {
        this.setState({
          validate: true,
          validateMsg: true,
          validateReq: true
        });
        return;
      }
      if (!isValid(taxoData.thrtNm)) {
        this.showPopup(
          "ALERT",
          <IntlMessages id="reg" />,
          <IntlMessages id="no.select.parent" />
        );
        return;
      }
      this.showPopup(
        "CONFIRM",
        <IntlMessages id="reg" />,
        <IntlMessages id="do.register" />
      );
    } else if (this.state.updateMod) {
      if (!isValid(this.state.taxoName)) {
        this.setState({
          validate: true,
          validateMsg: true,
          validateReq: true
        });
        return;
      }
      if (!isValid(taxoData.thrtNm)) {
        return;
      }
      this.showPopup(
        "CONFIRM",
        <IntlMessages id="edit" />,
        <IntlMessages id="do.edit" />
      );
    }
  };

  onCancel = () => {
    this.setState({
      taxoName: "",
      vulName: "",
      cve: "",
      cpe: "",
      resDtl: "",
      hasSubNode: "",
      isDisabled: true,
      disableIsrt: false,
      disableUpdt: true,
      disableDelt: true,
      insertMod: false,
      updateMod: false,
      deleteMod: false,
      validate: false,
      validateMsg: false,
      validateReq: false
    });
  };

  onConfirm = async () => {
    const { ThreatTaxonomyActions, taxoData } = this.props;
    const { insertMod, updateMod, deleteMod, selectedNode } = this.state;

    if (insertMod) {
      const data = {
        tpThrtSeq: taxoData.thrtSeq,
        thrtTpPath:
          taxoData.thrtSeq === 0
            ? ""
            : taxoData.thrtTpPath
            ? `${taxoData.thrtTpPath} > ${taxoData.thrtNm}`
            : `${taxoData.thrtNm}`,
        thrtLv: taxoData.thrtLv,
        thrtNm: this.state.taxoName,
        vuln: this.state.vulName,
        cve: this.state.cve,
        cpe: this.state.cpe,
        resDtl: this.state.resDtl
      };
      this.onInsertExec(data);
    } else if (updateMod) {
      const { originTaxoData } = this.state;

      /** Node Position is changed && Target Node level is below 4(maximum value)  */
      if (originTaxoData.thrtSeq !== taxoData.thrtSeq && taxoData.thrtLv < 4) {
        if (originTaxoData.tpThrtSeq === taxoData.thrtSeq) {
          this.showPopup(
            "ALERT",
            <IntlMessages id="confirm" />,
            <IntlMessages id="move.node.unabled" />
          );
          return;
        }

        // 상위레벨체계 하위 존재 유무 및 분화코드 필요
        const res = await api.getNodeData(originTaxoData.thrtSeq);

        /** Case 1 : Original node contain sub node */
        if (res.data.hasSubNode === "Y") {
          /**
           * Get immediate subnodes for origin and the target node.
           * @originTaxodata : origin node data.
           * @taxodata : target node data.
           *  */
          const origSubNodes = await api.getSubNodes(originTaxoData.thrtSeq);
          const targetSubNodes = await api.getSubNodes(taxoData.thrtSeq);

          /**
           * check for subnode
           * @origSeqs : an Array containing sequence of origin subnodes.
           * @return : [Promise] => returns original subnode.
           */
          const originSeqs = origSubNodes.data;
          const originRx = this.searchOriginSubNode(originSeqs);
          originRx
            .then(originSubNodes => originSubNodes)
            .then(originSubNodes => {
              /**
               * Get immediate target subnodes
               * @thrtSeq : primaryKey for updated node.
               *  */
              // 대상노드에 하위있음
              if (targetSubNodes.data.length !== 0) {
                /**
                 * check for subnode
                 * @targetSeqs : an Array containing sequence of target subnodes.
                 * @return : (void), sets result to [this.state.originSubNodes]
                 */
                const targetSeqs = targetSubNodes.data;
                const targetRx = this.searchTargetSubNode(targetSeqs);
                targetRx
                  .then(targetSubNodes => targetSubNodes)
                  .then(async targetSubNodes => {
                    /**
                     * @originSubNodes 이동 될 노드
                     * @targetSubNodes 대상 노드
                     * 비교하여 depth 4 초과 시 업데이트 불가.
                     */

                    const originMax = originSubNodes.reduce((acc, value) => {
                      let max = value.thrtLv > acc.thrtLv ? value : acc;
                      return max;
                    });

                    let originLv = originTaxoData.thrtLv,
                      originMaxLv = originMax.thrtLv,
                      targetLv = taxoData.thrtLv;

                    const updateDepth = originMaxLv - originLv;
                    const resultLevel = targetLv + updateDepth;

                    if (resultLevel > 4) {
                      this.showPopup(
                        "ALERT",
                        <IntlMessages id="confirm" />,
                        <IntlMessages id="no.process.exceed" />
                      );
                      return;
                    } else {
                      const data = {
                        tpThrtSeq: taxoData.thrtSeq, // 이동대상 노드 seq
                        thrtLv: taxoData.thrtLv + 1,
                        thrtTpPath:
                          taxoData.thrtSeq === 0
                            ? ""
                            : taxoData.thrtTpPath
                            ? `${taxoData.thrtTpPath} > ${taxoData.thrtNm}`
                            : `${taxoData.thrtNm}`,
                        thrtSeq: originTaxoData.thrtSeq, // 수정 될 노드 seq
                        thrtNm: this.state.taxoName,
                        vuln: this.state.vulName,
                        cve: this.state.cve,
                        cpe: this.state.cpe,
                        resDtl: this.state.resDtl
                      };
                      this.onUpdateNodeHierExec(data);

                      //하위노드 레벨 & TpPath 업데이트
                      let levelDiff = targetLv + 1 - originLv;

                      let listByThrtLv = [];
                      for (let idx = 1; idx <= 4; idx++) {
                        listByThrtLv = originSubNodes.filter(
                          node => node.thrtLv === idx
                        );

                        let pathByThrtLv = "";
                        listByThrtLv = listByThrtLv.map(node => {
                          pathByThrtLv =
                            taxoData.thrtSeq === 0 ? `` : taxoData.thrtNm;
                          let newTpPath = node.thrtTpPath.split(">");
                          newTpPath = newTpPath.map(e => e.trim());
                          let updateTpPath = "";

                          /** 하위로 이동 */
                          if (levelDiff > 0) {
                            newTpPath.shift();
                            newTpPath.unshift(originTaxoData.thrtNm);
                            newTpPath.unshift(pathByThrtLv);
                            newTpPath = newTpPath.filter(e => e !== "");
                            updateTpPath = newTpPath.join(" > ");
                            // console.log(`updateTpPath:  ${updateTpPath} Lv: ${node.thrtLv}`);

                            /** 상위로 이동 */
                          } else if (levelDiff < 0) {
                            newTpPath.shift();
                            newTpPath.unshift(pathByThrtLv);
                            newTpPath = newTpPath.filter(e => e !== "");
                            updateTpPath = newTpPath.join(" > ");
                            // console.log(`updateTpPath:  ${updateTpPath} Lv: ${node.thrtLv}`);

                            /** 동일 레벨 */
                          } else {
                            newTpPath.shift();
                            newTpPath.unshift(pathByThrtLv);
                            newTpPath = newTpPath.filter(e => e !== "");
                            updateTpPath = newTpPath.join(" > ");
                            // console.log(`updateTpPath:  ${updateTpPath} Lv: ${node.thrtLv}`);
                          }
                          newTpPath = [];

                          return {
                            thrtSeq: node.thrtSeq,
                            thrtLv: node.thrtLv + levelDiff,
                            thrtTpPath: updateTpPath
                          };
                        });
                        if (listByThrtLv.length !== 0) {
                          await api.updateSubNodes(listByThrtLv);
                        }
                      }
                    }
                  });
              } else {
                /**
                 * check for subnode
                 * @origSeqs : an Array containing sequence of origin subnodes.
                 * @return : [Promise] => returns original subnode.
                 */
                const originSeqs = origSubNodes.data;
                const originRx = this.searchOriginSubNode(originSeqs);
                originRx.then(async originSubNodes => {
                  const originMax = originSubNodes.reduce((acc, value) => {
                    let max = value.thrtLv > acc.thrtLv ? value : acc;
                    return max;
                  });

                  let originLv = originTaxoData.thrtLv,
                    originMaxLv = originMax.thrtLv,
                    targetLv = taxoData.thrtLv;
                  const updateDepth = originMaxLv - originLv;
                  const resultLevel = targetLv + updateDepth;

                  if (resultLevel > 4) {
                    this.showPopup(
                      "ALERT",
                      <IntlMessages id="confirm" />,
                      <IntlMessages id="no.process.exceed" />
                    );
                    return;
                  } else {
                  }

                  //수정노드
                  const data = {
                    tpThrtSeq: taxoData.thrtSeq, // 이동대상 노드 seq
                    thrtLv: taxoData.thrtLv + 1,
                    thrtTpPath:
                      taxoData.thrtSeq === 0
                        ? ""
                        : taxoData.thrtTpPath
                        ? `${taxoData.thrtTpPath} > ${taxoData.thrtNm}`
                        : `${taxoData.thrtNm}`,
                    thrtSeq: originTaxoData.thrtSeq, // 수정 될 노드 seq
                    thrtNm: this.state.taxoName,
                    vuln: this.state.vulName,
                    cve: this.state.cve,
                    cpe: this.state.cpe,
                    resDtl: this.state.resDtl
                  };
                  this.onUpdateNodeHierExec(data);

                  //하위노드 레벨 & TpPath 업데이트
                  let levelDiff = targetLv + 1 - originLv;

                  let listByThrtLv = [];
                  for (let idx = 1; idx <= 4; idx++) {
                    listByThrtLv = originSubNodes.filter(
                      node => node.thrtLv === idx
                    );

                    let pathByThrtLv = "";
                    listByThrtLv = listByThrtLv.map(node => {
                      pathByThrtLv =
                        taxoData.thrtSeq === 0 ? `` : taxoData.thrtNm;

                      let newTpPath = node.thrtTpPath.split(">");
                      newTpPath = newTpPath.map(e => e.trim());
                      let updateTpPath = "";

                      /** 하위로 이동 */
                      if (levelDiff > 0) {
                        newTpPath.shift();
                        newTpPath.unshift(originTaxoData.thrtNm);
                        newTpPath.unshift(pathByThrtLv);
                        newTpPath = newTpPath.filter(e => e !== "");
                        updateTpPath = newTpPath.join(" > ");
                        // console.log(`updateTpPath:  ${updateTpPath} Lv: ${node.thrtLv}`);

                        /** 상위로 이동 */
                      } else if (levelDiff < 0) {
                        newTpPath.shift();
                        newTpPath.unshift(pathByThrtLv);
                        newTpPath = newTpPath.filter(e => e !== "");
                        updateTpPath = newTpPath.join(" > ");
                        // console.log(`updateTpPath:  ${updateTpPath} Lv: ${node.thrtLv}`);

                        /** 동일레벨 */
                      } else {
                        newTpPath.shift();
                        newTpPath.unshift(pathByThrtLv);
                        newTpPath = newTpPath.filter(e => e !== "");
                        updateTpPath = newTpPath.join(" > ");
                        // console.log(`updateTpPath:  ${updateTpPath} Lv: ${node.thrtLv}`);
                      }
                      newTpPath = [];

                      return {
                        thrtSeq: node.thrtSeq,
                        thrtLv: node.thrtLv + levelDiff,
                        thrtTpPath: updateTpPath
                      };
                    });
                    if (listByThrtLv.length !== 0) {
                      await api.updateSubNodes(listByThrtLv);
                    }
                  }
                });
              }
            });

          /** Case 2 : Original node does not contain sub nodes */
        } else {
          const data = {
            tpThrtSeq: taxoData.thrtSeq, // 이동대상 노드 seq
            thrtLv: taxoData.thrtLv + 1,
            thrtTpPath:
              taxoData.thrtSeq === 0
                ? ""
                : taxoData.thrtTpPath
                ? `${taxoData.thrtTpPath} > ${taxoData.thrtNm}`
                : `${taxoData.thrtNm}`,
            thrtSeq: originTaxoData.thrtSeq, // 수정 될 노드 seq
            thrtNm: this.state.taxoName,
            vuln: this.state.vulName,
            cve: this.state.cve,
            cpe: this.state.cpe,
            resDtl: this.state.resDtl
          };
          this.onUpdateNodeHierExec(data);
        }
      } else {
        /** 상위분류체계 변동 없을 시 */
        const data = {
          thrtSeq: taxoData.thrtSeq,
          thrtLv: taxoData.thrtLv,
          thrtTpPath: taxoData.thrtTpPath,
          thrtNm: this.state.taxoName,
          vuln: this.state.vulName,
          cve: this.state.cve,
          cpe: this.state.cpe,
          resDtl: this.state.resDtl
        };
        this.onUpdateNodeExec(data, originTaxoData);
      }
    } else if (deleteMod) {
      try {
        /** DB 데이터 상태 갱신 */
        const data = { thrtSeq: taxoData.thrtSeq };
        await ThreatTaxonomyActions.deleteTaxo(data);

        /** 트리구조에서 값 삭제 */
        this.onDeleteExec();
        this.onCloseConfirm();
        await ThreatTaxonomyActions.getNodeData(0);
      } catch (error) {
        console.log(error);
      }
    }
  };

  /**
   * find all subnodes related to updated node
   * @seqs : JSON array containing sequence. {  "thrtSeq" : el.thrtSeq , "thrtLv" : el.thrtLv }
   * @target : "O" = original node, else = target node
   */
  searchOriginSubNode = async origSeqs => {
    const startLv = origSeqs[0].thrtLv;
    const subNodesChildren = await api.checkSubNodes(origSeqs);
    let resultNode = [...origSeqs];
    return new Promise(async resolve => {
      if (subNodesChildren.data.length) {
        let subSeqs = subNodesChildren.data;
        resultNode = [...resultNode, ...subSeqs];
        for (let index = startLv; index < 4; index++) {
          if (subSeqs.length !== 0) {
            const nodes = await api.checkSubNodes(subSeqs);
            resultNode = [...resultNode, ...nodes.data];
            subSeqs = nodes.data;
          }
        }
      } else {
      }
      resolve(resultNode);
    });
  };

  searchTargetSubNode = async targetSeqs => {
    const startLv = targetSeqs[0].thrtLv;
    const subNodesChildren = await api.checkSubNodes(targetSeqs);
    let resultNode = [...targetSeqs];

    return new Promise(async resolve => {
      if (subNodesChildren.data.length) {
        let subSeqs = subNodesChildren.data;
        resultNode = [...resultNode, ...subSeqs];
        for (let index = startLv; index < 4; index++) {
          if (subSeqs.length !== 0) {
            const nodes = await api.checkSubNodes(subSeqs);
            resultNode = [...resultNode, ...nodes.data];
            subSeqs = nodes.data;
          }
        }
      } else {
      }
      resolve(resultNode);
    });
  };

  onInsertExec = async data => {
    const { ThreatTaxonomyActions } = this.props;
    try {
      const selectedKey = this.state.insertKey;

      /** Create variables for Tree manipulation. */
      let originEvtKey, originEvtKeyPath;

      /** if uppermost treeNode has been selected */
      if (selectedKey === "0") {
        originEvtKey = selectedKey;
        originEvtKeyPath = ["0"];

        /** DB 입력 */
        await ThreatTaxonomyActions.insertTaxo(data);

        const res = await api.getSubNodes(0);
        let thrtArr = [],
          maxSeq = 0;
        for (let i = 0; i < res.data.length; i++) {
          thrtArr.push(res.data[i].thrtSeq);
          maxSeq = thrtArr.reduce((a, b) => Math.max(a, b));
        }

        const resAdd = await api.getNodeData(maxSeq);
        const nodeName = resAdd.data.thrtNm,
          nodeKey = `${resAdd.data.tpThrtSeq}-${resAdd.data.thrtSeq}`;

        /** Create KeyPath for original node. */
        const keyPath = this.makeSelector(originEvtKeyPath);
        let nodeList = this.state.treeData.getIn([...keyPath]).toJS();

        let bin = nodeList.pop();
        let [binKey, binSeq] = bin.key.split("/");

        let newKey = `${binKey}/${nodeKey}`,
          newElement = Map({
            key: newKey,
            name: nodeName,
            seq: nodeKey,
            hasSubNode: "N",
            children: List([])
          }).toJS();

        let [binEvtParent, binEvtChild] = binKey.split("-"),
          binNewKey = `${binEvtParent}-${parseInt(binEvtChild) + 1}/${binSeq}`;
        bin.key = binNewKey;

        nodeList.push(newElement);
        nodeList.push(bin);
        nodeList = fromJS(nodeList);
        this.setState({
          treeData: this.state.treeData.setIn([...keyPath], nodeList)
        });
      } else {
        originEvtKey = selectedKey.split("/")[0];
        originEvtKeyPath = originEvtKey.split("-");

        /** Create KeyPath for original node. */
        let keyPath = this.makeSelector(originEvtKey.split("-")),
          keyPathParent = this.makeSelector(originEvtKey.split("-"), true);
        // keyPathGparent = this.makeSelector(
        //   originEvtKey.split('-'),
        //   false,
        //   true
        // );

        /** DB 입력 */
        await ThreatTaxonomyActions.insertTaxo(data);

        /** 추가 된 값 Load */
        const res = await api.getSubNodes(data.tpThrtSeq);
        let thrtArr = [],
          maxSeq = 0;
        for (let i = 0; i < res.data.length; i++) {
          thrtArr.push(res.data[i].thrtSeq);
          maxSeq = thrtArr.reduce((a, b) => Math.max(a, b));
        }

        /** 추가 요소 생성. */
        const resAdd = await api.getNodeData(maxSeq);
        const nodeName = resAdd.data.thrtNm,
          nodeKey = `${resAdd.data.tpThrtSeq}-${resAdd.data.thrtSeq}`,
          newElement = Map({
            key: `${originEvtKeyPath}/${nodeKey}`,
            name: nodeName,
            seq: nodeKey,
            hasSubNode: "N",
            children: List([])
          }).toJS();

        /** Create KeyPath for original node. */
        let nodeList = this.state.treeData.getIn([...keyPath]).toJS();

        nodeList.push(newElement);
        nodeList.map((node, idx) => {
          let dbKey = node.key.split("/")[1];
          node.key = `${originEvtKeyPath.join("-")}-${idx}/${dbKey}`;
          return node;
        });
        nodeList = fromJS(nodeList);

        /** change parent node property if it was empty, otherwise just update */
        if (nodeList.length === 0) {
          this.setState(
            {
              treeData: this.state.treeData.setIn([...keyPath], nodeList)
            },
            () => {
              this.setState({
                treeData: this.state.treeData.setIn(
                  [...keyPathParent],
                  this.state.treeData
                    .getIn([...keyPathParent])
                    .set("hasSubNode", "Y")
                )
              });
            }
          );
        } else {
          this.setState({
            treeData: this.state.treeData.setIn([...keyPath], nodeList)
          });
        }
      }

      this.onCloseConfirm();
      this.onCancel();
      ThreatTaxonomyActions.clearRuleData();
      return Promise.resolve();
    } catch (error) {
      console.log(error);
    }
  };

  /**
   * @data            : Data for update operation.
   * @originTaxoData  : Node's origin information .
   */
  onUpdateNodeHierExec = async data => {
    const { ThreatTaxonomyActions } = this.props;
    const { treeData, originTaxoKey } = this.state;

    try {
      await ThreatTaxonomyActions.updateTaxoHier(data);
    } catch (e) {
      console.log(e);
    }

    //state
    // originTaxoData  : {}, // 업데이트 시, 상위레벨체계 변동 시 변경대상 데이터 저장.
    // originTaxoKey   : {}, // 업데이트 시, 상위레벨체계 변동 시 트리구조 selectedKey/nodeKey 저장.

    /** Create variables for Tree manipulation. */
    let originEvtKey, originEvtKeyPath;
    originEvtKey = originTaxoKey.split("/")[0];
    originEvtKeyPath = originEvtKey.split("-");

    /** Create KeyPath for original node. */
    let keyPath = this.makeSelector(originEvtKey.split("-"), true),
      keyPathParent = this.makeSelector(originEvtKey.split("-"), false, true);
    // keyPathSub = this.makeSelector(originEvtKey.split('-'));

    /** Create variables for Tree manipulation. */
    let targetEvtKey, targetEvtKeyPath, keyPathTargetSub, keyPathTarget;

    const updateKey = this.state.updateKey;
    if (updateKey === "0") {
      targetEvtKey = "0";
      targetEvtKeyPath = ["0"];
    } else {
      targetEvtKey = updateKey.split("/")[0];
      targetEvtKeyPath = targetEvtKey.split("-");
    }

    /** Create KeyPath for original node. */
    keyPathTargetSub = this.makeSelector(targetEvtKey.split("-"));
    keyPathTarget = this.makeSelector(targetEvtKey.split("-"), true);

    /** Delete Original Node and re-assign correct index to remaining nodes. */
    const promise = new Promise(resolve => {
      /** Convert origin node to plain JS object. */
      let originNode = treeData.getIn([...keyPath]).toJS();

      /** Extract transferred Node */
      let txNode = originNode.filter(node => node.key === originTaxoKey);

      /** Exclude origin node */
      originNode = originNode.filter(node => node.key !== originTaxoKey);

      /** If there are no remaining elements in the parent node */
      if (originNode.length === 0) {
        /** Target Parent Node */
        let parentSeq = originEvtKeyPath.slice(-2, -1);

        /** Set parent property */
        let parentNode = treeData
          .getIn([...keyPathParent, parentSeq[0]])
          .toJS();
        parentNode.hasSubNode = "N";
        parentNode.children = List([]);
        parentNode = fromJS(parentNode);

        /** Update Tree Structure */
        this.setState({
          treeData: this.state.treeData.setIn(
            [...keyPathParent, parentSeq[0]],
            parentNode
          )
        });
      }

      /** Re-assign index to remaining nodes in correct order*/
      originNode = originNode.map((node, idx) => {
        const dbKey = node.key.split("/")[1];
        let evtKey = originTaxoKey.split("/")[0];
        evtKey = evtKey.split("-");
        evtKey.pop();
        let newKey = `${evtKey.join("-")}-${idx}/${dbKey}`;
        node.key = newKey;
        return node;
      });

      /** Convert plain JS object into Immutable Object */
      let newList = fromJS(originNode);

      /** Update Tree Structure */
      this.setState(
        {
          treeData: this.state.treeData.setIn([...keyPath], newList)
        },
        () => {
          /** chain treeData for Synchronicity */
          let promiseArr = [this.state.treeData, txNode];
          resolve(promiseArr);
        }
      );
    });

    promise.then(async promiseArr => {
      let [chainedTreeData, txNode] = [...promiseArr];

      /** Convert target node to plain JS object. */
      let targetSeq = targetEvtKeyPath.pop();
      let targetNode = chainedTreeData.getIn([...keyPathTarget, targetSeq[0]]);

      if (targetNode === undefined) {
        return;
      }

      targetNode = targetNode.toJS();
      /** Check if subnodes exists in target node */
      let targetNodeSub = this.state.treeData
        .getIn([...keyPathTargetSub])
        .toJS();

      targetNodeSub.push(txNode[0]);
      targetNodeSub.map((node, idx) => {
        let newDbKey = node.key.split("/")[1];
        let newKey = `${targetEvtKey}-${idx}/${newDbKey}`;
        return (node.key = newKey);
      });
      targetNodeSub = fromJS(targetNodeSub);

      if (targetNode.hasSubNode === "N") {
        /** Set parent property */
        targetNode.hasSubNode = "Y";
        targetNode = fromJS(targetNode);
        this.setState(
          {
            treeData: this.state.treeData.setIn(
              [...keyPathTarget, targetSeq[0]],
              targetNode
            )
          },
          () => {
            this.setState({
              treeData: this.state.treeData.setIn(
                [...keyPathTargetSub],
                targetNodeSub
              )
            });
          }
        );
      } else {
        if (targetNodeSub.size === 1) {
          /** 첫 로딩 안되었을 시 예외처리. */
        } else {
          this.setState({
            treeData: this.state.treeData.setIn(
              [...keyPathTargetSub],
              targetNodeSub
            )
          });
        }
      }
    });
    this.onCloseConfirm();
    this.onCancel(); // 모드 초기화
    ThreatTaxonomyActions.clearRuleData(); /// 룰 초기화
  };

  onUpdateNodeExec = async data => {
    const { ThreatTaxonomyActions, taxoData } = this.props;
    try {
      await ThreatTaxonomyActions.updateTaxo(data);
      const res = await ThreatTaxonomyActions.getNodeData(taxoData.thrtSeq);
      const selectedKey = this.state.updateKey;

      let exNodeKey = selectedKey.split("/")[0].split("-");
      exNodeKey.pop();
      const selector = this.makeSelector(exNodeKey);
      let nodeSeq = selectedKey.split("/")[0].split("-");
      nodeSeq = nodeSeq[nodeSeq.length - 1];

      console.log(
        "children",
        this.state.treeData.getIn([...selector, nodeSeq]).get("children")
      );
      this.setState({
        treeData: this.state.treeData.updateIn([...selector, nodeSeq], () =>
          Map({
            key: selectedKey,
            name: res.data.thrtNm,
            hasSubNode: res.data.hasSubNode,
            children: this.state.treeData
              .getIn([...selector, nodeSeq])
              .get(
                "children"
              ) /** 하위노드 있는 상태에서 업데이트 시 children 유지 */
          })
        )
      });
      this.onCloseConfirm();
      this.onCancel(); // 모드 초기화
      ThreatTaxonomyActions.clearRuleData(); /// 룰 초기화
    } catch (error) {
      console.log(error);
    }
  };

  onCloseConfirm = () => {
    this.setState({
      showConfirm: false,
      showAlert: false,
      showRuleInfo: false
    });
  };

  initNode = async () => {
    try {
      const res = await api.getSubNodes(0);
      for (let i = 0; i < res.data.length; i++) {
        let nodeKey = `${res.data[i].tpThrtSeq}-${res.data[i].thrtSeq}`;
        this.addInitNode(res.data[i].thrtNm, nodeKey, res.data[i].hasSubNode);
      }
      return Promise.resolve();
    } catch (error) {
      console.log(error);
    }
  };

  addInitNode = (nodeName, nodeKey, hasSubNode) => {
    const selectedKey = "0",
      selector = this.makeSelector(selectedKey.split("-")),
      nodeSeq = this.getChildrenCnt(selector),
      key = `${selectedKey}-${nodeSeq}/${nodeKey}`;

    this.setState({
      treeData: this.state.treeData.setIn(
        [...selector, nodeSeq],
        this.getNewNode(key, nodeName, nodeKey, hasSubNode, false)
      )
    });
  };

  //하위노드 추가
  addSubNode = (nodeName, nodeKey, hasSubNode, selectedKey) => {
    const exNodeKey = selectedKey.split("/")[0]; // nodeKey 분리.
    const selector = this.makeSelector(exNodeKey.split("-"));
    const nodeSeq = this.getChildrenCnt(selector);
    const key = `${exNodeKey}-${nodeSeq}/${nodeKey}`;

    this.setState({
      treeData: this.state.treeData.setIn(
        [...selector, nodeSeq],
        this.getNewNode(key, nodeName, nodeKey, hasSubNode, false)
      )
    });
  };

  //신규노드
  getNewNode = (key, name, seq, hasSubNode) =>
    Map({ key, name, seq, hasSubNode, children: List([]) });

  //현재 (선택)된 노드 키
  getSelectedKey = () => this.state.selectedKeys[0];
  getExpandedKey = () => this.state.expandedKeys[0];

  /**
   * @selectParent === true => 대상노드 동일레벨, false => 대상노드 하위레벨
   * @selectGrandParent === true -> 대상노드 상위레벨
   */
  makeSelector = (keys, selectParent = false, selectGrandParent = false) => {
    let selector = [];

    for (let i = 0, length = keys.length; i < length; i++) {
      if (i === length - 2 && selectGrandParent) {
        break;
      }
      if (i === length - 1 && selectParent) {
        break;
      }
      const key = Number(keys[i]);
      selector.push(key);
      selector.push("children");
    }
    return selector;
  };

  //selector로 자식노드의 갯수 반환
  getChildrenCnt = selector => {
    const target = this.state.treeData.getIn([...selector]);
    return target ? target.size : 0;
  };

  loadData = async treeNode => {
    const targetKey = treeNode.props.eventKey;
    const selectedKey = this.state.expandedKeys[
      this.state.expandedKeys.length - 1
    ];

    if (targetKey === "0") {
      return;
    }

    const selectEvtKey = selectedKey.split("/")[0],
      selectKeyPath = selectEvtKey.split("-");
    let keyPath = this.makeSelector(selectKeyPath),
      nodeList = this.state.treeData.getIn([...keyPath]).toJS();

    if (nodeList.length !== 0) {
      return;
    }

    const thrtSeq = targetKey.split("/")[1].split("-")[1];
    try {
      const res = await api.getSubNodes(thrtSeq);
      for (let i = 0; i < res.data.length; i++) {
        const nodeKey = `${res.data[i].tpThrtSeq}-${res.data[i].thrtSeq}`;
        this.addSubNode(
          res.data[i].thrtNm,
          nodeKey,
          res.data[i].hasSubNode,
          selectedKey
        );
      }
      return Promise.resolve();
    } catch (error) {
      console.log(error);
    }
  };

  onExpand = (expandedKeys, e) => {
    const selectedKeys = [];
    selectedKeys.push(e.node.props.eventKey);

    if (this.state.updateMod) {
      // showPopup();
      // return;
    }
    this.setState({
      expandedKeys
    });
    if (expandedKeys.length === 0) {
      return;
    }
  };

  //노드삭제
  onDeleteExec = async () => {
    /** Create variables for Tree manipulation. */
    const deleteKey = this.getSelectedKey(),
      deleteEvtKey = deleteKey.split("/")[0],
      deleteEvtKeyPath = deleteEvtKey.split("-");

    /** Create KeyPath for original node. */
    let keyPathParent = this.makeSelector(deleteEvtKeyPath, true),
      keyPathGparent = this.makeSelector(deleteEvtKeyPath, false, true);

    let nodeList = this.state.treeData.getIn([...keyPathParent]).toJS();

    nodeList = nodeList.filter(node => node.key !== deleteKey);
    nodeList = fromJS(
      nodeList.map((node, idx) => {
        let evtKey = node.key.split("/")[0];
        evtKey = evtKey.split("-");
        evtKey.pop();
        let newKey = `${evtKey.join("-")}-${idx}/${node.key.split("/")[1]}`;
        node.key = newKey;
        return node;
      })
    );

    if (nodeList.size === 0) {
      let targetKey = deleteEvtKeyPath.slice(-2, -1);
      this.setState(
        {
          treeData: this.state.treeData.setIn(
            [...keyPathGparent, ...targetKey],
            this.state.treeData
              .getIn([...keyPathGparent, ...targetKey])
              .set("hasSubNode", "N")
          )
        },
        () => {
          this.setState({
            treeData: this.state.treeData.setIn([...keyPathParent], nodeList),
            expandedKeys: this.state.expandedKeys.filter(
              key => key !== deleteKey
            )
          });
        }
      );
    } else {
      this.setState({
        treeData: this.state.treeData.setIn([...keyPathParent], nodeList),
        expandedKeys: this.state.expandedKeys.filter(key => key !== deleteKey)
      });
    }

    this.onCancel(); // 모드 초기화
  };

  setStateRule = () => {
    this.setState({
      showRuleInfo: true
    });
  };

  setStateSelectKey = async (selectedKeys, e) => {
    const { ThreatTaxonomyActions } = this.props;

    /** (수정) 상위레벨체계 (선택)
     * @selectedKeys :
     * @selectedNode :
     * @targetTaxo   : selectedKeys[0], 이동대상노드 // 팝업 (선택) 키 개별저장
     */
    if (this.state.updateMod) {
      this.setState({
        // selectedKeys,
        // selectedNode: e.node.props,
        updateKey: selectedKeys[0]
      });

      if (selectedKeys.length === 0) {
        return;
      }

      if (selectedKeys[0] === "0") {
        await ThreatTaxonomyActions.getNodeData(0);
        return;
      } else {
        try {
          const thrtSeq = selectedKeys[0].split("/")[1].split("-")[1];
          const res = await api.getNodeData(thrtSeq);
          if (res.data.thrtLv === 4) {
            this.showPopup(
              "ALERT",
              <IntlMessages id="edit" />,
              <IntlMessages id="cannot.move.lower" />
            );
            return;
          } else {
            await ThreatTaxonomyActions.getNodeData(thrtSeq);
          }
          return Promise.resolve();
        } catch (error) {
          console.log(error);
        }
      }

      /** (등록) 상위레벨체계 (선택) */
    } else {
      if (selectedKeys.length === 0) {
        return;
      }

      if (selectedKeys[0] === "0") {
        await ThreatTaxonomyActions.getNodeData(0);
        return;
      } else {
        try {
          const thrtSeq = selectedKeys[0].split("/")[1].split("-")[1];
          if (thrtSeq === "2") {
            return;
          } else {
            const res = await api.getNodeData(thrtSeq);
            if (res.data.thrtLv === 4) {
              this.showPopup(
                "ALERT",
                <IntlMessages id="reg" />,
                <IntlMessages id="cannot.move.lower" />
              );
              return;
            } else {
              await ThreatTaxonomyActions.getNodeData(thrtSeq);
            }
          }
          return Promise.resolve();
        } catch (error) {
          console.log(error);
        }
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper threat-class-system">
          <LeftMenu />
          <div
            className="component"
            style={{ width: "310px", marginRight: "20px" }}
          >
            <div
              className="component__title"
              style={{
                paddingBottom: "24px",
                borderBottom: "1px solid #484b54",
                marginBottom: "10px"
              }}
            >
              <span>
                <IntlMessages id="threat.class" />
              </span>
            </div>
            <div className="btns" style={{ borderBottom: "1px solid #484b54" }}>
              <button
                className="btn"
                disabled={this.state.disableIsrt}
                onClick={() => this.onInsertMod()}
                style={{
                  marginRight: 5,
                  marginBottom: 10,
                  float: "left",
                  width: "30%"
                }}
              >
                <IntlMessages id="reg" />
              </button>
              <button
                className="btn"
                disabled={this.state.disableUpdt}
                onClick={() => this.onUpdateMod()}
                style={{ marginLeft: 5, marginBottom: 10, width: "30%" }}
              >
                <IntlMessages id="edit" />
              </button>
              <button
                className="btn"
                disabled={this.state.disableDelt}
                onClick={() => this.onDeleteMod()}
                style={{
                  marginRight: 5,
                  marginBottom: 10,
                  float: "right",
                  width: "30%"
                }}
              >
                <IntlMessages id="delete" />
              </button>
            </div>
            <ThreatTaxoTree
              onSelect={this.onSelect}
              onExpand={this.onExpand}
              loadData={this.loadData}
              state={this.state}
            />
          </div>

          <div className="component-right">
            <div className="component-top">
              <div className="component-top__left">
                <TaxonomyInfo
                  state={this.state}
                  onChange={this.onChangeTaxoInfo}
                  setStateSelectKey={this.setStateSelectKey}
                  onSave={this.onSave}
                  onCancel={this.onCancel}
                  isDisabled={this.state.isDisabled}
                  updateMod={this.state.updateMod}
                  insertMod={this.state.insertMod}
                />
              </div>
              <div className="component component-top__right">
                <TaxonomyRule
                  parentState={this.state}
                  setStateRule={this.setStateRule}
                  onCloseConfirm={this.onCloseConfirm}
                />
              </div>
            </div>
          </div>
        </div>
        {this.state.showConfirm && (
          <ConfirmPopup
            resource={this.state.popupRescrs}
            onClose={() => this.onCloseConfirm()}
            onConfirm={() => this.onConfirm()}
          />
        )}
        {this.state.showAlert && (
          <AlertPopup
            resource={this.state.popupRescrs}
            onClose={() => this.onCloseConfirm()}
          />
        )}
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    taxoData: state.threattaxonomy.get("taxoData"),
    ruleData: state.threattaxonomy.get("ruleData"),
    rules: state.rule.get("rules"),
    loading: state.threattaxonomy.get("loading")
  }),
  dispatch => ({
    ThreatTaxonomyActions: bindActionCreators(threatTaxonomyActions, dispatch),
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(ThreatTaxonomySystemComponent);
