import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as threatTaxonomyActions from "../../store/modules/threattaxonomy";

import TreeSelectPopup from "./popups/TreeSelectPopup";
import ReactTooltip from "react-tooltip";

import { Textbox, Textarea } from "react-inputs-validation";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class TaxonomyInfo extends Component {
  state = {
    thrtTpPath: "",
    isShowTree: false
  };

  treeSelect = () => {
    this.setState({ isShowTree: true });
  };

  onClose = () => {
    this.setState({ isShowTree: false });
  };

  onConfirm = () => {
    this.setState({ isShowTree: false });
  };

  onSelect = async (selectedKeys, e) => {
    const { ThreatTaxonomyActions } = this.props;
    if (selectedKeys[0] === "0" || selectedKeys.length === 0) {
      return;
    } else {
      const thrtSeq = selectedKeys[0].split("-")[1];
      await ThreatTaxonomyActions.getNodeData(thrtSeq);
    }
  };

  render() {
    const {
      state,
      onSave,
      onCancel,
      onChange,
      isDisabled,
      taxoData,
      insertMod,
      updateMod,
      setStateSelectKey
    } = this.props;

    return (
      <div className="component">
        <div className="component__title">
          <span>
            <IntlMessages id="class.system.info" />
          </span>
        </div>
        <table className="table table--dark table--info">
          <colgroup>
            <col width="150px" />
            <col />
          </colgroup>
          <tbody>
            <tr style={{ display: "none" }}>
              <th></th>
              <td></td>
            </tr>
            <tr>
              <th>
                {isDisabled ? "" : <span className="font--red">*</span>}
                <IntlMessages id="top.class.system" />
                {insertMod || updateMod ? (
                  <button
                    onClick={() => this.treeSelect()}
                    className="btn btn-icon"
                    style={{ marginLeft: 5 }}
                  >
                    <img src="/images/common/ic_search.png" alt="ic_search" />
                  </button>
                ) : (
                  ""
                )}
              </th>
              <td>
                <span data-tip data-for="taxoPath">
                  {taxoData.thrtSeq === 0 ? (
                    <IntlMessages id="all" />
                  ) : taxoData.thrtTpPath ? (
                    <FormattedMessage id="all">
                      {all =>
                        all + ` > ${taxoData.thrtTpPath} > ${taxoData.thrtNm}`
                      }
                    </FormattedMessage>
                  ) : (
                    <FormattedMessage id="all">
                      {all => all + ` > ${taxoData.thrtNm}`}
                    </FormattedMessage>
                  )}
                </span>
              </td>
            </tr>
            <tr>
              <th>
                {isDisabled ? "" : <span className="font--red">*</span>}
                <IntlMessages id="class.system.name" />
              </th>
              <td>
                <Textbox
                  id="taxoName"
                  name="taxoName"
                  type="text"
                  maxLength={200}
                  value={state.taxoName || ""}
                  onChange={v => onChange(v, "taxoName")}
                  onBlur={e => {}}
                  validate={state.validate}
                  validationOption={{
                    showMsg: state.validateMsg,
                    required: state.validateReq,
                    locale: this.props.language
                  }}
                  classNameInput="form-control"
                  customStyleInput={{ color: "white" }}
                  disabled={isDisabled}
                />
              </td>
            </tr>
            <tr>
              <th>
                <IntlMessages id="vulnerability" />
              </th>
              <td>
                <Textarea
                  id="vulName"
                  name="vulName"
                  type="text"
                  maxLength={4000}
                  value={state.vulName || ""}
                  onChange={v => onChange(v, "vulName")}
                  onBlur={e => {}}
                  classNameInput="form-control"
                  customStyleInput={{ height: "200px", color: "white" }}
                  disabled={isDisabled}
                  validationOption={{ required: false }}
                />
              </td>
            </tr>
            <tr>
              <th>CVE</th>
              <td>
                <Textbox
                  id="cve"
                  name="cve"
                  type="text"
                  maxLength={20}
                  value={state.cve || ""}
                  onChange={v => onChange(v, "cve")}
                  onBlur={e => {}}
                  classNameInput="form-control"
                  customStyleInput={{ height: "50px", color: "white" }}
                  disabled={isDisabled}
                  validationOption={{ required: false }}
                />
              </td>
            </tr>
            <tr>
              <th>CPE</th>
              <td>
                <Textbox
                  id="cpe"
                  name="cpe"
                  type="text"
                  maxLength={100}
                  value={state.cpe || ""}
                  onChange={v => onChange(v, "cpe")}
                  onBlur={e => {}}
                  classNameInput="form-control"
                  customStyleInput={{ height: "50px", color: "white" }}
                  disabled={isDisabled}
                  validationOption={{ required: false }}
                />
              </td>
            </tr>
            <tr>
              <th>
                <IntlMessages id="res.info" />
              </th>
              <td>
                <Textarea
                  id="resDtl"
                  name="resDtl"
                  type="text"
                  maxLength={4000}
                  value={state.resDtl || ""}
                  onChange={v => onChange(v, "resDtl")}
                  onBlur={e => {}}
                  customStyleInput={{ height: "200px", color: "white" }}
                  classNameInput="form-control"
                  disabled={isDisabled}
                  validationOption={{ required: false }}
                />
              </td>
            </tr>
          </tbody>
        </table>
        <div style={{ marginTop: 10, width: "100%", textAlign: "right" }}>
          <div className="btns">
            <button
              className="btn"
              onClick={onCancel}
              disabled={state.isDisabled}
            >
              <IntlMessages id="cancel" />
            </button>
            <button
              className="btn"
              onClick={onSave}
              disabled={state.isDisabled}
            >
              <IntlMessages id="save" />
            </button>
          </div>
        </div>
        <ReactTooltip id="taxoPath" place="top" type="dark" effect="float">
          <span>
            {taxoData.thrtTpPath
              ? taxoData.thrtTpPath + " > " + taxoData.thrtNm
              : taxoData.thrtNm}
          </span>
        </ReactTooltip>
        {this.state.isShowTree && (
          <TreeSelectPopup
            onClose={() => this.onClose()}
            onConfirm={() => this.onConfirm()}
            setStateSelectKey={setStateSelectKey}
            parentState={state}
          />
        )}
      </div>
    );
  }
}

export default connect(
  state => ({
    taxoData: state.threattaxonomy.get("taxoData"),
    language: state.common.get("language")
  }),
  dispatch => ({
    ThreatTaxonomyActions: bindActionCreators(threatTaxonomyActions, dispatch)
  })
)(TaxonomyInfo);
