import React, { Component } from 'react';
import Tree, { TreeNode } from 'rc-tree';

import 'rc-tree/assets/index.css';

export default class ThreatTaxoTree extends Component {
  state = {
    useIcon: '',
  };

  render() {
    const treeCls = `myCls${(this.state.useIcon && ' customIcon') || ''}`;

    const loop = treeData => {
      return treeData.map(node => {
        try {
          if (node.get('children').size > 0) {
            return (
              <TreeNode
                title={getTitle(node)}
                key={node.get('key')}
                isLeaf={node.get('hasSubNode' === 'Y' ? false : true)}>
                {loop(node.get('children'))}
              </TreeNode>
            );
          } else {
            return (
              <TreeNode
                title={getTitle(node)}
                key={node.get('key')}
                isLeaf={node.get('hasSubNode') === 'Y' ? false : true}
              />
            );
          }
        } catch (error) {
          console.log('undefined Node error ', error);
        }
      });
    };

    //TreeNode title 생성
    const getTitle = node => {
      return <span style={{ marginRight: '20px' }}>{node.get('name')}</span>;
    };

    const { loadData, onSelect, onExpand, state } = this.props;

    return (
      <Tree
        showIcon={true}
        className={treeCls}
        onSelect={onSelect}
        onExpand={onExpand}
        loadData={loadData}
        selectedKeys={state.selectedKeys}
        expandedKeys={state.expandedKeys}
        selectable>
        {loop(state.treeData)}
      </Tree>
    );
  }
}
