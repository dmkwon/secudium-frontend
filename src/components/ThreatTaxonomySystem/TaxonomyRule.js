import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as threatTaxonomyActions from "../../store/modules/threattaxonomy";
import * as ruleActions from "../../store/modules/rule";
import ReactTooltip from "react-tooltip";

import GridTable from "./../Common/GridTable";
import RuleInfoPopup from "./popups/RuleInfoPopup";
import { getCodeList } from "../../store/api/common";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class TaxonomyRule extends Component {
  state = {
    ruleInfo: {},
    ruleType: []
  };

  componentDidMount() {
    this.doGetCodeList();
  }

  doGetCodeList = async () => {
    try {
      const ruleType = await getCodeList("RULE_TYPE");
      this.setState({
        ruleType: ruleType.data
      });
    } catch (err) {
      console.warn(err);
    }
  };

  getRule = async ruleSeq => {
    const { setStateRule } = this.props;
    const { RuleActions } = this.props;
    const res = await RuleActions.getRule(ruleSeq);
    this.setState(
      {
        ruleInfo: res.data
      },
      () => {
        setStateRule();
      }
    );
  };

  render() {
    const { parentState, onCloseConfirm } = this.props;

    const columns = [
      {
        Header: "No.",
        id: "row",
        accessor: "number",
        style: { textAlign: "center" },
        width: 50,
        Cell: row => {
          return <div>{row.index + 1}</div>;
        }
      },
      {
        Header: <IntlMessages id="rule.reg.datetime" />,
        accessor: "regDateFormatted",
        id: "regDateFormatted",
        style: { textAlign: "center" },
        width: 150
      },
      {
        Header: <IntlMessages id="reg.user" />,
        accessor: "regUsrNm",
        id: "regUsrNm",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="rule.name" />,
        accessor: "ruleNm",
        id: "ruleNm",
        Cell: props => {
          const tooltipId = `ruleNmTooltip_${props.original.ruleSeq}`;
          return (
            <React.Fragment>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
              <ReactTooltip
                id={tooltipId}
                className="tooltipDesc"
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={150}
                event="mouseover"
                globalEventOff="mouseout"
              >
                {props.value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      },
      {
        Header: <IntlMessages id="rule.type" />,
        accessor: "ruleTypeStr",
        id: "ruleType",
        style: { textAlign: "center" },
        width: 80,
        Cell: props => {
          if (this.state.ruleType.length > 0) {
            return props.value;
          } else {
            return "";
          }
        }
      },
      {
        Header: <IntlMessages id="rule.desc" />,
        accessor: "ruleDesc",
        id: "ruleDesc",
        Cell: props => {
          const tooltipId = `ruleDescTooltip_${props.original.ruleSeq}`;
          return (
            <React.Fragment>
              <ReactTooltip
                id={tooltipId}
                className="tooltipDesc"
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={150}
                event="mouseover"
                globalEventOff="mouseout"
                clickable={false}
              >
                {props.value}
              </ReactTooltip>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
            </React.Fragment>
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <div className="component__title">
          <span>
            <IntlMessages id="refer.rule" />
          </span>
        </div>
        <GridTable
          className="-striped -highlight"
          data={this.props.ruleData}
          columns={columns}
          resizable={true}
          getTrProps={(state, rowInfo, column, instance) => {
            return {
              onClick: async e => {
                this.getRule(rowInfo.original.ruleSeq);
              },
              style: { cursor: "pointer" }
            };
          }}
        />
        {parentState.showRuleInfo && (
          <RuleInfoPopup
            ruleInfo={this.state.ruleInfo}
            ruleType={this.state.ruleType}
            onClose={onCloseConfirm}
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    ruleData: state.threattaxonomy.get("ruleData"),
    rule: state.rule.get("rule")
  }),
  dispatch => ({
    ThreatTaxonomyActions: bindActionCreators(threatTaxonomyActions, dispatch),
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(TaxonomyRule);
