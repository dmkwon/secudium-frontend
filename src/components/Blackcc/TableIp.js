import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../store/modules/blackcc";

import GridTable from "./../Common/GridTable";
import ReactTooltip from "react-tooltip";

import UpdateIpPopup from "./popups/UpdateIpPopup";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

const intToIp = ipInt => {
  return (
    (ipInt >>> 24) +
    "." +
    ((ipInt >> 16) & 255) +
    "." +
    ((ipInt >> 8) & 255) +
    "." +
    (ipInt & 255)
  );
};
const ipToInt = ip => {
  return (
    ip.split(".").reduce(function(ipInt, octet) {
      return (ipInt << 8) + parseInt(octet, 10);
    }, 0) >>> 0
  );
};

class TableIp extends React.Component {
  state = {
    selectedRowId: "",
    input_dstnct: "",
    input_stIp: "",
    input_edIp: "",
    input_ipType: "R",
    input_country: "",
    input_detectDate: "",
    input_expDate: "",
    input_attkType: "",
    input_enabledYn: "",
    input_description: "",
    isShowUpdate: false,
    showConfirm: false,
    resource: {},
    rowData: ""
  };

  closeUpdatePopup = () => {
    this.setState({ isShowUpdate: false });
  };

  openDeletePopup = () => {
    this.setState({
      showConfirm: true,
      resource: {
        title: <IntlMessages id="confirm" />,
        message: <IntlMessages id="do.delete" />,
        hasChangeReason: false,
        btns: [
          {
            name: <IntlMessages id="cancel" />,
            CB: () => this.closeDeletePopup()
          },
          {
            name: <IntlMessages id="confirm" />,
            CB: () => this.onDeleteConfirm()
          }
        ]
      }
    });
  };

  closeDeletePopup = () => {
    this.setState({ showConfirm: false });
  };

  onDeleteConfirm = async () => {
    const { BlackccActions } = this.props;

    let rx = "";
    rx = await BlackccActions.putIp({
      ipListSeq: this.state.selectedRowId,
      ipDstnct: this.state.input_dstnct,
      stIp: ipToInt(this.state.input_stIp),
      edIp: ipToInt(this.state.input_edIp),
      country: this.state.input_country,
      attkType: this.state.input_attkType,
      detectDate: this.state.input_detectDate,
      expDate: this.state.input_expDate,
      useYn: "N",
      ipDesc: this.state.input_description
    });

    if (rx.status === 200) {
      this.closeDeletePopup();
      await BlackccActions.getIpList({});
    } else {
    }
  };

  render() {
    const {
      initTable,
      ipList,
      chkBoxSingleChange,
      chkBoxAllChange,
      state,
      attkCode,
      cntyCode
    } = this.props;

    const { showConfirm, isShowUpdate } = this.state;

    const columns_ip = [
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        sortable: false,
        width: 40
      },
      {
        Header: <IntlMessages id="ip.class" />,
        accessor: "ipDstnct",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="ip" />,
        accessor: "ip",
        style: { textAlign: "left" },
        Cell: data => {
          if (data.row._original.stIp !== data.row._original.edIp) {
            return (
              <div>{`${intToIp(data.row._original.stIp)}~${intToIp(
                data.row._original.edIp
              )}`}</div>
            );
          } else if (data.row._original.stIp === data.row._original.edIp) {
            return <div>{`${intToIp(data.row._original.stIp)}`}</div>;
          }
        },
        width: 200
      },
      {
        Header: <IntlMessages id="country" />,
        accessor: "country",
        style: { textAlign: "center" },
        width: 80
      },
      {
        Header: <IntlMessages id="detect.date" />,
        accessor: "detectDate",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="expire.date" />,
        accessor: "expDate",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="attack.type" />,
        accessor: "attkTypeString",
        style: { textAlign: "center" },
        width: 150
      },
      {
        Header: <IntlMessages id="desc" />,
        accessor: "ipDesc",
        style: { textAlign: "left" },
        Cell: props => {
          const tooltipId = `ipDescTooltip_${props.original.ipListSeq}`;
          return (
            <React.Fragment>
              <ReactTooltip
                id={tooltipId}
                className="tooltipDesc"
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={150}
                event="mouseover"
                globalEventOff="mouseout"
              >
                {props.value}
              </ReactTooltip>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
            </React.Fragment>
          );
        }
      },
      {
        Header: <IntlMessages id="reg.user" />,
        accessor: "regUsrNm",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="reg.date" />,
        accessor: "stringRegDate",
        style: { textAlign: "center" },
        width: 120
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "modUsrNm",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="mod.date" />,
        accessor: "stringModDate",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: "Actions",
        accessor: "icon",
        style: { textAlign: "center" },
        width: 70,
        Cell: row => {
          return (
            <div>
              <button id="ipDelete" className="btn btn-icon">
                <img
                  id="ipDelete"
                  src="/images/common/icon_delete.png"
                  alt="icon_delete"
                />
              </button>
              <button id="ipUpdate" className="btn btn-icon">
                <img
                  id="ipUpdate"
                  src="/images/common/icon_edit.png"
                  alt="icon_edit"
                />
              </button>
            </div>
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <GridTable
          columns={columns_ip}
          data={ipList.content}
          className="-striped -highlight"
          onFetchData={initTable}
          getTrProps={(state, rowInfo, column, instance) => {
            // 파라미터 4개 필수
            return {
              onClick: async e => {
                try {
                  if (e.target.id === "ipUpdate") {
                    let rowData = {
                      selectedRowId: rowInfo.original.ipListSeq,
                      dstnct: rowInfo.original.ipDstnct,
                      stIp: intToIp(rowInfo.original.stIp),
                      edIp:
                        rowInfo.original.edIp !== undefined
                          ? intToIp(rowInfo.original.edIp)
                          : undefined,
                      deviceIp: intToIp(rowInfo.original.deviceIp),
                      companyId: rowInfo.original.companyId,
                      ipType:
                        rowInfo.original.stIp !== rowInfo.original.edIp
                          ? "R"
                          : "S",
                      country: rowInfo.original.country,
                      detectDate: rowInfo.original.detectDate,
                      expDate: rowInfo.original.expDate,
                      attkType: rowInfo.original.attkType,
                      enabledYn: rowInfo.original.useYn,
                      description: rowInfo.original.ipDesc
                    };
                    this.setState({ rowData: rowData, isShowUpdate: true });
                  } else if (e.target.id === "ipDelete") {
                    this.setState(
                      {
                        selectedRowId: rowInfo.original.ipListSeq,
                        input_dstnct: rowInfo.original.ipDstnct,
                        input_stIp: intToIp(rowInfo.original.stIp),
                        input_edIp: intToIp(rowInfo.original.edIp),
                        deviceIp: intToIp(rowInfo.original.deviceIp),
                        companyId: rowInfo.original.companyId,
                        input_ipType: "R",
                        input_country: rowInfo.original.country,
                        input_detectDate: rowInfo.original.detectDate,
                        input_expDate: rowInfo.original.expDate,
                        input_attkType: rowInfo.original.attkType,
                        input_enabledYn: rowInfo.original.useYn,
                        input_description: rowInfo.original.ipDesc
                      },
                      () => {
                        this.openDeletePopup();
                      }
                    );
                  }
                } catch (e) {
                  console.log(e);
                }
              }
            };
          }}
        />
        {isShowUpdate && (
          <UpdateIpPopup
            rowData={this.state.rowData}
            onClose={this.closeUpdatePopup}
            attkCode={attkCode}
            cntyCode={cntyCode}
          />
        )}
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    ipList: state.blackcc.get("ipList")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(TableIp);
