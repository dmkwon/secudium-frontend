import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../../store/modules/blackcc";
import Draggable from "react-draggable";

import DatePicker from "react-datepicker";
import isAfter from "date-fns/is_after";
import "react-datepicker/dist/react-datepicker.css";

import { isValid } from "./../../../utils";
import { Textbox, Select } from "react-inputs-validation";
import CommonConfirmPopup from "../../Common/Popups/CommonConfirmPopup";

import IntlMessages from "util/IntlMessages";
import { FormattedMessage } from "react-intl";

const inputDstnctOpt = [
  { id: "BLACK", name: "Black IP" },
  { id: "CC", name: "Cc IP" },
  { id: "WHITE", name: "White IP" }
];

// const inputIpTypeOpt = [
//   {
//     id: "R",
//     name: <FormattedMessage id="range"> {text => text} </FormattedMessage>
//   },
//   {
//     id: "S",
//     name: <FormattedMessage id="single.item"> {text => text} </FormattedMessage>
//   }
// ];

const intToIp = ipInt => {
  return (
    (ipInt >>> 24) +
    "." +
    ((ipInt >> 16) & 255) +
    "." +
    ((ipInt >> 8) & 255) +
    "." +
    (ipInt & 255)
  );
};
const ipToInt = ip => {
  return (
    ip.split(".").reduce(function(ipInt, octet) {
      return (ipInt << 8) + parseInt(octet, 10);
    }, 0) >>> 0
  );
};
const isValidIp = ip => {
  return /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/.test(ip);
};

class InsertIpPopup extends React.Component {
  state = {
    input_country: "",
    input_attkType: "",
    input_dstnct: "",
    input_description: "",
    input_stIp: "",
    input_edIp: "",
    cvted_stIp: "",
    cvted_edIp: "",
    cvted_deviceIp: "",
    input_companyId: "",
    input_deviceIp: "",
    input_detectDate: null,
    input_expDate: null,
    input_ipType: "S",
    isEmpty: false,
    showConfirm: false,
    resource: {},
    validate: false,
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("insertIpPopup").offsetWidth;
    const elHeight = document.getElementById("insertIpPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  onChangeSchIp = e => {
    const { name, value } = e;
    const state = { ...this.state };
    state[name] = value;
    this.setState(state);
  };

  openConfirm = async () => {
    this.setState({ validate: true });
    const { BlackccActions } = this.props;
    const {
      input_dstnct,
      input_country,
      input_stIp,
      input_edIp,
      input_deviceIp
    } = this.state;
    let duplRx = {};

    let isIpSt = isValidIp(input_stIp),
      isIpEd = isValidIp(input_edIp),
      stIp = ipToInt(input_stIp),
      edIp = ipToInt(input_edIp),
      deviceIp = ipToInt(input_deviceIp);

    if (!isValid(input_dstnct)) {
      return;
    }

    if (!isValid(this.state.input_detectDate)) {
      this.setState({ isEmpty: true });
      return; //::isEmpty react-validation이 아닌 date-picker 형태이기 때문에 커스텀 유효성검사 메시지 위해 필요.
    }

    if (isValid(this.state.input_country)) {
      if (this.props.cntyCode.indexOf(input_country) === -1) {
        this.showPopup(
          "ALERT",
          <FormattedMessage id="wrong.country.code">
            {text => text}
          </FormattedMessage>
        );
        return;
      }
    }

    if (!isValid(input_stIp) && !isValid(input_edIp)) {
    } else if (!isValid(input_stIp) && isValid(input_edIp)) {
    } else if (isValid(input_stIp) && !isValid(input_edIp)) {
      if (!isIpSt) {
        return;
      } //IP 형식 검사

      let insertChkParam = {
        stIp: stIp
      };

      duplRx = await BlackccActions.getIpDupl(insertChkParam);
      if (duplRx.data.length !== 0) {
        let duplMsg = `[${intToIp(duplRx.data.stIp)} ${
          duplRx.data.edIp === duplRx.data.stIp ? "" : "~ N/A"
        }]`;
        this.showPopup(
          "ALERT",
          <FormattedMessage id="already.ip.reg">
            {text => text}
          </FormattedMessage>,
          duplMsg
        );
        return;
      }

      this.setState({ cvted_stIp: stIp, cvted_deviceIp: deviceIp }, () => {
        this.showPopup(
          "CONFIRM",
          <FormattedMessage id="do.register">{text => text}</FormattedMessage>
        );
      });
    } else {
      // Range IP 검색
      if (!isIpSt || !isIpEd) {
        return;
      } //IP 형식 검사
      if (stIp > edIp) {
        return;
      } //범위 유효성 검사

      let insertChkParam = {
        stIp: stIp,
        edIp: edIp
      };

      duplRx = await BlackccActions.getIpsDupl(insertChkParam);
      let len = duplRx.data.length;
      if (len !== 0) {
        let duplMsg = `[${intToIp(duplRx.data[0].stIp)} ~${intToIp(
          duplRx.data[0].edIp
        )} ${
          len > 1
            ? <FormattedMessage id="others">{text => text}</FormattedMessage> +
              (len - 1) +
              <FormattedMessage id="count">{text => text}</FormattedMessage>
            : ""
        }]`;
        this.showPopup(
          "ALERT",
          <FormattedMessage id="already.ip.reg">
            {text => text}
          </FormattedMessage>,
          duplMsg
        );
        return;
      }

      this.setState(
        { cvted_stIp: stIp, cvted_edIp: edIp, isEmpty: false },
        () => {
          this.showPopup(
            "CONFIRM",
            <FormattedMessage id="do.register">{text => text}</FormattedMessage>
          );
        }
      );
    }
  };

  showPopup = (type, showedMsg, data) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: <FormattedMessage id="confirm">{text => text}</FormattedMessage>,
        message: showedMsg,
        data: data,
        hasChangeReason: false,
        btns:
          type === "CONFIRM"
            ? [
                {
                  name: (
                    <FormattedMessage id="cancel">
                      {text => text}
                    </FormattedMessage>
                  ),
                  CB: () => this.onCloseConfirm()
                },
                {
                  name: (
                    <FormattedMessage id="confirm">
                      {text => text}
                    </FormattedMessage>
                  ),
                  CB: () => this.onInsertConfirm()
                }
              ]
            : [
                {
                  name: (
                    <FormattedMessage id="confirm">
                      {text => text}
                    </FormattedMessage>
                  ),
                  CB: () => this.onCloseConfirm()
                }
              ]
      }
    });
  };

  resetState = () => {
    this.setState({
      input_country: "",
      input_attkType: "",
      input_dstnct: "BLACK",
      input_description: "",
      input_stIp: "",
      input_edIp: "",
      cvted_stIp: "",
      cvted_edIp: "",
      input_companyId: "",
      input_deviceIp: "",
      input_detectDate: null,
      input_expDate: null
    });
  };

  onCloseConfirm = () => {
    this.setState({
      isEmpty: false,
      showConfirm: false
    });
  };

  onInsertConfirm = async () => {
    const { BlackccActions, onClose } = this.props;
    const { type, ip } = this.state;

    let offset = this.state.input_detectDate.getTimezoneOffset() * 60 * 1000;
    const rx = await BlackccActions.postIp({
      ipDstnct: this.state.input_dstnct,
      stIp: this.state.cvted_stIp,
      edIp: this.state.cvted_edIp || this.state.cvted_stIp, // 단건 입력 시 동일한 ip 입력
      country: this.state.input_country,
      detectDate: this.state.input_detectDate
        ? new Date(this.state.input_detectDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      expDate: this.state.input_expDate
        ? new Date(this.state.input_expDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      attkType: this.state.input_attkType,
      ipDesc: this.state.input_description,
      companyId: this.state.input_companyId,
      deviceIp: this.state.cvted_deviceIp
    });

    if (rx.status === 201) {
      this.onCloseConfirm();
      this.resetState();
      onClose();
      await BlackccActions.getIpList({ type, ip });
    } else if (rx.status !== 200) {
      this.onCloseConfirm();
    }
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  handleChange = ({ input_detectDate, input_expDate }) => {
    input_detectDate = input_detectDate || this.state.input_detectDate;
    input_expDate = input_expDate || this.state.input_expDate;
    if (isAfter(input_detectDate, input_expDate) && isValid(input_expDate)) {
      input_expDate = input_detectDate;
    }
    this.setState({ input_detectDate, input_expDate });
  };

  handleChangeDetDt = input_detectDate =>
    this.handleChange({ input_detectDate });
  handleChangeExpDt = input_expDate => this.handleChange({ input_expDate });

  render() {
    const { position, showConfirm } = this.state;
    const { onClose } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup" style={{ width: "650px" }} id="insertIpPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <FormattedMessage id="ip.reg">{text => text}</FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div className="popup__body">
              <div className="option-wrapper">
                <table className="table table--white table--info">
                  <colgroup>
                    <col width="100px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <FormattedMessage id="division">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          value={this.state.input_dstnct}
                          optionList={inputDstnctOpt}
                          onChange={(dstnct, e) => {
                            this.setState({
                              input_dstnct: dstnct,
                              input_ipType:
                                dstnct === "WHITE"
                                  ? "S"
                                  : this.state.input_ipType,
                              input_companyId:
                                dstnct === "WHITE"
                                  ? this.state.input_companyId
                                  : "",
                              input_deviceIp:
                                dstnct === "WHITE"
                                  ? this.state.input_deviceIp
                                  : ""
                            });
                          }}
                          onBlur={() => {}}
                          validate={this.state.validate}
                          validationOption={{ locale: this.props.language }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    {this.state.input_dstnct === "WHITE" ? (
                      <React.Fragment>
                        <tr>
                          <th>
                            Company ID
                            <span
                              className="font--red"
                              style={{ color: "red" }}
                            >
                              *
                            </span>
                          </th>
                          <td>
                            <Textbox
                              id="companyId"
                              name="companyId"
                              type="text"
                              value={this.state.input_companyId}
                              onChange={(name, e) => {
                                this.setState({
                                  input_companyId: e.target.value
                                });
                              }}
                              onBlur={e => {}}
                              validationOption={{
                                required: false,
                                reg: /^[0-9]+$/,
                                regMsg: <IntlMessages id="number.only" />,
                                customFunc: res => {
                                  if (res === "") {
                                    return <IntlMessages id="require.item" />;
                                  }
                                  if (this.state.input_companyId === "") {
                                    return true;
                                  }
                                  return true;
                                },
                                locale: this.props.language
                              }}
                            />
                          </td>
                        </tr>
                        <tr>
                          <th>
                            Device IP
                            <span
                              className="font--red"
                              style={{ color: "red" }}
                            >
                              *
                            </span>
                          </th>
                          <td>
                            <Textbox
                              id="deviceIp"
                              name="deviceIp"
                              type="text"
                              value={this.state.input_deviceIp}
                              onChange={(name, e) => {
                                this.setState({
                                  input_deviceIp: e.target.value
                                });
                              }}
                              onBlur={e => {}}
                              validationOption={{
                                reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                regMsg: <IntlMessages id="wrong.ip.type" />,
                                customFunc: res => {
                                  if (res === "") {
                                    return <IntlMessages id="require.item" />;
                                  }
                                  if (this.state.input_deviceIp === "") {
                                    return true;
                                  }
                                  return true;
                                },
                                locale: this.props.language
                              }}
                            />
                          </td>
                        </tr>
                      </React.Fragment>
                    ) : (
                      <React.Fragment></React.Fragment>
                    )}
                    <tr>
                      <th style={{ verticalAlign: "middle" }}>
                        <span style={{ color: "black" }}>IP</span>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <div>
                          <Textbox
                            id="stIp"
                            name="stIp"
                            type="text"
                            maxLength={30}
                            value={this.state.input_stIp}
                            onChange={e => {
                              this.setState({ input_stIp: e });
                            }}
                            onBlur={e => {}}
                            validate={this.state.validate}
                            validationCallback={res => {
                              this.setState({
                                hasNameError: res,
                                validate: false
                              });
                            }}
                            validationOption={{
                              reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                              regMsg: <IntlMessages id="wrong.ip.type" />,
                              customFunc: res => {
                                if (res === "") {
                                  return <IntlMessages id="require.item" />;
                                }
                                return true;
                              },
                              locale: this.props.language
                            }}
                          />
                        </div>
                      </td>
                    </tr>
                    {/* {this.state.input_dstnct === 'BLACK' 
                    || this.state.input_dstnct === 'CC' 
                    || this.state.input_dstnct === 'WHITE' ? (
                    
                    ) : (<tr>
                      <th style={{ height: '55px', verticalAlign: 'top' }}>
                        <span style={{ color: 'black' }}>IP</span>
                        <span className="font--red" style={{ color: 'red' }}>
                          *
                        </span>
                      </th>
                      <td style={{ display: 'flex' }}>
                        <Select
                          tabIndex="1"
                          value={this.state.input_ipType}
                          optionList={inputIpTypeOpt}
                          onChange={(schType, e) => {
                            if (
                              this.state.input_ipType === 'R' &&
                              schType === 'S'
                            ) {
                              this.setState({
                                input_ipType: schType,
                                input_edIp: '',
                              });
                            } else if (
                              this.state.input_ipType === 'S' &&
                              schType === 'R'
                            ) {
                              this.setState({ input_ipType: schType });
                            }
                          }}
                          onBlur={() => {}}
                          customStyleWrapper={{
                            width: '60px',
                            marginRight: 10,
                          }}
                        />
                        {this.state.input_ipType === 'R' ? (
                          <div>
                            <div
                              style={{ display: 'flex', alignItems: 'center' }}>
                              <Textbox
                                type="text"
                                maxLength={30}
                                value={this.state.input_stIp}
                                onChange={e => {
                                  this.setState({ input_stIp: e });
                                }}
                                onBlur={e => {}} // required.
                                customStyleWrapper={{
                                  marginRight: 5,
                                  height: '50px',
                                }}
                                validate={this.state.validate}
                                validationCallback={res => {
                                  this.setState({
                                    hasNameError: res,
                                    validate: false,
                                  });
                                }}
                                validationOption={{
                                  reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                  regMsg: '유효하지 않은 IP 형식입니다.',
                                  customFunc: res => {
                                    if (res === '') {
                                      return '해당항목은 필수항목입니다.';
                                    }
                                    if (this.state.input_edIp === '') {
                                      return true;
                                    }
                                    if (
                                      ipToInt(res) >
                                      ipToInt(this.state.input_edIp)
                                    ) {
                                      return '유효하지 않은 IP 범위입니다.';
                                    }
                                    return true;
                                  },
                                }}
                              />
                              <span style={{ height: '50px', color: 'black' }}>
                                
                                ~
                              </span>
                              <Textbox
                                type="text"
                                maxLength={30}
                                value={this.state.input_edIp}
                                onChange={e => {
                                  this.setState({ input_edIp: e });
                                }}
                                onBlur={e => {}}
                                customStyleWrapper={{
                                  marginLeft: 5,
                                  height: '50px',
                                }}
                                validate={this.state.validate}
                                validationCallback={res => {
                                  this.setState({
                                    hasNameError: res,
                                    validate: false,
                                  });
                                }}
                                validationOption={{
                                  required: false,
                                  reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                  regMsg: '유효하지 않은 IP 형식입니다.',
                                  customFunc: res => {
                                    return true;
                                  },
                                }}
                              />
                            </div>
                          </div>
                        ) : (
                          <div style={{ height: '45px' }}>
                            <Textbox
                              type="text"
                              maxLength={30}
                              value={this.state.input_stIp}
                              onChange={e => {
                                this.setState({ input_stIp: e });
                              }}
                              onBlur={e => {}}
                              validate={this.state.validate}
                              validationCallback={res => {
                                this.setState({
                                  hasNameError: res,
                                  validate: false,
                                });
                              }}
                              validationOption={{
                                reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                regMsg: '유효하지 않은 IP 형식입니다.',
                                customFunc: res => {
                                  if (res === '') {
                                    return '해당항목은 필수항목입니다.';
                                  }
                                  return true;
                                },
                              }}
                            />
                          </div>
                        )}
                      </td>
                    </tr>)} */}
                    <tr>
                      <th>
                        <FormattedMessage id="country">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" />
                      </th>
                      <td>
                        <Textbox
                          id="country"
                          name="country"
                          type="text"
                          maxLength={2}
                          value={this.state.input_country}
                          onChange={(name, e) => {
                            this.setState({
                              input_country: e.target.value.toUpperCase()
                            });
                          }}
                          onBlur={e => {}}
                          validationOption={{
                            required: false,
                            reg: /^[a-zA-Z]+$/,
                            regMsg: <IntlMessages id="accept.country.code" />,
                            locale: this.props.language,
                            customFunc: res =>
                              res === "" ||
                              this.props.cntyCode.indexOf(res) !== -1 ? (
                                true
                              ) : (
                                <IntlMessages id="wrong.country.code" />
                              )
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="detect.date">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_detectDate}
                          onChange={this.handleChangeDetDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsStart
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                        <span style={{ color: "red" }}>
                          {this.state.isEmpty ? (
                            <IntlMessages id="require.detect.date" />
                          ) : (
                            ""
                          )}
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="expire.date">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_expDate}
                          onChange={this.handleChangeExpDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsEnd
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                      </td>
                    </tr>
                    {this.state.input_dstnct !== "WHITE" ? (
                      <tr>
                        <th>
                          <FormattedMessage id="attack.type">
                            {text => text}
                          </FormattedMessage>
                        </th>
                        <td>
                          <Select
                            id="attkType"
                            name="attkType"
                            value={this.state.input_attkType}
                            optionList={this.props.attkCode}
                            onChange={(attk, e) => {
                              this.setState({ input_attkType: attk });
                            }}
                            onBlur={() => {}}
                            customStyleWrapper={{ width: "200px" }}
                          />
                        </td>
                      </tr>
                    ) : (
                      ""
                    )}
                    <tr>
                      <th>
                        <FormattedMessage id="desc">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <textarea
                          className="form-control"
                          id="desc"
                          name="desc"
                          type="text"
                          maxLength={2000}
                          onChange={e => {
                            this.setState({
                              input_description: e.target.value
                            });
                          }}
                          value={this.state.input_description}
                          style={{ height: 75 }}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--blue" onClick={this.openConfirm}>
                <FormattedMessage id="save">{text => text}</FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    ipList: state.blackcc.get("ipList"),
    language: state.common.get("language")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(InsertIpPopup);
