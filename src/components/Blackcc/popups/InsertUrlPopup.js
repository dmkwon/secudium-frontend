import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../../store/modules/blackcc";
import Draggable from "react-draggable";

import DatePicker from "react-datepicker";
import isAfter from "date-fns/is_after";
import "react-datepicker/dist/react-datepicker.css";

import { isValid } from "./../../../utils";
import { Textbox, Select } from "react-inputs-validation";
import CommonConfirmPopup from "../../Common/Popups/CommonConfirmPopup";

import { FormattedMessage } from "react-intl";

const inputDstnctOpt = [
  { id: "BLACK", name: "Black URL" },
  { id: "CC", name: "Cc URL" }
];

class insertUrlPopup extends React.Component {
  state = {
    input_dstnct: "",
    input_attkType: "",
    input_url: "",
    input_description: "",
    input_detectDate: null,
    input_expDate: null,
    showConfirm: false,
    resource: {},
    isEmpty: false,
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("insertUrlPopup").offsetWidth;
    const elHeight = document.getElementById("insertUrlPopup").offsetHeight;

    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  openConfirm = async () => {
    const { input_url, input_dstnct } = this.state;
    this.setState({ validate: true });

    if (!isValid(input_dstnct)) {
      return;
    }

    if (!isValid(input_url)) {
    } else if (!isValid(this.state.input_detectDate)) {
      this.setState({ isEmpty: true });
    } else {
      this.setState({
        showConfirm: true,
        resource: {
          title: (
            <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
          ),
          message: (
            <FormattedMessage id="do.register">{text => text}</FormattedMessage>
          ),
          hasChangeReason: false,
          btns: [
            {
              name: (
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              ),
              CB: () => this.onCloseConfirm()
            },
            {
              name: (
                <FormattedMessage id="confirm">{text => text}</FormattedMessage>
              ),
              CB: () => this.onInsertConfirm()
            }
          ]
        }
      });
    }
  };

  onCloseConfirm = () => {
    this.setState({ showConfirm: false, isEmpty: false });
  };

  onInsertConfirm = async () => {
    const { BlackccActions, onClose } = this.props;
    const { type, url } = this.state;

    let offset = this.state.input_detectDate.getTimezoneOffset() * 60 * 1000;
    const rx = await BlackccActions.postUrl({
      urlDstnct: this.state.input_dstnct,
      url: this.state.input_url,
      detectDate: this.state.input_detectDate
        ? new Date(this.state.input_detectDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      expDate: this.state.input_expDate
        ? new Date(this.state.input_expDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      attkType: this.state.input_attkType,
      urlDesc: this.state.input_description
    });

    if (rx.status === 201) {
      this.onCloseConfirm();
      this.resetState();
      onClose();
      await BlackccActions.getUrlList({ type, url });
    } else if (rx.status !== 200) {
      this.onCloseConfirm();
    }
  };

  resetState = () => {
    this.setState({
      input_attkType: "",
      input_dstnct: "BLACK",
      input_description: "",
      input_url: "",
      input_detectDate: null,
      input_expDate: null
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  handleChange = ({ input_detectDate, input_expDate }) => {
    input_detectDate = input_detectDate || this.state.input_detectDate;
    input_expDate = input_expDate || this.state.input_expDate;
    if (isAfter(input_detectDate, input_expDate) && isValid(input_expDate)) {
      input_expDate = input_detectDate;
    }
    this.setState({ input_detectDate, input_expDate });
  };

  handleChangeDetDt = input_detectDate =>
    this.handleChange({ input_detectDate });
  handleChangeExpDt = input_expDate => this.handleChange({ input_expDate });

  render() {
    const { position, showConfirm } = this.state;
    const { onClose } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup" style={{ width: "625px" }} id="insertUrlPopup">
            <div className="popup__header">
              <h5>
                <FormattedMessage id="url.create">
                  {text => text}
                </FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div className="popup__body">
              <div className="option-wrapper">
                <table className="table table--white table--info">
                  <colgroup>
                    <col width="100px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <FormattedMessage id="division">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          id="dstnct"
                          value={this.state.input_dstnct}
                          optionList={inputDstnctOpt}
                          onChange={(dstnct, e) => {
                            this.setState({ input_dstnct: dstnct });
                          }}
                          onBlur={() => {}}
                          validate={this.state.validate}
                          validationOption={{ locale: this.props.language }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th style={{ verticalAlign: "middle" }}>
                        <FormattedMessage id="url">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Textbox
                          id="url"
                          type="text"
                          maxLength={1000}
                          value={this.state.input_url}
                          onChange={e => {
                            this.setState({ input_url: e });
                          }}
                          onBlur={e => {}}
                          customStyleWrapper={{ marginRight: 5 }}
                          validate={this.state.validate}
                          validationOption={{ locale: this.props.language }}
                          validationCallback={res => {
                            this.setState({
                              hasNameError: res,
                              validate: false
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="detect.date">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_detectDate}
                          onChange={this.handleChangeDetDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsStart
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                        <span style={{ color: "red" }}>
                          {this.state.isEmpty ? (
                            <FormattedMessage id="require.detect.date">
                              {text => text}
                            </FormattedMessage>
                          ) : (
                            ""
                          )}
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="expire.date">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_expDate}
                          onChange={this.handleChangeExpDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsEnd
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="attack.type">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <Select
                          id="attkType"
                          value={this.state.input_attkType}
                          optionList={this.props.attkCode}
                          onChange={(attk, e) => {
                            this.setState({ input_attkType: attk });
                          }}
                          onBlur={() => {}}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="desc">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <textarea
                          className="form-control"
                          type="text"
                          maxLength={2000}
                          onChange={e => {
                            this.setState({
                              input_description: e.target.value
                            });
                          }}
                          value={this.state.input_description}
                          style={{ height: 75 }}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--blue" onClick={this.openConfirm}>
                <FormattedMessage id="save"> {text => text} </FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    urlList: state.blackcc.get("urlList")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(insertUrlPopup);
