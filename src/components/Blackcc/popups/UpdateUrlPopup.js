import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../../store/modules/blackcc";
import Draggable from "react-draggable";

import DatePicker from "react-datepicker";
import isAfter from "date-fns/is_after";
import "react-datepicker/dist/react-datepicker.css";

import { isValid } from "./../../../utils";
import { Textbox, Select } from "react-inputs-validation";
import CommonConfirmPopup from "../../Common/Popups/CommonConfirmPopup";

import { FormattedMessage } from "react-intl";

class UpdateUrlPopup extends React.Component {
  state = {
    selectedRowId: "",
    input_attkType: "",
    input_dstnct: "BLACK",
    input_description: "",
    input_url: "",
    input_detectDate: null,
    input_expDate: null,
    input_enabledYn: "Y",
    showConfirm: false,
    resource: {},
    isEmpty: false,
    isModified: false,
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    // popup 정중앙 계산
    const elWidth = document.getElementById("updateUrlPopup").offsetWidth;
    const elHeight = document.getElementById("updateUrlPopup").offsetHeight;
    // 선택데이터 props 통해 local state 전달.
    const rowInfo = this.props.rowData;
    this.setState({
      selectedRowId: rowInfo.selectedRowId,
      input_dstnct: rowInfo.dstnct,
      input_url: rowInfo.url,
      input_detectDate: rowInfo.detectDate
        ? new Date(rowInfo.detectDate)
        : null,
      input_expDate: rowInfo.expDate ? new Date(rowInfo.expDate) : null,
      input_attkType: rowInfo.attkType ? rowInfo.attkType : null,
      input_description: rowInfo.description,
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  openConfirm = async () => {
    const { input_url } = this.state;
    this.setState({ validate: true });
    if (!isValid(input_url)) {
    } else if (!isValid(this.state.input_detectDate)) {
      this.setState({ isEmpty: true });
    } else {
      if (!this.state.isModified) {
        return;
      }

      this.setState({
        isEmpty: false,
        isModified: false,
        showConfirm: true,
        resource: {
          title: (
            <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
          ),
          message: (
            <FormattedMessage id="do.edit"> {text => text} </FormattedMessage>
          ),
          hasChangeReason: false,
          btns: [
            {
              name: (
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              ),
              CB: () => this.onCloseConfirm()
            },
            {
              name: (
                <FormattedMessage id="confirm">{text => text}</FormattedMessage>
              ),
              CB: () => this.onUpdateConfirm()
            }
          ]
        }
      });

      this.setState({
        showConfirm: true,
        isEmpty: false,
        isModified: false
      });
    }
  };

  onCloseConfirm = () => {
    this.setState({
      showConfirm: false,
      isEmpty: false,
      isModified: false
    });
  };

  onUpdateConfirm = async () => {
    const { BlackccActions, onClose } = this.props;
    const { type, url } = this.state;

    let offset = this.state.input_detectDate.getTimezoneOffset() * 60 * 1000;
    const rx = await BlackccActions.putUrl({
      urlListSeq: this.state.selectedRowId,
      urlDstnct: this.state.input_dstnct,
      url: this.state.input_url,
      attkType: this.state.input_attkType,
      detectDate: this.state.input_detectDate
        ? new Date(this.state.input_detectDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      expDate: this.state.input_expDate
        ? new Date(this.state.input_expDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      urlDesc: this.state.input_description,
      useYn: "Y"
    });

    if (rx.status === 200) {
      this.onCloseConfirm();
      this.resetState();
      await BlackccActions.getUrlList({ type, url });
      onClose();
    } else if (rx.status !== 200) {
    }
  };

  resetState = () => {
    this.setState({
      input_country: "",
      input_attkType: "",
      input_dstnct: "BLACK",
      input_description: "",
      input_url: "",
      input_detectDate: null,
      input_expDate: null
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  handleChange = ({ input_detectDate, input_expDate }) => {
    input_detectDate = input_detectDate || this.state.input_detectDate;
    input_expDate = input_expDate || this.state.input_expDate;
    if (isAfter(input_detectDate, input_expDate) && isValid(input_expDate)) {
      input_expDate = input_detectDate;
    }
    this.setState({ input_detectDate, input_expDate, isModified: true });
  };

  handleChangeDetDt = input_detectDate =>
    this.handleChange({ input_detectDate });
  handleChangeExpDt = input_expDate => this.handleChange({ input_expDate });

  render() {
    const { position, showConfirm } = this.state;
    const { onClose } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup" style={{ width: "625px" }} id="updateUrlPopup">
            <div className="popup__header">
              <h5>
                <FormattedMessage id="url.edit">
                  {text => text}
                </FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div className="popup__body">
              <div className="option-wrapper">
                <table className="table table--white table--info">
                  <colgroup>
                    <col width="100px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <FormattedMessage id="division">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>{this.state.input_dstnct}</td>
                    </tr>
                    <tr>
                      <th style={{ verticalAlign: "middle" }}>
                        URL
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Textbox
                          name="input_url"
                          type="text"
                          maxLength={1000}
                          value={this.state.input_url}
                          onChange={e => {
                            this.setState({ input_url: e, isModified: true });
                          }}
                          onBlur={e => {}}
                          customStyleWrapper={{ marginRight: 5 }}
                          validate={this.state.validate}
                          validationOption={{ locale: this.props.language }}
                          validationCallback={res => {
                            this.setState({
                              hasNameError: res,
                              validate: false
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="detect.date">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_detectDate}
                          onChange={this.handleChangeDetDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsStart
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                        <span style={{ color: "red" }}>
                          {this.state.isEmpty ? (
                            <FormattedMessage id="require.detect.date">
                              {text => text}
                            </FormattedMessage>
                          ) : (
                            ""
                          )}
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="expire.date">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_expDate}
                          onChange={this.handleChangeExpDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsEnd
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="attack.type">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <Select
                          name={"input_attkType"}
                          value={this.state.input_attkType || ""}
                          optionList={this.props.attkCode}
                          onChange={(attk, e) => {
                            this.setState({
                              input_attkType: attk,
                              isModified: true
                            });
                          }}
                          onBlur={() => {}}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="desc">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <textarea
                          className="form-control"
                          name="input_description"
                          type="text"
                          maxLength={2000}
                          value={this.state.input_description || ""}
                          onChange={e => {
                            this.setState({
                              input_description: e.target.value,
                              isModified: true
                            });
                          }}
                          style={{ height: 75 }}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--blue" onClick={this.openConfirm}>
                <FormattedMessage id="save">{text => text}</FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    urlList: state.blackcc.get("urlList")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(UpdateUrlPopup);
