import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../../store/modules/blackcc";
import Draggable from "react-draggable";

import DatePicker from "react-datepicker";
import isAfter from "date-fns/is_after";
import "react-datepicker/dist/react-datepicker.css";

import { isValid } from "./../../../utils";
import { Textbox, Select } from "react-inputs-validation";
import CommonConfirmPopup from "../../Common/Popups/CommonConfirmPopup";
import { FormattedMessage } from "react-intl";
//import IntlMessages from "util/IntlMessages";

// const inputIpTypeOpt = [
//   {
//     id: "R",
//     name: <IntlMessages id="range" />
//   },
//   {
//     id: "S",
//     name: <IntlMessages id="single.item" />
//   }
// ];

const intToIp = ipInt => {
  return (
    (ipInt >>> 24) +
    "." +
    ((ipInt >> 16) & 255) +
    "." +
    ((ipInt >> 8) & 255) +
    "." +
    (ipInt & 255)
  );
};
const ipToInt = ip => {
  return (
    ip.split(".").reduce(function(ipInt, octet) {
      return (ipInt << 8) + parseInt(octet, 10);
    }, 0) >>> 0
  );
};
const isValidIp = ip => {
  return /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
    ip
  );
};

class UpdateIpPopup extends React.Component {
  state = {
    selectedRowId: "",
    input_country: "",
    input_attkType: "",
    input_dstnct: "BLACK",
    input_description: "",
    input_stIp: "",
    input_edIp: "",
    cvted_stIp: "",
    cvted_edIp: "",
    input_companyId: "",
    input_deviceIp: "",
    input_detectDate: null,
    input_expDate: null,
    input_ipType: "R",
    showConfirm: false,
    resource: {},
    validate: false,
    isEmpty: false,
    isModified: false,
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const rowInfo = this.props.rowData;

    // popup 정중앙 계산
    const elWidth = document.getElementById("updateIpPopup").offsetWidth;
    const elHeight = document.getElementById("updateIpPopup").offsetHeight;

    this.setState({
      selectedRowId: rowInfo.selectedRowId,
      input_dstnct: rowInfo.dstnct,
      input_stIp: rowInfo.stIp,
      input_edIp: rowInfo.edIp,
      input_ipType: rowInfo.ipType,
      input_country: rowInfo.country,
      input_detectDate: rowInfo.detectDate
        ? new Date(rowInfo.detectDate)
        : null,
      input_expDate: rowInfo.expDate ? new Date(rowInfo.expDate) : null,
      input_attkType: rowInfo.attkType ? rowInfo.attkType : null,
      input_description: rowInfo.description,
      input_enabledYn: "Y",
      input_companyId: rowInfo.companyId,
      input_deviceIp: rowInfo.deviceIp,
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  openConfirm = async () => {
    this.setState({ validate: true });
    const { BlackccActions } = this.props;
    const {
      input_stIp,
      input_edIp,
      input_country,
      input_deviceIp,
      selectedRowId
    } = this.state;
    let duplRx = {};

    let isIpSt = isValidIp(input_stIp),
      isIpEd = isValidIp(input_edIp),
      stIp = ipToInt(input_stIp),
      edIp = input_edIp !== undefined ? ipToInt(input_edIp) : undefined,
      deviceIp = ipToInt(input_deviceIp),
      schType = this.state.input_ipType;

    if (isValid(this.state.input_country)) {
      if (this.props.cntyCode.indexOf(input_country) === -1) {
        this.showPopup(
          "ALERT",
          <FormattedMessage id="wrong.country.code">
            {text => text}
          </FormattedMessage>
        );
        return;
      }
    }

    if (schType === "S") {
      if (!this.state.isModified) {
        return;
      } // 변경유무 검사
      if (!isIpSt) {
        return;
      } //IP 형식 검사

      let updateChkParam = {
        ipListSeq: selectedRowId,
        stIp: stIp
      };

      duplRx = await BlackccActions.getIpDuplUpdate(updateChkParam);
      if (duplRx.data.length !== 0) {
        let duplMsg = `[${intToIp(duplRx.data.stIp)} ~ ${intToIp(
          duplRx.data.edIp
        )}]`;
        this.showPopup(
          "ALERT",
          <FormattedMessage id="already.ip.reg">
            {text => text}
          </FormattedMessage>,
          duplMsg
        );
        return;
      }

      if (!isValid(this.state.input_detectDate)) {
        this.setState({ isEmpty: true });
        return;
      } //감지일 유무 검사

      this.setState(
        {
          cvted_stIp: stIp,
          cvted_deviceIp: deviceIp,
          isEmpty: false
        },
        () => {
          this.showPopup(
            "CONFIRM",
            <FormattedMessage id="do.edit"> {text => text} </FormattedMessage>
          );
        }
      );
    } else if (schType === "R") {
      if (!isValid(input_stIp) && !isValid(input_edIp)) {
      } else if (!isValid(input_stIp) && isValid(input_edIp)) {
      } else if (isValid(input_stIp) && !isValid(input_edIp)) {
        if (!this.state.isModified) {
          return;
        } // 변경유무 검사
        if (!isIpSt) {
          return;
        } //IP 형식 검사

        let updateChkParam = {
          ipListSeq: selectedRowId,
          stIp: stIp
        };

        duplRx = await BlackccActions.getIpDuplUpdate(updateChkParam);
        if (duplRx.data.length !== 0) {
          let duplMsg = `[${intToIp(duplRx.data.stIp)} ~ ${intToIp(
            duplRx.data.edIp
          )}]`;
          this.showPopup(
            "ALERT",
            <FormattedMessage id="already.ip.reg">
              {text => text}
            </FormattedMessage>,
            duplMsg
          );
          return;
        }

        if (!isValid(this.state.input_detectDate)) {
          this.setState({ isEmpty: true });
          return;
        } // 감지일 유무 검사

        this.setState(
          {
            cvted_stIp: stIp,
            isEmpty: false
          },
          () => {
            this.showPopup(
              "CONFIRM",
              <FormattedMessage id="do.edit"> {text => text} </FormattedMessage>
            );
          }
        );
      } else {
        // Range IP 검색

        if (!this.state.isModified) {
          return;
        } // 변경유무 검사
        if (!isIpSt || !isIpEd) {
          return;
        } //IP 형식 검사
        if (stIp > edIp) {
          return;
        } //범위 유효성 검사

        let updateChkParam = {
          ipListSeq: selectedRowId,
          stIp: stIp,
          edIp: edIp
        };

        duplRx = await BlackccActions.getIpsDuplUpdate(updateChkParam);
        let len = duplRx.data.length;
        if (len !== 0) {
          let duplMsg = `[${intToIp(duplRx.data[0].stIp)} ~${intToIp(
            duplRx.data[0].edIp
          )} ${
            len > 1
              ? (
                  <FormattedMessage id="others">
                    {text => text}
                  </FormattedMessage>
                ) +
                (len - 1) +
                <FormattedMessage id="count"> {text => text}</FormattedMessage>
              : ""
          }]`;
          this.showPopup(
            "ALERT",
            <FormattedMessage id="already.ip.reg">
              {text => text}
            </FormattedMessage>,
            duplMsg
          );
          return;
        }
        if (!isValid(this.state.input_detectDate)) {
          this.setState({ isEmpty: true });
          return;
        } // 감지일 유무 검사

        this.setState(
          {
            cvted_stIp: stIp,
            cvted_edIp: edIp,
            isEmpty: false
          },
          () => {
            this.showPopup(
              "CONFIRM",
              <FormattedMessage id="do.edit"> {text => text} </FormattedMessage>
            );
          }
        );
      }
    }
  };

  showPopup = (type, showedMsg, data) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: (
          <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
        ),
        message: showedMsg,
        data: data,
        hasChangeReason: false,
        btns:
          type === "CONFIRM"
            ? [
                {
                  name: (
                    <FormattedMessage id="cancel">
                      {text => text}
                    </FormattedMessage>
                  ),
                  CB: () => this.onCloseConfirm()
                },
                {
                  name: (
                    <FormattedMessage id="confirm">
                      {text => text}
                    </FormattedMessage>
                  ),
                  CB: () => this.onUpdateConfirm()
                }
              ]
            : [
                {
                  name: (
                    <FormattedMessage id="confirm">
                      {text => text}
                    </FormattedMessage>
                  ),
                  CB: () => this.onCloseConfirm()
                }
              ]
      }
    });
  };

  resetState = () => {
    this.setState({
      input_country: "",
      input_attkType: "",
      input_dstnct: "BLACK",
      input_description: "",
      input_stIp: "",
      input_edIp: "",
      cvted_stIp: "",
      cvted_edIp: "",
      cvted_deviceIp: "",
      input_companyId: "",
      input_deviceIp: "",
      input_detectDate: null,
      input_expDate: null,
      isModified: false
    });
  };

  onCloseConfirm = () => {
    this.setState({
      showConfirm: false,
      isEmpty: false
    });
  };

  onUpdateConfirm = async () => {
    const { BlackccActions, onClose } = this.props;
    const { type, ip } = this.state;

    let offset = this.state.input_detectDate.getTimezoneOffset() * 60 * 1000;
    const rx = await BlackccActions.putIp({
      ipListSeq: this.state.selectedRowId,
      ipDstnct: this.state.input_dstnct,
      stIp: this.state.cvted_stIp,
      edIp: this.state.cvted_edIp || this.state.cvted_stIp, // 단건 입력 시 동일한 ip 입력,
      country: this.state.input_country,
      attkType: this.state.input_attkType,
      detectDate: this.state.input_detectDate
        ? new Date(this.state.input_detectDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      expDate: this.state.input_expDate
        ? new Date(this.state.input_expDate.getTime() - offset)
            .toISOString()
            .substring(0, 10)
        : null,
      ipDesc: this.state.input_description,
      companyId: this.state.input_companyId,
      deviceIp: this.state.cvted_deviceIp,
      useYn: "Y"
    });

    if (rx.status === 200) {
      this.onCloseConfirm();
      this.resetState();
      await BlackccActions.getIpList({ type, ip });
      onClose();
    } else if (rx.status !== 200) {
      this.onCloseConfirm();
    }
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  handleChange = ({ input_detectDate, input_expDate }) => {
    input_detectDate = input_detectDate || this.state.input_detectDate;
    input_expDate = input_expDate || this.state.input_expDate;
    if (isAfter(input_detectDate, input_expDate) && isValid(input_expDate)) {
      input_expDate = input_detectDate;
    }
    this.setState({ input_detectDate, input_expDate, isModified: true });
  };

  handleChangeDetDt = input_detectDate =>
    this.handleChange({ input_detectDate });
  handleChangeExpDt = input_expDate => this.handleChange({ input_expDate });

  render() {
    const { position, showConfirm } = this.state;
    const { onClose } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup" style={{ width: "650px" }} id="updateIpPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <FormattedMessage id="ip.mod">{text => text}</FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div className="popup__body">
              <div className="option-wrapper">
                <table className="table table--white table--info">
                  <colgroup>
                    <col width="100px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <FormattedMessage id="division">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>{this.state.input_dstnct}</td>
                    </tr>
                    {this.state.input_dstnct === "WHITE" ? (
                      <React.Fragment>
                        <tr>
                          <th>
                            Company ID
                            <span
                              className="font--red"
                              style={{ color: "red" }}
                            >
                              *
                            </span>
                          </th>
                          <td>
                            <Textbox
                              type="text"
                              value={this.state.input_companyId}
                              onChange={(name, e) => {
                                this.setState({
                                  input_companyId: e.target.value,
                                  isModified: true
                                });
                              }}
                              onBlur={e => {}}
                              validationOption={{
                                required: false,
                                reg: /^[0-9]+$/,
                                regMsg: (
                                  <FormattedMessage id="number.only">
                                    {text => text}
                                  </FormattedMessage>
                                ),
                                customFunc: res => {
                                  if (res === "") {
                                    return (
                                      <FormattedMessage id="require.item">
                                        {text => text}
                                      </FormattedMessage>
                                    );
                                  }
                                  if (this.state.input_companyId === "") {
                                    return true;
                                  }
                                  return true;
                                },
                                locale: this.props.language
                              }}
                            />
                          </td>
                        </tr>
                        <tr>
                          <th>
                            Device IP
                            <span
                              className="font--red"
                              style={{ color: "red" }}
                            >
                              *
                            </span>
                          </th>
                          <td>
                            <Textbox
                              id="deviceIp"
                              type="text"
                              value={this.state.input_deviceIp}
                              onChange={(name, e) => {
                                this.setState({
                                  input_deviceIp: e.target.value,
                                  isModified: true
                                });
                              }}
                              onBlur={e => {}}
                              validationOption={{
                                reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                regMsg: (
                                  <FormattedMessage id="wrong.ip.type">
                                    {text => text}
                                  </FormattedMessage>
                                ),
                                customFunc: res => {
                                  if (res === "") {
                                    return (
                                      <FormattedMessage id="require.item">
                                        {text => text}
                                      </FormattedMessage>
                                    );
                                  }
                                  if (this.state.input_deviceIp === "") {
                                    return true;
                                  }
                                  return true;
                                },
                                locale: this.props.language
                              }}
                            />
                          </td>
                        </tr>
                      </React.Fragment>
                    ) : (
                      <React.Fragment></React.Fragment>
                    )}
                    <tr>
                      <th style={{ verticalAlign: "middle" }}>
                        <span style={{ color: "black" }}>IP</span>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <div>
                          <Textbox
                            id="stip"
                            type="text"
                            maxLength={30}
                            value={this.state.input_stIp}
                            onChange={e => {
                              this.setState({
                                input_stIp: e,
                                isModified: true
                              });
                            }}
                            onBlur={e => {}}
                            validate={this.state.validate}
                            validationCallback={res => {
                              this.setState({
                                hasNameError: res,
                                validate: false
                              });
                            }}
                            validationOption={{
                              reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                              regMsg: (
                                <FormattedMessage id="wrong.ip.type">
                                  {text => text}
                                </FormattedMessage>
                              ),
                              customFunc: res => {
                                if (res === "") {
                                  return (
                                    <FormattedMessage id="require.item">
                                      {text => text}
                                    </FormattedMessage>
                                  );
                                }
                                return true;
                              },
                              locale: this.props.language
                            }}
                          />
                        </div>
                      </td>
                    </tr>
                    {/* {this.state.input_dstnct === 'WHITE' ? (
                    
                    ) : (<tr>
                      <th style={{ height: '55px', verticalAlign: 'top' }}>
                        <span style={{ color: 'black' }}>IP</span>
                        <span className="font--red" style={{ color: 'red' }}>
                          *
                        </span>
                      </th>
                      <td style={{ display: 'flex' }}>
                        <Select
                          tabIndex="1"
                          value={this.state.input_ipType}
                          optionList={inputIpTypeOpt}
                          onChange={(schType, e) => {
                            if (
                              this.state.input_ipType === 'R' &&
                              schType === 'S'
                            ) {
                              this.setState({
                                input_ipType: schType,
                                input_edIp: '',
                                isModified: true
                              });
                            } else if (
                              this.state.input_ipType === 'S' &&
                              schType === 'R'
                            ) {
                              this.setState({
                                input_ipType: schType,
                                isModified: true
                              });
                            }
                          }}
                          onBlur={() => {}}
                          customStyleWrapper={{
                            width: '60px',
                            marginRight: 10,
                          }}
                        />
                        {this.state.input_ipType === 'R' ? (
                          <div>
                            <div
                              style={{ display: 'flex', alignItems: 'center' }}>
                              <Textbox
                                type="text"
                                maxLength={30}
                                value={this.state.input_stIp}
                                onChange={e => {
                                  this.setState({ 
                                    input_stIp: e,
                                    isModified: true
                                  });
                                }}
                                onBlur={e => {}} // required.
                                customStyleWrapper={{
                                  marginRight: 5,
                                  height: '50px',
                                }}
                                validate={this.state.validate}
                                validationCallback={res => {
                                  this.setState({
                                    hasNameError: res,
                                    validate: false,
                                  });
                                }}
                                validationOption={{
                                  reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                  regMsg: '유효하지 않은 IP 형식입니다.',
                                  customFunc: res => {
                                    if (res === '') {
                                      return '해당항목은 필수항목입니다.';
                                    }
                                    if (this.state.input_edIp === '') {
                                      return true;
                                    }
                                    if (
                                      ipToInt(res) >
                                      ipToInt(this.state.input_edIp)
                                    ) {
                                      return '유효하지 않은 IP 범위입니다.';
                                    }
                                    return true;
                                  },
                                }}
                              />
                              <span style={{ height: '50px', color: 'black' }}>
                                {' '}
                                ~{' '}
                              </span>
                              <Textbox
                                type="text"
                                maxLength={30}
                                value={this.state.input_edIp}
                                onChange={e => {
                                  this.setState({ 
                                    input_edIp: e,
                                    isModified: true
                                  });
                                }}
                                onBlur={e => {}}
                                customStyleWrapper={{
                                  marginLeft: 5,
                                  height: '50px',
                                }}
                                validate={this.state.validate}
                                validationCallback={res => {
                                  this.setState({
                                    hasNameError: res,
                                    validate: false,
                                  });
                                }}
                                validationOption={{
                                  required: false,
                                  reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                  regMsg: '유효하지 않은 IP 형식입니다.',
                                  customFunc: res => {
                                    return true;
                                  },
                                }}
                              />
                            </div>
                          </div>
                        ) : (
                          <div style={{ height: '45px' }}>
                            <Textbox
                              type="text"
                              maxLength={30}
                              value={this.state.input_stIp}
                              onChange={e => {
                                this.setState({ 
                                  input_stIp: e,
                                  isModified: true
                                });
                              }}
                              onBlur={e => {}}
                              validate={this.state.validate}
                              validationCallback={res => {
                                this.setState({
                                  hasNameError: res,
                                  validate: false,
                                });
                              }}
                              validationOption={{
                                reg: /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/,
                                regMsg: '유효하지 않은 IP 형식입니다.',
                                customFunc: res => {
                                  if (res === '') {
                                    return '해당항목은 필수항목입니다.';
                                  }
                                  return true;
                                },
                              }}
                            />
                          </div>
                        )}
                      </td>
                    </tr>)} */}
                    <tr>
                      <th>
                        <FormattedMessage id="country">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" />
                      </th>
                      <td>
                        <Textbox
                          type="text"
                          value={this.state.input_country}
                          maxLength={2}
                          onChange={(name, e) => {
                            this.setState({
                              input_country: e.target.value.toUpperCase(),
                              isModified: true
                            });
                          }}
                          onBlur={e => {}}
                          validationOption={{
                            required: false,
                            reg: /^[a-zA-Z]+$/,
                            regMsg: (
                              <FormattedMessage id="accept.country.code">
                                {text => text}
                              </FormattedMessage>
                            ),
                            locale: this.props.language,
                            customFunc: res =>
                              res === "" ||
                              this.props.cntyCode.indexOf(res) !== -1 ? (
                                true
                              ) : (
                                <FormattedMessage id="wrong.country.code">
                                  {text => text}
                                </FormattedMessage>
                              )
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="detect.date">
                          {text => text}
                        </FormattedMessage>
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_detectDate}
                          onChange={this.handleChangeDetDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsStart
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                        <span style={{ color: "red" }}>
                          {this.state.isEmpty ? (
                            <FormattedMessage id="require.detect.date">
                              {text => text}
                            </FormattedMessage>
                          ) : (
                            ""
                          )}
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <FormattedMessage id="expire.date">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <DatePicker
                          className="form-control"
                          dateFormat="yyyy-MM-dd"
                          selected={this.state.input_expDate}
                          onChange={this.handleChangeExpDt}
                          startDate={this.state.input_detectDate}
                          endDate={this.state.input_expDate}
                          selectsEnd
                          peekNextMonth
                          showMonthDropdown
                          showYearDropdown
                          dropdownMode="select"
                        />
                      </td>
                    </tr>
                    {this.state.input_dstnct !== "WHITE" ? (
                      <tr>
                        <th>
                          <FormattedMessage id="attack.type">
                            {text => text}
                          </FormattedMessage>
                        </th>
                        <td>
                          <Select
                            id="attkType"
                            value={this.state.input_attkType || ""}
                            optionList={this.props.attkCode}
                            onChange={(attk, e) => {
                              this.setState({
                                input_attkType: attk,
                                isModified: true
                              });
                            }}
                            onBlur={() => {}}
                            customStyleWrapper={{ width: "200px" }}
                          />
                        </td>
                      </tr>
                    ) : (
                      ""
                    )}
                    <tr>
                      <th>
                        <FormattedMessage id="desc">
                          {text => text}
                        </FormattedMessage>
                      </th>
                      <td>
                        <textarea
                          className="form-control"
                          type="text"
                          maxLength={2000}
                          onChange={e => {
                            this.setState({
                              input_description: e.target.value,
                              isModified: true
                            });
                          }}
                          value={this.state.input_description || ""}
                          style={{ height: 75 }}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--blue" onClick={this.openConfirm}>
                <FormattedMessage id="save">{text => text}</FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    ipList: state.blackcc.get("ipList"),
    language: state.common.get("language")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(UpdateIpPopup);
