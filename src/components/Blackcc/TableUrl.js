import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../store/modules/blackcc";

import GridTable from "./../Common/GridTable";
import ReactTooltip from "react-tooltip";

import UpdateUrlPopup from "./popups/UpdateUrlPopup";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";
import { getCodeList } from "../../store/api/common";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class TableUrl extends React.Component {
  state = {
    selectedRowId: "",
    input_dstnct: "",
    input_url: "",
    input_country: "",
    input_detectDate: "",
    input_expDate: "",
    input_attkType: "",
    input_enabledYn: "",
    input_description: "",
    attkCode: [],
    isShowUpdate: false,
    showConfirm: false,
    resource: {},
    rowData: "",
    loading: false
  };

  componentDidMount = async () => {
    const attkCode = await getCodeList("BLK_ATTK");
    this.setState({
      attkCode: attkCode.data
    });
  };

  openUpdatePopup = () => {
    this.setState({ isShowUpdate: true });
  };

  closeUpdatePopup = () => {
    this.setState({ isShowUpdate: false });
  };

  openDeletePopup = () => {
    this.setState({
      showConfirm: true,
      resource: {
        title: (
          <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
        ),
        message: (
          <FormattedMessage id="do.delete"> {text => text} </FormattedMessage>
        ),
        hasChangeReason: false,
        btns: [
          {
            name: (
              <FormattedMessage id="cancel"> {text => text} </FormattedMessage>
            ),
            CB: () => this.closeDeletePopup()
          },
          {
            name: (
              <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
            ),
            CB: () => this.onDeleteConfirm()
          }
        ]
      }
    });
  };

  closeDeletePopup = () => {
    this.setState({ showConfirm: false });
  };

  onDeleteConfirm = async () => {
    const { BlackccActions } = this.props;

    let rx = "";
    rx = await BlackccActions.putUrl({
      urlListSeq: this.state.selectedRowId,
      urlDstnct: this.state.input_dstnct,
      url: this.state.input_url,
      attkType: this.state.input_attkType,
      detectDate: this.state.input_detectDate,
      expDate: this.state.input_expDate,
      useYn: "N",
      urlDesc: this.state.input_description
    });

    if (rx.status === 200) {
      this.closeDeletePopup();
      await BlackccActions.getUrlList({});
    } else {
    }
  };

  render() {
    const {
      initTable,
      urlList,
      chkBoxSingleChange,
      chkBoxAllChange,
      state
    } = this.props;

    const { showConfirm, isShowUpdate } = this.state;

    const columns_url = [
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        sortable: false,
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  checked={state.checked[row.index] || ""}
                  onChange={() => chkBoxSingleChange(row.index)}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        width: 40
      },
      {
        Header: <IntlMessages id="url.type" />,
        accessor: "urlDstnct",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="url" />,
        accessor: "url",
        style: { textAlign: "left" }
      },
      {
        Header: <IntlMessages id="detect.date" />,
        accessor: "detectDate",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="expire.date" />,
        accessor: "expDate",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="attack.type" />,
        accessor: "attkTypeString",
        style: { textAlign: "center" },
        width: 150
      },
      {
        Header: <IntlMessages id="desc" />,
        accessor: "urlDesc",
        style: { textAlign: "left" },
        Cell: props => {
          const tooltipId = `urlDescTooltip_${props.original.ipListSeq}`;
          return (
            <React.Fragment>
              <ReactTooltip
                id={tooltipId}
                className="tooltipDesc"
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={150}
                event="mouseover"
                globalEventOff="mouseout"
              >
                {props.value}
              </ReactTooltip>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
            </React.Fragment>
          );
        }
      },
      {
        Header: <IntlMessages id="reg.user" />,
        accessor: "regUsrNm",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="reg.date" />,
        accessor: "stringRegDate",
        style: { textAlign: "center" },
        width: 120
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "modUsrNm",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="mod.date" />,
        accessor: "stringModDate",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: "Actions",
        accessor: "icon",
        style: { textAlign: "center" },
        width: 70,
        Cell: row => {
          return (
            <div>
              <button id="urlDelete" className="btn btn-icon">
                <img
                  id="urlDelete"
                  src="/images/common/icon_delete.png"
                  alt="icon_delete"
                />
              </button>
              <button id="urlUpdate" className="btn btn-icon">
                <img
                  id="urlUpdate"
                  src="/images/common/icon_edit.png"
                  alt="icon_edit"
                />
              </button>
            </div>
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <GridTable
          columns={columns_url}
          data={urlList.content}
          className="-striped -highlight"
          onFetchData={initTable}
          getTrProps={(state, rowInfo, column, instance) => {
            // 4개 모두 있어야 정상작동 rowInfo.original
            return {
              onClick: async e => {
                try {
                  if (e.target.id === "urlUpdate") {
                    let rowData = {
                      selectedRowId: rowInfo.original.urlListSeq,
                      dstnct: rowInfo.original.urlDstnct,
                      url: rowInfo.original.url,
                      country: rowInfo.original.country,
                      detectDate: rowInfo.original.detectDate,
                      expDate: rowInfo.original.expDate,
                      attkType: rowInfo.original.attkType,
                      enabledYn: rowInfo.original.useYn,
                      description: rowInfo.original.urlDesc
                    };
                    this.setState({ rowData: rowData, isShowUpdate: true });
                  } else if (e.target.id === "urlDelete") {
                    this.setState(
                      {
                        selectedRowId: rowInfo.original.urlListSeq,
                        input_dstnct: rowInfo.original.urlDstnct,
                        input_url: rowInfo.original.url,
                        input_country: rowInfo.original.country,
                        input_detectDate: rowInfo.original.detectDate,
                        input_expDate: rowInfo.original.expDate,
                        input_attkType: rowInfo.original.attkType,
                        input_enabledYn: rowInfo.original.useYn,
                        input_description: rowInfo.original.urlDesc
                      },
                      () => {
                        this.openDeletePopup();
                      }
                    );
                  }
                } catch (e) {
                  console.log(e);
                }
              }
            };
          }}
        />
        {isShowUpdate ? (
          <UpdateUrlPopup
            rowData={this.state.rowData}
            onClose={this.closeUpdatePopup}
            attkCode={this.state.attkCode}
          />
        ) : (
          <div></div>
        )}
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    urlList: state.blackcc.get("urlList")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(TableUrl);
