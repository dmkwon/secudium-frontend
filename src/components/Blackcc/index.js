import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../store/modules/blackcc";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import Loading from "../Common/Loading";

import SchIpComponent from "./SchIpComponent";
import SchUrlComponent from "./SchUrlComponent";
import TableIp from "./TableIp";
import TableUrl from "./TableUrl";

import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";
import { getCodeList } from "../../store/api/common";

import { FormattedMessage } from "react-intl";

import "./style.scss";

class BlackccComponent extends Component {
  state = {
    stdTab: "ipTab",
    loading: true,
    timestamp: "",
    selectAll: false,
    checked: [],
    paramList: [],
    showConfirm: false,
    resource: {},
    attkCode: [],
    cntyCode: []
  };

  componentDidMount = async () => {
    const attkCode = await getCodeList("BLK_ATTK");
    const cntyCodeObj = await getCodeList("COUNTRY_CODE");
    let cntyCode = [];
    cntyCodeObj.data.forEach(el => {
      cntyCode.push(el.id);
    });
    this.setState({
      attkCode: attkCode.data,
      cntyCode: cntyCode
    });
  };

  chkBoxAllChange = () => {
    const { ipList, urlList } = this.props;
    const { stdTab } = this.state;
    let checkArr = [];
    if (stdTab === "ipTab") {
      if (!this.state.selectAll) {
        for (let i = 0; i < ipList.content.length; i++) {
          checkArr.push(true);
        }
        this.setState({ selectAll: true, checked: checkArr });
      } else {
        for (let i = 0; i < ipList.content.length; i++) {
          checkArr.push(false);
        }
        this.setState({ selectAll: false, checked: checkArr });
      }
    } else if (stdTab === "urlTab") {
      if (!this.state.selectAll) {
        for (let i = 0; i < urlList.content.length; i++) {
          checkArr.push(true);
        }
        this.setState({ selectAll: true, checked: checkArr });
      } else {
        for (let i = 0; i < urlList.content.length; i++) {
          checkArr.push(false);
        }
        this.setState({ selectAll: false, checked: checkArr });
      }
    }
  };

  chkBoxSingleChange = index => {
    const { ipList, urlList } = this.props;
    const { stdTab } = this.state;
    let checkArr = this.state.checked;
    if (stdTab === "ipTab") {
      if (checkArr.length === 0) {
        for (let i = 0; i < ipList.content.length; i++) {
          checkArr.push(false);
        }
        checkArr.splice(index, 1, true);
        this.setState({ checked: checkArr });
      } else {
        if (checkArr[index]) {
          checkArr.splice(index, 1, false);
        } else {
          checkArr.splice(index, 1, true);
        }
        this.setState({ checked: checkArr });
      }
    } else if (stdTab === "urlTab") {
      if (checkArr.length === 0) {
        for (let i = 0; i < urlList.content.length; i++) {
          checkArr.push(false);
        }
        checkArr.splice(index, 1, true);
        this.setState({ checked: checkArr });
      } else {
        if (checkArr[index]) {
          checkArr.splice(index, 1, false);
        } else {
          checkArr.splice(index, 1, true);
        }
        this.setState({ checked: checkArr });
      }
    }
  };

  //탭 변경
  onConvertTab = tab => {
    const { BlackccActions } = this.props;
    let type = "",
      ip = "",
      url = "";
    this.setState({ type: "", checked: [] }, async () => {
      try {
        switch (tab) {
          case "ipTab":
            this.setState({
              stdTab: "ipTab",
              selectAll: false,
              checked: [],
              paramList: []
            });
            await BlackccActions.getIpList({ type, ip });
            break;
          case "urlTab":
            this.setState({
              stdTab: "urlTab",
              selectAll: false,
              checked: [],
              paramList: []
            });
            await BlackccActions.getUrlList({ type, url });
            break;
          default:
            break;
        }
      } catch (e) {
        console.log(e);
      }
    });
  };

  //팝업 confirm 시 화면숨김 및 새로고침.
  closeAllPopup = () => {
    const { stdTab } = this.state;
    const { BlackccActions } = this.props;
    this.onClosePopup();
    if (stdTab === "ipTab") {
      BlackccActions.getIpList();
    } else if (stdTab === "urlTab") {
      BlackccActions.getUrlList();
    }
  };

  onDeployHandler = async () => {
    const { BlackccActions } = this.props;
    try {
      await BlackccActions.deployAll();
    } catch (err) {
      console.error("err", err);
    }
  };

  //checkbox 선택삭제 전 데이터 셋팅
  onCheckDeleteHandler = e => {
    const { stdTab } = this.state;
    const { urlList, ipList } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];

    if (stdTab === "ipTab") {
      if (checkArr.length === 0) {
        this.showAlert(
          <FormattedMessage id="confirm">{text => text}</FormattedMessage>,
          <FormattedMessage id="no.selection">{text => text}</FormattedMessage>
        );
      } else {
        let stdExists = false;
        for (let idx = 0; idx < checkArr.length; idx++) {
          if (checkArr[idx]) {
            paramList.push(ipList.content[idx]);
            stdExists = true;
          }
        }
        if (stdExists) {
          this.setState({ paramList: paramList });
          this.openDelListPopup();
        } else {
          this.showAlert(
            <FormattedMessage id="confirm">{text => text}</FormattedMessage>,
            <FormattedMessage id="no.selection">
              {text => text}
            </FormattedMessage>
          );
        }
      }
    } else if (stdTab === "urlTab") {
      if (checkArr.length === 0) {
        this.showAlert(
          <FormattedMessage id="confirm">{text => text}</FormattedMessage>,
          <FormattedMessage id="no.selection">{text => text}</FormattedMessage>
        );
      } else {
        let stdExists = false;
        for (let idx = 0; idx < checkArr.length; idx++) {
          if (checkArr[idx]) {
            paramList.push(urlList.content[idx]);
            stdExists = true;
          }
        }
        if (stdExists) {
          this.setState({ paramList: paramList });
          this.openDelListPopup();
        } else {
          this.showAlert(
            <FormattedMessage id="confirm">{text => text}</FormattedMessage>,
            <FormattedMessage id="no.selection">
              {text => text}
            </FormattedMessage>
          );
        }
      }
    }
  };

  openDelListPopup = () => {
    this.setState({
      showConfirm: true,
      resource: {
        title: (
          <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
        ),
        message: (
          <FormattedMessage id="do.delete"> {text => text} </FormattedMessage>
        ),
        hasChangeReason: false,
        btns: [
          {
            name: (
              <FormattedMessage id="cancel"> {text => text} </FormattedMessage>
            ),
            CB: () => this.onClosePopup()
          },
          {
            name: (
              <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
            ),
            CB: () => this.onDeleteListConfirm()
          }
        ]
      }
    });
  };

  onClosePopup = () => {
    this.setState({ showConfirm: false });
  };

  //checkbox 선택삭제 confirm
  onDeleteListConfirm = async () => {
    const { BlackccActions } = this.props;
    const { stdTab, paramList } = this.state;
    let rx = "";

    if (stdTab === "ipTab") {
      rx = await BlackccActions.deleteIps(paramList);
      await BlackccActions.getIpList({});
    } else if (stdTab === "urlTab") {
      rx = await BlackccActions.deleteUrls(paramList);
      await BlackccActions.getUrlList({});
    }

    if (rx.status === 200) {
      this.closeAllPopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: []
      });
    } else {
      this.onClosePopup();
    }
  };

  initTable = async e => {
    const { BlackccActions } = this.props;
    const { ip, url, stdTab } = this.state;
    let type = "";

    if (stdTab === "ipTab") {
      await BlackccActions.getIpList({ type, ip }, () => {});
    } else if (stdTab === "urlTab") {
      await BlackccActions.getUrlList({ type, url }, () => {});
    }
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          {
            name: (
              <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
            ),
            CB: () => this.onClosePopup()
          }
        ]
      }
    });
  };

  render() {
    const { showConfirm } = this.state;

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper blackc2">
          <LeftMenu />
          <div className="component component-right">
            <ul className="component-tab">
              <li
                onClick={() => this.onConvertTab("ipTab")}
                className={this.state.stdTab === "ipTab" ? "active" : ""}
              >
                IP
              </li>
              <li
                onClick={() => this.onConvertTab("urlTab")}
                className={this.state.stdTab === "urlTab" ? "active" : ""}
              >
                URL
              </li>
            </ul>
            <div className="component component-list">
              {this.state.stdTab === "ipTab" ? (
                <SchIpComponent
                  checked={this.state.checked}
                  onDeployHandler={this.onDeployHandler}
                  onCheckDeleteHandler={this.onCheckDeleteHandler}
                  cntyCode={this.state.cntyCode}
                  attkCode={this.state.attkCode}
                />
              ) : (
                <SchUrlComponent
                  checked={this.state.checked}
                  onDeployHandler={this.onDeployHandler}
                  onCheckDeleteHandler={this.onCheckDeleteHandler}
                />
              )}
              <div className="table-wrapper">
                <div></div>
                {this.state.stdTab === "ipTab" ? (
                  <TableIp
                    state={this.state}
                    initTable={this.initTable}
                    chkBoxSingleChange={this.chkBoxSingleChange}
                    chkBoxAllChange={this.chkBoxAllChange}
                    cntyCode={this.state.cntyCode}
                    attkCode={this.state.attkCode}
                  />
                ) : (
                  <TableUrl
                    state={this.state}
                    initTable={this.initTable}
                    chkBoxSingleChange={this.chkBoxSingleChange}
                    chkBoxAllChange={this.chkBoxAllChange}
                  />
                )}
              </div>
            </div>
          </div>
        </div>
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    ipList: state.blackcc.get("ipList"),
    urlList: state.blackcc.get("urlList"),
    loading: state.blackcc.get("loading"),
    isShowDelListPopup: state.blackcc.get("isShowDelListPopup")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(BlackccComponent);
