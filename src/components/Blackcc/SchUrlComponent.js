import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../store/modules/blackcc";

import DatePicker from "react-datepicker";
import isAfter from "date-fns/is_after";
import "react-datepicker/dist/react-datepicker.css";

import { Select } from "react-inputs-validation";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";
import InsertUrlPopup from "./popups/InsertUrlPopup";
import { isValid } from "./../../utils";
import { getCodeList } from "../../store/api/common";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

const inputDstnctOpt = [
  {
    id: "",
    name: <IntlMessages id="all" />
  },
  { id: "BLACK", name: "Black URL" },
  { id: "CC", name: "Cc URL" }
];

class SchUrlComponent extends React.Component {
  state = {
    type: "",
    url: "",
    sdate: null,
    edate: null,
    attkCode: [],
    isDuplMsg: "",
    isShowInsertPopup: false,
    checked: this.props.checked,
    resource: {},
    showConfirm: false
  };

  componentDidMount = async () => {
    const attkCode = await getCodeList("BLK_ATTK");
    this.setState({
      attkCode: attkCode.data
    });
  };

  onCloseConfirm = () => {
    this.setState({ showConfirm: false });
  };

  onResetSchParam = async e => {
    // const { BlackccActions } = this.props;
    // await BlackccActions.getUrlList({});
    this.setState({
      type: "",
      url: "",
      sdate: null,
      edate: null
    });
  };

  onChangeSchUrl = e => {
    const { id, value } = e.target;
    this.setState({ [id]: value });
  };

  onSearchHandler = async e => {
    const { BlackccActions } = this.props;
    let { type, url, sdate, edate } = this.state;

    if (
      (isValid(sdate) && !isValid(edate)) ||
      (!isValid(sdate) && isValid(edate))
    ) {
      this.setState({
        showConfirm: true,
        resource: {
          title: <IntlMessages id="confirm" />,
          message: <IntlMessages id="enter.date.both" />,
          hasChangeReason: false,
          btns: [
            {
              name: <IntlMessages id="confirm" />,
              CB: () => this.onCloseConfirm()
            }
          ]
        }
      });
      return;
    }

    let offset = 0;
    if (!sdate && !edate) {
    } else if (sdate && !edate) {
      offset = sdate.getTimezoneOffset() * 60 * 1000;
      sdate = new Date(sdate.getTime() - offset).toISOString().substring(0, 10);
    } else if (!sdate && edate) {
      offset = edate.getTimezoneOffset() * 60 * 1000;
      edate = new Date(edate.getTime() - offset).toISOString().substring(0, 10);
    } else {
      offset = sdate.getTimezoneOffset() * 60 * 1000;
      sdate = new Date(sdate.getTime() - offset).toISOString().substring(0, 10);
      edate = new Date(edate.getTime() - offset).toISOString().substring(0, 10);
    }

    await BlackccActions.getUrlList({ type, url, sdate, edate });
  };

  onInsert = e => {
    this.setState({ isShowInsertPopup: true });
  };

  onClose = e => {
    this.setState({ isShowInsertPopup: false });
  };

  handleChange = ({ sdate, edate }) => {
    sdate = sdate || this.state.sdate;
    edate = edate || this.state.edate;
    if (isAfter(sdate, edate)) {
      edate = sdate;
    }
    this.setState({ sdate, edate });
  };

  handleChangeStart = sdate => this.handleChange({ sdate });
  handleChangeEnd = edate => this.handleChange({ edate });

  render() {
    const { onCheckDeleteHandler, onDeployHandler } = this.props;
    const { showConfirm } = this.state;

    return (
      <div className="component-search">
        <IntlMessages id="division" />
        <Select
          name={"input_dstnct"}
          value={this.state.type}
          optionList={inputDstnctOpt}
          onChange={(dstnct, e) => {
            this.setState({ type: dstnct });
          }}
          onBlur={() => {}}
          customStyleWrapper={{
            width: "100px",
            marginRight: 10,
            fontSize: "12px"
          }}
        />
        <div className="component-search__calendar">
          <IntlMessages id="detect.date" />
          <DatePicker
            className="form-control"
            dateFormat="yyyy-MM-dd"
            selected={this.state.sdate}
            onChange={this.handleChangeStart}
            startDate={this.state.sdate}
            endDate={this.state.edate}
            selectsStart
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
          />
          <span style={{ margin: "0 8px" }}>-</span>
          <DatePicker
            className="form-control"
            dateFormat="yyyy-MM-dd"
            selected={this.state.edate}
            onChange={this.handleChangeEnd}
            startDate={this.state.sdate}
            endDate={this.state.edate}
            selectsEnd
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
          />
        </div>
        <IntlMessages id="url" />
        <input
          id="url"
          className="form-control"
          onChange={this.onChangeSchUrl}
          value={this.state.url}
          style={{ width: 150, marginRight: 5 }}
        />
        <button className="btn" onClick={this.onSearchHandler}>
          <IntlMessages id="search" />
        </button>
        {/* <button id="resetSch" className="btn btn-icon" onClick={this.onShowConfirm} style={{marginLeft : 5}}> */}
        <button
          id="resetSch"
          className="btn btn-icon"
          onClick={this.onResetSchParam}
          style={{ marginLeft: 5 }}
        >
          <img
            id="resetSch"
            src="/images/common/icon_rollback.png"
            alt="icon_rollback"
          />
        </button>
        <div className="btns">
          <button id="stdDep" className="btn" onClick={onDeployHandler}>
            <IntlMessages id="deployment" />
          </button>
          <button
            className="btn"
            id="url_insert_mod"
            name="insert_mod"
            onClick={onCheckDeleteHandler}
          >
            <IntlMessages id="multi.delete" />
          </button>
          <button id="regNew" className="btn btn--blue" onClick={this.onInsert}>
            <IntlMessages id="new.reg" />
          </button>
          {this.state.isShowInsertPopup ? (
            <InsertUrlPopup
              onClose={this.onClose}
              attkCode={this.state.attkCode}
            />
          ) : (
            <div></div>
          )}
          {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    urlList: state.blackcc.get("urlList")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(SchUrlComponent);
