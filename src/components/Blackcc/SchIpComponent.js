import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as blackccActions from "../../store/modules/blackcc";

import DatePicker from "react-datepicker";
import isAfter from "date-fns/is_after";
import "react-datepicker/dist/react-datepicker.css";

import { Select } from "react-inputs-validation";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";
import InsertIpPopup from "./popups/InsertIpPopup";

import { isValid } from "./../../utils";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

const ipDstnctOpt = [
  {
    id: "",
    name: <IntlMessages id="all" />
  },
  { id: "BLACK", name: "Black IP" },
  { id: "CC", name: "Cc IP" },
  { id: "WHITE", name: "White IP" }
];

const ipPatrnOpt = [
  {
    id: "R",
    name: <IntlMessages id="range" />
  },
  {
    id: "S",
    name: <IntlMessages id="single.item" />
  }
];

const ipToInt = ip => {
  return (
    ip.split(".").reduce(function(ipInt, octet) {
      return (ipInt << 8) + parseInt(octet, 10);
    }, 0) >>> 0
  );
};
// const isValidIp = (ip) => { return /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ip); }
const isValidIp = ip => {
  return /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(
    ip
  );
};

class SchIpComponent extends React.Component {
  state = {
    type: "",
    ipSchPat: "R",
    sch_stIp: "",
    sch_edIp: "",
    sdate: null,
    edate: null,
    isDupl: "",
    isDuplMsg: "",
    isShowInsertPopup: false,
    checked: this.props.checked,
    resource: {},
    showConfirm: false
  };

  onCloseConfirm = () => {
    this.setState({ showConfirm: false });
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          {
            name: (
              <FormattedMessage id="cancel"> {text => text} </FormattedMessage>
            ),
            CB: () => this.onCloseConfirm()
          }
        ]
      }
    });
  };

  onResetSchParam = async () => {
    // const { BlackccActions } = this.props;
    // await BlackccActions.getIpList({});
    this.setState({
      type: "",
      sch_stIp: "",
      sch_edIp: "",
      sdate: null,
      edate: null
    });
  };

  onChangeSchIp = e => {
    const { id, value } = e.target;
    this.setState({ [id]: value });
  };

  onSearchHandler = async e => {
    const { BlackccActions } = this.props;
    let { type, sch_stIp, sch_edIp, sdate, edate } = this.state;
    let stIp = "";
    let edIp = "";

    if (
      (isValid(sdate) && !isValid(edate)) ||
      (!isValid(sdate) && isValid(edate))
    ) {
      this.setState({
        showConfirm: true,
        resource: {
          title: (
            <FormattedMessage id="confirm"> {text => text} </FormattedMessage>
          ),
          message: (
            <FormattedMessage id="enter.date.both">
              {text => text}
            </FormattedMessage>
          ),
          hasChangeReason: false,
          btns: [
            {
              name: (
                <FormattedMessage id="confirm">{text => text}</FormattedMessage>
              ),
              CB: () => this.onCloseConfirm()
            }
          ]
        }
      });
      return;
    }

    //check list
    let offset = 0;
    if (!sdate && !edate) {
    } else if (sdate && !edate) {
      offset = sdate.getTimezoneOffset() * 60 * 1000;
      sdate = new Date(sdate.getTime() - offset).toISOString().substring(0, 10);
    } else if (!sdate && edate) {
      offset = edate.getTimezoneOffset() * 60 * 1000;
      edate = new Date(edate.getTime() - offset).toISOString().substring(0, 10);
    } else {
      offset = sdate.getTimezoneOffset() * 60 * 1000;
      sdate = new Date(sdate.getTime() - offset).toISOString().substring(0, 10);
      edate = new Date(edate.getTime() - offset).toISOString().substring(0, 10);
    }

    const schPtrn = this.state.ipSchPat;
    if (schPtrn === "S" && sch_stIp.trim() !== "") {
      if (isValidIp(sch_stIp.trim()) === false) {
        this.showAlert(
          <FormattedMessage id="confirm">{text => text}</FormattedMessage>,
          <FormattedMessage id="wrong.ip.type">{text => text}</FormattedMessage>
        );
        return;
      }
      stIp = ipToInt(sch_stIp);
    }

    if (schPtrn === "R" && (sch_stIp.trim() !== "" || sch_edIp.trim() !== "")) {
      if (
        (sch_stIp.trim() !== "" && sch_edIp === "") ||
        (sch_stIp === "" && sch_edIp.trim() !== "")
      ) {
        this.showAlert(
          <FormattedMessage id="confirm"> {text => text} </FormattedMessage>,
          <FormattedMessage id="enter.ip.both">{text => text}</FormattedMessage>
        );
        return;
      }

      if (!isValidIp(sch_stIp.trim()) || !isValidIp(sch_edIp.trim())) {
        this.showAlert(
          <FormattedMessage id="confirm"> {text => text} </FormattedMessage>,
          <FormattedMessage id="wrong.ip.type">{text => text}</FormattedMessage>
        );
        return;
      }

      if (ipToInt(sch_stIp.trim()) > ipToInt(sch_edIp.trim())) {
        this.showAlert(
          <FormattedMessage id="confirm"> {text => text} </FormattedMessage>,
          <FormattedMessage id="wrong.ip.range">
            {text => text}
          </FormattedMessage>
        );
        return;
      }
      stIp = ipToInt(sch_stIp);
      edIp = ipToInt(sch_edIp);
    }

    await BlackccActions.getIpList({ type, stIp, edIp, sdate, edate });
  };

  onInsert = e => {
    this.setState({ isShowInsertPopup: true });
  };

  onClose = e => {
    this.setState({ isShowInsertPopup: false });
  };

  handleChange = ({ sdate, edate }) => {
    sdate = sdate || this.state.sdate;
    edate = edate || this.state.edate;
    if (isAfter(sdate, edate)) {
      edate = sdate;
    }
    this.setState({ sdate, edate });
  };

  handleChangeStart = sdate => this.handleChange({ sdate });
  handleChangeEnd = edate => this.handleChange({ edate });

  render() {
    const {
      onCheckDeleteHandler,
      onDeployHandler,
      cntyCode,
      attkCode
    } = this.props;
    const { showConfirm } = this.state;

    return (
      <div className="component-search">
        <IntlMessages id="division" />
        <Select
          name={"input_dstnct"}
          value={this.state.type}
          optionList={ipDstnctOpt}
          onChange={(dstnct, e) => {
            this.setState({ type: dstnct });
          }}
          onBlur={() => {}}
          customStyleWrapper={{
            width: "100px",
            marginRight: 10,
            fontSize: "12px"
          }}
        />

        <div className="component-search__calendar">
          <IntlMessages id="detect.date" />
          <DatePicker
            className="form-control"
            dateFormat="yyyy-MM-dd"
            selected={this.state.sdate}
            onChange={this.handleChangeStart}
            startDate={this.state.sdate}
            endDate={this.state.edate}
            selectsStart
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
          />
          <span style={{ margin: "0 8px" }}>-</span>
          <DatePicker
            className="form-control"
            dateFormat="yyyy-MM-dd"
            selected={this.state.edate}
            onChange={this.handleChangeEnd}
            startDate={this.state.sdate}
            endDate={this.state.edate}
            selectsEnd
            peekNextMonth
            showMonthDropdown
            showYearDropdown
            dropdownMode="select"
          />
        </div>
        <IntlMessages id="ip" />
        <Select
          name={"input_dstnct"}
          value={this.state.ipSchPat}
          optionList={ipPatrnOpt}
          onChange={(pattern, e) => {
            this.setState({ ipSchPat: pattern });
          }}
          onBlur={() => {}}
          customStyleWrapper={{
            width: "100px",
            marginRight: 10,
            fontSize: "12px"
          }}
        />
        {this.state.ipSchPat === "R" ? (
          <React.Fragment>
            <input
              type="text"
              maxLength="16"
              style={{ width: 130, marginLeft: 10, marginRight: 10 }}
              id="sch_stIp"
              value={this.state.sch_stIp}
              onChange={this.onChangeSchIp}
              className="form-control"
            />
            <span>~</span>
            <input
              type="text"
              maxLength="16"
              style={{ width: 130, marginRight: 5 }}
              id="sch_edIp"
              value={this.state.sch_edIp}
              onChange={this.onChangeSchIp}
              className="form-control"
            />
          </React.Fragment>
        ) : (
          <input
            type="text"
            maxLength="16"
            style={{ width: 130, marginLeft: 10, marginRight: 5 }}
            id="sch_stIp"
            value={this.state.sch_stIp}
            onChange={this.onChangeSchIp}
            className="form-control"
          />
        )}
        <button className="btn" onClick={this.onSearchHandler}>
          <IntlMessages id="search" />
        </button>
        {/* <button id="resetSch" className="btn btn-icon" onClick={this.onShowConfirm} style={{marginLeft : 5}}> */}
        <button
          id="resetSch"
          className="btn btn-icon"
          onClick={this.onResetSchParam}
          style={{ marginLeft: 5 }}
        >
          <img
            id="resetSch"
            src="/images/common/icon_rollback.png"
            alt="icon_rollback"
          />
        </button>

        <div className="btns">
          <button id="stdDep" className="btn" onClick={onDeployHandler}>
            <IntlMessages id="deployment" />
          </button>
          <button id="stdDel" className="btn" onClick={onCheckDeleteHandler}>
            <IntlMessages id="multi.delete" />
          </button>
          <button id="regNew" className="btn btn--blue" onClick={this.onInsert}>
            <IntlMessages id="new.reg" />
          </button>
          {this.state.isShowInsertPopup ? (
            <InsertIpPopup
              onClose={this.onClose}
              cntyCode={cntyCode}
              attkCode={attkCode}
            />
          ) : (
            <div></div>
          )}
          {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    ipList: state.blackcc.get("ipList")
  }),
  dispatch => ({
    BlackccActions: bindActionCreators(blackccActions, dispatch)
  })
)(SchIpComponent);
