import React, { Component } from "react";
import { connect } from "react-redux";
import { getCodeList } from "../../../store/api/common";
import { bindActionCreators } from "redux";
import { Textbox, Textarea, Select } from "react-inputs-validation";
import Draggable from "react-draggable";
import IntlMessages from "util/IntlMessages";

import * as ProgActions from "../../../store/modules/prog";
import { createProg } from "../../../store/api/prog";

import "./style.scss";

class AddProgPopup extends Component {
  state = {
    funcNmKo: "",
    funcNmEn: "",
    funcDesc: "",
    funcUrl: "",
    httpTrnsType: "0",
    funcTypeCode: "0",
    addInitTypes: [{ name: <IntlMessages id="select.options" />, id: "0" }],
    httpTransTypes: [],
    funcTypeCodes: [],

    validation: {
      validate: false,
      hasErrFuncNmKo: true,
      hasErrFuncNmEn: true,
      hasErrFuncUrl: true,
      hasErrFuncDesc: false
    },

    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("addProgpopup").offsetWidth;
    const elHeight = document.getElementById("addProgpopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  onChangeTopFunc = async (topFunc, topFunc2, e) => {
    try {
      const topFuncList = await getCodeList(topFunc);
      const topFuncList2 = await getCodeList(topFunc2);

      this.setState({
        httpTransTypes: this.state.addInitTypes.concat(topFuncList.data),
        funcTypeCodes: this.state.addInitTypes.concat(topFuncList2.data)
      });
    } catch (e) {
      console.warn(e);
    }
  };

  componentWillMount() {
    this.onChangeTopFunc("HTTP_PROTOCOL", "FUNC_TYPE");
  }

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrFuncNmKo,
      hasErrFuncNmEn,
      hasErrFuncUrl,
      hasErrFuncDesc
    } = this.state.validation;

    return (
      !hasErrFuncNmKo && !hasErrFuncNmEn && !hasErrFuncUrl && !hasErrFuncDesc
    );
  };

  // State 변경 handler
  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      [name]: value
    });
  };

  onClickSave = async e => {
    if (this.isValidate()) {
      const { ProgActions } = this.props;
      const InputProg = this.state;
      try {
        await createProg(InputProg);
        await ProgActions.getProgs();
        this.setState({
          funcTypeCode: "",
          funcNmKo: "",
          funcNmEn: "",
          funcDesc: "",
          funcUrl: "",
          httpTrnsType: ""
        });
        this.props.closePopup();
      } catch (e) {
        console.warn(e);
      }
    }
  };

  render() {
    const { handleChangeInput } = this;
    const { position, httpTransTypes, funcTypeCodes } = this.state;
    const { closePopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--move"
            id="addProgpopup"
            style={{ width: "550px" }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="func.add" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="func.name.ko" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="funcNmKo"
                        name="funcNmKo"
                        type="text"
                        tabIndex="1"
                        value={this.state.funcNmKo}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncNmKo: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncNmKo",
                          check: true,
                          required: true,
                          min: 2,
                          max: 100,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="func.name.en" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="funcNmEn"
                        name="funcNmEn"
                        type="text"
                        tabIndex="1"
                        value={this.state.funcNmEn}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncNmEn: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncNmEn",
                          check: true,
                          required: true,
                          min: 2,
                          max: 100,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="func.url" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="funcUrl"
                        name="funcUrl"
                        type="text"
                        tabIndex="2"
                        value={this.state.funcUrl}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncUrl: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncUrl",
                          check: true,
                          required: true,
                          min: 4,
                          max: 255,
                          locale: this.props.language,
                          reg: /^\/([/a-z0-9-%#?&._=\w{}])+$/,
                          regMsg: <IntlMessages id="wrong.url.type" />
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="func.type" />
                    </th>
                    <td>
                      <Select
                        id="funcTypeCode"
                        name="funcTypeCode"
                        tabIndex="3"
                        value={this.state.funcTypeCode}
                        optionList={funcTypeCodes}
                        onChange={(funcTypeCode, e) => {
                          this.setState({
                            funcTypeCode
                          });
                        }}
                        customStyleWrapper={{ width: "100%", resize: "none" }}
                        customStyleOptionListContainer={{
                          maxHeight: "140px",
                          overflow: "auto"
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="http.trans.method" />
                    </th>
                    <td>
                      <Select
                        id="httpTrnsType"
                        name="httpTrnsType"
                        type="text"
                        tabIndex="4"
                        value={this.state.httpTrnsType}
                        optionList={httpTransTypes}
                        onChange={(httpTrnsType, e) => {
                          this.setState({
                            httpTrnsType
                          });
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ verticalAlign: "top" }}>
                      <IntlMessages id="desc" />
                    </th>
                    <td>
                      <Textarea
                        id="funcDesc"
                        name="funcDesc"
                        type="text"
                        tabIndex="5"
                        value={this.state.funcDesc}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncDesc: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncDesc",
                          check: true,
                          required: false,
                          max: 1000,
                          locale: this.props.language
                        }}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "70px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={this.onClickSave}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    ProgActions: bindActionCreators(ProgActions, dispatch)
  })
)(AddProgPopup);
