import React, { Component } from "react";
import { connect } from "react-redux";
import { getCodeList } from "../../../store/api/common";
import { bindActionCreators } from "redux";
import { Textbox, Textarea, Select } from "react-inputs-validation";
import Draggable from "react-draggable";

import * as ProgActions from "../../../store/modules/prog";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class UpdateProgPopup extends Component {
  state = {
    useType: "Y",
    progBySeq: this.props.data,
    httpTransTypes: [],
    funcTypeCodes: [],

    validation: {
      validate: false,
      hasErrFuncNmKo: false,
      hasErrFuncNmEn: false,
      hasErrFuncUrl: false,
      hasErrFuncDesc: false
    },

    position: {
      x: 0,
      y: 0
    }
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrFuncNmKo,
      hasErrFuncNmEn,
      hasErrFuncUrl,
      hasErrFuncDesc
    } = this.state.validation;

    return (
      !hasErrFuncNmKo && !hasErrFuncNmEn && !hasErrFuncUrl && !hasErrFuncDesc
    );
  };

  handleSaveData = async () => {
    if (this.isValidate()) {
      const { ProgActions } = this.props;
      await ProgActions.updateProg(this.state.progBySeq);
      await ProgActions.getProgs();
      this.props.closePopup();
    }
  };

  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      progBySeq: {
        ...this.state.progBySeq,
        [name]: value
      }
    });
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("updateProgPopup").offsetWidth;
    const elHeight = document.getElementById("updateProgPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  onChangeTopFunc = async (topFunc, topFunc2, e) => {
    try {
      const topFuncList = await getCodeList(topFunc);
      const topFuncList2 = await getCodeList(topFunc2);

      this.setState({
        httpTransTypes: topFuncList.data,
        funcTypeCodes: topFuncList2.data
      });
    } catch (e) {
      console.warn(e);
    }
  };

  getProgBySeq = async funcSeq => {
    const { ProgActions } = this.props;
    try {
      await ProgActions.getProgBySeq(funcSeq);
      this.setState({
        progBySeq: this.props.progBySeq
      });
    } catch (e) {
      console.log(e);
    }
  };

  componentWillMount() {
    this.getProgBySeq(this.props.data);
    this.onChangeTopFunc("HTTP_PROTOCOL", "FUNC_TYPE");
  }

  render() {
    const { handleSaveData, handleChangeInput } = this;
    const { progBySeq, position, httpTransTypes, funcTypeCodes } = this.state;
    const { closePopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--move"
            id="updateProgPopup"
            style={{ width: "550px" }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="func.edit" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="func.name.ko" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="funcNmKo"
                        name="funcNmKo"
                        type="text"
                        tabIndex="1"
                        value={progBySeq.funcNmKo}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncNmKo: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncNmKo",
                          check: true,
                          required: true,
                          min: 2,
                          max: 100,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="func.name.en" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="funcNmEn"
                        name="funcNmEn"
                        type="text"
                        tabIndex="1"
                        value={progBySeq.funcNmEn}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncNmEn: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncNmEn",
                          check: true,
                          required: true,
                          min: 2,
                          max: 100,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="func.url" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="funcUrl"
                        type="text"
                        tabIndex="2"
                        value={progBySeq.funcUrl}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncUrl: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncUrl",
                          check: true,
                          required: true,
                          min: 4,
                          max: 255,
                          reg: /^\/([/a-z0-9-%#?&._=\w{}])+$/,
                          regMsg: <IntlMessages id="wrong.url.type" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="func.type" />
                    </th>
                    <td>
                      <Select
                        name="funcTypeCode"
                        tabIndex="3"
                        value={progBySeq.funcTypeCode}
                        optionList={funcTypeCodes}
                        onChange={(funcTypeCode, e) => {
                          this.setState({
                            progBySeq: {
                              ...this.state.progBySeq,
                              funcTypeCode
                            }
                          });
                        }}
                        customStyleOptionListContainer={{
                          maxHeight: "140px",
                          overflow: "auto"
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      HTTP <IntlMessages id="trans.method" />
                    </th>
                    <td>
                      <Select
                        name="httpMethod"
                        type="text"
                        tabIndex="4"
                        value={progBySeq.httpTrnsType}
                        optionList={httpTransTypes}
                        onChange={(httpTrnsType, e) => {
                          this.setState({
                            progBySeq: {
                              ...this.state.progBySeq,
                              httpTrnsType
                            }
                          });
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ verticalAlign: "top" }}>
                      <IntlMessages id="desc" />
                    </th>
                    <td>
                      <Textarea
                        name="funcDesc"
                        type="text"
                        tabIndex="5"
                        value={progBySeq.funcDesc}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrFuncDesc: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "FuncDesc",
                          check: true,
                          required: false,
                          max: 1000,
                          locale: this.props.language
                        }}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "70px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={handleSaveData}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language"),
    progBySeq: state.prog.get("progBySeq")
  }),
  dispatch => ({
    ProgActions: bindActionCreators(ProgActions, dispatch)
  })
)(UpdateProgPopup);
