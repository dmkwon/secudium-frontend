import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as progActions from "../../store/modules/prog";
import DeleteConfirmPopup from "./popups/DeleteConfirmPopup";
import DeleteConfirmListPopup from "./popups/DeleteConfirmListPopup";
import Loading from "../Common/Loading";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import AddProgPopup from "./popups/AddProgPopup";
import UpdateProgPopup from "./popups/UpdateProgPopup";

import GridTable from "../Common/GridTable";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class ProgComponent extends Component {
  state = {
    isProgPopup: false,
    isUpdateProgPopup: false,
    isDeleteConfirmPopup: false,
    isDeleteConfirmListPopup: false,
    showConfirm: false,
    selectAll: false,

    selFuncSeq: -1,
    checked: [],
    paramList: [],
    filtered: [],
    resource: {}
  };
  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { progs } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < progs.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < progs.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { progs } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < progs.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onCheckDeleteHandler = e => {
    const { progs } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];

    if (checkArr.length === 0) {
      this.showAlert(
        <IntlMessages id="confirm" />,
        <IntlMessages id="no.selection" />
      );
    } else {
      let stdExists = false;
      for (let idx = 0; idx < checkArr.length; idx++) {
        if (checkArr[idx]) {
          paramList.push(progs.content[idx]);
          stdExists = true;
        }
      }
      if (stdExists) {
        this.setState({
          paramList: paramList,
          isDeleteConfirmListPopup: true
        });
      } else {
        this.showAlert(
          <IntlMessages id="confirm" />,
          <IntlMessages id="no.selection" />
        );
      }
    }
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          { name: <IntlMessages id="confirm" />, CB: () => this.onClosePopup() }
        ]
      }
    });
  };

  onClosePopup = () => {
    this.setState({ showConfirm: false });
  };

  onDeleteListConfirm = async () => {
    const { ProgActions } = this.props;
    const { paramList } = this.state;
    let rx = "";
    rx = await ProgActions.removeProgs(paramList);
    await ProgActions.getProgs();
    if (rx.status === 200) {
      this.closePopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: []
      });
    } else {
      this.closePopup();
    }
  };

  getProgs = async () => {
    const { ProgActions } = this.props;
    try {
      await ProgActions.getProgs();
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    this.getProgs();
  }

  openProgPopup = () => {
    this.setState({
      isProgPopup: true
    });
  };

  updateProgPopup = () => {
    this.setState({
      isUpdateProgPopup: true
    });
  };

  deleteConfirmPopup = () => {
    this.setState({
      isDeleteConfirmPopup: true
    });
  };

  // 팝업 닫기
  closePopup = () => {
    this.setState({
      isProgPopup: false,
      isDeleteConfirmPopup: false,
      isDeleteConfirmListPopup: false,
      isUpdateProgPopup: false,
      selectAll: false,
      checked: [],
      paramList: []
    });
  };

  resetState = () => {
    this.setState({
      filtered: []
    });
  };

  render() {
    const {
      chkBoxAllChange,
      chkBoxSingleChange,
      onDeleteListConfirm,
      onCheckDeleteHandler
    } = this;
    const {
      isProgPopup,
      showConfirm,
      isUpdateProgPopup,
      isDeleteConfirmPopup,
      isDeleteConfirmListPopup
    } = this.state;
    const { progs } = this.props;
    const columns_progs = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        sortable: false,
        width: 40
      },
      {
        Header: <IntlMessages id="func.name.ko" />,
        accessor: "funcNmKo",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="func.name.en" />,
        accessor: "funcNmEn",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="func.url" />,
        accessor: "funcUrl",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="func.type" />,
        accessor: "funcTypeName",
        style: { textAlign: "left" },
        width: 120,
        filterable: true
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "modUsrNm",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="mod.date" />,
        accessor: "stringModDate",
        style: { textAlign: "center" },
        width: 150,
        filterable: true
      },
      {
        Header: "Actions",
        accessor: "icon",
        style: { textAlign: "center" },
        width: 70,
        sortable: false,
        Cell: row => {
          return (
            <div>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.setState({
                    selFuncSeq: row.original.funcSeq
                  });

                  this.deleteConfirmPopup();
                }}
              >
                <img src="/images/common/icon_delete.png" alt="icon_delete" />
              </button>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.setState({
                    selFuncSeq: row.original.funcSeq
                  });
                  this.updateProgPopup();
                }}
              >
                <img src="/images/common/icon_edit.png" alt="icon_edit" />
              </button>
            </div>
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper prog">
          <LeftMenu />

          <div className="component component-list">
            <div className="component__title">
              <span>
                <IntlMessages id="func" />
              </span>
              <div className="btns">
                <button className="btn" onClick={onCheckDeleteHandler}>
                  <IntlMessages id="multi.delete" />
                </button>
                <button className="btn btn--blue" onClick={this.openProgPopup}>
                  <IntlMessages id="new.reg" />
                </button>
              </div>
            </div>

            <GridTable
              columns={columns_progs}
              data={progs.content}
              className="-striped -highlight"
              resizable={false}
              filterable={false}
              // showPageJump={true}
              // filtered={this.state.filtered}
              // onFilteredChange={filtered => this.setState({ filtered })}
              getTrProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: async e => {
                    chkBoxSingleChange(rowInfo.index);
                  }
                };
              }}
            />
          </div>
        </div>
        {isProgPopup && <AddProgPopup closePopup={this.closePopup} />}
        {isUpdateProgPopup && (
          <UpdateProgPopup
            data={this.state.selFuncSeq}
            closePopup={this.closePopup}
          />
        )}
        {isDeleteConfirmPopup && (
          <DeleteConfirmPopup
            data={this.state.selFuncSeq}
            closePopup={this.closePopup}
          />
        )}
        {isDeleteConfirmListPopup && (
          <DeleteConfirmListPopup
            closePopup={this.closePopup}
            onDeleteListConfirm={onDeleteListConfirm}
          />
        )}
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    progs: state.prog.get("progs"),
    loading: state.prog.get("loading")
  }),
  dispatch => ({
    ProgActions: bindActionCreators(progActions, dispatch)
  })
)(ProgComponent);
