import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as roleActions from "../../store/modules/role";
import { removeRoles } from "../../store/api/role";
import Loading from "./../Common/Loading";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";

import jwtService from "services/jwtService";

import IntlMessages from "util/IntlMessages";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import UserTab from "./UserTab";
import MenuTab from "./MenuTab";
import ProgTab from "./ProgTab";

import AddRolePopup from "./popups/AddRolePopup";
import UpdateRolePopup from "./popups/UpdateRolePopup";
import DeleteRolePopup from "./popups/DeleteRolePopup";
import DeleteRoleListPopup from "./popups/DeleteRoleListPopup";

import GridTable from "../Common/GridTable";

import "./style.scss";

class RoleComponent extends Component {
  state = {
    selectedTab: "U",
    selectedRole: 0,
    isRolePopup: false,
    isUpdateRolePopup: false,
    isDeleteRolePopup: false,
    isDeleteRoleListPopup: false,
    checked: [],
    selectAll: false,
    paramList: [],
    progSeqList: [],
    showConfirm: false,
    resource: {}
  };

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { roles } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < roles.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < roles.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { roles } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < roles.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onCheckDeleteHandler = e => {
    const { roles } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];

    if (checkArr.length === 0) {
      this.showAlert(
        <IntlMessages id="confirm" />,
        <IntlMessages id="no.selection" />
      );
    } else {
      let stdExists = false;
      for (let idx = 0; idx < checkArr.length; idx++) {
        if (checkArr[idx]) {
          paramList.push(roles.content[idx]);
          stdExists = true;
        }
      }
      if (stdExists) {
        this.setState({
          paramList: paramList,
          isDeleteRoleListPopup: true
        });
      } else {
        this.showAlert(
          <IntlMessages id="confirm" />,
          <IntlMessages id="no.selection" />
        );
      }
    }
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          { name: <IntlMessages id="confirm" />, CB: () => this.onClosePopup() }
        ]
      }
    });
  };

  onClosePopup = () => {
    this.setState({ showConfirm: false });
  };

  onDeleteListConfirm = async () => {
    const { RoleActions } = this.props;
    const { paramList } = this.state;
    let rx = "";
    rx = await removeRoles(paramList);
    await RoleActions.getRoles();
    if (rx.status === 200) {
      this.closePopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: []
      });
    } else {
      this.closePopup();
    }
  };

  onSelectedTab = tab => {
    this.setState({
      selectedTab: tab
    });
  };

  getRoleSeq = roleSeq => {
    this.setState({
      rolesSeq: roleSeq
    });
  };

  getData = roleSeq => {
    this.getUsers(roleSeq);
    this.getMenus(roleSeq);
    this.getProgs(roleSeq);
    this.getRoleSeq(roleSeq);
  };

  // click tr
  loadData = roleSeq => {
    this.getData(roleSeq);
  };

  // 이벤트 전파 방지
  handleCheckboxClick(e) {
    e.stopPropagation();
  }

  // 롤 Read
  getRoles = async () => {
    const { RoleActions } = this.props;
    try {
      await RoleActions.getRoles();

      if (this.props.roles != null && this.props.roles.content.length !== 0) {
        let roleSeq = this.props.roles.content[0].roleSeq;
        this.setState({
          rolesSeq: roleSeq,
          selectedRole: roleSeq
        });
        this.getUsers(roleSeq);
        this.getMenus(roleSeq);
        this.getProgs(roleSeq);
      }
    } catch (e) {
      console.log(e);
    }
  };

  // 롤 Read by Seq
  getRolesBySeq = async roleSeq => {
    const { RoleActions } = this.props;
    try {
      await RoleActions.getRolesBySeq(roleSeq);
    } catch (e) {
      console.log(e);
    }
  };

  getUsers = async (roleSeq = "") => {
    const { RoleActions } = this.props;
    try {
      if (roleSeq === "") {
        await RoleActions.getUsers();
      } else {
        await RoleActions.getUsersBySeq(roleSeq);
      }
    } catch (e) {
      console.log(e);
    }
  };

  getMenus = async (roleSeq = "") => {
    const { RoleActions } = this.props;
    try {
      if (roleSeq === "") {
        await RoleActions.getMenus();
      } else {
        await RoleActions.getMenusBySeq(roleSeq);
      }
    } catch (e) {
      console.log(e);
    }
  };

  getProgs = async (roleSeq = "") => {
    const { RoleActions } = this.props;
    try {
      if (roleSeq === "") {
        await RoleActions.getProgs();
      } else {
        await RoleActions.getProgsBySeq(roleSeq);
      }
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    jwtService.on("onChangeLang", () => {
      this.getRoles();
    });

    this.getRoles();
  }

  openRolePopup = () => {
    this.setState({
      isRolePopup: true
    });
  };

  deleteConfirmPopup = roleSeq => {
    const { RoleActions } = this.props;
    const ajax = RoleActions.getRolesBySeq(roleSeq);
    ajax.then(() => {
      this.setState({
        isDeleteRolePopup: true
      });
    });
  };

  updateRolePopup = roleSeq => {
    const { RoleActions } = this.props;
    const ajax = RoleActions.getRolesBySeq(roleSeq);
    ajax.then(() => {
      this.setState({
        isUpdateRolePopup: true
      });
    });
  };

  // 팝업 닫기
  closePopup = () => {
    this.setState({
      isRolePopup: false,
      isDeleteRolePopup: false,
      isDeleteRoleListPopup: false,
      isUpdateRolePopup: false,
      isUpdateRoleMenuPopup: false,
      checked: [],
      selectAll: false,
      paramList: []
    });
  };

  // [역할명] 클릭
  onClickRoleNm = async roleSeq => {
    const { RoleActions } = this.props;
    try {
      this.setState({
        selectedRole: roleSeq
      });
      await RoleActions.getRolesBySeq(roleSeq);
      this.loadData(roleSeq);
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const {
      onCheckDeleteHandler,
      chkBoxAllChange,
      chkBoxSingleChange,
      onDeleteListConfirm,
      onClickRoleNm
    } = this;
    const {
      selectedTab,
      showConfirm,
      isRolePopup,
      isUpdateRolePopup,
      isDeleteRolePopup,
      isDeleteRoleListPopup
    } = this.state;
    const { roles, rolesBySeq } = this.props;

    const columns_role = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onClick={e => {
                    this.handleCheckboxClick(e);
                  }}
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        sortable: false,
        style: { textAlign: "center" },
        width: 40
      },
      {
        Header: <IntlMessages id="role.id" />,
        accessor: "roleId",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="role.name" />,
        accessor: "roleNm",
        style: { textAlign: "left" },
        width: 120,
        filterable: true,
        Cell: row => {
          let target = row.original;
          return (
            <span
              className="link-style"
              data-tip={target.roleNm}
              data-for="tooltips"
              onClick={() => {
                onClickRoleNm(target.roleSeq);
              }}
            >
              {target.roleNm}
            </span>
          );
        }
      },
      {
        Header: <IntlMessages id="mod.date" />,
        accessor: "stringModDate",
        style: { textAlign: "center" },
        filterable: true
      },
      {
        Header: "Actions",
        accessor: "icon",
        style: { textAlign: "center" },
        width: 70,
        sortable: false,
        Cell: row => {
          return (
            <div>
              <button
                className="btn btn-icon"
                onClick={e => {
                  this.handleCheckboxClick(e);
                  this.deleteConfirmPopup(row.original.roleSeq);
                }}
              >
                <img src="/images/common/icon_delete.png" alt="icon_delete" />
              </button>
              <button
                className="btn btn-icon"
                onClick={e => {
                  this.handleCheckboxClick(e);
                  this.updateRolePopup(row.original.roleSeq);
                }}
              >
                <img src="/images/common/icon_edit.png" alt="icon_edit" />
              </button>
            </div>
          );
        }
      }
    ];
    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper role">
          <LeftMenu />
          <div className="component component-list" style={{ width: "580px" }}>
            <div className="component__title">
              <span>
                <IntlMessages id="role" />
              </span>
              <div className="btns">
                <button className="btn" onClick={onCheckDeleteHandler}>
                  <IntlMessages id="multi.delete" />
                </button>
                <button className="btn btn--blue" onClick={this.openRolePopup}>
                  <IntlMessages id="new.reg" />
                </button>
              </div>
            </div>
            <GridTable
              columns={columns_role}
              data={roles.content}
              filterable={false}
              resizable={false}
              className="-highlight"
              getTrProps={(state, rowInfo, column, instance) => {
                if (rowInfo) {
                  return {
                    style: {
                      backgroundColor:
                        rowInfo.original.roleSeq === this.state.selectedRole
                          ? "#282a35"
                          : "#2f323c"
                    }
                  };
                } else {
                  return {};
                }
              }}
            />
          </div>
          <div className="component component-info">
            <div className="component-info__contents">
              <ul className="component-tab">
                <li
                  onClick={() => this.onSelectedTab("U")}
                  className={selectedTab === "U" ? "active" : ""}
                >
                  <IntlMessages id="user.permit.manage" />
                </li>
                <li
                  onClick={() => this.onSelectedTab("M")}
                  className={selectedTab === "M" ? "active" : ""}
                >
                  <IntlMessages id="menu.right.manage" />
                </li>
                <li
                  onClick={() => this.onSelectedTab("P")}
                  className={selectedTab === "P" ? "active" : ""}
                >
                  <IntlMessages id="func.permit.manage" />
                </li>
              </ul>
              <div className="component">
                {selectedTab === "U" ? (
                  <UserTab rolesSeq={this.state.rolesSeq} />
                ) : (
                  ""
                )}
                {selectedTab === "M" ? (
                  <MenuTab
                    rolesSeq={this.state.rolesSeq}
                    getData={this.getData}
                  />
                ) : (
                  ""
                )}
                {selectedTab === "P" ? (
                  <ProgTab
                    rolesSeq={this.state.rolesSeq}
                    getData={this.getData}
                  />
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
          {isRolePopup && <AddRolePopup closePopup={this.closePopup} />}
          {isUpdateRolePopup && (
            <UpdateRolePopup closePopup={this.closePopup} data={rolesBySeq} />
          )}
          {isDeleteRolePopup && (
            <DeleteRolePopup closePopup={this.closePopup} />
          )}
          {isDeleteRoleListPopup && (
            <DeleteRoleListPopup
              closePopup={this.closePopup}
              onDeleteListConfirm={onDeleteListConfirm}
            />
          )}
        </div>
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    roles: state.role.get("roles"),
    loading: state.role.get("loading"),
    rolesBySeq: state.role.get("rolesBySeq")
  }),
  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(RoleComponent);
