import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as roleActions from "../../store/modules/role";
import GridTable from "../Common/GridTable";
import UpdateRoleProgPopup from "./popups/UpdateRoleProgPopup";
import ReactTooltip from "react-tooltip";
import IntlMessages from "util/IntlMessages";

class ProgTab extends Component {
  state = {
    progSeq: "",
    progNm: "",
    progDesc: "",
    httpMethod: "",

    checked: [],
    paramList: [],
    isUpdateRoleProgPopup: false,
    selectAll: false,
    progsBySeq: this.props.progsBySeq || []
  };

  componentDidMount() {
    this.drawChecked();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { progsBySeq } = nextProps;
    let checkArr = [];
    let yCount = 0;
    for (let i = 0; i < progsBySeq.content.length; i++) {
      if (progsBySeq.content[i].checked === "Y") {
        yCount++;
        checkArr.push(true);
      } else {
        checkArr.push(false);
      }
    }

    if (progsBySeq.content.length > 0 && progsBySeq.content.length === yCount) {
      this.setState({
        selectAll: true,
        checked: checkArr,
        progsBySeq: nextProps.progsBySeq
      });
    } else {
      this.setState({
        selectAll: false,
        checked: checkArr,
        progsBySeq: nextProps.progsBySeq
      });
    }
  }

  drawChecked = () => {
    const { progsBySeq } = this.state;
    let checkArr = [];
    let yCount = 0;
    for (let i = 0; i < progsBySeq.content.length; i++) {
      if (progsBySeq.content[i].checked === "Y") {
        yCount++;
        checkArr.push(true);
      } else {
        checkArr.push(false);
      }
    }

    if (progsBySeq.content.length > 0 && progsBySeq.content.length === yCount) {
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { progsBySeq } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < progsBySeq.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < progsBySeq.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { progsBySeq } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < progsBySeq.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onCheckUpdateHandler = e => {
    const { progsBySeq } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];
    if (checkArr.length === 0) {
    } else {
      for (let idx = 0; idx < checkArr.length; idx++) {
        if (checkArr[idx]) {
          paramList.push(progsBySeq.content[idx]);
        }
      }
      this.setState({
        paramList: paramList,
        isUpdateRoleProgPopup: true
      });
    }
  };

  onUpdateListConfirm = async () => {
    const { RoleActions, rolesSeq } = this.props;
    const { paramList } = this.state;
    let rx = "";
    rx = await RoleActions.updateProgRoleBySeq(rolesSeq, paramList);
    this.props.getData(rolesSeq);
    if (rx.status === 200) {
      this.closePopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: [],
        rolesSeq: []
      });
    } else {
      this.closePopup();
    }
  };

  // 이벤트 전파 방지
  handleCheckboxClick(e) {
    e.stopPropagation();
  }

  // 팝업 닫기
  closePopup = () => {
    this.setState({
      isUpdateRoleProgPopup: false,
      checked: [],
      paramList: [],
      selectAll: false
    });
  };

  render() {
    const {
      chkBoxSingleChange,
      chkBoxAllChange,
      onUpdateListConfirm,
      onCheckUpdateHandler
    } = this;
    const { isUpdateRoleProgPopup } = this.state;
    const { progsBySeq } = this.props;

    const columns_prog = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        filterable: false,
        resizable: false,
        sortable: false,
        style: { textAlign: "center" },
        width: 40
      },
      {
        Header: <IntlMessages id="func.name" />,
        accessor: "funcNm",
        style: { textAlign: "left" },
        width: 300,
        filterable: true,
        Cell: props => {
          const tooltipId = `funcNmTooltip_${props.original.funcSeq}`;
          return (
            <React.Fragment>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
              <ReactTooltip
                id={tooltipId}
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={150}
                event="mouseover"
                globalEventOff="mouseout"
              >
                {props.value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      },
      {
        Header: <IntlMessages id="func.url" />,
        accessor: "funcUrl",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="func.type" />,
        accessor: "funcTypeName",
        style: { textAlign: "left" },
        width: 120,
        filterable: true
      }
    ];
    return (
      <React.Fragment>
        <div className="component__title">
          <div className="btns">
            <button className="btn btn--blue" onClick={onCheckUpdateHandler}>
              <IntlMessages id="save" />
            </button>
          </div>
        </div>
        <GridTable
          columns={columns_prog}
          data={progsBySeq.content}
          className="-striped -highlight"
          resizable={false}
          filterable={false}
          getTrProps={(state, rowInfo, column, instance) => {
            return {
              onClick: async e => {
                chkBoxSingleChange(rowInfo.index);
              }
            };
          }}
        />
        {isUpdateRoleProgPopup && (
          <UpdateRoleProgPopup
            closePopup={this.closePopup}
            onUpdateListConfirm={onUpdateListConfirm}
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    progsBySeq: state.role.get("progsBySeq")
  }),
  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(ProgTab);
