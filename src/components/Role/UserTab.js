import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as roleActions from "../../store/modules/role";
import GridTable from "../Common/GridTable";
import DeleteUserListPopup from "./popups/DeleteUserListPopup";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";
import AddRoleUserPopup from "./popups/AddRoleUserPopup";

import IntlMessages from "util/IntlMessages";

class UserTab extends Component {
  state = {
    usrNo: "",
    checked: [],
    paramList: [],
    resource: {},
    isDeleteUserListPopup: false,
    selectAll: false,
    showConfirm: false,
    usersBySeq: this.props.usersBySeq || []
  };

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { usersBySeq } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < usersBySeq.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < usersBySeq.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { usersBySeq } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < usersBySeq.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onDeleteListConfirm = async () => {
    const { RoleActions, rolesSeq } = this.props;
    const { paramList } = this.state;
    let rx = await RoleActions.removeUsersBySeq(rolesSeq, paramList);
    await RoleActions.getUsersBySeq(rolesSeq);
    if (rx.status === 200) {
      this.setState({
        selectAll: false,
        checked: [],
        paramList: []
      });
      this.closePopup();
    } else {
      this.closePopup();
    }
  };

  onCheckDeleteHandler = e => {
    const { usersBySeq } = this.props;
    const checkArr = this.state.checked;

    let paramList = [];
    if (checkArr.length === 0) {
      this.showAlert(
        <IntlMessages id="confirm" />,
        <IntlMessages id="no.selection" />
      );
    } else {
      let stdExists = false;
      checkArr.map((el, idx) => {
        if (el) {
          paramList.push(usersBySeq.content[idx]);
          stdExists = true;
        }
      });
      if (stdExists) {
        this.setState({
          paramList: paramList,
          isDeleteUserListPopup: true
        });
      } else {
        this.showAlert(
          <IntlMessages id="confirm" />,
          <IntlMessages id="no.selection" />
        );
      }
    }
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          { name: <IntlMessages id="confirm" />, CB: () => this.onClosePopup() }
        ]
      }
    });
  };

  onClosePopup = () => {
    this.setState({ showConfirm: false });
  };

  // 이벤트 전파 방지
  handleCheckboxClick(e) {
    e.stopPropagation();
  }

  openUserPopup = async () => {
    const { RoleActions } = this.props;
    try {
      await RoleActions.getUsers();

      this.setState({
        isUserPopup: true
      });
    } catch (e) {
      console.log(e);
    }
  };

  // 팝업 닫기
  closePopup = () => {
    this.setState({
      isDeleteUserListPopup: false,
      isUserPopup: false,
      checked: [],
      selectAll: false,
      paramList: []
    });
  };

  render() {
    const {
      chkBoxSingleChange,
      chkBoxAllChange,
      onDeleteListConfirm,
      onCheckDeleteHandler
    } = this;
    const { isDeleteUserListPopup, isUserPopup, showConfirm } = this.state;
    const { usersBySeq } = this.props;

    const columns_user = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        sortable: false,
        style: { textAlign: "center" },
        width: 40
      },
      {
        Header: <IntlMessages id="company" />,
        accessor: "coNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: "ID",
        accessor: "usrId",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="name" />,
        accessor: "usrNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="department" />,
        accessor: "deptNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="position" />,
        accessor: "dutyName",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="user.division" />,
        accessor: "accTypeName",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="reg.date" />,
        accessor: "stringRegDate",
        style: { textAlign: "center" },
        filterable: true
      }
    ];
    return (
      <React.Fragment>
        <div className="component__title">
          <div className="btns">
            <button className="btn" onClick={onCheckDeleteHandler}>
              <IntlMessages id="multi.delete" />
            </button>
            <button className="btn btn--blue" onClick={this.openUserPopup}>
              <IntlMessages id="user.add" />
            </button>
          </div>
        </div>
        <GridTable
          columns={columns_user}
          data={usersBySeq.content}
          className="-striped -highlight"
          resizable={false}
          filterable={false}
          getTrProps={(state, rowInfo, column, instance) => {
            return {
              onClick: async e => {
                chkBoxSingleChange(rowInfo.index);
              }
            };
          }}
        />
        {isDeleteUserListPopup && (
          <DeleteUserListPopup
            closePopup={this.closePopup}
            onDeleteListConfirm={onDeleteListConfirm}
          />
        )}
        {isUserPopup && (
          <AddRoleUserPopup
            rolesSeq={this.props.rolesSeq}
            closePopup={this.closePopup}
          />
        )}
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    usersBySeq: state.role.get("usersBySeq"),
    users: state.role.get("users")
  }),
  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(UserTab);
