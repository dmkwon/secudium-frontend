import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as roleActions from "../../../store/modules/role";
import Draggable from "react-draggable";
import IntlMessages from "util/IntlMessages";

class DeleteUserListPopup extends Component {
  state = {
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("deleteUsersPopup").offsetWidth;
    const elHeight = document.getElementById("deleteUsersPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  // 팝업 위치 조절
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { closePopup, onDeleteListConfirm } = this.props;
    const { position } = this.state;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--alert popup--move"
            id="deleteUsersPopup"
            style={{ width: "400px" }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="confirm" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <IntlMessages id="delete.select.user" />
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={onDeleteListConfirm}>
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    rolesBySeq: state.role.get("rolesBySeq")
  }),

  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(DeleteUserListPopup);
