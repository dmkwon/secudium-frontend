import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Draggable from "react-draggable";
import { Textbox, Textarea } from "react-inputs-validation";
import IntlMessages from "util/IntlMessages";

import * as roleActions from "./../../../store/modules/role";
import { createRole } from "./../../../store/api/role";

import "./style.scss";

class AddRolePopup extends Component {
  state = {
    roleId: "",
    roleNm: "",
    roleDesc: "",

    position: {
      x: 0,
      y: 0
    },

    validation: {
      validate: false,
      hasErrRoleId: true,
      hasErrRoleNm: true,
      hasErrRoleDesc: false
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("addRolePopup").offsetWidth;
    const elHeight = document.getElementById("addRolePopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  //팝업 draggable
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrRoleId,
      hasErrRoleNm,
      hasErrRoleDesc
    } = this.state.validation;

    return !hasErrRoleId && !hasErrRoleNm && !hasErrRoleDesc;
  };

  //Input state 변경
  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      [name]: value
    });
  };

  onClickSave = async e => {
    if (this.isValidate()) {
      const { RoleActions } = this.props;
      const inputRole = this.state;
      try {
        await createRole(inputRole);
        this.setState({
          roleId: "",
          roleNm: "",
          roleDesc: ""
        });
        this.props.closePopup();
        RoleActions.getRoles();
      } catch (e) {
        console.warn(e);
      }
    }
  };

  render() {
    const { onClickSave, handleChangeInput } = this;
    const { position } = this.state;
    const { validate } = this.state.validation;
    const { closePopup } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--move" id="addRolePopup">
            <div className="popup__header">
              <h5>
                <IntlMessages id="role.add" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="role.id" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="roleId"
                        name="roleId"
                        value={this.state.roleId}
                        type="text"
                        tabIndex="1"
                        onChange={handleChangeInput}
                        placeholder="ROLE_CODE"
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrRoleId: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "roleId",
                          check: true,
                          required: true,
                          min: 4,
                          max: 50,
                          reg: /^[0-9a-zA-Z!@#$%^*+=_-]+$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="role.name" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="roleNm"
                        name="roleNm"
                        type="text"
                        tabIndex="2"
                        value={this.state.roleNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrRoleNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "roleNm",
                          check: true,
                          required: true,
                          max: 50,
                          reg: /\S+$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="role.name" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="roleNm"
                        name="roleNm"
                        type="text"
                        tabIndex="2"
                        value={this.state.roleNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrRoleNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "roleNm",
                          check: true,
                          required: true,
                          max: 50,
                          reg: /\S+$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ verticalAlign: "top" }}>
                      <IntlMessages id="desc" />
                    </th>
                    <td>
                      <Textarea
                        id="roleDesc"
                        name="roleDesc"
                        type="text"
                        tabIndex="3"
                        value={this.state.roleDesc}
                        onChange={handleChangeInput}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "100px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={onClickSave}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    roles: state.role.get("roles"),
    language: state.common.get("language")
  }),
  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(AddRolePopup);
