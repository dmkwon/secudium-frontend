import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Draggable from "react-draggable";
import { Textbox, Textarea } from "react-inputs-validation";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

import * as roleActions from "../../../store/modules/role";

class UpdateRolePopup extends Component {
  state = {
    rolesBySeq: this.props.data,
    validation: {
      validate: false,
      hasErrRoleId: false,
      hasErrRoleNm: false,
      hasErrRoleDesc: false
    },
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("updateRolePopup").offsetWidth;
    const elHeight = document.getElementById("updateRolePopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  //팝업 위치 조절
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrRoleId,
      hasErrRoleNm,
      hasErrRoleDesc
    } = this.state.validation;

    return !hasErrRoleId && !hasErrRoleNm && !hasErrRoleDesc;
  };

  handleSaveData = async () => {
    if (this.isValidate()) {
      const { RoleActions } = this.props;
      await RoleActions.updateRole(this.state.rolesBySeq);
      this.props.closePopup();
      RoleActions.getRoles();
    }
  };

  //Input state 변경
  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      rolesBySeq: {
        ...this.state.rolesBySeq,
        [name]: value
      }
    });
  };

  render() {
    const { handleSaveData, handleChangeInput } = this;
    const { validate, rolesBySeq, position } = this.state;
    const { closePopup } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--move"
            id="updateRolePopup"
            style={{ width: "480px" }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="role.edit" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="role.id" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="roleId"
                        name="roleId"
                        type="text"
                        tabIndex="1"
                        value={rolesBySeq.roleId}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrRoleId: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "roleId",
                          check: true,
                          required: true,
                          min: 4,
                          max: 50,
                          reg: /^[0-9a-zA-Z!@#$%^*+=_-]+$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="role.name" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="roleNm"
                        name="roleNm"
                        type="text"
                        tabIndex="2"
                        value={rolesBySeq.roleNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        vliadate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrRoleNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "roleNm",
                          check: true,
                          required: true,
                          max: 50,
                          reg: /\S+$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="role.name" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="roleNm"
                        name="roleNm"
                        type="text"
                        tabIndex="2"
                        value={rolesBySeq.roleNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        vliadate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrRoleNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "roleNm",
                          check: true,
                          required: true,
                          max: 50,
                          reg: /\S+$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ verticalAlign: "top" }}>
                      <IntlMessages id="desc" />
                    </th>
                    <td>
                      <Textarea
                        id="roleDesc"
                        name="roleDesc"
                        type="text"
                        tabIndex="3"
                        value={rolesBySeq.roleDesc}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrRoleDesc: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "RoleDesc",
                          check: true,
                          required: false,
                          max: 1000,
                          locale: this.props.language
                        }}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "100px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={handleSaveData}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    rolesBySeq: state.role.get("rolesBySeq"),
    language: state.common.get("language")
  }),
  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(UpdateRolePopup);
