import React, { Component } from "react";
import GridTable from "../../Common/GridTable";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as roleActions from "../../../store/modules/role";
import Draggable from "react-draggable";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class AddRoleUserPopup extends Component {
  state = {
    position: {
      x: 0,
      y: 0
    },

    selectAll: false,
    checked: [],
    paramList: [],
    users: this.props.users || [],
    rolesSeq: this.props.rolesSeq
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("addRoleUserPopup").offsetWidth;
    const elHeight = document.getElementById("addRoleUserPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  // 팝업 위치 조절
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { users } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < users.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < users.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { users } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < users.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onCheckAddHandler = async () => {
    const { RoleActions, users, rolesSeq } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];
    let rx = "";
    if (checkArr.length === 0) {
    } else {
      for (let idx = 0; idx < checkArr.length; idx++) {
        if (checkArr[idx]) {
          paramList.push(users.content[idx]);
        }
      }

      rx = await RoleActions.updateUserRoleBySeq(rolesSeq, paramList);
      await RoleActions.getUsersBySeq(rolesSeq);
      if (rx.status === 200) {
        this.setState({
          selectAll: false,
          checked: [],
          paramList: []
        });
        this.props.closePopup();
      } else {
        this.props.closePopup();
      }
    }
  };

  render() {
    const { chkBoxAllChange, chkBoxSingleChange, onCheckAddHandler } = this;
    const { position } = this.state;
    const { users, closePopup } = this.props;

    const columns_user = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        sortable: false,
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: <IntlMessages id="company" />,
        accessor: "coNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: "ID",
        accessor: "usrId",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="name" />,
        accessor: "usrNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="department" />,
        accessor: "deptNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="position" />,
        accessor: "dutyName",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="user.division" />,
        accessor: "accTypeName",
        style: { textAlign: "left" },
        filterable: true
      }
    ];
    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--dark popup--role" id="addRoleUserPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <IntlMessages id="user.select" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <GridTable
                columns={columns_user}
                data={users.content}
                className="-striped -highlight"
                filterable={false}
                resizable={false}
                getTrProps={(state, rowInfo, column, instance) => {
                  return {
                    onClick: async e => {
                      chkBoxSingleChange(rowInfo.index);
                    }
                  };
                }}
              />
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={onCheckAddHandler}>
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    users: state.role.get("users")
  }),
  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(AddRoleUserPopup);
