import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as roleActions from "../../store/modules/role";
import GridTable from "../Common/GridTable";
import UpdateRoleMenuPopup from "./popups/UpdateRoleMenuPopup";
import IntlMessages from "util/IntlMessages";

class MenuTab extends Component {
  state = {
    menuSeq: "",
    menuNm: "",
    url: "",
    step: "",

    checked: [],
    paramList: [],
    selectAll: false,
    isUpdateRoleMenuPopup: false,
    menusBySeq: this.props.menusBySeq || []
  };

  componentDidMount() {
    this.drawChecked();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { menusBySeq } = nextProps;
    let checkArr = [];
    let yCount = 0;
    for (let i = 0; i < menusBySeq.content.length; i++) {
      if (menusBySeq.content[i].checked === "Y") {
        yCount++;
        checkArr.push(true);
      } else {
        checkArr.push(false);
      }
    }

    if (menusBySeq.content.length > 0 && menusBySeq.content.length === yCount) {
      this.setState({
        selectAll: true,
        checked: checkArr,
        menusBySeq: nextProps.menusBySeq
      });
    } else {
      this.setState({
        selectAll: false,
        checked: checkArr,
        menusBySeq: nextProps.menusBySeq
      });
    }
  }

  drawChecked = () => {
    const { menusBySeq } = this.state;
    let checkArr = [];
    let yCount = 0;
    for (let i = 0; i < menusBySeq.content.length; i++) {
      if (menusBySeq.content[i].checked === "Y") {
        yCount++;
        checkArr.push(true);
      } else {
        checkArr.push(false);
      }
    }

    if (menusBySeq.content.length > 0 && menusBySeq.content.length === yCount) {
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { menusBySeq } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < menusBySeq.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < menusBySeq.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { menusBySeq } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < menusBySeq.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onCheckUpdateHandler = e => {
    const { menusBySeq } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];
    if (checkArr.length > 0) {
      checkArr.map((el, idx) => {
        if (el) {
          paramList.push(menusBySeq.content[idx]);
        }
      });
      this.setState({
        paramList: paramList,
        isUpdateRoleMenuPopup: true
      });
    }
  };

  onUpdateListConfirm = async () => {
    const { RoleActions, rolesSeq } = this.props;
    const { paramList } = this.state;
    let rx = "";
    rx = await RoleActions.updateMenuRoleBySeq(rolesSeq, paramList);
    this.props.getData(rolesSeq);
    if (rx.status === 200) {
      this.closePopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: [],
        rolesSeq: []
      });
    } else {
      this.closePopup();
    }
  };

  // 이벤트 전파 방지
  handleCheckboxClick(e) {
    e.stopPropagation();
  }

  // 팝업 닫기
  closePopup = () => {
    this.setState({
      isUpdateRoleMenuPopup: false,
      checked: [],
      paramList: [],
      selectAll: false
    });
  };

  render() {
    const {
      chkBoxSingleChange,
      chkBoxAllChange,
      onUpdateListConfirm,
      onCheckUpdateHandler
    } = this;
    const { isUpdateRoleMenuPopup } = this.state;
    const { menusBySeq } = this.props;

    const columns_menu = [
      {
        Header: "No. ",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        filterable: false,
        resizable: false,
        sortable: false,
        style: { textAlign: "center" },
        width: 40
      },
      {
        Header: <IntlMessages id="level" />,
        accessor: "menuLevel",
        style: { textAlign: "right" },
        width: 70,
        filterable: true
      },
      {
        Header: <IntlMessages id="sort.order" />,
        accessor: "menuSortOrder",
        style: { textAlign: "right" },
        width: 70,
        filterable: true
      },
      {
        Header: <IntlMessages id="menu.name" />,
        accessor: "menuNm",
        style: { textAlign: "left" },
        width: 200,
        filterable: true
      },
      {
        Header: <IntlMessages id="menu.url" />,
        accessor: "menuUrl",
        style: { textAlign: "left" },
        filterable: true
      }
    ];
    return (
      <React.Fragment>
        <div className="component__title">
          <div className="btns">
            <button className="btn btn--blue" onClick={onCheckUpdateHandler}>
              <IntlMessages id="save" />
            </button>
          </div>
        </div>
        <GridTable
          columns={columns_menu}
          data={menusBySeq.content}
          className="-striped -highlight"
          resizable={false}
          filterable={false}
          getTrProps={(state, rowInfo, column, instance) => {
            return {
              onClick: async e => {
                if (rowInfo) {
                  chkBoxSingleChange(rowInfo.index);
                }
              }
            };
          }}
        />
        {isUpdateRoleMenuPopup && (
          <UpdateRoleMenuPopup
            closePopup={this.closePopup}
            onUpdateListConfirm={onUpdateListConfirm}
          />
        )}
      </React.Fragment>
    );
  }
}
export default connect(
  state => ({
    menusBySeq: state.role.get("menusBySeq")
  }),
  dispatch => ({
    RoleActions: bindActionCreators(roleActions, dispatch)
  })
)(MenuTab);
