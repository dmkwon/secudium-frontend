import React, { Component } from "react";
import ReactDOM from "react-dom";
import Tree, { TreeNode } from "rc-tree";
import { IntlProvider } from "react-intl";

import "react-contexify/dist/ReactContexify.min.css";

const getSubSize = sub => (sub != null ? sub.length : 0);
const isLeaf = fg => fg.sub == null || getSubSize(fg.sub) === 0;

class CommonContextMenuTree extends Component {
  componentDidMount() {
    if (this.props.hasContextMenu) {
      this.getCMContainer();
      const hardThis = this;

      document.addEventListener("click", function() {
        if (hardThis.cmContainer != null) {
          ReactDOM.unmountComponentAtNode(hardThis.cmContainer);
          document.body.removeChild(hardThis.cmContainer);
          hardThis.cmContainer = null;
          hardThis.contextMenu = null;
        }
      });
    }
  }

  componentWillUnmount() {
    if (this.props.hasContextMenu) {
      if (this.cmContainer) {
        ReactDOM.unmountComponentAtNode(this.cmContainer);
        document.body.removeChild(this.cmContainer);
        this.cmContainer = null;
      }
    }
  }

  onRightClick = info => {
    if (this.props.hasContextMenu) {
      this.props.isDebug && console.log("onRightClick", info);
      this.renderContextMenu(info);
    }
  };

  getCMContainer() {
    if (this.props.hasContextMenu) {
      if (!this.cmContainer) {
        this.cmContainer = document.createElement("div");
        document.body.appendChild(this.cmContainer);
      }
      return this.cmContainer;
    }
  }

  renderContextMenu(info) {
    if (this.props.hasContextMenu) {
      const style = {
        position: "absolute",
        zIndex: 1,
        left: `${info.event.pageX}px`,
        top: `${info.event.pageY}px`
      };

      if (this.contextMenu) {
        ReactDOM.unmountComponentAtNode(this.cmContainer);
        this.contextMenu = null;
      }

      this.getCMContainer();
      Object.assign(this.cmContainer.style, style);

      ReactDOM.render(
        this.props.getContextMenu(info.node.props.targetData),
        this.cmContainer
      );
    }
  }

  render() {
    const { treeData, getTreeNodeTitle, getTreeNodeKey } = this.props;

    //this.props.isDebug && console.log(treeData);

    const loop = data => {
      return data.map(subDdata => {
        if (getSubSize(subDdata.sub) > 0) {
          return (
            <TreeNode
              title={getTreeNodeTitle(subDdata)}
              key={getTreeNodeKey(subDdata)}
              targetData={subDdata}
            >
              {loop(subDdata.sub)}
            </TreeNode>
          );
        }

        return (
          <TreeNode
            title={getTreeNodeTitle(subDdata)}
            key={getTreeNodeKey(subDdata)}
            targetData={subDdata}
            isLeaf={isLeaf(subDdata)}
          />
        );
      });
    };

    const treeNodes = data => loop(data);
    const subTree = () => {
      if (Array.isArray(treeData)) {
        return treeNodes(treeData);
      } else {
        return (
          <TreeNode
            title={getTreeNodeTitle(treeData)}
            key={getTreeNodeKey(treeData)}
            targetData={treeData}
          >
            {treeNodes(treeData.sub)}
          </TreeNode>
        );
      }
    };
    return (
      <Tree
        onRightClick={this.onRightClick}
        onSelect={this.props.onSelect}
        defaultExpandAll
        showLine
        showIcon={false}
        selectable={true}
        defaultSelectedKeys={[
          this.props.firstkey
            ? getTreeNodeKey(this.props.firstkey)
            : getTreeNodeKey(treeData)
        ]}
      >
        {subTree()}
      </Tree>
    );
  }
}

export default CommonContextMenuTree;
