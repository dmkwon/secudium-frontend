import React, { Component } from "react";
import FilterCommonGrid from "./CommonGrid";
import IntlMessages from "util/IntlMessages";

class RuleList extends Component {
  selectRule = rule => {
    // 상위 UI에 데이터 올림
    this.props.handleRuleInfoPopup(true, rule);
  };

  render() {
    const columns = [
      {
        Header: <IntlMessages id="rule.name" />,
        accessor: "ruleNm",
        Cell: cell => {
          let target = cell.original;
          return (
            <span
              className="link-style"
              onClick={() => this.selectRule(target)}
            >
              {target.ruleNm}
            </span>
          );
        }
      }
    ];

    const addNoData = () => {
      let list = this.props.ruleList;
      list && list.map((data, i) => (data.no = i + 1));
      return list;
    };
    return (
      <FilterCommonGrid
        className="-highlight"
        grideColumns={columns}
        gridData={addNoData()}
        hasNo={true}
        hasCheckBox={false}
        // isDisableCheckBox={}
        // seletedItems={} // Map 자료형임, readOnly
        // checkSelected={}
        // checkAllSelected={}
        // onClickCkeckBox={}
        hasAction={false}
        //makeActionBtn={makeActionBtn}

        defaultPageSize={5}
        pageSizeOptions={[5, 10]}
        //columnWidth={columnWidth}
      />
    );
  }
}

export default RuleList;
