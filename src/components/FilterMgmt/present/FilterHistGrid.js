import React, { Component } from "react";

import {
  filterPopupType,
  filterConfirmPopupResource
} from "../const/popupResource";

import FilterCommonGrid from "./CommonGrid";
import ReactTooltip from "react-tooltip";
import { isDebug } from "../const/filterConfig";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class FilterHistGrid extends Component {
  selectFilterHist = filterHist => {
    // 상위 UI에 데이터 올림
    this.props.selectedFilterHist(filterHist);
  };

  componentDidMount() {
    isDebug &&
      console.log("componentDidMount init data::", this.props.filterHistData);
    if (this.props.filterHistData && this.props.filterHistData.length > 0) {
      this.selectFilterHist(this.props.filterHistData[0]); // 디폴트, 가장 최근 hist
    }
  }

  saveFilter = (resource, changeReason) => {
    let filter = resource.target; // history를 가지고 rollback하는 경우
    filter.filterChgReason = changeReason;
    filter.name = filter.filterNm;

    this.props.handleSaveFilter(filter, true); // 수정 필요
  };

  onClickRollbackBtn = target => {
    isDebug && console.log("onClickRollbackBtn:", target);
    let resource =
      this.props.filter.ruleCnt > 0
        ? filterConfirmPopupResource(
            filterPopupType.saveFilterWithRule,
            this.props.closePopup,
            this.saveFilter
          )
        : filterConfirmPopupResource(
            filterPopupType.saveFilter,
            this.props.closePopup,
            this.saveFilter
          );
    resource.target = target;
    this.props.openConfirmPopup(resource);
  };

  render() {
    const columnWidth = { no: 40, action: 60 };
    const columns = [
      {
        Header: (
          <IntlMessages id="mod.date"/>
        ),
        accessor: "regDate",
        width: 115,
        style: { textAlign: "center" }
      },
      {
        Header: (
          <IntlMessages id="modifier"/>
        ),
        accessor: "regUsrName",
        width: 85
      },
      {
        Header: (
          <IntlMessages id="reason.change"/>
        ),
        accessor: "filterChgReason",
        width: 240,
        Cell: cell => {
          isDebug && cell.index === 0 && console.log(cell);
          let target = cell.original;
          return (
            <span
              className="link-style"
              data-tip={target.filterChgReason}
              data-for="tooltips"
              onClick={() => this.selectFilterHist(target)}
            >
              {target.filterChgReason}
            </span>
          );
        }
      }
    ];

    const isFirst = row => this.props.filterHistData[0] === row;
    const makeActionBtn = target => {
      return (
        <div>
          {!isFirst(target) && (
            <button
              id="rollbackBtn"
              className="btn btn-icon"
              onClick={() => this.onClickRollbackBtn(target)}
            >
            <FormattedMessage id="back.select">
              {title => (
              <img
                src="/images/common/icon_rollback.png"
                alt="ollback"
                title={title}
              />
              )}
            </FormattedMessage>
            </button>
          )}
        </div>
      );
    };

    return (
      <div>
        <FilterCommonGrid
          // 필수 항목
          grideColumns={columns}
          gridData={this.props.filterHistData}
          // 선택 항목
          hasNo={true}
          hasCheckBox={false}
          // isDisableCheckBox={}
          // seletedItems={} // Map 자료형임, readOnly
          // checkSelected={}
          // checkAllSelected={}
          // onClickCkeckBox={}
          hasAction={true}
          makeActionBtn={makeActionBtn}
          // reative gride 자체 옵션
          defaultPageSize={15}
          pageSizeOptions={[10, 15, 20, 25]}
          columnWidth={columnWidth}
        />

        <ReactTooltip
          className="tooltipClass"
          id="tooltips"
          effect="float"
          place="right"
        />
      </div>
    );
  }
}

export default FilterHistGrid;
