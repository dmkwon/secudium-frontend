import React, { Component } from "react";
import GridTable from "../../Common/GridTable";

import { isDebug } from "../const/filterConfig";

class FilterCommonGrid extends Component {
  onClickFilter = cell => {
    isDebug && console.log(cell);
    //this.props.goToTargetPage(fg);
  };

  render() {
    const {
      grideColumns,
      gridData, // Array 형

      hasNo, // 첫 필드가 No.인지 여부 (true/false)
      hasCheckBox, // 체크 박스가 있는지 여부 (true/false)
      isDisableCheckBox, // 일부에 대해여 disable 시키고 싶으면 사용(함수)
      checkSelected, // selected 여부를 확인 하는
      checkAllSelected,
      onClickCkeckBox,

      hasAction, // action 필드가 있는 경우
      makeActionBtn,

      defaultPageSize,
      pageSizeOptions,
      columnWidth
    } = this.props;

    const makeColumns = insertColumns => {
      let columns = [];

      // [1] No.
      hasNo &&
        columns.push({
          Header: "No.",
          accessor: "no",
          sortable: true,
          width: columnWidth && columnWidth.no ? columnWidth.no : 80,
          style: { textAlign: "center" }
        });

      // [2] check box
      hasCheckBox &&
        columns.push({
          Header: (
            <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
              <label>
                <input
                  type="checkbox"
                  onChange={() => onClickCkeckBox(true)}
                  checked={checkAllSelected()}
                />
                {/* 중복 허용하지 않고, 개수가 같으면 모두 선택 */}
                <div className="icon" />
              </label>
            </div>
          ),
          accessor: "checkBox",
          Cell: cell => (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => onClickCkeckBox(false, cell.original)}
                  checked={checkSelected(cell.original)}
                  disabled={isDisableCheckBox(cell.original)}
                />
                <div className="icon" />
              </label>
            </div>
          ),
          style: { textAlign: "center" },
          sortable: false,
          width: columnWidth && columnWidth.ckBox ? columnWidth.ckBox : 40
        });

      // [3] 실제 추가되는 데이터들 - 상위에서 전달된 중간에 삽입 될 컬럼 정보, 배열로 되어 있음!!
      insertColumns.map(col => {
        columns.push(col);
      });

      // [4] 추가되는 action 들
      hasAction &&
        columns.push({
          Header: " Actions",
          accessor: "action",
          width: columnWidth && columnWidth.action ? columnWidth.action : 100,
          style: {
            textAlign: "center",
            paddingLeft: "5px",
            paddingRight: "5px"
          },
          Cell: cell => {
            return makeActionBtn(cell.original);
          }
        });

      return columns;
    };

    return (
      <GridTable
        columns={makeColumns(grideColumns)}
        data={gridData ? gridData : []}
        defaultPageSize={defaultPageSize}
        pageSizeOptions={pageSizeOptions}
      />
    );
  }
}

export default FilterCommonGrid;
