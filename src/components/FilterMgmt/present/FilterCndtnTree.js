import React, { Component } from "react";
import { Menu, Item, MenuProvider } from "react-contexify";
import ReactTooltip from "react-tooltip";

import Dropdown from "../../Common/Dropdown";
import {
  filterPopupType,
  filterConfirmPopupResource
} from "../const/popupResource";

import "react-contexify/dist/ReactContexify.min.css";
import "../style.scss";
import {
  isDebug,
  GROUP_OP_TYPE,
  SELECT_POPUP_KEY_FIELD_TYPE,
  VIEW_TYPE,
  FIELD_TYPE,
  MAX_SELECT_KEY_FIELD,
  FIXED_OPR_EQ,
  MAX_SELECT_FIELD_OPR_EQ,
  FIXED_OPR_EQ_VALUE_DELIMIT
} from "../const/filterConfig";
import { FormattedMessage } from "react-intl";

export default class FilterCndtnTree extends Component {
  state = {
    selectedFieldForChange: null,
    checkedFieldList: []
  };

  componentDidMount = () => {
    this.props.cndtnData && this.getFieldYnList(this.props.cndtnData);
    isDebug &&
      console.log(
        "componentDidMount: checkedFieldList",
        this.state.checkedFieldList
      );
  };

  // action에 의한 동작 정의
  changeGroupOpr = (row, type) => {
    row.groupOprtr = type;
    row.changeState = row.changeState === "o" ? "c" : row.changeState; // 변경되지 않은 상태만 변경(c)으로 수정.
    this.props.changeCndtnData(this.props.cndtnData);
  };

  findParent = (list, row) => {
    for (let data of list) {
      if (data === row) {
        return list;
      }

      if (data.sub && data.sub.length > 0) {
        let parent = this.findParent(data.sub, row);
        if (parent) return parent;
      }
    }
  };

  removeRow = row => {
    let parent;
    if (row.depth === 0) {
      parent = this.props.cndtnData;
    } else {
      parent = this.findParent(this.props.cndtnData, row);
    }

    isDebug && console.log("removeRow", parent, row);
    let idx = parent.indexOf(row);
    parent.splice(idx, 1);

    this.props.deleteCndtn(row);

    this.props.changeCndtnData(this.props.cndtnData);
  };

  deletGroupOpr = row => {
    if (row.sub && row.sub.length > 0) {
      // confirm 팝업
      let resource = filterConfirmPopupResource(
        filterPopupType.deleteGroupOpr,
        this.props.closePopup,
        () => this.removeRow(row)
      );
      this.props.openConfirmPopup(resource);
    } else {
      // 삭제
      this.removeRow(row);
      if (row === this.props.selectedCndtnRow)
        this.props.changeSelectCndtnRow(this.props.cndtnData);
    }
  };

  delectFieldRow = row => {
    this.removeRow(row);
  };

  // 화면 구성을 위한 구현
  contextMenu = row => (
    <Menu id={`mc_${this.getID(row)}`}>
      {this.props.groupOprCode.map((opt, i) => (
        <Item
          key={`it-${this.getID(row)}-${i}`}
          onClick={() => {
            this.changeGroupOpr(row, opt.id);
          }}
        >
          {opt.id}
        </Item>
      ))}
      <Item
        key={`it-${this.getID(row)}-delete`}
        onClick={() => this.deletGroupOpr(row)}
      >
        <FormattedMessage id="delete"> {text => text} </FormattedMessage>
      </Item>
    </Menu>
  );

  selectedGroupOpr = row => {
    isDebug && console.log("selectedGroupOpr", row);
    this.props.selectedCndtnRow === row
      ? this.props.changeSelectCndtnRow(this.props.cndtnData) // reset해서 root를 가르키도록 함.
      : this.props.changeSelectCndtnRow(row);
  };

  cutEQOprValue = row => {
    let index = row.cndtnValue.indexOf(FIXED_OPR_EQ_VALUE_DELIMIT);
    if (row.oprtr === FIXED_OPR_EQ && index !== -1) {
      row.cndtnValue = row.cndtnValue.slice(0, index);
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          filterPopupType.maxSelectCount,
          null,
          this.props.closePopup,
          MAX_SELECT_FIELD_OPR_EQ
        )
      );

      return true;
    }

    return false;
  };

  selectedFieldOpr = (selectedItem, row) => {
    if (row.oprtr !== selectedItem.value) {
      row.oprtr = selectedItem.value;
      row.changeState = row.changeState === "o" ? "c" : row.changeState; // 변경되지 않은 상태만 변경(c)으로 수정.

      this.cutEQOprValue(row); // 추가 요구로 == 일때 ,값 앞에 하나만 남기기

      isDebug &&
        console.log(
          "selectedFieldOpr",
          selectedItem,
          row,
          this.props.cndtnData
        );
      this.props.changeCndtnData(this.props.cndtnData);
    }
  };

  getFieldYnList = list => {
    this.props.cndtnData.map(row => {
      if (row.keyFieldYn && row.keyFieldYn === "Y") {
        list.push(row);
        this.setState({ checkedFieldList: list });
      }

      row.sub && row.sub.length > 0 && this.getFieldYnList(row.sub);
    });
  };

  selectCheckBox = row => {
    if (row.keyFieldYn && row.keyFieldYn === "Y") {
      row.keyFieldYn = "N";
      // 삭제
      let list = this.state.checkedFieldList;
      let idx = list.indexOf(row);
      list.splice(idx, 1);
      this.setState({ checkedFieldList: list });

      row.changeState = row.changeState === "o" ? "c" : row.changeState; // 변경되지 않은 상태만 변경(c)으로 수정.
    } else {
      if (this.state.checkedFieldList.length < MAX_SELECT_KEY_FIELD) {
        row.keyFieldYn = "Y";
        // 추가
        let list = this.state.checkedFieldList;
        list.push(row);
        this.setState({ checkedFieldList: list });

        row.changeState = row.changeState === "o" ? "c" : row.changeState; // 변경되지 않은 상태만 변경(c)으로 수정.
      } else {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.maxSelectCount,
            null,
            this.props.closePopup,
            MAX_SELECT_KEY_FIELD
          )
        );
        return;
      }
    }
    isDebug && console.log("selectCheckBox", row, this.state.checkedFieldList);
    this.props.changeCndtnData(this.props.cndtnData);
  };

  openSelectPopup = row => {
    this.setState({ selectedFieldForChange: row });
    this.props.openSelectFieldPopup(
      SELECT_POPUP_KEY_FIELD_TYPE[row.field.toLowerCase()],
      this.setCndtnValueFromPopup,
      row
    );
  };

  // 화면설계서 내용
  // - companyNm: “고객사 선택” 화면, “고객사명” 세팅 > companyName
  // - agentIp: “자산 선택” 화면, “장비 IP” 세팅 > agentIp
  // - hostname: “자산 선택” 화면, “장비명” 세팅 > agentHosts
  // - vendor: “장비 벤더 선택” 화면, “벤더 명” 세팅 > agentVendorName
  // - deviceModel: “장비 모델 선택” 화면, “모델 명” 세팅 > agentModelName

  setCndtnValueFromPopup = (mapData, target) => {
    let row = this.state.selectedFieldForChange;
    let value;
    isDebug && console.log("setCndtnValueFromPopup:", mapData, row);

    [...mapData.values()].map(data => {
      isDebug && console.log("setCndtnValueFromPopup:", data);
      switch (row.field.toLowerCase()) {
        case FIELD_TYPE.AGENT_IP:
          value = data.agentIp;
          break;
        case FIELD_TYPE.HOST_NM:
          value = data.agentModelName;
          break;
        case FIELD_TYPE.COMPANY_NM:
          value = data.companyName;
          break;
        case FIELD_TYPE.VENDOR:
          value = data.agentVendorName;
          break;
        case FIELD_TYPE.DEVICE_MODEL:
          value = data.agentModelName;
          break;
        default:
          break;
      }
      row.cndtnValue =
        row.cndtnValue && row.cndtnValue.length > 0
          ? `${row.cndtnValue},${value}`
          : `${value}`;
      return null;
    });

    row.changeState = row.changeState === "o" ? "c" : row.changeState;
    this.props.changeCndtnData(this.props.cndtnData);
  };

  onChageCndtnValueFromInput = (e, row) => {
    //isDebug && console.log("onChageCndtnValueFromInput", e.target.value, row)

    row.cndtnValue = e.target.value;
    row.changeState = row.changeState === "o" ? "c" : row.changeState;
    this.props.changeCndtnData(this.props.cndtnData);
  };

  onBlurCndtnInput = (e, row) => {
    let isChanged = this.cutEQOprValue(row);
    if (isChanged) {
      row.changeState = row.changeState === "o" ? "c" : row.changeState;
      this.props.changeCndtnData(this.props.cndtnData);
    }
  };

  getSubSize = sub => (sub != null ? Object.keys(sub).length : 0);
  getClassName = row =>
    this.getSubSize(row.sub) > 0 ? "has-child active" : "has-child";
  hasContextMenu = row => (row.groupOprtr ? true : false);
  getID = row => `${row.depth}-${row.order}`;
  isViewMode = () => this.props.viewType === VIEW_TYPE.VIEW;

  dropdownProps = row => {
    let option = [];
    this.props.fieldOprCode.map(data => {
      option.push({ text: data.id, value: data.name });
    });
    return {
      options: option,
      selected: row ? row.oprtr : "",
      disabled: this.isViewMode() ? true : false,
      name: "selectOpr" /*row*/,
      hasSelect: false,
      onChange: selectedItem => this.selectedFieldOpr(selectedItem, row)
    };
  };

  tooltipId = row => `cndtnTooltip-${row.depth}-${row.order}-${row.field}`;

  fieldRow = row => {
    return this.isViewMode() ? (
      <div className="child">
        <span className="icon" />
        <div className="checkbox checkbox--dark">
          {row.keyFieldYn === "Y" && (
            <label>
              <input type="checkbox" checked={true} disabled />
              <div className="icon" />
            </label>
          )}
        </div>
        <button className="field__badge" style={{ color: "#f5a623" }}>
          {row.fieldType}
        </button>
        <span style={{ marginRight: 5 }}>{row.field}</span>
        <span style={{ marginRight: 5 }}>{row.oprtr}</span>
        <span style={{ marginRight: 5 }}>{row.cndtnValue}</span>
        {/* <span type="text" className="form-control" 
            data-tip data-for={this.tooltipId(row)}
            style={{backgroundColor: "#383b48", color: "#ffffff", border: "1px solid #1b1e29"}}>{row.cndtnValue}</span> 
        {row.cndtnValue && <ReactTooltip  id={this.tooltipId(row)} className="tooltipClass"
            place="right" type="dark" effect="float">
            {row.cndtnValue} </ReactTooltip>} */}
      </div>
    ) : (
      <div className="child">
        <span className="icon" />
        {row.fieldType === "S" && (
          <div className="checkbox checkbox--dark">
            <label>
              <input
                type="checkbox"
                onChange={() => this.selectCheckBox(row)}
                checked={row.keyFieldYn.indexOf("Y") > -1}
              />
              <div className="icon" />
            </label>
          </div>
        )}
        <button className="field__badge" style={{ color: "#f5a623" }}>
          {row.fieldType}
        </button>
        <span className="title" style={{ marginRight: 5 }}>
          {row.field}
        </span>
        <Dropdown {...this.dropdownProps(row)} />
        <input
          type="text"
          className="form-control"
          data-tip
          data-for={this.tooltipId(row)}
          value={row.cndtnValue}
          onChange={e => this.onChageCndtnValueFromInput(e, row)}
          onBlur={e => this.onBlurCndtnInput(e, row)}
        />
        {row.cndtnValue && (
          <ReactTooltip
            id={this.tooltipId(row)}
            className="tooltipClass"
            place="right"
            type="dark"
            effect="float"
          >
            {row.cndtnValue}
          </ReactTooltip>
        )}
        {row.field && SELECT_POPUP_KEY_FIELD_TYPE[row.field.toLowerCase()] && (
          <button
            onClick={() => this.openSelectPopup(row)}
            className="btn btn-icon"
            style={{ marginLeft: 5 }}
          >
            <img src="/images/common/ic_search.png" alt="ic_search" />
          </button>
        )}
        <button
          className="btn btn-icon btn--close"
          onClick={() => {
            this.delectFieldRow(row);
          }}
        />
      </div>
    );
  };

  getOptBtn = optId => {
    switch (optId) {
      case GROUP_OP_TYPE.AND:
        return (
          <button
            className="btn btn-icon btn--and"
            style={{ marginRight: 5 }}
          />
        );
      case GROUP_OP_TYPE.OR:
        return (
          <button className="btn btn-icon btn--or" style={{ marginRight: 5 }} />
        );
      case GROUP_OP_TYPE.NOT:
        return (
          <button
            className="btn btn-icon btn--not"
            style={{ marginRight: 5 }}
          />
        );
      default:
        return "";
    }
  };

  groupOprRow = row => {
    return this.isViewMode() ? (
      <React.Fragment>
        <div className={this.getClassName(row)}>
          <span className="icon" />
          {this.getOptBtn(row.groupOprtr)}
          <span className="title" style={{ marginRight: 5 }}>
            {row.groupOprtr}
          </span>
        </div>
      </React.Fragment>
    ) : (
      <React.Fragment>
        <MenuProvider
          id={`mc_${this.getID(row)}`}
          className={this.getClassName(row)}
          onClick={() => this.selectedGroupOpr(row)}
        >
          <span className="icon" />
          {this.getOptBtn(row.groupOprtr)}
          <span className="title" style={{ marginRight: 5 }}>
            {row.groupOprtr}
          </span>
          {this.props.selectedCndtnRow === row && (
            <img src="/images/common/radio_on_dark.png" alt="icon_checked" />
          )}
        </MenuProvider>
        {this.hasContextMenu(row) && this.contextMenu(row)}
      </React.Fragment>
    );
  };

  CndtnTree = props => (
    <ul>
      {props.treeData.map((row, i) => (
        <li key={this.getID(row)} id={this.getID(row)}>
          {row.fieldId && row.fieldId > 0
            ? this.fieldRow(row)
            : this.groupOprRow(row)}

          {this.getSubSize(row.sub) > 0 && (
            <this.CndtnTree treeData={row.sub} />
          )}
        </li>
      ))}
    </ul>
  );

  render() {
    //isDebug && console.log("FilterCndtnTree: ", this.props.cndtnData, this.props.viewType);

    return (
      <div className="filter-tree__wrap type-condition">
        {this.props.cndtnData && (
          <this.CndtnTree treeData={this.props.cndtnData} />
        )}
      </div>
    );
  }
}
