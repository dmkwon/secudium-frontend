import React, { Component } from "react";
import { connect } from "react-redux";

import {
  isDebug,
  MAX_FILTER_GROUP,
  FIXED_FILTER_GROUP
} from "../const/filterConfig";

import "react-contexify/dist/ReactContexify.min.css";
import "../style.scss";
import CommonContextMenuTree from "./CommonTree";

class FilterGroupTree extends Component {
  onClickContextMenu = (fg, menuType) => {
    isDebug && console.log("onClickContextMenu", fg, menuType);
    switch (menuType) {
      case "new":
      case "edit":
        this.props.openEditFilterGroupPopup(fg, menuType);
        break;
      case "delete":
        this.props.onClickDeleteOne(fg);
        break;
      default:
        isDebug &&
          console.log("onClickContextMenu: menuType Error!!!", fg, menuType);
        break;
    }
  };

  getContextMenu = fg => {
    switch (fg.filterGroupSeq) {
      case FIXED_FILTER_GROUP.ALL: // 전체
      case FIXED_FILTER_GROUP.UNDEFINED:
      case FIXED_FILTER_GROUP.TEMP:
        return (
          <React.Fragment>
            <ul className="tree-contextmenu">
              <li className="disabled">
                {this.props.language === "ko"
                  ? "필터그룹 - 신규등록"
                  : "Filter Group - New Registration"}
              </li>
              <li className="disabled">
                {this.props.language === "ko"
                  ? "필터그룹 - 수정"
                  : "Filter Group - Edit"}
              </li>
              <li className="disabled">
                {this.props.language === "ko"
                  ? "필터그룹 - 삭제"
                  : "Filter Group - Delete"}
              </li>
            </ul>
          </React.Fragment>
        );

      case FIXED_FILTER_GROUP.SECURITY_EVENT:
      case FIXED_FILTER_GROUP.RULE_EVENT:
        return (
          <ul className="tree-contextmenu">
            <li onClick={() => this.onClickContextMenu(fg, "new")}>
              {this.props.language === "ko"
                ? "필터그룹 - 신규등록"
                : "Filter Group - New Registration"}
            </li>
            <li className="binder" />
            <li className="disabled">
              {this.props.language === "ko"
                ? "필터그룹 - 수정"
                : "Filter Group - Edit"}
            </li>
            <li className="disabled">
              {this.props.language === "ko"
                ? "필터그룹 - 삭제"
                : "Filter Group - Delete"}
            </li>
          </ul>
        );
      default:
        if (fg.depth === MAX_FILTER_GROUP) {
          return (
            <ul className="tree-contextmenu">
              <li className="disabled">
                {this.props.language === "ko"
                  ? "필터그룹 - 신규등록"
                  : "Filter Group - New Registration"}
              </li>
              <li className="binder" />
              <li onClick={() => this.onClickContextMenu(fg, "edit")}>
                {this.props.language === "ko"
                  ? "필터그룹 - 수정"
                  : "Filter Group - Edit"}
              </li>
              <li onClick={() => this.onClickContextMenu(fg, "delete")}>
                {this.props.language === "ko"
                  ? "필터그룹 - 삭제"
                  : "Filter Group - Delete"}
              </li>
            </ul>
          );
        } else {
          return (
            <ul className="tree-contextmenu">
              <li onClick={() => this.onClickContextMenu(fg, "new")}>
                {this.props.language === "ko"
                  ? "필터그룹 - 신규등록"
                  : "Filter Group - New Registration"}
              </li>
              <li onClick={() => this.onClickContextMenu(fg, "edit")}>
                {this.props.language === "ko"
                  ? "필터그룹 - 수정"
                  : "Filter Group - Edit"}
              </li>
              <li onClick={() => this.onClickContextMenu(fg, "delete")}>
                {this.props.language === "ko"
                  ? "필터그룹 - 삭제"
                  : "Filter Group - Delete"}
              </li>
            </ul>
          );
        }
    }
  };

  getTreeNodeTitle = target => target.name;
  getTreeNodeKey = target => `${target.filterGroupSeq}-${target.name}`;

  onSelect = (selectedKeys, event) => {
    isDebug && console.log("selected", selectedKeys, event);
    this.props.handleSubFilterGroup(event.node.props.targetData);
  };

  render() {
    const { filterGroupData } = this.props;

    return (
      <CommonContextMenuTree
        treeData={filterGroupData}
        getTreeNodeTitle={this.getTreeNodeTitle}
        getTreeNodeKey={this.getTreeNodeKey}
        hasContextMenu={true}
        getContextMenu={this.getContextMenu}
        onSelect={this.onSelect}
        isDebug={isDebug}
      />
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({})
)(FilterGroupTree);
