import React, { Component } from "react";
import FilterCommonGrid from "./CommonGrid";
import ReactTooltip from "react-tooltip";

import {
  filterPopupType,
  filterConfirmPopupResource
} from "../const/popupResource";
import {
  isDebug,
  pngIconStyle,
  PAGE_TYPE,
  VIEW_TYPE
} from "../const/filterConfig";
import {
  getStrFilterGridKey,
  getStrFilterPeriod,
  isFilter,
  isFilterGroup,
  getFilterLen,
  getPngIcon
} from "../const/utils";
import { exportObjectToJson } from "../const/jsonUtils";
import IntlMessages from "util/IntlMessages";

import "../style.scss";

class FilterGrid extends Component {
  // 필터명 선택 시, 필터 보기
  onClickFilter = targetFilter => {
    isDebug && console.log("goToTargetPage >> ", targetFilter);
    if (isFilter(targetFilter)) {
      this.props.goToTargetPage(PAGE_TYPE.DETAIL, VIEW_TYPE.VIEW, targetFilter);
    }
  };

  // 필터 - export (팝업 추가함)
  handleExportFilter = resource => {
    exportObjectToJson(resource.target);
  };

  onClickExportBtn = targetFilter => {
    let resource = filterConfirmPopupResource(
      filterPopupType.exportFilter,
      this.props.closePopup,
      this.handleExportFilter
    );

    resource.target = targetFilter;
    this.props.openConfirmPopup(resource);
  };

  // 필터 - 삭제
  onClickDeleteBtn = targetFilter => {
    this.props.onClickDeleteOne(targetFilter);
  };

  // 필터 - 수정(edit)
  onClickEditBtn = targetFilter => {
    // 페이지 스위칭을 위해서 index.js까지 정보 올린다.
    this.props.goToTargetPage(PAGE_TYPE.DETAIL, VIEW_TYPE.EDIT, targetFilter);
  };

  // 필터 그룹 - 삭제
  onClickDeleteFGBtn = targetFG => {
    this.props.onClickDeleteOne(targetFG);
  };

  // 필터 그룹 - 수정(edit)
  onClickEditFGBtn = targetFG => {
    this.props.openEditFilterGroupPopup(targetFG, "edit");
  };

  render() {
    const makeActionBtn = target => {
      let type = target.type;

      switch (type) {
        case "F": // filter의 경우
          return (
            <div>
              <button
                id="eoUndo"
                className="btn btn-icon"
                onClick={() => this.onClickExportBtn(target)}
              >
                <img src="/images/common/icon_export.png" alt="icon_export" />
              </button>
              <button
                id="eoDelete"
                className="btn btn-icon"
                onClick={() => this.onClickDeleteBtn(target)}
              >
                <img src="/images/common/icon_delete.png" alt="icon_delete" />
              </button>
              <button
                className="btn btn-icon btn-rollback"
                onClick={() => this.onClickEditBtn(target)}
              >
                <img src="/images/common/icon_edit.png" alt="icon_edit" />
              </button>
            </div>
          );

        case "G": // filter 그룹의 경우
          return (
            <div>
              <button
                id="eoDelete"
                className="btn btn-icon"
                onClick={() => this.onClickDeleteFGBtn(target)}
              >
                <img src="/images/common/icon_delete.png" alt="icon_delete" />
              </button>
              <button
                className="btn btn-icon btn-rollback"
                onClick={() => this.onClickEditFGBtn(target)}
              >
                <img src="/images/common/icon_edit.png" alt="icon_edit" />
              </button>
            </div>
          );

        default:
          return "";
      }
    };

    const columns = [
      {
        Header: <IntlMessages id="filter.name" />,
        accessor: "name",
        width: 250,
        style: { textAlign: "left", paddingLeft: "10px", paddingRight: "10px" },
        Cell: cell => {
          const tooltipId = `filterNmTooltip_${cell.original.filterGroupSeq}`;
          //isDebug && cell.index === 0 && console.log(cell);
          let target = cell.original;
          if (isFilter(target)) {
            return (
              <React.Fragment>
                <div className="icon-box">
                  {target.filterIconImg && (
                    <img
                      id="testpngIcon"
                      src={getPngIcon(target)}
                      alt=""
                      style={pngIconStyle}
                    />
                  )}
                  <span
                    className="link-style"
                    data-tip
                    data-for={tooltipId}
                    onClick={() => this.onClickFilter(target)}
                  >
                    {target.name}
                  </span>
                </div>
                <ReactTooltip
                  id={tooltipId}
                  className="tooltipClass"
                  place="right"
                  type="dark"
                  effect="float"
                  event="mouseover"
                  globalEventOff="mouseout"
                >
                  {target.name}
                </ReactTooltip>
              </React.Fragment>
            );
            // return (
            //   <div className="icon-box">
            //     {target.filterIconImg && (
            //     <img id="testpngIcon" src={getPngIcon(target)} alt="" style={pngIconStyle}/>)}
            //     <span className="link-style" data-tip={target.name} data-for="tooltips"
            //         onClick={() => this.onClickFilter(target)}>{target.name}</span>
            //   </div> )
          } else {
            // 그룹의 경우, 링크 제거
            return (
              <React.Fragment>
                <span data-tip data-for={tooltipId}>
                  {target.name}
                </span>
                <ReactTooltip
                  id={tooltipId}
                  className="tooltipClass"
                  place="right"
                  type="dark"
                  effect="float"
                  event="mouseover"
                  globalEventOff="mouseout"
                >
                  {target.name}
                </ReactTooltip>
              </React.Fragment>
            );
            // return <span data-tip={target.name} data-for="tooltips">{target.name}</span>
          }
        }
      },
      {
        Header: <IntlMessages id="rule.number.used" />,
        accessor: "ruleCnt",
        width: 120,
        style: { textAlign: "right", paddingLeft: "10px", paddingRight: "10px" }
      },
      {
        Header: <IntlMessages id="apply.period" />,
        accessor: "filterCommitPeriodStrDay",
        width: 300,
        style: {
          textAlign: "center",
          paddingLeft: "10px",
          paddingRight: "10px"
        },
        Cell: cell => {
          //isDebug && cell.index === 0 && console.log(cell);
          return getStrFilterPeriod(cell.original);
        }
      },
      {
        Header: <IntlMessages id="filter.group" />,
        accessor: "filterGroupNmPath",
        width: 320,
        style: { textAlign: "left", paddingLeft: "10px", paddingRight: "10px" },
        Cell: cell => {
          const tooltipId = `filterGroupTooltip_${cell.original.filterGroupSeq}`;
          return (
            <React.Fragment>
              <div data-tip data-for={tooltipId}>
                {cell.value}
              </div>
              <ReactTooltip
                id={tooltipId}
                className="tooltipClass"
                place="right"
                type="dark"
                effect="float"
                event="mouseover"
                globalEventOff="mouseout"
              >
                {cell.value}
              </ReactTooltip>
            </React.Fragment>
          );
          // return <div data-tip={value} data-for="tooltips" >{value}</div>
        }
      }
    ];

    const isDisableCheckBox = target => isFilterGroup(target);
    const checkSelected = target =>
      this.props.seletedItems.has(getStrFilterGridKey(target));
    const checkAllSelected = () => {
      let filterLen = getFilterLen(this.props.filterListData);
      //isDebug && console.log("checkAllSelected", filterLen);
      return this.props.seletedItems.size > 0
        ? filterLen === this.props.seletedItems.size
        : false;
    };

    // 하나 체크인 경우와 all 체크인 경우를 구분해서 처리
    const onClickCkeckBox = (isAll, data = null) => {
      let items;

      if (isAll) {
        items = new Map();
        if (checkAllSelected() === false) {
          this.props.filterListData.map(item => {
            if (isFilter(item)) {
              items.set(getStrFilterGridKey(item), item);
            }
          });
        }
      } else {
        items = new Map(this.props.seletedItems);
        let key = getStrFilterGridKey(data);
        items.has(key) ? items.delete(key) : items.set(key, data);
      }
      isDebug && console.log("onClickCkeckBox", items);
      this.props.onChangeCKBox(items);
    };

    return (
      <div>
        <FilterCommonGrid
          // 필수 항목
          grideColumns={columns}
          gridData={this.props.filterListData}
          // 선택 항목
          hasNo={true}
          hasCheckBox={true}
          isDisableCheckBox={isDisableCheckBox}
          seletedItems={this.props.seletedItems} // Map 자료형임, readOnly
          checkSelected={checkSelected}
          checkAllSelected={checkAllSelected}
          onClickCkeckBox={onClickCkeckBox}
          hasAction={true}
          makeActionBtn={makeActionBtn}
          // reative gride 자체 옵션
          defaultPageSize={10}
          pageSizeOptions={[10, 15, 20]}
        />
      </div>
    );
  }
}

export default FilterGrid;
