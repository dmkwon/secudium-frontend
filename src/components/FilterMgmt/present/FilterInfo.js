import React, { Component } from "react";
import DatePicker from "react-datepicker";
import { Textbox } from "react-inputs-validation";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

import {
  filterPopupType,
  filterConfirmPopupResource
} from "../const/popupResource";
import {
  isDebug,
  VIEW_TYPE,
  FILTER_GROUP_TYPE,
  pngHeader,
  pngIconStyle
} from "../const/filterConfig";
import {
  convertDateToStr,
  getPngIcon,
  getStrFileSize,
  getStrFilterPeriod
} from "../const/utils";

class FilterInfo extends Component {
  state = {
    pngFileNm: ""
  };

  componentWillMount() {
    isDebug &&
      console.log(
        "FilterInfo, componentWillMount",
        this.props.filter,
        this.props.detailPageType
      );
  }

  componentDidMount() {
    isDebug &&
      console.log(
        "FilterInfo, componentDidMount",
        this.props.filter,
        this.props.detailPageType
      );
  }

  onChangeFilterNm = (value, e) => {
    e.preventDefault();
    //isDebug && console.log("onChangeFilterNm: ", e);

    let changeFilter = { ...this.props.filter, name: value };
    this.props.changeFilterInfo(changeFilter);
  };

  onChangeFilterDesc = e => {
    e.preventDefault();
    //isDebug && console.log("onChangeFilterDesc: ", e.target);

    let changeFilter = { ...this.props.filter };
    changeFilter.filterDesc = e.target.value;
    this.props.changeFilterInfo(changeFilter);
  };

  isRuleEvent = () =>
    this.props.filter &&
    this.props.filter.status === FILTER_GROUP_TYPE.RULE_EVENT;

  isCheckedPeriodYn = () => {
    return this.props.filter && this.props.filter.filterCommitPeriodYn === "Y"
      ? true
      : false;
  };

  getRadioBgImg = isChecked => {
    return isChecked ? (
      <div
        className="icon"
        style={{ backgroundImage: "url(/images/common/radio_on_dark.png)" }}
      />
    ) : (
      <div
        className="icon"
        style={{ backgroundImage: "url(/images/common/radio_off_dark.png)" }}
      />
    );
  };

  onChangePeriodYn = e => {
    e.preventDefault();

    let changeFilter = { ...this.props.filter };
    changeFilter.filterCommitPeriodYn = this.isCheckedPeriodYn() ? "N" : "Y";

    //isDebug && console.log("onChangePeriodYn: ", e.target, this.props.filter, changeFilter);
    this.props.changeFilterInfo(changeFilter);
  };

  getDefaultDateStart = () => {
    const { filter } = this.props;
    return filter.filterCommitPeriodStrDay
      ? new Date(filter.filterCommitPeriodStrDay)
      : new Date();
  };

  getDefaultDateEnd = () => {
    const { filter } = this.props;
    return filter.filterCommitPeriodEndDay
      ? new Date(filter.filterCommitPeriodEndDay)
      : new Date();
  };

  handleDateStart = date => {
    isDebug && console.log("handleDateStart: ", date);

    let changeFilter = { ...this.props.filter };
    changeFilter.filterCommitPeriodStrDay = convertDateToStr(date);
    this.props.changeFilterInfo(changeFilter);
  };

  handleDateEnd = date => {
    isDebug && console.log("handleDateEnd: ", date);

    let changeFilter = { ...this.props.filter };
    changeFilter.filterCommitPeriodEndDay = convertDateToStr(date);
    this.props.changeFilterInfo(changeFilter);
  };

  isEditable = () => {
    switch (this.props.detailPageType) {
      case VIEW_TYPE.VIEW:
        return false;

      case VIEW_TYPE.NEW:
      case VIEW_TYPE.EDIT:
        return true;

      default:
        isDebug && console.log("isEditable Error!!! undefine detailPageType");
        return false;
    }
  };
  removePngIcon = () => {
    this.setState({ pngFileNm: "" });

    let changeFilter = { ...this.props.filter };
    changeFilter.filterIconImg = null;
    this.props.changeFilterInfo(changeFilter);
  };

  loadingIconFile = e => {
    let importPngFile = e.target.files[0];
    // isDebug && console.log("loadingIconFile file=", importPngFile);

    if (importPngFile.size > 0 && importPngFile.size < 2 * 1024) {
      // 사이즈 제한, accept="image/png" 사용
      let reader = new FileReader();
      reader.readAsDataURL(importPngFile);
      reader.onload = e => {
        // 파일명 설정.
        this.setState({ pngFileNm: importPngFile.name });

        // 이미지 필터에 설정.
        let pngRowData = e.target.result.replace(pngHeader, ""); //"data:image/png;base64,"
        let changeFilter = { ...this.props.filter };
        changeFilter.filterIconImg = pngRowData; // DB에 저장될
        this.props.changeFilterInfo(changeFilter);
        isDebug &&
          console.log(
            "loadingIconFile pngRowData=",
            pngRowData,
            importPngFile,
            changeFilter
          );
      };
    } else {
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          filterPopupType.notSupportFileSize,
          null,
          this.props.closePopup,
          getStrFileSize(importPngFile.size)
        )
      );
    }
  };

  renderEditMode = () => {
    const { filter, validation } = this.props;
    //isDebug && console.log("Filter=", filter)

    return (
      <tbody>
        <tr>
          <th>
            <FormattedMessage id="filter.group">
              {text => text}
            </FormattedMessage>
            <button
              className="btn btn-icon btn--search"
              onClick={this.props.openSelectFilterGroupPopup}
            />
          </th>
          <td> {filter && filter.filterGroupNmPath} </td>
        </tr>
        <tr>
          <th>
            <FormattedMessage id="filter.name">{text => text}</FormattedMessage>
            <span className="font--red">*</span>
          </th>
          <td style={{ paddingRight: 10, paddingLeft: 10 }}>
            <Textbox
              className="form-control"
              value={filter.name ? filter.name : ""}
              onChange={this.onChangeFilterNm}
              onBlur={() => {}}
              validate={validation.isVlidation}
              validationCallback={res => {
                let chValication = {
                  ...validation,
                  hasfGroupName: res,
                  validate: false
                };
                this.props.changeValidation(chValication);
              }}
              validationOption={{
                locale: this.props.language,
                check: true,
                required: true,
                min: 2,
                max: 50, // 100/2
                msgOnError: (
                  <FormattedMessage id="field.require.fifty">
                    {text => text}
                  </FormattedMessage>
                )
              }}
              customStyleInput={{
                height: "24px",
                width: "100%",
                background: "#383b48",
                color: "#ffffff",
                outline: "0px",
                border: "1px solid #1b1e29",
                padding: "0 10px"
              }}
            />
          </td>
        </tr>
        <tr className="desc">
          <th>
            <IntlMessages id="desc" />
          </th>
          <td>
            <textarea
              className="form-control"
              name="filterDesc"
              onChange={this.onChangeFilterDesc}
              value={filter.filterDesc ? filter.filterDesc : ""}
            />
          </td>
        </tr>

        {this.isRuleEvent() && (
          <tr className="period">
            <th>
              <FormattedMessage id="apply.period">
                {text => text}
              </FormattedMessage>
              <span className="font--red">*</span>
            </th>
            <td style={{ paddingLeft: 0 }}>
              <div className="radio" style={{ marginLeft: 10 }}>
                <label>
                  <input
                    type="radio"
                    onChange={this.onChangePeriodYn}
                    value="Y"
                    checked={this.isCheckedPeriodYn()}
                  />
                  {this.getRadioBgImg(this.isCheckedPeriodYn())}
                  <span>
                    <FormattedMessage id="period">
                      {text => text}
                    </FormattedMessage>
                  </span>
                </label>
              </div>
              <div className="radio" style={{ marginLeft: 10 }}>
                <label>
                  <input
                    type="radio"
                    onChange={this.onChangePeriodYn}
                    value="N"
                    checked={!this.isCheckedPeriodYn()}
                  />
                  {this.getRadioBgImg(!this.isCheckedPeriodYn())}
                  <span>
                    <FormattedMessage id="permanent">
                      {text => text}
                    </FormattedMessage>
                  </span>
                </label>
              </div>
              <p className="row" />
              {this.isCheckedPeriodYn() && (
                <div style={{ padding: "0 10px" }}>
                  <DatePicker
                    className="form-control"
                    onChange={this.handleDateStart}
                    selected={this.getDefaultDateStart()}
                    dateFormat="yyyy-MM-dd"
                  />
                  <span style={{ margin: "0 5px" }}>-</span>
                  <DatePicker
                    className="form-control"
                    onChange={this.handleDateEnd}
                    selected={this.getDefaultDateEnd()}
                    dateFormat="yyyy-MM-dd"
                  />
                </div>
              )}
            </td>
          </tr>
        )}

        {this.isRuleEvent() && (
          <tr className="icon">
            <th>
              <FormattedMessage id="related.icons">
                {text => text}
              </FormattedMessage>
            </th>
            <td>
              {filter.filterIconImg && (
                <div style={{ float: "left", marginRight: 10 }}>
                  <div className="icon-box" style={pngIconStyle}>
                    <img
                      id="testpngIcon"
                      src={getPngIcon(filter)}
                      alt="I"
                      style={pngIconStyle}
                    />
                  </div>
                  <button
                    className="btn btn-icon btn--dark-close"
                    onClick={this.removePngIcon}
                    style={pngIconStyle}
                  />
                </div>
              )}
              <div style={{ float: "left" }}>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.pngFileNm}
                  style={{ width: 120, padding: 5, color: "#ffffff" }}
                  disabled
                />
              </div>
              <div
                className="file-import"
                style={{ padding: 0, float: "left", margin: 0 }}
              >
                <input
                  id="importPngBtn"
                  type="file"
                  className="form-control"
                  onChange={this.loadingIconFile}
                  accept="image/png"
                />
                <label
                  htmlFor="importPngBtn"
                  className="btn btn-icon btn--file small"
                  style={{
                    backgroundImage: "url(/images/common/icon-file.png)",
                    margin: 0,
                    ...pngIconStyle
                  }}
                />
              </div>
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  renderViewMode = () => {
    const { filter } = this.props;
    isDebug &&
      console.log(
        "FilterInfo, renderViewMode",
        this.props.filter,
        this.props.detailPageType
      );
    return (
      <tbody>
        <tr>
          <th>
            <FormattedMessage id="filter.group">
              {text => text}
            </FormattedMessage>
          </th>
          <td>{filter ? filter.filterGroupNmPath : ""}</td>
        </tr>
        <tr>
          <th>
            <FormattedMessage id="filter.name">{text => text}</FormattedMessage>
          </th>
          <td style={{ paddingRight: 10, paddingLeft: 10 }}>
            {filter ? filter.name : ""}
          </td>
        </tr>
        <tr className="desc">
          <th>
            <IntlMessages id="desc" />
          </th>
          <td>
            <textarea
              className="form-control"
              name="filterDesc"
              readOnly
              value={filter && filter.filterDesc ? filter.filterDesc : ""}
            />
          </td>
        </tr>
        {this.isRuleEvent() && (
          <tr className="period">
            <th>
              <FormattedMessage id="apply.period">
                {text => text}
              </FormattedMessage>
            </th>
            <td>{getStrFilterPeriod(filter)}</td>
          </tr>
        )}
        {this.isRuleEvent() && (
          <tr className="icon">
            <th>
              <FormattedMessage id="related.icons">
                {text => text}
              </FormattedMessage>
            </th>
            <td>
              {filter.filterIconImg && (
                <div className="icon-box">
                  <img
                    id="testpngIcon"
                    src={getPngIcon(filter)}
                    alt="I"
                    style={{ width: 20, height: 20 }}
                  />
                </div>
              )}
            </td>
          </tr>
        )}
      </tbody>
    );
  };

  render() {
    return (
      <div className="table-wrapper">
        <table className="table table--dark table--info security-event">
          <colgroup>
            <col width="130px" />
            <col />
          </colgroup>
          {this.isEditable() ? this.renderEditMode() : this.renderViewMode()}
        </table>
      </div>
    );
  }
}

export default FilterInfo;
