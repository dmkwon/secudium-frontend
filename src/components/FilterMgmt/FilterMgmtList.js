import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { action as filterAction } from "../../store/modules/filterMgmtListAction";

import FilterGroupTree from "./present/FilterGroupTree";
import FilterGrid from "./present/FilterGrid";

import {
  filterPopupType,
  filterConfirmPopupResource
} from "./const/popupResource";
import {
  isDebug,
  FIXED_FILTER_GROUP,
  PAGE_TYPE,
  VIEW_TYPE
} from "./const/filterConfig";

import "./style.scss";
import { isFilter, getMapsValueArray, find1DepthGroup } from "./const/utils";
import { FormattedMessage } from "react-intl";

class FilterMgmtListComponent extends Component {
  state = {
    searchKeyWord: "",

    sortKey: "reg_date", // TODO: 제거 필요, grid 기능 사용하면 되고, 따로 sorting은 필요 없음.
    sortType: "ASC",

    selectedALL: false,
    seletedItems: new Map() // 체크 박스에 선택된 항목들 (Map형)
  };

  componentDidMount() {
    // 초기 데이터 가져오기
    this.handleLoadData(null);
  }

  // 데이터 가져오는 ------------------------------------
  // 초기데이터: 그룹 전체, 필터 전체 가져옴 - 검색어가 있으면 검색어에 맞게
  handleLoadData = async searchKeyWord => {
    const { filterAction } = this.props;
    this.props.setLoading(true);
    this.setState({ seletedItems: new Map() });

    try {
      let data = await filterAction.getInitData(
        {
          /*searchKeyWord*/
        },
        { searchKeyWord }
      );
      isDebug && console.log("handleLoadData", data);
    } catch (error) {
      console.error("handleLoadData::", error);
      //this.props.errorNotiPopup(error);
    } finally {
      this.props.setLoading(false);
    }
  };

  // 필터 그룹 트리에서 선택시 데이터 가져오는 함수
  handleSubFilterGroup = async fg => {
    const { filterAction } = this.props;
    this.props.setLoading(true);

    this.restSearchKeyWord();
    this.setState({ seletedItems: new Map() }); // 체크 박스 리셋

    if (fg.filterGroupSeq === 0) {
      this.handleLoadData("");
    } else {
      try {
        let data = await filterAction.getSubGroupData(fg.filterGroupSeq);

        isDebug && console.log("handleSubFilterGroup", data);
      } catch (error) {
        console.error("handleSubFilterGroup::", error);
        //this.props.errorNotiPopup(error);
      } finally {
        this.props.setLoading(false);
      }
    }
  };

  // 필터에 대한 처리 ------------------------------------
  // 선택된 필터(하나, 여러개) 삭제 > 휴지통 그룹으로 이동
  handleDelFilter = async (resource, changeReason) => {
    // 0. 삭제
    // const { filterAction } = this.props
    // this.props.setLoading(true);
    // this.restSearchKeyWord();

    // let varray = getMapsValueArray(resource.target);
    // let updateFilterList = [...varray];

    // updateFilterList.map((item) => {
    //   item.useYn = "N";
    //   item.filterChgReason = changeReason;
    //   return null;
    // })

    // isDebug && console.log("handleDelFilter", updateFilterList);
    // try {
    //   let data = await filterAction.editFilters(updateFilterList)
    //   isDebug && console.log("handleDelFilter", data)
    // } catch (error) {
    //   console.error(error)
    // } finally {
    //   //this.props.setLoading(false);
    //   this.handleLoadData("");
    // }

    // 1. 필터 삭제시 휴지통으로 이동(기능 변경)
    let trash = find1DepthGroup(
      this.props.filterGroupData,
      FIXED_FILTER_GROUP.TEMP
    );
    this.handleMoveFTtoParentFG(trash, resource.target, changeReason);
  };

  moveFTtoParentFGCB = (resource, changeReason) => {
    this.handleMoveFTtoParentFG(
      resource.target,
      getMapsValueArray(this.state.seletedItems),
      changeReason
    );
  };

  // 필터 리스트의 그룹 이동
  handleMoveFTtoParentFG = async (parentFG, filterList, changeReason) => {
    isDebug &&
      console.log("handleMoveFTtoParentFG", parentFG, filterList, changeReason);

    const { filterAction } = this.props;
    this.props.setLoading(true);

    this.restSearchKeyWord();
    this.setState({ seletedItems: new Map() });

    let updateFilterList = [...filterList];

    updateFilterList.map(item => {
      item.filterGroupNm = parentFG.name;
      item.filterGroupNmPath = `${parentFG.filterGroupNmPath} > ${parentFG.name}`;
      item.status = parentFG.status;
      item.filterGroupSeq = parentFG.filterGroupSeq;

      item.filterChgReason = changeReason;

      return null;
    });

    isDebug &&
      console.log(
        "handleMoveFTtoParentFG: ",
        this.state.seletedItems,
        updateFilterList
      );

    try {
      let reData = await filterAction.editFilters(updateFilterList);

      isDebug && console.log("handleMoveFTtoParentFG", reData);
    } catch (error) {
      console.error("handleMoveFTtoParentFG::", error);
      //this.props.errorNotiPopup(error);
    } finally {
      //this.props.setLoading(false);
      this.handleLoadData("");
    }
  };

  // 그룹에 대한 처리 ------------------------------------
  // 필터 그룹 삭제
  handleDelFGroup = async resource => {
    // 0. 휴지통 이동
    // let parent = this.findTrash();
    // this.handleMoveFGtoParentFG(resource.target, parent);

    // 1. 필트 그룹 삭제 (기능 변경)
    const { filterAction } = this.props;
    this.props.setLoading(true);

    this.restSearchKeyWord();
    this.setState({ seletedItems: new Map() });

    let updateTarget = resource.target; // 복사 후 변경
    updateTarget.useYn = "N";

    try {
      let reData = await filterAction.editFilterGroup(updateTarget);
      isDebug && console.log("handleMoveFGtoParentFG", reData);
    } catch (error) {
      console.error("handleDelFGroup", error);
      //this.props.errorNotiPopup(error);
    } finally {
      //this.props.setLoading(false);
      this.handleLoadData("");
    }
  };

  // 필터 그룹의 그룹 이동 (기능 변경으로 사용되지 않음)
  // handleMoveFGtoParentFG = async (target, parentFG) => {
  //   const { filterAction } = this.props

  //   // 삭제할 데이터 새로 만들기
  //   let updateTarget = target; // 복사 후 변경
  //   updateTarget.bakFilterGroupSeq = target.topFilterGroupSeq;
  //   updateTarget.topFilterGroupSeq = parentFG.filterGroupSeq;
  //   updateTarget.status = parentFG.status;      // 삭제의 경우 'D'
  //   updateTarget.depth = (parentFG.depth + 1)   // 삭제의 경우 2
  //   updateTarget.order = 0;

  //   try {
  //     let reData = await filterAction.editFilterGroup(updateTarget)
  //     isDebug && console.log("handleMoveFGtoParentFG", reData)
  //   } catch (error) {
  //     console.error(error)
  //   } finally {
  //     this.props.setLoading(false);
  //   }
  // }

  handleSaveFGroup = async (fg, type, updateName) => {
    const { filterAction } = this.props;
    this.props.setLoading(true);

    this.restSearchKeyWord();
    this.setState({ seletedItems: new Map() });

    let updateFG = fg;
    updateFG.name = updateName;

    isDebug && console.log("handleSaveFGroup", fg, type, updateName);
    this.props.setLoading(true);
    try {
      if (type === "new") {
        let data = await filterAction.addFilterGroup(updateFG);
        isDebug && console.log("handleSaveFGroup new:", data);
      } else {
        // edit
        let data = await filterAction.editFilterGroup(updateFG);
        isDebug && console.log("handleSaveFGroup edit:", data);
      }
    } catch (error) {
      console.error("handleSaveFGroup::", error);
      //this.props.errorNotiPopup(error);
    } finally {
      //this.props.setLoading(false);
      this.handleLoadData("");
    }
  };

  // 버튼 클릭 등 이벤트 ----------------------------------------
  onChangeKeyWord = e => {
    // 검색 키워드 입력
    e.preventDefault();

    this.setState({
      searchKeyWord: e.target.value
    });
  };

  onClickSearchBtn = () => {
    // 키워드 검색
    this.handleLoadData(this.state.searchKeyWord);
  };

  onClickResetBtn = () => {
    // 키워드 리셋하고 전체 가져오기
    this.restSearchKeyWord();
    this.handleLoadData(null);
  };

  restSearchKeyWord = () => {
    this.setState({
      searchKeyWord: ""
    });
  };

  onKeyDown = e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.onClickSearchBtn();
    }
  };

  // 그리드에서 사용하는 ckeckbox 상태 저장
  onChangeCKBox = checkedItems => {
    this.setState({ seletedItems: checkedItems });
  };
  // 삭제하기 전에 삭제가능한지 여유에 대한, 유효성 검사(룰 사용 여부 확인해서 삭제 가능한지 확인)
  checkDelValidation = () => {
    let isValide = true;

    this.state.seletedItems.forEach((value, key) => {
      isDebug && console.log("001 checkDelValidation : ", key, value);
      if (value.ruleCnt > 0) {
        isValide = false;
      }
    });
    isDebug && console.log("0002 checkDelValidation : isValide=", isValide);
    return isValide;
  };

  // 선택 항목(여러 개에 대한) 삭제
  onClickDeleteSome = () => {
    const { closePopup, openConfirmPopup } = this.props;

    let popupResource =
      this.state.seletedItems.size > 0
        ? this.checkDelValidation() // 룰 사용 여부 확인해서 삭제 가능한지 검토함.
          ? filterConfirmPopupResource(
              filterPopupType.delFilter,
              closePopup,
              this.handleDelFilter
            ) // 삭제
          : filterConfirmPopupResource(
              filterPopupType.cantDeleteFilterWithRule,
              null,
              closePopup
            ) // 삭제할 수 없음
        : filterConfirmPopupResource(
            filterPopupType.noneSeletedFilter,
            null,
            closePopup
          ); // 선택된 항목 없음.

    popupResource.target = getMapsValueArray(this.state.seletedItems);
    openConfirmPopup(popupResource);
  };

  onClickDeleteOne = target => {
    const { closePopup, openConfirmPopup } = this.props;
    let popupResource;

    isDebug && console.log("onClickDeleteOne", target);

    if (isFilter(target)) {
      // 필터의 경우
      popupResource =
        target.ruleCnt === 0 // 필터의 경우 sql에서 처리
          ? filterConfirmPopupResource(
              filterPopupType.delFilter,
              closePopup,
              this.handleDelFilter
            ) // 삭제
          : filterConfirmPopupResource(
              filterPopupType.cantDeleteFilterWithRule,
              null,
              closePopup
            ); // 삭제할 수 없음
    } else {
      // 필터 그룹의 경우
      popupResource =
        target.depth === 1
          ? filterConfirmPopupResource(
              filterPopupType.cantDelete1DepthFGroup,
              null,
              closePopup,
              target.name
            ) // 삭제 불가
          : (target.sub && target.sub.length > 0) || target.filterCnt > 0 // 그룹은 서브 그룹, 필터 존재여부 확인
          ? filterConfirmPopupResource(
              filterPopupType.cantDeleteFGroup,
              null,
              closePopup,
              target.name
            ) // 삭제 불가
          : filterConfirmPopupResource(
              filterPopupType.delFGroup,
              closePopup,
              this.handleDelFGroup,
              target.name
            ); // 삭제
    }

    popupResource.target = [target];
    openConfirmPopup(popupResource);
  };

  openEditFilterGroupPopup = (target, menuType) => {
    const { openEditFilterGroupPopup } = this.props;

    // 저장/변경 데이터 새로 만들기
    let updateTarget;
    if (menuType === "new") {
      updateTarget = {};
      updateTarget.filterGroupNmPath = `${target.filterGroupNmPath} > ${target.name}`;
      updateTarget.topFilterGroupSeq = target.filterGroupSeq;
      updateTarget.status = target.status;
      updateTarget.depth = target.depth + 1;
      updateTarget.order = 0;
    } else {
      updateTarget = target; // 복사 후 변경
    }

    updateTarget.saveHandle = (target, type, changedName) =>
      this.handleSaveFGroup(target, type, changedName);
    isDebug &&
      console.log(
        "openEditFilterGroupPopup new:",
        menuType,
        target,
        updateTarget
      );
    openEditFilterGroupPopup(updateTarget, menuType);
  };

  // 선택 항목 그룹으로 이동 confirm(확인)
  confirmFilterGroupCB = targetFG => {
    let resource = filterConfirmPopupResource(
      filterPopupType.editFilter,
      this.props.closePopup,
      this.moveFTtoParentFGCB
    );

    resource.target = targetFG;
    this.props.openConfirmPopup(resource);
  };

  // open 필터 그룹 팝업
  onClickSetFilterGroup = () => {
    this.state.seletedItems.size > 0
      ? this.props.openSelectFilterGroupPopup(
          this.props.filterGroupData,
          this.confirmFilterGroupCB
        )
      : this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.noneSeletedFilter,
            null,
            this.props.closePopup
          )
        );
  };

  render() {
    const {
      openEditFilterGroupPopup,
      openConfirmPopup,
      goToTargetPage,
      filterListData,
      filterGroupData
    } = this.props;

    return (
      <div className="component">
        <div className="section__title">
          <FormattedMessage id="filter.manage">{text => text}</FormattedMessage>
        </div>

        <div className="component-content">
          <div className="component filter-tree">
            <div className="filter-tree__wrap">
              {filterGroupData && (
                <FilterGroupTree
                  openEditFilterGroupPopup={this.openEditFilterGroupPopup}
                  onClickDeleteOne={this.onClickDeleteOne}
                  filterGroupData={filterGroupData}
                  handleSubFilterGroup={this.handleSubFilterGroup}
                />
              )}
            </div>
            {/* <Test/> test: rc-tree + contextmenu를 가지는 팝업이 뜬다*/}
          </div>

          {/* filter result */}
          <div className="filter-result">
            <div className="component__title">
              <div className="search-bar">
                <FormattedMessage id="filtername.filtergroup">
                  {placeholder => (
                    <input
                      type="text"
                      className="form-control"
                      placeholder={placeholder}
                      value={this.state.searchKeyWord}
                      onChange={this.onChangeKeyWord}
                      onKeyDown={this.onKeyDown}
                    />
                  )}
                </FormattedMessage>
                <div className="binder" />
                <button
                  className="btn btn-icon small btn--go"
                  onClick={() => this.onClickSearchBtn()}
                />
                <button
                  className="btn btn-icon small btn--filter"
                  onClick={() => this.onClickResetBtn()}
                />
              </div>
              <div className="btns">
                <button
                  className="btn btn--dark"
                  onClick={() => this.onClickDeleteSome()}
                >
                  <FormattedMessage id="multi.delete">
                    {text => text}
                  </FormattedMessage>
                </button>
                <button
                  className="btn btn--dark"
                  onClick={() => this.onClickSetFilterGroup()}
                >
                  <FormattedMessage id="filter.group.setting">
                    {text => text}
                  </FormattedMessage>
                </button>
                <button
                  className="btn btn--blue"
                  onClick={() =>
                    goToTargetPage(PAGE_TYPE.DETAIL, VIEW_TYPE.NEW, null)
                  }
                >
                  <FormattedMessage id="new.reg">
                    {text => text}
                  </FormattedMessage>
                </button>
              </div>
            </div>

            <FilterGrid
              goToTargetPage={goToTargetPage}
              openConfirmPopup={openConfirmPopup}
              filterListData={filterListData}
              seletedItems={this.state.seletedItems} // Map 자료형임
              onChangeCKBox={this.onChangeCKBox}
              openEditFilterGroupPopup={openEditFilterGroupPopup}
              onClickDeleteOne={this.onClickDeleteOne}
              closePopup={this.props.closePopup}
            />
          </div>
          {/* 결과 없음 페이지 입니다. (화면설계서(통합) 35p 정보없음 - 공통) text에 문구를 써주세요.
          결과 없음 페이지가 필요하다면, 트리 옆 하나의 div로 묶인 것 대신 사용하면 됩니다. (필터리스트 기준: .filter-result 대신 사용) */}
          {/* <NoResultComponent text="좌측 트리에서 필터를 선택해주세요." /> */}
        </div>
      </div>
    );
  }
}

const FilterListConnecter = connect(
  state => ({
    filterListData: state.filterMgmtList.get("filterList"),
    filterGroupData: state.filterMgmtList.get("filterGroup"),
    isSuccess: state.filterMgmtList.get("isSuccess")
  }),
  dispatch => ({
    filterAction: bindActionCreators(filterAction, dispatch)
  })
)(FilterMgmtListComponent);

export default FilterListConnecter;
