import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Draggable from "react-draggable";
import {
  filterPopupType,
  filterConfirmPopupResource
} from "../const/popupResource";

import { action as filterAction } from "../../../store/modules/filterMgmtListAction";
import { getCodeList } from "../../../store/api/common";

// 팝업 구성: tree/grid/info/condition/list
import CommonContextMenuTree from "../present/CommonTree";
import { isDebug, VIEW_TYPE } from "../const/filterConfig";

import FilterCommonGrid from "../present/CommonGrid";
import ReactTooltip from "react-tooltip";

import FilterInfo from "../present/FilterInfo";

import FilterCndtnTree from "../present/FilterCndtnTree";

import "./style.scss";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class SelectFilterPopup extends Component {
  state = {
    // 외부로부터 전달된 this.props.selectedFilte를 사용하고 없는 경우는 filterList에 첫번째를 보여줌
    selectedFilter: null,
    searchKeyWord: "",

    cndtnData: null,

    position: {
      x: 0,
      y: 0
    },

    // 공통 코드
    groupOprCode: [],
    fieldOprCode: []
  };

  componentDidMount() {
    const elWidth = document.getElementById("selectFilterPopup").offsetWidth;
    const elHeight = document.getElementById("selectFilterPopup").offsetHeight;

    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });

    // 초기 데이터 가져오기
    this.props.isFilterInfoViewType
      ? this.setState({
          // 필터 하나만 표시 되는 경우
          selectedFilter: this.props.target,
          cndtnData: this.getCndtnData(this.props.target)
        })
      : this.handleLoadData(""); // 필터 전체 리스트
    this.handleGetCommonCode();
  }

  // 전에 상태가 전체 데이터에 대한 것이 아님으로, 데이터 새로 가져오기
  handleLoadData = async searchKeyWord => {
    const { filterAction } = this.props;

    this.props.setLoading(true);

    try {
      //  필터 리스트에 그룹 정보를 포함시키지 않는다 (검색어 포함)
      let data = await filterAction.getInitData({}, { searchKeyWord });
      isDebug && console.log("handleLoadData", data);

      // 화면 설계서 페이지 43, "선택된 필터가 없는 경우"에 대한 내용으로 유추,
      // 초기 선택 항목은 없고, 클릭에 의해서만 선택
      // let selectedFilter = this.props.filterListData[0] ? this.props.filterListData[0]: this.props.target;
      // this.setState({
      //   selectedFilter: selectedFilter,
      //   cndtnData: this.getCndtnData(selectedFilter)
      // });
    } catch (error) {
      console.error("handleLoadData", error);
      // this.props.errorNotiPopup(error); // 룰에서 사용함으로 우선 주석처리, 자체적으로 표현하도롤 수정 필요
    } finally {
      this.props.setLoading(false);
    }
  };

  handleGetCommonCode = async () => {
    try {
      const groupOprCode = await getCodeList("GROUP_OPRTR");
      const fieldOprCode = await getCodeList("FIELD_OPRTR");

      this.setState({
        groupOprCode: groupOprCode.data,
        fieldOprCode: fieldOprCode.data
      });

      isDebug && console.log("공통코드", groupOprCode, fieldOprCode);
    } catch (error) {
      console.error(error);
    }
  };

  // 필터 그룹 트리에서 선택시 데이터 가져오는 함수
  handleSubFilterGroup = async fg => {
    const { filterAction } = this.props;

    this.props.setLoading(true);
    this.restSearchKeyWord();

    if (fg.filterGroupSeq === 0) {
      this.handleLoadData("");
    } else {
      try {
        let data = await filterAction.getSubGroupData(fg.filterGroupSeq);
        isDebug && console.log("handleSubFilterGroup", data);
      } catch (error) {
        console.error("handleSubFilterGroup::", error);
        //this.props.errorNotiPopup(error); // 룰에서 사용함으로 우선 주석처리, 자체적으로 표현하도롤 수정 필요
      } finally {
        this.props.setLoading(false);
      }
    }
  };

  getCndtnData = selectedFilter => {
    if (selectedFilter && selectedFilter.filterCndtnJson) {
      let cndtnData = JSON.parse(selectedFilter.filterCndtnJson);
      return cndtnData;
    }
    return null;
  };

  // 필터 그룹 Tree 구성을 위해 필요한 --------------------------------------
  onChageKeyWord = e => {
    // 검색 키워드 입력
    this.setState({
      searchKeyWord: e.target.value
    });
  };

  onClickSearchBtn = () => {
    // 키워드 검색
    this.handleLoadData(this.state.searchKeyWord);
  };

  restSearchKeyWord = () => {
    this.setState({ searchKeyWord: "" });
  };

  onClickResetBtn = () => {
    // 키워드 리셋하고 전체 가져오기
    this.restSearchKeyWord();
    this.handleLoadData(null);
  };

  onKeyDown = e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      this.onClickSearchBtn();
    }
  };

  // 필터 그룹 Tree 구성을 위해 필요한 --------------------------------------
  getTreeNodeTitle = target => target.name;
  getTreeNodeKey = target => `${target.filterGroupSeq}-${target.name}`;

  onSelectTreeNode = (selectedKeys, event) => {
    isDebug &&
      console.log("selected", selectedKeys, event.node.props.targetData);
    let subNode = event.selected
      ? event.selectedNodes[0].props.targetData
      : null;
    this.handleSubFilterGroup(subNode);
  };

  // 필터 리스트(Grid) 구성을 위해 필요한 --------------------------------------
  onClickFilter = filter => {
    this.setState({
      selectedFilter: filter,
      cndtnData: this.getCndtnData(filter)
    });
  };

  onClickConfirmBtn = () => {
    if (this.state.selectedFilter) {
      this.props.closePopup();
      this.props.seletcFilterCB(this.state.selectedFilter);
    } else {
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          filterPopupType.noneSeletedFilter,
          null,
          this.props.closeConfirmPopup
        )
      );
    }
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { closePopup, isFilterInfoViewType } = this.props;

    const columns = [
      {
        Header: <IntlMessages id="filter.name" />,
        accessor: "name",
        width: 120,
        Cell: cell => {
          let target = cell.original;
          return (
            <span
              className="link-style"
              data-tip={target.name}
              data-for="tooltips"
              onClick={() => this.onClickFilter(target)}
            >
              {target.name}
            </span>
          );
        }
      },
      {
        Header: <IntlMessages id="filter.group" />,
        accessor: "filterGroupNmPath",
        width: 280,
        Cell: ({ value }) => {
          return (
            <div data-tip={value} data-for="tooltips">
              {value}
            </div>
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={this.state.position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--dark popup--filter"
            id="selectFilterPopup"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                {isFilterInfoViewType ? (
                  <FormattedMessage id="filter.info">
                    {text => text}
                  </FormattedMessage>
                ) : (
                  <FormattedMessage id="filter.select">
                    {text => text}
                  </FormattedMessage>
                )}
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <div className="flex-box">
                {/* 왼쪽 트리  */}
                {!isFilterInfoViewType && (
                  <div className="component filter-tree">
                    <div className="filter-tree__wrap">
                      <CommonContextMenuTree
                        treeData={this.props.filterGroupData}
                        getTreeNodeTitle={this.getTreeNodeTitle}
                        getTreeNodeKey={this.getTreeNodeKey}
                        hasContextMenu={false}
                        getContextMenu={null}
                        onSelect={this.onSelectTreeNode}
                        isDebug={isDebug}
                      />
                    </div>
                  </div>
                )}
                {/* 필터 그리드  */}
                {!isFilterInfoViewType && (
                  <div className="filter-table">
                    <div
                      className="search-bar"
                      style={{ borderBottom: 0, paddingBottom: 7 }}
                    >
                      <FormattedMessage id="filtername.filtergroup">
                        {placeholder => (
                          <input
                            type="text"
                            className="form-control"
                            placeholder={placeholder}
                            value={this.state.searchKeyWord}
                            onChange={this.onChageKeyWord}
                            onKeyDown={this.onKeyDown}
                          />
                        )}
                      </FormattedMessage>
                      <div className="binder" />
                      <button
                        className="btn btn-icon small btn--go"
                        onClick={() => this.onClickSearchBtn()}
                      />
                      <button
                        className="btn btn-icon small btn--filter"
                        onClick={() => this.onClickResetBtn()}
                      />
                    </div>

                    <FilterCommonGrid
                      // 필수 항목
                      grideColumns={columns}
                      gridData={this.props.filterListData}
                      // 선택 항목
                      hasNo={true}
                      hasCheckBox={false}
                      hasAction={false}
                      // reative gride 자체 옵션
                      defaultPageSize={10}
                      pageSizeOptions={[10, 15, 20, 25]}
                      columnWidth={{ no: 45 }}
                    />
                    <ReactTooltip
                      id="tooltips"
                      className="tooltipClass"
                      effect="float"
                      place="right"
                    />
                  </div>
                )}
                <div className="filter-info">
                  <div
                    className="component__title"
                    style={{
                      borderBottom: "1px solid #4c4d57",
                      paddingBottom: 12,
                      marginBottom: 0
                    }}
                  >
                    <span>
                      <FormattedMessage id="filter.info">
                        {text => text}
                      </FormattedMessage>
                    </span>
                  </div>
                  <FilterInfo
                    style={{ borderTop: "1px solid #4c4d57" }}
                    openSelectFilterGroupPopup={this.openSelectFilterGroupPopup}
                    detailPageType={VIEW_TYPE.VIEW}
                    filter={this.state.selectedFilter}
                  />
                </div>

                <div className="filter-condition">
                  <div
                    className="component__title"
                    style={{ paddingBottom: 12, marginBottom: 0 }}
                  >
                    <span>
                      <FormattedMessage id="filter.condition">
                        {text => text}
                      </FormattedMessage>
                    </span>
                  </div>

                  <div className="component__box" style={{ width: 400 }}>
                    <FilterCndtnTree
                      cndtnData={this.state.cndtnData}
                      groupOprCode={this.state.groupOprCode}
                      fieldOprCode={this.state.fieldOprCode}
                      viewType={VIEW_TYPE.VIEW}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="popup__footer">
              {isFilterInfoViewType ? (
                <button className="btn btn--white" onClick={closePopup}>
                  <FormattedMessage id="close">{text => text}</FormattedMessage>
                </button>
              ) : (
                <button className="btn btn--white" onClick={closePopup}>
                  <FormattedMessage id="cancel">
                    {text => text}
                  </FormattedMessage>
                </button>
              )}
              {!isFilterInfoViewType && (
                <button
                  className="btn btn--dark"
                  onClick={this.onClickConfirmBtn}
                >
                  <FormattedMessage id="confirm">
                    {text => text}
                  </FormattedMessage>
                </button>
              )}
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

const SelectFilterConnecter = connect(
  state => ({
    filterListData: state.filterMgmtList.get("filterList"),
    filterGroupData: state.filterMgmtList.get("filterGroup")
  }),
  dispatch => ({
    filterAction: bindActionCreators(filterAction, dispatch)
  })
)(SelectFilterPopup);

export default SelectFilterConnecter;
