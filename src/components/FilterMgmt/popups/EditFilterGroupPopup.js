import React, { Component } from "react";
import { connect } from "react-redux";
import Draggable from "react-draggable";

import "./style.scss";
import { Textbox } from "react-inputs-validation";
import { FormattedMessage } from "react-intl";

class EditFilterGroupPopup extends Component {
  state = {
    fGroupName: "",
    position: {
      x: 0,
      y: 0
    },
    validation: {
      validate: false,
      hasfGroupName: true
    }
  };

  componentDidMount() {
    const elWidth = document.getElementById("editFilterGroupPopup").offsetWidth;
    const elHeight = document.getElementById("editFilterGroupPopup")
      .offsetHeight;

    this.setState({
      fGroupName: this.props.target.name,
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  }

  onChangeText = (value, e) => {
    e.preventDefault();
    this.setState({
      fGroupName: value
    });
  };

  EditPopBody = () => {
    const { validate } = this.state.validation;

    return (
      <div className="popup__body">
        <div className="group-wrapper">
          <div className="form-group">
            <label>
              <FormattedMessage id="parent.filter.group">
                {text => text}
              </FormattedMessage>
            </label>
            <input
              type="text"
              className="form-control"
              value={this.props.target.filterGroupNmPath}
              readOnly
            />
          </div>
          <div className="form-group">
            <label>
              <FormattedMessage id="filter.group.name">
                {text => text}
              </FormattedMessage>
              <span className="font--red">*</span>
            </label>
            {/* <input type="text" className="form-control" autoFocus
            value={this.state.fGroupName} onChange={this.onChangeText}/> */}
            <Textbox
              value={this.state.fGroupName}
              onChange={this.onChangeText}
              onBlur={() => {}}
              validate={validate}
              validationCallback={res => {
                this.setState({
                  validation: {
                    hasfGroupName: res,
                    validate: false
                  }
                });
              }}
              validationOption={{
                check: true,
                required: true,
                min: 2,
                max: 50, // 100/2
                locale: this.props.language
              }}
              customStyleInput={{ width: "290px" }}
            />
          </div>
        </div>
      </div>
    );
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  // validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  // form validation
  isValidate = () => {
    // validate를 true로 설정 시 모든 form의 유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);

    const { hasfGroupName } = this.state.validation;

    return !hasfGroupName;
  };

  render() {
    const { closePopup, target, type } = this.props;
    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={this.state.position}
          onDrag={this.handleDrag}
        >
          <div className="popup" id="editFilterGroupPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <FormattedMessage id="filter.group">
                  {text => text}
                </FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>

            {this.EditPopBody()}

            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button
                className="btn btn--dark"
                onClick={() => {
                  if (this.isValidate()) {
                    closePopup();
                    target.saveHandle(target, type, this.state.fGroupName);
                  }
                }}
              >
                <FormattedMessage id="save"> {text => text} </FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({})
)(EditFilterGroupPopup);
