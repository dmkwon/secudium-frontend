import React, { Component } from "react";
import FilterCommonGrid from "../present/CommonGrid";
import Tree, { TreeNode } from "rc-tree";

import Draggable from "react-draggable";
import { FormattedMessage } from "react-intl";
import "./style.scss";

import {
  filterPopupType,
  filterConfirmPopupResource
} from "../const/popupResource";
import {
  SELECT_POPUP_TYPE,
  MAX_INFINITE_COUNT,
  MAX_SELECT_FIELD_OPR_EQ
} from "../const/filterConfig";

class CommonPopup extends Component {
  state = {
    keyword: "", // 검색어
    result: new Map(), // 오른쪽에 선택된 항목들 (Map형)
    selected: new Map(), // 체크 박스에 선택된 항목들 (Map형)

    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount() {
    this.onSelect(["all"]);

    const elWidth = document.getElementById("selectPopupForFilter").offsetWidth;
    const elHeight = document.getElementById("selectPopupForFilter")
      .offsetHeight;

    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  }

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  /** TREE 그룹 검색 **/
  onSelect = key => {
    const { Action } = this.props;
    const tree = key[0]; //rc-tree의 key는 배열형태로 전달됨
    // all 일 때
    if (tree === "all") {
      if (this.props.target === SELECT_POPUP_TYPE.fieldPopup) {
        Action.getFilterFields();
        Action.getFilterFieldTrees();
      } else if (
        this.props.target === SELECT_POPUP_TYPE.companyPopup ||
        this.props.target === SELECT_POPUP_TYPE.companySelectPopup
      ) {
        Action.getCompanys();
        Action.getCompanyTrees();
      } else if (this.props.target === SELECT_POPUP_TYPE.agentPopup) {
        Action.getAgents();
      } else if (this.props.target === SELECT_POPUP_TYPE.agentVendorPopup) {
        Action.getAgentVendors();
      } else if (this.props.target === SELECT_POPUP_TYPE.agentModelPopup) {
        Action.getAgentModels();
        Action.getAgentModelTrees();
      }
    }
    // all이 아닐때
    else if (tree !== undefined) {
      if (this.props.target === SELECT_POPUP_TYPE.fieldPopup) {
        Action.getFilterFields("fieldGroupcd=" + tree);
      } else if (
        this.props.target === SELECT_POPUP_TYPE.companyPopup ||
        this.props.target === SELECT_POPUP_TYPE.companySelectPopup
      ) {
        Action.getCompanys("companyGroupSeq=" + tree);
      } else if (this.props.target === SELECT_POPUP_TYPE.agentModelPopup) {
        Action.getAgentModels("agentVendorSeq=" + tree);
      }
    }
  };

  /** 그리드에서 사용하는 ckeckbox 상태 저장 **/
  onChangeCKBox = checkedItems => {
    this.setState({ selected: checkedItems });
  };

  /** 이동 버튼 클릭 이벤트 **/
  moveClick = () => {
    // 6. 회사 팝업을 제외한 팝업 select moveClick
    // 3. 자산 선택
    if (this.props.target === SELECT_POPUP_TYPE.agentPopup) {
      if (this.props.maxSelectCount === MAX_SELECT_FIELD_OPR_EQ) {
        this.state.result.clear();
        for (let [key, value] of this.state.selected) {
          this.state.result.set(key, value);
        }
      } else if (
        this.props.maxSelectCount === undefined ||
        this.props.maxSelectCount === MAX_INFINITE_COUNT
      ) {
        // 그리드 선택 + 선택된 항목 > maxCount 보다 많을 때 선택된 항목 reset
        if (
          this.state.selected.size + this.state.result.size >
          Number(this.props.maxSelectCount)
        ) {
          this.state.result.clear();
        }
        for (let [key, value] of this.state.selected) {
          let duplicate;
          if (!this.state.result.has(key)) {
            duplicate = false;
            // 자산 모델 명으로 중복 체크 (key는 seq)
            for (let [key2, value2] of this.state.result) {
              if (value2.agentModelName === value.agentModelName) {
                duplicate = true;
              }
            }
            if (duplicate === false) {
              this.state.result.set(key, value);
            }
          }
        }
      }
    } else {
      if (this.props.maxSelectCount === MAX_SELECT_FIELD_OPR_EQ) {
        this.state.result.clear();
        for (let [key, value] of this.state.selected) {
          this.state.result.set(key, value);
        }
      } else if (
        this.props.maxSelectCount === undefined ||
        this.props.maxSelectCount === MAX_INFINITE_COUNT
      ) {
        // 그리드 선택 + 선택된 항목 > maxCount 보다 많을 때 선택된 항목 reset
        if (
          this.state.selected.size + this.state.result.size >
          Number(this.props.maxSelectCount)
        ) {
          this.state.result.clear();
        }
        for (let [key, value] of this.state.selected) {
          if (!this.state.result.has(key)) {
            this.state.result.set(key, value);
          }
        }
      }
    }
    this.setState({
      selected: new Map()
    });
  };

  /** 선택된 항목에서 제거 **/
  removeClick = index => {
    if (this.props.target === SELECT_POPUP_TYPE.fieldPopup) {
      this.setState({
        result: this.state.result.filter(select => select.fieldSeq !== index)
      });
    } else if (
      this.props.target === SELECT_POPUP_TYPE.companyPopup ||
      this.props.target === SELECT_POPUP_TYPE.companySelectPopup
    ) {
      this.setState({
        result: this.state.result.filter(select => select.companySeq !== index)
      });
    } else if (this.props.target === SELECT_POPUP_TYPE.agentPopup) {
      this.setState({
        result: this.state.result.filter(select => select.agentSeq !== index)
      });
    } else if (this.props.target === SELECT_POPUP_TYPE.agentVendorPopup) {
      this.setState({
        result: this.state.result.filter(
          select => select.agentVendorSeq !== index
        )
      });
    } else if (this.props.target === SELECT_POPUP_TYPE.agentModelPopup) {
      this.setState({
        result: this.state.result.filter(
          select => select.agentModelSeq !== index
        )
      });
    }
  };

  /** 필터 */
  onChangeInput = e => this.setState({ [e.target.name]: e.target.value });
  /** 필터 enter 조회 */
  onKeydownInput = e => {
    if (e.keyCode === 13) {
      this.onClickSearch();
    }
  };

  /** 조회버튼 클릭 */
  onClickSearch = () => this.getFields();
  /** 필터 리셋버튼 클릭 */
  onClickReset = () => this.setState({ keyword: "" }, () => this.getFields());

  /** 검색 조회 */
  getFields = async () => {
    const { Action } = this.props;
    const { keyword } = this.state;

    if (this.props.target === SELECT_POPUP_TYPE.fieldPopup) {
      await Action.getFilterFields("field=" + keyword);
    } else if (
      this.props.target === SELECT_POPUP_TYPE.companyPopup ||
      this.props.target === SELECT_POPUP_TYPE.companySelectPopup
    ) {
      await Action.getCompanys("companyName=" + keyword);
    } else if (this.props.target === SELECT_POPUP_TYPE.agentPopup) {
      await Action.getAgents("agentModelName=" + keyword);
    } else if (this.props.target === SELECT_POPUP_TYPE.agentVendorPopup) {
      await Action.getAgentVendors("agentVendorName=" + keyword);
    } else if (this.props.target === SELECT_POPUP_TYPE.agentModelPopup) {
      await Action.getAgentModels("agentModelName=" + keyword);
    }
  };

  confirm = () => {
    if (this.state.result.size === 0) {
      if (this.props.target === SELECT_POPUP_TYPE.fieldPopup) {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.noneSeletedField,
            null,
            this.props.closeConfirmPopup
          )
        );
      } else if (this.props.target === SELECT_POPUP_TYPE.companyPopup) {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.noneSeletedCompany,
            null,
            this.props.closeConfirmPopup
          )
        );
      } else if (this.props.target === SELECT_POPUP_TYPE.agentPopup) {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.noneSeletedAgent,
            null,
            this.props.closeConfirmPopup
          )
        );
      } else if (this.props.target === SELECT_POPUP_TYPE.agentVendorPopup) {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.noneSeletedAgentVendor,
            null,
            this.props.closeConfirmPopup
          )
        );
      } else if (this.props.target === SELECT_POPUP_TYPE.agentModelPopup) {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.noneSeletedAgentModel,
            null,
            this.props.closeConfirmPopup
          )
        );
      } else if (this.props.target === SELECT_POPUP_TYPE.companySelectPopup) {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.noneSeletedSelectCompany,
            null,
            this.props.closeConfirmPopup
          )
        );
      }
    } else {
      this.props.resultData(this.state.result, this.props.target);
      this.props.closePopup();
    }
  };

  treeExist = (popupNum, treeData) => {
    const { onSelect } = this;

    // 1.필드선택
    if (popupNum === SELECT_POPUP_TYPE.fieldPopup) {
      return (
        <div className="component filter-tree">
          <div className="filter-tree__wrap">
            <div className="component__title" />
            <div className="component__box">
              <Tree
                showLine
                selectable={true}
                onSelect={onSelect}
                defaultExpandAll
                defaultSelectedKeys={["all"]}
              >
                <TreeNode
                  title={
                    <FormattedMessage id="all">{text => text}</FormattedMessage>
                  }
                  key="all"
                >
                  {Array.from(treeData.values()).map(value => {
                    return (
                      <TreeNode
                        title={value.fieldGroupName}
                        key={value.fieldGroupCode}
                      />
                    );
                  })}
                </TreeNode>
              </Tree>
            </div>
          </div>
        </div>
      );
    }
    // 2.고객사선택, 6.회사선택
    else if (
      popupNum === SELECT_POPUP_TYPE.companyPopup ||
      popupNum === SELECT_POPUP_TYPE.companySelectPopup
    ) {
      return (
        <div className="component filter-tree">
          <div className="filter-tree__wrap">
            <div className="component__title" />
            <div className="component__box">
              <Tree
                showLine
                selectable={true}
                onSelect={onSelect}
                defaultExpandAll
                defaultSelectedKeys={["all"]}
              >
                <TreeNode
                  title={
                    <FormattedMessage id="all">{text => text}</FormattedMessage>
                  }
                  key="all"
                >
                  {Array.from(treeData.values()).map(value => {
                    return (
                      <TreeNode
                        title={value.custmGroupNm}
                        key={value.custmGroupSeq}
                      />
                    );
                  })}
                </TreeNode>
              </Tree>
            </div>
          </div>
        </div>
      );
    }
    // 3.자산선택, 4.장비벤더선택
    else if (
      popupNum === SELECT_POPUP_TYPE.agentPopup ||
      popupNum === SELECT_POPUP_TYPE.agentVendorPopup
    ) {
      return "";
    }
    // 5.장비모델
    else if (popupNum === SELECT_POPUP_TYPE.agentModelPopup) {
      return (
        <div className="component filter-tree">
          <div className="filter-tree__wrap">
            <div className="component__title" />
            <div className="component__box">
              <Tree
                showLine
                selectable={true}
                onSelect={onSelect}
                defaultExpandAll
                defaultSelectedKeys={["all"]}
              >
                <TreeNode
                  title={
                    <FormattedMessage id="all">{text => text}</FormattedMessage>
                  }
                  key="all"
                >
                  {Array.from(treeData.values()).map(value => {
                    return (
                      <TreeNode
                        title={value.agentVendorName}
                        key={value.agentVendorSeq}
                      />
                    );
                  })}
                </TreeNode>
              </Tree>
            </div>
          </div>
        </div>
      );
    }
  };

  filter = target => {
    const { onChangeInput, onKeydownInput } = this;
    const { keyword } = this.state;
    if (target === SELECT_POPUP_TYPE.fieldPopup) {
      return (
        <FormattedMessage id="filed.or.name">
          {placeholder => (
            <input
              type="text"
              className="form-control"
              name="keyword"
              placeholder={placeholder}
              value={keyword}
              onChange={onChangeInput}
              onKeyDown={onKeydownInput}
            />
          )}
        </FormattedMessage>
      );
    } else if (
      target === SELECT_POPUP_TYPE.companyPopup ||
      target === SELECT_POPUP_TYPE.companySelectPopup
    ) {
      return (
        <input
          type="text"
          className="form-control"
          name="keyword"
          placeholder="Company Name"
          value={keyword}
          onChange={onChangeInput}
          onKeyDown={onKeydownInput}
        />
      );
    } else if (target === SELECT_POPUP_TYPE.agentPopup) {
      return (
        <FormattedMessage id="cust.ip.name">
          {placeholder => (
            <input
              type="text"
              className="form-control"
              name="keyword"
              placeholder={placeholder}
              value={keyword}
              onChange={onChangeInput}
              onKeyDown={onKeydownInput}
            />
          )}
        </FormattedMessage>
      );
    } else if (target === SELECT_POPUP_TYPE.agentVendorPopup) {
      return (
        <FormattedMessage id="vendor.name">
          {placeholder => (
            <input
              type="text"
              className="form-control"
              name="keyword"
              placeholder={placeholder}
              value={keyword}
              onChange={onChangeInput}
              onKeyDown={onKeydownInput}
            />
          )}
        </FormattedMessage>
      );
    } else if (target === SELECT_POPUP_TYPE.agentModelPopup) {
      return (
        <FormattedMessage id="vendor.model.name">
          {placeholder => (
            <input
              type="text"
              className="form-control"
              name="keyword"
              placeholder={placeholder}
              value={keyword}
              onChange={onChangeInput}
              onKeyDown={onKeydownInput}
            />
          )}
        </FormattedMessage>
      );
    }
  };

  buttonExist = target => {
    const { moveClick } = this;
    if (target !== SELECT_POPUP_TYPE.companySelectPopup) {
      return (
        <button
          className="btn btn--blue btn-icon customer-wrapper__btn"
          onClick={moveClick}
        />
      );
    }
  };

  selectExist = target => {
    const { result } = this.state;
    if (target !== SELECT_POPUP_TYPE.companySelectPopup) {
      return (
        <div className="selected-el">
          <div className="component__title">
            <span>
              <FormattedMessage id="selected.item">
                {text => text}
              </FormattedMessage>
            </span>
          </div>
          <ul className="select-result">
            {Array.from(result.values()).map(value => {
              if (target === SELECT_POPUP_TYPE.fieldPopup) {
                return (
                  <li key={value.fieldSeq}>
                    {value.field}
                    <button
                      className="btn btn-icon small btn-delete"
                      onClick={() => this.removeClick(value.fieldSeq)}
                    />
                  </li>
                );
              } else if (
                target === SELECT_POPUP_TYPE.companyPopup ||
                target === SELECT_POPUP_TYPE.companySelectPopup
              ) {
                return (
                  <li key={value.companySeq}>
                    {value.companyName}
                    <button
                      className="btn btn-icon small btn-delete"
                      onClick={() => this.removeClick(value.companySeq)}
                    />
                  </li>
                );
              } else if (target === SELECT_POPUP_TYPE.agentPopup) {
                return (
                  <li key={value.agentSeq}>
                    {value.agentModelName}
                    <button
                      className="btn btn-icon small btn-delete"
                      onClick={() => this.removeClick(value.agentSeq)}
                    />
                  </li>
                );
              } else if (target === SELECT_POPUP_TYPE.agentVendorPopup) {
                return (
                  <li key={value.agentVendorSeq}>
                    {value.agentVendorName}
                    <button
                      className="btn btn-icon small btn-delete"
                      onClick={() => this.removeClick(value.agentVendorSeq)}
                    />
                  </li>
                );
              } else if (target === SELECT_POPUP_TYPE.agentModelPopup) {
                return (
                  <li key={value.agentModelSeq}>
                    {value.agentModelName}
                    <button
                      className="btn btn-icon small btn-delete"
                      onClick={() => this.removeClick(value.agentModelSeq)}
                    />
                  </li>
                );
              }
              return null;
            })}
          </ul>
        </div>
      );
    }
  };

  render() {
    const { onClickReset, onClickSearch, confirm } = this;
    const {
      closePopup,
      gridData,
      treeData,
      target,
      columns,
      maxSelectCount
    } = this.props;

    const getStrGridKey = obj => {
      if (target === SELECT_POPUP_TYPE.fieldPopup) {
        return `${obj.fieldSeq}`;
      } else if (
        target === SELECT_POPUP_TYPE.companyPopup ||
        target === SELECT_POPUP_TYPE.companySelectPopup
      ) {
        return `${obj.companySeq}`;
      } else if (target === SELECT_POPUP_TYPE.agentPopup) {
        return `${obj.agentSeq}`;
      } else if (target === SELECT_POPUP_TYPE.agentVendorPopup) {
        return `${obj.agentVendorSeq}`;
      } else if (target === SELECT_POPUP_TYPE.agentModelPopup) {
        return `${obj.agentModelSeq}`;
      }
      // return `${obj.no}`
    };

    const isDisableCheckBox = target => false;
    const checkSelected = target =>
      this.state.selected.has(getStrGridKey(target));
    const checkAllSelected = () => {
      if (target === SELECT_POPUP_TYPE.companySelectPopup) {
        return false;
      } else if (gridData.length === 0) {
        return false;
      } else {
        return gridData.length === this.state.selected.size;
      }
    };

    // 하나 체크인 경우와 all 체크인 경우를 구분해서 처리
    const onClickCkeckBox = (isAll, data = null) => {
      let items;
      // 6. 회사 선택
      if (this.props.target === SELECT_POPUP_TYPE.companySelectPopup) {
        if (!isAll) {
          if (this.state.selected.size === 0) {
            items = new Map(this.state.selected);
            let key = getStrGridKey(data);
            items.set(key, data);
            this.onChangeCKBox(items);
            this.setState({
              result: items
            });
          } else if (this.state.selected.size === 1) {
            items = new Map(this.state.selected);
            let key = getStrGridKey(data);
            if (items.has(key)) {
              items.delete(key);
              this.onChangeCKBox(items);
              this.setState({
                result: new Map()
              });
            } else {
              items = new Map();
              items.set(key, data);
              this.onChangeCKBox(items);
              this.setState({
                result: items
              });
            }
          }
        }
      } else {
        if (maxSelectCount === MAX_SELECT_FIELD_OPR_EQ) {
          if (!isAll) {
            if (this.state.selected.size === 0) {
              items = new Map(this.state.selected);
              let key = getStrGridKey(data);
              items.set(key, data);
              this.onChangeCKBox(items);
            } else if (this.state.selected.size === MAX_SELECT_FIELD_OPR_EQ) {
              items = new Map(this.state.selected);
              let key = getStrGridKey(data);
              if (items.has(key)) {
                items.delete(key);
                this.onChangeCKBox(items);
              } else {
                items = new Map();
                items.set(key, data);
                this.onChangeCKBox(items);
              }
            }
          }
        } else if (
          maxSelectCount === undefined ||
          maxSelectCount === MAX_INFINITE_COUNT
        ) {
          let size;
          if (isAll) {
            items = new Map();
            if (checkAllSelected() === false) {
              gridData.map(item => items.set(getStrGridKey(item), item));
            }
            size = items.size;
          } else {
            items = new Map(this.state.selected);
            let key = getStrGridKey(data);
            items.has(key) ? items.delete(key) : items.set(key, data);
            size = items.size;
          }

          // 그리드 클릭시 maxCount보다 많으면 팝업
          if (Number(maxSelectCount) < size) {
            this.props.openConfirmPopup(
              filterConfirmPopupResource(
                filterPopupType.maxSelectCount,
                null,
                this.props.closeConfirmPopup,
                maxSelectCount
              )
            );
          } else {
            this.onChangeCKBox(items);
          }
        }
      }
    };

    const SELECT_POPUP_TITLE = {
      fieldPopup: (
        <FormattedMessage id="field.select"> {text => text} </FormattedMessage>
      ),
      companyPopup: (
        <FormattedMessage id="cust.select"> {text => text} </FormattedMessage>
      ),
      agentPopup: (
        <FormattedMessage id="asset.select"> {text => text} </FormattedMessage>
      ),
      agentVendorPopup: (
        <FormattedMessage id="equip.vendor.select">
          {text => text}
        </FormattedMessage>
      ),
      agentModelPopup: (
        <FormattedMessage id="equip.model.select">
          {text => text}
        </FormattedMessage>
      ),
      companySelectPopup: (
        <FormattedMessage id="select.company">{text => text}</FormattedMessage>
      )
    };

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={this.state.position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--dark popup--select"
            id="selectPopupForFilter"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>{SELECT_POPUP_TITLE[target]}</h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <div className="flex-box">
                {/* tree or not tree function*/}
                {this.treeExist(target, treeData)}
                <div className="asset-table">
                  <div className="search-bar">
                    {this.filter(target)}

                    <div className="binder" />
                    <button
                      className="btn btn-icon small btn--go"
                      onClick={onClickSearch}
                    />
                    <button
                      className="btn btn-icon small btn--filter"
                      onClick={onClickReset}
                    />
                  </div>
                  <FilterCommonGrid
                    grideColumns={columns}
                    gridData={gridData}
                    // sortable

                    hasNo={true}
                    hasCheckBox={true}
                    isDisableCheckBox={isDisableCheckBox}
                    seletedItems={this.state.selected} // Map 자료형임, readOnly
                    checkSelected={checkSelected}
                    checkAllSelected={checkAllSelected}
                    onClickCkeckBox={onClickCkeckBox}
                  />
                </div>
                {this.buttonExist(target)}
                {this.selectExist(target)}
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--dark" onClick={confirm}>
                <FormattedMessage id="confirm">{text => text}</FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default CommonPopup;
