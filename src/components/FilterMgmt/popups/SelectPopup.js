import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as filterPopupActions from "../../../store/modules/filterPopup";

import CommonPopup from "./SelectCommonPopup";
import { SELECT_POPUP_TYPE, isDebug } from "../const/filterConfig";
import IntlMessages from "util/IntlMessages";

class SelectPopup extends Component {
  columns = {
    fieldPopup: [
      {
        Header: (
          <IntlMessages id="filter"/>
        ),
        accessor: "field",
        width: 100
      },
      {
        Header: (
          <IntlMessages id="filter.desc"/>
        ),
        accessor: "fieldDesc",
        width: 150
      },
      {
        Header: (
          <IntlMessages id="filter.group"/>
        ),
        accessor: "fieldGroupcdString"
      }
    ],
    companyPopup: [{ Header: "Company Name", accessor: "companyName" }],
    agentPopup: [
      {
        Header: (
          <IntlMessages id="channel.cust"/>
        ),
        accessor: "agentHosts",
        style: { textAlign: "center" },
        width: 150
      },
      {
        Header: (
          <IntlMessages id="equip.ip"/>
        ),
        accessor: "agentIp",
        width: 100
      },
      {
        Header: (
          <IntlMessages id="equip.name"/>
        ),
        accessor: "agentModelName"
      }
    ],
    agentVendorPopup: [
      {
        Header: (
          <IntlMessages id="vendor.name"/>
        ),
        accessor: "agentVendorName"
      }
    ],
    agentModelPopup: [
      {
        Header: (
          <IntlMessages id="vendor.name"/>
        ),
        accessor: "agentVendorName",
        style: { textAlign: "center" },
        width: 150
      },
      {
        Header: (
          <IntlMessages id="model.name"/>
        ),
        accessor: "agentModelName"
      }
    ],
    companySelectPopup: [{ Header: "Company Name", accessor: "companyName" }]
  };

  render() {
    const { type, closePopup, selectPopupCB } = this.props;

    isDebug && console.log(type);

    switch (type) {
      case "fieldPopup":
        return (
          <CommonPopup
            target={SELECT_POPUP_TYPE[type]} // 팝업 화면 번호 (ex 1번 필드)
            columns={this.columns[type]} // 팝업 그리드 필드 (ex 1번 필드)
            treeData={this.props.fieldTreeInfo} // 왼쪽 트리 데이터
            gridData={this.props.fieldInfo} // 그리드 데이터
            Action={this.props.FilterPopupActions}
            openConfirmPopup={this.props.openConfirmPopup}
            closeConfirmPopup={this.props.closeConfirmPopup}
            resultData={selectPopupCB} // 팝업 종료시 메인화면에 저장되는 함수
            closePopup={closePopup}
          />
        );

      case "companyPopup":
        return (
          <CommonPopup
            target={SELECT_POPUP_TYPE[type]}
            columns={this.columns[type]}
            treeData={this.props.companyTreeInfo}
            gridData={this.props.companyInfo}
            Action={this.props.FilterPopupActions}
            openConfirmPopup={this.props.openConfirmPopup}
            closeConfirmPopup={this.props.closeConfirmPopup}
            resultData={selectPopupCB}
            closePopup={closePopup}
            maxSelectCount={this.props.maxSelectCount}
          />
        );

      case "agentPopup":
        return (
          <CommonPopup
            target={SELECT_POPUP_TYPE[type]}
            columns={this.columns[type]}
            gridData={this.props.agentInfo}
            Action={this.props.FilterPopupActions}
            openConfirmPopup={this.props.openConfirmPopup}
            closeConfirmPopup={this.props.closeConfirmPopup}
            resultData={selectPopupCB}
            closePopup={closePopup}
            maxSelectCount={this.props.maxSelectCount}
          />
        );

      case "agentVendorPopup":
        return (
          <CommonPopup
            target={SELECT_POPUP_TYPE[type]}
            columns={this.columns[type]}
            gridData={this.props.agentVendorInfo}
            Action={this.props.FilterPopupActions}
            openConfirmPopup={this.props.openConfirmPopup}
            closeConfirmPopup={this.props.closeConfirmPopup}
            resultData={selectPopupCB}
            closePopup={closePopup}
            maxSelectCount={this.props.maxSelectCount}
          />
        );

      case "agentModelPopup":
        return (
          <CommonPopup
            target={SELECT_POPUP_TYPE[type]}
            columns={this.columns[type]}
            treeData={this.props.agentModelTreeInfo}
            gridData={this.props.agentModelInfo}
            Action={this.props.FilterPopupActions}
            openConfirmPopup={this.props.openConfirmPopup}
            closeConfirmPopup={this.props.closeConfirmPopup}
            resultData={selectPopupCB}
            closePopup={closePopup}
            maxSelectCount={this.props.maxSelectCount}
          />
        );

      case "companySelectPopup":
        return (
          <CommonPopup
            target={SELECT_POPUP_TYPE[type]}
            columns={this.columns[type]}
            treeData={this.props.companyTreeInfo}
            gridData={this.props.companyInfo}
            Action={this.props.FilterPopupActions}
            openConfirmPopup={this.props.openConfirmPopup}
            closeConfirmPopup={this.props.closeConfirmPopup}
            resultData={selectPopupCB}
            closePopup={closePopup}
            maxSelectCount={this.props.maxSelectCount}
          />
        );

      default:
        return;
    }
  }
}

export default connect(
  state => ({
    fieldInfo: state.filterPopup.get("fields"),
    fieldTreeInfo: state.filterPopup.get("fieldTrees"),
    companyInfo: state.filterPopup.get("companys"),
    companyTreeInfo: state.filterPopup.get("companyTrees"),
    agentInfo: state.filterPopup.get("agents"),
    agentVendorInfo: state.filterPopup.get("agentVendors"),
    agentModelInfo: state.filterPopup.get("agentModels"),
    agentModelTreeInfo: state.filterPopup.get("agentModelTrees")
  }),
  dispatch => ({
    FilterPopupActions: bindActionCreators(filterPopupActions, dispatch)
  })
)(SelectPopup);
