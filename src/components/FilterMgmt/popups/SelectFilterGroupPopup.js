import React, { Component } from "react";
import CommonContextMenuTree from "../present/CommonTree";
import Draggable from "react-draggable";

import {
  isDebug,
  FIXED_FILTER_GROUP,
  FILTER_GROUP_TYPE
} from "../const/filterConfig";
import "./style.scss";
import { find1DepthGroup } from "../const/utils";
import { FormattedMessage } from "react-intl";

class SelectFilterGroupPopup extends Component {
  state = {
    selectedTarget: null,
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount() {
    const elWidth = document.getElementById("selectFieldGroupPopup")
      .offsetWidth;
    const elHeight = document.getElementById("selectFieldGroupPopup")
      .offsetHeight;

    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  }

  getTreeNodeTitle = target => target.name;
  getTreeNodeKey = target => `${target.filterGroupSeq}-${target.name}`;

  onSelect = (selectedKeys, event) => {
    isDebug && console.log("selected", selectedKeys, event);
    let selectedDatas = event.selected
      ? event.selectedNodes[0].props.targetData
      : null;
    this.setState({
      selectedTarget: selectedDatas
    });
  };

  onClickConfim = () => {
    this.props.closePopup();
    this.props.seletcFilterGroupCB(this.state.selectedTarget);
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { closePopup, filterGroupData } = this.props;

    const selectFilterGroupData = () => {
      let fgData = [];
      filterGroupData.sub.map(subFG => {
        if (subFG.status !== FILTER_GROUP_TYPE.TEMP) {
          fgData.push(subFG);
        }
        return null;
      });
      isDebug && console.log("selectFilterGroupData", fgData);
      return fgData;
    };

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={this.state.position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--dark" id="selectFieldGroupPopup">
            <div className="popup__header">
              <h5>
                <FormattedMessage id="filter.group.select">
                  {text => text}
                </FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <div className="filter-tree__wrap">
                <CommonContextMenuTree
                  treeData={selectFilterGroupData()}
                  getTreeNodeTitle={this.getTreeNodeTitle}
                  getTreeNodeKey={this.getTreeNodeKey}
                  hasContextMenu={false}
                  getContextMenu={null}
                  onSelect={this.onSelect}
                  isDebug={isDebug}
                  firstkey={find1DepthGroup(
                    filterGroupData,
                    FIXED_FILTER_GROUP.UNDEFINED
                  )}
                />
              </div>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              {this.state.selectedTarget ? (
                <button className="btn btn--dark" onClick={this.onClickConfim}>
                  <FormattedMessage id="confirm">
                    {text => text}
                  </FormattedMessage>
                </button>
              ) : (
                <button className="btn btn--dark" disabled>
                  <FormattedMessage id="confirm">
                    {text => text}
                  </FormattedMessage>
                </button>
              )}
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default SelectFilterGroupPopup;
