import React, { Component } from 'react'

import Navbar from '../Common/Navbar'
import LeftMenu from '../Common/LeftMenu'
import Loading from '../Common/Loading'

import FilterMgmtList from './FilterMgmtList'
import FilterMgmtDetail from './FilterMgmtDetail'

// 팝업 
import EditFilterGroupPopup from './popups/EditFilterGroupPopup' // 필터 그룹 수정 팝업 
import SelectFilterGroupPopup from './popups/SelectFilterGroupPopup' // 필터그룹 선택 팝업

import SelectFilterPopup from './popups/SelectFilterPopup' // 필터 선택

// 공통으로 사용하는 confirm 팝업
import ConfirmPopup from '../Common/Popups/CommonConfirmPopup'


// 필터 정보, SelectFilterPopup 하나로 처리 
//import FilterInfoPopup from './popups/FilterInfoPopup'

// field, company, agent, vender model 선택 팝업 공통으로 하나로 처리
import SelectPopup from './popups/SelectPopup'


import './style.scss'
import { isDebug, MAX_INFINITE_COUNT } from './const/filterConfig';
import { filterConfirmPopupResource, filterPopupType } from './const/popupResource';

class FilterMgmtComponent extends Component {
  state = {
    // 스위칭되는 target page 정보 
    isShowTargetPage: 'list',    // list: 필터 목록 / detail: 필터 상세/수정/등록
    detailPageType: 'view',      // 'view', 'new', 'edit', 'import' - filterConfig.js > VIEW_TYPE 정의
    selectedFilter: null,        // detail로 전달된 필터 정보 

    // ConfirmPopup: 삭제 포함한 모든 confirm 팝업을 위한 (commonConfirmPopup 사용)
    isShowConfirmPopup: false,
    confirmPopupResource: null,  // 팝업을 구성하는 리소스
    
    // EditFilterGroupPopup: 필터 그룹 - 추가/변경을 위한
    isShowEditFilterGroupPopup: false, // 필터 그룹: 추가, 수정 팝업
    editFilterGroupType: 'new',  // edit 팝업 type, 생성 또는 수정 - new or edit
    selectedFilterGroup: null,

    // SelectFilterGroupPopup: 멀티 필터 선택 후 변경을 위한
    isShowSelectFilterGroup: false,
    filterGroupData: null,
    seletcFilterGroupCB: null, 
    
    isShowSelectFilter: false,
    isShowFilterInfo: false,
    seletcFilterCB: null,

    // field, company, agent, vender model 선택 팝업 공통으로 하나로 처리
    isShowSelectCommonPopup: false,
    selectPopupType: null,
    selectPopupCB: null,
    maxSelectCount: MAX_INFINITE_COUNT,
    
    // 백단에서 데이터를 가져오는 경우, 로딩은 기본으로 띄움
    isLoading: false
  }

  goToTargetPage = (target, type = null, filter = null) => {
    this.closePopup();

    this.setState({
      isShowTargetPage: target,
      detailPageType: type,
      selectedFilter: filter
    });
  }

   // 필터 그룹 - 생성, 수정 팝업 open 
  openEditFilterGroupPopup = (filterGroup, type) => {
    this.setState({
      isShowEditFilterGroupPopup: true,
      editFilterGroupType: type,
      selectedFilterGroup: filterGroup
    })
  }

 // 삭제 포함해, one/two 버튼의 confirm 팝업 하나로 처리
 openConfirmPopup = (resource) => {
    this.setState({
      isShowConfirmPopup: true,
      confirmPopupResource: resource,
    })
  }

  openFilterInfoPopup = (target) => {
    isDebug && console.log("openFilterInfoPopup", target);
    this.setState({
      isShowFilterInfo: true,
      selectedFilter: target
    })
  }

  openSelectFilterPopup = (seletcFilterCB) => {
    this.setState({
      isShowSelectFilter: true,
      seletcFilterCB: seletcFilterCB
    })
  }

  openSelectPopup = (popuptype, returnCBFunc, maxSelectCount = MAX_INFINITE_COUNT) => {
    isDebug && console.log("openSelectPopup:", popuptype, maxSelectCount)
    this.setState({
      isShowSelectCommonPopup: true,
      selectPopupType: popuptype,
      selectPopupCB: returnCBFunc,
      maxSelectCount: maxSelectCount
    })
  }

  // 필터 그룹 트리 팝업 - 필터 그룹 중 하나를 선탤할 수 있음
  openSelectFilterGroupPopup = (filterGroupData, seletcFilterGroupCB ) => {
    isDebug && console.log("openSelectFilterGroupPopup", filterGroupData)
    this.setState({
      isShowSelectFilterGroup: true,
      filterGroupData: filterGroupData,
      seletcFilterGroupCB: seletcFilterGroupCB
    })
  }

  // loading 
  setLoading = (isLoading) => {
    this.setState({
      isLoading: isLoading
    })
  }

  /* confirm 팝업만 close */
  closeConfirmPopup = () => {
    this.setState({
      isShowConfirmPopup: false,
    })
  }

  /* ALL 팝업 close */
  closePopup = () => {
    this.setState({
      isShowEditFilterGroupPopup: false,
      isShowSelectFilterGroup: false,
      
      isShowFilterInfo: false,
      isShowSelectFilter: false,
      
      isShowConfirmPopup: false,
      isShowSelectCommonPopup: false
    })
  }

  errorNotiPopup = (error) => {
    let resource = 
        filterConfirmPopupResource(filterPopupType.networkError, 
        null, this.closeConfirmPopup, error.message)

    this.openConfirmPopup(resource);
  }

  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper filter-mgmt">
          <LeftMenu />

          {this.state.isShowTargetPage === 'list' && (
            <FilterMgmtList
              openEditFilterGroupPopup={this.openEditFilterGroupPopup}
              openSelectFilterGroupPopup={this.openSelectFilterGroupPopup}
              openConfirmPopup={this.openConfirmPopup}
              errorNotiPopup={this.errorNotiPopup}
              closePopup={this.closePopup}
              goToTargetPage={this.goToTargetPage}
              setLoading={this.setLoading}
            />
          )}
          {this.state.isShowTargetPage === 'detail' && (
            <FilterMgmtDetail
              openSelectFilterGroupPopup={this.openSelectFilterGroupPopup}
              openSelectFilterPopup={this.openSelectFilterPopup}
              openSelectPopup={this.openSelectPopup}
              openFilterInfoPopup={this.openFilterInfoPopup}
              errorNotiPopup={this.errorNotiPopup}
              goToTargetPage={this.goToTargetPage}
              detailPageType={this.state.detailPageType}
              selectedFilter={this.state.selectedFilter}
              setLoading={this.setLoading}
              closePopup={this.closePopup}
              openConfirmPopup={this.openConfirmPopup}
            />
          )}
          {this.state.isShowEditFilterGroupPopup && (
            <EditFilterGroupPopup
              closePopup={this.closePopup}
              target={this.state.selectedFilterGroup}
              type={this.state.editFilterGroupType}
            />
          )}
          {this.state.isShowSelectFilterGroup && (
            <SelectFilterGroupPopup 
              openConfirmPopup={this.openConfirmPopup}
              closePopup={this.closePopup}
              filterGroupData={this.state.filterGroupData}
              seletcFilterGroupCB={this.state.seletcFilterGroupCB}
            />
          )}
          {this.state.isShowSelectFilter && (
            <SelectFilterPopup
              openConfirmPopup={this.openConfirmPopup}
              closeConfirmPopup={this.closeConfirmPopup}
              closePopup={this.closePopup}
              isFilterInfoViewType={false}
              seletcFilterCB={this.state.seletcFilterCB}
              setLoading={this.setLoading}
            />
          )}
          {this.state.isShowFilterInfo && (
            // <FilterInfoPopup closePopup={this.closePopup} />
            <SelectFilterPopup 
              closePopup={this.closePopup}
              isFilterInfoViewType={true}
              target={this.state.selectedFilter}
            />
          )}

          {/* field, company, agent, vender model 선택 팝업 공통으로 하나로 처리 */}
          {this.state.isShowSelectCommonPopup && (
            <SelectPopup 
                openConfirmPopup={this.openConfirmPopup}
                closeConfirmPopup={this.closeConfirmPopup}
                closePopup={this.closePopup}
                type={this.state.selectPopupType}
                selectPopupCB={this.state.selectPopupCB}

                maxSelectCount={this.state.maxSelectCount}
            />
          )}

          {/* common confrim popup */}
          {this.state.isShowConfirmPopup && (
            <ConfirmPopup
              resource={this.state.confirmPopupResource}/>
          )}
          {/* 로딩 컴포넌트 */}
          {this.state.isLoading && (
            <Loading />
          )}
        </div>
      </React.Fragment>
    )
  }
}

export default FilterMgmtComponent
