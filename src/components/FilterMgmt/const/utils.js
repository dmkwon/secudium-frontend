import {
  isDebug,
  allowedFilterImportExtensions,
  allowedFilterIconExtensions,
  FILTER_GROUP_TYPE,
  pngHeader
} from "./filterConfig";

// 여러 파일에서 사용되거나 여러 곳에서 사용되는데 코드 길이을 길게 만드는 것은 명시적인 명칭으로 함수화 함.

// array의 경우 length 계산, 아닌 경우는 0
// Array.isArray(null), Array.isArray(undefined) => false
const getArrayLen = list => (Array.isArray(list) ? list.length : 0);

// 리스트에 포함된 필터 개수만 카운팅
const getFilterLen = list => {
  return Array.isArray(list)
    ? list.reduce(
        (accumulator, currentValue) =>
          isFilter(currentValue) ? accumulator + 1 : accumulator,
        0
      )
    : 0;
};

// 필터와 필터 그룹 구분
const isFilter = obj =>
  obj != null && (obj.type === "F" || obj.filter_hist_seq);
const isFilterGroup = obj => obj != null && obj.type === "G";

// 적용 기간 문구 생성
const getStrFilterPeriod = obj =>
  isFilter(obj)
    ? obj.status === FILTER_GROUP_TYPE.RULE_EVENT
      ? obj.filterCommitPeriodYn === "Y"
        ? `${obj.filterCommitPeriodStrDay} ~ ${obj.filterCommitPeriodEndDay}`
        : "영구" // dmkwon
      : "-"
    : obj.periodInfo;

const getStrFilterGridKey = obj =>
  isFilter(obj)
    ? `${obj.type}-${obj.filterSeq}`
    : `${obj.type}-${obj.filterGroupSeq}`;

// date 형 변환
const convertStrToDate = strDate => new Date(strDate);
const convertDateToStr = date => date.toISOString().substring(0, 10);

// depth 값을 기준으로, 자녀 노드를 찾아서, 리스트(array형)을 받아서 트리형(subTree array 형을 가지는) 변환함.
const convertListToTree = (list, tree, addedAction = null, startDepth = 1) => {
  let _subTreeMap = new Map();
  const key = depth => `${depth}depth`;
  tree.sub = [];

  isDebug && console.log("convertListToTree in:", tree, list);

  list.map(item => {
    // 다음 작업을 위해서 depth 별로 서브 목록을 생성해서 빠르게 접근할 수 있도록 함
    if (!item.sub) item.sub = [];
    if (!_subTreeMap.get(key(item.depth))) _subTreeMap.set(key(item.depth), []);
    _subTreeMap.get(key(item.depth)).push(item);

    //isDebug && console.log("convertListToTree 00: ", _subTreeMap, tree, item);
    if (item.depth === startDepth) {
      // 필요한 추가 정보를 포함 시킴
      if (addedAction != null) item = addedAction(item);
      tree.sub.push(item);
    } else {
      let _subTree = _subTreeMap.get(key(item.depth - 1));
      //isDebug && console.log("convertListToTree 01: ", _subTree, tree, item);
      _subTree.map(parent => {
        //isDebug && console.log("convertListToTree 01: ", _subTreeMap, tree, item);
        if (
          (parent.filterGroupSeq &&
            parent.filterGroupSeq === item.topFilterGroupSeq) ||
          (parent.filterCndtnSeq &&
            parent.filterCndtnSeq === item.topFilterCndtnSeq)
        ) {
          // 필요한 추가 정보를 포함 시킴
          if (addedAction != null) item = addedAction(item, parent);
          parent.sub.push(item);
        }
      });
    }
  });

  isDebug && console.log("convertListToTree out:", _subTreeMap, tree, list);
  return { list, tree };
};

// changeCndtnData
// changeSelectCndtnRow

const getMapsValueArray = map => {
  let keys = [...map.keys()];
  let varray = [];

  //isDebug && console.log(map, keys);

  keys &&
    keys.map((k, i) => {
      let temp = map.get(k); // filter info
      varray.push(temp);
      return null;
    });

  return varray;
};

const find1DepthGroup = (filterGroupData, filterGroupSeq) => {
  let group;
  filterGroupData.sub.map(item => {
    if (item.filterGroupSeq === filterGroupSeq) {
      group = item;
    }
  });
  return group;
};

const checkJsonFile = fileNm =>
  allowedFilterImportExtensions.exec(fileNm) ? true : false;
const checkPngIcon = ImgNm =>
  allowedFilterIconExtensions.exec(ImgNm) ? true : false;

const getPngIcon = filter => `${pngHeader}${filter.filterIconImg}`;

const getStrFileSize = size => {
  if (size < 1024) {
    return size + "bytes";
  } else if (size > 1024 && size < 1048576) {
    return (size / 1024).toFixed(1) + "KB";
  } else if (size > 1048576) {
    return (size / 1048576).toFixed(1) + "MB";
  }
};

export {
  isFilter,
  isFilterGroup,
  getArrayLen,
  getFilterLen,
  getStrFilterPeriod,
  getStrFilterGridKey,
  getMapsValueArray,
  convertStrToDate,
  convertDateToStr,
  convertListToTree,
  checkJsonFile,
  checkPngIcon,
  getStrFileSize,
  getPngIcon,
  find1DepthGroup
};
