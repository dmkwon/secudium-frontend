import { convertDateToStr } from './utils';

// 파일명 생성하는 함수
const makeJsonFileName = prefix =>
  `${prefix}_${convertDateToStr(new Date())}.json`;

// JS Object > JSON으로 생성해서 down까지 되는 함수
const exportObjectToJson = (jsObj, prefixOfFileName = 'export') => {
  let exportData = JSON.stringify(jsObj, undefined, 2);
  let exportURIData = 'data:application/json;charset=utf-8,';
  exportURIData = exportURIData + encodeURIComponent(exportData); // URI로 데이터를 전달하기 위해서 문자열을 인코딩

  var downloadAnchorNode = document.createElement('a');
  downloadAnchorNode.setAttribute('href', exportURIData);
  downloadAnchorNode.setAttribute(
    'download',
    makeJsonFileName(prefixOfFileName)
  );
  document.body.appendChild(downloadAnchorNode); // required for firefox
  downloadAnchorNode.click();

  downloadAnchorNode.remove();
};

const importJsonFileToObjec = () => {
  // 구현 확인 중
};

const convertJsObjectToJsonbStr = (jsObj, replacer = undefined, space = 2) => {
  let jsonStr = JSON.stringify(jsObj, replacer, 0);
  return jsonStr;
};

const convertJsonbStrtoJsObj = jsonStr => {
  let jsObj = JSON.parse(jsonStr);
  return jsObj;
};

export {
  exportObjectToJson,
  importJsonFileToObjec,
  convertJsObjectToJsonbStr,
  convertJsonbStrtoJsObj,
};
