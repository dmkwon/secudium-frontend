const isDebug = false;

const MAX_FILTER_GROUP = 3;
const FGTREE_FIXED_DEPTH = 1;

const allowedFilterImportExtensions = /(\.json|\.Json|\.JSON)$/i;
const allowedFilterIconExtensions = /(\.png|\.Png|\.PNG)$/i;

const jsonMineType = "application/json";
const pngMineType = "image/png";

const pngHeader = "data:image/png;base64,";

const pngIconStyle = { width: 20, height: 20, verticalAlign: "middle" };

const FIXED_FILTER_GROUP = {
  ALL: 0,
  UNDEFINED: 1,
  TEMP: 2,
  SECURITY_EVENT: 3,
  RULE_EVENT: 4
};

const FILTER_GROUP_TYPE = {
  UNDEFINED: "W",
  TEMP: "D",
  SECURITY_EVENT: "S",
  RULE_EVENT: "R"
};

const PAGE_TYPE = {
  LIST: "list",
  DETAIL: "detail"
};

const DETAIL_TAB_TYPE = {
  INFO: "info",
  HIST: "history"
};

const VIEW_TYPE = {
  VIEW: "view",
  NEW: "new",
  EDIT: "edit",
  IMPORT: "import"
};

const GROUP_OP_TYPE = {
  AND: "AND",
  NOT: "NOT",
  OR: "OR"
};

const SELECT_POPUP_TYPE = {
  fieldPopup: "fieldPopup",
  companyPopup: "companyPopup",
  agentPopup: "agentPopup",
  agentVendorPopup: "agentVendorPopup",
  agentModelPopup: "agentModelPopup",
  companySelectPopup: "companySelectPopup"
};

// 아래 키 값에 해당되는 경우는 팝업에 의해 선택할 수 있음

const FIELD_TYPE = {
  COMPANY_NM: "companynm",
  AGENT_IP: "agentip",
  HOST_NM: "hostname",
  VENDOR: "vendor",
  DEVICE_MODEL: "devicemodel"
};

const SELECT_POPUP_KEY_FIELD_TYPE = {
  companynm: "companyPopup",
  agentip: "agentPopup",
  hostname: "agentPopup",
  vendor: "agentVendorPopup",
  devicemodel: "agentModelPopup"
};

const MAX_SELECT_KEY_FIELD = 2; // key 필드는 최대 2개까지만 선택 가능(설계서상)

const MAX_INFINITE_COUNT = 9999; // 선택 개수에 제한 없음.

const FIXED_OPR_EQ = "==";
const FIXED_OPR_EQ_VALUE_DELIMIT = ","; // ==의 경우 값이 하나만 입력되어 한다는 제약조건이 추가됨. 구분자를 변경할 수 있도로 상수로 정의함
const MAX_SELECT_FIELD_OPR_EQ = 1;

export {
  isDebug,
  MAX_FILTER_GROUP,
  FILTER_GROUP_TYPE,
  FGTREE_FIXED_DEPTH,
  FIXED_FILTER_GROUP,
  VIEW_TYPE,
  DETAIL_TAB_TYPE,
  PAGE_TYPE,
  GROUP_OP_TYPE,
  SELECT_POPUP_TYPE,
  SELECT_POPUP_KEY_FIELD_TYPE,
  FIELD_TYPE,
  allowedFilterImportExtensions,
  jsonMineType,
  allowedFilterIconExtensions,
  pngMineType,
  pngHeader,
  pngIconStyle,
  MAX_SELECT_KEY_FIELD,
  MAX_INFINITE_COUNT,
  FIXED_OPR_EQ,
  MAX_SELECT_FIELD_OPR_EQ,
  FIXED_OPR_EQ_VALUE_DELIMIT
};
