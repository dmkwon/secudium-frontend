import React from "react";
import { isDebug } from "./filterConfig";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

const ONE_BTN_CONFIRM = confirmCB => [
  {
    name: <IntlMessages id="confirm" />,
    CB: confirmCB
  }
];

const TWO_BTN_CONFIRM = (cancelCB, confirmCB) => [
  {
    name: <IntlMessages id="cancel" />,
    CB: cancelCB
  },
  {
    name: <IntlMessages id="confirm" />,
    CB: confirmCB
  }
];

const filterPopupType = {
  delFilter: "delFilter",
  editFilter: "editFilter",
  deletedFilter: "deletedFilter",
  noneSeletedFilter: "noneSeletedFilter",
  cantDeleteFilterWithRule: "cantDeleteFilterWithRule",
  saveFilter: "saveFilter",
  doneSaveFilter: "doneSaveFilter",
  saveFilterWithRule: "saveFilterWithRule",
  editFilterWithRule: "editFilterWithRule",
  doneRollBackFilter: "doneRollBackFilter",
  exportFilter: "exportFilter",
  delFGroup: "delFGroup",
  cantDeleteFGroup: "cantDeleteFGroup",
  cantDelete1DepthFGroup: "cantDelete1DepthFGroup",
  doneRollback: "doneRollback",
  resetCndtn: "resetCndtn",
  needCndtn: "needCndtn",
  deleteGroupOpr: "deleteGroupOpr",
  noneSeletedField: "noneSeletedField",
  noneSeletedCompany: "noneSeletedCompany",
  noneSeletedAgent: "noneSeletedAgent",
  noneSeletedAgentVendor: "noneSeletedAgentVendor",
  noneSeletedAgentModel: "noneSeletedAgentModel",
  noneSeletedSelectCompany: "noneSeletedSelectCompany",
  noneSeletedCndtn: "noneSeletedCndtn",
  notSupportFile: "notSupportFile",
  notSupportFileSize: "notSupportFileSize",
  maxSelectCount: "maxSelectCount",
  networkError: "networkError"
};

const getMessage = (type, text = null) => {
  switch (type) {
    case "delFilter":
      return (
        <FormattedMessage id="delete.select.filter">
          {text => text}
        </FormattedMessage>
      );
    case "editFilter":
      return (
        <FormattedMessage id="change.select.filter">
          {text => text}
        </FormattedMessage>
      );
    case "deletedFilter":
      return (
        <FormattedMessage id="complete.delete">{text => text}</FormattedMessage>
      );
    case "noneSeletedFilter":
      return (
        <FormattedMessage id="select.filter"> {text => text} </FormattedMessage>
      );
    case "cantDeleteFilterWithRule":
      return (
        <FormattedMessage id="no.delete"> {text => text} </FormattedMessage>
      );
    case "saveFilter":
      return (
        <FormattedMessage id="apply.change"> {text => text} </FormattedMessage>
      );
    case "doneSaveFilter":
      return (
        <FormattedMessage id="save.sentence"> {text => text} </FormattedMessage>
      );
    case "saveFilterWithRule":
      return (
        <FormattedMessage id="reflect.filter.change">
          {text => text}
        </FormattedMessage>
      );
    case "editFilterWithRule":
      return (
        <FormattedMessage id="do.edit.filter">{text => text}</FormattedMessage>
      );
    case "doneRollBackFilter":
      return (
        <FormattedMessage id="is.roll.back"> {text => text} </FormattedMessage>
      );
    case "exportFilter":
      return (
        <FormattedMessage id="export.filter"> {text => text} </FormattedMessage>
      );
    case "delFGroup":
      return <FormattedMessage id="delete.group" values={{ group: text }} />;
    case "cantDeleteFGroup":
      return (
        <FormattedMessage
          id="delete.not.subgroup.filters"
          values={{ group: text }}
        />
      );
    case "cantDelete1DepthFGroup":
      return (
        <FormattedMessage id="delete.not.1depth" values={{ group: text }} />
      );
    case "doneRollback":
      return (
        <FormattedMessage id="is.roll.back"> {text => text} </FormattedMessage>
      );
    case "resetCndtn":
      return (
        <FormattedMessage id="reset.filter.condition">
          {text => text}
        </FormattedMessage>
      );
    case "needCndtn":
      return (
        <FormattedMessage id="require.filter.save">
          {text => text}
        </FormattedMessage>
      );
    case "deleteGroupOpr":
      return (
        <FormattedMessage id="delete.filed.operator">
          {text => text}
        </FormattedMessage>
      );
    case "noneSeletedField":
      return (
        <FormattedMessage id="select.field"> {text => text} </FormattedMessage>
      );
    case "noneSeletedCompany":
      return (
        <FormattedMessage id="select.cust.info">
          {text => text}
        </FormattedMessage>
      );
    case "noneSeletedAgent":
      return (
        <FormattedMessage id="select.asset.info">
          {text => text}
        </FormattedMessage>
      );
    case "noneSeletedAgentVendor":
      return (
        <FormattedMessage id="select.equip.vendor.info">
          {text => text}
        </FormattedMessage>
      );
    case "noneSeletedAgentModel":
      return (
        <FormattedMessage id="select.equip.model.info">
          {text => text}
        </FormattedMessage>
      );
    case "noneSeletedSelectCompany":
      return (
        <FormattedMessage id="select.company.info">
          {text => text}
        </FormattedMessage>
      );
    case "noneSeletedCndtn":
      return (
        <FormattedMessage id="add.field.select">
          {text => text}
        </FormattedMessage>
      );
    case "notSupportFile":
      return <FormattedMessage id="file.type" values={{ format: text }} />;
    case "notSupportFileSize":
      return <FormattedMessage id="png.file.upload" values={{ size: text }} />;
    case "maxSelectCount":
      return <FormattedMessage id="max.count" values={{ count: text }} />;
    case "networkError":
      return <FormattedMessage id="error.server" values={{ error: text }} />;
    default:
      isDebug && console.log("Error!!!! undefined type=", type);
      return "";
  }
};

const filterConfirmPopupResource = (
  type,
  cancelCB = null,
  confirmCB = null,
  gName = null,
  hasChangeReason = null
) => {
  isDebug && console.log("filterConfirmPopupResource", type, hasChangeReason);
  switch (type) {
    case "delFilter":
    case "editFilter":
    case "saveFilter":
    case "saveFilterWithRule":
      return {
        title: (
          <FormattedMessage id="filter.manage">{text => text}</FormattedMessage>
        ),
        message: getMessage(type, gName),
        hasChangeReason: hasChangeReason != null ? hasChangeReason : true,
        btns: TWO_BTN_CONFIRM(cancelCB, confirmCB)
      };

    case "deletedFilter":
    case "noneSeletedFilter":
    case "noneSeletedField":
    case "cantDeleteFilterWithRule":
    case "doneSaveFilter":
    case "doneRollBackFilter":
    case "cantDeleteFGroup":
    case "cantDelete1DepthFGroup":
    case "noneSeletedCndtn":
    case "notSupportFile":
    case "notSupportFileSize":
    case "maxSelectCount":
    case "networkError":
    case "needCndtn":
      return {
        title:
          type === "noneSeletedField" ? (
            <FormattedMessage id="field.select">
              {text => text}
            </FormattedMessage>
          ) : (
            <FormattedMessage id="filter.manage">
              {text => text}
            </FormattedMessage>
          ),
        message: getMessage(type, gName),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    case "exportFilter":
    case "delFGroup":
    case "resetCndtn":
    case "deleteGroupOpr":
    case "editFilterWithRule":
      return {
        title: (
          <FormattedMessage id="filter.manage">{text => text}</FormattedMessage>
        ),
        message: getMessage(type, gName),
        hasChangeReason: false,
        btns: TWO_BTN_CONFIRM(cancelCB, confirmCB)
      };

    case "noneSeletedCompany":
      return {
        title: (
          <FormattedMessage id="cust.select"> {text => text} </FormattedMessage>
        ),
        message: getMessage(type, gName),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    case "noneSeletedAgent":
      return {
        title: (
          <FormattedMessage id="asset.select">{text => text}</FormattedMessage>
        ),
        message: getMessage(type, gName),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    case "noneSeletedAgentVendor":
      return {
        title: (
          <FormattedMessage id="equip.vendor.select">
            {text => text}
          </FormattedMessage>
        ),
        message: getMessage(type, gName),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    case "noneSeletedAgentModel":
      return {
        title: (
          <FormattedMessage id="equip.model.select">
            {text => text}
          </FormattedMessage>
        ),
        message: getMessage(type, gName),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    case "noneSeletedSelectCompany":
      return {
        title: (
          <FormattedMessage id="company.select">
            {text => text}
          </FormattedMessage>
        ),
        message: getMessage(type, gName),
        hasChangeReason: false,
        btns: ONE_BTN_CONFIRM(confirmCB)
      };

    default:
      isDebug && console.log("Error!!!! undefined type=", type);
      return null;
  }
};

export { filterPopupType, filterConfirmPopupResource };
