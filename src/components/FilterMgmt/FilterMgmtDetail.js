import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textarea } from "react-inputs-validation";

import {
  filterPopupType,
  filterConfirmPopupResource
} from "./const/popupResource";

import { action as filterDetailAction } from "../../store/modules/filterMgmtDetailAction";
import { action as filterAction } from "../../store/modules/filterMgmtListAction";
import * as ruleActions from "../../store/modules/rule";

import FilterCndtnTree from "./present/FilterCndtnTree";
import FilterInfo from "./present/FilterInfo";
import RuleList from "./present/RuleGrid";

// 룰 팝업을 위해서
import RuleInfoPopup from "../ThreatTaxonomySystem/popups/RuleInfoPopup";
import { getCodeList } from "../../store/api/common";

import {
  isDebug,
  DETAIL_TAB_TYPE,
  VIEW_TYPE,
  GROUP_OP_TYPE,
  SELECT_POPUP_TYPE,
  FIXED_FILTER_GROUP,
  PAGE_TYPE,
  MAX_INFINITE_COUNT,
  FIXED_OPR_EQ,
  MAX_SELECT_FIELD_OPR_EQ
} from "./const/filterConfig";
import FilterHistGrid from "./present/FilterHistGrid";
import {
  checkJsonFile,
  find1DepthGroup,
  convertListToTree
} from "./const/utils";

import { exportObjectToJson } from "./const/jsonUtils";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class FilterMgmtDetailComponent extends Component {
  state = {
    pageType: this.props.detailPageType,
    selectedTab: DETAIL_TAB_TYPE.INFO,

    currentFilter: this.props.selectedFilter ? this.props.selectedFilter : {}, // 현재 필터
    changeReason: "",

    // 필터 조건
    cndtnData: null, // 필터 조건
    selectedCndtnRow: null, // 필터 조건중 선택된 필터조건 그룹 오퍼레이션
    deleteCndtn: [],
    backupFiler: {
      // tab 전환시에 백업된 정보
      filter: null,
      cndtnData: null
    },

    // 룰 정보 관련
    isShowRuleInfo: false,
    selectedRule: null,
    ruleType: [],

    // 유효성 검토
    validation: {
      isVlidation: false,
      hasErrGroupNm: true, // has Error, 초기값은 에거가 존대한다고 봄
      hasErrFilterNm: true,
      hasErrCommitPeriod: true,
      hasErrChReason: true
    },

    // 공통 코드
    groupOprCode: [],
    fieldOprCode: []
  };

  changeValidation = validation => {
    this.setState({ validation });
  };

  checkValidation = () => {
    const { validation } = this.state;

    // validation.isVlidation 상태 값 변경
    this.setState({
      validation: { ...this.state.validation, isVlidation: true }
    });

    return (
      !validation.hasErrGroupNm &&
      !validation.hasErrFilterNm &&
      !validation.hasErrCommitPeriod &&
      !validation.hasErrChReason
    );
  };

  isNewMode = () => this.state.pageType === VIEW_TYPE.NEW;
  isViewMode = () => this.state.pageType === VIEW_TYPE.VIEW;

  setDefautlFilterInfo = () => {
    let filter = this.state.currentFilter;
    isDebug &&
      console.log("setDefautlFilterInfo-001", this.state.currentFilter);

    // 신규의 경우 디폴트 그룹은 미분류로 설정
    if (
      this.isNewMode() &&
      (!filter.filterGroupNm || filter.filterGroupNm.length === 0)
    ) {
      let fg = find1DepthGroup(
        this.props.filterGroupData,
        FIXED_FILTER_GROUP.UNDEFINED
      );
      filter.filterGroupNm = fg.name;
      filter.filterGroupNmPath = `${fg.filterGroupNmPath} > ${fg.name}`;
      filter.filterGroupSeq = fg.filterGroupSeq;
      filter.status = fg.status;
    }
    // 룰 선택시 디폴트로 설정(기간, 현재시점으로)
    if (!filter.filterCommitPeriodYn) filter.filterCommitPeriodYn = "Y";
    if (!filter.filterCommitPeriodStrDay)
      filter.filterCommitPeriodStrDay = new Date();
    if (!filter.filterCommitPeriodEndDay)
      filter.filterCommitPeriodEndDay = new Date();

    isDebug && console.log("setDefautlFilterInfo-002", filter);
    this.setState({ currentFilter: filter });
  };

  // db cndtn table 정보를 이용해서
  setFilterInfo = cndtnDataList => {
    if (this.props.selectedFilter) {
      // db로 부터 읽어온 데이터를 front에서 사용할 수 있도록 가공함.
      let cndtnDataTree = null;
      isDebug &&
        console.log("setFilterInfo", this.props.filterCndtn, cndtnDataList);
      if (cndtnDataList) {
        cndtnDataTree = {};
        convertListToTree(
          cndtnDataList,
          cndtnDataTree,
          cndtn => {
            cndtn.changeState = "o";
            return cndtn;
          },
          0
        );
        cndtnDataTree = cndtnDataTree.sub;
      }

      isDebug && console.log("setFilterInfo", cndtnDataTree);
      this.setState({
        currentFilter: this.props.selectedFilter,
        cndtnData: cndtnDataTree,
        selectedCndtnRow: cndtnDataTree
      });
    }
  };

  componentDidMount = () => {
    if (this.isNewMode()) {
      // new의 경우 디폴트 미분류그룹 설정
      this.setDefautlFilterInfo();
    }

    // 초기 데이터 가져오기
    this.handleInitLoadData();
    this.handleGetCommonCode();
  };

  handleGetCommonCode = async () => {
    try {
      const groupOprCode = await getCodeList("GROUP_OPRTR");
      const fieldOprCode = await getCodeList("FIELD_OPRTR");

      this.setState({
        groupOprCode: groupOprCode.data,
        fieldOprCode: fieldOprCode.data
      });

      isDebug && console.log("공통코드", groupOprCode, fieldOprCode);
    } catch (error) {
      console.error(error);
    }
  };

  // 초기데이터: 그룹 전체, 필터 전체 가져옴
  handleInitLoadData = async () => {
    const { filterListAction, filterDetailAction } = this.props;

    this.props.setLoading(true);
    let filter = this.state.currentFilter;

    try {
      await filterListAction.getInitData({}, {});

      if (!this.isNewMode() && filter.filterSeq) {
        let returnCndtn = await filterDetailAction.getFilterCndtn(
          filter.filterSeq
        );
        let returnHist = await filterDetailAction.getFilterHist(
          filter.filterSeq
        );
        isDebug &&
          console.log(
            "handleLoadData histdata=",
            filter,
            returnHist,
            returnCndtn
          );

        this.setFilterInfo(returnCndtn.data);

        this.handleGetFilterRules(
          filter,
          this.props.filterHist ? this.props.filterHist[0] : null
        );
      }
    } catch (error) {
      console.error("handleInitLoadData", error);
      //this.props.errorNotiPopup(error);
    } finally {
      this.props.setLoading(false);
    }
  };

  // hist를 다시 가져오는 경우는 필터가 seq를 기준으로 완전히 변경된 경우임.
  handleGetFilterHist = async filter => {
    const { filterDetailAction } = this.props;

    this.props.setLoading(true);

    try {
      let data1 = await filterDetailAction.getFilterHist(filter.filterSeq);

      isDebug && console.log("handleGetFilterHist", data1);

      filter &&
        this.handleGetFilterRules(
          filter,
          this.props.filterHist ? this.props.filterHist[0] : null
        );
    } catch (error) {
      console.error("handleGetFilterHist", error);
      //this.props.errorNotiPopup(error);
    } finally {
      this.props.setLoading(false);
    }
  };

  // rule 정보는 hist와 filter에 따라서 가져와야 함 (현재는 데이터가 없어서 filterSeq만 고려)
  handleGetFilterRules = async (filter, filterhist) => {
    const { filterDetailAction } = this.props;

    this.props.setLoading(true);

    try {
      let data1 = await filterDetailAction.getFilterRules(
        filter.filterSeq,
        /*filterhist ? filterhist.filterChgHistSeq : null*/ null
      ); // 임시로 null

      let ruleType = await getCodeList("RULE_TYPE");
      this.setState({
        ruleType: ruleType.data
      });

      isDebug && console.log("handleGetFilterRules", data1, ruleType);
    } catch (error) {
      console.error("handleGetFilterRules", error);
      //this.props.errorNotiPopup(error);
    } finally {
      this.props.setLoading(false);
    }
  };

  handleSaveFilter = async (target, isRollback = false) => {
    const { filterListAction } = this.props;
    this.props.setLoading(true);
    let cndtnList = [...this.state.cndtnData, ...this.state.deleteCndtn];

    isDebug &&
      console.log(
        "handleSaveFilter type=",
        this.state.pageType,
        target,
        cndtnList
      );
    try {
      let data = (await (this.state.pageType === VIEW_TYPE.NEW))
        ? filterListAction.addFilter(target, cndtnList ? cndtnList : null)
        : filterListAction.editFilter(target, cndtnList ? cndtnList : null);

      isDebug &&
        console.log(
          "handleSaveFilter return data=",
          data,
          this.props.saveFilterInfo
        );

      // 저장 여부 confirm 팝업
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          isRollback
            ? filterPopupType.doneRollBackFilter
            : filterPopupType.doneSaveFilter,
          null,
          () => {
            this.props.goToTargetPage(PAGE_TYPE.LIST);
            this.props.closePopup();
          }
        )
      );
    } catch (error) {
      console.error("handleSaveFilter", error);
      //this.props.errorNotiPopup(error);
    } finally {
      this.props.setLoading(false);
    }
  };

  handleDeleteFilter = async () => {
    const { filterListAction } = this.props;
    this.props.setLoading(true);

    let target = this.props.selectedFilter; // 삭제시는 기존 변경 사항 반영하지 않고 삭제만
    // 삭제 대신 휴지통으로 이동
    //target.useYn = "N";
    let parentFG = find1DepthGroup(
      this.props.filterGroupData,
      FIXED_FILTER_GROUP.TEMP
    );
    target.filterGroupNm = parentFG.name;
    target.filterGroupNmPath = `${parentFG.filterGroupNmPath} > ${parentFG.name}`;
    target.filterGroupSeq = parentFG.filterGroupSeq;
    target.status = parentFG.status;
    target.filterChgReason = this.state.changeReason;

    isDebug && console.log("handleDeleteFilter return", target);

    try {
      let data = await filterListAction.editFilters([target], {});

      isDebug && console.log("handleDeleteFilter return data=", data);

      this.props.openConfirmPopup(
        filterConfirmPopupResource(filterPopupType.deletedFilter, null, () => {
          this.props.closePopup();
          this.props.goToTargetPage(PAGE_TYPE.LIST);
        })
      );
    } catch (error) {
      console.error("handleDeleteFilter", error);
      //this.props.errorNotiPopup(error);
    } finally {
      this.props.setLoading(false);
    }
  };

  handleRuleInfoPopup = async (isShow, target) => {
    if (isShow) {
      const { RuleActions } = this.props;
      const res = await RuleActions.getRule(target.ruleSeq);
      this.setState({
        isShowRuleInfo: isShow,
        selectedRule: res.data
      });
    } else {
      this.setState({
        isShowRuleInfo: isShow,
        selectedRule: target
      });
    }
  };

  onClickSelectTab = tab => {
    if (tab === DETAIL_TAB_TYPE.HIST) {
      this.setState({
        selectedTab: tab,

        backupFiler: {
          // 수정 중인 정보를 유지할 수 있도록 백업
          filter: this.state.currentFilter,
          cndtnData: this.state.cndtnData
        }
      });
    } else {
      let backupFiler = this.state.backupFiler;
      isDebug && console.log("onClickSelectTab-list", backupFiler);
      this.setState({
        selectedTab: tab,

        currentFilter: backupFiler.filter, // 백업 복구
        cndtnData: backupFiler.cndtnData,
        selectedCndtnRow: backupFiler.cndtnData
      });
    }
  };

  selectedFilterHist = filterHist => {
    // history tab에서 변경된 데이터를 UI에 반영
    if (this.isHistTab()) {
      let selectedftHist = filterHist;
      if (filterHist.filterNm) {
        // 필수 항목, 없다면 데이터가 없는 빈 필터로 봄.
        selectedftHist.name = filterHist.filterNm;
        if (selectedftHist.filterCndtnJson) {
          let cndtnData = JSON.parse(selectedftHist.filterCndtnJson);
          this.changeCndtn(cndtnData);

          this.setState({
            currentFilter: selectedftHist,
            cndtnData: cndtnData,
            selectedCndtnRow: cndtnData
          });
        }
      } else {
        this.setState({
          currentFilter: selectedftHist
        });
      }
    }
  };

  onClickDeleteFilter = () => {
    if (this.props.selectedFilter && this.props.selectedFilter.ruleCnt > 0) {
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          filterPopupType.cantDeleteFilterWithRule,
          null,
          this.props.closePopup
        )
      );
    } else {
      if (this.checkValidation()) {
        this.handleDeleteFilter();
      }
    }
  };

  onClickEditFilter = () => {
    if (this.props.selectedFilter && this.props.selectedFilter.ruleCnt > 0) {
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          filterPopupType.editFilterWithRule,
          this.props.closePopup,
          () => this.setState({ pageType: VIEW_TYPE.EDIT })
        )
      );
    } else {
      this.setState({ pageType: VIEW_TYPE.EDIT });
    }
  };

  saveFilter = () => {
    let target = this.state.currentFilter;
    target.filterChgReason = this.state.changeReason;
    target.filterCndtnJson = this.state.cndtnData
      ? JSON.stringify(this.state.cndtnData)
      : null;

    this.handleSaveFilter(target);
  };

  onClickSaveFilter = () => {
    if (!this.state.cndtnData || this.state.cndtnData.length < 1) {
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          filterPopupType.needCndtn,
          null,
          this.props.closePopup
        )
      );
      return;
    }

    if (this.checkValidation()) {
      if (this.props.selectedFilter && this.props.selectedFilter.ruleCnt > 0) {
        this.props.openConfirmPopup(
          filterConfirmPopupResource(
            filterPopupType.saveFilterWithRule,
            this.props.closePopup,
            () => this.saveFilter(),
            null,
            false
          )
        );
      } else {
        this.saveFilter();
      }
    }
  };

  changeFilterInfo = filter => {
    //isDebug && console.log("changeFilterInfo:", filter)
    this.setState({
      currentFilter: filter
    });
  };

  selectFilterFromPopup = filter => {
    //isDebug && console.log("changeFilterInfo:", filter)

    let cndtnData = JSON.parse(filter.filterCndtnJson);
    this.changeCndtn(cndtnData);

    this.setState({
      currentFilter: filter,
      cndtnData: cndtnData,
      selectedCndtnRow: cndtnData
    });

    // this.handleGetFilterHist(filter); 필터에 대한 정보만 가져옴, history는 영향을 주면 않는다.
  };

  // 필터 그룹에서 그룹 선택하고 그 결과를 돌려 받는 콜백 함수
  seletcFilterGroupCB = fg => {
    let filter = this.state.currentFilter;
    filter.filterGroupNm = fg.name;
    filter.filterGroupNmPath = `${fg.filterGroupNmPath} > ${fg.name}`;
    filter.status = fg.status;
    filter.filterGroupSeq = fg.filterGroupSeq;

    isDebug && console.log("seletcFilterGroupCB: ", fg, filter);
    this.setState({
      currentFilter: filter
    });
  };

  onChangeFilterChangeReason = (value, e) => {
    e.preventDefault();
    //isDebug && console.log("onChangeFilterChangeReason: ", e.target.vlue);
    this.setState({
      changeReason: value
    });
  };

  importJosnFile = e => {
    let importJsonFile = e.target.files[0];
    isDebug && console.log("importJosnFile", importJsonFile);

    if (checkJsonFile(importJsonFile.name)) {
      let reader = new FileReader();
      reader.onload = e => {
        let jsonObj = JSON.parse(e.target.result);

        let cndtnData = JSON.parse(jsonObj.filterCndtnJson);
        this.changeCndtn(cndtnData);

        isDebug &&
          console.log(
            "importJosnFile",
            cndtnData,
            jsonObj,
            importJsonFile.size
          );

        this.setState({
          currentFilter: jsonObj,
          cndtnData: cndtnData,
          selectedCndtnRow: cndtnData
        });
      };
      reader.readAsText(importJsonFile);
    } else {
      this.props.openConfirmPopup(
        filterConfirmPopupResource(
          filterPopupType.notSupportFile,
          null,
          this.props.closePopup,
          "json"
        )
      );
    }
  };

  handleExportFilter = resource => {
    exportObjectToJson(resource.target);
  };

  onClickExportBtn = () => {
    let resource = filterConfirmPopupResource(
      filterPopupType.exportFilter,
      this.props.closePopup,
      this.handleExportFilter
    );

    resource.target = this.state.currentFilter;
    this.props.openConfirmPopup(resource);
  };

  changeCndtnData = (cndtnData, row) => {
    this.setState({ cndtnData: cndtnData });
  };

  changeSelectCndtnRow = row => {
    this.setState({ selectedCndtnRow: row });
  };

  // 필터 조건의 저장/변경/삭제 등의 상태를 파악하기 위해 상태 값을 추가하는 함수
  setCndtnState = (row, state) => {
    row.changeState = state;

    if (row.sub && row.sub.length > 0) {
      row.sub.map(data => {
        if (data.sub) {
          this.setCndtnState(data, "d");
        } else {
          data.changeState = state;
        }
      });
    }
  };

  deleteCndtn = row => {
    if (row.changeState !== "n") {
      // 신규는 삭제항목에 넣으면 안됨. o와 c 만 넣는다
      this.setCndtnState(row, "d");

      let delList = this.state.deleteCndtn;
      delList.push(row);
      this.setState({ deleteCndtn: delList });
      isDebug && console.log("deleteCndtn: ", row, delList);
    }
  };

  // 완전히 새로운 것으로 변경할 때
  changeCndtn = newCndtn => {
    // 기존의 것이 있다면, 기존의 것 삭제 상태가 추가하고 delete 항목으로 저장해 둠.
    this.state.cndtnData &&
      this.state.cndtnData.map(data => this.deleteCndtn(data));
    // 새로운 것 상태값 추가
    newCndtn && newCndtn.map(row => this.setCndtnState(row, "n"));
  };

  addGroupOpr = type => {
    const { cndtnData, selectedCndtnRow, currentFilter } = this.state;
    let target = selectedCndtnRow ? selectedCndtnRow : [];
    let groupOprToAdd = {
      depth:
        selectedCndtnRow && cndtnData !== selectedCndtnRow
          ? target.depth + 1
          : 0,
      order: Array.isArray(selectedCndtnRow)
        ? target && target.length > 0
          ? target[target.length - 1].order + 1
          : 0 //root
        : target.sub && target.sub.length > 0
        ? target.sub[target.sub.length - 1].order + 1
        : 0, // root 아닌 경우

      filterSeq: currentFilter.filterSeq ? currentFilter.filterSeq : 0, // 신규등록 0
      filterChgHistSeq: 0,

      groupOprtr: type,
      changeState: "n"
    };

    if (selectedCndtnRow && cndtnData !== selectedCndtnRow) {
      if (!target.sub) target.sub = [];
      target.sub.push(groupOprToAdd);
      this.setState({
        selectedCndtnRow: target
      });
    } else {
      // root
      target.push(groupOprToAdd);
      this.setState({
        cndtnData: target,
        selectedCndtnRow: target
      });
    }

    //isDebug && console.log("addGroupOpr:", target, groupOprToAdd, this.state.cndtnData, this.state.selectedCndtnRow);
  };

  resetCndtn = () => {
    let resource = filterConfirmPopupResource(
      filterPopupType.resetCndtn,
      this.props.closePopup,
      () => {
        this.changeCndtn(null);
        this.setState({
          cndtnData: null,
          selectedCndtnRow: null
        });
      }
    );

    this.props.openConfirmPopup(resource);
  };

  addField = mapData => {
    let parent = this.state.selectedCndtnRow;
    let startOrder =
      parent.sub && parent.sub.length > 0
        ? parent.sub[parent.sub.length - 1].order + 1
        : 0;
    let selectItems = [];
    [...mapData.values()].map((data, i) =>
      selectItems.push({
        depth: parent.depth + 1,
        order: startOrder + i,

        filterSeq: this.state.currentFilter.filterSeq
          ? this.state.currentFilter.filterSeq
          : 0, // 신규등록 0
        filterChgHistSeq: 0,

        fieldId: data.fieldId,
        fieldType: data.fieldType,
        field: data.field,
        oprtr: FIXED_OPR_EQ,
        cndtnValue: "",
        keyFieldYn: "N",

        changeState: "n"
      })
    );

    isDebug && console.log("selectPopupCB:", mapData, selectItems, parent);

    parent.sub = parent.sub
      ? [...parent.sub, ...selectItems]
      : [...selectItems];
    this.setState({ selectedCndtnRow: parent });
  };

  openSelectFieldPopup = (
    type = SELECT_POPUP_TYPE.fieldPopup,
    CBFunction,
    row
  ) => {
    isDebug &&
      console.log(
        "openSelectFieldPopup:",
        type,
        CBFunction,
        this.state.selectedCndtnRow
      );
    if (type === SELECT_POPUP_TYPE.fieldPopup) {
      this.state.selectedCndtnRow &&
      this.state.cndtnData !== this.state.selectedCndtnRow
        ? this.props.openSelectPopup(type, this.addField)
        : this.props.openConfirmPopup(
            filterConfirmPopupResource(
              filterPopupType.noneSeletedCndtn,
              null,
              this.props.closePopup
            )
          );
    } else {
      this.props.openSelectPopup(
        type,
        CBFunction,
        row.oprtr && row.oprtr === FIXED_OPR_EQ
          ? MAX_SELECT_FIELD_OPR_EQ
          : MAX_INFINITE_COUNT
      );
    }
  };

  isInfoTab = () => this.state.selectedTab === DETAIL_TAB_TYPE.INFO;
  isHistTab = () => this.state.selectedTab === DETAIL_TAB_TYPE.HIST;

  getOptBtn = opt => {
    switch (opt.id) {
      case GROUP_OP_TYPE.AND:
        return (
          <button
            key="gOPAnd"
            className="btn btn-icon small btn--and"
            onClick={() => this.addGroupOpr(opt.id)}
          />
        );
      case GROUP_OP_TYPE.OR:
        return (
          <button
            key="gOPOr"
            className="btn btn-icon small btn--or"
            onClick={() => this.addGroupOpr(opt.id)}
          />
        );
      case GROUP_OP_TYPE.NOT:
        return (
          <button
            key="gOPNot"
            className="btn btn-icon small btn--not"
            onClick={() => this.addGroupOpr(opt.id)}
          />
        );
      default:
        return "";
    }
  };
  renderFilterCndtnHeader = () => {
    return this.isInfoTab() ? (
      <div className="component__title">
        <span>
          <IntlMessages id="filter.condition" />
        </span>
        <span className="font--red">*</span>
        <div className="btns btns--left">
          {this.state.groupOprCode &&
            this.state.groupOprCode.map(opt => this.getOptBtn(opt))}
        </div>
        <div className="binder" />
        <button
          className="btn btn--blue"
          onClick={() => this.openSelectFieldPopup()}
        >
          <IntlMessages id="field.create" />
        </button>

        <div className="btns btns--right">
          <button className="btn btn--dark" onClick={this.resetCndtn}>
            <IntlMessages id="reset" />
          </button>
          <button
            className="btn btn--dark"
            onClick={() =>
              this.props.openSelectFilterPopup(this.selectFilterFromPopup)
            }
          >
            <IntlMessages id="filter.select" />
          </button>
        </div>
      </div>
    ) : (
      <div className="component__title" style={{ marginBottom: 14 }}>
        <span>
          <IntlMessages id="filter.condition" />
        </span>
      </div>
    );
  };

  // 필터 변경 이력
  renderHistory = () => {
    return (
      <div className="history">
        {this.isNewMode() ? (
          <FilterHistGrid
            filterHistData={[]}
            selectedFilterHist={this.selectedFilterHist}
          />
        ) : (
          <FilterHistGrid
            filterHistData={this.isNewMode() ? [] : this.props.filterHist}
            filter={this.props.selectedFilter}
            selectedFilterHist={this.selectedFilterHist}
            handleSaveFilter={this.handleSaveFilter}
            openConfirmPopup={this.props.openConfirmPopup}
            closePopup={this.props.closePopup}
          />
        )}
      </div>
    );
  };

  renderBottomBtns = () => {
    const { goToTargetPage } = this.props;
    return this.isInfoTab() ? (
      <div className="btns">
        <button
          className="btn btn--dark"
          onClick={() => goToTargetPage(PAGE_TYPE.LIST)}
        >
          <IntlMessages id="cancel" />
        </button>
        {this.isNewMode() === false && (
          <button className="btn btn--dark" onClick={this.onClickDeleteFilter}>
            <IntlMessages id="delete" />
          </button>
        )}
        {this.isViewMode() ? (
          <button className="btn btn--blue" onClick={this.onClickEditFilter}>
            <IntlMessages id="edit" />
          </button>
        ) : (
          <button className="btn btn--blue" onClick={this.onClickSaveFilter}>
            <IntlMessages id="save" />
          </button>
        )}
      </div>
    ) : (
      <div className="btns">
        <button
          className="btn btn--dark"
          onClick={() => goToTargetPage(PAGE_TYPE.LIST)}
        >
          <IntlMessages id="cancel" />
        </button>
      </div>
    );
  };

  // 변경 이력을 포함하지 않은 경우 - this.state.selectedTab is 'info'
  renderCommon = () => {
    return (
      <div className="component filter-detail">
        <div className="top">
          {this.isHistTab() && this.renderHistory()}
          {/* 필터정보 */}
          <div className="left">
            <div className="filter-info">
              <div className="component__title">
                <span className="filter-title">
                  <IntlMessages id="basic.info" />
                </span>

                {this.isInfoTab() && this.isNewMode() && (
                  <div className="file-import">
                    <input
                      id="importBtn"
                      type="file"
                      className="form-control"
                      onChange={this.importJosnFile}
                      accept="*.json"
                    ></input>
                    <label
                      htmlFor="importBtn"
                      className="btn btn-icon btn--file"
                    />
                  </div>
                )}
                {this.isInfoTab() && this.isViewMode() && (
                  <div className="file-import">
                    <button
                      id="export"
                      className="btn btn-icon"
                      onClick={() => this.onClickExportBtn()}
                    >
                      <img
                        src="/images/common/icon_export.png"
                        alt="icon_export"
                      />
                    </button>
                  </div>
                )}
              </div>
              <FilterInfo
                openSelectFilterGroupPopup={() =>
                  this.props.openSelectFilterGroupPopup(
                    this.props.filterGroupData,
                    this.seletcFilterGroupCB
                  )
                }
                detailPageType={
                  this.isHistTab() ? VIEW_TYPE.VIEW : this.state.pageType
                }
                filter={
                  this.isHistTab() && this.isNewMode()
                    ? {}
                    : this.state.currentFilter
                }
                changeFilterInfo={this.changeFilterInfo}
                openConfirmPopup={this.props.openConfirmPopup}
                closePopup={this.props.closePopup}
                validation={this.state.validation}
                changeValidation={this.changeValidation}
              />
            </div>

            <div className="use-rule">
              <div className="component__title">
                <span>
                  <IntlMessages id="use.rule" />
                </span>
              </div>
              <RuleList
                ruleList={this.isNewMode() ? [] : this.props.filterRules}
                handleRuleInfoPopup={this.handleRuleInfoPopup}
              />
            </div>

            <div className="change-reason">
              <div className="component__title">
                <span>
                  <IntlMessages id="reason.change.reg" />
                </span>
                <span className="font--red">*</span>
              </div>
              {this.isHistTab() ? (
                <textarea
                  className="form-control"
                  name="ftChangeReason"
                  disabled
                  value={
                    this.state.currentFilter.filterChgReason
                      ? this.state.currentFilter.filterChgReason
                      : ""
                  }
                />
              ) : (
                <FormattedMessage id="enter.change.reason">
                  {placeholder => (
                    <Textarea
                      className="form-control"
                      name="ftChangeReason"
                      value={this.state.changeReason}
                      onChange={this.onChangeFilterChangeReason}
                      placeholder={placeholder}
                      onBlur={() => {}}
                      validate={this.state.validation.isVlidation}
                      validationCallback={res => {
                        this.setState({
                          validation: {
                            hasErrChReason: res,
                            isVlidation: false
                          }
                        });
                      }}
                      validationOption={{
                        name: <IntlMessages id="reason.change" />,
                        locale: this.props.language,
                        check: true,
                        required: true,
                        type: "string",
                        min: 5,
                        max: 2000, // (4000/2)
                        msgOnError: <IntlMessages id="field.reuqire.two" />
                      }}
                      customStyleInput={{
                        height: "80px",
                        width: "100%",
                        background: "#383b48",
                        color: "#ffffff",
                        outline: "0px",
                        border: "1px solid #1b1e29",
                        padding: "0 10px",
                        fontSize: "12px"
                      }}
                    />
                  )}
                </FormattedMessage>
              )}
            </div>
          </div>
          {/* <div className="left"></div> */}
          <div className="right">
            <div className="filter-condition">
              {/* 필터 조건 윗부분에 헤더  */}
              {this.renderFilterCndtnHeader()}

              {/* 필터 조건이 표시 되는 부분 */}
              <div className="component__box" style={{ maxWidth: 1175 }}>
                {/* {(!this.state.cndtnData || this.state.cndtnData.length < 1) && <span> 필수 입력 항목입니다.</span>} */}
                <FilterCndtnTree
                  cndtnData={
                    this.isHistTab() && this.isNewMode()
                      ? []
                      : this.state.cndtnData
                  }
                  selectedCndtnRow={this.state.selectedCndtnRow}
                  changeCndtnData={this.changeCndtnData}
                  changeSelectCndtnRow={this.changeSelectCndtnRow}
                  deleteCndtn={this.deleteCndtn}
                  openSelectFieldPopup={this.openSelectFieldPopup}
                  openConfirmPopup={this.props.openConfirmPopup}
                  closePopup={this.props.closePopup}
                  viewType={
                    this.isHistTab() ? VIEW_TYPE.VIEW : this.state.pageType
                  }
                  groupOprCode={this.state.groupOprCode}
                  fieldOprCode={this.state.fieldOprCode}
                />
              </div>
            </div>
            {/* <div className="filter-condition"> */}
          </div>
        </div>

        <div className="bottom">{this.renderBottomBtns()}</div>
      </div>
    );
  };

  render() {
    return (
      <div className="component-filter-detail">
        <ul className="component-tab">
          <li
            className={this.isInfoTab() ? "active" : ""}
            onClick={() => this.onClickSelectTab(DETAIL_TAB_TYPE.INFO)}
          >
            <IntlMessages id="filter.info" />
          </li>
          <li
            className={this.isHistTab() ? "active" : ""}
            onClick={() => this.onClickSelectTab(DETAIL_TAB_TYPE.HIST)}
          >
            <IntlMessages id="change.hist" />
          </li>
        </ul>

        {this.renderCommon()}

        {this.state.isShowRuleInfo && (
          <RuleInfoPopup
            ruleInfo={this.state.selectedRule}
            ruleType={this.state.ruleType}
            onClose={() => this.handleRuleInfoPopup(false, null)}
          />
        )}
      </div>
    );
  }
}

const FilterDetailConnecter = connect(
  state => ({
    // filter info
    filterHist: state.filterMgmtDetail.get("filterHist"),
    filterRules: state.filterMgmtDetail.get("filterRules"),
    filterCndtn: state.filterMgmtDetail.get("filterCndtn"),

    filterGroupData: state.filterMgmtList.get("filterGroup"),
    filterListData: state.filterMgmtList.get("filterList"),
    saveFilterInfo: state.filterMgmtList.get("saveInfo")
  }),
  dispatch => ({
    filterDetailAction: bindActionCreators(filterDetailAction, dispatch),
    filterListAction: bindActionCreators(filterAction, dispatch),
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(FilterMgmtDetailComponent);

export default FilterDetailConnecter;
export { FilterMgmtDetailComponent };
