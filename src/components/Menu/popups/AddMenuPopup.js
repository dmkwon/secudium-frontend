import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Textarea, Select } from "react-inputs-validation";
import Draggable from "react-draggable";

import * as menuActions from "./../../../store/modules/menu";
import { createMenu, getMenusByParentSeq } from "./../../../store/api/menu";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class AddMenuPopup extends Component {
  state = {
    menuNmKo: "",
    menuNmEn: "",
    menuUrl: "",
    menuSortOrder: "",
    menuDesc: "",
    topMenuSeq: "0",
    topMenuList: [{ name: <IntlMessages id="select.options" />, id: "0" }],

    useTypes: [
      { name: <IntlMessages id="use" />, id: "Y" },
      { name: <IntlMessages id="unused" />, id: "N" }
    ],

    validation: {
      validate: false,
      hasErrMenuNmKo: true,
      hasErrMenuNmEn: true,
      hasErrMenuUrl: true,
      hasErrMenuSortOrder: true,
      hasErrMenuDesc: false
    },
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("addMenuPopup").offsetWidth;
    const elHeight = document.getElementById("addMenuPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrTopMenuSeq,
      hasErrMenuNmKo,
      hasErrMenuNmEn,
      hasErrMenuUrl,
      hasErrMenuDesc,
      hasErrMenuSortOrder
    } = this.state.validation;

    return (
      !hasErrTopMenuSeq &&
      !hasErrMenuNmKo &&
      !hasErrMenuNmEn &&
      !hasErrMenuUrl &&
      !hasErrMenuDesc &&
      !hasErrMenuSortOrder
    );
  };

  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      [name]: value
    });
  };

  onClickSave = async e => {
    if (this.isValidate()) {
      const { MenuActions } = this.props;
      const inputMenu = this.state;
      try {
        await createMenu(inputMenu);
        this.setState({
          topMenuSeq: "",
          menuNmKo: "",
          menuNmEn: "",
          menuUrl: "",
          menuSortOrder: "",
          menuDesc: ""
        });
        this.props.closePopup();
        MenuActions.getMenus();
      } catch (e) {
        console.warn(e);
      }
    }
  };

  onChangeTopMenu = async (topMenu, e) => {
    try {
      const topMenus = await getMenusByParentSeq(topMenu);
      const menuList = topMenus.data.map(el => {
        return { name: el.menuNm, id: el.menuSeq };
      });
      this.setState({
        topMenuList: this.state.topMenuList.concat(menuList)
      });
    } catch (e) {
      console.warn(e);
    }
  };

  getTopMenuList = topMenuSeq => {
    this.setState({
      topMenuSeq
    });
  };

  componentWillMount() {
    this.onChangeTopMenu("0");
  }

  render() {
    const { handleChangeInput, onClickSave, getTopMenuList } = this;
    const { topMenuList, position } = this.state;
    const { validate } = this.state.validation;
    const { closePopup } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--move"
            id="addMenuPopup"
            style={{ width: "480px" }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="menu.reg" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="top.menu" />
                    </th>
                    <td>
                      <Select
                        id="topMenuSeq"
                        name="topMenuSeq"
                        tabIndex="1"
                        value={this.state.topMenuSeq}
                        optionList={topMenuList}
                        onChange={getTopMenuList}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="menu.name.ko" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="menuNmKo"
                        name="menuNmKo"
                        type="text"
                        tabIndex="2"
                        value={this.state.menuNmKo}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrMenuNmKo: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "MenuNmKo",
                          check: true,
                          required: true,
                          min: 2,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="menu.name.en" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="menuNmEn"
                        name="menuNmEn"
                        type="text"
                        tabIndex="2"
                        value={this.state.menuNmEn}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrMenuNmEn: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "MenuNmEn",
                          check: true,
                          required: true,
                          min: 2,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="menu.url" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="menuUrl"
                        name="menuUrl"
                        type="text"
                        tabIndex="3"
                        value={this.state.menuUrl}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrMenuUrl: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "MenuUrl",
                          check: true,
                          required: true,
                          min: 0,
                          max: 255,
                          reg: /^\/([/a-z0-9-%#?&._=\w])+$/,
                          regMsg: <IntlMessages id="wrong.url.type" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="sort.order" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="menuSortOrder"
                        name="menuSortOrder"
                        type="text"
                        tabIndex="4"
                        value={this.state.menuSortOrder}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrMenuSortOrder: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "MenuSortOrder",
                          check: true,
                          required: true,
                          reg: /^[0-9]+$/,
                          regMsg: <IntlMessages id="number.only" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ verticalAlign: "top" }}>
                      <IntlMessages id="desc" />
                    </th>
                    <td>
                      <Textarea
                        id="menuDesc"
                        name="menuDesc"
                        type="text"
                        tabIndex="5"
                        value={this.state.menuDesc}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrMenuDesc: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "MenuDesc",
                          check: true,
                          required: false,
                          max: 1000,
                          locale: this.props.language
                        }}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "70px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={onClickSave}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    MenuActions: bindActionCreators(menuActions, dispatch)
  })
)(AddMenuPopup);
