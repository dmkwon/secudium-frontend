import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as menuActions from "../../store/modules/menu";
import DeleteConfirmPopup from "./popups/DeleteConfirmPopup";
import DeleteConfirmListPopup from "./popups/DeleteConfirmListPopup";
import Loading from "./../Common/Loading";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import AddMenuPopup from "./popups/AddMenuPopup";
import UpdateMenuPopup from "./popups/UpdateMenuPopup";
import GridTable from "../Common/GridTable";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class MenuComponent extends Component {
  state = {
    isMenuPopup: false,
    isUpdateMenuPopup: false,
    isDeleteConfirmPopup: false,
    isDeleteConfirmListPopup: false,
    selectAll: false,
    checked: [],
    paramList: [],
    resource: {},
    showConfirm: false,

    menuSeq: "",
    menuId: "",
    menuNm: "",
    url: "",
    step: "",
    sort: ""
  };

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { menu } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < menu.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < menu.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { menu } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < menu.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onCheckDeleteHandler = e => {
    const { menu } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];

    if (checkArr.length === 0) {
      this.showAlert(
        <IntlMessages id="confirm" />,
        <IntlMessages id="no.selection" />
      );
    } else {
      let stdExists = false;
      for (let idx = 0; idx < checkArr.length; idx++) {
        if (checkArr[idx]) {
          paramList.push(menu.content[idx]);
          stdExists = true;
        }
      }
      if (stdExists) {
        this.setState({
          paramList: paramList,
          isDeleteConfirmListPopup: true
        });
      } else {
        this.showAlert(
          <IntlMessages id="confirm" />,
          <IntlMessages id="no.selection" />
        );
      }
    }
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          { name: <IntlMessages id="confirm" />, CB: () => this.onClosePopup() }
        ]
      }
    });
  };

  onClosePopup = () => {
    this.setState({ showConfirm: false });
  };
  onDeleteListConfirm = async () => {
    const { MenuActions } = this.props;
    const { paramList } = this.state;
    let rx = "";
    rx = await MenuActions.removeMenus(paramList);
    await MenuActions.getMenus();
    if (rx.status === 200) {
      this.closePopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: []
      });
    } else {
      this.closePopup();
    }
  };

  getMenus = async () => {
    const { MenuActions } = this.props;
    try {
      await MenuActions.getMenus();
    } catch (e) {
      console.log(e);
    }
  };

  getMenusBySeq = async menuSeq => {
    const { MenuActions } = this.props;
    try {
      await MenuActions.getMenusBySeq(menuSeq);
    } catch (e) {
      console.log(e);
    }
  };

  createMenus = async data => {
    const { MenuActions } = this.props;
    try {
      await MenuActions.createMenus(data);
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    this.getMenus();
  }

  openMenuPopup = () => {
    this.setState({
      isMenuPopup: true
    });
  };

  updateMenuPopup = menuSeq => {
    const { MenuActions } = this.props;
    const ajax = MenuActions.getMenusBySeq(menuSeq);
    ajax.then(() => {
      this.setState({
        isUpdateMenuPopup: true
      });
    });
  };

  deleteConfirmPopup = menuSeq => {
    const { MenuActions } = this.props;
    const ajax = MenuActions.getMenusBySeq(menuSeq);
    ajax.then(() => {
      this.setState({
        isDeleteConfirmPopup: true
      });
    });
  };

  closePopup = () => {
    this.setState({
      isMenuPopup: false,
      isUpdateMenuPopup: false,
      isDeleteConfirmPopup: false,
      isDeleteConfirmListPopup: false,
      selectAll: false,
      checked: [],
      paramList: []
    });
  };

  render() {
    const {
      chkBoxAllChange,
      chkBoxSingleChange,
      onDeleteListConfirm,
      onCheckDeleteHandler
    } = this;
    const {
      isMenuPopup,
      isUpdateMenuPopup,
      isDeleteConfirmPopup,
      isDeleteConfirmListPopup,
      showConfirm
    } = this.state;
    const { menu, menuBySeq } = this.props;

    const columns_menu = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        sortable: false,
        width: 40
      },
      {
        Header: <IntlMessages id="level" />,
        accessor: "menuLevel",
        style: { textAlign: "right" },
        width: 100,
        filterable: true
      },
      {
        Header: <IntlMessages id="sort.order" />,
        accessor: "menuSortOrder",
        style: { textAlign: "right" },
        width: 100,
        filterable: true
      },
      {
        Header: <IntlMessages id="menu.name.ko" />,
        accessor: "menuNmKo",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="menu.name.en" />,
        accessor: "menuNmEn",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="menu.url" />,
        accessor: "menuUrl",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "modUsrNm",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="mod.date" />,
        accessor: "stringModDate",
        style: { textAlign: "center" },
        width: 150,
        filterable: true
      },
      {
        Header: "Actions",
        accessor: "icon",
        style: { textAlign: "center" },
        width: 70,
        sortable: false,
        Cell: row => {
          return (
            <div>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.deleteConfirmPopup(row.original.menuSeq);
                }}
              >
                <img src="/images/common/icon_delete.png" alt="icon_delete" />
              </button>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.updateMenuPopup(row.original.menuSeq);
                }}
              >
                <img src="/images/common/icon_edit.png" alt="icon_edit" />
              </button>
            </div>
          );
        }
      }
    ];
    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper menu">
          <LeftMenu />
          <div className="component component-list">
            <div className="component__title">
              <span>
                <IntlMessages id="menu" />
              </span>
              <div className="btns">
                <button className="btn" onClick={onCheckDeleteHandler}>
                  <IntlMessages id="multi.delete" />
                </button>
                <button className="btn btn--blue" onClick={this.openMenuPopup}>
                  <IntlMessages id="new.reg" />
                </button>
                {/* <button className="btn btn-icon btn-rollback" onClick={this.getMenus}>
                                    <img src="/images/common/icon_rollback.png" alt="icon_rollback"/>
                                </button> */}
              </div>
            </div>

            <GridTable
              columns={columns_menu}
              data={menu.content}
              className="-striped -highlight"
              resizable={false}
              filterable={false}
              getTrProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: async e => {
                    chkBoxSingleChange(rowInfo.index);
                  }
                };
              }}
            />
          </div>
          {isMenuPopup && <AddMenuPopup closePopup={this.closePopup} />}
          {isUpdateMenuPopup && (
            <UpdateMenuPopup data={menuBySeq} closePopup={this.closePopup} />
          )}
          {isDeleteConfirmPopup && (
            <DeleteConfirmPopup data={menuBySeq} closePopup={this.closePopup} />
          )}
          {isDeleteConfirmListPopup && (
            <DeleteConfirmListPopup
              closePopup={this.closePopup}
              onDeleteListConfirm={onDeleteListConfirm}
            />
          )}
        </div>
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    menu: state.menu.get("menu"),
    loading: state.menu.get("loading"),
    menuBySeq: state.menu.get("menuBySeq")
  }),
  dispatch => ({
    MenuActions: bindActionCreators(menuActions, dispatch)
  })
)(MenuComponent);
