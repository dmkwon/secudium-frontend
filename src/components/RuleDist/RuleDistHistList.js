import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import ReactTooltip from "react-tooltip";

import GridTable from "./../Common/GridTable";
import ChgReasonPopup from "./../Field/popups/ChgReasonPopup";

import * as ruleDistActions from "../../store/modules/ruleDist";
import IntlMessages from "util/IntlMessages";

class RuleDistHistList extends Component {
  state = {
    selected: 0,
    histSeq: "",
    rollbackPopup: false
  };

  /** 롤백 버튼 */
  onClickRollback = ({ ruleDstrbtHistSeq }) => {
    this.setState(
      {
        histSeq: ruleDstrbtHistSeq
      },
      () => this.openPopup("rollbackPopup")
    );
  };
  /** 롤백이력 */
  rollbackHistory = async dstrbtCont => {
    try {
      const { RuleDistActions } = this.props;
      const ruleDstrbtHistSeq = this.state.histSeq;
      await RuleDistActions.rollbackDistHistory({
        ruleDstrbtHistSeq,
        dstrbtCont
      });

      this.closePopup("rollbackPopup");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 팝업제어 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** 이력 조회 */
  getRuleDistHistories = async () => {
    const { RuleDistActions } = this.props;
    try {
      const res = await RuleDistActions.getRuleDistHistories();
      if (res.data[0]) {
        this.props.onClickRow(res.data[0].ruleDstrbtHistSeq);
      }
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    this.getRuleDistHistories();
  }

  render() {
    const { onClickRollback, closePopup, rollbackHistory } = this;
    const { ruleDistHists } = this.props;
    const { rollbackPopup } = this.state;

    const columns = [
      {
        Header: "No.",
        accessor: "no",
        width: 50,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="deployment.timedate" />,
        accessor: "regDateFormatted",
        sortable: true,
        width: 120,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="deployment.person" />,
        accessor: "regUsrNm",
        width: 80,
        style: { textAlign: "center" }
      },
      {
        Header: <IntlMessages id="contents" />,
        accessor: "dstrbtCont",
        Cell: props => {
          const tooltipId = `fieldChgTooltip_${props.original.no}`;
          return (
            <React.Fragment>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
              <ReactTooltip
                className="tooltipClass"
                id={tooltipId}
                place="left"
                type="dark"
                effect="float"
              >
                {props.value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      },
      {
        Header: " Actions",
        accessor: "action1",
        width: 70,
        style: { textAlign: "center" },
        Cell: row => {
          return row.original.isLatest === "N" ? (
            <React.Fragment>
              <button
                type="button"
                className="btn"
                style={{ height: "28px", width: "28px", padding: "0" }}
                onClick={e => {
                  e.stopPropagation();
                  onClickRollback(row.original);
                }}
              >
                <i className="fas fa-redo"></i>
              </button>
            </React.Fragment>
          ) : (
            ""
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <GridTable
          columns={columns}
          data={ruleDistHists}
          filterable={false}
          sortable
          resizable={false}
          className="-highlight"
          defaultSorted={[{ id: "regDateFormatted", desc: true }]}
          getTrProps={(state, row, column) => {
            if (row) {
              return {
                onClick: (e, handleOriginal) => {
                  this.setState({ selected: row.index });
                  //룰배포이력 상세조회
                  this.props.onClickRow(row.original.ruleDstrbtHistSeq);
                  if (handleOriginal) {
                    handleOriginal();
                  }
                },
                style: {
                  backgroundColor:
                    row.index === this.state.selected ? "#282a35" : "#2f323c",
                  cursor: "pointer"
                }
              };
            } else {
              return {
                style: {
                  cursor: "pointer"
                }
              };
            }
          }}
        />
        {rollbackPopup && (
          <ChgReasonPopup
            onClose={() => closePopup("rollbackPopup")}
            onConfirm={rollbackHistory}
            message={<IntlMessages id="roll.back.rule.info" />}
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    ruleDistHists: state.ruleDist.get("ruleDistHists")
  }),
  dispatch => ({
    RuleDistActions: bindActionCreators(ruleDistActions, dispatch)
  })
)(RuleDistHistList);
