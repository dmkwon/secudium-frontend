import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Map } from "immutable";
import ReactTooltip from "react-tooltip";

import GridTable from "./../Common/GridTable";
import RuleInfoPopup from "../ThreatTaxonomySystem/popups/RuleInfoPopup";

import { getCodeList } from "../../store/api/common";

import * as ruleActions from "../../store/modules/rule";
import * as ruleDistActions from "../../store/modules/ruleDist";
import IntlMessages from "util/IntlMessages";

class RuleDistDetailList extends Component {
  state = {
    ruleInfoPopup: false,
    rule: {},
    ruleType: []
  };

  /** 수정, 삭제 버튼 */
  onClickUpdate = field => {
    const fieldHist = Map({
      field,
      fieldChgReason: ""
    });
    this.setState({ fieldHist }, () => this.openPopup("updatePopup"));
  };
  onClickDelete = field => {
    const fieldHist = Map({
      field,
      fieldChgReason: ""
    });
    this.setState({ fieldHist }, () => this.openPopup("deletePopup"));
  };

  /** 팝업제어관련 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** rule 정보 클릭 */
  showRuleInfo = async ({ ruleHistSeq }) => {
    const { RuleActions } = this.props;
    const res = await RuleActions.getRuleHistory(ruleHistSeq);
    this.setState({ rule: res.data }, () => this.openPopup("ruleInfoPopup"));
  };

  /** 필드 조회 */
  getLatestRuleDistDetail = () => {
    const { RuleDistActions } = this.props;
    try {
      RuleDistActions.getLatestRuleDistDetail();
    } catch (err) {
      console.warn(err);
    }
  };

  getCodes = async () => {
    try {
      const ruleType = await getCodeList("RULE_TYPE");
      this.setState({
        codes: {
          ruleType: ruleType.data
        }
      });
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    //this.getLatestRuleDistDetail();
    this.getCodes();
  }

  /** 팝업제어 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  /** props setting */
  static defaultProps = {
    ruleDistDetail: []
  };
  static propTypes = {
    groupcd: PropTypes.arrayOf(PropTypes.object)
  };

  render() {
    const { showRuleInfo, closePopup } = this;
    const { ruleInfoPopup, rule, ruleType } = this.state;
    const { ruleDistDetail } = this.props;

    const columns = [
      {
        Header: "No.",
        id: "no",
        accessor: d => Number(d.no),
        style: { textAlign: "center" },
        width: 60
      },
      {
        Header: () => {
          return (
            <React.Fragment>
              <span className="link-style" data-tip data-for="diffYn">
                <IntlMessages id="change.check" />
              </span>
              <ReactTooltip
                id="diffYn"
                className="tooltipDesc"
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={200}
                event="mouseover"
                globalEventOff="mouseout"
              >
                <IntlMessages id="display.rule.prev.version" />
              </ReactTooltip>
            </React.Fragment>
          );
        },
        accessor: "diffYn",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="rule.name" />,
        accessor: "ruleNm",
        style: { textAlign: "left" },
        Cell: ({ value, original }) => {
          const tooltipId = `ruleNmTooltip_${original.no}`;
          return (
            <React.Fragment>
              <span
                className="link-style"
                data-tip
                data-for={tooltipId}
                onClick={() => {
                  showRuleInfo(original);
                }}
              >
                {value}
              </span>
              <ReactTooltip
                id={tooltipId}
                className="tooltipDesc"
                place="left"
                type="dark"
                effect="float"
                scrollHide={false}
                delayShow={100}
                delayHide={200}
                event="mouseover"
                globalEventOff="mouseout"
              >
                {value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      },
      {
        Header: <IntlMessages id="rule.type" />,
        accessor: "ruleType",
        style: { textAlign: "center" },
        width: 80
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "modUsrNm",
        style: { textAlign: "center" },
        width: 80
      },
      {
        Header: <IntlMessages id="mod.datetime" />,
        accessor: "modDateFormatted",
        style: { textAlign: "center" },
        width: 120
      },
      {
        Header: <IntlMessages id="reason.change" />,
        accessor: "ruleChgReason",
        style: { textAlign: "left" },
        Cell: props => {
          const tooltipId = `ruleChgReasonTooltip_${props.original.no}`;
          return (
            <React.Fragment>
              <span data-tip data-for={tooltipId}>
                {props.value}
              </span>
              <ReactTooltip
                className="tooltipClass"
                id={tooltipId}
                place="bottom"
                type="dark"
                effect="float"
              >
                {props.value}
              </ReactTooltip>
            </React.Fragment>
          );
        }
      }
    ];
    return (
      <React.Fragment>
        <GridTable
          columns={columns}
          data={ruleDistDetail}
          sortable
          defaultSorted={[{ id: "modDateFormatted", desc: true }]}
          filterable={false}
          resizable={false}
          className="-highlight"
        />
        {ruleInfoPopup && (
          <RuleInfoPopup
            ruleInfo={rule}
            ruleType={ruleType}
            onClose={() => closePopup("ruleInfoPopup")}
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    ruleDistDetail: state.ruleDist.get("ruleDistDetail")
  }),
  dispatch => ({
    RuleDistActions: bindActionCreators(ruleDistActions, dispatch),
    RuleActions: bindActionCreators(ruleActions, dispatch)
  })
)(RuleDistDetailList);
