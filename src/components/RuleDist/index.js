import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";

import RuleDistHistList from "./RuleDistHistList";
import RuleDistDetailList from "./RuleDistDetailList";

import Loading from "./../Common/Loading";

import * as ruleDistActions from "../../store/modules/ruleDist";

import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class RuleDistHistComponent extends Component {
  state = {
    keyword: "",
    histSeq: "",
    diffYn: false
  };

  onClickRuleDistHis = histSeq =>
    this.setState({ histSeq, keyword: "", diffYn: false }, () =>
      this.getRuleDistDetail()
    );

  /** 룰 조회 */
  getRuleDistDetail = () => {
    const { RuleDistActions } = this.props;
    const { histSeq, keyword, diffYn } = this.state;
    try {
      RuleDistActions.getRuleDistDetail(histSeq, keyword, diffYn ? "Y" : "N");
    } catch (err) {
      console.warn(err);
    }
  };

  /** 검색어 제어 */
  onChangeKeyword = e => this.setState({ keyword: e.target.value });
  onKeyDownKeyword = e => {
    if (e.keyCode === 13) {
      this.getRuleDistDetail();
    }
  };
  /** 검색어 초기화 */
  onClickKeywordClean = () => {
    const { RuleDistActions } = this.props;
    this.setState({ keyword: "", diffYn: false });
    RuleDistActions.getLatestRuleDistDetail();
  };

  /** 검색 클릭 */
  onClickSearch = () => this.getRuleDistDetail();

  /** 체크박스 */
  onChangeCheck = e =>
    this.setState({ diffYn: e.target.checked }, () => this.getRuleDistDetail());

  /** 팝업제어관련 */
  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  render() {
    const {
      onChangeKeyword,
      onClickSearch,
      onClickKeywordClean,
      onKeyDownKeyword,
      onChangeCheck,
      onClickRuleDistHis
    } = this;
    const { loading, loading2 } = this.props;
    const { keyword, diffYn } = this.state;

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper rule-mgmt">
          <LeftMenu />
          <div
            className="component-right"
            style={{ flex: 2, marginRight: "20px" }}
          >
            <div
              className="component rule-list"
              style={{ marginBottom: 0, height: "100%" }}
            >
              <div className="component__title">
                <div
                  className="search-bar"
                  style={{
                    borderBottom: "none",
                    marginBottom: 0,
                    paddingBottom: "9px"
                  }}
                >
                  <FormattedMessage id="rule.name">
                    {ruleName => (
                      <input
                        type="text"
                        className="form-control"
                        name="keyword"
                        placeholder={ruleName}
                        value={keyword}
                        onChange={onChangeKeyword}
                        onKeyDown={onKeyDownKeyword}
                      />
                    )}
                  </FormattedMessage>
                  <div className="binder" />
                  <button
                    className="btn btn-icon small btn--go"
                    onClick={onClickSearch}
                  />
                  <button
                    className="btn btn-icon small btn--filter"
                    onClick={onClickKeywordClean}
                  />
                </div>
                <div className="btns">
                  <div>
                    <div className="checkbox checkbox--dark">
                      <label>
                        <input
                          type="checkbox"
                          checked={diffYn}
                          onChange={onChangeCheck}
                        />
                        <div className="icon" />
                        <span>
                          <IntlMessages id="view.rule.prev.version" />
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <RuleDistDetailList />
            </div>
          </div>
          <div
            className="component component-tree vender-tree"
            style={{
              marginRight: 0,
              height: "calc(100vh - 110px)",
              maxHeight: "970px"
            }}
          >
            <div className="component__title" style={{ borderBottom: "none" }}>
              <IntlMessages id="rule.deploy.hist" />
            </div>
            <div className="component__box">
              <RuleDistHistList
                onClickRow={onClickRuleDistHis}
                onReset={onClickKeywordClean}
              />
            </div>
          </div>
        </div>
        {loading && <Loading />}
        {loading2 && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    loading: state.rule.get("loading"),
    loading2: state.ruleDist.get("loading")
  }),
  dispatch => ({
    RuleDistActions: bindActionCreators(ruleDistActions, dispatch)
  })
)(RuleDistHistComponent);
