import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Select } from "react-inputs-validation";
import Draggable from "react-draggable";

import "./style.scss";

import * as userActions from "./../../../store/modules/user";
import * as api from "./../../../store/api/user";

import SelectPopup from "./../../FilterMgmt/popups/SelectPopup";
import ConfirmPopup from "./../../Common/Popups/CommonConfirmPopup";
import IntlMessages from "util/IntlMessages";
import { FormattedMessage } from "react-intl";

class AddUserPopup extends Component {
  state = {
    usrNo: "",
    usrId: "",
    usrNm: "",
    coNm: "",
    deptNm: "",
    dutyCode: "0",
    accTypeCode: "",
    usrPwd: "",
    usrEmail: "",
    usrMobileNo: "",
    custmSeq: "",
    selectedCoNm: "",

    useTypes: [
      { name: <IntlMessages id="use" />, id: "Y" },
      { name: <IntlMessages id="unused" />, id: "N" }
    ],
    addInitTypes: [{ name: <IntlMessages id="select.options" />, id: "" }],
    accTypeCodeList: this.props.accTypeCodeList || [],
    dutyCodeList: this.props.dutyCodeList || [],

    validation: {
      validate: false,
      hasErrAccTypeCode: true,
      hasErrUsrNm: true,
      hasErrUsrId: true,
      hasErrUsrPwd: true,
      hasErrCoNm: false,
      hasErrDeptNm: false,
      hasErrUsrEmail: false,
      hasErrUsrMobileNo: false
    },
    position: {
      x: 0,
      y: 0
    },
    isCompanyPopup: false,
    isShowConfirmPopup: false,
    confirmPopupResource: null,
    selectPopupType: "companySelectPopup"
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("addUserPopup").offsetWidth;
    const elHeight = document.getElementById("addUserPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  componentWillMount() {
    this.setState({
      accTypeCodeList: this.state.addInitTypes.concat(
        this.state.accTypeCodeList
      ),
      dutyCodeList: this.state.addInitTypes.concat(this.state.dutyCodeList)
    });
  }

  // 팝업 위치 조절
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  // validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  // form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrAccTypeCode,
      hasErrUsrNm,
      hasErrUsrId,
      hasErrUsrPwd,
      hasErrCoNm,
      hasErrDeptNm,
      hasErrDutyCode,
      hasErrUsrEmail,
      hasErrUsrMobileNo
    } = this.state.validation;

    return (
      !hasErrAccTypeCode &&
      !hasErrUsrNm &&
      !hasErrUsrId &&
      !hasErrUsrPwd &&
      !hasErrCoNm &&
      !hasErrDeptNm &&
      !hasErrDutyCode &&
      !hasErrUsrEmail &&
      !hasErrUsrMobileNo
    );
  };

  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      [name]: value
    });
  };

  onClickSave = async e => {
    if (this.isValidate()) {
      const { UserActions } = this.props;
      const inputData = this.state;
      try {
        if (inputData.coNm !== inputData.selectedCoNm) {
          inputData.custmSeq = "";
        }
        await UserActions.createUser(inputData);
        await UserActions.getUsers();
        this.setState({
          usrId: "",
          usrNm: "",
          coNm: "",
          deptNm: "",
          dutyCode: "",
          accTypeCode: "",
          usrPwd: "",
          usrEmail: "",
          usrMobileNo: "",
          custmSeq: "",
          selectedCoNm: ""
        });
        this.props.closePopup();
      } catch (e) {
        console.warn(e);
      }
    }
  };

  // 공통팝업 제어
  openConfirmPopup = resource => {
    this.setState({
      isShowConfirmPopup: true,
      confirmPopupResource: resource
    });
  };
  closeConfirmPopup = () => {
    this.setState({
      isShowConfirmPopup: false
    });
  };

  setCompanyInfo = mapData => {
    [...mapData.values()].map((data, i) =>
      this.setState({
        coNm: data.companyName,
        custmSeq: data.companySeq,
        selectedCoNm: data.companyName
      })
    );
  };
  openSelectCompanyPopup = () => {
    this.setState({
      isCompanyPopup: true
    });
  };

  closePopup = () => {
    this.setState({
      isCompanyPopup: false
    });
  };

  render() {
    const { handleChangeInput } = this;
    const { position, dutyCodeList, accTypeCodeList } = this.state;
    const { closePopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--user popup--move"
            id="addUserPopup"
            style={{ width: "610px", zIndex: 11 }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="user.reg" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="user.division" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Select
                        name="accTypeCode"
                        tabIndex="1"
                        value={this.state.accTypeCode}
                        optionList={accTypeCodeList}
                        onChange={(accTypeCode, e) => {
                          this.setState({
                            accTypeCode
                          });
                        }}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrAccTypeCode: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "AccTypeCode",
                          check: true,
                          required: true,
                          locale: this.props.language
                        }}
                        customStyleWrapper={{ width: "270px" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="name" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="usrNm"
                        type="text"
                        tabIndex="2"
                        value={this.state.usrNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrUsrNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "UsrNm",
                          check: true,
                          required: true,
                          min: 2,
                          max: 32,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "270px", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      ID<span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="usrId"
                        type="text"
                        tabIndex="3"
                        value={this.state.usrId}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrUsrId: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          reg: /^[a-zA-Z0-9_]+$/,
                          regMsg: <IntlMessages id="alphabet.number.only" />,
                          name: "UsrId",
                          check: true,
                          required: true,
                          min: 4,
                          max: 50,
                          locale: this.props.language,
                          customFunc: value => {
                            try {
                              const res = api.isDuplicate(value);
                              if (res.status === 200) {
                                if (res.responseText === "true") {
                                  return <IntlMessages id="already.id.use" />;
                                }
                              } else {
                                return <IntlMessages id="occur.error" />;
                              }
                            } catch (e) {
                              return <IntlMessages id="occur.error" />;
                            }
                            return true;
                          }
                        }}
                        customStyleInput={{ width: "270px", resize: "none" }}
                      />
                    </td>
                  </tr>

                  <tr>
                    <th>
                      <IntlMessages id="password" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <IntlMessages id="input.alphabet.number.specialcharacter.four.fifty" />
                      <Textbox
                        name="usrPwd"
                        type="password"
                        tabIndex="4"
                        value={this.state.usrPwd}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrUsrPwd: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "UsrPwd",
                          check: true,
                          required: true,
                          min: 4,
                          max: 50,
                          reg: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*+=-])[A-Za-z0-9!@#$%^&*+=-]+$/,
                          regMsg: <IntlMessages id="wrong.ps" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="password.confirm" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="usrPwdConfirm"
                        type="password"
                        tabIndex="5"
                        value={this.state.usrPwdConfirm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "UsrPwdConfirm",
                          check: true,
                          required: true,
                          min: 4,
                          max: 50,
                          reg: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*+=-])[A-Za-z0-9!@#$%^&*+=-]+$/,
                          regMsg: <IntlMessages id="wrong.ps" />,
                          compare: this.state.usrPwd,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="company.name" />
                    </th>
                    <td className="inner-btn">
                      <div className="flex-box">
                        <Textbox
                          name="coNm"
                          type="text"
                          tabIndex="6"
                          value={this.state.coNm}
                          onChange={handleChangeInput}
                          onBlur={() => {}}
                          validate={validate}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrCoNm: res,
                                validate: false
                              }
                            });
                          }}
                          validationOption={{
                            name: "CoNm",
                            check: true,
                            required: false,
                            max: 50,
                            locale: this.props.language
                          }}
                          customStyleInput={{ width: "100%", resize: "none" }}
                        />
                        <button
                          onClick={() => this.openSelectCompanyPopup()}
                          className="btn btn-icon"
                          style={{ marginLeft: 5 }}
                        >
                          <img
                            src="/images/common/ic_search.png"
                            alt="ic_search"
                          />
                        </button>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="department.name" />
                    </th>
                    <td>
                      <Textbox
                        name="deptNm"
                        type="text"
                        tabIndex="7"
                        value={this.state.deptNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrDeptNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "DeptNm",
                          check: true,
                          required: false,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="position" />
                    </th>
                    <td>
                      <Select
                        name="dutyCode"
                        tabIndex="8"
                        value={this.state.dutyCode}
                        optionList={dutyCodeList}
                        onChange={(dutyCode, e) => {
                          this.setState({
                            dutyCode
                          });
                        }}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrDutyCode: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "DutyCode",
                          check: true,
                          required: false,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="email" />
                    </th>
                    <td>
                      <Textbox
                        name="usrEmail"
                        type="text"
                        tabIndex="9"
                        value={this.state.usrEmail}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrUsrEmail: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          check: true,
                          required: false,
                          name: "usrEmail",
                          min: 6,
                          max: 255,
                          reg: /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*\.[a-zA-Z]{1,4}$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="mobile.no" />
                    </th>
                    <td>
                      <FormattedMessage id="input.without.hyphen">
                        {placeholder => (
                          <Textbox
                            name="usrMobileNo"
                            type="text"
                            tabIndex="10"
                            placeholder={placeholder}
                            value={this.state.usrMobileNo}
                            onChange={handleChangeInput}
                            onBlur={() => {}}
                            validate={validate}
                            validationCallback={res => {
                              this.setState({
                                validation: {
                                  ...this.state.validation,
                                  hasErrUsrMobileNo: res,
                                  validate: false
                                }
                              });
                            }}
                            validationOption={{
                              name: "UsrMobileNo",
                              check: true,
                              required: false,
                              min: 4,
                              max: 50,
                              reg: /^[0-9]+$/,
                              regMsg: <IntlMessages id="wrong.value" />,
                              locale: this.props.language
                            }}
                            customStyleInput={{ width: "100%", resize: "none" }}
                          />
                        )}
                      </FormattedMessage>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={this.onClickSave}>
                <IntlMessages id="save" />
              </button>
            </div>

            {this.state.isCompanyPopup && (
              <SelectPopup
                openConfirmPopup={this.openConfirmPopup}
                closeConfirmPopup={this.closeConfirmPopup}
                closePopup={this.closePopup}
                type={this.state.selectPopupType}
                selectPopupCB={this.setCompanyInfo}
              />
            )}
            {this.state.isShowConfirmPopup && (
              <ConfirmPopup resource={this.state.confirmPopupResource} />
            )}
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    UserActions: bindActionCreators(userActions, dispatch)
  })
)(AddUserPopup);
