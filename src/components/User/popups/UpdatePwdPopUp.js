import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox } from "react-inputs-validation";
import * as userActions from "../../../store/modules/user";
import IntlMessages from "util/IntlMessages";

class UpdatePwdPopUp extends Component {
  state = {
    usrPwd: "",
    usrPwdCofirm: "",
    usersBySeq: this.props.usersBySeq,
    validation: {
      validate: false,
      hasErrUsrPwd: false,
      hasErrUsrPwdConfirm: true
    }
  };

  handleSaveData = async () => {
    if (this.isValidate()) {
      const { UserActions } = this.props;
      await UserActions.changePwd(
        this.state.usersBySeq.usrNo,
        this.state.usersBySeq
      );
      this.props.handlerAccLock();
      await UserActions.getUsers();
      this.props.closeUpdataPwdPopup();
    }
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate: true
      }
    });

  // form validation
  isValidate = () => {
    this.toggleValidating(true);
    const { hasErrUsrPwd, hasErrUsrPwdConfirm } = this.state.validation;
    return !hasErrUsrPwd && !hasErrUsrPwdConfirm;
  };

  render() {
    const { closeUpdataPwdPopup } = this.props;
    const { usersBySeq } = this.state;
    const { validate } = this.state.validation;
    const { handleSaveData } = this;

    return (
      <React.Fragment>
        <div
          className="popup popup--move"
          id="updatePwdPopup"
          style={{ width: "450px" }}
        >
          <div className="popup__header">
            <h5>
              <IntlMessages id="password.edit" />
            </h5>
            <button className="btn btn-close" onClick={closeUpdataPwdPopup} />
          </div>
          <div className="popup__body">
            <table className="table table--info">
              <tbody>
                <tr>
                  <th>
                    <IntlMessages id="name" />
                  </th>
                  <td>
                    <Textbox
                      name="usrNm"
                      type="text"
                      id="_usrNm"
                      value={usersBySeq.usrNm}
                      disabled
                      customStyleInput={{ width: "250px", resize: "none" }}
                    />
                  </td>
                </tr>
                <tr>
                  <th>ID</th>
                  <td>
                    <Textbox
                      name="usrId"
                      type="text"
                      id="_usrId"
                      value={usersBySeq.usrId}
                      disabled
                      customStyleInput={{ width: "250px", resize: "none" }}
                    />
                  </td>
                </tr>
                <tr>
                  <th>
                    <IntlMessages id="password" />
                    <span className="font--red">*</span>
                  </th>
                  <td>
                    <IntlMessages id="input.alphabet.number.specialcharacter.four.fifty" />
                    <Textbox
                      name="usrPwd"
                      type="password"
                      id="usrPwd"
                      value={usersBySeq.usrPwd}
                      onChange={(usrPwd, e) => {
                        this.setState({
                          usersBySeq: {
                            ...this.state.usersBySeq,
                            usrPwd
                          }
                        });
                      }}
                      onBlur={() => {}}
                      validate={validate}
                      validationCallback={res => {
                        this.setState({
                          validation: {
                            ...this.state.validation,
                            hasErrUsrPwd: res,
                            validate: false
                          }
                        });
                      }}
                      validationOption={{
                        name: "UsrPwd",
                        check: true,
                        required: true,
                        min: 4,
                        max: 255,
                        reg: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*+=-])[A-Za-z0-9!@#$%^&*+=-]+$/,
                        regMsg: <IntlMessages id="wrong.ps" />,
                        locale: this.props.language
                      }}
                      customStyleInput={{ width: "250px", resize: "none" }}
                    />
                  </td>
                </tr>
                <tr>
                  <th>
                    <IntlMessages id="password.confirm" />
                    <span className="font--red">*</span>
                  </th>
                  <td className="inner-btn">
                    <div className="flex-box">
                      <Textbox
                        name="usrPwdConfirm"
                        type="password"
                        id="usrPwdConfirm"
                        value={this.state.usrPwdConfirm}
                        onChange={(usrPwdConfirm, e) => {
                          this.setState({
                            usrPwdConfirm
                          });
                        }}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrUsrPwdConfirm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "UsrPwdConfirm",
                          check: true,
                          required: true,
                          min: 4,
                          max: 255,
                          reg: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*+=-])[A-Za-z0-9!@#$%^&*+=-]+$/,
                          regMsg: <IntlMessages id="wrong.ps" />,
                          compare: usersBySeq.usrPwd,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "250px", resize: "none" }}
                      />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td />
                  <td>
                    <p />
                    <IntlMessages id="resetting.password.unlock" />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={closeUpdataPwdPopup}>
              <IntlMessages id="cancel" />
            </button>
            <button className="btn btn--dark" onClick={handleSaveData}>
              <IntlMessages id="save" />
            </button>
          </div>
        </div>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    usersBySeq: state.user.get("usersBySeq")
  }),
  dispatch => ({
    UserActions: bindActionCreators(userActions, dispatch)
  })
)(UpdatePwdPopUp);
