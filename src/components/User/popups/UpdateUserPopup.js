import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Select } from "react-inputs-validation";
import * as userActions from "../../../store/modules/user";
import Draggable from "react-draggable";
import UpdatePwdPopUp from "./UpdatePwdPopUp";

import SelectPopup from "./../../FilterMgmt/popups/SelectPopup";
import ConfirmPopup from "./../../Common/Popups/CommonConfirmPopup";
import IntlMessages from "util/IntlMessages";
import { FormattedMessage } from "react-intl";

import "./style.scss";

class UpdateUserPopup extends Component {
  state = {
    usrPwdConfirm: "",
    useType: "Y",
    usersBySeq: this.props.data,
    useTypes: [
      { name: <IntlMessages id="use" />, id: "Y" },
      { name: <IntlMessages id="unused" />, id: "N" }
    ],
    accTypeCodeList: this.props.accTypeCodeList || [],
    dutyCodeList: this.props.dutyCodeList || [],

    validation: {
      validate: false,
      hasErrAccTypeCode: false,
      hasErrUsrNm: false,
      hasErrUsrPwd: false,
      hasErrCoNm: false,
      hasErrDeptNm: false,
      hasErrUsrEmail: false,
      hasErrUsrMobileNo: false
    },
    position: {
      x: 0,
      y: 0
    },
    selectedCoNm: this.props.data.coNm,
    isUpdatePwdPopUp: false,
    isCompanyPopup: false,
    isShowConfirmPopup: false,
    confirmPopupResource: null,
    selectPopupType: "companySelectPopup"
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("updateUserPopup").offsetWidth;
    const elHeight = document.getElementById("updateUserPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  // 팝업 위치 조절
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  // validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  // form validation
  isValidate = () => {
    // validate를 true로 설정 시 모든 form<IntlMessages id="of" />유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);

    const {
      hasErrAccTypeCode,
      hasErrUsrNm,
      hasErrCoNm,
      hasErrDeptNm,
      hasErrDutyCode,
      hasErrUsrEmail,
      hasErrUsrMobileNo
    } = this.state.validation;

    return (
      !hasErrAccTypeCode &&
      !hasErrUsrNm &&
      !hasErrCoNm &&
      !hasErrDeptNm &&
      !hasErrDutyCode &&
      !hasErrUsrEmail &&
      !hasErrUsrMobileNo
    );
  };

  handleSaveData = async () => {
    if (this.isValidate()) {
      const { UserActions } = this.props;
      const inputData = this.state.usersBySeq;
      try {
        if (inputData.coNm !== inputData.selectedCoNm) {
          inputData.custmSeq = "";
        }
        await UserActions.updateUser(inputData);
        await UserActions.getUsers();
        this.props.closePopup();
      } catch (e) {
        console.warn(e);
      }
    }
  };

  // State <IntlMessages id="change" /> handler
  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      usersBySeq: {
        ...this.state.usersBySeq,
        [name]: value
      }
    });
  };

  updatePwdPopUp = () => {
    this.setState({
      isUpdatePwdPopUp: true
    });
  };

  closeUpdataPwdPopup = () => {
    this.setState({
      isUpdatePwdPopUp: false
    });
  };

  // 공통팝업 제어
  openConfirmPopup = resource => {
    this.setState({
      isShowConfirmPopup: true,
      confirmPopupResource: resource
    });
  };
  closeConfirmPopup = () => {
    this.setState({
      isShowConfirmPopup: false
    });
  };
  setCompanyInfo = mapData => {
    let map = { companyName: "", companySeq: "" };
    [...mapData.values()].map(
      (data, i) =>
        (map = {
          companyName: data.companyName,
          companySeq: data.companySeq
        })
    );
    this.setState({
      usersBySeq: {
        ...this.state.usersBySeq,
        coNm: map.companyName,
        custmSeq: map.companySeq,
        selectedCoNm: map.companyName
      }
    });
  };
  openSelectCompanyPopup = () => {
    this.setState({
      isCompanyPopup: true
    });
  };

  isAccLockYn = value => {
    if (value === "Y") {
      return (
        <font color="red">
          <IntlMessages id="acct.locked" />
        </font>
      );
    } else {
      return;
    }
  };

  // 계정 잠김 state Handler
  handlerAccLock = () => {
    this.setState({
      usersBySeq: {
        ...this.state.usersBySeq,
        accLockYn: "N"
      }
    });
  };

  // 팝업 <IntlMessages id="close" />
  closePopup = () => {
    this.setState({
      isCompanyPopup: false
    });
  };

  render() {
    const { handleSaveData, handleChangeInput, updatePwdPopUp } = this;
    const { position, usersBySeq, accTypeCodeList, dutyCodeList } = this.state;
    const { closePopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--user popup--move"
            id="updateUserPopup"
            style={{ width: "550px" }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="user.edit" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th style={{ width: "30%" }}>
                      <IntlMessages id="user.division" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Select
                        name="userDnct"
                        tabIndex="1"
                        value={usersBySeq.accTypeCode}
                        optionList={accTypeCodeList}
                        onChange={(accTypeCode, e) => {
                          this.setState({
                            usersBySeq: {
                              ...this.state.usersBySeq,
                              accTypeCode
                            }
                          });
                        }}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrAccTypeCode: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "AccTypeCode",
                          check: true,
                          required: true,
                          locale: this.props.language
                        }}
                        customStyleWrapper={{ width: "200px", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="name" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="usrNm"
                        id="usrNm"
                        type="text"
                        tabIndex="2"
                        value={usersBySeq.usrNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrUsrNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "UsrNm",
                          check: true,
                          required: true,
                          min: 2,
                          max: 32,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "200px", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      ID<span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="usrId"
                        id="usrId"
                        type="text"
                        value={usersBySeq.usrId}
                        disabled
                        customStyleInput={{ width: "200px", resize: "none" }}
                      />
                      {this.isAccLockYn(usersBySeq.accLockYn)}
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="password" />
                      <span className="font--red">*</span>
                    </th>
                    <td className="inner-btn">
                      <div className="flex-box">
                        <Textbox
                          name="usrPwd"
                          value="**********"
                          disabled
                          customStyleInput={{ width: "200px", resize: "none" }}
                        />
                        <button
                          className="btn"
                          onClick={updatePwdPopUp}
                          style={{ verticalAlign: "top" }}
                        >
                          <IntlMessages id="password.edit" />
                        </button>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="company.name" />
                    </th>
                    <td className="inner-btn">
                      <div className="flex-box">
                        <Textbox
                          name="coNm"
                          id="coNm"
                          type="text"
                          tabIndex="3"
                          value={usersBySeq.coNm}
                          onChange={handleChangeInput}
                          onBlur={() => {}}
                          validate={validate}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrCoNm: res,
                                validate: false
                              }
                            });
                          }}
                          validationOption={{
                            name: "CoNm",
                            check: true,
                            required: false,
                            max: 50,
                            locale: this.props.language
                          }}
                          customStyleInput={{ width: "100%", resize: "none" }}
                        />
                        <button
                          onClick={() => this.openSelectCompanyPopup()}
                          className="btn btn-icon"
                          style={{ marginLeft: 5 }}
                        >
                          <img
                            src="/images/common/ic_search.png"
                            alt="ic_search"
                          />
                        </button>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="department.name" />
                    </th>
                    <td>
                      <Textbox
                        name="deptNm"
                        id="deptNm"
                        type="text"
                        tabIndex="4"
                        value={usersBySeq.deptNm}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrDeptNm: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "DeptNm",
                          check: true,
                          required: false,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="position" />
                    </th>
                    <td>
                      <Select
                        name="dutyCode"
                        tabIndex="5"
                        value={usersBySeq.dutyCode}
                        optionList={dutyCodeList}
                        onChange={(dutyCode, e) => {
                          this.setState({
                            usersBySeq: {
                              ...this.state.usersBySeq,
                              dutyCode
                            }
                          });
                        }}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrDutyCode: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "DutyCode",
                          check: true,
                          required: false,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleWrapper={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="email" />
                    </th>
                    <td>
                      <Textbox
                        name="usrEmail"
                        id="usrEmail"
                        type="text"
                        tabIndex="6"
                        value={usersBySeq.usrEmail}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrUsrEmail: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          check: true,
                          required: false,
                          name: "usrEmail",
                          min: 6,
                          max: 255,
                          reg: /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*\.[a-zA-Z]{1,4}$/,
                          regMsg: <IntlMessages id="wrong.value" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="mobile.no" />
                    </th>
                    <td>
                      <FormattedMessage id="input.without.hyphen">
                        {placeholder => (
                          <Textbox
                            name="usrMobileNo"
                            id="usrMobileNo"
                            type="text"
                            tabIndex="7"
                            value={usersBySeq.usrMobileNo}
                            placeholder={placeholder}
                            onChange={handleChangeInput}
                            onBlur={() => {}}
                            validate={validate}
                            validationCallback={res => {
                              this.setState({
                                validation: {
                                  ...this.state.validation,
                                  hasErrUsrMobileNo: res,
                                  validate: false
                                }
                              });
                            }}
                            validationOption={{
                              name: "UsrMobileNo",
                              check: true,
                              required: false,
                              min: 4,
                              max: 20,
                              reg: /^[0-9]+$/,
                              regMsg: <IntlMessages id="wrong.value" />,
                              locale: this.props.language
                            }}
                            customStyleInput={{ width: "100%", resize: "none" }}
                          />
                        )}
                      </FormattedMessage>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={handleSaveData}>
                <IntlMessages id="save" />
              </button>
            </div>
            {this.state.isUpdatePwdPopUp && (
              <UpdatePwdPopUp
                data={usersBySeq}
                updatePwdPopUp={this.updatePwdPopUp}
                handlerAccLock={this.handlerAccLock}
                closeUpdataPwdPopup={this.closeUpdataPwdPopup}
              />
            )}
            {this.state.isCompanyPopup && (
              <SelectPopup
                openConfirmPopup={this.openConfirmPopup}
                closeConfirmPopup={this.closeConfirmPopup}
                closePopup={this.closePopup}
                type={this.state.selectPopupType}
                selectPopupCB={this.setCompanyInfo}
              />
            )}
            {this.state.isShowConfirmPopup && (
              <ConfirmPopup resource={this.state.confirmPopupResource} />
            )}
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    usersBySeq: state.user.get("usersBySeq"),
    language: state.common.get("language")
  }),
  dispatch => ({
    UserActions: bindActionCreators(userActions, dispatch)
  })
)(UpdateUserPopup);
