import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../../store/modules/user";
import { removeUsers } from "../../store/api/user";
import DeleteConfirmPopup from "./popups/DeleteConfirmPopup";
import DeleteConfirmListPopup from "./popups/DeleteConfirmListPopup";
import { getCodeList } from "./../../store/api/common";
import Loading from "./../Common/Loading";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import AddUserPopup from "./popups/AddUserPopup";
import UpdateUserPopup from "./popups/UpdateUserPopup";
import GridTable from "../Common/GridTable";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class UserComponent extends Component {
  state = {
    isUserPopup: false,
    isUpdateUserPopup: false,
    isDeleteConfirmPopup: false,
    isDeleteConfirmListPopup: false,
    showConfirm: false,

    selectAll: false,
    resource: {},
    checked: [],
    paramList: [],
    accTypeCodeList: [],
    dutyCodeList: [],
    custmGroup: this.props.custmGroup,
    coNmByCustmGroupSeq: this.props.coNmByCustmGroupSeq,
    usrNo: "",
    coNm: "",
    usrId: "",
    usrNm: "",
    deptNm: "",
    dutyCode: "",
    accTypeCode: "",
    usrPwd: "",
    usrEmail: "",
    usrMobileNo: ""
  };

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { users } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < users.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < users.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { users } = this.props;
    let checkArr = this.state.checked;
    if (checkArr.length === 0) {
      for (let i = 0; i < users.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  // CheckBox 옵션삭제 Handler
  onCheckDeleteHandler = e => {
    const { users } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];

    if (checkArr.length === 0) {
      this.showAlert(
        <IntlMessages id="confirm" />,
        <IntlMessages id="no.selection" />
      );
    } else {
      let stdExists = false;
      for (let idx = 0; idx < checkArr.length; idx++) {
        if (checkArr[idx]) {
          paramList.push(users.content[idx]);
          stdExists = true;
        }
      }
      if (stdExists) {
        this.setState({
          paramList: paramList,
          isDeleteConfirmListPopup: true
        });
      } else {
        this.showAlert(
          <IntlMessages id="confirm" />,
          <IntlMessages id="no.selection" />
        );
      }
    }
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          { name: <IntlMessages id="confirm" />, CB: () => this.onClosePopup() }
        ]
      }
    });
  };

  onClosePopup = () => {
    this.setState({ showConfirm: false });
  };

  //checkbox (선택)삭제 확인
  onDeleteListConfirm = async () => {
    const { UserActions } = this.props;
    const { paramList } = this.state;
    let rx = "";
    rx = await removeUsers(paramList);
    await UserActions.getUsers();
    if (rx.status === 200) {
      this.closePopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: []
      });
    } else {
      this.closePopup();
    }
  };

  // Users 조회
  getUsers = async () => {
    const { UserActions } = this.props;
    try {
      await UserActions.getUsers();
    } catch (e) {
      console.log(e);
    }
  };

  setSelectItems = async () => {
    try {
      const accTypeCodeList = await getCodeList("ATCD");
      const dutyCodeList = await getCodeList("MBPO");
      this.setState({
        accTypeCodeList: accTypeCodeList.data,
        dutyCodeList: dutyCodeList.data
      });
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    this.getUsers();
    // this.getCompanyGoup();
    this.setSelectItems();
  }

  openUserPopup = () => {
    this.setState({
      isUserPopup: true
    });
  };

  // 팝업 닫기
  closePopup = () => {
    this.setState({
      isUserPopup: false,
      isDeleteConfirmPopup: false,
      isDeleteConfirmListPopup: false,
      isUpdateUserPopup: false,

      selectAll: false,
      checked: [],
      paramList: []
    });
  };

  // 수정 팝업
  updateUserPopup = usrNo => {
    const { UserActions } = this.props;
    const ajax = UserActions.getUsersBySeq(usrNo);
    ajax.then(() => {
      this.setState({
        isUpdateUserPopup: true
      });
    });
  };

  // 삭제 팝업
  deleteConfirmPopup = usrNo => {
    const { UserActions } = this.props;
    const ajax = UserActions.getUsersBySeq(usrNo);
    ajax.then(() => {
      this.setState({
        isDeleteConfirmPopup: true
      });
    });
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ users: nextProps.users });
  }

  render() {
    const {
      chkBoxSingleChange,
      chkBoxAllChange,
      onDeleteListConfirm,
      onCheckDeleteHandler
    } = this;
    const {
      isUserPopup,
      isUpdateUserPopup,
      isDeleteConfirmPopup,
      isDeleteConfirmListPopup,
      dutyCodeList,
      accTypeCodeList,
      showConfirm
    } = this.state;

    const { users, usersBySeq } = this.props;

    const columns_user = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50,
        sortable: false
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        sortable: false,
        width: 40
      },
      {
        Header: <IntlMessages id="company" />,
        accessor: "coNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: "ID",
        accessor: "usrId",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="name" />,
        accessor: "usrNm",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="department" />,
        accessor: "deptNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="position" />,
        accessor: "dutyName",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="user.division" />,
        accessor: "accTypeName",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "modUsrNm",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="mod.date" />,
        accessor: "stringModDate",
        style: { textAlign: "center" },
        width: 150,
        filterable: true
      },
      {
        Header: "Action",
        accessor: "icon",
        style: { textAlign: "center" },
        width: 70,
        sortable: false,
        Cell: row => {
          return (
            <div>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.deleteConfirmPopup(row.original.usrNo);
                }}
              >
                <img src="/images/common/icon_delete.png" alt="icon_delete" />
              </button>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.updateUserPopup(row.original.usrNo);
                }}
              >
                <img src="/images/common/icon_edit.png" alt="icon_edit" />
              </button>
            </div>
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper user">
          <LeftMenu />
          <div className="component component-list">
            <div className="component__title">
              <span>
                <IntlMessages id="user" />
              </span>
              <div className="btns">
                <button className="btn" onClick={onCheckDeleteHandler}>
                  <IntlMessages id="multi.delete" />
                </button>
                <button className="btn btn--blue" onClick={this.openUserPopup}>
                  <IntlMessages id="new.reg" />
                </button>
              </div>
            </div>

            <GridTable
              columns={columns_user}
              data={users.content}
              className="-striped -highlight"
              // onFetchData={this.getUsers}
              filterable={false}
              resizable={false}
              getTrProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: async e => {
                    chkBoxSingleChange(rowInfo.index);
                  }
                };
              }}
            />
          </div>
          {isUserPopup && (
            <AddUserPopup
              closePopup={this.closePopup}
              accTypeCodeList={accTypeCodeList}
              dutyCodeList={dutyCodeList}
            />
          )}
          {isUpdateUserPopup && (
            <UpdateUserPopup
              data={usersBySeq}
              closePopup={this.closePopup}
              accTypeCodeList={accTypeCodeList}
              dutyCodeList={dutyCodeList}
            />
          )}
          {isDeleteConfirmPopup && (
            <DeleteConfirmPopup
              data={usersBySeq}
              closePopup={this.closePopup}
            />
          )}
          {isDeleteConfirmListPopup && (
            <DeleteConfirmListPopup
              closePopup={this.closePopup}
              onDeleteListConfirm={onDeleteListConfirm}
            />
          )}
        </div>
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    users: state.user.get("users"),
    loading: state.user.get("loading"),
    custmGroup: state.user.get("custmGroup"),
    usersBySeq: state.user.get("usersBySeq"),
    coNmByCustmGroupSeq: state.user.get("coNmByCustmGroupSeq")
  }),
  dispatch => ({
    UserActions: bindActionCreators(userActions, dispatch)
  })
)(UserComponent);
