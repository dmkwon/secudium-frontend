import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import { PAGE_ACTION_TYPE } from "store/modules/nmlzRule";

import { Select, Textbox } from "react-inputs-validation";
import GridTable from "components/Common/GridTable";
import Draggable from "react-draggable";

import IntlMessages from "util/IntlMessages";

import AceEditor from "react-ace";
import "brace/mode/json";
import "brace/theme/monokai";

import app from "../regexp/app";
import "../regexp/sass/regexp.scss";

const useYnOpt = [
  { id: "Y", name: <IntlMessages id="use" /> },
  { id: "N", name: <IntlMessages id="unused" /> }
];

class NmlzRuleHdrDetailPopup extends React.Component {
  state = {
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount() {
    const elWidth = document.getElementById("hdrPopup").offsetWidth;
    const elHeight = document.getElementById("hdrPopup").offsetHeight;
    this.setState(
      { position: { x: elWidth / 2, y: (elHeight / 2) * -1 } },
      () => {}
    );

    if (this.props.nmlzRule.logFormatCode === "RGX") {
      const { NmlzRuleActions } = this.props;
      NmlzRuleActions.setMatchInfo([]);
      app.init(false, ".container", ".result");
      app.expression.setValue(this.props.nmlzRuleHdr.nmlzRuleHdrRegexp);
      app.text.setValue(this.props.nmlzRuleHdr.nmlzRuleHdrSampLog);
      app.expression.on("change", () => this.changeExpression());
      app.text.on("change", () => this.changeText());
      app.on("result", () => this.updateResult());
    }
  }

  componentWillUnmount() {
    app.cleanup();
  }

  closeComposeDialog = () => {
    const { NmlzRuleActions } = this.props;
    this.props.hdrPopup.type === PAGE_ACTION_TYPE.EDIT
      ? NmlzRuleActions.closeEditHdrPopup(this.props.hdrPopup.data)
      : NmlzRuleActions.closeNewHdrPopup(this.props.hdrPopup.data);
  };

  handleChange = (value, event) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleHdr({
      name: event.target.name,
      value: event.target.type === "checkbox" ? event.target.checked : value
    });
    NmlzRuleActions.canBeHdrSave();
  };

  handleItemChange = (value, name) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleHdr({
      name: name,
      value: value
    });
    NmlzRuleActions.canBeHdrSave();
  };

  onChange = newValue => {
    const { NmlzRuleActions } = this.props;
    try {
      NmlzRuleActions.editNmlzRuleHdr({
        name: "nmlzRuleHdrSampLog",
        value: newValue
      });
      NmlzRuleActions.canBeHdrSave();
    } catch (e) {
      if (e instanceof SyntaxError) {
        console.error("err", e);
        NmlzRuleActions.editNmlzRuleHdr({
          name: "nmlzRuleHdrSampLog",
          value: ""
        });
        NmlzRuleActions.canBeHdrSave();
      }
    }
  };

  formatJSON = (input, space) => {
    if (input.length === 0) {
      return "";
    } else {
      try {
        var parsedData = JSON.parse(input);
        return JSON.stringify(parsedData, null, space);
      } catch (err) {
        console.error(err);
        return input;
      }
    }
  };

  beautify = () => {
    const { NmlzRuleActions, nmlzRuleHdr } = this.props;
    NmlzRuleActions.editNmlzRuleHdr({
      name: "nmlzRuleHdrSampLog",
      value: this.formatJSON(nmlzRuleHdr.nmlzRuleHdrSampLog, 2)
    });
    NmlzRuleActions.canBeHdrSave();
  };

  changeExpression() {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleHdr({
      name: "nmlzRuleHdrRegexp",
      value: app.expression.getPattern()
    });
    NmlzRuleActions.canBeHdrSave();
  }

  changeText() {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleHdr({
      name: "nmlzRuleHdrSampLog",
      value: app.text.getValue()
    });
    NmlzRuleActions.canBeHdrSave();
  }

  updateResult() {
    let result = app.getResult();
    if (!result.error) {
      const { NmlzRuleActions } = this.props;
      NmlzRuleActions.setMatchInfo(app.getResult().matches);
    }
  }

  getMatchVal(match, str) {
    let val =
      match.s || (match.i === undefined ? "" : str.substr(match.i, match.l));
    return val;
  }

  onHdrSave = () => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.saveNmlzRuleHdr();
    NmlzRuleActions.canBeSubmit();
  };

  /**
   * Drag cb func
   */
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { handleChange, handleItemChange } = this;

    const { position } = this.state;

    const {
      useYn,
      nmlzRuleHdrSampLog,
      nmlzRuleHdrPrefix,
      nmlzRuleHdrKeyField
    } = this.props.nmlzRuleHdr;

    const { hdrPopup } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--dark popup--select" id="hdrPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                {hdrPopup.type === "add" ? (
                  <IntlMessages id="header.rule.create" />
                ) : (
                  <IntlMessages id="header.rule.edit" />
                )}
              </h5>
              <button
                className="btn btn-close"
                onClick={() => {
                  this.closeComposeDialog();
                }}
              />
            </div>
            <div className="popup__body">
              <div className="component__title">
                <span>
                  <IntlMessages id="header.rule" />
                </span>
              </div>
              <div className="component hdr">
                <div className="form-group" style={{ marginBottom: "10px" }}>
                  <span>
                    <IntlMessages id="use.yn" />
                    <span className="font--red" style={{ color: "red" }}>
                      *
                    </span>
                  </span>
                  <Select
                    name={"useYn"}
                    value={useYn}
                    optionList={useYnOpt}
                    onChange={(useYn, e) => {
                      handleItemChange(useYn, "useYn");
                    }}
                    customStyleWrapper={{
                      width: "100px",
                      height: "24px",
                      fontSize: "12px",
                      marginLeft: "17px"
                    }}
                  />
                </div>
                <div className="flex-box">
                  {this.props.nmlzRule.logFormatCode === "JSON" && (
                    <React.Fragment>
                      <div className="left__json">
                        <div className="component__title">
                          <span>
                            <IntlMessages id="sample.log" />
                          </span>
                          <div className="btns">
                            <button
                              className="btn btn--blue"
                              onClick={() => {
                                this.beautify();
                              }}
                            >
                              beautify
                            </button>
                          </div>
                        </div>
                        <div
                          className="component hdr json-parser"
                          style={{ height: 260, padding: 0 }}
                        >
                          <AceEditor
                            mode="json"
                            theme="monokai"
                            value={nmlzRuleHdrSampLog}
                            onChange={this.onChange}
                            name="json-hr"
                            showPrintMargin={false}
                            editorProps={{ $blockScrolling: true }}
                            width={"100%"}
                            height={"300px"}
                          />
                        </div>
                        <div style={{ marginTop: 20 }}>
                          <table className="table table--dark table--info">
                            <colgroup>
                              <col width="150px" />
                              <col />
                            </colgroup>
                            <tbody>
                              <tr>
                                <th>Log prefix string</th>
                                <td>
                                  <Textbox
                                    name="nmlzRuleHdrPrefix"
                                    type="text"
                                    value={nmlzRuleHdrPrefix}
                                    onChange={handleChange}
                                    classNameInput="form-control"
                                  />
                                </td>
                              </tr>
                              <tr>
                                <th>Header Key Field</th>
                                <td>
                                  <Textbox
                                    name="nmlzRuleHdrKeyField"
                                    type="text"
                                    value={nmlzRuleHdrKeyField}
                                    onChange={handleChange}
                                    classNameInput="form-control"
                                  />
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </React.Fragment>
                  )}
                  {this.props.nmlzRule.logFormatCode === "RGX" && (
                    <React.Fragment>
                      <div className="left__regex">
                        <div className="component__title">
                          <span>
                            <IntlMessages id="regex" />
                          </span>
                        </div>
                        <div className="container">
                          <div className="app">
                            <div className="doc">
                              <div className="blocker" />
                              <section className="expression">
                                <header>
                                  <h1>
                                    <IntlMessages id="header.rule" />
                                  </h1>
                                </header>
                                <article className="editor" />
                              </section>

                              <section className="text">
                                <header>
                                  <h1>
                                    <IntlMessages id="log.sample" />
                                  </h1>
                                  <div className="controlbar">
                                    <div className="control result">
                                      No match.
                                    </div>
                                  </div>
                                </header>
                                <article className="editor multiline">
                                  <div className="pad">
                                    <textarea className="inputc" />
                                  </div>
                                </article>
                              </section>
                            </div>
                          </div>
                        </div>
                        <table className="table table--dark table--info">
                          <colgroup>
                            <col width="150px" />
                            <col />
                          </colgroup>
                          <tbody>
                            <tr>
                              <th>Log prefix string</th>
                              <td>
                                <Textbox
                                  name="nmlzRuleHdrPrefix"
                                  type="text"
                                  value={nmlzRuleHdrPrefix}
                                  onChange={handleChange}
                                  classNameInput="form-control"
                                />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                      <div className="right__regex">
                        <div className="component__title">
                          <span>
                            <IntlMessages id="matching.info" />
                          </span>
                        </div>
                        <GridTable
                          className="-striped -highlight"
                          data={this.props.matchInfo}
                          columns={[
                            {
                              Header: "No.",
                              id: "row",
                              accessor: "number",
                              style: { textAlign: "center" },
                              width: 50,
                              Cell: row => {
                                return <div>{row.original.num + 1}</div>;
                              }
                            },
                            {
                              Header: <IntlMessages id="matching.info" />,
                              accessor: "result",
                              width: 660,
                              Cell: row => {
                                if (row) {
                                  return (
                                    <div>
                                      {this.getMatchVal(
                                        row.original,
                                        nmlzRuleHdrSampLog
                                      )}
                                    </div>
                                  );
                                }
                              }
                            }
                          ]}
                          manual
                          showPagination={false}
                          showPaginationOptions={false}
                          style={{
                            height: "320px"
                          }}
                        />
                      </div>
                    </React.Fragment>
                  )}
                </div>
                <div className="btns" style={{ textAlign: "right" }}>
                  <button
                    className="btn"
                    onClick={() => {
                      if (this.props.canBeHdrSave) {
                        const { NmlzRuleActions } = this.props;
                        NmlzRuleActions.openCancelConfirmPopup({
                          from: "nmlzRuleHdr"
                        });
                      } else {
                        this.closeComposeDialog();
                      }
                    }}
                  >
                    <IntlMessages id="cancel" />
                  </button>
                  <button
                    className="btn btn--blue"
                    disabled={!this.props.canBeHdrSave}
                    onClick={() => {
                      this.onHdrSave();
                      this.closeComposeDialog();
                    }}
                  >
                    <IntlMessages id="header.rule.save" />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Draggable>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    hdrPopup: state.nmlzRule.get("hdrPopup"),
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    nmlzRuleHdr: state.nmlzRule.get("nmlzRuleHdr"),
    canBeHdrSave: state.nmlzRule.get("canBeHdrSave"),
    matchInfo: state.nmlzRule.get("matchInfo")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch)
  })
)(NmlzRuleHdrDetailPopup);
