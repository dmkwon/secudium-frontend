import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import * as nmlzRuleActions from "store/modules/nmlzRule";

import IntlMessages from "util/IntlMessages";

class DuplicatePopup extends Component {
  render() {
    return (
      <div>
        <div className="popup popup--alert" style={{ width: "400px" }}>
          <div className="popup__header">
            <h5>
              <IntlMessages id="confirm" />
            </h5>
            <button
              className="btn btn-close"
              onClick={() => {
                this.props.closeDuplicatePopup();
              }}
            />
          </div>
          <div className="popup__body">
            <IntlMessages id="exist.nrmlz.rule" />
          </div>
          <div className="popup__footer">
            <button
              className="btn btn--white"
              onClick={() => {
                this.props.closeDuplicatePopup();
              }}
            >
              <IntlMessages id="confirm" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => {
    return bindActionCreators(
      {
        closeDuplicatePopup: nmlzRuleActions.closeDuplicatePopup
      },
      dispatch
    );
  }
)(DuplicatePopup);
