import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import * as comCodeActions from "store/modules/code";

import Draggable from "react-draggable";
import { Select, Textbox } from "react-inputs-validation";

import _ from "@lodash";
import GlobalUtils from "util/GlobalUtils";
import IntlMessages from "util/IntlMessages";

const fieldFuncState = {
  nmlzRuleFieldFuncSeq: -1,
  nmlzRuleContSeq: -1,
  field: "",
  fieldId: "",
  fieldFuncCode: "",
  fieldFuncCodeNm: "",
  fieldFuncArg: "",
  fieldFuncArgCode: "",
  printFieldId: -1,
  printField: ""
};

const initState = {
  form: { ...fieldFuncState },
  funcOpt: [],
  argument: "",
  argumentOpt: [],
  editable: false,
  fieldFuncCustomArg: "",
  argumentStr: {
    value: "",
    disabled: false
  },
  validation: {
    validate: false,
    hasErrFieldId: true,
    hasErrFunc: true,
    hasErrArgument: true,
    hasErrPrintField: true
  },
  position: {
    x: 0,
    y: 0
  }
};

let fieldOpt = [];

class FieldFuncPopup extends React.Component {
  argumentRef = React.createRef();

  state = { ...initState };

  getFieldFunc = async () => {
    const { ComCodeActions } = this.props;

    let funcOpt = await ComCodeActions.getCodesByParentCode("FIELD_FUNC");
    let newFuncOpt = funcOpt.data.map(func => {
      return { id: func.commCode, name: func.commCodeNm };
    });
    this.setState({
      funcOpt: newFuncOpt
    });
  };

  getFieldFuncArguments = async code => {
    const { ComCodeActions } = this.props;

    let argumentOpt = await ComCodeActions.getCodesByParentCode(code);
    let newArgumentOpt = argumentOpt.data.map(argument => {
      return { id: argument.commCode, name: argument.commCodeNm };
    });
    this.setState({
      argumentOpt: newArgumentOpt
    });
  };

  componentDidMount() {
    const elWidth = document.getElementById("fieldFuncPopup").offsetWidth;
    const elHeight = document.getElementById("fieldFuncPopup").offsetHeight;
    this.setState(
      { position: { x: elWidth / 2, y: (elHeight / 2) * -1 } },
      () => {}
    );

    if (this.props.fieldFuncPopup.type === "new") {
      fieldOpt = this.props.nmlzRuleCont.nmlzRuleFields.map(field => {
        return { id: field.fieldId, name: field.field };
      });
      fieldFuncState.nmlzRuleContSeq = this.props.fieldFuncPopup.data;
      fieldFuncState.nmlzRuleFieldFuncSeq = GlobalUtils.ID();
      this.setState({
        form: { ...fieldFuncState }
      });
      this.getFieldFunc();
    } else if (this.props.fieldFuncPopup.type === "edit") {
      fieldOpt = this.props.nmlzRuleCont.nmlzRuleFields.map(field => {
        return { id: field.fieldId, name: field.field };
      });
      this.getFieldFunc();
      this.changeArgumentOpt(this.props.fieldFuncPopup.data.fieldFuncCode);
      this.setState({
        form: { ...this.props.fieldFuncPopup.data },
        validation: {
          validate: false,
          hasErrFieldId: false,
          hasErrFunc: false,
          hasErrArgument: false,
          hasErrPrintField: false
        }
      });
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.fieldSelectPopup.props.open &&
      !this.props.fieldSelectPopup.props.open
    ) {
      if (this.props.fieldSelectPopup.props.from === "fieldFunc") {
        if (this.props.selectedField && !_.isEmpty(this.props.selectedField)) {
          const form = this.state.form;
          form["printFieldId"] = this.props.selectedField.fieldId;
          form["printField"] = this.props.selectedField.field;
          this.setState({ form });
          const validation = this.state.validation;
          validation.validate = true;
          this.setState({ validation });
        }
      }
    }
  }
  //validate 토글
  toggleValidating = validate => {
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });
  };

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrFieldId,
      hasErrFunc,
      hasErrArgument,
      hasErrPrintField
    } = this.state.validation;
    return (
      !hasErrFieldId && !hasErrFunc && !hasErrArgument && !hasErrPrintField
    );
  };

  closeComposeDialog = () => {
    const { NmlzRuleActions } = this.props;
    this.props.fieldFuncPopup.type === "edit"
      ? NmlzRuleActions.closeEditFieldFuncPopup()
      : NmlzRuleActions.closeNewFieldFuncPopup();
  };

  changeArgumentOpt = func => {
    if (func === "") return;

    const node = this.argumentRef.current;
    node.optionItems = [];
    this.getFieldFuncArguments(func);
  };
  /**
   * Drag cb func
   */
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  handleChange = (value, event) => {
    const { name } = event.target;
    const form = _.set({ ...this.state.form }, name, value);
    this.setState({ form });
  };

  handleFieldChange = value => {
    const item = _.filter(fieldOpt, field => {
      return field.id === parseInt(value);
    })[0];

    const form = this.state.form;
    form["fieldId"] = item.id;
    form["field"] = item.name;
    this.setState({ form });
  };

  handleFieldFuncCodeChange = value => {
    const form = this.state.form;

    let func = _.filter(this.state.funcOpt, opt => {
      return opt.id === value;
    });

    form["fieldFuncCode"] = value;
    form["fieldFuncCodeNm"] = func[0].name;
    form["fieldFuncArg"] = "";
    this.setState({ form: form, editable: value === "PRF" ? true : false });
  };

  handleFieldFuncArgChange = value => {
    let event = { target: { name: "fieldFuncArg" } };
    this.handleChange(value, event);
  };

  handlePrintFieldChange = value => {
    let event = { target: { name: "printField" } };
    this.handleChange(value, event);
  };

  render() {
    const { position, form } = this.state;
    const { fieldFuncPopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup"
            style={{
              width: "480px"
            }}
            id="fieldFuncPopup"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                {fieldFuncPopup.type === "new" ? (
                  <IntlMessages id="field.func.reg" />
                ) : (
                  <IntlMessages id="field.func.edit" />
                )}
              </h5>
              <button
                className="btn btn-close"
                onClick={() => {
                  this.closeComposeDialog();
                }}
              />
            </div>
            <div className="popup__body">
              <div className="option-wrapper">
                <table className="table table--white table--info">
                  <colgroup>
                    <col width="85px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <IntlMessages id="field" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          tabIndex="1"
                          name="fieldId"
                          value={form.fieldId}
                          optionList={fieldOpt}
                          onChange={this.handleFieldChange}
                          onBlur={() => {}}
                          validate={validate}
                          validationOption={{locale: this.props.language}}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrFieldId: res,
                                validate: false
                              }
                            });
                          }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="function" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          tabIndex="2"
                          name="fieldFuncCode"
                          value={form.fieldFuncCode}
                          optionList={this.state.funcOpt}
                          onChange={(func, e) => {
                            this.changeArgumentOpt(func);
                            this.handleFieldFuncCodeChange(func, e);
                          }}
                          onBlur={() => {}}
                          validate={validate}
                          validationOption={{locale: this.props.language}}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrFunc: res,
                                validate: false
                              }
                            });
                          }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="factor.list" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          ref={this.argumentRef}
                          tabIndex="3"
                          name="fieldFuncArgCode"
                          value={form.fieldFuncArgCode}
                          optionList={this.state.argumentOpt}
                          onChange={(code, e) => {
                            let argument = _.filter(
                              this.state.argumentOpt,
                              opt => {
                                return opt.id === code;
                              }
                            );

                            const form = this.state.form;
                            form["fieldFuncArgCode"] = code;
                            form["fieldFuncArg"] = argument[0].name;
                            this.setState({ form });
                          }}
                          onBlur={() => {}}
                          validate={validate}
                          validationOption={{locale: this.props.language}}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrArgument: res,
                                validate: false
                              }
                            });
                          }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="factor" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Textbox
                          tabIndex="4"
                          name="fieldFuncArg"
                          type="text"
                          disabled={!this.state.editable}
                          value={form.fieldFuncArg}
                          onChange={this.handleFieldFuncArgChange}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="output.field" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td className="inner-btn">
                        <div className="flex-box">
                          <Textbox
                            tabIndex="5" //Option
                            id="printField"
                            name="printField"
                            type="text"
                            disabled={true}
                            value={form.printField}
                            validate={validate}
                            validationOption={{locale: this.props.language}}
                            validationCallback={res => {
                              this.setState({
                                validation: {
                                  ...this.state.validation,
                                  hasErrPrintField: res,
                                  validate: false
                                }
                              });
                            }}
                          />
                          <div className="btns">
                            <button
                              className="btn btn--blue"
                              onClick={() => {
                                const { NmlzRuleActions } = this.props;
                                NmlzRuleActions.openFieldSelectPopup({
                                  from: "fieldFunc"
                                });
                              }}
                            >
                              <IntlMessages id="field.select" />
                            </button>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="popup__footer">
              <button
                className="btn btn--white"
                onClick={() => {
                  this.closeComposeDialog();
                }}
              >
                <IntlMessages id="cancel" />
              </button>
              <button
                className="btn btn--blue"
                onClick={() => {
                  if (this.isValidate()) {
                    const { NmlzRuleActions } = this.props;
                    NmlzRuleActions.saveNmlzRuleFieldFuncs({
                      type: fieldFuncPopup.type,
                      data: form
                    });
                    NmlzRuleActions.canBeContSave();
                    this.closeComposeDialog();
                  }
                }}
              >
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    fieldFuncPopup: state.nmlzRule.get("fieldFuncPopup"),
    fieldSelectPopup: state.nmlzRule.get("fieldSelectPopup"),
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont"),
    selectedField: state.nmlzRule.get("selectedField")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch),
    ComCodeActions: bindActionCreators(comCodeActions, dispatch)
  })
)(FieldFuncPopup);
