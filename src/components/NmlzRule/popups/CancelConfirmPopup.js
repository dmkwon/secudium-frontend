import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import IntlMessages from "util/IntlMessages";

class CancelConfirmPopup extends Component {
  render() {
    return (
      <div>
        <div className="popup popup--alert" style={{ width: "400px" }}>
          <div className="popup__header">
            <h5>
              <IntlMessages id="confirm" />
            </h5>
            <button
              className="btn btn-close"
              onClick={() => {
                this.props.cancelConfirmPopup.props.action = "cancel";
                this.props.closeCancelConfirmPopup(
                  this.props.cancelConfirmPopup
                );
              }}
            />
          </div>
          <div className="popup__body">
            <IntlMessages id="cancel.change" />
          </div>
          <div className="popup__footer">
            <button
              className="btn btn--white"
              onClick={() => {
                this.props.cancelConfirmPopup.props.action = "cancel";
                this.props.closeCancelConfirmPopup(
                  this.props.cancelConfirmPopup
                );
              }}
            >
              <IntlMessages id="cancel" />
            </button>
            <button
              className="btn btn--dark"
              onClick={() => {
                if (this.props.cancelConfirmPopup.props.from === "nmlzRule") {
                  this.props.cancelNmlzRule();
                  this.props.canBeSubmit();
                  this.props.cancelConfirmPopup.props.action = "ok";
                } else if (
                  this.props.cancelConfirmPopup.props.from === "nmlzRuleHdr"
                ) {
                  this.props.cancelNmlzRuleHdr();
                  this.props.canBeHdrSave();
                } else if (
                  this.props.cancelConfirmPopup.props.from === "nmlzRuleCont"
                ) {
                  this.props.setIsExistDefaultUseYn(false);
                  this.props.cancelNmlzRuleCont();
                  this.props.canBeContSave();
                }

                this.props.closeCancelConfirmPopup(
                  this.props.cancelConfirmPopup
                );
              }}
            >
              <IntlMessages id="confirm" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    cancelConfirmPopup: state.nmlzRule.get("cancelConfirmPopup")
  }),
  dispatch => {
    return bindActionCreators(
      {
        closeCancelConfirmPopup: nmlzRuleActions.closeCancelConfirmPopup,
        navPageTarget: nmlzRuleActions.navPageTarget,
        clearData: nmlzRuleActions.clearData,
        cancelNmlzRule: nmlzRuleActions.cancelNmlzRule,
        canBeSubmit: nmlzRuleActions.canBeSubmit,
        cancelNmlzRuleCont: nmlzRuleActions.cancelNmlzRuleCont,
        canBeContSave: nmlzRuleActions.canBeContSave,
        setIsExistDefaultUseYn: nmlzRuleActions.setIsExistDefaultUseYn,
        cancelNmlzRuleHdr: nmlzRuleActions.cancelNmlzRuleHdr,
        canBeHdrSave: nmlzRuleActions.canBeHdrSave
      },
      dispatch
    );
  }
)(CancelConfirmPopup);
