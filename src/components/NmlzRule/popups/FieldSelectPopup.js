import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";

import * as nmlzRuleActions from "store/modules/nmlzRule";
import * as filterPopupActions from "store/modules/filterPopup";

import Tree, { TreeNode } from "rc-tree";
import GridTable from "components/Common/GridTable";

import Draggable from "react-draggable";
import IntlMessages from "util/IntlMessages";

class FieldSelectPopup extends React.Component {
  state = {
    position: {
      x: 0,
      y: 0
    },
    keyword: ""
  };

  componentDidMount() {
    const elWidth = document.getElementById("fieldSelectPopup").offsetWidth;
    const elHeight = document.getElementById("fieldSelectPopup").offsetHeight;
    this.setState(
      { position: { x: elWidth / 2, y: (elHeight / 2) * -1 } },
      () => {}
    );
    this.onSelect(["all"]);
  }

  closeComposeDialog = () => {
    this.props.closeFieldSelectPopup(this.props.fieldSelectPopup);
  };

  onSelect = key => {
    const tree = key[0];
    if (tree === "all") {
      this.props.getFilterFields();
      this.props.getFilterFieldTrees();
    } else {
      this.props.getFilterFields("fieldGroupcd=" + tree);
    }
  };

  onChangeInput = e => this.setState({ [e.target.name]: e.target.value });
  onKeydownInput = e => {
    if (e.keyCode === 13) {
      this.onClickSearch();
    }
  };

  /** 조회버튼 클릭 */
  onClickSearch = () => this.getFields();
  onClickReset = () => this.setState({ keyword: "" }, () => this.getFields());

  getFields = async () => {
    const { keyword } = this.state;
    await this.props.getFilterFields("field=" + keyword);
  };
  /**
   * Drag cb func
   */
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { position, keyword } = this.state;

    const { fieldTreeInfo, fieldInfo } = this.props;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--dark popup--field" id="fieldSelectPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <IntlMessages id="field.select" />
              </h5>
              <button
                className="btn btn-close"
                onClick={() => {
                  this.props.setField({});
                  this.closeComposeDialog();
                }}
              />
            </div>
            <div className="popup__body">
              <div className="flex-box">
                <div className="component filter-tree">
                  <div className="filter-tree__wrap">
                    <div className="component__title" />
                    <div className="component__box">
                      <Tree
                        showLine
                        selectable={true}
                        onSelect={this.onSelect}
                        defaultExpandAll
                        defaultSelectedKeys={["all"]}
                      >
                        <TreeNode title={<IntlMessages id="all" />} key="all">
                          {Array.from(fieldTreeInfo.values()).map(value => {
                            return (
                              <TreeNode
                                title={value.fieldGroupName}
                                key={value.fieldGroupCode}
                              />
                            );
                          })}
                        </TreeNode>
                      </Tree>
                    </div>
                  </div>
                </div>
                <div className="asset-table">
                  <div className="search-bar">
                    <FormattedMessage id="filed.or.name">
                      {placeholder => (
                        <input
                          type="text"
                          className="form-control"
                          name="keyword"
                          placeholder={placeholder}
                          value={keyword}
                          onChange={this.onChangeInput}
                          onKeyDown={this.onKeydownInput}
                        />
                      )}
                    </FormattedMessage>
                    <div className="binder" />
                    <button
                      className="btn btn-icon small btn--go"
                      onClick={this.onClickSearch}
                    />
                    <button
                      className="btn btn-icon small btn--filter"
                      onClick={this.onClickReset}
                    />
                  </div>
                  <GridTable
                    className="-striped -highlight"
                    getTrProps={(state, rowInfo, column) => {
                      return {
                        className: "cursor-pointer",
                        onClick: (e, handleOriginal) => {
                          if (rowInfo) {
                            this.props.setField(rowInfo.original);
                            setTimeout(() => {
                              this.closeComposeDialog();
                            }, 300);
                          }
                        }
                      };
                    }}
                    columns={[
                      {
                        Header: "No.",
                        accessor: "no",
                        sortable: true,
                        width: 80,
                        style: { textAlign: "center" }
                      },
                      {
                        Header: <IntlMessages id="field" />,
                        accessor: "field",
                        width: 100
                      },
                      {
                        Header: <IntlMessages id="field.desc" />,
                        accessor: "fieldDesc",
                        width: 150
                      },
                      {
                        Header: <IntlMessages id="field.group" />,
                        accessor: "fieldGroupcdString"
                      }
                    ]}
                    data={fieldInfo}
                    defaultPageSize={10}
                    pageSizeOptions={[10, 15, 20]}
                    style={{
                      height: "500px"
                    }}
                  />
                </div>
              </div>
            </div>
            <div className="popup__footer">
              <button
                className="btn btn--white"
                onClick={() => {
                  this.props.setField({});
                  this.closeComposeDialog();
                }}
              >
                <IntlMessages id="cancel" />
              </button>
            </div>
          </div>
        </Draggable>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    fieldSelectPopup: state.nmlzRule.get("fieldSelectPopup"),
    fieldInfo: state.filterPopup.get("fields"),
    fieldTreeInfo: state.filterPopup.get("fieldTrees")
  }),
  dispatch => {
    return bindActionCreators(
      {
        closeFieldSelectPopup: nmlzRuleActions.closeFieldSelectPopup,
        setField: nmlzRuleActions.setField,
        getFilterFields: filterPopupActions.getFilterFields,
        getFilterFieldTrees: filterPopupActions.getFilterFieldTrees
      },
      dispatch
    );
  }
)(FieldSelectPopup);
