import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import IntlMessages from "util/IntlMessages";

class DefaultValueApplyPopup extends Component {
  render() {
    return (
      <div>
        <div className="popup popup--alert" style={{ width: "400px" }}>
          <div className="popup__header">
            <h5>
              <IntlMessages id="confirm" />
            </h5>
            <button
              className="btn btn-close"
              onClick={() => {
                this.props.defaultValueApplyPopup.data = null;
                this.props.closeDefaultValueApplyPopup(
                  this.props.defaultValueApplyPopup.data
                );
                this.props.canBeContSave();
              }}
            />
          </div>
          <div className="popup__body">
            <IntlMessages id="apply.default.set" />
          </div>
          <div className="popup__footer">
            <button
              className="btn btn--white"
              onClick={() => {
                this.props.defaultValueApplyPopup.data = null;
                this.props.closeDefaultValueApplyPopup(
                  this.props.defaultValueApplyPopup.data
                );
                this.props.canBeContSave();
              }}
            >
              <IntlMessages id="cancel" />
            </button>
            <button
              className="btn btn--dark"
              onClick={() => {
                this.props.closeDefaultValueApplyPopup(
                  this.props.defaultValueApplyPopup.data
                );
                this.props.canBeContSave();
              }}
            >
              <IntlMessages id="confirm" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    defaultValueApplyPopup: state.nmlzRule.get("defaultValueApplyPopup")
  }),
  dispatch => {
    return bindActionCreators(
      {
        closeDefaultValueApplyPopup:
          nmlzRuleActions.closeDefaultValueApplyPopup,
        canBeContSave: nmlzRuleActions.canBeContSave
      },
      dispatch
    );
  }
)(DefaultValueApplyPopup);
