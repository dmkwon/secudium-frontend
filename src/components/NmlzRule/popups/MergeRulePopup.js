import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import { Textbox, Select } from "react-inputs-validation";

import Draggable from "react-draggable";

import _ from "@lodash";
import GlobalUtils from "util/GlobalUtils";
import IntlMessages from "util/IntlMessages";

const mergeRuleState = {
  nmlzRuleFieldMergeSeq: -1,
  nmlzRuleContSeq: -1,
  field1: "",
  fieldId1: "",
  field2: "",
  fieldId2: "",
  printField: "",
  printFieldId: -1
};

let fieldOpt = [];
class MergeRulePopup extends React.Component {
  state = {
    form: { ...mergeRuleState },
    validation: {
      validate: false,
      hasErrField1: true,
      hasErrField2: true,
      hasErrOutputField: true
    },
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount() {
    const elWidth = document.getElementById("mergeRulePopup").offsetWidth;
    const elHeight = document.getElementById("mergeRulePopup").offsetHeight;
    this.setState(
      { position: { x: elWidth / 2, y: (elHeight / 2) * -1 } },
      () => {}
    );

    if (this.props.mergePopup.type === "new") {
      fieldOpt = this.props.nmlzRuleCont.nmlzRuleFields.map(field => {
        return { id: field.fieldId, name: field.field };
      });
      mergeRuleState.nmlzRuleContSeq = this.props.mergePopup.data;
      mergeRuleState.nmlzRuleFieldMergeSeq = GlobalUtils.ID();
      this.setState({
        form: { ...mergeRuleState }
      });
    } else if (this.props.mergePopup.type === "edit") {
      fieldOpt = this.props.nmlzRuleCont.nmlzRuleFields.map(field => {
        return { id: field.fieldId, name: field.field };
      });

      this.setState({
        form: { ...this.props.mergePopup.data },
        validation: {
          validate: false,
          hasErrField1: false,
          hasErrField2: false,
          hasErrOutputField: false
        }
      });
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.fieldSelectPopup.props.open &&
      !this.props.fieldSelectPopup.props.open
    ) {
      if (this.props.fieldSelectPopup.props.from === "merge") {
        if (this.props.selectedField && !_.isEmpty(this.props.selectedField)) {
          const form = this.state.form;
          form["printFieldId"] = this.props.selectedField.fieldId;
          form["printField"] = this.props.selectedField.field;
          this.setState({ form });
          const validation = this.state.validation;
          validation.validate = true;
          this.setState({ validation });
        }
      }
    }
  }

  closeComposeDialog = () => {
    this.props.mergePopup.type === "edit"
      ? this.props.closeEditMergePopup()
      : this.props.closeNewMergePopup();
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  handleChange = (value, event) => {
    const { name } = event.target;
    const form = _.set({ ...this.state.form }, name, value);
    this.setState({ form });
  };

  handleSelectChange = (value, name) => {
    const item = _.filter(fieldOpt, field => {
      return field.id === parseInt(value);
    })[0];

    const form = this.state.form;
    form[name] = item.id;
    form[name.replace("Id", "")] = item.name;
    this.setState({ form });
  };

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrField1,
      hasErrField2,
      hasErrOutputField
    } = this.state.validation;

    return !hasErrField1 && !hasErrField2 && !hasErrOutputField;
  };

  /**
   * Drag cb func
   */
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { position, form } = this.state;

    const { mergePopup } = this.props;

    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup"
            style={{
              width: "480px"
            }}
            id="mergeRulePopup"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                {mergePopup.type === "new" ? (
                  <IntlMessages id="merge.rule.new" />
                ) : (
                  <IntlMessages id="merge.rule.edit" />
                )}
              </h5>
              <button
                className="btn btn-close"
                onClick={() => {
                  this.closeComposeDialog();
                }}
              />
            </div>
            <div className="popup__body">
              <div className="option-wrapper">
                <table className="table table--white table--info">
                  <colgroup>
                    <col width="85px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <IntlMessages id="input.field1" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          tabIndex="1" //Option
                          name="fieldId1"
                          value={form.fieldId1}
                          optionList={fieldOpt}
                          onChange={value => {
                            this.handleSelectChange(value, "fieldId1");
                          }}
                          onBlur={() => {}}
                          validate={validate}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrField1: res,
                                validate: false
                              }
                            });
                          }}
                          validationOption={{
                            name: "fieldId1",
                            check: true,
                            required: true,
                            locale: this.props.language
                          }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="input.field2" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          tabIndex="2" //Option
                          name="fieldId2"
                          value={form.fieldId2}
                          optionList={fieldOpt}
                          onChange={(value, e) => {
                            this.handleSelectChange(value, "fieldId2");
                          }}
                          onBlur={() => {}}
                          validate={validate}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrField2: res,
                                validate: false
                              }
                            });
                          }}
                          validationOption={{
                            name: "fieldId2",
                            check: true,
                            required: true,
                            locale: this.props.language
                          }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="output.field" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td className="inner-btn">
                        <div className="flex-box">
                          <Textbox
                            tabIndex="5" //Option
                            name="printField"
                            type="text"
                            value={form.printField}
                            disabled={true}
                            onBlur={e => {}}
                            validate={validate}
                            validationOption={{locale: this.props.language}}
                            validationCallback={res => {
                              this.setState({
                                validation: {
                                  ...this.state.validation,
                                  hasErrOutputField: res,
                                  validate: false
                                }
                              });
                            }}
                          />
                          <div className="btns">
                            <button
                              className="btn btn--blue"
                              onClick={() => {
                                this.props.openFieldSelectPopup({
                                  from: "merge"
                                });
                              }}
                            >
                              <IntlMessages id="field.select" />
                            </button>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="popup__footer">
              <button
                className="btn btn--white"
                onClick={() => {
                  this.closeComposeDialog();
                }}
              >
                <IntlMessages id="cancel" />
              </button>
              <button
                className="btn btn--blue"
                onClick={() => {
                  if (this.isValidate()) {
                    this.props.saveNmlzRuleFieldMerges({
                      type: mergePopup.type,
                      data: form
                    });
                    this.props.canBeContSave();
                    this.closeComposeDialog();
                  }
                }}
              >
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    mergePopup: state.nmlzRule.get("mergePopup"),
    fieldSelectPopup: state.nmlzRule.get("fieldSelectPopup"),
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont"),
    selectedField: state.nmlzRule.get("selectedField"),
    language: state.common.get("language")
  }),
  dispatch => {
    return bindActionCreators(
      {
        closeNewMergePopup: nmlzRuleActions.closeNewMergePopup,
        closeEditMergePopup: nmlzRuleActions.closeEditMergePopup,
        openFieldSelectPopup: nmlzRuleActions.openFieldSelectPopup,
        saveNmlzRuleFieldMerges: nmlzRuleActions.saveNmlzRuleFieldMerges,
        canBeContSave: nmlzRuleActions.canBeContSave
      },
      dispatch
    );
  }
)(MergeRulePopup);
