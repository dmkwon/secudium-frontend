import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Textarea } from "react-inputs-validation";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import IntlMessages from "util/IntlMessages";

const rollbackState = {
  chgReason: "",
  nmlzRuleHist: {},
  validation: {
    validate: false,
    hasErrChgReason: true
  }
};

class RollbackNmlzRulePopup extends Component {
  state = { ...rollbackState };

  componentDidMount() {
    rollbackState.nmlzRuleHist = this.props.rollbackPopup.data;
    this.setState({ ...rollbackState });
  }

  onClick = () => {
    if (!this.isValidate()) {
      return;
    }

    const { NmlzRuleActions } = this.props;
    const ajax = NmlzRuleActions.rollBackNmlzRule({
      nmlzRuleHistSeq: this.state.nmlzRuleHist.nmlzRuleHistSeq,
      chgReason: this.state.chgReason
    });
    ajax.then(
      () => {
        NmlzRuleActions.closeRollBackPopup();
      },
      err => {
        console.error("err", err);
        NmlzRuleActions.closeRollBackPopup();
      }
    );
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);
    const { hasErrChgReason } = this.state.validation;
    return !hasErrChgReason;
  };

  onChange = (value, e) => {
    this.setState({
      chgReason: value
    });
  };

  render() {
    const { onClick, onChange } = this;
    const { NmlzRuleActions } = this.props;

    const { chgReason } = this.state;
    const { validate } = this.state.validation;

    return (
      <div className="popup popup--alert" style={{ width: "400px" }}>
        <div className="popup__header">
          <h5>
            <IntlMessages id="confirm" />
          </h5>
          <button
            className="btn btn-close"
            onClick={() => {
              NmlzRuleActions.closeRollBackPopup();
            }}
          />
        </div>
        <div className="popup__body">
          <IntlMessages id="roll.back.hist" />
          <div className="change-reason">
            <label>
              <IntlMessages id="reason.change.delete" />
              <span className="font--red">*</span>
            </label>
            <Textarea
              name="chgReason"
              type="text"
              tabIndex="1"
              maxLength="4000"
              value={chgReason}
              onChange={onChange}
              onBlur={() => {}}
              validate={validate}
              validationOption={{locale: this.props.language}}
              validationCallback={res => {
                this.setState({
                  validation: {
                    ...this.state.validation,
                    hasErrChgReason: res,
                    validate: false
                  }
                });
              }}
              customStyleInput={{
                width: "100%",
                resize: "none",
                height: "80px",
                marginTop: "15px"
              }}
            />
          </div>
        </div>
        <div className="popup__footer">
          <button
            className="btn btn--white"
            onClick={() => {
              NmlzRuleActions.closeRollBackPopup();
            }}
          >
            <IntlMessages id="cancel" />
          </button>
          <button className="btn btn--dark" onClick={onClick}>
            <IntlMessages id="confirm" />
          </button>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    rollbackPopup: state.nmlzRule.get("rollbackPopup")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch)
  })
)(RollbackNmlzRulePopup);
