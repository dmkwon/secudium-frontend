import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Textarea } from "react-inputs-validation";
import _ from "@lodash";

import * as nmlzRuleActions from "store/modules/nmlzRule";

import IntlMessages from "util/IntlMessages";

const deleteNmlzRuleState = {
  nmlzRuleSeqs: [],
  nmlzRuleChgReason: "",
  validation: {
    validate: false,
    hasErrNmlzRuleChgReason: true
  }
};

class DeleteNmlzRulePopup extends Component {
  state = { ...deleteNmlzRuleState };

  componentDidMount() {
    deleteNmlzRuleState.nmlzRuleSeqs = this.props.deletePopup.data;
    this.setState({ ...deleteNmlzRuleState });
  }

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);
    const { hasErrNmlzRuleChgReason } = this.state.validation;
    return !hasErrNmlzRuleChgReason;
  };

  handleChange = (value, event) => {
    this.setState(
      _.set(
        { ...this.state },
        event.target.name,
        event.target.type === "checkbox" ? event.target.checked : value
      )
    );
  };

  doRemoveNmlzRules = async params => {
    try {
      await this.props.removeNmlzRule(params);
      this.props.closeDeletePopup();
    } catch (err) {
      console.error("err", err);
    }
  };

  closeDeletePopup = () => {
    this.props.closeDeletePopup();
  };

  render() {
    const { validate } = this.state.validation;

    return (
      <div>
        <div className="popup popup--alert" style={{ width: "400px" }}>
          <div className="popup__header">
            <h5>
              <IntlMessages id="confirm" />
            </h5>
            <button className="btn btn-close" onClick={this.closeDeletePopup} />
          </div>
          <div className="popup__body">
            <IntlMessages id="delete.nmlz.rule" />
            <div className="change-reason">
              <label>
                <IntlMessages id="reason.change.delete" />
                <span className="font--red">*</span>
              </label>
              <Textarea
                name="nmlzRuleChgReason"
                type="text"
                tabIndex="1"
                maxLength="4000"
                value={this.state.nmlzRuleChgReason}
                onChange={this.handleChange}
                onBlur={() => {}}
                validate={validate}
                validationOption={{locale: this.props.language}}
                validationCallback={res => {
                  this.setState({
                    validation: {
                      ...this.state.validation,
                      hasErrNmlzRuleChgReason: res,
                      validate: false
                    }
                  });
                }}
                customStyleInput={{
                  width: "100%",
                  resize: "none",
                  height: "80px",
                  marginTop: "15px"
                }}
              />
            </div>
          </div>
          <div className="popup__footer">
            <button
              className="btn btn--white"
              onClick={() => {
                this.closeDeletePopup();
              }}
            >
              <IntlMessages id="cancel" />
            </button>
            <button
              className="btn btn--dark"
              onClick={() => {
                if (!this.isValidate()) {
                  return;
                }
                this.doRemoveNmlzRules({
                  nmlzRuleSeqs: this.state.nmlzRuleSeqs,
                  nmlzRuleChgReason: this.state.nmlzRuleChgReason
                });
              }}
            >
              <IntlMessages id="confirm" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    deletePopup: state.nmlzRule.get("deletePopup")
  }),
  dispatch => {
    return bindActionCreators(
      {
        removeNmlzRule: nmlzRuleActions.removeNmlzRule,
        closeDeletePopup: nmlzRuleActions.closeDeletePopup
      },
      dispatch
    );
  }
)(DeleteNmlzRulePopup);
