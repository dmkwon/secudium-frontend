import _ from "@lodash";

import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import * as comCodeActions from "store/modules/code";

import { PAGE_ACTION_TYPE } from "store/modules/nmlzRule";

import Draggable from "react-draggable";

import app from "../regexp/app";

import "antd/dist/antd.css";
import { Select as ASelect } from "antd";
import { Select, Textarea } from "react-inputs-validation";

import AceEditor from "react-ace";
import "brace/mode/json";
import "brace/theme/monokai";

// import FieldFunction from '../Detail/Tab/Accordion/ContentsRuleSet/parts/FieldFunction';
// import MergeRule from '../Detail/Tab/Accordion/ContentsRuleSet/parts/MergeRule';
// import ReplaceRule from '../Detail/Tab/Accordion/ContentsRuleSet/parts/ReplaceRule';
import ArrangeRule from "../Detail/Tab/Accordion/ContRule/parts/ArrangeRule";
import IntlMessages from "util/IntlMessages";
import { FormattedMessage } from "react-intl";

import "../regexp/sass/regexp.scss";

const Option = ASelect.Option;
const useYnOpt = [
  { id: "Y", name: <IntlMessages id="use" /> },
  { id: "N", name: <IntlMessages id="unused" /> }
];

class NmlzRuleContDetailPopup extends React.Component {
  state = {
    position: {
      x: 0,
      y: 0
    },
    regexResult: [],
    eventTypeCodeOpt: []
  };

  getEventType = async () => {
    const { ComCodeActions } = this.props;

    let eventTypeCodeOpt = await ComCodeActions.getCodesByParentCode(
      "EVENT_TYPE"
    );
    let newEventTypeCodeOpt = eventTypeCodeOpt.data.map(func => {
      return { id: func.commCode, name: func.commCodeNm };
    });
    this.setState({
      eventTypeCodeOpt: newEventTypeCodeOpt
    });
  };

  changeExpression() {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleCont({
      name: "contRegexp",
      value: app.expression.getPattern()
    });
    NmlzRuleActions.canBeContSave();
    NmlzRuleActions.canBeSubmit();
  }

  changeText() {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleCont({
      name: "contSampLog",
      value: app.text.getValue()
    });
    NmlzRuleActions.canBeContSave();
    NmlzRuleActions.canBeSubmit();
  }

  updateResult() {
    const result = app.getResult();
    const { NmlzRuleActions, nmlzRuleCont } = this.props;
    const sliceNmlzRuleFields = _.slice(
      nmlzRuleCont.nmlzRuleFields,
      0,
      result.matches[0].groups === undefined
        ? 0
        : result.matches[0].groups.length
    );
    NmlzRuleActions.editNmlzRuleCont({
      name: "nmlzRuleFields",
      value: sliceNmlzRuleFields
    });
    NmlzRuleActions.canBeContSave();
    NmlzRuleActions.canBeSubmit();

    NmlzRuleActions.setRegexResult({
      result: result
    });
  }

  componentDidMount() {
    const elWidth = document.getElementById("contPopup").offsetWidth;
    const elHeight = document.getElementById("contPopup").offsetHeight;
    this.setState(
      { position: { x: elWidth / 2, y: (elHeight / 2) * -1 } },
      () => {}
    );

    if (this.props.nmlzRule.logFormatCode === "RGX") {
      app.init(false, ".container1", ".result1");
      app.expression.setValue(this.props.nmlzRuleCont.contRegexp);
      app.text.setValue(this.props.nmlzRuleCont.contSampLog);
      app.expression.on("change", () => this.changeExpression());
      app.text.on("change", () => this.changeText());
      app.on("result", () => this.updateResult());
    }

    this.getEventType();
  }

  componentWillUnmount() {
    if (this.props.nmlzRule.logFormatCode === "RGX") {
      app.cleanup();
    }
  }

  closeComposeDialog = () => {
    const { NmlzRuleActions } = this.props;
    this.props.contPopup.type === PAGE_ACTION_TYPE.EDIT
      ? NmlzRuleActions.closeEditContPopup(this.props.contPopup.data)
      : NmlzRuleActions.closeNewContPopup(this.props.contPopup.data);
  };

  /**
   * Drag cb func
   */
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  handleChange = (value, event) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleCont({
      name: event.target.name,
      value: event.target.type === "checkbox" ? event.target.checked : value
    });
    NmlzRuleActions.canBeContSave();
  };

  handleItemChange = (value, name) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleCont({
      name: name,
      value: value
    });
    NmlzRuleActions.canBeContSave();
  };

  handleDefaultUseYnChange = event => {
    const { name, checked } = event.target;
    const { NmlzRuleActions } = this.props;
    if (checked && this.props.isExistDefaultUseYn) {
      NmlzRuleActions.openDefaultValueApplyPopup({
        name: name,
        value: "Y"
      });
    } else {
      NmlzRuleActions.editNmlzRuleCont({
        name: name,
        value: checked === true ? "Y" : "N"
      });
      NmlzRuleActions.setIsExistDefaultUseYn(checked);
      NmlzRuleActions.canBeContSave();
    }
  };

  handleEventTypeCodeChange = value => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleCont({
      name: "eventTypeCode",
      value: value
    });
    NmlzRuleActions.canBeContSave();
  };

  onContSave = () => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.saveNmlzRuleCont();
    NmlzRuleActions.canBeSubmit();
  };

  onChange = newValue => {
    const { NmlzRuleActions } = this.props;
    try {
      NmlzRuleActions.editNmlzRuleCont({
        name: "contSampLog",
        value: newValue
      });
      NmlzRuleActions.canBeContSave();
    } catch (e) {
      if (e instanceof SyntaxError) {
        NmlzRuleActions.editNmlzRuleCont({
          name: "contSampLog",
          value: ""
        });
        NmlzRuleActions.canBeContSave();
      }
    }
  };

  formatJSON = (input, space) => {
    if (input.length === 0) {
      return "";
    } else {
      try {
        var parsedData = JSON.parse(input);
        return JSON.stringify(parsedData, null, space);
      } catch (err) {
        console.error(err);
        return input;
      }
    }
  };

  beautify = () => {
    const { NmlzRuleActions, nmlzRuleCont } = this.props;
    NmlzRuleActions.editNmlzRuleCont({
      name: "contSampLog",
      value: this.formatJSON(nmlzRuleCont.contSampLog, 2)
    });
    NmlzRuleActions.canBeContSave();
  };

  render() {
    const {
      handleItemChange,
      handleEventTypeCodeChange
      //, handleDefaultUseYnChange
    } = this;

    const { position, eventTypeCodeOpt } = this.state;

    const { contPopup } = this.props;
    const {
      //, defaultUseYn
      contSampLog,
      useYn,
      contDesc,
      eventTypeCode
    } = this.props.nmlzRuleCont;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--dark popup--select" id="contPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                {contPopup.type === "add" ? (
                  <IntlMessages id="content.rule.create" />
                ) : (
                  <IntlMessages id="content.rule.edit" />
                )}
              </h5>
              <button
                className="btn btn-close"
                onClick={() => {
                  const { NmlzRuleActions } = this.props;
                  NmlzRuleActions.setIsExistDefaultUseYn(false);
                  this.closeComposeDialog();
                }}
              />
            </div>
            <div className="popup__body">
              <div className="component__title">
                <span>
                  <IntlMessages id="content.rule" />
                </span>
              </div>
              <div className="component cont" style={{ height: "580px" }}>
                <div className="form-group" style={{ marginBottom: "10px" }}>
                  <span>
                    Event Type
                    <span className="font--red" style={{ color: "red" }}>
                      *
                    </span>
                  </span>
                  {/* <Select
                    name={'eventTypeCode'}
                    value={eventTypeCode}
                    optionList={eventTypeCodeOpt}
                    onChange={(eventTypeCode, e) => {
                      handleItemChange(eventTypeCode, 'eventTypeCode')
                    }}
                    customStyleWrapper={{
                      width: ' 100px',
                      height: '24px',
                      fontSize: '12px',
                    }}
                  /> */}
                  <FormattedMessage id="select.event.type">
                    {placeholder => (
                      <ASelect
                        mode="multiple"
                        style={{ width: "1000px" }}
                        value={eventTypeCode}
                        placeholder={placeholder}
                        onChange={handleEventTypeCodeChange}
                      >
                        {eventTypeCodeOpt !== undefined &&
                          eventTypeCodeOpt.map(opt => {
                            return <Option key={opt.id}>{opt.name}</Option>;
                          })}
                      </ASelect>
                    )}
                  </FormattedMessage>
                  {/* <div className="checkbox checkbox--dark">
                    <label>
                      <input
                        type="checkbox"
                        name="defaultUseYn"
                        checked={defaultUseYn === 'Y' ? true : false}
                        onChange={handleDefaultUseYnChange}
                      />
                      <div className="icon" />
                      <span>기본값 적용</span>
                    </label>
                  </div> */}
                </div>
                <div className="form-group" style={{ marginBottom: "10px" }}>
                  <span>
                    <IntlMessages id="use.yn" />
                    <span className="font--red" style={{ color: "red" }}>
                      *
                    </span>
                  </span>

                  <Select
                    name={"useYn"}
                    value={useYn}
                    optionList={useYnOpt}
                    onChange={(useYn, e) => {
                      handleItemChange(useYn, "useYn");
                    }}
                    customStyleWrapper={{
                      width: "100px",
                      height: "24px",
                      fontSize: "12px",
                      marginLeft: "17px"
                    }}
                  />
                </div>
                <div className="form-group" style={{ marginBottom: "10px" }}>
                  <span style={{ verticalAlign: "top" }}>
                    <IntlMessages id="desc" />
                  </span>
                  <Textarea
                    classNameInput="form-control"
                    name="contDesc"
                    value={contDesc}
                    onChange={(contDesc, e) => {
                      handleItemChange(contDesc, "contDesc");
                    }}
                    maxLenth="4000"
                    customStyleInput={{
                      width: "641px",
                      height: "60px",
                      fontSize: "12px",
                      marginLeft: "47px"
                    }}
                  />
                </div>
                <div className="flex-box-arrange">
                  {this.props.nmlzRule.logFormatCode === "JSON" && (
                    <div className="left__json">
                      <div className="component__title">
                        <span>
                          <IntlMessages id="parser" />
                        </span>
                        <div className="btns">
                          <button
                            className="btn btn--blue"
                            onClick={() => {
                              this.beautify();
                            }}
                          >
                            beautify
                          </button>
                        </div>
                      </div>
                      <AceEditor
                        mode="json"
                        theme="monokai"
                        value={contSampLog}
                        onChange={this.onChange}
                        name="json-hr"
                        showPrintMargin={false}
                        editorProps={{ $blockScrolling: true }}
                        width={"660px"}
                        height={"260px"}
                      />
                    </div>
                  )}
                  {this.props.nmlzRule.logFormatCode === "RGX" && (
                    <div className="left__regex">
                      <div className="component__title">
                        <span>
                          <IntlMessages id="parser" />
                        </span>
                      </div>
                      <div className="container1">
                        <div className="app">
                          <div className="doc">
                            <div className="blocker" />
                            <section className="expression">
                              <header>
                                <h1>
                                  <IntlMessages id="regex.rule" />
                                </h1>
                              </header>
                              <article className="editor" />
                            </section>
                            <section className="text">
                              <header>
                                <h1>
                                  <IntlMessages id="log.sample" />
                                </h1>
                                <div className="controlbar">
                                  <div className="control result1">
                                    No match.
                                  </div>
                                </div>
                              </header>
                              <article className="editor multiline">
                                <div className="pad">
                                  <textarea className="inputc" />
                                </div>
                              </article>
                            </section>
                          </div>
                        </div>
                      </div>
                    </div>
                  )}
                  <div
                    className={
                      this.props.nmlzRule.logFormatCode === "JSON"
                        ? "right__json"
                        : "right__regex"
                    }
                  >
                    <ArrangeRule />
                  </div>
                </div>
                {/* <FieldFunction />
                <div className="flex-box">
                  <div className="left">
                    <MergeRule />
                  </div>
                  <div className="right">
                    <ReplaceRule />
                  </div>
                </div> */}
                <div className="btns" style={{ textAlign: "right" }}>
                  <button
                    className="btn"
                    onClick={() => {
                      if (this.props.canBeContSave) {
                        const { NmlzRuleActions } = this.props;
                        NmlzRuleActions.openCancelConfirmPopup({
                          from: "nmlzRuleCont"
                        });
                      } else {
                        //const { NmlzRuleActions } = this.props;
                        //NmlzRuleActions.setIsExistDefaultUseYn(false);
                        this.closeComposeDialog();
                      }
                    }}
                  >
                    <IntlMessages id="cancel" />
                  </button>
                  <button
                    className="btn btn--blue"
                    disabled={!this.props.canBeContSave}
                    onClick={() => {
                      this.onContSave();
                      this.closeComposeDialog();
                    }}
                  >
                    <IntlMessages id="content.rule.save" />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Draggable>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    contPopup: state.nmlzRule.get("contPopup"),
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont"),
    canBeContSave: state.nmlzRule.get("canBeContSave"),
    isExistDefaultUseYn: state.nmlzRule.get("isExistDefaultUseYn"),
    currentHdrIdx: state.nmlzRule.get("currentHdrIdx")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch),
    ComCodeActions: bindActionCreators(comCodeActions, dispatch)
  })
)(NmlzRuleContDetailPopup);
