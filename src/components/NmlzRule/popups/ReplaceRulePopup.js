import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import Draggable from "react-draggable";
import { Select, Textbox } from "react-inputs-validation";

import _ from "@lodash";
import GlobalUtils from "util/GlobalUtils";
import IntlMessages from "util/IntlMessages";

const replaceRuleState = {
  nmlzRuleFieldReplaceSeq: -1,
  nmlzRuleContSeq: -1,
  fieldId: "",
  field: "",
  inValue: "",
  printValue: ""
};

let fieldOpt = [];
class ReplaceRulePopup extends React.Component {
  state = {
    form: { ...replaceRuleState },
    validation: {
      validate: false,
      hasErrField: true,
      hasErrInValue: true,
      hasErrPrintValue: true
    },
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount() {
    const elWidth = document.getElementById("replaceRulePopup").offsetWidth;
    const elHeight = document.getElementById("replaceRulePopup").offsetHeight;
    this.setState(
      { position: { x: elWidth / 2, y: (elHeight / 2) * -1 } },
      () => {}
    );

    if (this.props.replacePopup.type === "new") {
      fieldOpt = this.props.nmlzRuleCont.nmlzRuleFields.map(field => {
        return { id: field.fieldId, name: field.field };
      });
      replaceRuleState.nmlzRuleContSeq = this.props.replacePopup.data;
      replaceRuleState.nmlzRuleFieldReplaceSeq = GlobalUtils.ID();

      this.setState({
        form: { ...replaceRuleState }
      });
    } else if (this.props.replacePopup.type === "edit") {
      fieldOpt = this.props.nmlzRuleCont.nmlzRuleFields.map(field => {
        return { id: field.fieldId, name: field.field };
      });

      this.setState({
        form: { ...this.props.replacePopup.data },
        validation: {
          validate: false,
          hasErrField: false,
          hasErrInValue: false,
          hasErrPrintValue: false
        }
      });
    }
  }

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  handleSelectChange = (value, name) => {
    const item = _.filter(fieldOpt, field => {
      return field.id === parseInt(value);
    })[0];

    const form = this.state.form;
    form[name] = item.id;
    form[name.replace("Id", "")] = item.name;
    this.setState({ form });
  };

  handleValueChange = (value, name) => {
    const form = this.state.form;
    form[name] = value;
    this.setState({ form });
  };

  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrField,
      hasErrInValue,
      hasErrPrintValue
    } = this.state.validation;

    return !hasErrField && !hasErrInValue && !hasErrPrintValue;
  };

  closeComposeDialog = () => {
    this.props.replacePopup.type === "edit"
      ? this.props.closeEditReplacePopup()
      : this.props.closeNewReplacePopup();
  };

  /**
   * Drag cb func
   */
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { position, form } = this.state;
    const { replacePopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup"
            style={{
              width: "480px"
            }}
            id="replaceRulePopup"
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                {replacePopup.type === "new" ? (
                  <IntlMessages id="replace.rule.new" />
                ) : (
                  <IntlMessages id="replace.rule.edit" />
                )}
              </h5>
              <button
                className="btn btn-close"
                onClick={() => {
                  this.closeComposeDialog();
                }}
              />
            </div>
            <div className="popup__body">
              <div className="option-wrapper">
                <table className="table table--white table--info">
                  <colgroup>
                    <col width="85px" />
                    <col />
                  </colgroup>
                  <tbody>
                    <tr>
                      <th>
                        <IntlMessages id="field" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Select
                          tabIndex="1" //Option
                          id={"field"}
                          name={"field"}
                          value={form.fieldId}
                          optionList={fieldOpt}
                          onChange={(value, e) => {
                            this.handleSelectChange(value, "fieldId");
                          }}
                          onBlur={() => {}}
                          validate={validate}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrField: res,
                                validate: false
                              }
                            });
                          }}
                          validationOption={{
                            name: "field",
                            check: true,
                            required: true,
                            locale: this.props.language
                          }}
                          customStyleWrapper={{ width: "200px" }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="find.string" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Textbox
                          tabIndex="2" //Option
                          id={"inValue"}
                          name={"inValue"}
                          type="text"
                          value={form.inValue}
                          onChange={(value, e) => {
                            this.handleValueChange(value, "inValue");
                          }}
                          onBlur={e => {}}
                          validate={validate}
                          validationOption={{locale: this.props.language}}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrInValue: res,
                                validate: false
                              }
                            });
                          }}
                        />
                      </td>
                    </tr>
                    <tr>
                      <th>
                        <IntlMessages id="replace.string" />
                        <span className="font--red" style={{ color: "red" }}>
                          *
                        </span>
                      </th>
                      <td>
                        <Textbox
                          tabIndex="3" //Option
                          id={"printValue"}
                          name={"printValue"}
                          type="text"
                          value={form.printValue}
                          onChange={(value, e) => {
                            this.handleValueChange(value, "printValue");
                          }}
                          onBlur={e => {}}
                          validate={validate}
                          validationOption={{locale: this.props.language}}
                          validationCallback={res => {
                            this.setState({
                              validation: {
                                ...this.state.validation,
                                hasErrPrintValue: res,
                                validate: false
                              }
                            });
                          }}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className="popup__footer">
              <button
                className="btn btn--white"
                onClick={() => {
                  this.closeComposeDialog();
                }}
              >
                <IntlMessages id="cancel" />
              </button>
              <button
                className="btn btn--blue"
                onClick={() => {
                  if (this.isValidate()) {
                    this.props.saveNmlzRuleFieldReplaces({
                      type: replacePopup.type,
                      data: form
                    });
                    this.props.canBeContSave();
                    this.closeComposeDialog();
                  }
                }}
              >
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    replacePopup: state.nmlzRule.get("replacePopup"),
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont"),
    language: state.common.get("language")
  }),
  dispatch => {
    return bindActionCreators(
      {
        closeNewReplacePopup: nmlzRuleActions.closeNewReplacePopup,
        closeEditReplacePopup: nmlzRuleActions.closeEditReplacePopup,
        saveNmlzRuleFieldReplaces: nmlzRuleActions.saveNmlzRuleFieldReplaces,
        canBeContSave: nmlzRuleActions.canBeContSave
      },
      dispatch
    );
  }
)(ReplaceRulePopup);
