import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import { Textbox } from "react-inputs-validation";

import AceEditor from "react-ace";

import "brace/mode/json";
import "brace/theme/monokai";

import IntlMessages from "util/IntlMessages";

import "../../../../style.scss";

class JsonHdr extends Component {
  handleChange = (value, event) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: event.target.name,
      value: event.target.type === "checkbox" ? event.target.checked : value
    });
    NmlzRuleActions.canBeSubmit();
  };

  onChange = newValue => {
    const { NmlzRuleActions } = this.props;
    try {
      NmlzRuleActions.editNmlzRule({
        name: "hdrSampLog",
        value: newValue
      });
      NmlzRuleActions.canBeSubmit();
    } catch (e) {
      if (e instanceof SyntaxError) {
        console.error("err", e);
        NmlzRuleActions.editNmlzRule({
          name: "hdrSampLog",
          value: ""
        });
        NmlzRuleActions.canBeSubmit();
      }
    }
  };

  formatJSON = (input, space) => {
    if (input.length === 0) {
      return "";
    } else {
      try {
        var parsedData = JSON.parse(input);
        return JSON.stringify(parsedData, null, space);
      } catch (err) {
        console.error(err);
        return input;
      }
    }
  };

  beautify = () => {
    const { NmlzRuleActions, nmlzRule } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: "hdrSampLog",
      value: this.formatJSON(nmlzRule.hdrSampLog, 2)
    });
    NmlzRuleActions.canBeSubmit();
  };

  render() {
    const { handleChange } = this;

    const { hdrPrefix, hdrKeyField, hdrSampLog } = this.props.nmlzRule;

    return (
      <div className="list__info--content">
        <div className="left__json">
          <div className="component__title">
            <span>
              <IntlMessages id="sample.log" />
            </span>
            <div className="btns">
              <button
                className="btn btn--blue"
                onClick={() => {
                  this.beautify();
                }}
              >
                beautify
              </button>
            </div>
          </div>
          <div
            className="component json-parser"
            style={{ height: 300, padding: 0 }}
          >
            <AceEditor
              mode="json"
              theme="monokai"
              value={hdrSampLog}
              onChange={this.onChange}
              name="json-hr"
              showPrintMargin={false}
              editorProps={{ $blockScrolling: true }}
              width={"100%"}
              height={"300px"}
            />
          </div>
        </div>
        <div className="right__json">
          <div className="component__title">
            <span>
              <IntlMessages id="header.rule" />
            </span>
          </div>
          <table className="table table--dark table--info">
            <colgroup>
              <col width="150px" />
              <col />
            </colgroup>
            <tbody>
              <tr>
                <th>Log prefix string</th>
                <td>
                  <Textbox
                    name="hdrPrefix"
                    type="text"
                    value={hdrPrefix}
                    onChange={handleChange}
                    classNameInput="form-control"
                  />
                </td>
              </tr>
              <tr>
                <th>Header Key Field</th>
                <td>
                  <Textbox
                    name="hdrKeyField"
                    type="text"
                    value={hdrKeyField}
                    onChange={handleChange}
                    classNameInput="form-control"
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    sampleLog: state.nmlzRule.get("sampleLog")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch)
  })
)(JsonHdr);
