import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import { Textbox } from "react-inputs-validation";

import GridTable from "components/Common/GridTable";
import IntlMessages from "util/IntlMessages";

import app from "../../../../regexp/app";

import "../../../../style.scss";
import "../../../../regexp/sass/regexp.scss";

class RegexHdr extends Component {
  state = {
    matching: []
  };

  handleChange = (value, event) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: event.target.name,
      value: event.target.type === "checkbox" ? event.target.checked : value
    });
    NmlzRuleActions.canBeSubmit();
  };

  changeExpression() {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: "hdrRegexp",
      value: app.expression.getPattern()
    });
    NmlzRuleActions.canBeSubmit();
  }

  changeText() {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: "hdrSampLog",
      value: app.text.getValue()
    });
    NmlzRuleActions.canBeSubmit();
  }

  updateResult() {
    let result = app.getResult();
    if (!result.error) {
      const { NmlzRuleActions } = this.props;
      NmlzRuleActions.setMatchInfo(app.getResult().matches);
    }
  }

  componentDidMount() {
    app.init(false, ".container", ".result");
    app.expression.setValue(this.props.nmlzRule.hdrRegexp);
    app.text.setValue(this.props.nmlzRule.hdrSampLog);
    app.expression.on("change", () => this.changeExpression());
    app.text.on("change", () => this.changeText());
    app.on("result", () => this.updateResult());
  }

  componentWillUnmount() {
    app.cleanup();
  }

  getMatchVal(match, str) {
    let val =
      match.s || (match.i === undefined ? "" : str.substr(match.i, match.l));
    return val;
  }

  render() {
    const { handleChange } = this;

    const { hdrSampLog, hdrPrefix } = this.props.nmlzRule;

    return (
      <div className="list__info--content">
        <div className="left__regex">
          <div className="component__title">
            <span>
              <IntlMessages id="regex" />
            </span>
          </div>
          <div className="container">
            <div className="app">
              <div className="doc">
                <div className="blocker" />
                <section className="expression">
                  <header>
                    <h1>
                      <IntlMessages id="header.rule" />
                    </h1>
                  </header>
                  <article className="editor" />
                </section>

                <section className="text">
                  <header>
                    <h1>
                      <IntlMessages id="log.sample" />
                    </h1>
                    <div className="controlbar">
                      <div className="control result">No match.</div>
                    </div>
                  </header>
                  <article className="editor multiline">
                    <div className="pad">
                      <textarea className="inputc" />
                    </div>
                  </article>
                </section>
              </div>
            </div>
          </div>
          <table className="table table--dark table--info">
            <colgroup>
              <col width="150px" />
              <col />
            </colgroup>
            <tbody>
              <tr>
                <th>Log prefix string</th>
                <td>
                  <Textbox
                    name="hdrPrefix"
                    type="text"
                    value={hdrPrefix}
                    onChange={handleChange}
                    classNameInput="form-control"
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="right__regex">
          <div className="component__title">
            <span>
              <IntlMessages id="matching.info" />
            </span>
          </div>
          <GridTable
            className="-striped -highlight"
            data={this.props.matchInfo}
            columns={[
              {
                Header: "No.",
                id: "row",
                accessor: "number",
                style: { textAlign: "center" },
                width: 50,
                Cell: row => {
                  return <div>{row.original.num + 1}</div>;
                }
              },
              {
                Header: <IntlMessages id="matching.info" />,
                accessor: "result",
                width: 530,
                Cell: row => {
                  if (row) {
                    return (
                      <div>{this.getMatchVal(row.original, hdrSampLog)}</div>
                    );
                  }
                }
              }
            ]}
            manual
            showPagination={false}
            showPaginationOptions={false}
            style={{
              height: "360px"
            }}
          />
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    contPopup: state.nmlzRule.get("contPopup"),
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    matchInfo: state.nmlzRule.get("matchInfo")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch)
  })
)(RegexHdr);
