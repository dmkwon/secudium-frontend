import React, { Component } from "react";
import { connect } from "react-redux";

import RegexHdrList from "./RegexHdrList";
import JsonHdrList from "./JsonHdrList";

class HdrRule extends Component {
  render() {
    const {
      onAddHdrRow,
      onEditHdrRow,
      onDeleteHdrRow,
      onGetNmlzRuleConts
    } = this.props;

    return (
      <React.Fragment>
        {this.props.nmlzRule.logFormatCode === "RGX" ? (
          <div className="list__info--content flex-none">
            <RegexHdrList
              onDeleteHdrRow={onDeleteHdrRow}
              onEditHdrRow={onEditHdrRow}
              onAddHdrRow={onAddHdrRow}
              onGetNmlzRuleConts={onGetNmlzRuleConts}
            />
          </div>
        ) : (
          ""
        )}
        {this.props.nmlzRule.logFormatCode === "JSON" ? (
          <div className="list__info--content flex-none">
            <JsonHdrList
              onDeleteHdrRow={onDeleteHdrRow}
              onEditHdrRow={onEditHdrRow}
              onAddHdrRow={onAddHdrRow}
              onGetNmlzRuleConts={onGetNmlzRuleConts}
            />
          </div>
        ) : (
          ""
        )}
      </React.Fragment>
    );
  }
}

export default connect(state => ({
  nmlzRule: state.nmlzRule.get("nmlzRule")
}))(HdrRule);
