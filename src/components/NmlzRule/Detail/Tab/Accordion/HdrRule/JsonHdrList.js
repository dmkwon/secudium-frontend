import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as nmlzRuleActions from "store/modules/nmlzRule";

import { Textbox } from "react-inputs-validation";
import GridTable from "components/Common/GridTable";
import IntlMessages from "util/IntlMessages";

import "../../../../style.scss";

class JsonHdrList extends Component {
  state = {
    selected: -1
  };

  componentDidUpdate(prevProps, prevState) {}

  handleChange = (value, row) => {
    if (value.match(/^[0-9]+$/) || value === "") {
      const { NmlzRuleActions } = this.props;
      NmlzRuleActions.setHdrPriority({
        index: row.index,
        value: value
      });
      NmlzRuleActions.canBeSubmit();
    }
  };

  render() {
    const { nmlzRuleHdrs } = this.props.nmlzRule;
    const {
      onAddHdrRow,
      onEditHdrRow,
      onDeleteHdrRow,
      onGetNmlzRuleConts
    } = this.props;

    return (
      <React.Fragment>
        <div className="component__title">
          <span>
            <IntlMessages id="header.rule.list" />
          </span>
          <div className="btns">
            <button
              className="btn btn--blue"
              onClick={() => {
                onAddHdrRow();
              }}
            >
              <IntlMessages id="create.rules" />
            </button>
          </div>
        </div>

        <GridTable
          className="-striped -highlight"
          getTrProps={(state, row, col, instance) => {
            if (row) {
              return {
                className: "cursor-pointer",
                onClick: (e, handleOriginal) => {
                  e.stopPropagation();
                  this.setState({
                    selected: row.index
                  });
                  onGetNmlzRuleConts(row.index);
                },
                style: {
                  backgroundColor:
                    row.index === this.state.selected ? "#282a35" : "#383b48",
                  cursor: "pointer"
                }
              };
            } else {
              return {
                style: {
                  cursor: "pointer"
                }
              };
            }
          }}
          data={nmlzRuleHdrs}
          columns={[
            {
              Header: "ID",
              id: "row",
              accessor: "nmlzRuleHdrSeq",
              style: { textAlign: "center" },
              width: 50,
              Cell: row => {
                if (nmlzRuleHdrs[row.index].status === "A") {
                  return <div></div>;
                } else {
                  return <div>{nmlzRuleHdrs[row.index].nmlzRuleHdrSeq}</div>;
                }
              }
            },
            {
              Header: "Log prefix string",
              accessor: "nmlzRuleHdrPrefix",
              width: 240,
              style: { textAlign: "left" }
            },
            {
              Header: <IntlMessages id="header.sample.log" />,
              accessor: "nmlzRuleHdrSampLog",
              width: 760,
              style: { textAlign: "left" }
            },
            {
              Header: "Header Key Field",
              accessor: "nmlzRuleHdrKeyField",
              width: 240,
              style: { textAlign: "left" }
            },
            {
              Header: <IntlMessages id="priority" />,
              accessor: "hdrPriority",
              width: 80,
              style: { textAlign: "center" },
              Cell: row => {
                return (
                  <Textbox
                    id={["hdrPriority", row.index].join("_")}
                    name="hdrPriority"
                    type="text"
                    value={row.original.hdrPriority}
                    validate={true}
                    validationOption={{locale: this.props.language}}
                    onChange={value => {
                      this.handleChange(value, row);
                    }}
                    classNameInput="form-control"
                  />
                );
              }
            },
            {
              Header: "Actions",
              accessor: "icon",
              style: { textAlign: "center" },
              Cell: row => {
                return (
                  <div>
                    <button id="btnJsonRuleDelete" className="btn btn-icon">
                      <img
                        id="imgJsonRuleDelete"
                        src="/images/common/icon_delete.png"
                        alt="icon_delete"
                        onClick={e => {
                          e.stopPropagation();
                          onDeleteHdrRow(row.index);
                        }}
                      />
                    </button>
                    <button id="btnJsonRuleEdit" className="btn btn-icon">
                      <img
                        id="imgJsonRuleEdit"
                        src="/images/common/icon_edit.png"
                        alt="icon_edit"
                        onClick={e => {
                          e.stopPropagation();
                          onEditHdrRow(row.index);
                        }}
                      />
                    </button>
                  </div>
                );
              },
              sortable: false,
              width: 96
            }
          ]}
          manual
          showPagination={false}
          showPaginationOptions={false}
          style={{
            height: "320px",
            marginBottom: 30
          }}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    hdrPopup: state.nmlzRule.get("hdrPopup")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch)
  })
)(JsonHdrList);
