import React, { Component } from 'react';
import { connect } from 'react-redux';

import RegexCrList from './RegexCrList';
import JsonCrList from './JsonCrList';

class ContRule extends Component {
  render() {
    const { onAddCrsRow, onEditCrsRow, onDeleteCrsRow, eventTypeCodeOpt } = this.props;

    return (
      <React.Fragment>
        {this.props.nmlzRule.logFormatCode === 'RGX' ? (
          <div className="list__info--content flex-none">
            <RegexCrList
              onDeleteCrsRow={onDeleteCrsRow}
              onEditCrsRow={onEditCrsRow}
              onAddCrsRow={onAddCrsRow}
              eventTypeCodeOpt={eventTypeCodeOpt}
            />
          </div>
        ) : (
          ''
        )}
        {this.props.nmlzRule.logFormatCode === 'JSON' ? (
          <div className="list__info--content flex-none">
            <JsonCrList
              onDeleteCrsRow={onDeleteCrsRow}
              onEditCrsRow={onEditCrsRow}
              onAddCrsRow={onAddCrsRow}
              eventTypeCodeOpt={eventTypeCodeOpt}
            />
          </div>
        ) : (
          ''
        )}
      </React.Fragment>
    );
  }
}

export default connect(state => ({
  nmlzRule: state.nmlzRule.get('nmlzRule'),
}))(ContRule);
