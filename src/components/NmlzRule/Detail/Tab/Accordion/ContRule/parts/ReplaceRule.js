import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import GridTable from "components/Common/GridTable";
import IntlMessages from "util/IntlMessages";

import "../../../../../style.scss";

class ReplaceRule extends Component {
  onDeleteRow = index => {
    this.props.removeNmlzRuleFieldReplaces(
      this.props.nmlzRuleCont.nmlzRuleFieldReplaces[index]
    );
    this.props.canBeContSave();
  };

  render() {
    const { onDeleteRow } = this;

    const { openNewReplacePopup, openEditReplacePopup } = this.props;

    const { nmlzRuleFields, nmlzRuleFieldReplaces } = this.props.nmlzRuleCont;

    return (
      <React.Fragment>
        <div className="component__title">
          <span>
            <IntlMessages id="replace.rule" />
          </span>
          <div className="btns">
            <button
              className="btn btn--blue"
              disabled={
                nmlzRuleFields === undefined || nmlzRuleFields.length === 0
              }
              onClick={() => {
                openNewReplacePopup(this.props.nmlzRuleCont.nmlzRuleContSeq);
              }}
            >
              <IntlMessages id="create.rules" />
            </button>
          </div>
        </div>

        <GridTable
          className="-striped -highlight"
          data={nmlzRuleFieldReplaces}
          columns={[
            {
              Header: "No.",
              id: "row",
              accessor: "number",
              style: { textAlign: "center" },
              width: 60,
              Cell: row => {
                return <div>{row.index + 1}</div>;
              }
            },
            {
              Header: <IntlMessages id="field" />,
              accessor: "field",
              width: 180
            },
            {
              Header: "In String",
              accessor: "inValue",
              width: 180
            },
            {
              Header: "Out String",
              accessor: "printValue",
              width: 180
            },
            {
              Header: "Actions",
              accessor: "icon",
              style: { textAlign: "center" },
              Cell: row => {
                return (
                  <div>
                    <button
                      id="btnReplaceRuleDelete"
                      className="btn btn-icon"
                      onClick={() => onDeleteRow(row.index)}
                    >
                      <img
                        id="imgReplaceRuleDelete"
                        src="/images/common/icon_delete.png"
                        alt="icon_delete"
                      />
                    </button>
                    <button
                      id="btnReplaceRuleEdit"
                      className="btn btn-icon"
                      onClick={() => {
                        openEditReplacePopup(nmlzRuleFieldReplaces[row.index]);
                      }}
                    >
                      <img
                        id="imgReplaceRuleEdit"
                        src="/images/common/icon_edit.png"
                        alt="icon_edit"
                      />
                    </button>
                  </div>
                );
              },
              sortable: false,
              width: 96
            }
          ]}
          manual
          showPagination={false}
          showPaginationOptions={false}
          style={{
            height: "320px",
            marginBottom: 30
          }}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont")
  }),
  dispatch => {
    return bindActionCreators(
      {
        openNewReplacePopup: nmlzRuleActions.openNewReplacePopup,
        openEditReplacePopup: nmlzRuleActions.openEditReplacePopup,
        removeNmlzRuleFieldReplaces:
          nmlzRuleActions.removeNmlzRuleFieldReplaces,
        canBeContSave: nmlzRuleActions.canBeContSave
      },
      dispatch
    );
  }
)(ReplaceRule);
