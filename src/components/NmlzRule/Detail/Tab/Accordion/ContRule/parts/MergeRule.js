import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import GridTable from "components/Common/GridTable";
import IntlMessages from "util/";

import "../../../../../style.scss";

class MergeRule extends Component {
  onDeleteRow = index => {
    this.props.removeNmlzRuleFieldMerges(
      this.props.nmlzRuleCont.nmlzRuleFieldMerges[index]
    );
    this.props.canBeContSave();
  };

  render() {
    const { onDeleteRow } = this;

    const { openNewMergePopup, openEditMergePopup } = this.props;

    const { nmlzRuleFields, nmlzRuleFieldMerges } = this.props.nmlzRuleCont;

    return (
      <React.Fragment>
        <div className="component__title">
          <span>
            <IntlMessages id="merge.rule" />
          </span>
          <div className="btns">
            <button
              className="btn btn--blue"
              disabled={
                nmlzRuleFields === undefined || nmlzRuleFields.length === 0
              }
              onClick={() => {
                openNewMergePopup(this.props.nmlzRuleCont.nmlzRuleContSeq);
              }}
            >
              <IntlMessages id="create.rules" />
            </button>
          </div>
        </div>

        <GridTable
          className="-striped -highlight"
          data={nmlzRuleFieldMerges}
          columns={[
            {
              Header: "No.",
              id: "row",
              accessor: "number",
              style: { textAlign: "center" },
              width: 60,
              Cell: row => {
                return <div>{row.index + 1}</div>;
              }
            },
            {
              Header: <IntlMessages id="input.field1" />,
              accessor: "field1",
              width: 180
            },
            {
              Header: <IntlMessages id="input.field2" />,
              accessor: "field2",
              width: 180
            },
            {
              Header: <IntlMessages id="output.field" />,
              accessor: "printField",
              width: 180
            },
            {
              Header: "Actions",
              accessor: "icon",
              style: { textAlign: "center" },
              Cell: row => {
                return (
                  <div>
                    <button
                      id="btnMergeRuleDelete"
                      className="btn btn-icon"
                      onClick={() => onDeleteRow(row.index)}
                    >
                      <img
                        id="imgMergeRuleDelete"
                        src="/images/common/icon_delete.png"
                        alt="icon_delete"
                      />
                    </button>
                    <button
                      id="btnMergeRuleEdit"
                      className="btn btn-icon"
                      onClick={() => {
                        openEditMergePopup(nmlzRuleFieldMerges[row.index]);
                      }}
                    >
                      <img
                        id="imgMergeRuleEdit"
                        src="/images/common/icon_edit.png"
                        alt="icon_edit"
                      />
                    </button>
                  </div>
                );
              },
              sortable: false,
              width: 96
            }
          ]}
          manual
          showPagination={false}
          showPaginationOptions={false}
          style={{
            height: "320px",
            marginBottom: 30
          }}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont")
  }),
  dispatch => {
    return bindActionCreators(
      {
        openNewMergePopup: nmlzRuleActions.openNewMergePopup,
        openEditMergePopup: nmlzRuleActions.openEditMergePopup,
        removeNmlzRuleFieldMerges: nmlzRuleActions.removeNmlzRuleFieldMerges,
        canBeContSave: nmlzRuleActions.canBeContSave
      },
      dispatch
    );
  }
)(MergeRule);
