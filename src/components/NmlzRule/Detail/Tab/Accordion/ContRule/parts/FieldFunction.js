import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import GridTable from "components/Common/GridTable";
import IntlMessages from "util/IntlMessages";

import "../../../../../style.scss";

class FieldFunction extends Component {
  onDeleteRow = index => {
    this.props.removeNmlzRuleFieldFuncs(
      this.props.nmlzRuleCont.nmlzRuleFieldFuncs[index]
    );
    this.props.canBeContSave();
  };

  render() {
    const { onDeleteRow } = this;

    const { openNewFieldFuncPopup, openEditFieldFuncPopup } = this.props;

    const { nmlzRuleFields, nmlzRuleFieldFuncs } = this.props.nmlzRuleCont;

    return (
      <React.Fragment>
        <div className="component__title">
          <span>
            <IntlMessages id="field.func" />
          </span>
          <div className="btns">
            <button
              className="btn btn--blue"
              disabled={
                nmlzRuleFields === undefined || nmlzRuleFields.length === 0
              }
              onClick={() => {
                openNewFieldFuncPopup(this.props.nmlzRuleCont.nmlzRuleContSeq);
              }}
            >
              <IntlMessages id="reg.func" />
            </button>
          </div>
        </div>

        <GridTable
          className="-striped -highlight"
          data={nmlzRuleFieldFuncs}
          columns={[
            {
              Header: "No.",
              id: "row",
              accessor: "number",
              style: { textAlign: "center" },
              width: 60,
              Cell: row => {
                return <div>{row.index + 1}</div>;
              }
            },
            {
              Header: <IntlMessages id="field" />,
              accessor: "field",
              width: 220
            },
            {
              Header: <IntlMessages id="function" />,
              accessor: "fieldFuncCodeNm",
              width: 380
            },
            {
              Header: "Argument",
              accessor: "fieldFuncArg",
              width: 390
            },
            {
              Header: <IntlMessages id="output.field" />,
              accessor: "printField",
              width: 300
            },
            {
              Header: "Actions",
              accessor: "icon",
              style: { textAlign: "center" },
              Cell: row => {
                return (
                  <div>
                    <button
                      id="btnFieldFunctionDelete"
                      className="btn btn-icon"
                      onClick={() => {
                        onDeleteRow(row.index);
                      }}
                    >
                      <img
                        id="imgFieldFunctionDelete"
                        src="/images/common/icon_delete.png"
                        alt="icon_delete"
                      />
                    </button>
                    <button
                      id="btnFieldFunctionEdit"
                      className="btn btn-icon"
                      onClick={() => {
                        openEditFieldFuncPopup(nmlzRuleFieldFuncs[row.index]);
                      }}
                    >
                      <img
                        id="imgFieldFunctionEdit"
                        src="/images/common/icon_edit.png"
                        alt="icon_edit"
                      />
                    </button>
                  </div>
                );
              },
              sortable: false,
              width: 96
            }
          ]}
          manual
          showPagination={false}
          showPaginationOptions={false}
          style={{
            width: "1460px",
            height: "320px",
            marginBottom: 30
          }}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont")
  }),
  dispatch => {
    return bindActionCreators(
      {
        openNewFieldFuncPopup: nmlzRuleActions.openNewFieldFuncPopup,
        openEditFieldFuncPopup: nmlzRuleActions.openEditFieldFuncPopup,
        removeNmlzRuleFieldFuncs: nmlzRuleActions.removeNmlzRuleFieldFuncs,
        canBeContSave: nmlzRuleActions.canBeContSave
      },
      dispatch
    );
  }
)(FieldFunction);
