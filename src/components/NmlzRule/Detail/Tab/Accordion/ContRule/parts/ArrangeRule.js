import React from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";

import _ from "@lodash";

import { Textbox } from "react-inputs-validation";

import GridTable from "components/Common/GridTable";

import IntlMessages from "util/IntlMessages";

import "../../../../../style.scss";

class ArrangeRule extends React.PureComponent {
  state = {
    fullMatch: ""
  };

  componentDidUpdate(prevProps, prevState) {
    /**
     * delete Popup 닫친 후 처리
     */
    if (
      prevProps.fieldSelectPopup.props.open &&
      !this.props.fieldSelectPopup.props.open
    ) {
      if (this.props.fieldSelectPopup.props.from === "arrange_field") {
        if (this.props.selectedField && !_.isEmpty(this.props.selectedField)) {
          this.handleChange(
            this.props.fieldSelectPopup.data,
            "field",
            this.props.selectedField.field
          );
          this.handleChange(
            this.props.fieldSelectPopup.data,
            "fieldId",
            this.props.selectedField.fieldId
          );
        }
      } else {
        if (this.props.fieldSelectPopup.props.from === "arrange_subfield") {
          if (
            this.props.selectedField &&
            !_.isEmpty(this.props.selectedField)
          ) {
            this.handleChange(
              this.props.fieldSelectPopup.data,
              "subField",
              this.props.selectedField.field
            );
            this.handleChange(
              this.props.fieldSelectPopup.data,
              "subFieldId",
              this.props.selectedField.fieldId
            );
          }
        }
      }
    }
  }

  handleChange = (index, name, value) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRuleField({
      index: index,
      name: name,
      value: value
    });
    NmlzRuleActions.canBeContSave();
  };

  makeColumns = logFormatCode => {
    const { NmlzRuleActions } = this.props;

    let columns = [];

    if (logFormatCode === "RGX") {
      columns.push(
        {
          Header: "No.",
          id: "row",
          accessor: "matchNo",
          style: { textAlign: "center" },
          width: 50
        },
        {
          Header: "Match Result",
          accessor: "matchResult",
          style: { textAlign: "left" },
          width: 200
        }
      );
    } else if (logFormatCode === "JSON") {
      columns.push({
        Header: "No.",
        id: "row",
        accessor: "number",
        style: { textAlign: "center" },
        width: 50,
        Cell: row => {
          return <div>{row.index + 1}</div>;
        }
      });
      columns.push({
        Header: <IntlMessages id="json.key" />,
        accessor: "jsonKey",
        width: 200,
        Cell: row => {
          return (
            <Textbox
              id={["jsonKey", row.index].join("_")}
              name="jsonKey"
              type="text"
              value={row.original.jsonKey}
              onChange={value => {
                this.handleChange(row.index, "jsonKey", value);
              }}
              classNameInput="form-control"
            />
          );
        }
      });
    }

    columns.push(
      {
        Header: <IntlMessages id="secudium.field" />,
        accessor: "fieldInfo",
        style: { textAlign: "left" },
        Cell: row => {
          return (
            <div className="flex-box-arrange-row">
              <div>{row.original.field}</div>
              <div>
                <button
                  className="btn"
                  onClick={() => {
                    NmlzRuleActions.openFieldSelectPopup({
                      from: "arrange_field",
                      data: row.index
                    });
                  }}
                >
                  <IntlMessages id="field.select" />
                </button>
              </div>
            </div>
          );
        },
        width: 260
      },
      {
        Header: "Sub Field",
        accessor: "subfieldInfo",
        width: 200,
        Cell: row => {
          return (
            <div className="flex-box-arrange-row">
              <div>{row.original.subField}</div>
              <div>
                <button
                  className="btn"
                  onClick={() => {
                    NmlzRuleActions.openFieldSelectPopup({
                      from: "arrange_subfield",
                      data: row.index
                    });
                  }}
                >
                  <IntlMessages id="field.select" />
                </button>
              </div>
            </div>
          );
        }
      }
    );

    if (logFormatCode === "JSON") {
      columns.push({
        Header: "Actions",
        accessor: "icon",
        style: { textAlign: "center" },
        Cell: row => {
          return (
            <div>
              <button id="btnJsonArrangeDelete" className="btn btn-icon">
                <img
                  id="imgJsonArrangeDelete"
                  src="/images/common/icon_delete.png"
                  alt="icon_delete"
                  onClick={e => {
                    e.stopPropagation();
                    const { nmlzRuleFields } = this.props.nmlzRuleCont;
                    NmlzRuleActions.removeNmlzRuleField(
                      nmlzRuleFields[row.index]
                    );
                    NmlzRuleActions.canBeContSave();
                  }}
                />
              </button>
            </div>
          );
        },
        sortable: false,
        width: 56
      });
    }

    return columns;
  };

  getMatchVal(match, str) {
    let val =
      match.s || (match.i === undefined ? "" : str.substr(match.i, match.l));
    return val;
  }

  makeIndex = index => {
    this.handleChange(index, "matchNo", index + 1);
    return index + 1;
  };

  render() {
    const { NmlzRuleActions, regexResult } = this.props;

    const { logFormatCode } = this.props.nmlzRule;

    const { nmlzRuleFields, contSampLog } = this.props.nmlzRuleCont;

    const renderRegexResult = matches => {
      return matches.length > 0
        ? matches.map((match, index) => {
            return (
              <div key={index}>
                <div className="match">
                  <span>{"Match " + ++index}</span>
                </div>
                <table key={index} className="table table--dark table--info">
                  <colgroup>
                    <col width="50px" />
                    <col width="50px" />
                    <col width="104px" />
                    <col width="250px" />
                    <col width="250px" />
                  </colgroup>
                  <thead>
                    <tr>
                      <td colSpan="2">Full Match</td>
                      <td colSpan="3" style={{ textAlign: "left" }}>
                        <Textbox
                          id={["fieldDefault", index].join("_")}
                          name="fieldDefault"
                          type="text"
                          disabled={false}
                          value={this.getMatchVal(match, contSampLog)}
                          classNameInput="form-control"
                        />
                      </td>
                    </tr>
                    <tr>
                      <th style={{ textAlign: "center" }} colSpan="3">
                        Match Result
                      </th>
                      <th style={{ textAlign: "center" }}>
                        <IntlMessages id="secudium.field" />
                      </th>
                      <th style={{ textAlign: "center" }}>
                        <IntlMessages id="sub.field" />
                      </th>
                    </tr>
                  </thead>
                  <tbody>{renderResultRows(match, index)}</tbody>
                </table>
              </div>
            );
          })
        : [];
    };

    const renderResultRows = (match, matchIndex) => {
      return match.groups.length > 0
        ? match.groups.map((group, index) => {
            if (group.s !== undefined) {
              return (
                <tr key={index}>
                  <td style={{ textAlign: "center" }}>
                    {this.makeIndex(index)}
                  </td>
                  <td colSpan="2" style={{ textAlign: "left" }}>
                    {group.s}
                  </td>
                  <td>
                    <div className="flex-box-arrange-row">
                      <div className="text-baseline" style={{ width: "150px" }}>
                        <span style={{ verticalAlign: "middle" }}>
                          {nmlzRuleFields.length > 0 &&
                            nmlzRuleFields[index] &&
                            nmlzRuleFields[index].field}
                        </span>
                        {nmlzRuleFields.length > 0 &&
                          nmlzRuleFields[index] &&
                          nmlzRuleFields[index].field && (
                            <button
                              className="btn btn--dark-close"
                              style={{ backgroundSize: "cover" }}
                              onClick={() => {
                                this.handleChange(index, "field", "");
                                this.handleChange(index, "fieldId", "");
                              }}
                            />
                          )}
                      </div>
                      {matchIndex === 1 && (
                        <button
                          className="btn"
                          onClick={() => {
                            NmlzRuleActions.openFieldSelectPopup({
                              from: "arrange_field",
                              data: index
                            });
                          }}
                        >
                          <IntlMessages id="field.select" />
                        </button>
                      )}
                    </div>
                  </td>
                  <td>
                    <div className="flex-box-arrange-row">
                      <div className="text-baseline" style={{ width: "150px" }}>
                        <span style={{ verticalAlign: "middle" }}>
                          {nmlzRuleFields.length > 0 &&
                            nmlzRuleFields[index] &&
                            nmlzRuleFields[index].subField}
                        </span>
                        {nmlzRuleFields.length > 0 &&
                          nmlzRuleFields[index] &&
                          nmlzRuleFields[index].subField && (
                            <button
                              className="btn btn--dark-close"
                              style={{ backgroundSize: "cover" }}
                              onClick={() => {
                                this.handleChange(index, "subField", "");
                                this.handleChange(index, "subFieldId", "");
                              }}
                            />
                          )}
                      </div>
                      {matchIndex === 1 && (
                        <button
                          className="btn"
                          onClick={() => {
                            NmlzRuleActions.openFieldSelectPopup({
                              from: "arrange_subfield",
                              data: index
                            });
                          }}
                        >
                          <IntlMessages id="field.select" />
                        </button>
                      )}
                    </div>
                  </td>

                  {/* <td>
                    <Textbox
                      id={['fieldDefault', index].join('_')}
                      name="fieldDefault"
                      type="text"
                      value={
                        nmlzRuleFields.length > 0 &&
                        nmlzRuleFields[index] &&
                        nmlzRuleFields[index].fieldDefault
                      }
                      disabled={matchIndex > 1}
                      onChange={value => {
                        this.handleChange(index, 'fieldDefault', value);
                      }}
                      classNameInput="form-control"
                    />
                  </td> */}
                </tr>
              );
            }
          })
        : [];
    };

    return (
      <React.Fragment>
        <div className="component__title">
          <div style={{ width: "720px", height: "20px" }}>
            <IntlMessages id="arrange.rule" />
          </div>
          {logFormatCode === "JSON" && (
            <div className="btns">
              <button
                className="btn btn--blue"
                onClick={() => {
                  NmlzRuleActions.addNmlzRuleField(
                    this.props.nmlzRuleCont.nmlzRuleContSeq
                  );
                  NmlzRuleActions.canBeContSave();
                }}
              >
                <IntlMessages id="create.rules" />
              </button>
            </div>
          )}
        </div>
        {logFormatCode === "RGX" &&
          regexResult.result &&
          !regexResult.result.error &&
          regexResult.result.matches.length > 0 && (
            <div
              className="table-wrapper"
              style={{
                height: "300px",
                overflowY: "scroll"
              }}
            >
              {renderRegexResult(regexResult.result.matches)}
            </div>
          )}
        {logFormatCode === "RGX" &&
          regexResult.result &&
          (regexResult.result.error ||
            regexResult.result.matches.length === 0) && (
            <div className="noData">
              <IntlMessages id="no.data" />
            </div>
          )}
        {logFormatCode === "JSON" && (
          <GridTable
            className="-striped -highlight"
            data={nmlzRuleFields}
            columns={this.makeColumns(logFormatCode)}
            manual
            showPagination={false}
            showPaginationOptions={false}
            style={{
              height: "240px",
              marginBottom: 30
            }}
          />
        )}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    nmlzRuleCont: state.nmlzRule.get("nmlzRuleCont"),
    selectedField: state.nmlzRule.get("selectedField"),
    fieldSelectPopup: state.nmlzRule.get("fieldSelectPopup"),
    regexResult: state.nmlzRule.get("regexResult")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch)
  })
)(ArrangeRule);
