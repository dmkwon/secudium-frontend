import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import _ from "@lodash";

import { Textbox } from "react-inputs-validation";
import GridTable from "components/Common/GridTable";
import IntlMessages from "util/IntlMessages";

import "../../../../style.scss";

class RegexCrList extends Component {
  componentDidUpdate(prevProps, prevState) {}

  handleChange = (value, row) => {
    if (value.match(/^[0-9]+$/) || value === "") {
      const { NmlzRuleActions } = this.props;
      NmlzRuleActions.setContPriority({
        index: row.index,
        value: value
      });
      NmlzRuleActions.canBeSubmit();
    }
  };

  getEventTypeNm = index => {
    const { eventTypeCodeOpt, nmlzRule, currentHdrIdx } = this.props;
    if (
      nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts[index]
        .eventTypeCode !== undefined
    ) {
      let eventTypeNm = [];
      for (
        let i = 0;
        i <
        nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts[index].eventTypeCode
          .length;
        i++
      ) {
        let findItem = _.find(eventTypeCodeOpt.data, [
          "commCode",
          nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts[index]
            .eventTypeCode[i]
        ]);
        if (findItem !== undefined) {
          eventTypeNm.push(findItem.commCodeNm);
        }
      }
      return _.join(eventTypeNm, ",");
    } else {
      return "";
    }
  };

  render() {
    const {
      onAddCrsRow,
      onEditCrsRow,
      onDeleteCrsRow,
      currentHdrIdx,
      nmlzRule
    } = this.props;

    return (
      <React.Fragment>
        <div className="component__title">
          <span>
            <IntlMessages id="content.rule.list" />
          </span>
          <div className="btns">
            <button
              className="btn btn--blue"
              disabled={!this.props.canBeAddCont}
              onClick={() => {
                onAddCrsRow();
              }}
            >
              <IntlMessages id="create.rules" />
            </button>
          </div>
        </div>

        <GridTable
          className="-striped -highlight"
          getTdProps={(state, row, col, instance) => {
            return {
              className: "cursor-pointer",
              onClick: (e, handleOriginal) => {
                e.stopPropagation();
                if (col.id !== "contPriority") {
                  if (row) {
                    onEditCrsRow(row.index);
                  }
                }
              }
            };
          }}
          data={
            currentHdrIdx === -1
              ? []
              : nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts !== undefined
              ? nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts
              : []
          }
          columns={[
            {
              Header: "ID",
              id: "row",
              accessor: "nmlzRuleContSeq",
              style: { textAlign: "center" },
              width: 50,
              Cell: row => {
                if (currentHdrIdx !== -1) {
                  if (
                    nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts[
                      row.index
                    ].status === "A"
                  ) {
                    return <div></div>;
                  } else {
                    return (
                      <div>
                        {
                          nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts[
                            row.index
                          ].nmlzRuleContSeq
                        }
                      </div>
                    );
                  }
                } else {
                  return <div></div>;
                }
              }
            },
            {
              Header: "Event Type",
              accessor: "eventTypeCode",
              width: 100,
              style: { textAlign: "center" },
              Cell: row => {
                return <div>{this.getEventTypeNm(row.index)}</div>;
              }
            },
            {
              Header: <IntlMessages id="regex" />,
              accessor: "contRegexp",
              width: 380,
              style: { textAlign: "left" }
            },
            {
              Header: <IntlMessages id="sample.log" />,
              accessor: "contSampLog",
              width: 380,
              style: { textAlign: "left" }
            },
            {
              Header: <IntlMessages id="desc" />,
              accessor: "contDesc",
              width: 380,
              style: { textAlign: "left" }
            },
            {
              Header: <IntlMessages id="priority" />,
              accessor: "contPriority",
              width: 80,
              style: { textAlign: "center" },
              Cell: row => {
                return (
                  <Textbox
                    id={["contPriority", row.index].join("_")}
                    name="contPriority"
                    type="text"
                    value={row.original.contPriority}
                    onChange={value => {
                      this.handleChange(value, row);
                    }}
                    classNameInput="form-control"
                  />
                );
              }
            },
            // {
            //   Header: '기본값 적용 여부',
            //   accessor: 'defaultUseYn',
            //   width: 100,
            //   style: { textAlign: 'center' },
            //   Cell: row => {
            //     return row.original.defaultUseYn === 'Y' ? '적용' : '미적용';
            //   },
            // },
            {
              Header: "Actions",
              accessor: "icon",
              style: { textAlign: "center" },
              Cell: row => {
                return (
                  <div>
                    <button id="btnRegexRuleDelete" className="btn btn-icon">
                      <img
                        id="imgRegexRuleDelete"
                        src="/images/common/icon_delete.png"
                        alt="icon_delete"
                        onClick={e => {
                          e.stopPropagation();
                          onDeleteCrsRow(row.index);
                        }}
                      />
                    </button>
                    <button id="btnRegexRuleEdit" className="btn btn-icon">
                      <img
                        id="imgRegexRuleEdit"
                        src="/images/common/icon_edit.png"
                        alt="icon_edit"
                        onClick={e => {
                          e.stopPropagation();
                          onEditCrsRow(row.index);
                        }}
                      />
                    </button>
                  </div>
                );
              },
              sortable: false,
              width: 96
            }
          ]}
          manual
          showPagination={false}
          showPaginationOptions={false}
          style={{
            height: "320px",
            marginBottom: 30
          }}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    contPopup: state.nmlzRule.get("contPopup"),
    //nmlzRuleConts: state.nmlzRule.get('nmlzRuleConts'),
    currentHdrIdx: state.nmlzRule.get("currentHdrIdx"),
    canBeAddCont: state.nmlzRule.get("canBeAddCont")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch)
  })
)(RegexCrList);
