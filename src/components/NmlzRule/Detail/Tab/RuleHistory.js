import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "@lodash";

import * as nmlzRuleActions from "store/modules/nmlzRule";
import * as comCodeActions from "store/modules/code";

import { PAGE_TYPE, PAGE_ACTION_TYPE } from "store/modules/nmlzRule";

import GridTable from "components/Common/GridTable";
import Loading from "components/Common/Loading";
import { Textbox } from "react-inputs-validation";

import "antd/dist/antd.css";
import { Select as ASelect } from "antd";
import IntlMessages from "util/IntlMessages";

import RollbackNmlzRulePopup from "../../popups/RollbackNmlzRulePopup";
import DuplicatePopup from "../../popups/DuplicatePopup";

class RuleHistory extends Component {
  state = {
    selectedVendors: [],
    selectedSatcs: [],
    selectedModels: [],
    selected: 0,
    selectedHdr: -1,
    eventTypeCodeOpt: []
  };

  doGetNmlzRulesHists = async () => {
    const { NmlzRuleActions, navPageTarget } = this.props;
    try {
      if (navPageTarget.detail === PAGE_ACTION_TYPE.EDIT) {
        this.onGetNmlzRuleContHists(-1);
        await NmlzRuleActions.getNmlzRulesHists(navPageTarget.data);
        this.doGetNmlzRulesHist(
          this.props.nmlzRuleHists.content[0].nmlzRuleHistSeq
        );
      }
    } catch (err) {
      console.error(err);
    }
  };

  doGetNmlzRulesHist = async nmlzRuleHistSeq => {
    const { NmlzRuleActions } = this.props;
    try {
      await NmlzRuleActions.getNmlzRulesHist(nmlzRuleHistSeq);
      this.setInitVendor();
    } catch (err) {
      console.error(err);
    }
  };

  setInitVendor = () => {
    let keys = _.groupBy(
      this.props.nmlzRuleHist.nmlzRuleAgentHists,
      "agentVendorNm"
    );
    const selectedVendors = Object.keys(keys);
    this.setState(
      {
        selectedVendors: selectedVendors
      },
      () => {
        this.setInitSatc();
      }
    );
  };

  setInitSatc = () => {
    let keys = _.groupBy(
      this.props.nmlzRuleHist.nmlzRuleAgentHists,
      "serviceItemCodeNm"
    );
    const selectedSatcs = Object.keys(keys);
    this.setState(
      {
        selectedSatcs: selectedSatcs
      },
      () => {
        this.setInitModel();
      }
    );
  };

  setInitModel = () => {
    let keys = _.groupBy(
      this.props.nmlzRuleHist.nmlzRuleAgentHists,
      "agentModelNm"
    );
    const selectedModels = Object.keys(keys);
    this.setState({
      selectedModels: selectedModels[0] === "undefined" ? [] : selectedModels
    });
  };

  convertUseYn = useYn => {
    if (useYn === "Y") return <IntlMessages id="use" />;
    else if (useYn === "N") return <IntlMessages id="unused" />;
  };

  onGetNmlzRuleContHists = index => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.getNmlzRuleContHists({
      hdrIndex: index
    });
  };

  getEventType = async () => {
    const { ComCodeActions } = this.props;
    let eventTypeCodeOpt = await ComCodeActions.getCodesByParentCode(
      "EVENT_TYPE"
    );
    this.setState({
      eventTypeCodeOpt: eventTypeCodeOpt
    });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.rollbackPopup.props.open &&
      !this.props.rollbackPopup.props.open
    ) {
      if (this.props.isExistDuplicate) {
        const { NmlzRuleActions } = this.props;
        NmlzRuleActions.openDuplicatePopup({
          data: null
        });
      } else {
        this.setState({
          selected: 0
        });
        this.doGetNmlzRulesHists();
        this.getEventType();
      }
    }
  }

  getEventTypeNm = index => {
    const { nmlzRuleHist, currentHdrHistIdx } = this.props;
    const { eventTypeCodeOpt } = this.state;

    console.log("1", eventTypeCodeOpt);
    console.log("2", nmlzRuleHist.nmlzRuleHdrHists);
    let eventTypeNm = [];

    if (
      nmlzRuleHist.nmlzRuleHdrHists[currentHdrHistIdx].nmlzRuleContHists[index]
        .eventTypeCode !== undefined
    ) {
      for (
        let i = 0;
        i <
        nmlzRuleHist.nmlzRuleHdrHists[currentHdrHistIdx].nmlzRuleContHists[
          index
        ].eventTypeCode.length;
        i++
      ) {
        console.log(eventTypeCodeOpt);
        console.log(
          nmlzRuleHist.nmlzRuleHdrHists[currentHdrHistIdx].nmlzRuleContHists[
            index
          ].eventTypeCode[i]
        );
        let findItem = _.find(eventTypeCodeOpt.data, [
          "commCode",
          nmlzRuleHist.nmlzRuleHdrHists[currentHdrHistIdx].nmlzRuleContHists[
            index
          ].eventTypeCode[i]
        ]);
        console.log(findItem);
        if (findItem !== undefined) {
          eventTypeNm.push(findItem.commCodeNm);
        }
      }
      console.log(eventTypeNm);
      return _.join(eventTypeNm, ",");
    } else {
      return "";
    }
  };

  componentDidMount() {
    this.doGetNmlzRulesHists();
    this.getEventType();
  }

  render() {
    const { doGetNmlzRulesHist } = this;

    const { selectedVendors, selectedSatcs, selectedModels } = this.state;

    const { NmlzRuleActions, nmlzRuleHist, currentHdrHistIdx } = this.props;

    const { content } = this.props.nmlzRuleHists;

    const makeHdrColumn = logFormatCode => {
      if (logFormatCode === "RGX") {
        return [
          {
            Header: "No.",
            id: "row",
            accessor: "number",
            style: { textAlign: "center" },
            width: 50,
            Cell: row => {
              return <div>{row.index + 1}</div>;
            }
          },
          {
            Header: <IntlMessages id="header.regex" />,
            accessor: "nmlzRuleHdrRegexp",
            width: 200,

            style: { textAlign: "left" }
          },
          {
            Header: <IntlMessages id="header.sample.log" />,
            accessor: "nmlzRuleHdrSampLog",
            width: 170,
            style: { textAlign: "left" }
          },
          {
            Header: "Log prefix string",
            accessor: "nmlzRuleHdrPrefix",
            width: 120,

            style: { textAlign: "left" }
          },
          {
            Header: "Header Key Field",
            accessor: "nmlzRuleHdrKeyField",
            width: 140,
            style: { textAlign: "left" }
          }
        ];
      } else if (logFormatCode === "JSON") {
        return [
          {
            Header: "No.",
            id: "row",
            accessor: "number",
            style: { textAlign: "center" },
            width: 50,
            Cell: row => {
              return <div>{row.index + 1}</div>;
            }
          },
          {
            Header: <IntlMessages id="header.sample.log" />,
            accessor: "nmlzRuleHdrSampLog",
            width: 240,
            style: { textAlign: "left" }
          },
          {
            Header: "Log prefix string",
            accessor: "nmlzRuleHdrPrefix",
            width: 240,
            style: { textAlign: "left" }
          },
          {
            Header: "Header Key Field",
            accessor: "nmlzRuleHdrKeyField",
            width: 230,
            style: { textAlign: "left" }
          }
        ];
      } else {
        return [];
      }
    };

    const makeContColumn = logFormatCode => {
      if (logFormatCode === "RGX") {
        return [
          {
            Header: "No.",
            id: "row",
            accessor: "number",
            style: { textAlign: "center" },
            width: 60,
            Cell: row => {
              return <div>{row.index + 1}</div>;
            }
          },
          {
            Header: "Event Type",
            accessor: "eventTypeCode",
            width: 120,
            style: { textAlign: "center" },
            Cell: row => {
              if (row) {
                return <div>{this.getEventTypeNm(row.index)}</div>;
              } else {
                return "";
              }
            }
          },
          {
            Header: <IntlMessages id="regex" />,
            accessor: "contRegexp",
            width: 200,
            style: { textAlign: "left" }
          },
          {
            Header: <IntlMessages id="sample.log" />,
            accessor: "contSampLog",
            width: 180,
            style: { textAlign: "left" }
          },
          {
            Header: <IntlMessages id="desc" />,
            accessor: "contDesc",
            width: 120,
            style: { textAlign: "left" }
          }
        ];
      } else if (logFormatCode === "JSON") {
        return [
          {
            Header: "No.",
            id: "row",
            accessor: "number",
            style: { textAlign: "center" },
            width: 60,
            Cell: row => {
              return <div>{row.index + 1}</div>;
            }
          },
          {
            Header: "Event Type",
            accessor: "eventTypeCode",
            width: 120,
            style: { textAlign: "center" }
          },
          {
            Header: "Json",
            accessor: "json",
            width: 340,
            style: { textAlign: "left" },
            Cell: row => {
              let jsonString = [];
              for (
                let i = 0;
                i <
                nmlzRuleHist.nmlzRuleHdrHists[currentHdrHistIdx]
                  .nmlzRuleContHists[row.index].nmlzRuleFieldHists.length;
                i++
              ) {
                let item = [];
                item.push(
                  nmlzRuleHist.nmlzRuleHdrHists[currentHdrHistIdx]
                    .nmlzRuleContHists[row.index].nmlzRuleFieldHists[i].jsonKey
                );
                item.push(
                  nmlzRuleHist.nmlzRuleHdrHists[currentHdrHistIdx]
                    .nmlzRuleContHists[row.index].nmlzRuleFieldHists[i].field
                );
                jsonString.push(_.join(item, ":"));
              }

              return <div>{_.join(jsonString, ",")}</div>;
            }
          },
          {
            Header: <IntlMessages id="sample.log" />,
            accessor: "contSampLog",
            width: 240,
            style: { textAlign: "left" }
          }
        ];
      } else {
        return [];
      }
    };

    const {
      nmlzRuleNm,
      logFormatCode,
      nmlzRuleDesc,
      useYn,
      nmlzRuleHdrHists,
      nmlzRuleChgReason
    } = this.props.nmlzRuleHist;

    return (
      <div className="contents history">
        <div className="top">
          <div className="flex-box">
            <div className="left">
              <GridTable
                className="-striped -highlight"
                getTrProps={(state, rowInfo, column) => {
                  if (rowInfo) {
                    return {
                      onClick: (e, handleOriginal) => {
                        this.setState({
                          selected: rowInfo.index,
                          selectedHdr: -1
                        });
                        doGetNmlzRulesHist(rowInfo.original.nmlzRuleHistSeq);
                      },
                      style: {
                        backgroundColor:
                          rowInfo.index === this.state.selected
                            ? "#282a35"
                            : "#2f323c",
                        cursor: "pointer"
                      }
                    };
                  } else {
                    return {
                      style: {
                        cursor: "pointer"
                      }
                    };
                  }
                }}
                data={content}
                columns={[
                  {
                    Header: "No.",
                    id: "row",
                    accessor: "number",
                    style: { textAlign: "center" },
                    width: 50,
                    Cell: row => {
                      return <div>{row.index + 1}</div>;
                    }
                  },
                  {
                    Header: <IntlMessages id="normal.rule.name" />,
                    accessor: "nmlzRuleNm",
                    style: { textAlign: "left" },
                    width: 160
                  },
                  {
                    Header: <IntlMessages id="log.method" />,
                    accessor: "logFormatCode",
                    style: { textAlign: "center" },
                    width: 80
                  },
                  {
                    Header: <IntlMessages id="use.yn" />,
                    accessor: "useYn",
                    style: { textAlign: "center" },
                    width: 80,
                    Cell: row => {
                      return this.convertUseYn(row.original.useYn);
                    }
                  },
                  {
                    Header: <IntlMessages id="desc" />,
                    accessor: "nmlzRuleDesc",
                    style: { textAlign: "left" },
                    width: 80
                  },
                  {
                    Header: <IntlMessages id="reason.change" />,
                    accessor: "nmlzRuleChgReason",
                    style: { textAlign: "left" },
                    width: this.props.language === 'ko' ? 120 : 120
                  },
                  {
                    Header: <IntlMessages id="change.modifier" />,
                    accessor: "regUsrNm",
                    style: { textAlign: "center" },
                    width: 80
                  },
                  {
                    Header: <IntlMessages id="change.datetime" />,
                    accessor: "regDateFormatted",
                    style: { textAlign: "center" },
                    width: 120
                  },
                  {
                    Header: " Actions",
                    accessor: "action",
                    width: 70,
                    style: { textAlign: "center" },
                    sortable: false,
                    Cell: row => {
                      if (row.original.lastVersion) {
                        return <React.Fragment></React.Fragment>;
                      } else {
                        return (
                          <React.Fragment>
                            <button
                              type="button"
                              className="btn"
                              style={{
                                height: "28px",
                                width: "28px",
                                padding: "0"
                              }}
                              onClick={e => {
                                e.stopPropagation();
                                NmlzRuleActions.openRollBackPopup(
                                  content[row.index]
                                );
                              }}
                            >
                              <i className="fas fa-redo" />
                            </button>
                          </React.Fragment>
                        );
                      }
                    }
                  }
                ]}
                defaultPageSize={20}
              />
            </div>

            <div className="right">
              <div className="component__title">
                <span>
                  <IntlMessages id="basic.info" />
                </span>
              </div>
              <table className="table table--dark table--info">
                <colgroup>
                  <col width="150px" />
                  <col />
                </colgroup>
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="rule.name" />
                    </th>
                    <td>{nmlzRuleNm}</td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="log.format" />
                    </th>
                    <td>{logFormatCode}</td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="use.yn" />
                    </th>
                    <td>{this.convertUseYn(useYn)}</td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="desc" />
                    </th>
                    <td>{nmlzRuleDesc}</td>
                  </tr>
                </tbody>
              </table>
              <div className="component__title">
                <span>Device Type</span>
              </div>
              <table className="table table--dark table--info">
                <colgroup>
                  <col width="150px" />
                  <col />
                </colgroup>
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="vendor" />
                    </th>
                    <td className="select-td">
                      <ASelect
                        mode="multiple"
                        style={{ width: "100%" }}
                        value={selectedVendors}
                        placeholder=""
                        disabled={true}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="equip.type" />
                    </th>
                    <td className="select-td">
                      <ASelect
                        mode="multiple"
                        style={{ width: "100%" }}
                        value={selectedSatcs}
                        placeholder=""
                        disabled={true}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="equip.model" />
                    </th>
                    <td className="select-td">
                      <ASelect
                        mode="multiple"
                        style={{ width: "100%" }}
                        value={selectedModels}
                        placeholder=""
                        disabled={true}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* 헤더규칙 */}
              <div className="component__title" style={{ marginTop: 30 }}>
                <span>
                  <IntlMessages id="header.rule.list" />
                </span>
              </div>
              <GridTable
                className="-striped -highlight"
                getTrProps={(state, row, col, instance) => {
                  if (row) {
                    return {
                      className: "cursor-pointer",
                      onClick: (e, handleOriginal) => {
                        e.stopPropagation();
                        this.setState({
                          selectedHdr: row.index
                        });
                        this.onGetNmlzRuleContHists(row.index);
                      },
                      style: {
                        backgroundColor:
                          row.index === this.state.selectedHdr
                            ? "#282a35"
                            : "#2f323c",
                        cursor: "pointer"
                      }
                    };
                  } else {
                    return {
                      style: {
                        cursor: "pointer"
                      }
                    };
                  }
                }}
                columns={makeHdrColumn(logFormatCode)}
                data={nmlzRuleHdrHists}
                manual
                showPagination={false}
                showPaginationOptions={false}
                style={{ marginBottom: 30, height: 320 }}
              />
              <div className="component__title">
                <span>
                  <IntlMessages id="content.rule.list" />
                </span>
              </div>
              <GridTable
                className="-striped -highlight"
                columns={makeContColumn(logFormatCode)}
                data={
                  this.state.selectedHdr !== -1 &&
                  currentHdrHistIdx !== -1 &&
                  nmlzRuleHdrHists[currentHdrHistIdx].nmlzRuleContHists !==
                    undefined
                    ? nmlzRuleHdrHists[currentHdrHistIdx].nmlzRuleContHists
                    : []
                }
                manual
                showPagination={false}
                showPaginationOptions={false}
                style={{ marginBottom: 30, height: 320 }}
              />
              <div className="change-reason">
                <div className="component__title">
                  <span>
                    <IntlMessages id="reason.change.reg" />
                  </span>
                </div>
                <Textbox
                  name="nmlzRuleChgReason"
                  type="text"
                  value={nmlzRuleChgReason}
                  classNameInput="form-control"
                />
              </div>
            </div>
          </div>
        </div>
        <div className="bottom">
          <div className="btns">
            <button
              className="btn"
              onClick={() =>
                NmlzRuleActions.navPageTarget({ type: PAGE_TYPE.LIST })
              }
            >
              <IntlMessages id="cancel" />
            </button>
          </div>
        </div>
        {this.props.rollbackPopup.props.open && <RollbackNmlzRulePopup />}
        {this.props.duplicatePopup.props.open && <DuplicatePopup />}
        {/* 로딩 컴포넌트 */}
        {this.props.loading && <Loading />}
      </div>
    );
  }
}

export default connect(
  state => ({
    rollbackPopup: state.nmlzRule.get("rollbackPopup"),
    nmlzRuleHist: state.nmlzRule.get("nmlzRuleHist"),
    nmlzRuleHists: state.nmlzRule.get("nmlzRuleHists"),
    currentHdrHistIdx: state.nmlzRule.get("currentHdrHistIdx"),
    loading: state.nmlzRule.get("loading"),
    navPageTarget: state.nmlzRule.get("navPageTarget"),
    duplicatePopup: state.nmlzRule.get("duplicatePopup"),
    isExistDuplicate: state.nmlzRule.get("isExistDuplicate"),
    language: state.common.get("language")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch),
    ComCodeActions: bindActionCreators(comCodeActions, dispatch)
  })
)(RuleHistory);
