import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import * as commonActions from "store/modules/common";
import * as comCodeActions from "store/modules/code";
import { PAGE_TYPE, PAGE_ACTION_TYPE } from "store/modules/nmlzRule";

import Loading from "components/Common/Loading";

import { Select, Textbox, Textarea } from "react-inputs-validation";
import "antd/dist/antd.css";
import { Select as ASelect } from "antd";
import _ from "@lodash";

import HdrRule from "./Accordion/HdrRule/index";
import ContRule from "./Accordion/ContRule/index";

import DeleteNmlzRulePopup from "../../popups/DeleteNmlzRulePopup";

import NmlzRuleContDetailPopup from "../../popups/NmlzRuleContDetailPopup";
import NmlzRuleHdrDetailPopup from "../../popups/NmlzRuleHdrDetailPopup";
import FieldFuncPopup from "../../popups/FieldFuncPopup";
import MergeRulePopup from "../../popups/MergeRulePopup";
import ReplaceRulePopup from "../../popups/ReplaceRulePopup";
import FieldSelectPopup from "../../popups/FieldSelectPopup";
import CancelConfirmPopup from "../../popups/CancelConfirmPopup";
import DefaultValueApplyPopup from "../../popups/DefaultValueApplyPopup";
import DuplicatePopup from "../../popups/DuplicatePopup";

import app from "../../regexp/app";
import IntlMessages from "util/IntlMessages";
import { FormattedMessage } from "react-intl";

const Option = ASelect.Option;

const logFormatCodeOpt = [
  { id: "RGX", name: "Regex" },
  { id: "JSON", name: "JSON" }
];

const useYnOpt = [
  { id: "Y", name: <IntlMessages id="use" /> },
  { id: "N", name: <IntlMessages id="unused" /> }
];

class RuleInfo extends Component {
  state = {
    disabled: false,
    sectionList: [
      {
        validation: false,
        title: <IntlMessages id="step.equip.info" />,
        summary: <IntlMessages id="enter.rule.set" />,
        value: "SI",
        isShow: true
      },
      {
        validation: false,
        title: <IntlMessages id="step.header.rule" />,
        summary: <IntlMessages id="regex.mapping.header" />,
        value: "HR",
        isShow: false
      },
      {
        validation: false,
        title: <IntlMessages id="step.content.rule" />,
        summary: <IntlMessages id="set.rule.nmlz.content" />,
        value: "CR",
        isShow: false
      }
    ],
    validation: {
      validate: false,
      hasErrRuleNm: true,
      hasErrChgReason: true
    },
    nmlzRuleChgReason: "",
    selectedVendors: [],
    selectedSatcs: [],
    selectedModels: [],
    eventTypeCodeOpt: []
  };

  toggleViewList = idx => {
    const sectionList = [...this.state.sectionList];
    sectionList[idx].isShow = !sectionList[idx].isShow;
    this.setState({
      sectionList
    });
  };

  doGetNmlzRule = async () => {
    const { NmlzRuleActions, CommonActions, navPageTarget } = this.props;
    try {
      await NmlzRuleActions.getNmlzRule(navPageTarget.data);
      if (this.props.selectedVendors.length > 0) {
        await CommonActions.getSatcOfModelByVendorIds(
          _.join(this.props.selectedVendors, ",")
        );
        await CommonActions.getModelsOfSatcByVendorIds(
          _.join(this.props.selectedVendors, ","),
          _.join(this.props.selectedSatcs, ",")
        );
      }

      this.setState({
        selectedVendors: this.props.selectedVendors,
        selectedSatcs: this.props.selectedSatcs,
        selectedModels: this.props.selectedModels
      });

      // if (this.props.nmlzRule.logFormatCode === 'RGX') {
      //   this.onRegexParser();
      // }
    } catch (err) {
      console.error("err", err);
    }
  };

  doGetVendorList = async () => {
    const { CommonActions } = this.props;
    try {
      await CommonActions.getVendors();
    } catch (err) {
      console.error("err", err);
    }
  };

  doGetSatcList = async vendors => {
    const { CommonActions } = this.props;
    try {
      await CommonActions.getSatcOfModelByVendorIds(vendors);
    } catch (err) {
      console.error("err", err);
    }
  };

  doGetModelList = async (vendors, satcs) => {
    const { CommonActions } = this.props;
    try {
      await CommonActions.getModelsOfSatcByVendorIds(vendors, satcs);
    } catch (err) {
      console.error("err", err);
    }
  };

  isEnableSave = () => {
    return this.isSelectedVendors() && this.isSelectedSatcs();
  };

  isSelectedVendors = () => {
    return this.state.selectedVendors.length > 0;
  };

  isSelectedSatcs = () => {
    return this.state.selectedSatcs.length > 0;
  };

  doMakeAgentList() {
    const { NmlzRuleActions, vendors, satcs, models } = this.props;

    const selVenders = this.state.selectedVendors;
    const selSatcs = this.state.selectedSatcs;
    const selModels = this.state.selectedModels;

    let newNmlzRuleAgents = [];
    if (selVenders.length > 0) {
      const vendor = _.find(vendors, vendor => {
        return selVenders[0] === vendor.value;
      });

      if (selSatcs.length > 0) {
        for (let k = 0; k < selSatcs.length; k++) {
          const satc = _.find(satcs, satc => {
            return selSatcs[k] === satc.value;
          });

          if (selModels.length > 0) {
            for (let k = 0; k < selModels.length; k++) {
              const model = _.find(models, model => {
                return parseInt(selModels[k]) === model.agentModelId;
              });

              let object = {
                agentModelId: model.agentModelId,
                agentModelNm: model.agentModelNm,
                serviceItemCode: satc.value,
                serviceItemCodeNm: satc.text,
                agentVendorId: vendor.value,
                agentVendorNm: vendor.text,
                nmlzRuleSeq: this.props.nmlzRuleSeq
              };
              newNmlzRuleAgents.push(object);
            }
          } else {
            let object = {
              agentModelId: "",
              agentModelNm: "",
              serviceItemCode: satc.value,
              serviceItemCodeNm: satc.text,
              agentVendorId: vendor.value,
              agentVendorNm: vendor.text,
              nmlzRuleSeq: this.props.nmlzRuleSeq
            };
            newNmlzRuleAgents.push(object);
          }
        }
      } else {
        let object = {
          agentModelId: "",
          agentModelNm: "",
          serviceItemCode: "",
          serviceItemCodeNm: "",
          agentVendorId: vendor.value,
          agentVendorNm: vendor.text,
          nmlzRuleSeq: this.props.nmlzRuleSeq
        };
        newNmlzRuleAgents.push(object);
      }
    }

    NmlzRuleActions.editNmlzRule({
      name: "nmlzRuleAgents",
      value: newNmlzRuleAgents
    });

    NmlzRuleActions.canBeSubmit();
  }

  handleChange = (value, event) => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: event.target.name,
      value: event.target.type === "checkbox" ? event.target.checked : value
    });

    NmlzRuleActions.canBeSubmit();
  };

  handleLogFormatChange = value => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: "logFormatCode",
      value: value
    });

    NmlzRuleActions.canBeSubmit();
  };

  handleUseYnChange = value => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.editNmlzRule({
      name: "useYn",
      value: value
    });
    if (this.isEnableSave()) {
      this.newMethod(NmlzRuleActions);
    }
  };

  handleChgReasonChange = value => {
    this.setState({ nmlzRuleChgReason: value });
  };

  handleVendorChange = value => {
    this.setState(
      {
        selectedVendors: [value]
      },
      () => {
        if (value.length !== 0) {
          this.setState(
            {
              selectedSatcs: [],
              selectedModels: []
            },
            () => {
              this.doMakeAgentList();
              const { CommonActions } = this.props;
              CommonActions.clearSatcs();
              CommonActions.clearModels();
              this.doGetSatcList(_.join([value], ","));
            }
          );
        } else if (value.length === 0) {
          this.setState(
            {
              selectedSatcs: [],
              selectedModels: []
            },
            () => {
              const { CommonActions } = this.props;
              CommonActions.clearSatcs();
              CommonActions.clearModels();
              this.doMakeAgentList();
            }
          );
        }
      }
    );
  };

  handleSatcChange = value => {
    this.setState(
      {
        selectedSatcs: value
      },
      () => {
        const models = this.state.selectedModels;
        let newModels = [];
        if (this.props.models !== undefined) {
          for (let i = 0; i < this.props.models.length; i++) {
            for (let j = 0; j < models.length; j++) {
              if (this.props.models[i].agentModelId === parseInt(models[j])) {
                if (_.includes(value, this.props.models[i].agentModelSection)) {
                  newModels.push(models[j]);
                }
              }
            }
          }
          this.setState(
            {
              selectedModels: newModels
            },
            () => {
              this.doGetModelList(
                _.join(this.state.selectedVendors, ","),
                _.join(value, ",")
              );
              this.doMakeAgentList();
            }
          );
        } else {
          this.doGetModelList(
            _.join(this.state.selectedVendors, ","),
            _.join(value, ",")
          );
        }
      }
    );
  };

  handleModelChange = value => {
    this.setState(
      {
        selectedModels: value
      },
      () => {
        this.doMakeAgentList();
      }
    );
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);
    const { hasErrRuleNm, hasErrChgReason } = this.state.validation;

    return !hasErrRuleNm && !hasErrChgReason;
  };

  saveNmlzRule = async () => {
    const { NmlzRuleActions, CommonActions, navPageTarget } = this.props;
    try {
      this.props.nmlzRule.nmlzRuleChgReason = this.state.nmlzRuleChgReason;
      if (navPageTarget.detail === PAGE_ACTION_TYPE.EDIT) {
        await NmlzRuleActions.updateNmlzRule(this.props.nmlzRule);
      } else {
        await NmlzRuleActions.createNmlzRule(this.props.nmlzRule);
      }

      if (this.props.isExistDuplicate) {
        NmlzRuleActions.openDuplicatePopup({
          data: this.props.nmlzRule
        });
      } else {
        NmlzRuleActions.clearData();
        CommonActions.clearData();
        NmlzRuleActions.navPageTarget({ type: PAGE_TYPE.LIST });
      }
    } catch (err) {
      console.error("err", err);
    }
  };

  onDeleteCrsRow = index => {
    const { NmlzRuleActions, nmlzRule, currentHdrIdx } = this.props;
    NmlzRuleActions.removeNmlzRuleCont(
      nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts[index].nmlzRuleContSeq
    );
    NmlzRuleActions.canBeHdrSave();
    NmlzRuleActions.canBeSubmit();
  };

  onEditCrsRow = index => {
    const { NmlzRuleActions, nmlzRule, currentHdrIdx } = this.props;
    NmlzRuleActions.procNmlzRuleCont({
      type: PAGE_ACTION_TYPE.EDIT,
      data: nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleConts[index]
    });
    NmlzRuleActions.canBeContSave();
    NmlzRuleActions.openEditContPopup({
      data: index
    });
  };

  onAddCrsRow = () => {
    const { NmlzRuleActions, nmlzRule, currentHdrIdx } = this.props;
    if (nmlzRule.logFormatCode === "RGX") {
      app.cleanup();
    }

    NmlzRuleActions.procNmlzRuleCont({
      type: PAGE_ACTION_TYPE.ADD,
      data: {
        nmlzRuleHdrSeq: nmlzRule.nmlzRuleHdrs[currentHdrIdx].nmlzRuleHdrSeq
      }
    });

    NmlzRuleActions.canBeContSave();
    NmlzRuleActions.openNewContPopup();
  };

  onDeleteHdrRow = index => {
    const { NmlzRuleActions, nmlzRule } = this.props;
    NmlzRuleActions.removeNmlzRuleHdr(
      nmlzRule.nmlzRuleHdrs[index].nmlzRuleHdrSeq
    );
    NmlzRuleActions.canBeSubmit();
  };

  onEditHdrRow = index => {
    const { NmlzRuleActions, nmlzRule } = this.props;
    NmlzRuleActions.procNmlzRuleHdr({
      type: PAGE_ACTION_TYPE.EDIT,
      data: nmlzRule.nmlzRuleHdrs[index]
    });
    NmlzRuleActions.canBeHdrSave();
    NmlzRuleActions.openEditHdrPopup({
      data: index
    });
  };

  onAddHdrRow = () => {
    const { NmlzRuleActions, nmlzRule } = this.props;
    if (this.props.nmlzRule.logFormatCode === "RGX") {
      app.cleanup();
    }

    NmlzRuleActions.procNmlzRuleHdr({
      type: PAGE_ACTION_TYPE.ADD,
      data: { nmlzRuleSeq: nmlzRule.nmlzRuleSeq }
    });
    NmlzRuleActions.canBeHdrSave();
    NmlzRuleActions.openNewHdrPopup();
  };

  onGetNmlzRuleConts = index => {
    const { NmlzRuleActions } = this.props;
    NmlzRuleActions.getNmlzRuleConts({
      hdrIndex: index
    });
  };

  // changeExpression() {
  //   const { NmlzRuleActions } = this.props;
  //   NmlzRuleActions.editNmlzRule({
  //     name: 'hdrRegexp',
  //     value: app.expression.getPattern(),
  //   });
  //   NmlzRuleActions.canBeSubmit();
  // }

  // changeText() {
  //   const { NmlzRuleActions } = this.props;
  //   NmlzRuleActions.editNmlzRule({
  //     name: 'hdrSampLog',
  //     value: app.text.getValue(),
  //   });
  //   NmlzRuleActions.canBeSubmit();
  // }

  // updateResult() {
  //   //console.log('updateResult', app.getResult());
  //   let result = app.getResult();
  //   if (!result.error) {
  //     const { NmlzRuleActions } = this.props;
  //     NmlzRuleActions.setMatchInfo(app.getResult().matches);
  //   }
  // }

  // onRegexParser = () => {
  //   app.init(false, '.container', '.result');
  //   app.expression.setValue(this.props.nmlzRule.hdrRegexp);
  //   app.text.setValue(this.props.nmlzRule.hdrSampLog);
  //   app.expression.on('change', () => this.changeExpression());
  //   app.text.on('change', () => this.changeText());
  //   app.on('result', () => this.updateResult());
  // };

  initValidation = type => {
    const validation = this.state.validation;
    if (type === PAGE_ACTION_TYPE.EDIT) {
      validation.validate = false;
      validation.hasErrRuleNm = false;
      validation.hasErrChgReason = true;
    } else {
      validation.validate = false;
      validation.hasErrRuleNm = true;
      validation.hasErrChgReason = true;
    }
    this.setState({
      validation: validation
    });
  };

  getEventType = async () => {
    const { ComCodeActions } = this.props;
    let eventTypeCodeOpt = await ComCodeActions.getCodesByParentCode(
      "EVENT_TYPE"
    );
    this.setState({
      eventTypeCodeOpt: eventTypeCodeOpt
    });
  };

  initLoadData = type => {
    this.doGetVendorList();
    const { NmlzRuleActions } = this.props;
    if (type === PAGE_ACTION_TYPE.EDIT) {
      this.doGetNmlzRule();
    } else {
      NmlzRuleActions.initNmlzRule();
    }

    this.getEventType();

    NmlzRuleActions.canBeSubmit();
  };

  initPageFunc = () => {
    const { navPageTarget } = this.props;
    this.initValidation(navPageTarget.detail);
    this.initLoadData(navPageTarget.detail);
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // if (prevProps.contPopup.props.open && !this.props.contPopup.props.open) {
    //   if (this.props.nmlzRule.logFormatCode === 'RGX') {
    //     this.onRegexParser();
    //   }
    // } else

    if (
      prevProps.defaultValueApplyPopup.props.open &&
      !this.props.defaultValueApplyPopup.props.open
    ) {
      if (this.props.defaultValueApplyPopup.data !== null) {
        //update 기본값 설정
        const { NmlzRuleActions } = this.props;
        NmlzRuleActions.editNmlzRuleCont({
          name: this.props.defaultValueApplyPopup.data.name,
          value: this.props.defaultValueApplyPopup.data.value
        });

        if (this.props.defaultValueApplyPopup.data.value === "Y") {
          NmlzRuleActions.editNmlzRuleCont({
            name: "contRegexp",
            value: ".*"
          });
        }

        NmlzRuleActions.canBeContSave();
      }
    } else if (
      prevProps.deletePopup.props.open &&
      !this.props.deletePopup.props.open
    ) {
      const { NmlzRuleActions, CommonActions } = this.props;
      NmlzRuleActions.clearData();
      CommonActions.clearData();
      NmlzRuleActions.navPageTarget({ type: PAGE_TYPE.LIST });
    } else if (
      prevProps.cancelConfirmPopup.props.open &&
      !this.props.cancelConfirmPopup.props.open
    ) {
      if (this.props.cancelConfirmPopup.props.action === "ok") {
        const { nmlzRuleAgents } = this.props.nmlzRule;
        const selectedVendors = _.keys(
          _.groupBy(nmlzRuleAgents, "agentVendorId")
        );
        const selectedSatcs = _.keys(
          _.groupBy(nmlzRuleAgents, "serviceItemCode")
        );
        const selectedModels = _.keys(
          _.groupBy(nmlzRuleAgents, "agentModelId")
        );
        this.setState({
          selectedVendors:
            selectedVendors[0] === "undefined" ? [] : selectedVendors,
          selectedSatcs: selectedSatcs[0] === "undefined" ? [] : selectedSatcs,
          selectedModels:
            selectedModels[0] === "undefined" ? [] : selectedModels
        });
      }
    }
  }

  componentDidMount() {
    this.initPageFunc();
  }

  componentWillUnmount() {
    if (this.props.nmlzRule.logFormatCode === "RGX") {
      app.cleanup();
    }
  }

  render() {
    const {
      handleChange,
      handleLogFormatChange,
      handleUseYnChange,
      handleVendorChange,
      handleSatcChange,
      handleModelChange,
      handleChgReasonChange,
      saveNmlzRule,
      onAddCrsRow,
      onEditCrsRow,
      onDeleteCrsRow,
      onAddHdrRow,
      onEditHdrRow,
      onDeleteHdrRow,
      onGetNmlzRuleConts
    } = this;

    const {
      sectionList,
      nmlzRuleChgReason,
      selectedVendors,
      selectedSatcs,
      selectedModels,
      eventTypeCodeOpt
    } = this.state;

    const {
      nmlzRuleNm,
      logFormatCode,
      useYn,
      nmlzRuleDesc
    } = this.props.nmlzRule;

    const { validate } = this.state.validation;

    const { NmlzRuleActions, CommonActions, navPageTarget } = this.props;

    const sectionRow = sectionList.map((section, index) => {
      return (
        <div
          className={`list ${section.isShow === true ? "open" : ""}`}
          key={index}
        >
          <div className="list__info">
            <div
              className="list__info--title"
              onClick={() => this.toggleViewList(index)}
            >
              <b>{section.title}</b>
              <p>{section.summary}</p>
            </div>
            {section.value === "SI" ? (
              <div className="list__info--content">
                <div className="left">
                  <div className="component__title">
                    <span>
                      <IntlMessages id="basic.info" />
                    </span>
                  </div>
                  <table className="table table--dark table--info">
                    <colgroup>
                      <col width="150px" />
                      <col />
                    </colgroup>
                    <tbody>
                      <tr>
                        <th>
                          <IntlMessages id="rule.name" />
                          <span className="font--red" style={{ color: "red" }}>
                            *
                          </span>
                        </th>

                        <td>
                          <Textbox
                            name="nmlzRuleNm"
                            type="text"
                            value={nmlzRuleNm}
                            onChange={handleChange}
                            onBlur={() => {}} //blur검증 시 반드시 구현해야함
                            validate={validate}
                            validationOption={{locale: this.props.language}}
                            validationCallback={res => {
                              this.setState({
                                validation: {
                                  ...this.state.validation,
                                  hasErrRuleNm: res,
                                  validate: false
                                }
                              });
                            }}
                            classNameInput="form-control"
                          />
                        </td>
                      </tr>
                      <tr>
                        <th>
                          <IntlMessages id="log.format" />
                          <span className="font--red" style={{ color: "red" }}>
                            *
                          </span>
                        </th>
                        <td className="select-td">
                          <Select
                            name={"logFormatCode"}
                            value={logFormatCode}
                            disabled={
                              this.props.navPageTarget.detail ===
                              PAGE_ACTION_TYPE.EDIT
                            }
                            optionList={logFormatCodeOpt}
                            onChange={(logFormatCode, e) => {
                              handleLogFormatChange(logFormatCode);
                            }}
                            customStyleWrapper={{
                              width: "100%",
                              height: "24px",
                              fontSize: "12px"
                            }}
                          />
                        </td>
                      </tr>
                      <tr>
                        <th>
                          <IntlMessages id="use.yn" />
                          <span className="font--red" style={{ color: "red" }}>
                            *
                          </span>
                        </th>
                        <td className="select-td">
                          <Select
                            name={"useYn"}
                            value={useYn}
                            optionList={useYnOpt}
                            onChange={(useYn, e) => {
                              handleUseYnChange(useYn);
                            }}
                            customStyleWrapper={{
                              width: "100%",
                              height: "24px",
                              fontSize: "12px"
                            }}
                          />
                        </td>
                      </tr>
                      <tr>
                        <th>
                          <IntlMessages id="desc" />
                        </th>
                        <td>
                          <Textarea
                            name="nmlzRuleDesc"
                            type="text"
                            value={nmlzRuleDesc}
                            onChange={handleChange}
                            classNameInput="form-control h150"
                          />
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="right">
                  <div className="component__title">
                    <span>Device Type</span>
                  </div>
                  <table className="table table--dark table--info">
                    <colgroup>
                      <col width="150px" />
                      <col />
                    </colgroup>
                    <tbody>
                      <tr>
                        <th>
                          <IntlMessages id="vendor" />
                        </th>
                        <td className="select-td">
                          <FormattedMessage id="select.vendor">
                            {placeholder => (
                              <ASelect
                                //mode="multiple"
                                style={{ width: "100%" }}
                                value={selectedVendors}
                                placeholder={placeholder}
                                onChange={handleVendorChange}
                              >
                                {this.props.vendors !== undefined &&
                                  this.props.vendors.map(vendor => {
                                    return (
                                      <Option key={vendor.value}>
                                        {vendor.text}
                                      </Option>
                                    );
                                  })}
                              </ASelect>
                            )}
                          </FormattedMessage>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          <IntlMessages id="equip.type" />
                        </th>
                        <td className="select-td">
                          <FormattedMessage id="select.equip.type">
                            {placeholder => (
                              <ASelect
                                mode="multiple"
                                style={{ width: "100%" }}
                                value={selectedSatcs}
                                placeholder={placeholder}
                                onChange={handleSatcChange}
                              >
                                {this.props.satcs !== undefined &&
                                  this.props.satcs.map(satc => {
                                    return (
                                      <Option key={satc.value}>
                                        {satc.text}
                                      </Option>
                                    );
                                  })}
                              </ASelect>
                            )}
                          </FormattedMessage>
                        </td>
                      </tr>
                      <tr>
                        <th>
                          <IntlMessages id="equip.model" />
                        </th>
                        <td className="select-td">
                          <FormattedMessage id="select.equip.model">
                            {placeholder => (
                              <ASelect
                                mode="multiple"
                                style={{ width: "100%" }}
                                value={selectedModels}
                                placeholder={placeholder}
                                onChange={handleModelChange}
                              >
                                {this.props.models !== undefined &&
                                  this.props.models.map(model => {
                                    return (
                                      <Option key={model.agentModelId}>
                                        {model.agentModelNm}
                                      </Option>
                                    );
                                  })}
                              </ASelect>
                            )}
                          </FormattedMessage>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            ) : (
              ""
            )}
            {section.value === "HR" ? (
              <HdrRule
                onDeleteHdrRow={onDeleteHdrRow}
                onEditHdrRow={onEditHdrRow}
                onAddHdrRow={onAddHdrRow}
                onGetNmlzRuleConts={onGetNmlzRuleConts}
              />
            ) : (
              ""
            )}
            {section.value === "CR" ? (
              <ContRule
                onDeleteCrsRow={onDeleteCrsRow}
                onEditCrsRow={onEditCrsRow}
                onAddCrsRow={onAddCrsRow}
                eventTypeCodeOpt={eventTypeCodeOpt}
              />
            ) : (
              ""
            )}
          </div>
        </div>
      );
    });

    return (
      <div className="contents info">
        <div className="top">
          <div className="accordion-list">{sectionRow}</div>
        </div>

        <div className="bottom">
          <div className="change-reason">
            <div className="component__title">
              <span>
                <IntlMessages id="reason.change" />
              </span>
              <span className="font--red" style={{ color: "red" }}>
                *
              </span>
            </div>
            <Textbox
              id="nmlzRuleChgReason"
              name="nmlzRuleChgReason"
              type="text"
              value={nmlzRuleChgReason}
              onChange={handleChgReasonChange}
              onBlur={() => {}} //blur검증 시 반드시 구현해야함
              validate={validate}
              validationOption={{locale: this.props.language}}
              validationCallback={res => {
                this.setState({
                  validation: {
                    ...this.state.validation,
                    hasErrChgReason: res,
                    validate: false
                  }
                });
              }}
              classNameInput="form-control"
            />
          </div>
          <div className="btns">
            <button
              className="btn"
              onClick={() => {
                if (this.props.canBeSubmit) {
                  NmlzRuleActions.openCancelConfirmPopup({
                    from: "nmlzRule"
                  });
                } else {
                  NmlzRuleActions.clearData();
                  CommonActions.clearData();
                  NmlzRuleActions.navPageTarget({ type: PAGE_TYPE.LIST });
                }
              }}
            >
              <IntlMessages id="cancel" />
            </button>
            {navPageTarget.detail === PAGE_ACTION_TYPE.EDIT ? (
              <button
                className="btn"
                onClick={ev => {
                  ev.stopPropagation();
                  NmlzRuleActions.openDeletePopup({
                    from: "detail",
                    data: [this.props.nmlzRule.nmlzRuleSeq]
                  });
                }}
              >
                <IntlMessages id="delete" />
              </button>
            ) : (
              ""
            )}
            <button
              className="btn btn--blue"
              disabled={!this.isEnableSave()}
              onClick={ev => {
                ev.stopPropagation();
                if (this.isValidate()) {
                  saveNmlzRule();
                }
              }}
            >
              <IntlMessages id="save" />
            </button>
          </div>
        </div>
        {this.props.deletePopup.props.open && <DeleteNmlzRulePopup />}
        {this.props.hdrPopup.props.open && <NmlzRuleHdrDetailPopup />}
        {this.props.contPopup.props.open && <NmlzRuleContDetailPopup />}
        {this.props.fieldFuncPopup.props.open && <FieldFuncPopup />}
        {this.props.mergePopup.props.open && <MergeRulePopup />}
        {this.props.replacePopup.props.open && <ReplaceRulePopup />}
        {this.props.fieldSelectPopup.props.open && <FieldSelectPopup />}
        {this.props.cancelConfirmPopup.props.open && <CancelConfirmPopup />}
        {this.props.defaultValueApplyPopup.props.open && (
          <DefaultValueApplyPopup />
        )}
        {this.props.duplicatePopup.props.open && <DuplicatePopup />}
        {/* 로딩 컴포넌트 */}
        {this.props.loading && <Loading />}
      </div>
    );
  }
}

export default connect(
  state => ({
    nmlzRule: state.nmlzRule.get("nmlzRule"),
    nmlzRuleHdr: state.nmlzRule.get("nmlzRuleHdr"),
    currentHdrIdx: state.nmlzRule.get("currentHdrIdx"),
    canBeSubmit: state.nmlzRule.get("canBeSubmit"),
    isExistDuplicate: state.nmlzRule.get("isExistDuplicate"),
    hdrPopup: state.nmlzRule.get("hdrPopup"),
    contPopup: state.nmlzRule.get("contPopup"),
    fieldFuncPopup: state.nmlzRule.get("fieldFuncPopup"),
    mergePopup: state.nmlzRule.get("mergePopup"),
    replacePopup: state.nmlzRule.get("replacePopup"),
    deletePopup: state.nmlzRule.get("deletePopup"),
    fieldSelectPopup: state.nmlzRule.get("fieldSelectPopup"),
    cancelConfirmPopup: state.nmlzRule.get("cancelConfirmPopup"),
    defaultValueApplyPopup: state.nmlzRule.get("defaultValueApplyPopup"),
    duplicatePopup: state.nmlzRule.get("duplicatePopup"),

    selectedVendors: state.nmlzRule.get("selectedVendors"),
    selectedSatcs: state.nmlzRule.get("selectedSatcs"),
    selectedModels: state.nmlzRule.get("selectedModels"),

    navPageTarget: state.nmlzRule.get("navPageTarget"),
    vendors: state.common.get("vendors"),
    satcs: state.common.get("satcOfModelByVendorIds"),
    models: state.common.get("modelsOfSatcByVendorIds"),

    loading: state.nmlzRule.get("loading"),

    canBeContSave: state.nmlzRule.get("canBeContSave")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch),
    CommonActions: bindActionCreators(commonActions, dispatch),
    ComCodeActions: bindActionCreators(comCodeActions, dispatch)
  })
)(RuleInfo);
