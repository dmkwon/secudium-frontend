import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import * as commonActions from "store/modules/common";

import { PAGE_TYPE } from "store/modules/nmlzRule";

import Navbar from "components/Common/Navbar";
import LeftMenu from "components/Common/LeftMenu";

import NmlzRuleList from "./NmlzRuleList";
import NmlzRuleDetail from "./NmlzRuleDetail";

import "./style.scss";

class NmlzRuleComponent extends Component {
  componentWillUnmount() {
    const { NmlzRuleActions, CommonActions } = this.props;
    NmlzRuleActions.clearData();
    CommonActions.clearData();
  }

  render() {
    const { navPageTarget } = this.props;

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper normalization-rule">
          <LeftMenu />
          {navPageTarget.type === PAGE_TYPE.LIST && <NmlzRuleList />}
          {navPageTarget.type === PAGE_TYPE.DETAIL && <NmlzRuleDetail />}
        </div>
      </React.Fragment>
    );
  }
}
export default connect(
  state => ({
    navPageTarget: state.nmlzRule.get("navPageTarget")
  }),
  dispatch => ({
    NmlzRuleActions: bindActionCreators(nmlzRuleActions, dispatch),
    CommonActions: bindActionCreators(commonActions, dispatch)
  })
)(NmlzRuleComponent);
