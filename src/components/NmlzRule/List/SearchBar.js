import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import IntlMessages from "util/IntlMessages";
import { FormattedMessage } from "react-intl";

import "../style.scss";

class SearchBar extends React.Component {
  render() {
    const {
      onSearch,
      setSearchText,
      onDeployRule,
      onAddNewRule,
      onSelectDelete
    } = this.props;
    return (
      <div className="component__title">
        <div className="search-bar" style={{width: this.props.language === 'ko' ? "261px" : "350px"}}>
          <FormattedMessage id="vendor.equip.model">
            {placeholder => (
              <input
                type="text"
                name="keyword"
                value={this.props.searchText}
                className="form-control"
                onChange={e => {
                  setSearchText(e.target.value);
                }}
                placeholder={placeholder}
                style={{width: this.props.language === 'ko' ? "180px" : "252px"}}
              />
            )}
          </FormattedMessage>
          <div className="binder" />
          <button
            className="btn btn-icon small btn--go"
            onClick={() => {
              onSearch(this.props.searchText);
            }}
          />
          <button
            className="btn btn-icon small btn--filter"
            onClick={() => {
              setSearchText("");
            }}
          />
        </div>
        <div className="btns">
          <button className="btn btn--dark" onClick={() => onDeployRule()}>
            <IntlMessages id="rule.deploy" />
          </button>
          <button className="btn btn--dark" onClick={() => onSelectDelete()}>
            <IntlMessages id="multi.delete" />
          </button>
          <button className="btn btn--blue" onClick={() => onAddNewRule()}>
            <IntlMessages id="new.reg" />
          </button>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    searchText: state.nmlzRule.get("searchText"),
    language: state.common.get("language")
  }),
  dispatch => {
    return bindActionCreators(
      {
        setSearchText: nmlzRuleActions.setSearchText
      },
      dispatch
    );
  }
)(SearchBar);
