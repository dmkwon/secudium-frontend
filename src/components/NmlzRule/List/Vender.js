import React from "react";

import VendorTree from "components/Common/VendorTree";
import IntlMessages from "util/IntlMessages";

import "../style.scss";

class Vender extends React.Component {
  render() {
    const { onSelect } = this.props;

    return (
      <div className="component component-tree vender-tree">
        <div className="component__title">
          <IntlMessages id="vendor.name" />
        </div>
        <div className="component__box">
          <VendorTree onSelect={onSelect} />
        </div>
      </div>
    );
  }
}

export default Vender;
