import React, { Component } from "react";

import RuleInfo from "./Detail/Tab/RuleInfo";
import RuleHistory from "./Detail/Tab/RuleHistory";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

export default class NmlzRuleDetailComponent extends Component {
  state = {
    selectedTab: "info"
  };

  selectRuleDetailTab = tab => {
    this.setState({
      selectedTab: tab
    });
  };

  render() {
    const { selectedTab } = this.state;

    return (
      <div className="component rule-detail">
        <div className="rule-detail__contents">
          <ul className="component-tab">
            <li
              className={selectedTab === "info" ? "active" : ""}
              onClick={() => this.selectRuleDetailTab("info")}
            >
              <IntlMessages id="normal.rule.info" />
            </li>
            <li
              className={selectedTab === "history" ? "active" : ""}
              onClick={() => this.selectRuleDetailTab("history")}
            >
              <IntlMessages id="change.hist" />
            </li>
          </ul>
          {/* 정규화 룰 정보 */}
          {selectedTab === "info" && <RuleInfo />}

          {/* 변경 이력 */}
          {selectedTab === "history" && <RuleHistory />}
        </div>
      </div>
    );
  }
}
