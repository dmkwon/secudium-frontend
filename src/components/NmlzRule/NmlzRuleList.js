import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as nmlzRuleActions from "store/modules/nmlzRule";
import { PAGE_TYPE, PAGE_ACTION_TYPE } from "store/modules/nmlzRule";
import ReactTooltip from "react-tooltip";

import GridTable from "components/Common/GridTable";
import Loading from "components/Common/Loading";

import Vender from "./List/Vender";
import SearchBar from "./List/SearchBar";

import DeleteNmlzRulePopup from "./popups/DeleteNmlzRulePopup";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class NmlzRuleListComponent extends Component {
  doGetNmlzRules = async params => {
    try {
      this.props.deSelectAllNmlzRules();
      await this.props.getNmlzRules(params);
    } catch (err) {
      console.error("err", err);
    }
  };

  doDeployRule = async () => {
    try {
      await this.props.deployRule();
    } catch (err) {
      console.error("err", err);
    }
  };

  onSelectVendorTreeNode = (selectedKeys, e) => {
    const key = selectedKeys[0];

    if (key !== undefined) {
      this.props.setSearchText("");
      let values = key.split("_");
      this.doGetNmlzRules({
        agentVendorId: values[1],
        serviceItemCode: values[2],
        agentModelId: values[3]
      });
    }
  };

  onSearch = keyword => {
    this.doGetNmlzRules({
      keyword: keyword
    });
  };

  onDeployRule = () => {
    this.doDeployRule();
  };

  onEditRule = index => {
    this.props.setSearchText("");
    this.props.deSelectAllNmlzRules();
    this.props.navPageTarget({
      type: PAGE_TYPE.DETAIL,
      detail: PAGE_ACTION_TYPE.EDIT,
      data: this.props.nmlzRules.content[index].nmlzRuleSeq
    });
  };

  onAddNewRule = () => {
    this.props.navPageTarget({
      type: PAGE_TYPE.DETAIL,
      detail: PAGE_ACTION_TYPE.ADD
    });
  };

  componentDidMount() {
    this.doGetNmlzRules();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.deletePopup.props.open === true &&
      this.props.deletePopup.props.open === false
    ) {
      this.doGetNmlzRules(this.props.routeParams);
    }
  }

  render() {
    const { onEditRule } = this;

    const {
      nmlzRules,
      selectedNmlzRuleSeqs,
      selectAllNmlzRules,
      deSelectAllNmlzRules,
      toggleInSelectedNmlzRules,
      openDeletePopup
    } = this.props;

    return (
      <React.Fragment>
        <div className="component">
          <div className="section__title">
            <IntlMessages id="normal.rule.manage" />
          </div>
          <div className="component-content">
            <Vender onSelect={this.onSelectVendorTreeNode} />

            <div className="rule-result">
              <SearchBar
                onSearch={this.onSearch}
                onDeployRule={this.onDeployRule}
                onSelectDelete={() => {
                  selectedNmlzRuleSeqs.length > 0 &&
                    openDeletePopup({
                      from: "list",
                      data: selectedNmlzRuleSeqs
                    });
                }}
                onAddNewRule={this.onAddNewRule}
              />
              <GridTable
                className="-striped -highlight"
                getTdProps={(state, row, col, instance) => {
                  return {
                    className: "cursor-pointer",
                    onClick: (e, handleOriginal) => {
                      e.stopPropagation();
                      if (col.id !== "checkBox") {
                        if (row) {
                          this.onEditRule(row.index);
                        }
                      }
                    }
                  };
                }}
                data={nmlzRules.content}
                columns={[
                  {
                    Header: "No.",
                    id: "row",
                    accessor: "number",
                    style: { textAlign: "center" },
                    width: 50,
                    Cell: row => {
                      return <div>{row.index + 1}</div>;
                    }
                  },
                  {
                    Header: (
                      <div
                        className="checkbox checkbox--dark"
                        style={{ marginTop: 2 }}
                      >
                        <label>
                          <input
                            type="checkbox"
                            onClick={event => {
                              event.stopPropagation();
                            }}
                            onChange={event => {
                              event.target.checked
                                ? selectAllNmlzRules()
                                : deSelectAllNmlzRules();
                            }}
                            checked={
                              selectedNmlzRuleSeqs.length ===
                                Object.keys(
                                  nmlzRules.content === undefined
                                    ? []
                                    : nmlzRules.content
                                ).length && selectedNmlzRuleSeqs.length > 0
                            }
                          />
                          <div className="icon" />
                        </label>
                      </div>
                    ),
                    accessor: "checkBox",
                    Cell: row => {
                      return (
                        <div className="checkbox checkbox--dark">
                          <label>
                            <input
                              type="checkbox"
                              onClick={event => {
                                event.stopPropagation();
                              }}
                              checked={selectedNmlzRuleSeqs.includes(
                                row.original.nmlzRuleSeq
                              )}
                              onChange={() =>
                                toggleInSelectedNmlzRules(
                                  row.original.nmlzRuleSeq
                                )
                              }
                            />
                            <div className="icon" />
                          </label>
                        </div>
                      );
                    },
                    style: { textAlign: "center" },
                    sortable: false,
                    width: 60
                  },
                  {
                    Header: <IntlMessages id="vendor.name" />,
                    accessor: "agentVenderNm",
                    style: { textAlign: "left" },
                    width: 120,
                    Cell: row => {
                      if (row) {
                        if (row.original.agentVenderNm === undefined)
                          return "ALL";
                        else return row.original.agentVenderNm;
                      }
                    }
                  },
                  {
                    Header: <IntlMessages id="normal.rule.name" />,
                    accessor: "nmlzRuleNm",
                    style: { textAlign: "left" },
                    width: 200
                  },
                  {
                    Header: <IntlMessages id="log.method" />,
                    accessor: "logFormatCode",
                    style: { textAlign: "center" },
                    width: 100,
                    Cell: row => {
                      if (row.original.logFormatCode === "RGX") return "Regex";
                      else if (row.original.logFormatCode === "JSON")
                        return "JSON";
                    }
                  },
                  {
                    Header: <IntlMessages id="desc" />,
                    accessor: "nmlzRuleDesc",
                    style: { textAlign: "left" },
                    width: 305,
                    Cell: row => {
                      if (row) {
                        const tooltipId = `nmlzRuleNmTooltip_${row.original.nmlzRuleSeq}`;
                        let target = row.original;
                        return (
                          <React.Fragment>
                            <span data-tip data-for={tooltipId}>
                              {target.nmlzRuleDesc}
                            </span>
                            <ReactTooltip
                              id={tooltipId}
                              className="tooltipClass"
                              place="right"
                              type="dark"
                              effect="float"
                              event="mouseover"
                              globalEventOff="mouseout"
                            >
                              {target.nmlzRuleDesc}
                            </ReactTooltip>
                          </React.Fragment>
                        );
                      }
                    }
                  },
                  {
                    Header: <IntlMessages id="use.yn" />,
                    accessor: "useYn",
                    style: { textAlign: "center" },
                    width: 80,
                    Cell: row => {
                      if (row.original.useYn === "Y")
                        return <IntlMessages id="use" />;
                      else if (row.original.useYn === "N")
                        return <IntlMessages id="unused" />;
                    }
                  },
                  {
                    Header: <IntlMessages id="modifier" />,
                    accessor: "modUsrNm",
                    style: { textAlign: "center" },
                    width: 100
                  },
                  {
                    Header: <IntlMessages id="mod.datetime" />,
                    accessor: "modDateFormatted",
                    style: { textAlign: "center" },
                    width: 140
                  },
                  {
                    Header: "Actions",
                    accessor: "icon",
                    style: { textAlign: "center" },
                    Cell: row => {
                      return (
                        <div>
                          <button
                            id="btnNmlzRuleDelete"
                            className="btn btn-icon"
                            onClick={ev => {
                              ev.stopPropagation();
                              openDeletePopup({
                                from: "list",
                                data: [row.original.nmlzRuleSeq]
                              });
                            }}
                          >
                            <img
                              id="imgNmlzRuleDelete"
                              src="/images/common/icon_delete.png"
                              alt="icon_delete"
                            />
                          </button>
                          <button
                            id="btnNmlzRuleEdit"
                            className="btn btn-icon"
                            onClick={() => {
                              onEditRule(row.index);
                            }}
                          >
                            <img
                              id="imgNmlzRuleEdit"
                              src="/images/common/icon_edit.png"
                              alt="icon_edit"
                            />
                          </button>
                        </div>
                      );
                    },
                    sortable: false,
                    width: 72
                  }
                ]}
                pageSizeOptions={[10, 15, 20, 50, 100]}
                defaultPageSize={100}
                resizable={false}
                filterable={false}
                style={{
                  maxHeight: "860px"
                }}
              />
            </div>
          </div>
        </div>
        {this.props.deletePopup.props.open && <DeleteNmlzRulePopup />}
        {/* 로딩 컴포넌트 */}
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    nmlzRules: state.nmlzRule.get("nmlzRules"),
    loading: state.nmlzRule.get("loading"),
    selectedNmlzRuleSeqs: state.nmlzRule.get("selectedNmlzRuleSeqs"),
    deletePopup: state.nmlzRule.get("deletePopup"),
    routeParams: state.nmlzRule.get("routeParams"),
    navPageTarget: state.nmlzRule.get("navPageTarget")
  }),
  dispatch => {
    return bindActionCreators(
      {
        getNmlzRules: nmlzRuleActions.getNmlzRules,
        selectAllNmlzRules: nmlzRuleActions.selectAllNmlzRules,
        deSelectAllNmlzRules: nmlzRuleActions.deSelectAllNmlzRules,
        toggleInSelectedNmlzRules: nmlzRuleActions.toggleInSelectedNmlzRules,
        setSearchText: nmlzRuleActions.setSearchText,
        openDeletePopup: nmlzRuleActions.openDeletePopup,
        navPageTarget: nmlzRuleActions.navPageTarget,
        deployRule: nmlzRuleActions.deployRule
      },
      dispatch
    );
  }
)(NmlzRuleListComponent);
