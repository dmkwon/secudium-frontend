export default class Prefs {
  constructor(el) {
    this._load();
  }

  read(key) {
    return this._data[key];
  }

  write(key, value) {
    if (this._data[key] === value) {
      return;
    }
    this._data[key] = value;
    this._save();
  }

  clear(key) {
    delete this._data[key];
    this._save();
  }

  _load() {
    let match = /(?:^|;\s*)prefs=\s*([^;]*)/.exec(document.cookie);
    if (match && match[1]) {
      try {
        this._data = JSON.parse(unescape(match[1]));
        return;
      } catch (e) {}
    }
    this._data = {};
  }

  _save() {
    let str = escape(JSON.stringify(this._data));
    document.cookie =
      'prefs=' + str + '; expires=Fri, 31 Dec 9999 23:59:59 GMT;';
  }
}
