import $ from '../utils/DOMGenerator';
import Explain from './tools/Explain';
import Details from './tools/Details';
import Replace from './tools/Replace';

import SubstLexer from '../SubstLexer';
import EventDispatcher from '../events/EventDispatcher';

import app from '../app';

export default class Tools extends EventDispatcher {
  constructor(el) {
    super();
    this.el = el;
    this._initUI();
    this.value = null;
  }

  set value(o) {
    if (!o) {
      //this.show('explain');
      //this._toolValues = Utils.copy({}, Tools.DEFAULT_VALUES);
    } else {
      this.show(o.id);
      if (o.input != null) {
        this.editor.setValue(o.input);
      }
    }
  }

  get value() {
    return {
      id: this._toolId,
      input: this.input,
    };
  }

  get input() {
    return this.hasInput ? this.editor.getValue() : null;
  }

  get hasInput() {
    let id = this._toolId;
    return id === 'replace' || id === 'list';
  }

  show(id) {
    if (!id || id === this._toolId) {
      return;
    }

    // this.toolList.selected = this._toolId = id;
    let input = id === 'replace' || id === 'list';

    if (this._tool) {
      this._tool.cleanup();
    }

    //$.toggleClass($.query('> article', this.el), 'showinput', input);
    if (input) {
      this.editor.setValue(this._toolValues[id]);
      this.editor.refresh();
      this.editor.focus();
    }

    if (id === 'explain') {
      this._tool = new Explain(this.contentEl);
    } else if (id === 'details') {
      this._tool = new Details(this.contentEl);
    } else if (id === 'replace' || id === 'list') {
      this._tool = new Replace(this.resultEl, this.editor);
    }

    this._toolId = id;
    this._updateHighlights();
  }

  _initUI() {
    app.flavor.on('change', () => this._updateHighlights());
    app.expression.on('change', () => this._updateHighlights());

    this.lexer = new SubstLexer();
  }

  _handleEditorChange() {
    this._updateHighlights();
    this._toolValues[this._toolId] = this.editor.getValue();
    this.dispatchEvent('change');
  }

  _updateHighlights() {
    if (!this.hasInput) {
      return;
    } // only for Replace & List
    this.lexer.profile = app.flavor.profile;
    let token = this.lexer.parse(this.editor.getValue());
    this.highlighter.draw(token);
    this.hover.token = token;
  }

  _handleListChange() {
    this.show(this.toolList.selected);
  }

  _handleHeaderClick(evt) {
    if (
      $.hasClass(this.el, 'closed') ||
      !this.toolListEl.contains(evt.target)
    ) {
      $.togglePanel(this.el, 'article');
    }
  }
}
Tools.DEFAULT_VALUES = {
  replace: '<< $& >>',
  list: '$&\\n',
};
