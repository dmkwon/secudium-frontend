import $ from '../utils/DOMGenerator';
import CMUtils from '../utils/CMUtils';
import Utils from '../utils/Utils';

import ExpressionLexer from '../ExpressionLexer';
import ExpressionHighlighter from './ExpressionHighlighter';
import ExpressionHover from './ExpressionHover';
import EventDispatcher from '../events/EventDispatcher';

import app from '../app';

export default class Expression extends EventDispatcher {
  constructor(el) {
    super();
    this.el = el;
    this.delim = '/';
    this.lexer = new ExpressionLexer();

    this._initUI(el);
  }

  set value(expression) {
    let regex = Utils.decomposeRegEx(
      expression || Expression.DEFAULT_EXPRESSION,
      this.delim
    );
    this.pattern = regex.source;
    this.flags = regex.flags;
  }

  get value() {
    return this.editor.getValue();
  }

  set pattern(pattern) {
    let index = this.editor.getValue().lastIndexOf(this.delim);
    this.editor.replaceRange(
      pattern,
      { line: 0, ch: 1 },
      { line: 0, ch: index }
    );
    this._deferUpdate();
  }

  get pattern() {
    return Utils.decomposeRegEx(this.editor.getValue(), this.delim).source;
  }

  set flags(flags) {
    flags = app.flavor.validateFlagsStr(flags);
    let str = this.editor.getValue(),
      index = str.lastIndexOf(this.delim);
    this.editor.replaceRange(
      flags,
      { line: 0, ch: index + 1 },
      { line: 0, ch: str.length }
    ); // this doesn't work if readOnly is false.
  }

  get flags() {
    return Utils.decomposeRegEx(this.editor.getValue(), this.delim).flags;
  }

  get token() {
    return this.lexer.token;
  }

  setValue(expression) {
    let regex = Utils.decomposeRegEx(expression, this.delim);
    this.pattern = regex.source;
    this.flags = regex.flags;
  }

  getValue() {
    return this.editor.getValue();
  }

  getPattern() {
    return Utils.decomposeRegEx(this.editor.getValue(), this.delim).source;
  }
  insert(str) {
    this.editor.replaceSelection(str, 'end');
  }

  selectAll() {
    CMUtils.selectAll(this.editor);
  }

  // private methods:
  _initUI(el) {
    this.editorEl = $.query('> .editor', el);
    this.editor = CMUtils.create(
      $.empty(this.editorEl),
      {
        autofocus: true,
        maxLength: 2500,
        singleLine: true,
      },
      '100%',
      '100%'
    );
    let editor = this.editor;

    editor.on('change', (cm, evt) => this._onEditorChange(cm, evt));
    editor.on('keydown', (cm, evt) => this._onEditorKeyDown(cm, evt));
    // hacky method to disable overwrite mode on expressions to avoid overwriting flags:
    editor.toggleOverwrite = () => {};

    this.highlighter = new ExpressionHighlighter(editor);
    this.hover = new ExpressionHover(editor, this.highlighter);

    this._setInitialExpression();
    this.value = Expression.DEFAULT_EXPRESSION;
  }

  _setInitialExpression() {
    let editor = this.editor;
    editor.setValue('/./g');

    // leading /
    editor.getDoc().markText(
      { line: 0, ch: 0 },
      {
        line: 0,
        ch: 1,
      },
      {
        className: 'exp-decorator',
        readOnly: true,
        atomic: true,
        inclusiveLeft: true,
      }
    );

    // trailing /g
    editor.getDoc().markText(
      { line: 0, ch: 2 },
      {
        line: 0,
        ch: 4,
      },
      {
        className: 'exp-decorator',
        readOnly: false,
        atomic: true,
        inclusiveRight: true,
      }
    );
    this._deferUpdate();
  }

  _deferUpdate() {
    Utils.defer(() => this._update(), 'Expression._update');
  }

  _update() {
    let expr = this.editor.getValue();
    this.lexer.profile = app.flavor.profile;
    let token = this.lexer.parse(expr);
    $.toggleClass(this.editorEl, 'error', !!this.lexer.errors.length);
    this.hover.token = token;
    this.highlighter.draw(token);
    this.dispatchEvent('change');
  }

  _onEditorChange(cm, evt) {
    // catches pasting full expressions in.
    // TODO: will need to be updated to work with other delimeters
    this._deferUpdate();
    let str = evt.text[0];
    if (
      str.length < 3 ||
      !str.match(/^\/.+[^\\]\/[a-z]*$/gi) ||
      evt.from.ch !== 1 ||
      evt.to.ch !== 1 + evt.removed[0].length
    ) {
      // not pasting a full expression.
      return;
    }
    this.value = str;
  }

  _onEditorKeyDown(cm, evt) {
    // Ctrl or Command + D by default, will delete the expression and the flags field, Re: https://github.com/gskinner/regexr/issues/74
    // So we just manually reset to nothing here.
    if ((evt.ctrlKey || evt.metaKey) && evt.keyCode === 68) {
      evt.preventDefault();
      this.pattern = '';
    }
  }
}

Expression.DEFAULT_EXPRESSION = '/([A-Z])\\w+/g';
