import EventDispatcher from './events/EventDispatcher';

import $ from './utils/DOMGenerator';
import Utils from './utils/Utils';

import Expression from './views/Expression';
import Text from './views/Text';
import Tools from './views/Tools';

import Reference from './docs/Reference';
import reference_content from './docs/reference_content';
import Flavor from './Flavor';

import RefCoverage from './RefCoverage';
import Prefs from './helpers/Prefs';

export default class RegExrp extends EventDispatcher {
  init(state, clacc, resultc) {
    this.clacc = clacc;
    this.resultc = resultc;
    this.prefs = new Prefs();
    this.flavor = new Flavor();
    this.reference = new Reference(reference_content, this.flavor);
    this._initUI();

    if (state === false) {
      this._localInit();
    } else {
      this.state = state;
    }
    this._savedHash = null;

    let params = Utils.getUrlParams();
    if (params.engine) {
      this.flavor.value = params.engine;
    }
    if (params.expression) {
      this.expression.value = params.expression;
    }
    if (params.text) {
      this.text.value = params.text;
    }
    if (params.tool) {
      this.tools.value = { id: params.tool, input: params.input };
    }
  }

  cleanup() {
    this.removeAllEventListeners('result');
    this.removeAllEventListeners('change');
  }

  _localInit() {
    new RefCoverage();
  }

  getResult() {
    return this.result;
  }

  // getter / setters:
  get state() {
    let o = {
      expression: this.expression.value,
      text: this.text.value,
      flavor: this.flavor.value,
      tool: this.tools.value,
    };
    // copy share values onto the pattern object:
    return Utils.copy(this.share.value, o);
  }

  set state(o) {
    if (!o) {
      return;
    }
    this.flavor.value = o.flavor;
    this.expression.value = o.expression;
    this.text.value = o.text;
    this.tools.value = o.tool;
  }

  get hash() {
    return Utils.getHashCode(
      this.expression.value +
        '\t' +
        this.text.value +
        '\t' +
        this.flavor.value +
        '\t'
    );
  }

  get unsaved() {
    return this.hash !== this._savedHash;
  }

  get isNarrow() {
    return this._matchList.matches;
  }

  // public methods:
  resetUnsaved() {
    this._savedHash = this.hash;
  }

  newDoc(warn = true) {
    this.load(
      { flavor: this.flavor.value, expression: '.', text: 'Text' },
      warn
    );
    this.expression.selectAll();
  }

  load(state, warn = true) {
    if (warn === true) {
      warn = 'You have unsaved changes. Continue without saving?';
    }
    if (warn && this.unsaved && !window.confirm(warn)) {
      return;
    }
    this.state = Utils.clone(state);
  }

  // private methods:
  _initUI() {
    // TODO: break into own Device class? Rename mobile.scss too?
    // mobile setup
    // keep synced with "mobile.scss":
    if (window.screen.width < 500) {
      document
        .getElementById('viewport')
        .setAttribute('content', 'width=500, user-scalable=0');
    }
    this._matchList = window.matchMedia('(max-width: 900px)');
    this._matchList.addListener(q => this.dispatchEvent('narrow')); // currently unused.

    // UI:
    //this.el = $.query('.container');
    this.el = $.query(this.clacc);
    let el = $.query('.app > .doc', this.el);
    this.expression = new Expression($.query('> section.expression', el));

    this.text = new Text($.query('> section.text', el), this.resultc);
    this.tools = new Tools($.query('> section.tools', el));

    this.expression.on('change', () => this._change());
    this.text.on('change', () => this._change());
    this.tools.on('change', () => this._change());
  }

  _change() {
    this.dispatchEvent('change');
    var solver = this.flavor.solver,
      exp = this.expression;
    solver.solve(
      {
        pattern: exp.pattern,
        flags: exp.flags,
        text: this.text.value,
        tool: this.tools.value,
      },
      result => this._handleResult(result)
    );
  }

  _handleResult(result) {
    this.result = this._processResult(result);
    this.dispatchEvent('result');
  }

  _processResult(result) {
    result.matches && result.matches.forEach((o, i) => (o.num = i));
    return result;
  }
}
