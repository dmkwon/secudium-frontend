import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as comCodeActions from "../../store/modules/code";
import Loading from "./../Common/Loading";
import DeleteConfirmPopup from "./popups/DeleteConfirmPopup";
import DeleteConfirmListPopup from "./popups/DeleteConfirmListPopup";
import CommonConfirmPopup from "../Common/Popups/CommonConfirmPopup";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import AddCodePopup from "./popups/AddCodePopup";
import UpdateCodePopup from "./popups/UpdateCodePopup";
import { getCodeList } from "./../../store/api/common";

import GridTable from "../Common/GridTable";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

class CodeComponent extends Component {
  state = {
    isCodePopup: false,
    isUpdateCodePopup: false,
    isDeleteConfirmPopup: false,
    isDeleteConfirmListPopup: false,
    selectAll: false,
    checked: [],
    paramList: [],
    showConfirm: false,

    resource: {},
    commCodeSeq: "",
    commCode: "",
    commCodeNmKo: "",
    commCodeNmEn: "",
    commCodeDesc: "",
    commCodeLevel: "",
    commCodeSortOrder: "",
    topCommCode: [
      {
        id: "0",
        name: <IntlMessages id="select.options" />
      }
    ],

    useTypes: [
      {
        name: <IntlMessages id="use" />,
        id: "Y"
      },
      {
        name: <IntlMessages id="unused" />,
        id: "N"
      }
    ]
  };

  onChangeTopFunc = async e => {
    try {
      const topCommCode = await getCodeList("0");

      this.setState({
        topCommCode: this.state.topCommCode.concat(topCommCode.data)
      });
    } catch (e) {
      console.warn(e);
    }
  };

  componentWillMount() {
    this.onChangeTopFunc();
  }

  // CheckBox: All Check
  chkBoxAllChange = () => {
    const { comCodes } = this.props;
    let checkArr = [];
    if (!this.state.selectAll) {
      for (let i = 0; i < comCodes.content.length; i++) {
        checkArr.push(true);
      }
      this.setState({ selectAll: true, checked: checkArr });
    } else {
      for (let i = 0; i < comCodes.content.length; i++) {
        checkArr.push(false);
      }
      this.setState({ selectAll: false, checked: checkArr });
    }
  };

  // CheckBox: Single Check
  chkBoxSingleChange = index => {
    const { comCodes } = this.props;
    let checkArr = this.state.checked;

    if (checkArr.length === 0) {
      for (let i = 0; i < comCodes.content.length; i++) {
        checkArr.push(false);
      }
      checkArr.splice(index, 1, true);
      this.setState({ checked: checkArr });
    } else {
      if (checkArr[index]) {
        checkArr.splice(index, 1, false);
      } else {
        checkArr.splice(index, 1, true);
      }
      this.setState({ checked: checkArr });
    }
  };

  onCheckDeleteHandler = e => {
    const { comCodes } = this.props;
    const checkArr = this.state.checked;
    let paramList = [];

    if (checkArr.length === 0) {
      this.showAlert(
        <FormattedMessage id="confirm"> {text => text} </FormattedMessage>,
        <FormattedMessage id="no.selection"> {text => text} </FormattedMessage>
      );
    } else {
      let stdExists = false;
      for (let idx = 0; idx < checkArr.length; idx++) {
        if (checkArr[idx]) {
          paramList.push(comCodes.content[idx]);
          stdExists = true;
        }
      }
      if (stdExists) {
        this.setState({
          paramList: paramList,
          isDeleteConfirmListPopup: true
        });
      } else {
        this.showAlert(
          <FormattedMessage id="confirm"> {text => text} </FormattedMessage>,
          <FormattedMessage id="no.selection">{text => text}</FormattedMessage>
        );
      }
    }
  };

  showAlert = (title, msg) => {
    this.setState({
      showConfirm: true,
      resource: {
        title: title,
        message: msg,
        hasChangeReason: false,
        btns: [
          {
            name: <IntlMessages id="confirm" />,
            CB: () => this.onClosePopup()
          }
        ]
      }
    });
  };

  onClosePopup = () => {
    this.setState({ showConfirm: false });
  };

  onDeleteListConfirm = async () => {
    const { ComCodeActions } = this.props;
    const { paramList } = this.state;
    let rx = "";
    rx = await ComCodeActions.removeComCodes(paramList);
    await ComCodeActions.getComCodes();
    if (rx.status === 200) {
      this.closePopup();
      this.setState({
        selectAll: false,
        checked: [],
        paramList: []
      });
    } else {
      this.closePopup();
    }
  };

  getComCodes = async () => {
    const { ComCodeActions } = this.props;
    try {
      await ComCodeActions.getComCodes();
    } catch (e) {
      console.log(e);
    }
  };

  getComCodeBySeq = async codesBySeq => {
    const { ComCodeActions } = this.props;
    try {
      await ComCodeActions.getComCodeBySeq(codesBySeq);
    } catch (e) {
      console.log(e);
    }
  };

  createCodes = async data => {
    const { ComCodeActions } = this.props;
    try {
      await ComCodeActions.createCodes(data);
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    this.getComCodes();
  }

  onChangeInput = e => {
    const { name, value } = e.target;
    this.setState({
      ...this.state,
      [name]: value
    });
  };

  updateCodePopup = codeSeq => {
    const { ComCodeActions } = this.props;
    const ajax = ComCodeActions.getComCodeBySeq(codeSeq);
    ajax.then(() => {
      this.setState({
        isUpdateCodePopup: true
      });
    });
  };

  deleteConfirmPopup = commCodeSeq => {
    const { ComCodeActions } = this.props;
    const ajax = ComCodeActions.getComCodeBySeq(commCodeSeq);
    ajax.then(() => {
      this.setState({
        isDeleteConfirmPopup: true
      });
    });
  };

  openCodePopup = () => {
    this.setState({
      isCodePopup: true
    });
  };

  closePopup = () => {
    this.setState({
      isCodePopup: false,
      isDeleteConfirmPopup: false,
      isDeleteConfirmListPopup: false,
      isUpdateCodePopup: false,
      selectAll: false,
      checked: [],
      paramList: []
    });
  };

  render() {
    const {
      chkBoxAllChange,
      chkBoxSingleChange,
      onDeleteListConfirm,
      onCheckDeleteHandler
    } = this;
    const {
      isCodePopup,
      showConfirm,
      isUpdateCodePopup,
      isDeleteConfirmPopup,
      isDeleteConfirmListPopup
    } = this.state;
    const { comCodes, codesBySeq } = this.props;
    const columns_codeCodes = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 50
      },
      {
        Header: (
          <div className="checkbox checkbox--dark" style={{ marginTop: 2 }}>
            <label>
              <input
                type="checkbox"
                onChange={chkBoxAllChange}
                checked={this.state.selectAll}
              />
              <div className="icon" />
            </label>
          </div>
        ),
        accessor: "checkBox",
        Cell: row => {
          return (
            <div className="checkbox checkbox--dark">
              <label>
                <input
                  type="checkbox"
                  onChange={() => chkBoxSingleChange(row.index)}
                  checked={this.state.checked[row.index] || ""}
                />
                <div className="icon" />
              </label>
            </div>
          );
        },
        style: { textAlign: "center" },
        sortable: false,
        width: 40
      },
      {
        Header: <IntlMessages id="parent.code" />,
        accessor: "topCommCode",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="code" />,
        accessor: "commCode",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="code.name.ko" />,
        accessor: "commCodeNmKo",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="code.name.en" />,
        accessor: "commCodeNmEn",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="level" />,
        accessor: "commCodeLevel",
        style: { textAlign: "right" },
        width: 100,
        filterable: true
      },
      {
        Header: <IntlMessages id="sort.order" />,
        accessor: "commCodeSortOrder",
        style: { textAlign: "right" },
        width: 100,
        filterable: true
      },
      {
        Header: <IntlMessages id="modifier" />,
        accessor: "regUsrNm",
        style: { textAlign: "left" },
        width: 150,
        filterable: true
      },
      {
        Header: <IntlMessages id="mod.date" />,
        accessor: "stringRegDate",
        style: { textAlign: "center" },
        width: 150,
        filterable: true
      },
      {
        Header: "Actions",
        accessor: "icon",
        style: { textAlign: "center" },
        width: 70,
        sortable: false,
        Cell: row => {
          return (
            <div>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.deleteConfirmPopup(row.original.commCodeSeq);
                }}
              >
                <img src="/images/common/icon_delete.png" alt="icon_delete" />
              </button>
              <button
                className="btn btn-icon"
                onClick={e => {
                  e.stopPropagation();
                  this.updateCodePopup(row.original.commCodeSeq);
                }}
              >
                <img src="/images/common/icon_edit.png" alt="icon_edit" />
              </button>
            </div>
          );
        }
      }
    ];

    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper code">
          <LeftMenu />
          <div className="component component-list">
            <div className="component__title">
              <span>
                <IntlMessages id="comm.code" />
              </span>
              <div className="btns">
                <button className="btn" onClick={onCheckDeleteHandler}>
                  <IntlMessages id="multi.delete" />
                </button>
                <button className="btn btn--blue" onClick={this.openCodePopup}>
                  <IntlMessages id="new.reg" />
                </button>
              </div>
            </div>

            <GridTable
              columns={columns_codeCodes}
              data={comCodes.content}
              className="-striped -highlight"
              filterable={false}
              resizable={false}
              getTrProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: async e => {
                    chkBoxSingleChange(rowInfo.index);
                  }
                };
              }}
            />
          </div>
        </div>
        {isCodePopup && (
          <AddCodePopup
            closePopup={this.closePopup}
            topCommCode={this.state.topCommCode}
          />
        )}
        {isUpdateCodePopup && (
          <UpdateCodePopup
            data={codesBySeq}
            closePopup={this.closePopup}
            topCommCode={this.state.topCommCode}
          />
        )}
        {isDeleteConfirmPopup && (
          <DeleteConfirmPopup data={codesBySeq} closePopup={this.closePopup} />
        )}
        {isDeleteConfirmListPopup && (
          <DeleteConfirmListPopup
            closePopup={this.closePopup}
            onDeleteListConfirm={onDeleteListConfirm}
          />
        )}
        {this.props.loading && <Loading />}
        {showConfirm && <CommonConfirmPopup resource={this.state.resource} />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    comCodes: state.code.get("comCodes"),
    codesBySeq: state.code.get("codesBySeq"),
    codesByParentCode: state.code.getIn(["codesByParentCode", "content"]),
    loading: state.code.get("loading")
  }),
  dispatch => ({
    ComCodeActions: bindActionCreators(comCodeActions, dispatch)
  })
)(CodeComponent);
