import React, { Component } from "react";
import Draggable from "react-draggable";
import { FormattedMessage } from "react-intl";

export default class DeleteConfirmListPopup extends Component {
  state = {
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("popup").offsetWidth;
    const elHeight = document.getElementById("popup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  render() {
    const { closePopup, onDeleteListConfirm } = this.props;
    const { position } = this.state;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--alert popup--move"
            id="popup"
            style={{ width: "400px" }}
          >
            <div className="popup__header">
              <h5>
                <FormattedMessage id="confirm">{text => text}</FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <FormattedMessage id="delete.select.code">
                {text => text}
              </FormattedMessage>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--dark" onClick={onDeleteListConfirm}>
                <FormattedMessage id="confirm">{text => text}</FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}
