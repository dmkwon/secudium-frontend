import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Textarea, Select } from "react-inputs-validation";
import Draggable from "react-draggable";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

import "./style.scss";

import * as comCodeActions from "./../../../store/modules/code";
import { createCode } from "./../../../store/api/code";

class AddCodePopup extends Component {
  state = {
    topCommCode: "0",
    commCode: "",
    commCodeNmKo: "",
    commCodeNmEn: "",
    commCodeSortOrder: "",
    commCodeDesc: "",
    topCommCodeList: this.props.topCommCode,

    validation: {
      validate: false,
      hasErrCommCode: true,
      hasErrCommCodeNmKo: true,
      hasErrCommCodeNmEn: true,
      hasErrCommCodeSortOrder: false,
      hasErrCommCodeDesc: false
    },

    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("addCodePopup").offsetWidth;
    const elHeight = document.getElementById("addCodePopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  // 팝업 위치 조절
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrCommCode,
      hasErrCommCodeNmKo,
      hasErrCommCodeNmEn,
      hasErrCommCodeSortOrder,
      hasErrCommCodeDesc
    } = this.state.validation;

    return (
      !hasErrCommCode &&
      !hasErrCommCodeNmKo &&
      !hasErrCommCodeNmEn &&
      !hasErrCommCodeDesc &&
      !hasErrCommCodeSortOrder
    );
  };

  // State 변경 handler
  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      [name]: value
    });
  };

  // 저장
  onClickSave = async e => {
    if (this.isValidate()) {
      const { ComCodeActions } = this.props;
      const InputCode = this.state;
      try {
        await createCode(InputCode);
        this.setState({
          topCommCode: "",
          commCode: "",
          commCodeNmKo: "",
          commCodeNmEn: "",
          commCommCodeSortOrder: "",
          commCodeDesc: "",
          extraCodeNm: ""
        });
        ComCodeActions.getComCodes();
        this.props.closePopup();
      } catch (e) {
        console.warn(e);
      }
    }
  };

  getTopCommCodeList = topCommCode => {
    this.setState({
      topCommCode
    });
  };

  render() {
    const { handleChangeInput, getTopCommCodeList } = this;
    const { position, topCommCodeList } = this.state;
    const { closePopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--move" id="addCodePopup">
            <div className="popup__header">
              <h5>
                <IntlMessages id="code.reg" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="parent.code" />
                    </th>
                    <td>
                      <Select
                        id="topCommCode"
                        name="topCommCode"
                        tabIndex="1"
                        value={this.state.topCommCode}
                        optionList={topCommCodeList}
                        onChange={getTopCommCodeList}
                        customStyleWrapper={{ width: "100%", resize: "none" }}
                        customStyleOptionListContainer={{
                          maxHeight: "217px",
                          overflow: "auto"
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="code" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="commCode"
                        name="commCode"
                        type="text"
                        tabIndex="2"
                        value={this.state.commCode}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationOption={{ locale: this.props.language }}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCode: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCode",
                          check: true,
                          required: true,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="code.name.ko" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="commCodeNmKo"
                        name="commCodeNmKo"
                        type="text"
                        tabIndex="3"
                        value={this.state.commCodeNmKo}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        onFocus={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeNmKo: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCodeNmKo",
                          check: true,
                          required: true,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="code.name.en" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="commCodeNmEn"
                        name="commCodeNmEn"
                        type="text"
                        tabIndex="3"
                        value={this.state.commCodeNmEn}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        onFocus={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeNmEn: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCodeNmEn",
                          check: true,
                          required: true,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="sort.order" />
                    </th>
                    <td>
                      <Textbox
                        id="commCodeSortOrder"
                        name="commCodeSortOrder"
                        type="text"
                        tabIndex="4"
                        value={this.state.commCodeSortOrder}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeSortOrder: res,
                              validate: false
                            }
                          });
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                        validationOption={{
                          name: "commCodeSortOrder",
                          check: true,
                          required: false,
                          reg: /^[0-9]+$/,
                          regMsg: (
                            <FormattedMessage id="number.only">
                              {text => text}
                            </FormattedMessage>
                          ),
                          locale: this.props.language
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ verticalAlign: "top" }}>
                      <IntlMessages id="desc" />
                    </th>
                    <td>
                      <Textarea
                        id="commCodeDesc"
                        name="commCodeDesc"
                        type="text"
                        tabIndex="5"
                        value={this.state.commCodeDesc}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeDesc: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCodeDesc",
                          check: true,
                          required: false,
                          max: 1000,
                          locale: this.props.language
                        }}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "70px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={this.onClickSave}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    ComCodeActions: bindActionCreators(comCodeActions, dispatch)
  })
)(AddCodePopup);
