import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Textarea, Select } from "react-inputs-validation";
import Draggable from "react-draggable";
import IntlMessages from "util/IntlMessages";

import "./style.scss";
import * as comCodeActions from "../../../store/modules/code";

class UpdateCodePopup extends Component {
  state = {
    topCommCodeList: this.props.topCommCode,
    commCodeList: [],
    codesBySeq: this.props.data,

    validation: {
      validate: false,
      hasErrCommCode: false,
      hasErrCommCodeNmKo: false,
      hasErrCommCodeNmEn: false,
      hasErrCommCodeSortOrder: false,
      hasErrCommCodeDesc: false
    },

    position: {
      x: 0,
      y: 0
    }
  };
  componentDidMount = () => {
    const elWidth = document.getElementById("updateCodePopup").offsetWidth;
    const elHeight = document.getElementById("updateCodePopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };
  // 팝업 위치 조절
  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrCommCode,
      hasErrCommCodeNmKo,
      hasErrCommCodeNmEn,
      hasErrCommCodeSortOrder
    } = this.state.validation;

    return !hasErrCommCode && !hasErrCommCodeNmKo && !hasErrCommCodeNmEn && !hasErrCommCodeSortOrder;
  };

  // State 변경 handler
  handleChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      codesBySeq: {
        ...this.state.codesBySeq,
        [name]: value
      }
    });
  };

  // 저장
  handleSaveData = async e => {
    if (this.isValidate()) {
      const { ComCodeActions } = this.props;
      try {
        await ComCodeActions.updateCodes(this.state.codesBySeq);
        await ComCodeActions.getComCodes();
        this.props.closePopup();
      } catch (e) {
        console.warn(e);
      }
    }
  };

  getTopCommCodeList = topCommCode => {
    this.setState({
      codesBySeq: {
        ...this.state.codesBySeq,
        topCommCode
      }
    });
  };

  render() {
    const { handleSaveData, handleChangeInput, getTopCommCodeList } = this;
    const { codesBySeq, position, topCommCodeList } = this.state;
    const { closePopup } = this.props;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--move" id="updateCodePopup">
            <div className="popup__header">
              <h5>
                <IntlMessages id="code.edit" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th>
                      <IntlMessages id="parent.code" />
                    </th>
                    <td>
                      <Select
                        id="topCommCode"
                        name="topCommCode"
                        tabIndex="1"
                        value={codesBySeq.topCommCode}
                        optionList={topCommCodeList}
                        onChange={getTopCommCodeList}
                        customStyleWrapper={{ width: "100%", resize: "none" }}
                        customStyleOptionListContainer={{
                          maxHeight: "217px",
                          overflow: "auto"
                        }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="code" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="commCode"
                        name="commCode"
                        type="text"
                        tabIndex="2"
                        value={codesBySeq.commCode}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCode: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCode",
                          check: true,
                          required: true,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="code.name.ko" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="commCodeNm"
                        name="commCodeNm"
                        type="text"
                        tabIndex="3"
                        value={codesBySeq.commCodeNmKo}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeNmKo: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCodeNmKo",
                          check: true,
                          required: true,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="code.name.en" />
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        id="commCodeNm"
                        name="commCodeNm"
                        type="text"
                        tabIndex="3"
                        value={codesBySeq.commCodeNmEn}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeNmEn: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCodeNmEn",
                          check: true,
                          required: true,
                          max: 50,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <IntlMessages id="sort.order" />
                    </th>
                    <td>
                      <Textbox
                        id="commCodeSortOrder"
                        name="commCodeSortOrder"
                        type="text"
                        tabIndex="4"
                        value={codesBySeq.commCodeSortOrder}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeSortOrder: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "commCodeSortOrder",
                          check: true,
                          required: false,
                          reg: /^[0-9]+$/,
                          regMsg: <IntlMessages id="number.only" />,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ verticalAlign: "top" }}>
                      <IntlMessages id="desc" />
                    </th>
                    <td>
                      <Textarea
                        id="commCodeDesc"
                        name="commCodeDesc"
                        type="text"
                        tabIndex="5"
                        value={codesBySeq.commCodeDesc}
                        onChange={handleChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrCommCodeDesc: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          name: "CommCodeDesc",
                          check: true,
                          required: false,
                          max: 1000,
                          locale: this.props.language
                        }}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "70px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={closePopup}>
                <IntlMessages id="cancel" />
              </button>
              <button className="btn btn--dark" onClick={handleSaveData}>
                <IntlMessages id="save" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    codesBySeq: state.code.get("codesBySeq"),
    language: state.common.get("language")
  }),
  dispatch => ({
    ComCodeActions: bindActionCreators(comCodeActions, dispatch)
  })
)(UpdateCodePopup);
