import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import UserDefineHdrDetailPopup from "./popups/UserDefineHdrDetailPopup";
import ConfirmPopup from "./popups/ConfirmPopup";

import * as engineEnvActions from "../../store/modules/engineEnv";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

const initHdr = {
  userDefineHdrSeq: "",
  nodePath: "",
  hdr: "",
  useYn: ""
};

class UserDefineHdrTable extends Component {
  state = {
    detailPopup: false,
    deletePopup: false,
    confirmPopup: false,
    userDefineHdr: initHdr
  };

  getUserDefineHdrs = async () => {
    const { EngineEnvActions } = this.props;
    try {
      await EngineEnvActions.getUserDefineHdrs();
    } catch (e) {
      console.log(e);
    }
  };

  //등록
  onClickReg = () =>
    this.setState({ userDefineHdr: initHdr }, this.openPopup("detailPopup"));
  //row 클릭 이벤트(수정)
  onClickEdit = userDefineHdr =>
    this.setState({ userDefineHdr }, this.openPopup("detailPopup"));
  //삭제
  onClickDel = userDefineHdr =>
    this.setState({ userDefineHdr }, this.openPopup("confirmPopup"));
  deleteUserDefineHdr = data => {
    const { EngineEnvActions } = this.props;
    const ajax = EngineEnvActions.deleteUserDefineHdr(data);
    ajax.then(() => {
      this.getUserDefineHdrs();
      this.closePopup("confirmPopup");
    });
  };

  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  componentDidMount() {
    this.getUserDefineHdrs();
  }

  render() {
    const {
      onClickReg,
      onClickEdit,
      onClickDel,
      closePopup,
      deleteUserDefineHdr
    } = this;
    const { userDefineHdrs } = this.props;
    const { userDefineHdr, detailPopup, confirmPopup } = this.state;

    const renderResultRows = userDefineHdrs => {
      return userDefineHdrs.length > 0
        ? userDefineHdrs.map((userDefineHdr, index) => {
            return (
              <tr key={++index}>
                <td>{index}</td>
                <td style={{ textAlign: "left" }}>{userDefineHdr.nodePath}</td>
                <td style={{ textAlign: "left" }}>{userDefineHdr.hdr}</td>
                <td>{userDefineHdr.modUsrNm}</td>
                <td>{userDefineHdr.modDateYmd}</td>
                <td>
                  <button
                    className="btn btn-icon"
                    onClick={() => onClickDel(userDefineHdr)}
                  >
                    <img
                      id="ipDelete"
                      src="/images/common/icon_delete.png"
                      alt="icon_delete"
                    />
                  </button>
                  <button
                    className="btn btn-icon"
                    style={{ marginLeft: "2px" }}
                    onClick={() => onClickEdit(userDefineHdr)}
                  >
                    <img
                      id="ipUpdate"
                      src="/images/common/icon_edit.png"
                      alt="icon_edit"
                    />
                  </button>
                </td>
              </tr>
            );
          })
        : [];
    };

    return (
      <React.Fragment>
        <div className="component__title">
          <div className="btns">
            <button className="btn" onClick={onClickReg}>
              <IntlMessages id="new.reg" />
            </button>
          </div>
        </div>
        <div className="table-wrapper">
          <table className="table table--dark">
            <colgroup>
              <col width="60px" />
              <col width="300px" />
              <col />
              <col width="80px" />
              <col width="80px" />
              <col width="70px" />
            </colgroup>
            <thead>
              <tr>
                <th>No.</th>
                <th>
                  <FormattedMessage id="node.path">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>Header</th>
                <th>
                  <FormattedMessage id="modifier">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>
                  <FormattedMessage id="mod.date">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{renderResultRows(userDefineHdrs)}</tbody>
          </table>
        </div>
        {detailPopup ? (
          <UserDefineHdrDetailPopup
            isShow={detailPopup}
            data={userDefineHdr}
            onClose={() => closePopup("detailPopup")}
          />
        ) : (
          ""
        )}
        <ConfirmPopup
          isShow={confirmPopup}
          message={<IntlMessages id="do.delete" />}
          data={userDefineHdr}
          callback={deleteUserDefineHdr}
          onClose={() => closePopup("confirmPopup")}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    userDefineHdrs: state.engineEnv.get("userDefineHdrs")
  }),
  dispatch => ({
    EngineEnvActions: bindActionCreators(engineEnvActions, dispatch)
  })
)(UserDefineHdrTable);
