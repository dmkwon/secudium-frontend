import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ConfirmPopup from "./popups/ConfirmPopup";
import AlertPopup from "./popups/AlertPopup";
import { Textbox } from "react-inputs-validation";

import * as util from "./../../utils";
import * as engineEnvActions from "../../store/modules/engineEnv";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

class EnginEnvInfo extends React.Component {
  state = {
    engineEnv: this.props.engineEnv,
    disabled: true,
    confirmPopup: false,
    alertPopup: false,
    isEdit: false,
    validation: {
      validate: false,
      hasErrZkConnInfo: false,
      hasErrRootPath: false,
      hasErrBakPath: false,
      hasErrNodeSize: false
    }
  };

  getEngineEnv = async () => {
    const { EngineEnvActions } = this.props;
    try {
      await EngineEnvActions.getEngineEnv();
    } catch (e) {
      console.log(e);
    }
  };

  onChangeHandler = (value, e) => {
    const { name } = e.target;
    this.setState({ engineEnv: this.state.engineEnv.set(name, value) });
  };

  updateEngineEnv = () => {
    const { EngineEnvActions } = this.props;
    const ajax = EngineEnvActions.updateEngineEnv(this.state.engineEnv.toJS());
    ajax.then(() => {
      this.getEngineEnv();
      this.setEdit(false);
      this.closePopup("confirmPopup");
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    //validate를 true로 설정 시 모든 form의 유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);

    const {
      hasErrBakPath,
      hasErrZkConnInfo,
      hasErrNodeSize,
      hasErrRootPath
    } = this.state.validation;

    return (
      !hasErrBakPath && !hasErrZkConnInfo && !hasErrNodeSize && !hasErrRootPath
    );
  };

  onClickSave = () => {
    //수정됬는지 검증
    if (
      JSON.stringify(this.state.engineEnv.toJS()) !==
      JSON.stringify(this.props.engineEnv)
    ) {
      if (this.isValidate()) {
        //유효성 검사 후에만 저장될 수 있도록
        this.openPopup("confirmPopup");
      }
    } else {
      this.openPopup("alertPopup");
    }
  };

  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  onClickCancel = () => {
    this.setState(
      {
        engineEnv: this.props.engineEnv,
        disabled: true,
        isEdit: false
      },
      () => this.isValidate()
    );
  };

  setEdit = flag => {
    if (flag) {
      this.setState({ disabled: false, isEdit: true });
    } else {
      this.setState({ disabled: true, isEdit: false });
    }
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({ engineEnv: nextProps.engineEnv });
  }

  componentDidMount() {
    this.getEngineEnv();
  }

  //접속정보 validate
  validateConnInfo = string => {
    const errMsg = (
      <FormattedMessage id="wrong.connect.info">
        {text => text}
      </FormattedMessage>
    );
    const splitComma = string.split(",");
    //comma로 split되지 않은 경우
    if (splitComma.length <= 0) {
      return errMsg;
    }
    for (let i = 0, length = splitComma.length; i < length; i++) {
      const url = splitComma[i].split(":");

      //1개 이상의 콜론이 있을 경우
      if (url.length !== 2) {
        return errMsg;
      }
      const ip = url[0],
        port = url[1];
      //ip 또는 port형식이 아닌 경우
      if (!util.isIp(ip) || !util.isPort(port)) {
        return errMsg;
      }
    }
    return true;
  };
  //경로유효성 검사
  validatePath = path =>
    util.isLinuxPath(path) ? (
      true
    ) : (
      <FormattedMessage id="wrong.path"> {text => text} </FormattedMessage>
    );

  render() {
    const {
      onChangeHandler,
      setEdit,
      onClickSave,
      onClickCancel,
      closePopup,
      updateEngineEnv
    } = this;
    const {
      isEdit,
      disabled,
      engineEnv,
      confirmPopup,
      alertPopup
    } = this.state;
    const { validate } = this.state.validation;

    const buttonStyle = { minWidth: "50px", height: "30px" };
    const confirmMsg = <IntlMessages id="do.save" />;
    const alertMsg = <IntlMessages id="no.change" />;

    return (
      <div
        className="list__info--content-engineinfo"
        style={{ overflowY: "auto" }}
      >
        <div className="component__asset">
          <table className="table table--dark table--info">
            <colgroup>
              <col width="150px" />
              <col />
            </colgroup>
            <tbody>
              <tr>
                <th>
                  <FormattedMessage id="connect.info">
                    {text => text}
                  </FormattedMessage>
                  <span className="font--red">*</span>
                </th>
                <td>
                  <Textbox
                    id="zkConnInfo"
                    name="zkConnInfo"
                    type="text"
                    value={engineEnv.get("zkConnInfo")}
                    onChange={onChangeHandler}
                    onBlur={() => {}} //blur검증 시 반드시 구현해야함
                    disabled={disabled}
                    validate={validate}
                    validationCallback={res => {
                      this.setState({
                        validation: {
                          ...this.state.validation,
                          hasErrZkConnInfo: res,
                          validate: false
                        }
                      });
                    }}
                    classNameInput="form-control"
                    validationOption={{
                      customFunc: this.validateConnInfo,
                      locale: this.props.language
                    }}
                  />
                </td>
              </tr>
              <tr>
                <th>
                  <FormattedMessage id="top.level.path">
                    {text => text}
                  </FormattedMessage>
                  <span className="font--red">*</span>
                </th>
                <td>
                  <Textbox
                    id="rootPath"
                    name="rootPath"
                    type="text"
                    value={engineEnv.get("rootPath")}
                    onChange={onChangeHandler}
                    onBlur={() => {}} //blur검증 시 반드시 구현해야함
                    disabled={disabled}
                    validate={validate}
                    validationCallback={res => {
                      this.setState({
                        validation: {
                          ...this.state.validation,
                          hasErrRootPath: res,
                          validate: false
                        }
                      });
                    }}
                    classNameInput="form-control"
                    validationOption={{
                      customFunc: this.validatePath,
                      locale: this.props.language
                    }}
                  />
                </td>
              </tr>
              <tr>
                <th>
                  <FormattedMessage id="backup.path">
                    {text => text}
                  </FormattedMessage>
                  <span className="font--red">*</span>
                </th>
                <td>
                  <Textbox
                    id="bakPath"
                    name="bakPath"
                    type="text"
                    value={engineEnv.get("bakPath")}
                    onChange={onChangeHandler}
                    onBlur={() => {}}
                    disabled={disabled}
                    validate={validate}
                    classNameInput="form-control"
                    validationCallback={res => {
                      this.setState({
                        validation: {
                          ...this.state.validation,
                          hasErrBakPath: res,
                          validate: false
                        }
                      });
                    }}
                    validationOption={{
                      customFunc: this.validatePath,
                      locale: this.props.language
                    }}
                  />
                </td>
              </tr>
              <tr>
                <th>
                  <FormattedMessage id="load.limit.size">
                    {text => text}
                  </FormattedMessage>
                  <span className="font--red">*</span>
                </th>
                <td>
                  <Textbox
                    id="nodeSize"
                    name="nodeSize"
                    type="text"
                    maxLength="3"
                    value={engineEnv.get("nodeSize")}
                    onChange={onChangeHandler}
                    onBlur={() => {}}
                    disabled={disabled}
                    validate={validate}
                    classNameInput="form-control"
                    validationOption={{
                      type: "number",
                      min: 10,
                      max: 512,
                      locale: this.props.language
                    }}
                    validationCallback={res => {
                      this.setState({
                        validation: {
                          ...this.state.validation,
                          hasErrNodeSize: res,
                          validate: false
                        }
                      });
                    }}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div
          className="list__info--content-engineinfo--footer"
          style={{ display: isEdit ? "block" : "none" }}
        >
          <button
            className="btn btn--gray"
            style={buttonStyle}
            onClick={onClickCancel}
          >
            <IntlMessages id="cancel" />
          </button>
          <button
            className="btn btn--blue"
            style={buttonStyle}
            onClick={onClickSave}
          >
            <IntlMessages id="save" />
          </button>
        </div>
        <div
          className="list__info--content-engineinfo--footer"
          style={{ display: isEdit ? "none" : "block" }}
        >
          <button
            className="btn btn--gray"
            onClick={() => setEdit(true)}
            style={buttonStyle}
          >
            <IntlMessages id="edit" />
          </button>
        </div>
        <ConfirmPopup
          isShow={confirmPopup}
          message={confirmMsg}
          callback={updateEngineEnv}
          onClose={() => closePopup("confirmPopup")}
        />
        <AlertPopup
          isShow={alertPopup}
          message={alertMsg}
          onClose={() => closePopup("alertPopup")}
        />
      </div>
    );
  }
}

export default connect(
  state => ({
    engineEnv: state.engineEnv.get("engineEnv"),
    result: state.engineEnv.get("result"),
    language: state.common.get("language")
  }),
  dispatch => ({
    EngineEnvActions: bindActionCreators(engineEnvActions, dispatch)
  })
)(EnginEnvInfo);
