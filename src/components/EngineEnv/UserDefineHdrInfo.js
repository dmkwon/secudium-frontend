import React from "react";

import UserDefineHdrTable from "./UserDefineHdrTable";

export default class UserDefineHdrInfo extends React.Component {

  render() {

    return (
      <div className="list__info--content-userdefinehdr" style={{overflowY: "auto"}}>
        <div className="component__asset">
          <UserDefineHdrTable />
        </div>
      </div>
    )
  }
}