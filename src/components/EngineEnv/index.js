import React, { Component } from "react";

import Navbar from '../Common/Navbar';
import LeftMenu from '../Common/LeftMenu';

import EngineEnvDetail from "./EngineEnvDetail";

import './style.scss';

export default class EngineEnvComponent extends Component {
  
  /** 
   * engine 환경 설정 정보
   */
  render() {
  
    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper engine-env">
          <LeftMenu />
          <EngineEnvDetail />
        </div>
      </React.Fragment>
    );
  }
}