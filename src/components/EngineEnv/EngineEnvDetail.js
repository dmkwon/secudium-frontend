import React, { Component } from "react";

import "./style.scss";
import EnginEnvInfo from "./EnginEnvInfo";
import CommandInfo from "./CommandInfo";
import UserDefineHdrInfo from "./UserDefineHdrInfo";
import { FormattedMessage } from "react-intl";

export default class EngineEnvDetail extends Component {
  state = {
    sectionList: [
      {
        validation: false,
        title: (
          <FormattedMessage id="engine.setting">
            {text => text}
          </FormattedMessage>
        ),
        summary: (
          <FormattedMessage id="zookeeper.env.setting">
            {text => text}
          </FormattedMessage>
        ),
        value: "CON",
        isShow: true
      },
      {
        validation: false,
        title: (
          <FormattedMessage id="engine.instruct.setting">
            {text => text}
          </FormattedMessage>
        ),
        summary: (
          <FormattedMessage id="zookeeper.manage.command">
            {text => text}
          </FormattedMessage>
        ),
        value: "CMD",
        isShow: false
      },
      {
        validation: false,
        title: (
          <FormattedMessage id="custom.header">{text => text}</FormattedMessage>
        ),
        summary: (
          <FormattedMessage id="manage.header.info">
            {text => text}
          </FormattedMessage>
        ),
        value: "UDH",
        isShow: false
      }
    ]
  };

  toggleViewList = idx => {
    const sectionList = [...this.state.sectionList];
    sectionList[idx].isShow = !sectionList[idx].isShow;
    this.setState({
      sectionList
    });
  };

  handleChange = (e, index) => {
    const sectionList = [...this.state.sectionList];
    sectionList[index][e.target.name] = e.target.value;
    this.setState({ sectionList });
  };

  render() {
    const { sectionList } = this.state;

    const ruleRow = sectionList.map((section, index) => {
      return (
        <div
          className={`list ${section.isShow === true ? "open" : ""}`}
          key={index}
        >
          {/* <div className={`list__badge ${section.validation ? 'check' : 'warn'}`} /> */}
          <div className="list__info">
            <div
              className="list__info--title"
              onClick={() => this.toggleViewList(index)}
            >
              <b>{section.title}</b>
              <p>{section.summary}</p>
            </div>
            {section.value === "CON" ? <EnginEnvInfo /> : ""}
            {section.value === "CMD" ? <CommandInfo /> : ""}
            {section.value === "UDH" ? <UserDefineHdrInfo /> : ""}
          </div>
        </div>
      );
    });

    return (
      <div className="component component-detail">
        <div className="component__title">
          <FormattedMessage id="engine.env.setting">
            {text => text}
          </FormattedMessage>
        </div>
        <div className="top" style={{ margin: "0" }}>
          <div className="accordion-list" style={{ height: "760px" }}>
            {ruleRow}
          </div>
        </div>
      </div>
    );
  }
}
