import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Textarea } from "react-inputs-validation";
import Draggable from "react-draggable";

import * as util from "./../../../utils";

import ConfirmPopup from "./ConfirmPopup";
import * as engineEnvActions from "./../../../store/modules/engineEnv";
import { FormattedMessage } from "react-intl";

const regvalidation = {
  validate: false,
  hasErrZkCmd: true,
  hasErrZkDataPath: true,
  hasErrZkWatchPath: true,
  hasErrZkWatchMsg: true
};

const editValidation = {
  validate: false,
  hasErrZkCmd: false,
  hasErrZkDataPath: false,
  hasErrZkWatchPath: false,
  hasErrZkWatchMsg: false
};

class CommandDetailPopup extends Component {
  constructor(props) {
    super(props);

    //seq가 있을경우 수정, 없으면 신규등록
    const isEdit = util.isValid(props.data.engineCmdSeq);
    const callback = isEdit ? this.updateCommand : this.insertCommand;
    const validation = isEdit ? editValidation : regvalidation;

    this.state = {
      command: props.data,
      confirmPopup: false,
      callback,
      validation,
      position: {
        x: 0,
        y: 0
      },
      isEdit: isEdit
    };
  }

  componentDidMount = () => {
    const elWidth = document.getElementById("commandDetailPopup").offsetWidth;
    const elHeight = document.getElementById("commandDetailPopup").offsetHeight;

    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    this.toggleValidating(true);

    const {
      hasErrZkCmd,
      hasErrZkDataPath,
      hasErrZkWatchPath,
      hasErrZkWatchMsg
    } = this.state.validation;

    return (
      !hasErrZkCmd &&
      !hasErrZkDataPath &&
      !hasErrZkWatchPath &&
      !hasErrZkWatchMsg
    );
  };

  onClickSave = () => {
    if (this.isValidate()) {
      this.openPopup("confirmPopup");
    }
  };

  commonCloseCallback = async () => {
    const { EngineEnvActions, onClose } = this.props;
    try {
      EngineEnvActions.getEngineCommands();
      this.closePopup("confirmPopup");
      onClose();
    } catch (e) {
      console.log(e);
    }
  };

  insertCommand = data => {
    const { EngineEnvActions } = this.props;
    const ajax = EngineEnvActions.insertEngineCommand(data);
    ajax.then(() => {
      this.commonCloseCallback();
    });
  };

  updateCommand = data => {
    const { EngineEnvActions } = this.props;
    const ajax = EngineEnvActions.updateEngineCommand(data);
    ajax.then(() => {
      this.commonCloseCallback();
    });
  };

  onChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      command: {
        ...this.state.command,
        [name]: value
      }
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //경로유효성 검사
  validatePath = path =>
    util.isLinuxPath(path) ? (
      true
    ) : (
      <FormattedMessage id="wrong.path"> {text => text} </FormattedMessage>
    );

  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  render() {
    const { onChangeInput, onClickSave, closePopup, handleDrag } = this;
    const { onClose } = this.props;
    const { command, confirmPopup, callback, position, isEdit } = this.state;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={handleDrag}
        >
          <div
            className="popup"
            id="commandDetailPopup"
            style={{ width: "600px" }}
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <FormattedMessage id="engine.instruct.setting">
                  {text => text}
                </FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th style={{ width: "120px" }}>
                      <FormattedMessage id="command">
                        {text => text}
                      </FormattedMessage>
                      {isEdit ? "" : <span className="font--red">*</span>}
                    </th>
                    <td>
                      <Textbox
                        name="zkCmd"
                        type="text"
                        maxLength="50"
                        value={command.zkCmd}
                        onChange={onChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationOption={{locale: this.props.language}}
                        disabled={isEdit}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrZkCmd: res,
                              validate: false
                            }
                          });
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage id="data.path">
                        {text => text}
                      </FormattedMessage>
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="zkDataPath"
                        type="text"
                        value={command.zkDataPath}
                        onChange={onChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrZkDataPath: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          customFunc: this.validatePath,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage id="watch.path">
                        {text => text}
                      </FormattedMessage>
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="zkWatchPath"
                        type="text"
                        value={command.zkWatchPath}
                        onChange={onChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrZkWatchPath: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          customFunc: this.validatePath,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage id="watch.message">
                        {text => text}
                      </FormattedMessage>
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="zkWatchMsg"
                        type="text"
                        maxLength="50"
                        value={command.zkWatchMsg}
                        onChange={onChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationOption={{locale: this.props.language}}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrZkWatchMsg: res,
                              validate: false
                            }
                          });
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th>
                      <FormattedMessage id="data.delimiter">
                        {text => text}
                      </FormattedMessage>
                    </th>
                    <td>
                      <FormattedMessage id="enter.ascii.code">
                        {placeholder => (
                          <Textbox
                            name="dataSprtn"
                            type="text"
                            maxLength={50}
                            value={command.dataSprtn}
                            onChange={onChangeInput}
                            placeholder={placeholder}
                            customStyleInput={{ width: "100%", resize: "none" }}
                          />
                        )}
                      </FormattedMessage>
                    </td>
                  </tr>
                  <tr>
                    <th colSpan="2">
                      <FormattedMessage id="header.info">
                        {text => text}
                      </FormattedMessage>
                    </th>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <FormattedMessage id="enter.delimiter.value">
                        {placeholder => (
                          <Textarea
                            name="hdrDtl"
                            type="text"
                            maxLength={4000}
                            value={command.hdrDtl}
                            onChange={onChangeInput}
                            placeholder={placeholder}
                            customStyleInput={{
                              width: "100%",
                              resize: "none",
                              height: "200px"
                            }}
                          />
                        )}
                      </FormattedMessage>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--dark" onClick={onClickSave}>
                <FormattedMessage id="save"> {text => text} </FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
        <ConfirmPopup
          isShow={confirmPopup}
          data={command}
          callback={callback}
          message={
            <FormattedMessage id="do.save"> {text => text} </FormattedMessage>
          }
          onClose={() => closePopup("confirmPopup")}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    EngineEnvActions: bindActionCreators(engineEnvActions, dispatch)
  })
)(CommandDetailPopup);
