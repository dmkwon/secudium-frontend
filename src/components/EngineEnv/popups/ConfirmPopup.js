import React from "react";
import IntlMessages from "util/IntlMessages";

const ConfirmPopup = ({ isShow, callback, data, message, onClose }) => {
  return (
    <div style={{ display: isShow ? "block" : "none" }}>
      <div className="popup popup--alert" style={{ width: "400px" }}>
        <div className="popup__header">
          <h5>
            <IntlMessages id="confirm" />
          </h5>
          <button className="btn btn-close" onClick={onClose} />
        </div>
        <div className="popup__body">{message}</div>
        <div className="popup__footer">
          <button className="btn btn--white" onClick={onClose}>
            <IntlMessages id="cancel" />
          </button>
          <button className="btn btn--dark" onClick={() => callback(data)}>
            <IntlMessages id="confirm" />
          </button>
        </div>
      </div>
      <div className="dim" />
    </div>
  );
};

export default ConfirmPopup;
