import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Textarea } from "react-inputs-validation";
import Draggable from "react-draggable";

import * as util from "./../../../utils";

import ConfirmPopup from "./ConfirmPopup";
import * as engineEnvActions from "./../../../store/modules/engineEnv";
import { FormattedMessage } from "react-intl";

const regValidation = {
  validate: false,
  hasErrNodePath: true,
  hasErrHdr: true
};

const editValidation = {
  validate: false,
  hasErrNodePath: false,
  hasErrHdr: false
};

class UserDefineHdrDetailPopup extends Component {
  constructor(props) {
    super(props);

    const isEdit = util.isValid(props.data.userDefineHdrSeq);
    const callback = isEdit
      ? this.updateUserDefineHdr
      : this.insertUserDefineHdr;
    const validation = isEdit ? editValidation : regValidation;

    this.state = {
      userDefineHdr: props.data,
      confirmPopup: false,
      callback,
      validation,
      position: {
        x: 0,
        y: 0
      }
    };
  }

  componentDidMount = () => {
    const elWidth = document.getElementById("userDefineHdrDetailPopup")
      .offsetWidth;
    const elHeight = document.getElementById("userDefineHdrDetailPopup")
      .offsetHeight;

    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  //validate 토글
  toggleValidating = validate =>
    this.setState({
      validation: {
        ...this.state.validation,
        validate
      }
    });

  //form validation
  isValidate = () => {
    //validate를 true로 설정 시 모든 form의 유효성 검사 후 validationCallback을 동해 res를 전달받는다
    this.toggleValidating(true);
    const { hasErrNodePath, hasErrHdr } = this.state.validation;
    return !hasErrNodePath && !hasErrHdr;
  };

  onClickSave = () => {
    if (this.isValidate()) {
      this.openPopup("confirmPopup");
    }
  };

  commonCloseCallback = async () => {
    const { EngineEnvActions, onClose } = this.props;
    try {
      EngineEnvActions.getUserDefineHdrs();
      this.closePopup("confirmPopup");
      onClose();
    } catch (e) {
      console.log(e);
    }
  };

  insertUserDefineHdr = data => {
    const { EngineEnvActions } = this.props;
    const ajax = EngineEnvActions.insertUserDefineHdr(data);
    ajax.then(() => {
      this.commonCloseCallback();
    });
  };

  updateUserDefineHdr = data => {
    const { EngineEnvActions } = this.props;
    const ajax = EngineEnvActions.updateUserDefineHdr(data);
    ajax.then(() => {
      this.commonCloseCallback();
    });
  };

  onChangeInput = (value, e) => {
    const { name } = e.target;
    this.setState({
      userDefineHdr: {
        ...this.state.userDefineHdr,
        [name]: value
      }
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  //경로유효성 검사
  validatePath = path =>
    util.isLinuxPath(path) ? (
      true
    ) : (
      <FormattedMessage id="wrong.path"> {text => text} </FormattedMessage>
    );

  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  render() {
    const { onChangeInput, onClickSave, closePopup, handleDrag } = this;
    const { onClose } = this.props;
    const { userDefineHdr, confirmPopup, callback, position } = this.state;
    const { validate } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={handleDrag}
        >
          <div
            className="popup"
            id="userDefineHdrDetailPopup"
            style={{ width: "600px" }}
          >
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>
                <FormattedMessage id="custom.header">
                  {text => text}
                </FormattedMessage>
              </h5>
              <button className="btn btn-close" onClick={onClose} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th style={{ width: "120px" }}>
                      <FormattedMessage id="node.path">
                        {text => text}
                      </FormattedMessage>
                      <span className="font--red">*</span>
                    </th>
                    <td>
                      <Textbox
                        name="nodePath"
                        type="text"
                        value={userDefineHdr.nodePath}
                        onChange={onChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrNodePath: res,
                              validate: false
                            }
                          });
                        }}
                        validationOption={{
                          customFunc: this.validatePath,
                          locale: this.props.language
                        }}
                        customStyleInput={{ width: "100%", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th colSpan="2">
                      <FormattedMessage id="header.info">
                        {text => text}
                      </FormattedMessage>
                      <span className="font--red">*</span>
                    </th>
                  </tr>
                  <tr>
                    <td colSpan="2">
                      <Textarea
                        name="hdr"
                        type="text"
                        maxLength={4000}
                        value={userDefineHdr.hdr}
                        onChange={onChangeInput}
                        onBlur={() => {}}
                        validate={validate}
                        validationOption={{locale: this.props.language}}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              ...this.state.validation,
                              hasErrHdr: res,
                              validate: false
                            }
                          });
                        }}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "200px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--white" onClick={onClose}>
                <FormattedMessage id="cancel">{text => text}</FormattedMessage>
              </button>
              <button className="btn btn--dark" onClick={onClickSave}>
                <FormattedMessage id="save"> {text => text} </FormattedMessage>
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" />
        <ConfirmPopup
          isShow={confirmPopup}
          data={userDefineHdr}
          callback={callback}
          message={
            <FormattedMessage id="save"> {text => text} </FormattedMessage>
          }
          onClose={() => closePopup("confirmPopup")}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    EngineEnvActions: bindActionCreators(engineEnvActions, dispatch)
  })
)(UserDefineHdrDetailPopup);
