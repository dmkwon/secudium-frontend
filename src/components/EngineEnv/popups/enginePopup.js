// HOST/IP, Port No. 설명
// 라디오버튼으로 Ping Telnet Wget 선택
// WEBUI URL 텍스트로 입력

import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as engineStatusActions from "./../../../store/modules/enginestatus";
import { createEngines } from "./../../../store/api/enginestatus";

import "./style.scss";
import { FormattedMessage } from "react-intl";

class AddIpPopup extends Component {
  state = {
    engineIp: "",
    enginePort: "",
    engineWebuiUrl: "",
    engineMonitoringType: "",
    engineDesc: ""
  };

  onChangeInput = e => {
    const { name, value } = e.target;
    this.setState({
      ...this.state,
      [name]: value
    });
  };

  onClickSave = async e => {
    const engines = this.state;
    try {
      await createEngines(engines);
      this.setState({
        engineIp: "",
        enginePort: "",
        engineWebUiUrl: "",
        engineMonitoringType: "",
        engineDesc: ""
      });
    } catch (e) {
      console.warn(e);
    }
  };

  render() {
    const { closeAllPopup } = this.props;
    const {
      engineIp,
      enginePort,
      engineWebUiUrl,
      engineMonitoringType,
      engineDesc
    } = this.state;
    const { onChangeInput, onClickSave } = this;

    return (
      <React.Fragment>
        <div className="popup" id="addIpPopup">
          <div className="popup__header">
            <h5>
              <FormattedMessage id="engine.reg">
                {text => text}
              </FormattedMessage>
            </h5>
            <button className="btn-close" onClick={closeAllPopup} />
          </div>
          <div className="popup__body">
            <div className="item-search">
              <div className="form-group">
                <label>
                  <FormattedMessage id="host">{text => text}</FormattedMessage>/
                  <FormattedMessage id="ip"> {text => text} </FormattedMessage>
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="engineIp"
                  onChange={onChangeInput}
                  value={engineIp}
                />
              </div>
              <div className="form-group">
                <label>
                  <FormattedMessage id="port.no">
                    {text => text}
                  </FormattedMessage>
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="enginePort"
                  onChange={onChangeInput}
                  value={enginePort}
                />
              </div>
              <div className="form-group">
                <label>
                  <FormattedMessage id="desc">{text => text}</FormattedMessage>
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="engineDesc"
                  onChange={onChangeInput}
                  value={engineDesc}
                />
              </div>
              <div className="form-group">
                <label>
                  <FormattedMessage id="web.ui.url">
                    {text => text}
                  </FormattedMessage>
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="engineWebUiUrl"
                  onChange={onChangeInput}
                  value={engineWebUiUrl}
                />
              </div>
              <div className="form-group">
                <label>
                  <FormattedMessage id="monitoring.type">
                    {text => text}
                  </FormattedMessage>
                </label>
                <input
                  className="form-control"
                  type="text"
                  name="engineMonitoringType"
                  onChange={onChangeInput}
                  value={engineMonitoringType}
                />
              </div>
            </div>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white" onClick={closeAllPopup}>
              <FormattedMessage id="close"> {text => text} </FormattedMessage>
            </button>
            <button className="btn btn--dark" onClick={onClickSave}>
              <FormattedMessage id="save"> {text => text} </FormattedMessage>
            </button>
          </div>
        </div>
        <div className="dim" onClick={closeAllPopup} />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    engineStatus: state.enginestatus.get("engineStatus")
  }),
  dispatch => ({
    EngineStatusActions: bindActionCreators(engineStatusActions, dispatch)
  })
)(AddIpPopup);
