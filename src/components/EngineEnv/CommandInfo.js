import React from "react";

import CommandTable from "./CommandTable";

export default class CommandInfo extends React.Component {

  render() {

    return (
      <div className="list__info--content-commandinfo" style={{overflowY: "auto"}}>
        <div className="component__asset">
          <CommandTable />
        </div>
      </div>
    )
  }
}
