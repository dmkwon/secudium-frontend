import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import CommandDetailPopup from "./popups/CommandDetailPopup";
import ConfirmPopup from "./popups/ConfirmPopup";

import * as engineEnvActions from "../../store/modules/engineEnv";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

const initCmd = {
  engineCmdSeq: "",
  zkCmd: "",
  zkDataPath: "",
  zkWatchPath: "",
  zkWatchMsg: "",
  dataSprtn: "",
  hdrDtl: "",
  useYn: ""
};

class CommandTable extends Component {
  state = {
    detailPopup: false,
    deletePopup: false,
    confirmPopup: false,
    command: initCmd
  };

  getCommands = async () => {
    const { EngineEnvActions } = this.props;
    try {
      await EngineEnvActions.getEngineCommands();
    } catch (e) {
      console.log(e);
    }
  };

  //등록
  onClickReg = () =>
    this.setState({ command: initCmd }, this.openPopup("detailPopup"));
  //row 클릭 이벤트(수정)
  onClickEdit = command =>
    this.setState({ command }, this.openPopup("detailPopup"));
  //<FormattedMessage id="delete"> { text => text } </FormattedMessage>
  onClickDel = command =>
    this.setState({ command }, this.openPopup("confirmPopup"));
  deleteEngineCommand = data => {
    const { EngineEnvActions } = this.props;
    const ajax = EngineEnvActions.deleteEngineCommand(data);
    ajax.then(() => {
      this.getCommands();
      this.closePopup("confirmPopup");
    });
  };

  openPopup = popupName => this.setState({ [popupName]: true });
  closePopup = popupName => this.setState({ [popupName]: false });

  componentDidMount() {
    this.getCommands();
  }

  render() {
    const {
      onClickReg,
      onClickEdit,
      onClickDel,
      closePopup,
      deleteEngineCommand
    } = this;
    const { commands } = this.props;
    const { command, detailPopup, confirmPopup } = this.state;

    const renderResultRows = commands => {
      return commands.length > 0
        ? commands.map((command, index) => {
            return (
              <tr key={index}>
                <td>{++index}</td>
                {/* <td style={{textAlign: "left"}}>{command.zkCmd}</td> */}
                <td style={{ textAlign: "left" }}>{command.zkDataPath}</td>
                <td style={{ textAlign: "left" }}>{command.zkWatchPath}</td>
                <td style={{ textAlign: "left" }}>{command.zkWatchMsg}</td>
                <td style={{ textAlign: "left" }}>{command.dataSprtn}</td>
                <td style={{ textAlign: "left" }}>{command.hdrDtl}</td>
                <td>{command.modUsrNm}</td>
                <td>{command.modDateYmd}</td>
                <td>
                  <button
                    className="btn btn-icon"
                    onClick={() => onClickDel(command)}
                  >
                    <img
                      id="ipDelete"
                      src="/images/common/icon_delete.png"
                      alt="icon_delete"
                    />
                  </button>
                  <button
                    className="btn btn-icon"
                    style={{ marginLeft: "2px" }}
                    onClick={() => onClickEdit(command)}
                  >
                    <img
                      id="ipUpdate"
                      src="/images/common/icon_edit.png"
                      alt="icon_edit"
                    />
                  </button>
                </td>
              </tr>
            );
          })
        : [];
    };

    return (
      <React.Fragment>
        <div className="component__title">
          <div className="btns">
            <button className="btn" onClick={onClickReg}>
              <IntlMessages id="new.reg" />
            </button>
          </div>
        </div>
        <div className="table-wrapper">
          <table className="table table--dark">
            <colgroup>
              <col width="60px" />
              {/* <col width="160px" /> */}
              <col width="290px" />
              <col width="290px" />
              <col width="100px" />
              <col width="100px" />
              <col />
              <col width="80px" />
              <col width="80px" />
              <col width="70px" />
            </colgroup>
            <thead>
              <tr>
                <th>No.</th>
                {/* <th><FormattedMessage id="command"> { text => text } </FormattedMessage></th> */}
                <th>
                  <FormattedMessage id="data.path">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>
                  <FormattedMessage id="watch.path">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>
                  <FormattedMessage id="watch.message">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>
                  <FormattedMessage id="data.delimiter">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>
                  <FormattedMessage id="header.info">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>
                  <FormattedMessage id="modifier">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>
                  <FormattedMessage id="mod.date">
                    {text => text}
                  </FormattedMessage>
                </th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{renderResultRows(commands)}</tbody>
          </table>
        </div>
        {detailPopup ? (
          <CommandDetailPopup
            isShow={detailPopup}
            data={command}
            onClose={() => {
              closePopup("detailPopup");
            }}
          />
        ) : (
          ""
        )}
        <ConfirmPopup
          isShow={confirmPopup}
          message={<IntlMessages id="do.delete" />}
          data={command}
          callback={deleteEngineCommand}
          onClose={() => closePopup("confirmPopup")}
        />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    commands: state.engineEnv.get("commands")
  }),
  dispatch => ({
    EngineEnvActions: bindActionCreators(engineEnvActions, dispatch)
  })
)(CommandTable);
