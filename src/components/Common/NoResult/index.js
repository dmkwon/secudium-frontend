import React, { Component } from 'react'

import './style.scss'

export default class NoResultComponent extends Component {
  render() {
    // 문구를 text에 담아서 보내주세요 (ex: 좌측 트리에서 필터를 선택해주세요.)
    // 결과 없음 페이지가 필요하다면, 트리 옆 하나의 div로 묶인 것 대신 사용하면 됩니다. (필터리스트 기준: .filter-result 대신 사용)
    const { text } = this.props

    return (
      <div className="component-no-result">
        <div className="no-result__box">
          <img src="/images/common/img_no_result.png" alt="No Result" />
          <p>{text}</p>
        </div>
      </div>
    )
  }
}
