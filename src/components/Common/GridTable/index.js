import React, { Component } from "react";
import ReactTable, { ReactTableDefaults } from "react-table";
import { connect } from "react-redux";

import IntlMessages from "util/IntlMessages";

import "react-table/react-table.css";
import { gridGlobalSet } from "../../../constants";
import Pagination from "./Pagination";
import "./style.scss";

class GridTable extends Component {
  static defaultProps = {
    data: []
  };

  render() {
    const { language } = this.props;
    const gridText = {
      previousText: <IntlMessages id="prev.page" />,
      nextText: <IntlMessages id="next.page" />,
      loadingText: <IntlMessages id="searching" />,
      noDataText: <IntlMessages id="no.data" />,
      pageText: <IntlMessages id="page" />,
      ofText: <IntlMessages id="of" />,
      rowsText: language === "en" ? "row" : "행"
    };

    Object.assign(ReactTableDefaults, {
      ...gridGlobalSet,
      ...gridText
    });

    return (
      <div className="table-wrapper">
        <div className="table-total-count" style={{ fontSize: "12px" }}>
          Total Count:
          {this.props.totalPage === undefined
            ? this.props.data.length
            : this.props.totalPage}
        </div>
        <ReactTable
          showPageJump={false}
          PaginationComponent={Pagination}
          {...this.props}
        />
      </div>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({})
)(GridTable);
