import React from "react";
import * as commonActions from "./../../../store/modules/common";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";

const CommonAlertPopup = ({ code, CommonActions }) => {
  const onClose = () => CommonActions.closeCommonAlert();

  /**
   *  현재 정의되어있는 에러코드들 : 코드에 맞는 에러메세지는 추가해주셔야 알람 메세지가 출력됩니다
   * 	INVALID_INPUT_VALUE(400, "C001", "Invalid Input Value"),
   *  ENTITY_NOT_FOUND(400, "C002", "Entity Not Found"),
   *  INVALID_TYPE_VALUE(400, "C003", "Invalid Type Value"),
   *  HANDLE_ACCESS_DENIED(403, "C004", "Access is Denied"),
   *  METHOD_NOT_ALLOWED(405, "C005", "Invalid Input Value"),
   *  INTERNAL_SERVER_ERROR(500, "C006", "Internal Server Error"),
   *  NEED_LOGIN_VALUE(401, "C007", "Need Login"),
   *  UNSUPPORTED_ENCODING(500, "C008", "Unsupported Encoding Error"),
   *
   */

  const messages = {
    C004: (
      <span>
        <FormattedMessage id="no.permit" />
        <br />
        <FormattedMessage id="contact.admin" />
      </span>
    ),
    C006: (
      <span>
        <FormattedMessage id="occur.error" />
        <br />
        <FormattedMessage id="contact.admin" />
      </span>
    ),
    C009: (
      <span>
        <FormattedMessage id="wrong.zookeeper.path" />
        <br />
        <FormattedMessage id="check.engine.path" />
      </span>
    ),
    C010: (
      <span>
        <FormattedMessage id="wrong.id.ps" />
        <br />
        <FormattedMessage id="check.enter" />
      </span>
    ),
    C011: (
      <span>
        <FormattedMessage id="lock.acct" />
        <br />
        <FormattedMessage id="contact.admin" />
      </span>
    ),
    C012: (
      <span>
        <FormattedMessage id="no.id" />
      </span>
    )
  };

  return (
    <div>
      <div
        className="popup popup--alert"
        style={{ minWidth: "350px", zIndex: "13" }}
      >
        <div className="popup__header">
          <h5>
            <FormattedMessage id="alarm" />
          </h5>
          <button className="btn btn-close" onClick={onClose} />
        </div>
        <div className="popup__body">{messages[code] || messages["C006"]}</div>
        <div className="popup__footer">
          <button className="btn btn--dark" onClick={onClose}>
            <FormattedMessage id="confirm" />
          </button>
        </div>
      </div>
      <div className="innerDim" />
    </div>
  );
};

export default connect(
  state => ({
    code: state.common.get("commonAlertCode")
  }),
  dispatch => ({
    CommonActions: bindActionCreators(commonActions, dispatch)
  })
)(CommonAlertPopup);
