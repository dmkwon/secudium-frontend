import React, { Component } from "react";
import { connect } from "react-redux";
import { Textarea } from "react-inputs-validation";
import Draggable from "react-draggable";
import { FormattedMessage } from "react-intl";

class ConfirmPopup extends Component {
  state = {
    changeReason: "",

    validation: {
      isVlidation: false,
      isEmptyChangeReason: true // has Error
    },

    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount() {
    const elWidth = document.getElementById("commonConfirmPopup").offsetWidth;
    const elHeight = document.getElementById("commonConfirmPopup").offsetHeight;

    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  }

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };

  onChangeText = (value, e) => {
    e.preventDefault();
    this.setState({ changeReason: value });
  };

  // validate 토글
  toggleValidating = isVlidation => {
    this.setState({ validation: { ...this.state.validation, isVlidation } });
  };

  checkValidation = () => {
    this.toggleValidating(true);

    const { validation } = this.state;
    return this.props.resource.hasChangeReason
      ? !validation.isEmptyChangeReason
      : true;
  };

  render() {
    const { resource } = this.props; // title, message, hasChangeReason, btns 으로 구성된 object
    const { isVlidation } = this.state.validation;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={this.state.position}
          onDrag={this.handleDrag}
        >
          <div className="popup popup--alert" id="commonConfirmPopup">
            <div className="popup__header" style={{ cursor: "move" }}>
              <h5>{resource.title}</h5>
            </div>
            <div className="popup__body">
              {resource.message}
              {resource.data}
              {resource.hasChangeReason && (
                <div className="change-reason">
                  <label>
                    <FormattedMessage id="reason.change" />
                    <span className="font--red">*</span>
                  </label>

                  <FormattedMessage id="enter.change.reason">
                    {placeholder => (
                      <Textarea
                        className="form-control"
                        placeholder={placeholder}
                        value={this.state.changeReason}
                        onChange={this.onChangeText}
                        onBlur={() => {}}
                        validate={isVlidation}
                        validationCallback={res => {
                          this.setState({
                            validation: {
                              isEmptyChangeReason: res,
                              isVlidation: false
                            }
                          });
                        }}
                        validationOption={{
                          name: <FormattedMessage id="reason.change" />,
                          check: true,
                          required: true,
                          type: "string",
                          min: 5,
                          max: 2000, // (4000/2)
                          locale: this.props.language
                        }}
                      />
                    )}
                  </FormattedMessage>
                </div>
              )}
            </div>

            <div className="popup__footer">
              {resource.btns.length > 0 && (
                <button
                  key={"btn-key-1"}
                  className="btn btn--white"
                  onClick={() => {
                    resource.btns[0].CB();
                  }}
                >
                  {resource.btns[0].name}
                </button>
              )}
              {resource.btns.length === 2 && (
                <button
                  key={"btn-key-2"}
                  className="btn btn--white"
                  onClick={() => {
                    if (this.checkValidation()) {
                      resource.btns[0].CB(); // close 후에 동작
                      resource.btns[1].CB(resource, this.state.changeReason);
                    }
                  }}
                >
                  {resource.btns[1].name}
                </button>
              )}
            </div>
          </div>
        </Draggable>
      </React.Fragment>
    );
  }
}
export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({})
)(ConfirmPopup);
