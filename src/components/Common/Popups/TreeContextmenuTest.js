import React, { Component } from "react";
import ReactDOM from "react-dom";
import Tree, { TreeNode } from "rc-tree";
import ScrollArea from "react-scrollbar";

// 참고: http://react-component.github.io/tree/examples/contextmenu.html
export default class Test extends Component {
  componentDidMount() {
    this.getContainer();
    const hardThis = this;

    document.addEventListener("click", function() {
      ReactDOM.unmountComponentAtNode(hardThis.cmContainer);
      hardThis.toolTip = null;
    });
  }
  componentWillUnmount() {
    if (this.cmContainer) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      document.body.removeChild(this.cmContainer);
      this.cmContainer = null;
    }
  }

  onRightClick = info => {
    this.renderCm(info);
  };

  getContainer() {
    if (!this.cmContainer) {
      this.cmContainer = document.createElement("div");
      document.body.appendChild(this.cmContainer);
    }
    return this.cmContainer;
  }

  renderCm(info) {
    if (this.toolTip) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      this.toolTip = null;
    }
    this.toolTip = (
      <ul className="tree-contextmenu">
        <li onClick={() => this.test("add")}>
          <FormattedMessage id="class.system.create" />
        </li>
        <li onClick={() => this.test("edit")}>
          <FormattedMessage id="class.system.edit" />
        </li>
        <li className="binder" />
        <li className="disabled" onClick={() => this.test("remove")}>
          <FormattedMessage id="class.system.delete" />
        </li>
      </ul>
    );

    const container = this.getContainer();
    Object.assign(this.cmContainer.style, {
      position: "absolute",
      zIndex: 11,
      left: `${info.event.pageX}px`,
      top: `${info.event.pageY}px`
    });

    ReactDOM.render(this.toolTip, container);
  }

  render() {
    return (
      <React.Fragment>
        <div className="popup popup--dark">
          <div className="popup__header">
            <h5>
              <FormattedMessage id="title" />
            </h5>
          </div>
          {/* smoothScrolling={true}여야 ie에서도 스크롤이 스무스하게 내려갑니다. */}
          <div className="popup__body">
            {/* 좌우 스크롤이 있는 경우(rc-tree, 필터조건 등)는 예외적으로
              scrollarea-content 에 따로 display: inline-block, min-width: 100% 넣어줘야합니다.
              => contentStyle={{display: 'inline-block', minWidth: '100%'}} */}
            <ScrollArea
              contentStyle={{ display: "inline-block", minWidth: "100%" }}
              style={{ width: 120, height: 200 }}
              smoothScrolling={true}
            >
              <Tree
                onRightClick={this.onRightClick}
                onSelect={this.onSelect}
                multiple
                defaultExpandAll
                showLine
                showIcon={true}
              >
                <TreeNode title="parent 1" key="0-1">
                  <TreeNode title="parent 1-0" key="0-1-1">
                    <TreeNode title="leaf0" isLeaf />
                    <TreeNode title="leaf1" isLeaf />
                    <TreeNode title="leaf2" isLeaf />
                  </TreeNode>
                  <TreeNode title="parent 1-1">
                    <TreeNode title="leaf" isLeaf />
                  </TreeNode>
                </TreeNode>
              </Tree>
            </ScrollArea>
          </div>
          <div className="popup__footer">
            <button className="btn btn--white">
              <FormattedMessage id="close" />
            </button>
          </div>
        </div>
        <div className="dim" />
      </React.Fragment>
    );
  }
}
