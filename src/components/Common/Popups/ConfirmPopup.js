import React, { Component } from "react";

export default class ConfirmPopup extends Component {
  render() {
    const {
      isShowPopup,
      message,
      hasChangeReason,
      hasCancelBtn,
      hasConfirmBtn
    } = this.props;
    return (
      isShowPopup && (
        <React.Fragment>
          <div className="popup popup--alert">
            <div className="popup__header">
              <h5>
                <FormattedMessage id="confirm" />
              </h5>
            </div>
            <div className="popup__body">
              {message}
              {hasChangeReason && (
                <div className="change-reason">
                  <label>
                    <FormattedMessage id="reason.change" />
                  </label>
                  <textarea className="form-control" />
                </div>
              )}
            </div>
            <div className="popup__footer">
              {hasCancelBtn && (
                <button className="btn btn--white">
                  <FormattedMessage id="cancel" />
                </button>
              )}
              {hasConfirmBtn && (
                <button className="btn btn--white">
                  <FormattedMessage id="confirm" />
                </button>
              )}
            </div>
          </div>
        </React.Fragment>
      )
    );
  }
}
