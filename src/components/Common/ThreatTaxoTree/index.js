import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Tree, { TreeNode } from "rc-tree";

import * as threatTaxonomyActions from "../../../store/modules/threattaxonomy";

import "rc-tree/assets/index.css";
import { FormattedMessage } from "react-intl";

function setLeaf(treeData) {
  const loopLeaf = treeNodes => {
    treeNodes.forEach(node => {
      if (node.children) {
        loopLeaf(node.children);
      }
      if (node.hasSubNode === "N" && !node.children) {
        node.isLeaf = true;
      }
    });
  };

  loopLeaf(treeData);
}

function getNewTreeData(treeData, curKey, childNodes) {
  const loop = data => {
    data.forEach(item => {
      if (curKey.indexOf(item.key) === 0) {
        if (item.children) {
          loop(item.children);
        } else {
          item.children = childNodes;
        }
      } else {
        if (item.children) {
          loop(item.children);
        }
      }
    });
  };

  loop(treeData);
  setLeaf(treeData);
}

function generateTreeNodes(children) {
  const arr = [];
  for (let i = 0; i < children.length; i++) {
    arr.push({
      name: `${children[i].thrtNm}`,
      key: `${children[i].tpThrtSeq}-${children[i].thrtSeq}`,
      hasSubNode: children[i].hasSubNode
    });
  }
  return arr;
}

class ThreatTaxoTree extends Component {
  state = {
    treeData: [{ key: "0", name: <FormattedMessage id="all" /> }]
  };

  loadData = async treeNode => {
    const { ThreatTaxonomyActions } = this.props;
    const targetKey = treeNode.props.eventKey;
    const treeSeq =
      targetKey.split("-").length > 1 ? targetKey.split("-")[1] : targetKey;
    try {
      const res = await ThreatTaxonomyActions.getSubNodes(treeSeq);
      const treeData = [...this.state.treeData];
      getNewTreeData(treeData, targetKey, generateTreeNodes(res.data));
      this.setState({ treeData });
      return Promise.resolve();
    } catch (error) {
      console.log(error);
    }
  };

  static defaultProps = {
    onSelect: (selectedKeys, e) =>
      console.log("threatTaxonomyTree ::: onSelect is not Defined.", {
        selectedKeys,
        e
      }),
    onCheck: (checkedKeys, e) =>
      console.log("threatTaxonomyTree ::: onCheck is not Defined.", {
        checkedKeys,
        e
      }),
    checkable: false,
    selectable: true,
    arrowColor: "#2f323c"
  };

  render() {
    const treeCls = `myCls${(this.state.useIcon && " customIcon") || ""}`;

    const loop = data => {
      return data.map(item => {
        if (item.children) {
          return (
            <TreeNode
              title={item.name}
              key={item.key}
              disableCheckbox={
                item.key === "0" || item.key === "0-1" || item.key === "0-2"
                  ? true
                  : false
              }
            >
              {loop(item.children)}
            </TreeNode>
          );
        } else {
          return (
            <TreeNode
              title={item.name}
              key={item.key}
              isLeaf={item.isLeaf}
              disableCheckbox={
                item.key === "0" || item.key === "0-1" || item.key === "0-2"
                  ? true
                  : false
              }
            />
          );
        }
      });
    };
    const { loadData } = this;
    const { onSelect, onCheck, checkable, selectable } = this.props;

    return (
      <React.Fragment>
        <Tree
          className={treeCls}
          loadData={loadData}
          defaultExpandedKeys={["0"]}
          defaultSelectedKeys={["0"]}
          selectable={selectable}
          onSelect={onSelect}
          checkable={checkable}
          checkStrictly={checkable}
          onCheck={onCheck}
        >
          {loop(this.state.treeData)}
        </Tree>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    ThreatTaxonomyActions: bindActionCreators(threatTaxonomyActions, dispatch)
  })
)(ThreatTaxoTree);
