import React, { Component } from 'react'

import './style.scss'

export default class LoadingComponent extends Component {
  render() {
    return (
      <div className="component-loading">
        <img src="/images/common/loading.gif" alt="loading" />
      </div>
    )
  }
}
