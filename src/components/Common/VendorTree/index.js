import React, { Component } from "react";
import Tree, { TreeNode } from "rc-tree";
import "rc-tree/assets/index.css";
import * as api from "./../../../store/api/common";
import IntlMessages from "util/IntlMessages";

function setLeaf(treeData) {
  const loopLeaf = treeNodes => {
    treeNodes.forEach(node => {
      if (node.children) {
        loopLeaf(node.children);
      }
      // 2 depth 까지만 요청으로 수정
      if (node.depth === 2) {
        node.isLeaf = true;
      }
    });
  };

  loopLeaf(treeData);
}

function getNewTreeData(treeData, curKey, childNodes) {
  const loop = data => {
    data.forEach(item => {
      if (curKey.indexOf(item.key) === 0) {
        if (item.children) {
          loop(item.children);
        } else {
          item.children = childNodes;
        }
      } else {
        if (item.children) {
          loop(item.children);
        }
      }
    });
  };

  loop(treeData);
  setLeaf(treeData);
}

function generateTreeNodes(parentKey, children, depth) {
  const arr = [];
  for (let i = 0; i < children.length; i++) {
    arr.push({
      parentKey: parentKey,
      name: `${children[i].text}`,
      key: `${parentKey}_${children[i].value}`,
      depth: depth,
      hasSubNode: children[i].hasSubNode
    });
  }
  return arr;
}

export default class VendorTree extends Component {
  _isMounted = false;
  _updating = false;

  state = {
    treeData: [
      { parentKey: -1, key: 0, name: <IntlMessages id="all" />, depth: 0 }
    ]
  };

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  loadData = async treeNode => {
    if (this._updating) return;

    this._updating = true;
    const targetKey = treeNode.props.eventKey;
    const pos = treeNode.props.pos;
    const depth = pos.split("-").length - 1;
    if (depth > 2) return Promise.resolve();

    let res;
    if (depth === 1) {
      // 최상위 depth
      try {
        res = await api.getVendors();
      } catch (error) {
        console.log(error);
        this._updating = false;
        return Promise.resolve();
      }
    } else if (depth === 2) {
      try {
        res = await api.getSatcOfModel(targetKey.split("_")[1]);
      } catch (error) {
        console.log(error);
        this._updating = false;
        return Promise.resolve();
      }
    }
    // 2 depth 까지만 요청으로 수정
    // else if (depth === 3) {
    //   try {
    //     res = await api.getModelsOfSatc(
    //       targetKey.split('_')[1],
    //       targetKey.split('_')[2]
    //     );
    //   } catch (error) {
    //     console.log(error);
    //     return Promise.resolve();
    //   }
    // }
    if (res !== undefined) {
      const treeData = [...this.state.treeData];
      getNewTreeData(
        treeData,
        targetKey,
        generateTreeNodes(targetKey, res.data, depth)
      );
      this.setState({ treeData });
    }
    this._updating = false;
    return Promise.resolve();
  };

  static defaultProps = {
    onSelect: (selectedKeys, e) =>
      console.log("VendorTree ::: onSelect is not Defined.", {
        selectedKeys,
        e
      })
  };

  render() {
    const treeCls = `myCls${(this.state.useIcon && " customIcon") || ""}`;

    const loop = data => {
      return data.map(item => {
        if (item.children) {
          return (
            <TreeNode parent={item.parentKey} title={item.name} key={item.key}>
              {loop(item.children)}
            </TreeNode>
          );
        } else {
          return (
            <TreeNode
              parent={item.parentKey}
              title={item.name}
              key={item.key}
              isLeaf={item.isLeaf}
              disabled={item.key === "0-0-0-0"}
            />
          );
        }
      });
    };
    const { loadData, _isMounted } = this;
    const { onSelect } = this.props;

    return (
      <React.Fragment>
        {_isMounted && (
          <Tree
            className={treeCls}
            onSelect={onSelect}
            loadData={loadData}
            defaultExpandedKeys={["0"]}
            selectable
          >
            {loop(this.state.treeData)}
          </Tree>
        )}
      </React.Fragment>
    );
  }
}
