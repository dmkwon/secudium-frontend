import React, { Component } from "react";

import { NavLink } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import * as commonActions from "./../../../store/modules/common";
import * as menuActions from "./../../../store/modules/menu";
import jwtService from "services/jwtService";

import Dropdown from "components/Common/Dropdown";

import IntlMessages from "util/IntlMessages";

import "./style.scss";

class NavbarComponent extends Component {
  constructor(props) {
    super(props);

    this.setWrapperRef = this.setWrapperRef.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  state = {
    isShowInfo: false
  };

  handleChangeForDropdown = async ({ value }) => {
    const { CommonActions } = this.props;
    // server 로 날려야 한다.
    try {
      await CommonActions.setLanguage(value);
      await CommonActions.getMenus();
      jwtService.emit("onChangeLang");
    } catch (err) {
      console.error("err", err);
    }
  };

  onClickSignOut = () => {
    jwtService.logout();
  };

  initComponent() {
    const { CommonActions } = this.props;
    CommonActions.getMenus();
  }

  componentDidMount() {
    this.initComponent();
    document.addEventListener("mousedown", this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.closeAcctInfo();
    }
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  toggleInfo = () => {
    this.setState({
      isShowInfo: !this.state.isShowInfo
    });
  };

  closeAcctInfo = () => {
    if (this.state.isShowInfo === true) {
      this.setState({
        isShowInfo: false
      });
    }
  };

  render() {
    const { isShowInfo } = this.state;
    const { menu, languages, language } = this.props;
    const { toggleInfo, onClickSignOut, handleChangeForDropdown } = this;

    const usrNm = localStorage.getItem("USR_NM"),
      usrEmail = localStorage.getItem("USR_EMAIL"),
      lastLoginDate = localStorage.getItem("LAST_LOGIN_DATE");

    return (
      <React.Fragment>
        <nav className="navbar">
          <div className="navbar__logo">
            <div className="logo" />
          </div>
          <ul className="navbar__list">
            {menu.map((item, index) =>
              item.topMenuSeq === 0 && item.useYn === "Y" ? (
                <li key={index}>
                  <NavLink to={item.menuUrl} activeClassName="active">
                    {item.menuNm}
                  </NavLink>
                </li>
              ) : (
                ""
              )
            )}
          </ul>
          <div className="navbar__info">
            {/* <div className="time">2018/01/18 21:09:00</div> */}
            <Dropdown
              name="language"
              options={languages}
              selected={language}
              onChange={handleChangeForDropdown}
            />

            <ul className="menu">
              {/* <li className="noti">
              <img src="/images/common/icon-bell.png" alt="bell" />
            </li> */}
              <li className="user" ref={this.setWrapperRef}>
                <img
                  src="/images/common/icon-account.png"
                  alt="account"
                  onClick={toggleInfo}
                />
                {isShowInfo && (
                  <div className="sub-info">
                    <b>My Account</b>
                    <p>{`${usrNm}`}</p>
                    <b>E-mail</b>
                    <p>{`${usrEmail}`}</p>
                    <b>
                      <IntlMessages id="login.last.time" />
                    </b>
                    <p>{lastLoginDate}</p>
                    <button className="btn btn--dark" onClick={onClickSignOut}>
                      Logout
                    </button>
                  </div>
                )}
              </li>
              {/* <li className="help">
              <img src="/images/common/icon-question.png" alt="question" />
            </li> */}
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language"),
    languages: state.common.get("languages"),
    menu: state.common.get("menu"),
    children: PropTypes.element.isRequired
  }),
  dispatch => ({
    CommonActions: bindActionCreators(commonActions, dispatch),
    MenuActions: bindActionCreators(menuActions, dispatch)
  })
)(NavbarComponent);
