import React from "react";
import { FormattedMessage } from "react-intl";

const AlertPopup = ({ message, onClose }) => {
  return (
    <div>
      <div
        className="popup popup--alert"
        style={{ minWidth: "350px", zIndex: "13" }}
      >
        <div className="popup__header">
          <h5>
            <FormattedMessage id="alarm" />
          </h5>
          <button className="btn btn-close" onClick={onClose} />
        </div>
        <div className="popup__body">{message}</div>
        <div className="popup__footer">
          <button className="btn btn--dark" onClick={onClose}>
            <FormattedMessage id="confirm" />
          </button>
        </div>
      </div>
      <div className="innerDim" />
    </div>
  );
};

export default AlertPopup;
