import React, { Component } from "react";
import Tree, { TreeNode } from "rc-tree";
import equal from "fast-deep-equal";
import { v4 as getUUID } from "uuid";

import * as util from "./../../../utils";
import * as api from "../../../store/api/filter";
import { getCodeList } from "../../../store/api/common";

import AlertPopup from "./AlertPopup";
import ConfirmPopup from "./ConfirmPopup";
import SelectFilterPopup from "./../../FilterMgmt/popups/SelectFilterPopup";

import "rc-tree/assets/index.css";
import { FormattedMessage } from "react-intl";
import IntlMessages from "util/IntlMessages";

const arrowPath =
  "M869 487.8L491.2 159.9c-2.9-2.5-6.6-3.9-10.5-3.9h-88" +
  ".5c-7.4 0-10.8 9.2-.2 14l350.2 304H152c-4.4 0-8 3.6-8 8v60c0 4.4 3." +
  "6 8 8 8h585.1L386.9 854c-5.6 4.9-2.2 14 5.2 14h91.5c1.9 0 3.8-0.7 5." +
  "2-2L869 536.2c14.7-12.8 14.7-35.6 0-48.4z";

const getSvgIcon = (path, iStyle = {}, style = {}) => {
  return (
    <i style={iStyle}>
      <svg
        viewBox="0 0 1024 1024"
        width="1em"
        height="1em"
        fill="currentColor"
        style={{ verticalAlign: "-.125em", ...style }}
      >
        <path d={path} />
      </svg>
    </i>
  );
};

const initTreeData = [];
const charSet = {
  AND: "&",
  OR: "||",
  NOT: "!="
};
const FILTER_LIMIT = 3;

export default class FilterTree extends Component {
  constructor(props) {
    super(props);
    const treeData =
      props.data && props.data.length > 0 ? props.data : initTreeData;
    this.state = {
      treeData,
      selectedKeys: [],
      expandedKeys: treeData.map(node => node.nodeSeq),
      disabled: props.disabled,
      isShowSelectFilter: false, //필터선택팝업
      isShowFilterInfo: false, //필터정보팝업
      confirmPopup: false,
      alertPopup: false,
      alertMsg: "",

      groupOprtr: [],

      operator: [],
      filter: []
    };
  }

  static defaultProps = {
    onSelect: (selectedKeys, e) =>
      console.log("threatTaxonomyTree ::: onSelect is not Defined.", {
        selectedKeys,
        e
      }),
    btnVisibled: true
  };

  onSelectNode = (selectedKeys, e) => {
    this.setState({ selectedKeys, selectedNode: e.node.props });
  };

  //연산자노드 추가
  addOperNode = nodeType => {
    const parentKey = this.getSelectedKey();

    //노드가 선택된 경우
    if (util.isValid(parentKey)) {
      if (this.state.selectedNode.isLeaf) {
        this.showAlert(<FormattedMessage id="no.child.node" />);
        return;
      }

      const parent = this.state.treeData.find(
        node => node.nodeSeq === parentKey
      );
      const children = this.state.treeData.filter(
        node => node.parentNodeSeq === parent.nodeSeq
      );
      const nodeSeq = getUUID();

      this.setState(
        {
          treeData: [
            ...this.state.treeData,
            {
              nodeSeq: nodeSeq,
              parentNodeSeq: parent.nodeSeq,
              cndtnGroupOprtr: nodeType,
              filterSeq: "",
              filterNm: "",
              detectCndtnLevel: parent.detectCndtnLevel + 1,
              detectCndtnSortOrder: children.length + 1
            }
          ],
          expandedKeys: [...this.state.expandedKeys, nodeSeq]
        },
        () => this.props.onChange(this.state.treeData)
      );
    } else {
      const children = this.state.treeData.filter(
        node => node.parentNodeSeq === "0"
      );
      const nodeSeq = getUUID();

      this.setState(
        {
          treeData: [
            ...this.state.treeData,
            {
              nodeSeq: nodeSeq,
              parentNodeSeq: "0",
              cndtnGroupOprtr: nodeType,
              filterSeq: "",
              filterNm: "",
              detectCndtnLevel: 1,
              detectCndtnSortOrder: children.length + 1
            }
          ],
          expandedKeys: [...this.state.expandedKeys, nodeSeq]
        },
        () => this.props.onChange(this.state.treeData)
      );
    }
  };

  addFilterNode = filter => {
    const parentKey = this.getSelectedKey();
    const parent = this.state.treeData.find(node => node.nodeSeq === parentKey);
    const children = this.state.treeData.filter(
      node => node.parentNodeSeq === parent.nodeSeq
    );
    const nodeSeq = getUUID();

    this.setState(
      {
        treeData: [
          ...this.state.treeData,
          {
            nodeSeq: nodeSeq,
            parentNodeSeq: parent.nodeSeq,
            filterSeq: filter.filterSeq,
            cndtnGroupOprtr: "",
            filterNm: filter.name,
            detectCndtnLevel: parent.detectCndtnLevel + 1,
            detectCndtnSortOrder: children.length + 1
          }
        ],
        expandedKeys: [...this.state.expandedKeys, nodeSeq]
      },
      () => this.props.onChange(this.state.treeData)
    );
  };

  onClickAddFilter = () => {
    const selectedKey = this.getSelectedKey();
    if (!util.isValid(selectedKey)) {
      this.showAlert(<FormattedMessage id="no.node" />);
      return;
    }
    if (this.state.selectedNode.isLeaf) {
      this.showAlert(<FormattedMessage id="no.child.node" />);
      return;
    }

    const filterNode = this.state.treeData.filter(node =>
      Boolean(node.filterNm)
    );
    if (filterNode.length + 1 > FILTER_LIMIT) {
      this.showAlert(
        <FormattedMessage
          id="filter.add.limit"
          values={{ limit: FILTER_LIMIT }}
        />
      );
      return;
    }

    this.openPopup("isShowSelectFilter");
  };

  //현재 선택된 노드 키
  getSelectedKey = () => this.state.selectedKeys[0];

  /** 노드삭제 버튼 클릭 */
  onClickDeleteNode = (deleteKey, e) => {
    e.stopPropagation();

    const nodeSeqArr = [];
    const appendChildren = (arr, deleteKeys) => {
      if (!Array.isArray(deleteKeys)) {
        deleteKeys = [deleteKeys];
      }
      arr = arr.concat(deleteKeys);

      const children = [];
      this.state.treeData.forEach(node => {
        //노드들 중 자식노드들을 탐색
        if (deleteKeys.indexOf(node.parentNodeSeq) !== -1) {
          children.push(node.nodeSeq);
        }
      });
      return children.length > 0 ? appendChildren(arr, children) : arr;
    };

    const deleteNodeSeq = appendChildren(nodeSeqArr, deleteKey);
    this.setState({ deleteNodeSeq }, () => {
      if (deleteNodeSeq.length > 1) {
        this.openPopup("deletePopup");
      } else {
        this.deleteNode();
      }
    });
  };

  /** 노드삭제 */
  deleteNode = () => {
    const deleteNodeSeq = this.state.deleteNodeSeq;
    // const treeData = this.state.treeData.filter((node) => node.nodeSeq !== deleteKey);
    const treeData = this.state.treeData.filter(
      node => deleteNodeSeq.indexOf(node.nodeSeq) === -1
    );
    this.setState(
      {
        treeData,
        expandedKeys: this.state.expandedKeys.filter(
          key => deleteNodeSeq.indexOf(key) === -1
        ),
        selectedKeys: this.state.selectedKeys.filter(
          key => deleteNodeSeq.indexOf(key) === -1
        )
      },
      () => {
        this.props.onChange(this.state.treeData);
        this.closePopup("deletePopup");
      }
    );
  };

  //초기화(모든 노드 삭제)
  onClickReset = () => {
    this.openPopup("resetPopup");
  };

  deleteAllNode = () =>
    this.setState({ treeData: initTreeData, resetPopup: false });

  //필터정보 팝업
  showNodeInfo = async (node, e) => {
    e.stopPropagation();
    const res = await api.getFilterBySeq(node.filterSeq);
    this.setState({ target: res.data }, () =>
      this.openPopup("isShowFilterInfo")
    );
  };

  //노드가 펼쳐질때
  onExpandNode = (expandedKeys, e) => {
    this.setState({ expandedKeys });
  };

  /** 팝업 제어 */
  closePopup = popupName => this.setState({ [popupName]: false });
  openPopup = popupName => this.setState({ [popupName]: true });
  showAlert = alertMsg => this.setState({ alertMsg, alertPopup: true });

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!equal(nextProps, this.props)) {
      this.setState({
        treeData: nextProps.data,
        disabled: nextProps.disabled,
        expandedKeys: nextProps.data.map(node => node.nodeSeq)
      });
    }
  }

  getCodes = async () => {
    try {
      const groupOprtr = await getCodeList("GROUP_OPRTR");
      this.setState({ groupOprtr: groupOprtr.data });
    } catch (err) {
      console.warn(err);
    }
  };

  componentDidMount() {
    this.getCodes();
  }

  render() {
    const switcherIcon = obj => {
      if (obj.isLeaf) {
        return getSvgIcon(
          arrowPath,
          { cursor: "pointer", backgroundColor: "#2f323c" },
          { transform: "rotate(90deg)" }
        );
      }
      return getSvgIcon(
        arrowPath,
        { cursor: "pointer", backgroundColor: "#2f323c" },
        { transform: `rotate(${obj.expanded ? 90 : 0}deg)` }
      );
    };

    const treeCls = `myCls${(this.state.useIcon && " customIcon") || ""}`;

    const loop = (data, parent) => {
      const target = data.filter(node => node.parentNodeSeq === parent);
      target.sort((a, b) => {
        //order순으로 정렬
        if (a.detectCndtnSortOrder > b.detectCndtnSortOrder) return 1;
        else if (a.detectCndtnSortOrder < b.detectCndtnSortOrder) return -1;
        else return 0;
      });
      return target.map(node => {
        const children = data.filter(
          child => child.parentNodeSeq === node.nodeSeq
        );
        if (children.length > 0) {
          return (
            <TreeNode
              title={getTitle(node)}
              key={node.nodeSeq}
              isLeaf={node.filterNm ? true : false}
              icon={
                node.cndtnGroupOprtr ? (
                  <span className={`icon icon-${node.cndtnGroupOprtr}`}>
                    {charSet[node.cndtnGroupOprtr]}
                  </span>
                ) : (
                  ""
                )
              }
            >
              {loop(data, node.nodeSeq)}
            </TreeNode>
          );
        } else {
          return (
            <TreeNode
              title={getTitle(node)}
              key={node.nodeSeq}
              icon={
                node.cndtnGroupOprtr ? (
                  <span className={`icon icon-${node.cndtnGroupOprtr}`}>
                    {charSet[node.cndtnGroupOprtr]}
                  </span>
                ) : (
                  ""
                )
              }
              isLeaf={node.filterNm ? true : false}
            />
          );
        }
      });
    };

    //TreeNode title 생성
    const getTitle = node => {
      const style = {
        height: "20px",
        marginLeft: "3px",
        display: this.props.btnVisibled ? "inline" : "none"
      };
      return (
        <React.Fragment>
          <span style={{ marginRight: "20px" }}>
            {node.filterNm || node.cndtnGroupOprtr}
          </span>
          <button
            className="btn btn--dark"
            style={style}
            disabled={this.state.disabled}
            onClick={e => this.onClickDeleteNode(node.nodeSeq, e)}
          >
            <IntlMessages id="delete" />
          </button>
          {node.filterNm ? ( //필터일 경우에만 정보 버튼을 렌더링
            <button
              className="btn btn--dark"
              style={style}
              // onClick={(e) => this.showNodeInfo(node.filterSeq, e)}
              onClick={e => this.showNodeInfo(node, e)}
            >
              <IntlMessages id="info" />
            </button>
          ) : (
            ""
          )}
        </React.Fragment>
      );
    };

    const {
      onSelectNode,
      onExpandNode,
      addOperNode,
      addFilterNode,
      deleteAllNode,
      onClickAddFilter,
      closePopup,
      deleteNode,
      onClickReset
    } = this;
    const {
      selectedKeys,
      expandedKeys,
      treeData,
      disabled,
      isShowSelectFilter,
      isShowFilterInfo,
      target
    } = this.state;

    const style = { marginRight: "5px", width: "70px" };
    const disabledStyle = { display: disabled ? "none" : "block" };

    return (
      <React.Fragment>
        <span className="icon-and" />
        <div className="component__title">
          <span>
            <IntlMessages id="detect.condition" />
          </span>
          <div className="btns btns--left" style={disabledStyle}>
            {this.state.groupOprtr.map(({ id }, index) => {
              return (
                <button
                  key={index}
                  className={`btn btn-icon small btn--${id.toLowerCase()}`}
                  disabled={disabled}
                  onClick={() => addOperNode(id)}
                />
              );
            })}
          </div>
          <div className="binder" style={disabledStyle} />
          <div className="btns btns--right" style={disabledStyle}>
            <button
              className="btn btn--dark"
              disabled={disabled}
              onClick={onClickReset}
            >
              <IntlMessages id="reset" />
            </button>
            <button
              className="btn btn--dark"
              disabled={disabled}
              onClick={onClickAddFilter}
            >
              <IntlMessages id="filter.select" />
            </button>
          </div>
        </div>
        <div className="component__box" style={{ height: "400px" }}>
          <Tree
            id="filterTree"
            switcherIcon={switcherIcon}
            className={treeCls}
            onSelect={onSelectNode}
            onExpand={onExpandNode}
            selectedKeys={selectedKeys}
            expandedKeys={expandedKeys}
            selectable
          >
            {loop(treeData, "0")}
          </Tree>
        </div>
        {isShowSelectFilter && ( // 필터 선택 팝업
          <SelectFilterPopup
            closePopup={() => closePopup("isShowSelectFilter")}
            isFilterInfoViewType={false}
            seletcFilterCB={addFilterNode}
            setLoading={() => {}}
          />
        )}
        {isShowFilterInfo && ( // 필터 정보 팝업
          <SelectFilterPopup
            closePopup={() => closePopup("isShowFilterInfo")}
            isFilterInfoViewType={true}
            setLoading={() => {}}
            target={target}
          />
        )}
        {this.state.resetPopup && (
          <ConfirmPopup
            onClose={() => closePopup("resetPopup")}
            onClickConfirm={deleteAllNode}
            message={<IntlMessages id="reset.detect.condition" />}
          />
        )}
        {this.state.deletePopup && (
          <ConfirmPopup
            onClose={() => closePopup("deletePopup")}
            onClickConfirm={deleteNode}
            message={<IntlMessages id="delete.filed.operator" />}
          />
        )}
        {this.state.alertPopup && (
          <AlertPopup
            onClose={() => closePopup("alertPopup")}
            message={this.state.alertMsg}
          />
        )}
      </React.Fragment>
    );
  }
}
