import React from "react";
import { FormattedMessage } from "react-intl";

const ConfirmPopup = ({ message, onClickConfirm, onClose }) => {
  return (
    <div>
      <div
        className="popup popup--alert"
        style={{ width: "400px", zIndex: "13" }}
      >
        <div className="popup__header">
          <h5>
            <FormattedMessage id="confirm" />
          </h5>
          <button className="btn btn-close" onClick={onClose} />
        </div>
        <div className="popup__body">{message}</div>
        <div className="popup__footer">
          <button className="btn btn--white" onClick={onClose}>
            <FormattedMessage id="cancel" />
          </button>
          <button className="btn btn--dark" onClick={onClickConfirm}>
            <FormattedMessage id="confirm" />
          </button>
        </div>
      </div>
      <div className="innerDim" />
    </div>
  );
};

export default ConfirmPopup;
