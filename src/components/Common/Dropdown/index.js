import React, { Component } from "react";
import PropTypes from "prop-types";

import * as util from "./../../../utils";

import "./style.scss";
import { FormattedMessage } from "react-intl";

export default class DropdownComponent extends Component {
  state = {
    isShowMenu: false
  };

  static propTypes = {
    options: PropTypes.arrayOf(PropTypes.object),
    handleChange: PropTypes.func
  };

  static defaultProps = {
    options: [
      { text: "default1", value: 1 },
      { text: "default2", value: 2 },
      { text: "default3", value: 3 }
    ],
    selected: "",
    disabled: false,
    name: "default",
    handleChange: () => console.warn("Dropdown :: handleChange is not defined.")
  };

  openDropdown = () => {
    if (this.state.isShowMenu) {
      this.setState({ isShowMenu: false });
    } else {
      this.setState({ isShowMenu: true });
    }
  };

  closeDropdown = () => {
    this.setState({ isShowMenu: false });
  };

  render() {
    const { isShowMenu } = this.state;
    const {
      options,
      selected,
      name,
      onChange,
      disabled,
      hasSelect
    } = this.props;
    const { openDropdown, closeDropdown } = this;
    let optionMap = {};
    const optionRow =
      options &&
      options.map((option, index) => {
        const { text, value } = option;
        optionMap[value] = text;
        return (
          <li
            key={index}
            className={selected === value ? "active" : ""}
            onClick={() => {
              !hasSelect && closeDropdown();
              !hasSelect && onChange({ name, value });
            }}
          >
            {hasSelect ? (
              <div className="checkbox checkbox--dark">
                <label>
                  <input type="checkbox" />
                  <div className="icon" />
                  <span>{text}</span>
                </label>
              </div>
            ) : (
              text
            )}
          </li>
        );
      });

    return (
      <React.Fragment>
        <div className={`dropdown ${isShowMenu ? "open" : ""}`}>
          <button
            className="dropdown-toggle"
            onClick={!disabled ? openDropdown : null}
          >
            {util.isValid(selected) ? (
              optionMap[selected]
            ) : (
              <FormattedMessage id="select" />
            )}
          </button>
          {isShowMenu && <ul className="dropdown-list">{optionRow}</ul>}
        </div>
        <div className="dropdown-dim" onClick={closeDropdown} />
      </React.Fragment>
    );
  }
}
