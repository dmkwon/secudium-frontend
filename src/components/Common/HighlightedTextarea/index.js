import React, { Component } from "react";
import PropTypes from "prop-types";

import "./styles.css";

export default class HighlightedTextarea extends Component {

  static OPEN_MARK = "<mark>";
  static CLOSE_MARK = "</mark>";
  
  static propTypes = {
    regexp: PropTypes.instanceOf(RegExp),
    value: PropTypes.string,
    onChange: PropTypes.func
  };

  static defaultProps = {
    regexp: new RegExp(""),
    value: "",
    onChange: () => console.warn("HighlightedTextarea :: onChange is not defined.")
  };

  constructor(props) {
    super(props);
    this.state = {
      input: props.value || ""
    };
  }

  handleInputChange = (e) => {
      const { value } = e.target;
      this.setState({ input: value });
      this.props.onChange(e);
  }
  
  handleScroll = (e) => {
    const scrollTop = e.target.scrollTop;
    this.refs.backdrop.scrollTop = scrollTop;
  };

  handleRegexHighlight = (input, regexp) => input.replace(regexp, HighlightedTextarea.OPEN_MARK + "$&" + HighlightedTextarea.CLOSE_MARK);

  getHighlights = () => {
    let highlightMarks = this.props.value;
    const regexp = this.props.regexp;

    // escape HTML
    highlightMarks = highlightMarks.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    highlightMarks = this.handleRegexHighlight(highlightMarks, regexp);
 
    // this keeps scrolling aligned when input ends with a newline
    return highlightMarks.replace(new RegExp(`\\n(${HighlightedTextarea.CLOSE_MARK})?$`), "\n\n$1");
  }

  render() {
    const { handleScroll, getHighlights } = this;
    const { value, name, style, isDisabled, onChange } = this.props;

    return (
      <div className="hwt-container" style={style}>
        <div className="hwt-backdrop" ref="backdrop">
          <div className="hwt-highlights hwt-content" 
            dangerouslySetInnerHTML={{ __html: getHighlights() }} 
          />
        </div>
        <textarea className="hwt-input hwt-content"
          onChange={onChange}
          onScroll={handleScroll}
          value={value}
          disabled={isDisabled}
          name={name}
        />
      </div>
    );
  }
}