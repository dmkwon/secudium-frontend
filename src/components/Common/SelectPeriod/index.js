import React, { Component } from "react";
import Calendar from "react-calendar";

import "./style.scss";
import { FormattedMessage } from "react-intl";

export default class SelectPeriodComponent extends Component {
  state = {
    isShowPeriod: false,
    selectedPeriodTab: "quick",
    toDate: new Date(),
    fromDate: new Date()
  };

  openPeriodDiv = () => {
    this.setState({
      isShowPeriod: true
    });
  };

  onChangeForTo = toDate => {
    this.setState({ toDate });
  };
  onChangeForFrom = fromDate => {
    this.setState({ fromDate });
  };

  selectPeriodTab = tab => {
    this.setState({
      selectedPeriodTab: tab
    });
  };

  closePeriodDiv = () => {
    this.setState({
      isShowPeriod: false
    });
  };

  render() {
    const { isShowPeriod, selectedPeriodTab, toDate, fromDate } = this.state;

    let toDay = toDate.getDate();
    toDay = toDay >= 10 ? toDay : `0${toDay}`;
    let toMonth = toDate.getMonth() + 1;
    toMonth = toMonth >= 10 ? toMonth : `0${toMonth}`;
    const toYear = toDate.getFullYear();
    const fullToDate = `${toYear}-${toMonth}-${toDay}`;

    let fromDay = fromDate.getDate();
    fromDay = fromDay >= 10 ? fromDay : `0${fromDay}`;
    let fromMonth = fromDate.getMonth() + 1;
    fromMonth = fromMonth >= 10 ? fromMonth : `0${fromMonth}`;
    const fromYear = fromDate.getFullYear();
    const fullFromDate = `${fromYear}-${fromMonth}-${fromDay}`;

    return (
      <div className="select-period">
        <span onClick={this.openPeriodDiv}>
          <img
            src="/images/common/icon_time.png"
            alt="icon_time"
            style={{ width: 20, verticalAlign: "middle", marginTop: -2 }}
          />
          {fullFromDate} to {fullToDate}
        </span>

        {isShowPeriod && (
          <div className="period">
            <ul className="period-tabs">
              <li
                className={selectedPeriodTab === "quick" ? "active" : ""}
                onClick={() => this.selectPeriodTab("quick")}
              >
                Quick
              </li>
              <li
                className={selectedPeriodTab === "absolute" ? "active" : ""}
                onClick={() => this.selectPeriodTab("absolute")}
              >
                Absolute
              </li>
            </ul>

            {selectedPeriodTab === "quick" && (
              <div className="period-content quick">
                <ul>
                  <li>Today</li>
                  <li>Last 30 minutes</li>
                  <li>Last 7 days</li>
                </ul>
              </div>
            )}
            {selectedPeriodTab === "absolute" && (
              <div className="period-content absolute">
                <div className="from">
                  <div className="absolute__title">From</div>
                  <input
                    type="text"
                    className="form-control"
                    value={fullFromDate}
                  />
                  <Calendar
                    value={fromDate}
                    onChange={this.onChangeForFrom}
                    locale="en-US"
                    prevLabel={<button className="btn btn-icon btn-prev" />}
                    nextLabel={<button className="btn btn-icon btn-next" />}
                  />
                </div>
                <div className="to">
                  <div className="absolute__title">To</div>
                  <input
                    type="text"
                    className="form-control"
                    value={fullToDate}
                  />
                  <Calendar
                    value={toDate}
                    onChange={this.onChangeForTo}
                    locale="en-US"
                    prevLabel={<button className="btn btn-icon btn-prev" />}
                    nextLabel={<button className="btn btn-icon btn-next" />}
                  />
                </div>
              </div>
            )}
            <div className="btns">
              <button onClick={this.closePeriodDiv} className="btn btn--blue">
                <FormattedMessage id="apply" />
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}
