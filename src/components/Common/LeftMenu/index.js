import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import history from './../../../history';
import './style.scss';

class LeftMenuComponent extends Component {
	getLeftMenu = () => {
		const { menu } = this.props;
		const path = history.location.pathname;
		const navUrl = path.substring(0, path.indexOf('/', 1));

		return menu.map(
			(item, index) =>
				item.menuLevel === 2 && new RegExp(navUrl).test(item.menuUrl) && item.useYn === 'Y' ? (
					<li key={index}>
						<NavLink to={item.menuUrl} activeClassName="active">
							{item.menuNm}
						</NavLink>
					</li>
				) : (
					''
				)
		);
	};

	render() {
		const { getLeftMenu } = this;
		return (
			<div className="left-menu-wrapper">
			<div className="left-menu">
				<div className="left-menu__title">Admin</div>
				<ul className="left-menu__list">{getLeftMenu()}</ul>
			</div>
			</div>
		);
	}
}

export default connect((state) => ({
	menu: state.common.get('menu'),
}))(LeftMenuComponent);
