import React, { Component } from "react";
import Calendar from "react-calendar";
import PropTypes from "prop-types";

import "./style.scss";

export default class CalendarComponent extends Component {
  state = {
    isShowCalendar: false,
    date: this.props.date
  };

  static defaultProps = {
    date: new Date(),
    onChange: () => console.warn("Calendar :: onChange is not defined.")
  };

  static propTypes = {
    date: PropTypes.instanceOf(Date),
    onChange: PropTypes.func
  }

  openCalendar = (e) => {
    if (this.state.isShowCalendar) {
      this.setState({ isShowCalendar: false });
    } else {
      this.setState({
        ...this.state,
        isShowCalendar: true
      });
    }
  };

  closeCalendar = (e) => {
    //창 닫힘 방지
    // if (/react-calendar/.test(e.target.className)) {
    //   return false;
    // }

    this.setState({
      ...this.state,
      isShowCalendar: false
    });
  };

  onChange = (date) => {
    this.setState({ ...this.state, date });
    this.props.onChange(date);
  }

  componentDidMount() {
    this.props.onChange(this.state.date);
  }

  render() {
    const { isShowCalendar, date } = this.state;

    let day = date.getDate();
    day = day >= 10 ? day : `0${day}`;
    let month = date.getMonth() + 1;
    month = month >= 10 ? month : `0${month}`;
    const year = date.getFullYear();
    const fullDate = `${year}-${month}-${day}`;

    return (
      <div className={`calendar ${isShowCalendar ? "open" : ""}`}>
        <button className={`fake__dropdown ${isShowCalendar ? "open" : ""}`} onClick={this.openCalendar}>
          {fullDate}
        </button>
        <Calendar
          value={date}
          onChange={this.onChange}
          locale="en-US"
          prevLabel={
            <div className="btn btn-icon btn-prev" />
          }
          nextLabel={
            <div  className="btn btn-icon btn-next" />

          }
        />
        <div className="calendar-dim" onClick={this.closeCalendar} />
      </div>
    );
  }
}
