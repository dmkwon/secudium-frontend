import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as auditLogActions from "../../store/modules/auditlog";
import AuditLogDetailPopup from "./popups/AuditLogDetailPopup";
// import ReactTooltip from 'react-tooltip'
import Loading from "./../Common/Loading";
import jwtService from "services/jwtService";

import Navbar from "../Common/Navbar";
import LeftMenu from "../Common/LeftMenu";
import GridTable from "../Common/GridTable";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import isAfter from "date-fns/is_after";
import IntlMessages from "util/IntlMessages";

import { getPagingParams } from "./../../utils";

import "./style.scss";

class AuditLogComponent extends Component {
  state = {
    isAuditDetailPopUp: false,
    auditHistSeq: "",
    no: "",
    auditUrl: "",
    httpTrnsType: "",
    auditText: "",
    auditResultCode: "",
    sdate: null,
    edate: null
  };

  resetDatePicker = () => {
    this.setState({
      sdate: null,
      edate: null
    });
  };

  closePopup = () => {
    this.setState({
      isAuditDetailPopUp: false
    });
  };

  getAuditLogs = pageable => {
    const { AuditLogActions } = this.props;
    let { sdate, edate } = this.state;
    let offset = 0;

    if (sdate) {
      offset = sdate.getTimezoneOffset() * 60 * 1000;
      sdate = new Date(sdate.getTime() - offset).toISOString().substring(0, 10);
    }
    if (edate) {
      offset = edate.getTimezoneOffset() * 60 * 1000;
      edate = new Date(edate.getTime() - offset).toISOString().substring(0, 10);
    }

    try {
      AuditLogActions.getAuditLogs(getPagingParams(pageable, { sdate, edate }));
      this.setState({ pageable });
    } catch (e) {
      console.log(e);
    }
  };

  onSearchHandler = e => {
    const { AuditLogActions } = this.props;
    let { sdate, edate } = this.state;
    let offset = 0;
    if (sdate) {
      offset = sdate.getTimezoneOffset() * 60 * 1000;
      sdate = new Date(sdate.getTime() - offset).toISOString().substring(0, 10);
    }
    if (edate) {
      offset = edate.getTimezoneOffset() * 60 * 1000;
      edate = new Date(edate.getTime() - offset).toISOString().substring(0, 10);
    }
    AuditLogActions.getAuditLogs(
      getPagingParams(this.state.pageable, { sdate, edate })
    );
  };

  handleChange = ({ sdate, edate }) => {
    sdate = sdate || this.state.sdate;
    edate = edate || this.state.edate;
    if (isAfter(sdate, edate)) {
      edate = sdate;
    }
    this.setState({ sdate, edate });
  };

  handleChangeStart = sdate => this.handleChange({ sdate });
  handleChangeEnd = edate => this.handleChange({ edate });

  componentDidMount() {
    jwtService.on("onChangeLang", () => {
      this.getAuditLogs(this.state.pageable);
    });
  }

  render() {
    const { isAuditDetailPopUp } = this.state;
    const { auditLogs, auditLogsBySeq } = this.props;
    const columns_auditLogs = [
      {
        Header: "No.",
        accessor: "no",
        style: { textAlign: "center" },
        width: 100
      },
      {
        Header: <IntlMessages id="func.name" />,
        accessor: "auditFuncNm",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="func.url" />,
        accessor: "auditUrl",
        style: { textAlign: "left" },
        filterable: true
      },
      {
        Header: <IntlMessages id="func.type" />,
        accessor: "funcTypeName",
        style: { textAlign: "left" },
        width: 120,
        filterable: true
      },
      {
        Header: <IntlMessages id="condition" />,
        accessor: "auditResultName",
        style: { textAlign: "left" },
        width: 200,
        filterable: true
      },
      {
        Header: <IntlMessages id="reg.user" />,
        accessor: "regUsrNm",
        style: { textAlign: "left" },
        width: 120,
        filterable: true
      },
      {
        Header: <IntlMessages id="reg.datetime" />,
        accessor: "stringRegDate",
        style: { textAlign: "center" },
        width: 200,
        filterable: true
      }
    ];
    return (
      <React.Fragment>
        <Navbar />
        <div className="wrapper auditLog">
          <LeftMenu />

          <div className="component component-list">
            <div className="component__title">
              <span>
                <IntlMessages id="audit.hist" />
              </span>
              <div className="search-bar">
                <label>
                  <IntlMessages id="reg.date" />
                </label>
                <DatePicker
                  className="form-control"
                  dateFormat="yyyy-MM-dd"
                  selected={this.state.sdate}
                  onChange={this.handleChangeStart}
                  startDate={this.state.sdate}
                  endDate={this.state.edate}
                  selectsStart
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                />
                <span style={{ margin: "0 8px" }}>-</span>
                <DatePicker
                  className="form-control"
                  dateFormat="yyyy-MM-dd"
                  selected={this.state.edate}
                  onChange={this.handleChangeEnd}
                  startDate={this.state.sdate}
                  endDate={this.state.edate}
                  selectsEnd
                  peekNextMonth
                  showMonthDropdown
                  showYearDropdown
                  dropdownMode="select"
                />
                <span style={{ margin: "0 2px" }} />
                <button className="btn" onClick={this.onSearchHandler}>
                  <IntlMessages id="search" />
                </button>
                <span style={{ margin: "0 2px" }} />
                <button
                  className="btn btn-icon btn-rollback"
                  onClick={this.resetDatePicker}
                >
                  <img
                    src="/images/common/icon_rollback.png"
                    alt="icon_rollback"
                  />
                </button>
              </div>
            </div>

            <GridTable
              manual
              data={auditLogs.content}
              pages={auditLogs.totalPages}
              totalPage={auditLogs.totalElements}
              onFetchData={this.getAuditLogs}
              columns={columns_auditLogs}
              className="-striped -highlight"
              resizable={false}
              filterable={false}
              getTrProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: async e => {
                    const { AuditLogActions } = this.props;
                    try {
                      await AuditLogActions.getAuditLogsBySeq(
                        rowInfo.original.auditHistSeq
                      );
                      this.setState({
                        updateData: {
                          auditHistSeq: rowInfo.original.auditHistSeq,
                          auditResultCode: rowInfo.original.auditResultCode,
                          auditUrl: rowInfo.original.auditUrl,
                          httpTrnsType: rowInfo.original.httpTrnsType,
                          auditText: rowInfo.original.auditText,
                          regUsrNm: rowInfo.original.regUsrNm,
                          stringRegDate: rowInfo.original.stringRegDate
                        },
                        isAuditDetailPopUp: true
                      });
                    } catch (e) {
                      console.log(e);
                    }
                  }
                };
              }}
            />
          </div>
          {isAuditDetailPopUp && (
            <AuditLogDetailPopup
              data={auditLogsBySeq}
              closePopup={this.closePopup}
            />
          )}
        </div>
        {this.props.loading && <Loading />}
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    auditLogs: state.auditlog.get("auditLogs"),
    auditLogsBySeq: state.auditlog.get("auditLogsBySeq"),
    loading: state.auditlog.get("loading")
  }),

  dispatch => ({
    AuditLogActions: bindActionCreators(auditLogActions, dispatch)
  })
)(AuditLogComponent);
