import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Textbox, Textarea } from "react-inputs-validation";
import Draggable from "react-draggable";
import IntlMessages from "util/IntlMessages";

import "./../style.scss";
import * as auditLogActions from "./../../../store/modules/auditlog";

class AuditLogDetailPopup extends Component {
  state = {
    auditLogsBySeq: this.props.data,
    position: {
      x: 0,
      y: 0
    }
  };

  componentDidMount = () => {
    const elWidth = document.getElementById("viewAuditHistPopup").offsetWidth;
    const elHeight = document.getElementById("viewAuditHistPopup").offsetHeight;
    this.setState({
      position: {
        x: Math.floor(elWidth / 2),
        y: Math.floor((elHeight / 2) * -1)
      }
    });
  };

  handleDrag = (e, ui) => {
    const { x, y } = this.state.position;
    this.setState({
      position: {
        x: x + ui.deltaX,
        y: y + ui.deltaY
      }
    });
  };
  render() {
    const { closePopup } = this.props;
    const { auditLogsBySeq, position } = this.state;

    return (
      <React.Fragment>
        <Draggable
          handle=".popup__header"
          defaultPosition={{ x: 0, y: 0 }}
          position={position}
          onDrag={this.handleDrag}
        >
          <div
            className="popup popup--move"
            id="viewAuditHistPopup"
            style={{ width: "800px" }}
          >
            <div className="popup__header">
              <h5>
                <IntlMessages id="audit.log" />
              </h5>
              <button className="btn btn-close" onClick={closePopup} />
            </div>
            <div className="popup__body">
              <table className="table table--info">
                <tbody>
                  <tr>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="reg.user" />
                    </th>
                    <td>
                      <Textbox
                        id="regUsrNm"
                        name="regUsrNm"
                        type="text"
                        value={auditLogsBySeq.regUsrNm}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="reg.datetime" />
                    </th>
                    <td>
                      <Textbox
                        id="stringRegDate"
                        name="stringRegDate"
                        type="text"
                        value={auditLogsBySeq.stringRegDate}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="func" />
                    </th>
                    <td>
                      <Textbox
                        id="auditFuncNm"
                        name="auditFuncNm"
                        type="text"
                        value={auditLogsBySeq.auditFuncNm}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="url" />
                    </th>
                    <td>
                      <Textbox
                        id="auditUrl"
                        name="auditUrl"
                        type="text"
                        value={auditLogsBySeq.auditUrl}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="func.type" />
                    </th>
                    <td>
                      <Textbox
                        id="funcTypeName"
                        name="funcTypeName"
                        type="text"
                        value={auditLogsBySeq.funcTypeName}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="http.trans.method" />
                    </th>
                    <td>
                      <Textbox
                        id="httpTrnsType"
                        name="httpTrnsType"
                        type="text"
                        value={auditLogsBySeq.httpTrnsType}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="result.code" />
                    </th>
                    <td>
                      <Textbox
                        id="auditResultCode"
                        name="auditResultCode"
                        type="text"
                        value={auditLogsBySeq.auditResultCode}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="condition" />
                    </th>
                    <td>
                      <Textbox
                        id="auditResultName"
                        name="auditResultName"
                        type="text"
                        value={auditLogsBySeq.auditResultName}
                        disabled={true}
                        customStyleInput={{ width: "260px", resize: "none" }}
                      />
                    </td>
                  </tr>
                  <tr>
                    <th style={{ width: "100px" }}>
                      <IntlMessages id="detail" />
                    </th>
                    <td colSpan="3">
                      <Textarea
                        id="auditText"
                        name="auditText"
                        type="text"
                        value={auditLogsBySeq.auditText}
                        disabled={true}
                        customStyleInput={{
                          width: "100%",
                          resize: "none",
                          height: "200px"
                        }}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="popup__footer">
              <button className="btn btn--dark" onClick={closePopup}>
                <IntlMessages id="confirm" />
              </button>
            </div>
          </div>
        </Draggable>
        <div className="dim" onClick={closePopup} />
      </React.Fragment>
    );
  }
}

export default connect(
  state => ({
    auditLogs: state.auditlog.get("auditLogs")
  }),
  dispatch => ({
    AuditLogActions: bindActionCreators(auditLogActions, dispatch)
  })
)(AuditLogDetailPopup);
