//유효값 검사(undefined, null, 공백값 체크)
export const isValid = value =>
  typeof value !== "undefined" && value !== null && value !== "";

/**
 * 페이징, 정렬, 필터링 처리를 위한 params for axios
 * @param {*} react-table onFetchData호출 시 전달받는 state
 * @param {*} externalParams 추가적으로 요청할 데이터
 */
export const getPagingParams = (
  { page, pageSize, sorted, filtered },
  externalParams
) => {
  const params = {
    page,
    size: pageSize,
    ...externalParams
  };

  //sorting parameter
  let sorts = [];
  for (let i = 0, length = sorted.length; i < length; i++) {
    const sort = sorted[i];
    sorts.push(`${sort.id},${sort.desc ? "desc" : "asc"}`);
  }
  params["sort"] = sorts;

  //filtering parameter
  for (let i = 0, length = filtered.length; i < length; i++) {
    const filter = filtered[i];
    params[filter.id] = filter.value;
  }

  return params;
};

/** IP유효성 검사 */
export const isIp = ip =>
  /^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$/.test(ip);
/** Port유효성 검사 */
export const isPort = port => {
  if (isNaN(port)) {
    return false;
  }
  const toInt = Number.parseInt(port, 10);
  if (0 <= toInt && toInt <= 65535) {
    return true;
  } else {
    return false;
  }
};
/** 리눅스경로유효성 검사 */
export const isLinuxPath = path => /^(\/[^\\/:*?"<>| ]*)+\/?$/.test(path);
