export const host = window.host;

export const defaultUserInfo =
  process.env.NODE_ENV === "development"
    ? {
        id: "isap_admin",
        passwd: "Secudium!@#"
      }
    : {
        id: "",
        passwd: ""
      };

export const gridGlobalSet = {
  defaultPageSize: 10, //기본 페이지 사이즈
  minRows: 10, //최소 행 갯수
  pageSizeOptions: [5, 10, 20], //페이지사이즈 옵션
  defaultFilterMethod: (filter, row) => {
    //커스텀 필터링=> like search
    const { id, value } = filter;
    let target = row[id].toString();
    return target.match(new RegExp(`.*${value}.*`));
  }
};
