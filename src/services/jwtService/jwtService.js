import axios from "axios";
import { host } from "../../constants";

import GlobalUtils from "../../util/GlobalUtils";

class jwtService extends GlobalUtils.EventEmitter {
  init() {
    axios.defaults.withCredentials = true;
    this.setInterceptors();
    this.handleAuthentication();
  }

  setInterceptors = () => {
    axios.interceptors.response.use(
      response => {
        return response;
      },
      err => {
        return new Promise((resolve, reject) => {
          if (
            err.message === "Network Error" ||
            err.response.status === 403 ||
            (err.response.status === 401 &&
              err.config &&
              !err.config.__isRetryRequest)
          ) {
            // if you ever get an unauthorized response, logout the user
            this.setCurrentUser(null);
            this.emit("onAutoLogout");
          }
          throw err;
        });
      }
    );
  };

  handleAuthentication = () => {
    if (this.isLoggined()) {
      this.getCurrentUser()
        .then(res => {
          this.emit("onAutoLogin", true);
        })
        .catch(err => {
          //사용자 정보 가져오기 실패
          this.setCurrentUser(null);
          this.emit("onAutoLogout");
        });
    } else {
      this.setCurrentUser(null);
      this.emit("onAutoLogout");
    }
  };

  signInWithUserNameAndPassword = (username, password) => {
    return new Promise((resolve, reject) => {
      let form = new FormData();
      form.append("username", username);
      form.append("password", password);

      axios
        .post(`${host}/api/auth/signin`, {
          username,
          password
        })
        .then(response => {
          if (!response.error) {
            resolve();
          } else {
            reject(response.error);
          }
        })
        .catch(err => {
          console.log(err);
          reject(err);
        });
    });
  };

  getCurrentUser = () => {
    return new Promise((resolve, reject) => {
      axios
        .get(`${host}/api/auth/me`, { withCredentials: true })
        .then(response => {
          if (response.data) {
            this.setCurrentUser(response.data);
            resolve();
          } else {
            reject(response.error);
          }
        });
    });
  };

  isLoggined = () => {
    const usrNm = localStorage.getItem("USR_NM");
    if (usrNm === null) {
      return false;
    } else {
      return true;
    }
  };

  setCurrentUser = user => {
    if (user) {
      localStorage.setItem("USR_NM", user.usrNm);
      localStorage.setItem("USR_EMAIL", user.usrEmail);
      localStorage.setItem("USR_MOBILE", user.usrMobile);
      localStorage.setItem("LAST_LOGIN_DATE", user.lastLoginDate);
      localStorage.setItem("AUTHORITIES", JSON.stringify(user.authorities));
      localStorage.setItem("LANG", user.lang);
    } else {
      localStorage.removeItem("USR_NM");
      localStorage.removeItem("USR_EMAIL");
      localStorage.removeItem("USR_MOBILE");
      localStorage.removeItem("LAST_LOGIN_DATE");
      localStorage.removeItem("AUTHORITIES");
      localStorage.removeItem("LANG");
    }
  };

  logout = () => {
    return new Promise((resolve, reject) => {
      axios
        .get(`${host}/api/auth/signout`, { withCredentials: true })
        .then(response => {
          if (response.status === 200) {
            this.setCurrentUser(null);
            this.emit("onAutoLogout");
          } else {
            reject(response.error);
          }
        });
    });
  };
}

const instance = new jwtService();

export default instance;
