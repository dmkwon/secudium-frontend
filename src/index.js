import 'core-js';
import React from 'react';
import ReactDOM from 'react-dom';
import 'react-app-polyfill/ie11';

import AppContainer from './AppContainer';
import * as serviceWorker from './serviceWorker';

// datepicker 불러올때 css 필수 포함
import "react-datepicker/dist/react-datepicker.css";

ReactDOM.render(<AppContainer />, document.getElementById('root'));

serviceWorker.unregister();
