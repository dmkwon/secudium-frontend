import React, { Component } from "react";
import { Provider } from 'react-redux';

import App from "./App";
import configure from "./store/configure";


export const store = configure();

class AppContainer extends Component {
    render() {
        return (
            <Provider store={store}>
                <App />
            </Provider>
        );
    }
}

export default AppContainer;