import React, { Component } from "react";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { IntlProvider } from "react-intl";
import AppLocale from "./lngProvider";
import { Auth } from "./auth";

import * as commonActions from "./store/modules/common";

import LoginContainer from "./containers/LoginContainer";
import NmlzRuleContainer from "./containers/NmlzRuleContainer";
import EngineEnvContainer from "./containers/EngineEnvContainer";
import EngineOptionContainer from "./containers/EngineOptionContainer";
import BlackccContainer from "./containers/BlackccContainer";
import FilterMgmtContainer from "./containers/FilterMgmtContainer";
import FieldMgmtContainer from "./containers/FieldMgmtContainer";
import ThreatTaxonomySystemContainer from "./containers/ThreatTaxonomySystem";
import RuleMgmtContainer from "./containers/RuleMgmtContainer";
import RuleDistHistContainer from "./containers/RuleDistHistContainer";
import history from "./history";

// 시스템 운영
import RoleMgmtContainer from "./containers/RoleMgmtContainer";
import MenuMgmtContainer from "./containers/MenuMgmtContainer";
import ProgMgmtContainer from "./containers/ProgMgmtContainer";
import UserMgmtContainer from "./containers/UserMgmtContainer";
import AuditLogContainer from "./containers/AuditLogContainer";
import CodeMgmtContainer from "./containers/CodeMgmtContainer";

import "./App.scss";
import "./validation.css";

class App extends Component {
  render() {
    const { language } = this.props;
    const currentAppLocale = AppLocale[language];

    return (
      <IntlProvider
        locale={currentAppLocale.locale}
        messages={currentAppLocale.messages}
      >
        <Auth>
          <Router history={history}>
            <Switch>
              <Route path="/login" component={LoginContainer} />

              <Redirect exact from="/" to="/em/enginenv" />
              <Redirect exact from="/em" to="/em/enginenv" />
              {/* 엔진환경 */}
              <Route path="/em/enginenv" component={EngineEnvContainer} />
              {/* 엔진옵션 */}
              <Route path="/em/option" component={EngineOptionContainer} />
              {/* 필드관리 */}
              <Route path="/em/field" component={FieldMgmtContainer} />

              <Redirect exact from="/rm" to="/rm/nmlzrule" />
              {/* 정규화룰 */}
              <Route path="/rm/nmlzrule" component={NmlzRuleContainer} />
              {/* 룰관리 */}
              <Route path="/rm/rule" component={RuleMgmtContainer} />
              {/* 룰배포이력 */}
              <Route path="/rm/ruledist" component={RuleDistHistContainer} />
              {/* 필터관리 */}
              <Route path="/rm/filter" component={FilterMgmtContainer} />
              {/* 위협분류체계 */}
              <Route
                path="/rm/taxonomy"
                component={ThreatTaxonomySystemContainer}
              />
              {/* 블랙CC */}
              <Route path="/rm/blackcc" component={BlackccContainer} />

              <Redirect exact from="/so" to="/so/role" />
              <Route path="/so/role" component={RoleMgmtContainer} />
              <Route path="/so/user" component={UserMgmtContainer} />
              <Route path="/so/menu" component={MenuMgmtContainer} />
              <Route path="/so/prog" component={ProgMgmtContainer} />
              <Route path="/so/code" component={CodeMgmtContainer} />
              <Route path="/so/auditlog" component={AuditLogContainer} />

              {/* 라우팅 외의 url 처리 */}
              <Redirect from="*" to="/login" />
            </Switch>
          </Router>
        </Auth>
      </IntlProvider>
    );
  }
}

export default connect(
  state => ({
    language: state.common.get("language")
  }),
  dispatch => ({
    CommonActions: bindActionCreators(commonActions, dispatch)
  })
)(App);
