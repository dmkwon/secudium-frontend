import koLang from "./entries/ko_KR";
import enLang from "./entries/en_US";
import {addLocaleData} from "react-intl";

const AppLocale = {
  ko: koLang,
  en: enLang
};
addLocaleData(AppLocale.ko.data);
addLocaleData(AppLocale.en.data);

export default AppLocale;
