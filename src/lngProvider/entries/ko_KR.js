import appLocaleData from "react-intl/locale-data/ko";
import saMessages from "../locales/ko_KR.json";

const KoLang = {
  messages: {
    ...saMessages
  },
  locale: 'ko',
  data: appLocaleData
};
export default KoLang;
