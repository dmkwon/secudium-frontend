import React, { Component } from "react";
import jwtService from "services/jwtService";

import history from "../history";

class Auth extends Component {
  /*eslint-disable-next-line no-useless-constructor*/
  constructor(props) {
    super(props);

    /**
     * Comment the line if you do not use JWt
     */
    this.jwtCheck();
  }

  jwtCheck = () => {
    jwtService.on("onAutoLogin", () => {});

    jwtService.on("onAutoLogout", () => {
      history.push("/login");
    });

    jwtService.init();
  };

  render() {
    const { children } = this.props;

    return <React.Fragment>{children}</React.Fragment>;
  }
}

export default Auth;
