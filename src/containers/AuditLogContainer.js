import React from 'react'
import { connect } from 'react-redux';

import CommonAlertPopup from "./../components/Common/Popups/CommonAlertPopup";
import AuditLogComponent from "../components/AuditLog"

const AuditLogContainer = ({ match, commonAlertPopup, commonAlertCode }) => {
  return (
    <React.Fragment>
      <AuditLogComponent match={match} />
      {commonAlertPopup && <CommonAlertPopup code={commonAlertCode} />}
    </React.Fragment>
  )
}

export default connect(
  state => ({
    commonAlertPopup: state.common.get("commonAlertPopup"),
    commonAlertCode: state.common.get("commonAlertCode"),
  }),
  dispatch => ({
  })
)(AuditLogContainer);

