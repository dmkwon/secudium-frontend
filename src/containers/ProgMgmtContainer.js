import React from 'react'
import { connect } from 'react-redux';

import CommonAlertPopup from "./../components/Common/Popups/CommonAlertPopup";
import ProgComponent from "../components/Prog"

const ProgContainer = ({ match, commonAlertPopup, commonAlertCode }) => {
  return (
    <React.Fragment>
      <ProgComponent match={match} />
      {commonAlertPopup && <CommonAlertPopup code={commonAlertCode} />}
    </React.Fragment>
  )
}

export default connect(
  state => ({
    commonAlertPopup: state.common.get("commonAlertPopup"),
    commonAlertCode: state.common.get("commonAlertCode"),
  }),
  dispatch => ({
  })
)(ProgContainer);
