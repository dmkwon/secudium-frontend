import React from 'react'
import { connect } from 'react-redux';

import CommonAlertPopup from "./../components/Common/Popups/CommonAlertPopup";
import RuleDistHistComponent from "../components/RuleDist"

const RuleDistHistContainer = ({ commonAlertPopup, commonAlertCode }) => {
  return (
    <React.Fragment>
      <RuleDistHistComponent />
      {commonAlertPopup && <CommonAlertPopup code={commonAlertCode} />}
    </React.Fragment>
  )
}

export default connect(
  state => ({
    commonAlertPopup: state.common.get("commonAlertPopup"),
    commonAlertCode: state.common.get("commonAlertCode"),
  }),
  dispatch => ({
  })
)(RuleDistHistContainer);
