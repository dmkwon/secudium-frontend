import React from 'react'
import { connect } from 'react-redux';

import CommonAlertPopup from "./../components/Common/Popups/CommonAlertPopup";
import FilterMgmtComponent from "../components/FilterMgmt"

const FilterMgmtContainer = ({ match, commonAlertPopup, commonAlertCode }) => {
  return (
    <React.Fragment>
      <FilterMgmtComponent match={match} />
      {commonAlertPopup && <CommonAlertPopup code={commonAlertCode} />}
    </React.Fragment>
  )
}

export default connect(
  state => ({
    commonAlertPopup: state.common.get("commonAlertPopup"),
    commonAlertCode: state.common.get("commonAlertCode"),
  }),
  dispatch => ({
  })
)(FilterMgmtContainer);