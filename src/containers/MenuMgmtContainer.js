import React, { Component } from 'react'
import { connect } from 'react-redux';

import CommonAlertPopup from "./../components/Common/Popups/CommonAlertPopup";
import MenuComponent from "../components/Menu"

class MenuMgmTContainer extends Component {
  render() {
    return (
      <React.Fragment>
        <MenuComponent />
        {this.props.commonAlertPopup && <CommonAlertPopup code={this.props.commonAlertCode} />}
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    commonAlertPopup: state.common.get("commonAlertPopup"),
    commonAlertCode: state.common.get("commonAlertCode"),
  }),
  dispatch => ({
  })
)(MenuMgmTContainer);
