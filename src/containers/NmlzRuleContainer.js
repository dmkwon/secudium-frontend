import React from 'react';
import { connect } from 'react-redux';

import CommonAlertPopup from '../components/Common/Popups/CommonAlertPopup';
import NmlzRuleComponent from '../components/NmlzRule';

const NmlzRuleContainer = ({ match, commonAlertPopup, commonAlertCode }) => {
  return (
    <React.Fragment>
      <NmlzRuleComponent match={match} />
      {commonAlertPopup && <CommonAlertPopup code={commonAlertCode} />}
    </React.Fragment>
  );
};

export default connect(
  state => ({
    commonAlertPopup: state.common.get('commonAlertPopup'),
    commonAlertCode: state.common.get('commonAlertCode'),
  }),
  dispatch => ({})
)(NmlzRuleContainer);
