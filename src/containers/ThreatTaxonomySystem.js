import React, { Component } from 'react';
import { connect } from 'react-redux';

import CommonAlertPopup from "./../components/Common/Popups/CommonAlertPopup";
import ThreatTaxonomySystemComponent from "../components/ThreatTaxonomySystem";

class ThreatTaxonomySystemContainer extends Component {
  render() {
    return (
      <React.Fragment>
        <ThreatTaxonomySystemComponent />
        {this.props.commonAlertPopup && <CommonAlertPopup code={this.props.commonAlertCode} />}
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    commonAlertPopup: state.common.get("commonAlertPopup"),
    commonAlertCode: state.common.get("commonAlertCode"),
  }),
  dispatch => ({
  })
)(ThreatTaxonomySystemContainer);