import React, { Component } from 'react'
import { connect } from 'react-redux';

import CommonAlertPopup from "./../components/Common/Popups/CommonAlertPopup";
import FieldComponentComponent from '../components/Field'

class FieldComponentContainer extends Component {
  render() {
    return (
      <React.Fragment>
        <FieldComponentComponent />
        {this.props.commonAlertPopup && <CommonAlertPopup code={this.props.commonAlertCode} />}
      </React.Fragment>
    )
  }
}

export default connect(
  state => ({
    commonAlertPopup: state.common.get("commonAlertPopup"),
    commonAlertCode: state.common.get("commonAlertCode"),
  }),
  dispatch => ({
  })
)(FieldComponentContainer);
