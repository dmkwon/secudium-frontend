import { applyMiddleware, compose, createStore } from "redux";
import penderMiddleware from "redux-pender";
import reducers from "./modules/index";

// const middlewares = [penderMiddleware(), loggerMiddleware()];
const middlewares = [penderMiddleware()];

const isDev = true;
const devTools = isDev && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const composeEnhancers = devTools || compose;

const configure = preloadedState =>
  createStore(reducers, composeEnhancers(applyMiddleware(...middlewares)));

export default configure;
