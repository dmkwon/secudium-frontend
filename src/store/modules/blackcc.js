import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';

import * as api from '../api/blackcc';
import { pender } from 'redux-pender/lib/utils';

//action type
const GET_IP_LIST = 'ip/GET_IP_LIST',
  // GET_IP = 'ip/GET_IP',  //미사용
  POST_IP = 'ip/POST_IP',
  PUT_IP = 'ip/PUT_IP',
  DELETE_IP = 'ip/DELETE_IP',
  GET_IP_DUPL = 'ip/GET_IP_DUPL',
  GET_IP_DUPL_UPDATE = 'ip/GET_IP_DUPL_UPDATE',
  GET_IPS_DUPL = 'ip/GET_IPS_DUPL',
  GET_IPS_DUPL_UPDATE = 'ip/GET_IPS_DUPL_UPDATE',
  GET_URL_LIST = 'url/GET_URL_LIST',
  // GET_URL = 'url/GET_URL',  // 미사용
  POST_URL = 'url/POST_URL',
  PUT_URL = 'url/PUT_URL',
  DELETE_URL = 'url/DELETE_URL',
  DEPLOY_ALL = 'blackcc/DEPLOY_ALL';

//action creator(action)
export const getIpList = createAction(GET_IP_LIST, api.getIpList);
// export const getIp = createAction(GET_IP, api.getIp);  // 미사용
export const postIp = createAction(POST_IP, api.postIp);
export const putIp = createAction(PUT_IP, api.putIp);

export const deleteIps = createAction(DELETE_IP, api.deleteIps);
export const getIpsDupl = createAction(GET_IPS_DUPL, api.getIpsDupl);
export const getIpsDuplUpdate = createAction(
  GET_IPS_DUPL_UPDATE,
  api.getIpsDuplUpdate
);
export const getIpDupl = createAction(GET_IP_DUPL, api.getIpDupl);
export const getIpDuplUpdate = createAction(GET_IP_DUPL, api.getIpDuplUpdate);

export const getUrlList = createAction(GET_URL_LIST, api.getUrlList);
// export const getUrl = createAction(GET_URL, api.getUrl); // 미사용
export const postUrl = createAction(POST_URL, api.postUrl);
export const putUrl = createAction(PUT_URL, api.putUrl);

export const deleteUrls = createAction(DELETE_URL, api.deleteUrls);
export const deployAll = createAction(DEPLOY_ALL, api.deployAll);
//inital state
const initialState = Map({
  ipList: {
    content: [],
    page: 0,
    size: 0,
    totalElements: 0,
    totalPages: 0,
    last: false,
  },
  ipInfo: {
    regUsrNo: null,
    modUsrNo: null,
    regUsrNm: '',
    modUsrNm: '',
    regDate: null,
    modDate: null,
    ipListSeq: 0,
    ipDstnct: '',
    stIp: 0,
    edIp: 0,
    country: '',
    detectDate: null,
    expDate: null,
    attkType: '',
    ipDesc: '',
    useYn: '',
    regDateFormatted: '',
    modDateFormatted: '',
  },
  urlList: {
    content: [],
    page: 0,
    size: 0,
    totalElements: 0,
    totalPages: 0,
    last: false,
  },
  urlInfo: {
    regUsrNo: null,
    modUsrNo: null,
    regUsrNm: '',
    modUsrNm: '',
    regDate: null,
    modDate: null,
    stringModDate: '',
    stringRegDate: '',
    urlListSeq: 0,
    urlDstnct: '',
    url: '',
    detectDate: null,
    expDate: null,
    attkType: '',
    urlDesc: '',
    useYn: '',
    regDateFormatted: '',
    modDateFormatted: '',
  },
  isSuccess: 0,
  loading: false,
});

///action handler(reducer)
const reducer = handleActions(
  {
    ...pender({
      type: GET_IP_LIST,
      onSuccess: (state, action) =>
        state.set('ipList', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    // 미사용
    // ...pender({
    //   type: GET_IP,
    //   onSuccess: (state, action) =>
    //     state.set('ipInfo', action.payload.data).set('loading', false),
    //   onFailure: (state, action) => state.set('loading', false),
    //   onPending: (state, action) => state.set('loading', true),
    // }),
    ...pender({
      type: POST_IP,
      onSuccess: (state, action) =>
        state.set('isSuccess', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: PUT_IP,
      onSuccess: (state, action) =>
        state.set('isSuccess', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: DELETE_IP,
      onSuccess: (state, action) =>
        state.set('isSuccess', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: GET_IPS_DUPL,
    }),
    ...pender({
      type: GET_IPS_DUPL_UPDATE,
    }),
    ...pender({
      type: GET_IP_DUPL,
    }),
    ...pender({
      type: GET_IP_DUPL_UPDATE,
    }),
    ...pender({
      type: GET_URL_LIST,
      onSuccess: (state, action) =>
        state.set('urlList', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    // 미사용
    // ...pender({
    //   type: GET_URL,
    //   onSuccess: (state, action) =>
    //     state.set('urlInfo', action.payload.data).set('loading', false),
    //   onFailure: (state, action) => state.set('loading', false),
    //   onPending: (state, action) => state.set('loading', true),
    // }),
    ...pender({
      type: POST_URL,
      onSuccess: (state, action) =>
        state.set('isSuccess', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: PUT_URL,
      onSuccess: (state, action) =>
        state.set('isSuccess', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: DELETE_URL,
      onSuccess: (state, action) =>
        state.set('isSuccess', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: DEPLOY_ALL,
      onSuccess: (state, action) =>
        state.set('isSuccess', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
  },
  initialState
);

export default reducer;
