import { createAction, handleActions } from "redux-actions";
import { pender } from "redux-pender";
import { /*fromJS,*/ Map } from "immutable";

import * as api from "../api/filter";

import { isDebug } from "../../components/FilterMgmt/const/filterConfig";
import {
  isFilter,
  convertListToTree
} from "../../components/FilterMgmt/const/utils";

const ROOT_GROUP = {
  filterGroupSeq: 0,
  topFilterGroupSeq: null,
  status: null,
  name: "전체",
  depth: 0,
  subGroup: []
};

// action type 정의
const actionType = {
  GET_INIT_DATA: "filterMgmtList/GET_INIT_DATA", // 필터, 필터 그룹 데이터
  GET_SUB_GROUP_DATA: "filterMgmtList/GET_SUB_GROUP_DATA",

  ADD_FILTERGROUP: "filterMgmtList/ADD_FILTERGROUP",
  EDIT_FILTERGROUP: "filterMgmtList/EDIT_FILTERGROUP",

  EDIT_FILTERS: "filterMgmtList/EDIT_FILTERS",
  EDIT_FILTER: "filterMgmtList/EDIT_FILTER",
  ADD_FILTER: "filterMgmtList/ADD_FILTER"
};

// action 정의
const action = {
  getInitData: createAction(
    actionType.GET_INIT_DATA,
    api.getInitFilterListData
  ), // 그룹과 필터 정보
  getSubGroupData: createAction(
    actionType.GET_SUB_GROUP_DATA,
    api.getFilterSubGroupBySeq
  ), // 그룹의 하위 정보만

  addFilterGroup: createAction(actionType.ADD_FILTERGROUP, api.addFilterGroup),
  editFilterGroup: createAction(
    actionType.EDIT_FILTERGROUP,
    api.updateFilterGroup
  ),

  editFilters: createAction(actionType.EDIT_FILTERS, api.updateFilterList),
  editFilter: createAction(actionType.EDIT_FILTER, api.updateFilter),
  addFilter: createAction(actionType.ADD_FILTER, api.addFilter)
};

// 상태 초기값
const initialState = Map({
  filterGroup: undefined,
  filterGroupList: undefined, // 백업용
  filterList: [],
  saveInfo: {}
});

const convertFGroupListToFGTree = fgList => {
  let fgTree = ROOT_GROUP;

  return convertListToTree(fgList, fgTree, (item, parent = null) => {
    item.filterGroupNmPath =
      item.depth === 1
        ? ROOT_GROUP.name
        : `${parent.filterGroupNmPath} > ${parent.name}`;

    return item;
  });
};

// 그리드에서 사용할 data는 (필터 그룹 + 필터 정보)를 하나로 만든 리스트 형태
// 이 형태로 데이터 새로 구성함
const changeMakeData = (isIncludeFGInfo, fgrouplist, flist) => {
  let chData;
  let fgrouplistWithoutDeleteType = fgrouplist.filter(fg => fg.status !== "D");
  isIncludeFGInfo
    ? (chData = [...fgrouplistWithoutDeleteType, ...flist])
    : (chData = [...flist]);

  chData.map((item, i) => {
    item.no = i + 1; // 그룹을 추가하거나 다른 데이터와 합성하는 경우를 위해 그대로 됨(단일은 sql문에서)
    if (isFilter(item)) {
      for (let fg of fgrouplist) {
        if (fg.filterGroupSeq === item.filterGroupSeq) {
          fg.filterCnt = fg.filterCnt ? fg.filterCnt + 1 : 1;
          item.filterGroupNmPath =
            fg.filterGroupNmPath.length > 0
              ? `${fg.filterGroupNmPath} > ${fg.name}`
              : fg.name;
        }
      }
    }
    return null;
  });
  isDebug && console.log(chData);
  return chData;
};

const changeAndSetData = (isIncludeGroupData, fg, fs) => {
  // 그리드에서 표시할 수 있는 형태로 변환
  let groupData = convertFGroupListToFGTree(fg);
  let dataList = changeMakeData(isIncludeGroupData, groupData.list, fs);

  return { groupData, dataList };
};

// reducer 정의하기
export default handleActions(
  {
    ...pender({
      type: actionType.ADD_FILTERGROUP,
      onSuccess: (state, action) => {
        isDebug && console.log("onSuccess-ADD_FILTERGROUP: ", state, action);

        let groupData = convertFGroupListToFGTree(action.payload[0].data);
        isDebug &&
          console.log("onSuccess-ADD_FILTERGROUP: ", state, action, groupData);
        return state.set("filterGroup", null); //set("filterList", re.dataList)
      }
    }),
    ...pender({
      type: actionType.EDIT_FILTERGROUP,
      onSuccess: (state, action) => {
        isDebug && console.log("onSuccess-EDIT_FILTERGROUP: ", state, action);

        let re = changeAndSetData(
          false,
          action.payload[1].data,
          action.payload[2].data
        );
        isDebug &&
          console.log("onSuccess-EDIT_FILTERGROUP: ", state, action, re);
        return state
          .set("filterGroup", re.groupData.tree)
          .set("filterList", re.dataList);
      }
    }),
    ...pender({
      type: actionType.GET_SUB_GROUP_DATA,
      onSuccess: (state, action) => {
        let list = changeMakeData(
          false,
          state.get("filterGroupList"),
          action.payload.data.filterList
        );
        isDebug &&
          console.log("onSuccess-GET_SUB_GROUP_DATA: ", state, action, list);

        return state.set("filterList", list); //.set("filterGroup", re.groupData.tree)
      }
    }),
    ...pender({
      type: actionType.GET_INIT_DATA,
      onSuccess: (state, action) => {
        let re = changeAndSetData(
          false,
          action.payload[0].data,
          action.payload[1].data
        );
        isDebug && console.log("onSuccess-GET_INIT_DATA: ", state, action, re);

        return state
          .set("filterGroupList", re.groupData.list)
          .set("filterGroup", re.groupData.tree)
          .set("filterList", re.dataList);
      }
    }),
    ...pender({
      type: actionType.EDIT_FILTERS,
      onSuccess: (state, action) => {
        isDebug && console.log("actionType.EDIT_FILTERS: ", state, action);
        return state.set("saveInfo", action.payload.data);
      }
    }),
    ...pender({
      type: actionType.EDIT_FILTER,
      onSuccess: (state, action) => {
        isDebug && console.log("actionType.EDIT_FILTER: ", state, action);
        return state.set("saveInfo", action.payload.data);
      }
    }),
    ...pender({
      type: actionType.ADD_FILTER,
      onSuccess: (state, action) => {
        isDebug && console.log("actionType.ADD_FILTER: ", state, action);
        return state.set("saveInfo", action.payload.data);
      }
    })
  },
  initialState
);

export { action };
