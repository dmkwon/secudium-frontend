import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { pender } from 'redux-pender';

import * as api from './../api/field';

const GET_FIELDHISTS = 'field/GET_FIELDHISTS';
const INSERT_FIELDHIST = 'field/INSERT_FIELDHIST';
const UPDATE_FIELDHIST = 'field/UPDATE_FIELDHIST';
const DELETE_FIELDHIST = 'field/DELETE_FIELDHIST';

const GET_FIELDS = 'field/GET_FIELDS';
const GET_PREV_FIELDS = 'field/GET_PREV_FIELDS';

const ROLLBACK = 'field/ROLLBACK';

export const getFieldHists = createAction(GET_FIELDHISTS, api.getFieldHists);
export const insertFieldHist = createAction(
  INSERT_FIELDHIST,
  api.insertFieldHist
);
export const updateFieldHist = createAction(
  UPDATE_FIELDHIST,
  api.updateFieldHist
);
export const deleteFieldHist = createAction(
  DELETE_FIELDHIST,
  api.deleteFieldHist
);

export const getFields = createAction(GET_FIELDS, api.getFields);
export const getPrevFields = createAction(GET_PREV_FIELDS, api.getPrevFields);

export const rollback = createAction(ROLLBACK, api.rollback);

const initialState = Map({
  fieldHists: [],
  fields: [],
  loading: false,
});

export default handleActions(
  {
    ...pender({
      type: GET_FIELDHISTS,
      onSuccess: (state, action) =>
        state.set('fieldHists', action.payload.data).set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_FIELDS,
      onSuccess: (state, action) =>
        state.set('fields', action.payload.data).set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_PREV_FIELDS,
      onSuccess: (state, action) =>
        state.set('prevFields', action.payload.data).set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: INSERT_FIELDHIST,
      onSuccess: state => state.set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: UPDATE_FIELDHIST,
      onSuccess: state => state.set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: DELETE_FIELDHIST,
      onSuccess: state => state.set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: ROLLBACK,
      onSuccess: state => state.set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
  },
  initialState
);
