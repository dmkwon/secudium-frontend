import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { pender } from 'redux-pender';

import * as api from '../api/filterPopup';

const GET_FILTER_FIELDS = 'field/GET_FILTER_FIELDS';
const GET_FILTER_FIELD_TREES = 'field/GET_FILTER_FIELD_TREES';
const GET_AGENTS = 'agent/GET_AGENTS';
const GET_AGENT_VENDORS = 'agentVendor/GET_AGENT_VENDORS';
const GET_AGENT_MODELS = 'agentModel/GET_AGENT_MODELS';
const GET_AGENT_MODEL_TREES = 'agentModel/GET_AGENT_MODEL_TREES';
const GET_COMPANYS = 'company/GET_COMPANYS';
const GET_COMPANY_TREES = 'company/GET_COMPANY_TREES';

export const getFilterFields = createAction(
  GET_FILTER_FIELDS,
  api.getFilterFields
);
export const getFilterFieldTrees = createAction(
  GET_FILTER_FIELD_TREES,
  api.getFilterFieldTrees
);
export const getAgents = createAction(GET_AGENTS, api.getAgents);
export const getAgentVendors = createAction(
  GET_AGENT_VENDORS,
  api.getAgentVendors
);
export const getAgentModels = createAction(
  GET_AGENT_MODELS,
  api.getAgentModels
);
export const getAgentModelTrees = createAction(
  GET_AGENT_MODEL_TREES,
  api.getAgentModelTrees
);
export const getCompanys = createAction(GET_COMPANYS, api.getCompanys);
export const getCompanyTrees = createAction(
  GET_COMPANY_TREES,
  api.getCompanyTrees
);

const initialState = Map({
  fields: [],
  fieldTrees: [],
  agents: [],
  agentVendors: [],
  agentModels: [],
  agentModelTrees: [],
  companys: [],
  companyTrees: [],
  loading: false,
});

export default handleActions(
  {
    ...pender({
      type: GET_FILTER_FIELDS,
      onSuccess: (state, action) =>
        state.set('fields', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: GET_FILTER_FIELD_TREES,
      onSuccess: (state, action) =>
        state.set('fieldTrees', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: GET_AGENTS,
      onSuccess: (state, action) =>
        state.set('agents', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: GET_AGENT_VENDORS,
      onSuccess: (state, action) =>
        state.set('agentVendors', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: GET_AGENT_MODELS,
      onSuccess: (state, action) =>
        state.set('agentModels', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: GET_AGENT_MODEL_TREES,
      onSuccess: (state, action) =>
        state.set('agentModelTrees', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: GET_COMPANYS,
      onSuccess: (state, action) =>
        state.set('companys', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: GET_COMPANY_TREES,
      onSuccess: (state, action) =>
        state.set('companyTrees', action.payload.data.content),
      onFailure: state => state.set('loading', false),
    }),
  },
  initialState
);
