import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import * as api from '../api/threattaxonomy';
import { pender } from 'redux-pender/lib/utils';

//action type
const SELECT_TAXO = 'get/SELECT_TAXO',
  SELECT_RULE = 'get/SELECT_RULE',
  SELECT_RULE_TOTAL = 'get/SELECT_RULE_TOTAL',
  INSERT_TAXO = 'post/INSERT_TAXO',
  UPDATE_TAXO = 'post/UPDATE_TAXO',
  UPDATE_TAXO_HIER = 'post/UPDATE_TAXO_HIER',
  DELETE_TAXO = 'post/DELETE_TAXO',
  CLEAR_TPPATH = 'get/CLEAR_TPPATH',
  CLEAR_RULEDATA = 'get/CLEAR_RULEDATA',
  GET_SUB_NODES = 'threattaxo/GET_SUB_NODES';

//action creator(action)
export const getNodeData = createAction(SELECT_TAXO, api.getNodeData);
export const getRule = createAction(SELECT_RULE, api.getRule);
export const getRuleTotal = createAction(SELECT_RULE_TOTAL);
export const insertTaxo = createAction(INSERT_TAXO, api.insertTaxo);
export const updateTaxo = createAction(UPDATE_TAXO, api.updateTaxo);
export const updateTaxoHier = createAction(
  UPDATE_TAXO_HIER,
  api.updateTaxoHier
);
export const deleteTaxo = createAction(INSERT_TAXO, api.deleteTaxo);
export const clearTpPath = createAction(CLEAR_TPPATH);
export const clearRuleData = createAction(CLEAR_RULEDATA);
export const getSubNodes = createAction(GET_SUB_NODES, api.getSubNodes);

//inital state
const initialState = Map({
  taxoData: {
    thrtSeq: 0,
    tpThrtSeq: 0,
    thrtCd: '',
    thrtNm: '',
    thrtLv: 0,
    thrtSort: 0,
    bakThrtSeq: 0,
    thrtTpPath: '',
    vuln: '',
    cve: '',
    cpe: '',
    resInfo: '',
    useYn: '',
    hasSubNode: '',
  },
  ruleData: [],
  loading: false,
});

///action handler(reducer)
const reducer = handleActions(
  {
    ...pender({
      type: SELECT_TAXO,
      onSuccess: (state, action) =>
        state.set('taxoData', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: SELECT_RULE,
      onSuccess: (state, action) =>
        state.set('ruleData', action.payload.data).set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: INSERT_TAXO,
      onSuccess: (state, action) => state.set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: UPDATE_TAXO,
      onSuccess: (state, action) => state.set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: UPDATE_TAXO_HIER,
      onSuccess: (state, action) => state.set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: DELETE_TAXO,
      onSuccess: (state, action) => state.set('loading', false),
      onFailure: (state, action) => state.set('loading', false),
      onPending: (state, action) => state.set('loading', true),
    }),
    ...pender({
      type: GET_SUB_NODES,
      onSuccess: state => state.set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    [SELECT_RULE_TOTAL]: (state, action) =>
      state.set('ruleData', action.payload),
    [CLEAR_TPPATH]: (state, action) => state.set('taxoData', {}),
    [CLEAR_RULEDATA]: (state, action) => state.set('ruleData', []),
  },
  initialState
);

export default reducer;
