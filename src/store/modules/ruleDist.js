import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { pender } from 'redux-pender';

import * as api from './../api/ruleDist';

const GET_RULE_DIST_HISTORIES = 'ruleDist/GET_RULE_DIST_HISTORIES';
const GET_RULE_DIST_DETAIL = 'ruleDist/GET_RULE_DIST_DETAIL';

const ROLLBACK_RULE_DIST = 'ruleDist/ROLLBACK_RULE_DIST';

export const getRuleDistHistories = createAction(
  GET_RULE_DIST_HISTORIES,
  api.getRuleDistHistories
);
export const getRuleDistDetail = createAction(
  GET_RULE_DIST_DETAIL,
  api.getRuleDistDetail
);
export const getLatestRuleDistDetail = createAction(
  GET_RULE_DIST_DETAIL,
  api.getLatestRuleDistDetail
);

export const rollbackDistHistory = createAction(
  ROLLBACK_RULE_DIST,
  api.rollbackDistHistory
);

const initialState = Map({
  ruleDistHists: [],
  ruleDistDetail: [],
  loading: false,
});

export default handleActions(
  {
    ...pender({
      type: GET_RULE_DIST_HISTORIES,
      onSuccess: (state, action) =>
        state.set('ruleDistHists', action.payload.data).set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_RULE_DIST_DETAIL,
      onSuccess: (state, action) =>
        state.set('ruleDistDetail', action.payload.data).set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: ROLLBACK_RULE_DIST,
      onSuccess: state => state.set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
  },
  initialState
);
