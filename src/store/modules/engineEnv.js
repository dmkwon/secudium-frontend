import { createAction, handleActions } from 'redux-actions';
import { Map, fromJS } from 'immutable';
import { pender } from 'redux-pender/lib/utils';

import * as api from '../api/engineEnv';

//action type
const GET_ENGINEENV = 'engineenv/GET_ENGINEENV';
const UPDATE_ENGINEENV = 'engineenv/UPDATE_ENGINEENV';

const GET_ENGINE_COMMANDS = 'engineenv/GET_ENGINE_COMMANDS';
const INSERT_ENGINE_COMMAND = 'engineenv/INSERT_ENGINE_COMMAND';
const UPDATE_ENGINE_COMMAND = 'engineenv/UPDATE_ENGINE_COMMAND';
const DELETE_ENGINE_COMMAND = 'engineenv/DELETE_ENGINE_COMMAND';

const GET_ENGINE_USERDEFINEHDR = 'engineenv/GET_ENGINE_USERDEFINEHDR';
const INSERT_ENGINE_USERDEFINEHDR = 'engineenv/INSERT_ENGINE_USERDEFINEHDR';
const UPDATE_ENGINE_USERDEFINEHDR = 'engineenv/UPDATE_ENGINE_USERDEFINEHDR';
const DELETE_ENGINE_USERDEFINEHDR = 'engineenv/DELETE_ENGINE_USERDEFINEHDR';

//action creator(action)
export const getEngineEnv = createAction(GET_ENGINEENV, api.getEngineEnv);
export const updateEngineEnv = createAction(
  UPDATE_ENGINEENV,
  api.updateEngineEnv
);

export const getEngineCommands = createAction(
  GET_ENGINE_COMMANDS,
  api.getEngineCommands
);
export const insertEngineCommand = createAction(
  INSERT_ENGINE_COMMAND,
  api.insertEngineCommand
);
export const updateEngineCommand = createAction(
  UPDATE_ENGINE_COMMAND,
  api.updateEngineCommand
);
export const deleteEngineCommand = createAction(
  DELETE_ENGINE_COMMAND,
  api.deleteEngineCommand
);

export const getUserDefineHdrs = createAction(
  GET_ENGINE_USERDEFINEHDR,
  api.getUserDefineHdrs
);
export const insertUserDefineHdr = createAction(
  INSERT_ENGINE_USERDEFINEHDR,
  api.insertUserDefineHdr
);
export const updateUserDefineHdr = createAction(
  UPDATE_ENGINE_USERDEFINEHDR,
  api.updateUserDefineHdr
);
export const deleteUserDefineHdr = createAction(
  DELETE_ENGINE_USERDEFINEHDR,
  api.deleteUserDefineHdr
);

//inital state
const initialState = Map({
  engineEnv: Map({
    zkConnInfo: '',
    rootPath: '',
    bakPath: '',
    nodeSize: '',
    dataSprtn: '',
    hdrDtl: '',
  }),
  commands: [],
  userDefineHdrs: [],
});

export default handleActions(
  {
    ...pender({
      type: GET_ENGINEENV,
      onSuccess: (state, action) =>
        state.set('engineEnv', fromJS(action.payload.data)),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_ENGINE_COMMANDS,
      onSuccess: (state, action) => state.set('commands', action.payload.data),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_ENGINE_USERDEFINEHDR,
      onSuccess: (state, action) =>
        state.set('userDefineHdrs', action.payload.data),
      onFailure: state => state.set('loading', false),
    }),
  },
  initialState
);
