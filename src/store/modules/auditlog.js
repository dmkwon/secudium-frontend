import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { pender } from 'redux-pender/lib/utils';

import * as api from '../api/auditlog';

//action type
const GET_AUDIT_LOGS = 'auditlog/GET_AUDIT_LOGS';
const GET_AUDIT_LOGS_BY_SEQ = 'auditlog/GET_AUDIT_LOGS_BY_SEQ';

//action creator(action)
export const getAuditLogs = createAction(GET_AUDIT_LOGS, api.getAuditLogs);
export const getAuditLogsBySeq = createAction(
  GET_AUDIT_LOGS_BY_SEQ,
  api.getAuditLogsBySeq
);

//inital state
const initialState = Map({
  auditLogs: {
    content: [],
  },
  auditLogsBySeq: {
    auditLogDetail: '',
    resultCode: '',
    url: '',
    urlMethod: '',
  },
  loading: false,
});

//action handler(reducer)
export default handleActions(
  {
    ...pender({
      type: GET_AUDIT_LOGS,
      onSuccess: (state, action) =>
        state.set('auditLogs', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: GET_AUDIT_LOGS_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('auditLogsBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
  },
  initialState
);
