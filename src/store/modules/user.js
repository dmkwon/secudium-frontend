import { createAction, handleActions } from 'redux-actions';
import { Map, List } from 'immutable';
import { pender } from 'redux-pender/lib/utils';

import * as api from '../api/user';

//action type
const GET_USERS = 'users/GET_USERS';
const GET_USERS_BY_SEQ = 'users/GET_USERS_BY_SEQ';
const GET_CUSTM_GROUP = 'users/GET_CUSTM_GROUP';
const GET_CONM_BY_CUSTM_GROUP_SEQ = 'users/GET_CONM_BY_CUSTM_GROUP_SEQ';
const CREATE_USERS = 'users/CREATE_USERS';
const UPDATE_USERS = 'users/UPDATE_USERS';
const CHANGE_PWD = 'users/CHANGE_PWD';
const OPEN_DELETELIST_POPUP = 'users/OPEN_DELETELIST_POPUP';

const IS_DUPLICATE = 'users/IS_DUPLICATE';

//action creator(action)
export const getUsers = createAction(GET_USERS, api.getUsers);
export const getUsersBySeq = createAction(GET_USERS_BY_SEQ, api.getUsersBySeq);
export const getCustmGroup = createAction(GET_CUSTM_GROUP, api.getCustmGroup);
export const getCoNmByCustmGroupSeq = createAction(
  GET_CONM_BY_CUSTM_GROUP_SEQ,
  api.getCoNmByCustmGroupSeq
);
export const createUser = createAction(CREATE_USERS, api.createUser);
export const updateUser = createAction(UPDATE_USERS, api.updateUser);
export const changePwd = createAction(CHANGE_PWD, api.changePwd);
export const openDelListPopup = createAction(OPEN_DELETELIST_POPUP);

export const isDuplicate = createAction(IS_DUPLICATE, api.isDuplicate);

//inital state
const initialState = Map({
  users: {
    content: [],
  },

  usersBySeq: {
    usrNo: '',
    coNm: '',
    usrId: '',
    usrNm: '',
    deptNm: '',
    dutyCode: '',
    accTypeCode: '',
    usrPwd: '',
    usrEmail: '',
    usrMobileNo: '',
    accLockYn: '',
  },

  custmGroup: {
    content: List([]),
  },
  coNmByCustmGroupSeq: {
    content: List([]),
  },

  isDisabled: '',
  isSuccess: 0,
  loading: false,
  result: 0,
});

//action handler(reducer)
export default handleActions(
  {
    ...pender({
      type: GET_USERS,
      onSuccess: (state, action) =>
        state.set('users', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: GET_USERS_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('usersBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: GET_CUSTM_GROUP,
      onSuccess: (state, action) =>
        state.set('custmGroup', action.payload.data),
    }),

    ...pender({
      type: GET_CONM_BY_CUSTM_GROUP_SEQ,
      onSuccess: (state, action) =>
        state.set('coNmByCustmGroupSeq', action.payload.data),
    }),

    ...pender({
      type: CREATE_USERS,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: UPDATE_USERS,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: CHANGE_PWD,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: IS_DUPLICATE,
    }),
  },
  initialState
);
