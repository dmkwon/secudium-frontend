import _ from "@lodash";
import { Map } from "immutable";
import { createAction, handleActions } from "redux-actions";

import { pender } from "redux-pender";

import GlobalUtils from "util/GlobalUtils";
import * as api from "../api/nmlzRule";

export const PAGE_TYPE = {
  LIST: "list",
  DETAIL: "detail"
};

export const PAGE_ACTION_TYPE = {
  DEFAULT: "",
  ADD: "add",
  EDIT: "edit"
};

// action type
const GET_NMLZRULES = "nmlzRule/GET_NMLZRULES";
const GET_NMLZRULE = "nmlzRule/GET_NMLZRULE";
const CREATE_NMLZRULE = "nmlzRule/CREATE_NMLZRULE";
const UPDATE_NMLZRULE = "nmlzRule/UPDATE_NMLZRULE";
const REMOVE_NMLZRULE = "nmlzRule/REMOVE_NMLZRULE";

const CAN_BE_SUBMIT = "nmlzRule/CAN_BE_SUBMIT";

// history
const GET_NMLZRULES_HISTS = "nmlzRule/GET_NMLZRULES_HISTS";
const GET_NMLZRULES_HIST = "nmlzRule/GET_NMLZRULES_HIST";
const GET_NMLZRULES_ROLLBACK = "nmlzRule/GET_NMLZRULES_ROLLBACK";

const SELECT_ALL_NMLZRULES = "nmlzRule/SELECT_ALL_NMLZRULES";
const DESELECT_ALL_NMLZRULES = "nmlzRule/DESELECT_ALL_NMLZRULES";
const TOGGLE_IN_SELECTED_NMLZRULES = "nmlzRule/TOGGLE_IN_SELECTED_NMLZRULES";
const SET_SEARCH_TEXT = "nmlzRule/SET_SEARCH_TEXT";

const OPEN_DELETE_POPUP = "nmlzRule/OPEN_DELETE_POPUP";
const CLOSE_DELETE_POPUP = "nmlzRule/CLOSE_DELETE_POPUP";
const OPEN_ROLLBACK_POPUP = "nmlzRule/OPEN_ROLLBACK_POPUP";
const CLOSE_ROLLBACK_POPUP = "nmlzRule/CLOSE_ROLLBACK_POPUP";
const OPEN_FIELD_SELECT_POPUP = "nmlzRule/OPEN_FIELD_SELECT_POPUP";
const CLOSE_FIELD_SELECT_POPUP = "nmlzRule/CLOSE_FIELD_SELECT_POPUP";

const OPEN_NEW_FIELD_FUNC_POPUP = "nmlzRule/OPEN_NEW_FIELD_FUNC_POPUP";
const CLOSE_NEW_FIELD_FUNC_POPUP = "nmlzRule/CLOSE_NEW_FIELD_FUNC_POPUP";
const OPEN_EDIT_FIELD_FUNC_POPUP = "nmlzRule/OPEN_EDIT_FIELD_FUNC_POPUP";
const CLOSE_EDIT_FIELD_FUNC_POPUP = "nmlzRule/CLOSE_EDIT_FIELD_FUNC_POPUP";

const OPEN_NEW_MERGE_POPUP = "nmlzRule/OPEN_NEW_MERGE_POPUP";
const CLOSE_NEW_MERGE_POPUP = "nmlzRule/CLOSE_NEW_MERGE_POPUP";
const OPEN_EDIT_MERGE_POPUP = "nmlzRule/OPEN_EDIT_MERGE_POPUP";
const CLOSE_EDIT_MERGE_POPUP = "nmlzRule/CLOSE_EDIT_MERGE_POPUP";

const OPEN_NEW_REPLACE_POPUP = "nmlzRule/OPEN_NEW_REPLACE_POPUP";
const CLOSE_NEW_REPLACE_POPUP = "nmlzRule/CLOSE_NEW_REPLACE_POPUP";
const OPEN_EDIT_REPLACE_POPUP = "nmlzRule/OPEN_EDIT_REPLACE_POPUP";
const CLOSE_EDIT_REPLACE_POPUP = "nmlzRule/CLOSE_EDIT_REPLACE_POPUP";

const OPEN_CANCEL_CONFIRM_POPUP = "nmlzRule/OPEN_CANCEL_CONFIRM_POPUP";
const CLOSE_CANCEL_CONFIRM_POPUP = "nmlzRule/CLOSE_CANCEL_CONFIRM_POPUP";

const OPEN_NEW_HDR_POPUP = "nmlzRule/OPEN_NEW_HDR_POPUP";
const CLOSE_NEW_HDR_POPUP = "nmlzRule/CLOSE_NEW_HDR_POPUP";
const OPEN_EDIT_HDR_POPUP = "nmlzRule/OPEN_EDIT_HDR_POPUP";
const CLOSE_EDIT_HDR_POPUP = "nmlzRule/CLOSE_EDIT_HDR_POPUP";

const OPEN_NEW_CONT_POPUP = "nmlzRule/OPEN_NEW_CONT_POPUP";
const CLOSE_NEW_CONT_POPUP = "nmlzRule/CLOSE_NEW_CONT_POPUP";
const OPEN_EDIT_CONT_POPUP = "nmlzRule/OPEN_EDIT_CONT_POPUP";
const CLOSE_EDIT_CONT_POPUP = "nmlzRule/CLOSE_EDIT_CONT_POPUP";

const OPEN_DEFAULT_VALUE_APPLY_POPUP =
  "nmlzRule/OPEN_DEFAULT_VALUE_APPLY_POPUP";
const CLOSE_DEFAULT_VALUE_APPLY_POPUP =
  "nmlzRule/CLOSE_DEFAULT_VALUE_APPLY_POPUP";

const OPEN_DUPLICATE_POPUP = "nmlzRule/OPEN_DUPLICATE_POPUP";
const CLOSE_DUPLICATE_POPUP = "nmlzRule/CLOSE_DUPLICATE_POPUP";

const INIT_NMLZRULE = "nmlzRule/INIT_NMLZRULE";
const EDIT_NMLZRULE = "nmlzRule/EDIT_NMLZRULE";
const CANCEL_NMLZRULE = "nmlzRule/CANCEL_NMLZRULE";

const REMOVE_NMLZRULE_HDR = "nmlzRule/REMOVE_NMLZRULE_HDR";
const EDIT_NMLZRULE_HDR = "nmlzRule/EDIT_NMLZRULE_HDR";
const SAVE_NMLZRULE_HDR = "nmlzRule/SAVE_NMLZRULE_HDR";
const CANCEL_NMLZRULE_HDR = "nmlzRule/CANCEL_NMLZRULE_HDR";
const CAN_BE_HDR_SAVE = "nmlzRule/CAN_BE_HDR_SAVE";

const REMOVE_NMLZRULE_CONT = "nmlzRule/REMOVE_NMLZRULE_CONT";
const EDIT_NMLZRULE_CONT = "nmlzRule/EDIT_NMLZRULE_CONT";
const SAVE_NMLZRULE_CONT = "nmlzRule/SAVE_NMLZRULE_CONT";
const CANCEL_NMLZRULE_CONT = "nmlzRule/CANCEL_NMLZRULE_CONT";
const GET_NMLZRULE_CONTS = "nmlzRule/GET_NMLZRULE_CONTS";
const GET_NMLZRULE_CONT_HISTS = "nmlzRule/GET_NMLZRULE_CONT_HISTS";
const CAN_BE_CONT_SAVE = "nmlzRule/CAN_BE_CONT_SAVE";
const CAN_BE_ADD_CONT = "nmlzRule/CAN_BE_ADD_CONT";

const ADD_NMLZRULE_FIELD = "nmlzRule/ADD_NMLZRULE_FIELD";
const ADD_NMLZRULE_FIELDS = "nmlzRule/ADD_NMLZRULE_FIELDS";
const REMOVE_NMLZRULE_FIELD = "nmlzRule/REMOVE_NMLZRULE_FIELD";
const EDIT_NMLZRULE_FIELD = "nmlzRule/EDIT_NMLZRULE_FIELD";

const REMOVE_NMLZRULE_FIELD_FUNCS = "nmlzRule/REMOVE_NMLZRULE_FIELD_FUNCS";
const SAVE_NMLZRULE_FIELD_FUNCS = "nmlzRule/SAVE_NMLZRULE_FIELD_FUNCS";

const REMOVE_NMLZRULE_FIELD_MERGES = "nmlzRule/REMOVE_NMLZRULE_FIELD_MERGES";
const SAVE_NMLZRULE_FIELD_MERGES = "nmlzRule/SAVE_NMLZRULE_FIELD_MERGES";

const REMOVE_NMLZRULE_FIELD_REPLACES =
  "nmlzRule/REMOVE_NMLZRULE_FIELD_REPLACES";
const SAVE_NMLZRULE_FIELD_REPLACES = "nmlzRule/SAVE_NMLZRULE_FIELD_REPLACES";

const NAV_PAGE_TARGET = "nmlzRule/NAV_PAGE_TARGET";
const PROC_NMLZRULE_HDR = "nmlzRule/PROC_NMLZRULE_HDR";
const PROC_NMLZRULE_CONT = "nmlzRule/PROC_NMLZRULE_CONT";

const SET_FIELD = "nmlzRule/SET_FIELD";
const CLEAR_DATA = "nmlzRule/CLEAR_DATA";

const SET_VENDORS = "nmlzRule/SET_VENDORS";
const SET_SATCS = "nmlzRule/SET_SATCS";
const SET_MODELS = "nmlzRule/SET_MODELS";
const SET_IS_EXIST_DEFAULT_USE_YN = "nmlzRule/SET_IS_EXIST_DEFAULT_USE_YN";
const SET_REGEX_RESULT = "nmlzRule/SET_REGEX_RESULT";
const SET_MATCH_INFO = "nmlzRule/SET_MATCH_INFO";
const DEPLOY_RULE = "nmlzRule/DEPLOY_RULE";
const EDIT_HDR_PRIORITY = "nmlzRule/EDIT_HDR_PRIORITY";
const EDIT_CONT_PRIORITY = "nmlzRule/EDIT_CONT_PRIORITY";

/** 서버 통신 action */
export const getNmlzRules = createAction(GET_NMLZRULES, api.getNmlzRules);
export const getNmlzRule = createAction(GET_NMLZRULE, api.getNmlzRule);
export const getNmlzRulesHists = createAction(
  GET_NMLZRULES_HISTS,
  api.getNmlzRulesHists
);
export const getNmlzRulesHist = createAction(
  GET_NMLZRULES_HIST,
  api.getNmlzRulesHist
);
export const rollBackNmlzRule = createAction(
  GET_NMLZRULES_ROLLBACK,
  api.rollBackNmlzRule
);
export const createNmlzRule = createAction(CREATE_NMLZRULE, api.createNmlzRule);
export const updateNmlzRule = createAction(UPDATE_NMLZRULE, api.updateNmlzRule);
export const removeNmlzRule = createAction(REMOVE_NMLZRULE, api.removeNmlzRule);
export const deployRule = createAction(DEPLOY_RULE, api.deployRule);

/** UI/UX action */
/** 정규화룰 목록 actions */
export const selectAllNmlzRules = createAction(SELECT_ALL_NMLZRULES);
export const deSelectAllNmlzRules = createAction(DESELECT_ALL_NMLZRULES);
export const toggleInSelectedNmlzRules = createAction(
  TOGGLE_IN_SELECTED_NMLZRULES
);
export const setSearchText = createAction(SET_SEARCH_TEXT);
export const openDeletePopup = createAction(OPEN_DELETE_POPUP);
export const closeDeletePopup = createAction(CLOSE_DELETE_POPUP);
export const openRollBackPopup = createAction(OPEN_ROLLBACK_POPUP);
export const closeRollBackPopup = createAction(CLOSE_ROLLBACK_POPUP);
export const openFieldSelectPopup = createAction(OPEN_FIELD_SELECT_POPUP);
export const closeFieldSelectPopup = createAction(CLOSE_FIELD_SELECT_POPUP);
export const openDefaultValueApplyPopup = createAction(
  OPEN_DEFAULT_VALUE_APPLY_POPUP
);
export const closeDefaultValueApplyPopup = createAction(
  CLOSE_DEFAULT_VALUE_APPLY_POPUP
);

/** 정규화룰 submit 가능 체크 action */
export const canBeSubmit = createAction(CAN_BE_SUBMIT);

/** 정규화룰 신규 생성 시 초기값 setting action */
export const initNmlzRule = createAction(INIT_NMLZRULE);
/** 정규화룰 각 항목 값 변경 action */
export const editNmlzRule = createAction(EDIT_NMLZRULE);
export const cancelNmlzRule = createAction(CANCEL_NMLZRULE);

export const removeNmlzRuleHdr = createAction(REMOVE_NMLZRULE_HDR);
export const editNmlzRuleHdr = createAction(EDIT_NMLZRULE_HDR);
export const saveNmlzRuleHdr = createAction(SAVE_NMLZRULE_HDR);
export const procNmlzRuleHdr = createAction(PROC_NMLZRULE_HDR);
export const canBeHdrSave = createAction(CAN_BE_HDR_SAVE);

export const removeNmlzRuleCont = createAction(REMOVE_NMLZRULE_CONT);
export const editNmlzRuleCont = createAction(EDIT_NMLZRULE_CONT);
export const saveNmlzRuleCont = createAction(SAVE_NMLZRULE_CONT);
export const procNmlzRuleCont = createAction(PROC_NMLZRULE_CONT);
export const getNmlzRuleConts = createAction(GET_NMLZRULE_CONTS);
export const getNmlzRuleContHists = createAction(GET_NMLZRULE_CONT_HISTS);
export const canBeAddCont = createAction(CAN_BE_ADD_CONT);
export const canBeContSave = createAction(CAN_BE_CONT_SAVE);

export const removeNmlzRuleFieldFuncs = createAction(
  REMOVE_NMLZRULE_FIELD_FUNCS
);
export const saveNmlzRuleFieldFuncs = createAction(SAVE_NMLZRULE_FIELD_FUNCS);

export const removeNmlzRuleFieldMerges = createAction(
  REMOVE_NMLZRULE_FIELD_MERGES
);
export const saveNmlzRuleFieldMerges = createAction(SAVE_NMLZRULE_FIELD_MERGES);

export const removeNmlzRuleFieldReplaces = createAction(
  REMOVE_NMLZRULE_FIELD_REPLACES
);
export const saveNmlzRuleFieldReplaces = createAction(
  SAVE_NMLZRULE_FIELD_REPLACES
);

export const addNmlzRuleField = createAction(ADD_NMLZRULE_FIELD);
export const addNmlzRuleFields = createAction(ADD_NMLZRULE_FIELDS);

export const removeNmlzRuleField = createAction(REMOVE_NMLZRULE_FIELD);
export const editNmlzRuleField = createAction(EDIT_NMLZRULE_FIELD);

export const openNewFieldFuncPopup = createAction(OPEN_NEW_FIELD_FUNC_POPUP);
export const closeNewFieldFuncPopup = createAction(CLOSE_NEW_FIELD_FUNC_POPUP);
export const openEditFieldFuncPopup = createAction(OPEN_EDIT_FIELD_FUNC_POPUP);
export const closeEditFieldFuncPopup = createAction(
  CLOSE_EDIT_FIELD_FUNC_POPUP
);

export const openNewMergePopup = createAction(OPEN_NEW_MERGE_POPUP);
export const closeNewMergePopup = createAction(CLOSE_NEW_MERGE_POPUP);
export const openEditMergePopup = createAction(OPEN_EDIT_MERGE_POPUP);
export const closeEditMergePopup = createAction(CLOSE_EDIT_MERGE_POPUP);

export const openNewReplacePopup = createAction(OPEN_NEW_REPLACE_POPUP);
export const closeNewReplacePopup = createAction(CLOSE_NEW_REPLACE_POPUP);
export const openEditReplacePopup = createAction(OPEN_EDIT_REPLACE_POPUP);
export const closeEditReplacePopup = createAction(CLOSE_EDIT_REPLACE_POPUP);

export const openCancelConfirmPopup = createAction(OPEN_CANCEL_CONFIRM_POPUP);
export const closeCancelConfirmPopup = createAction(CLOSE_CANCEL_CONFIRM_POPUP);

export const openNewHdrPopup = createAction(OPEN_NEW_HDR_POPUP);
export const closeNewHdrPopup = createAction(CLOSE_NEW_HDR_POPUP);
export const openEditHdrPopup = createAction(OPEN_EDIT_HDR_POPUP);
export const closeEditHdrPopup = createAction(CLOSE_EDIT_HDR_POPUP);

export const openNewContPopup = createAction(OPEN_NEW_CONT_POPUP);
export const closeNewContPopup = createAction(CLOSE_NEW_CONT_POPUP);
export const openEditContPopup = createAction(OPEN_EDIT_CONT_POPUP);
export const closeEditContPopup = createAction(CLOSE_EDIT_CONT_POPUP);

export const openDuplicatePopup = createAction(OPEN_DUPLICATE_POPUP);
export const closeDuplicatePopup = createAction(CLOSE_DUPLICATE_POPUP);

export const navPageTarget = createAction(NAV_PAGE_TARGET);

export const setField = createAction(SET_FIELD);
export const clearData = createAction(CLEAR_DATA);
export const cancelNmlzRuleHdr = createAction(CANCEL_NMLZRULE_HDR);
export const cancelNmlzRuleCont = createAction(CANCEL_NMLZRULE_CONT);

export const setVendors = createAction(SET_VENDORS);
export const setSatcs = createAction(SET_SATCS);
export const setModels = createAction(SET_MODELS);
export const setIsExistDefaultUseYn = createAction(SET_IS_EXIST_DEFAULT_USE_YN);
export const setRegexResult = createAction(SET_REGEX_RESULT);
export const setMatchInfo = createAction(SET_MATCH_INFO);
export const setHdrPriority = createAction(EDIT_HDR_PRIORITY);
export const setContPriority = createAction(EDIT_CONT_PRIORITY);

const newNmlzRule = {
  logFormatCode: "RGX",
  nmlzRule: "",
  nmlzRuleAgents: [],
  nmlzRuleHdrs: [],
  nmlzRuleDesc: "",
  nmlzRuleNm: "",
  nmlzRuleSeq: GlobalUtils.ID(),
  status: "A", // A(add), U(update), D(delete)
  useYn: "Y"
};

const navState = {
  navPageTarget: {
    type: PAGE_TYPE.LIST /** navigation list, detail */,
    detail:
      PAGE_ACTION_TYPE.DEFAULT /** if type is list, then detail is '' and other  new or edit  */,
    data: null
  }
};

const dataState = {
  /** 정규화룰 목록 */
  nmlzRules: Map({
    content: [],
    last: false,
    page: 1,
    size: 10,
    totalElements: 0,
    totalPages: 0
  }),
  loading: false,

  /** 선택한 정규화룰 정보 */
  nmlzRule: {},
  /** 선택한 정규화룰 원본 정보 */
  origNmlzRule: {},

  procNmlzRule: {
    type: "new",
    data: null
  },

  procNmlzRuleHdr: {
    type: "new",
    data: null
  },

  /** 정규화룰 목록에서 선택/해제한 정규화룰 seq 목록 */
  selectedNmlzRuleSeqs: [],
  /** 저장 가능한 상태인지 체크 true(가능) / false(불가능) */
  canBeSubmit: false,
  /** 서버로의 검색 조건 */
  routeParams: {},
  /** 서버로의 검색 키워드 */
  searchText: "",

  /** 선택한 정규화룰의 헤더 규칙 목록 중 컨텐츠규칙 정보 */
  nmlzRuleHdr: {},
  /** 선택한 정규화룰의 헤더 규칙 목록 중 컨텐츠규칙의 원본 정보 */
  origNmlzRuleHdr: {},
  canBeHdrSave: false,
  canBeAddCont: false,

  /** 선택한 정규화룰의 컨텐츠 규칙 목록 중 컨텐츠규칙 정보 */
  nmlzRuleCont: {},
  /** 선택한 정규화룰의 컨텐츠 규칙 목록 중 컨텐츠규칙의 원본 정보 */
  origNmlzRuleCont: {},
  canBeContSave: false,

  /** 선택한 정규화룰 이력 목록 */
  nmlzRuleHists: Map({
    content: [],
    last: false,
    page: 1,
    size: 10,
    totalElements: 0,
    totalPages: 0
  }),
  /** 선택한 정규화룰 이력 정보 */
  nmlzRuleHist: {},
  /** 필드 선택 팝업에서 선택한 field 정보 */
  selectedField: {},

  selectedVendors: [],
  selectedSatcs: [],
  selectedModels: [],
  isExistDefaultUseYn: false,
  isExistDuplicate: false,
  regexResult: {},
  matchInfo: [],
  currentHdrIdx: -1,
  currentHdrHistIdx: -1
};

const popupState = {
  deletePopup: {
    /** 삭제 시 표시 팝업 메타 정보 */
    type: "delete",
    props: {
      open: false
    },
    data: null
  },
  rollbackPopup: {
    /** 롤백 시 표시 팝업 메타 정보 */
    type: "rollback",
    props: {
      open: false
    },
    data: null
  },
  fieldFuncPopup: {
    /** field function 추가(new)/수정(edit) 팝업 메타 정보 */
    type: "new",
    props: {
      open: false
    },
    data: null
  },
  mergePopup: {
    /** merge rule 추가(new)/수정(edit) 팝업 메타 정보 */
    type: "new",
    props: {
      open: false
    },
    data: null
  },
  replacePopup: {
    /** replace rule 추가(new)/수정(edit) 팝업 메타 정보 */
    type: "new",
    props: {
      open: false
    },
    data: null
  },
  fieldSelectPopup: {
    type: "field",
    props: {
      from: "",
      open: false
    },
    data: null
  },
  cancelConfirmPopup: {
    type: "cancel",
    props: {
      from: "",
      open: false,
      action: null
    },
    data: null
  },
  hdrPopup: {
    type: "new",
    props: {
      open: false
    },
    data: null
  },
  contPopup: {
    type: "new",
    props: {
      open: false
    },
    data: null
  },
  defaultValueApplyPopup: {
    type: "defaultValueApply",
    props: {
      open: false
    },
    data: null
  },
  duplicatePopup: {
    type: "duplicate",
    props: {
      open: false
    },
    data: null
  }
};

const initialState = Map({
  ...navState,
  ...dataState,
  ...popupState
});

/** 헤더 규칙 생성 시 초기 데이터 생성 함수 */
function getNewNmlzRuleHdr(nmlzRuleSeq) {
  return {
    nmlzRuleHdrSeq: GlobalUtils.ID(),
    nmlzRuleSeq: nmlzRuleSeq,
    nmlzRuleHdrRegexp: "",
    nmlzRuleHdrSampLog: "",
    nmlzRuleHdrPrefix: "",
    nmlzRuleHdrKeyField: "",
    hdrPriority: "",
    status: "A", // A(add), U(update), D(delete)
    useYn: "Y",
    nmlzRuleConts: []
  };
}

/** 컨텐츠 규칙 생성 시 초기 데이터 생성 함수 */
function getNewNmlzRuleCont(nmlzRuleHdrSeq) {
  return {
    contRegexp: "",
    contSampLog: "",
    contPriority: "",
    contDesc: "",
    defaultUseYn: "N",
    eventTypeCode: [],
    status: "A", // A(add), U(update), D(delete)
    useYn: "Y",
    nmlzRuleContSeq: GlobalUtils.ID(),
    nmlzRuleFieldFuncs: [],
    nmlzRuleFieldMerges: [],
    nmlzRuleFieldReplaces: [],
    nmlzRuleFields: [],
    nmlzRuleHdrSeq: nmlzRuleHdrSeq
  };
}

/** 필드 생성 시 초기 데이터 생성 함수 */
function getNewNmlzRuleField(nmlzRuleContSeq) {
  return {
    field: "",
    fieldDefault: "",
    fieldId: 0,
    subField: "",
    subFieldId: 0,
    jsonKey: "",
    matchNo: 0,
    nmlzRuleContSeq: nmlzRuleContSeq,
    nmlzRuleFieldSeq: GlobalUtils.ID()
  };
}

export default handleActions(
  {
    ...pender({
      type: GET_NMLZRULES,
      onSuccess: (state, action) => {
        if (
          action.payload.data.content &&
          action.payload.data.content.length > 0
        ) {
          let content = action.payload.data.content.map(item => {
            item.checked = false;
            return item;
          });
          action.payload.data.content = content;
        }
        return state
          .set("nmlzRules", action.payload.data)
          .set("loading", false)
          .set("routeParams", action.routeParams);
      },
      onPending: (state, action) => state.set("loading", true),
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_NMLZRULE,
      onSuccess: (state, action) => {
        const { nmlzRuleAgents } = action.payload.data;
        const selectedVendors = _.keys(
          _.groupBy(nmlzRuleAgents, "agentVendorId")
        );
        const selectedSatcs = _.keys(
          _.groupBy(nmlzRuleAgents, "serviceItemCode")
        );
        const selectedModels = _.keys(
          _.groupBy(nmlzRuleAgents, "agentModelId")
        );

        return state
          .set("nmlzRule", action.payload.data)
          .set("origNmlzRule", action.payload.data)
          .set("selectedVendors", selectedVendors)
          .set("selectedSatcs", selectedSatcs)
          .set(
            "selectedModels",
            selectedModels[0] === "undefined" ? [] : selectedModels
          )
          .set("currentHdrIdx", -1)
          .set("loading", false);
      },
      onPending: (state, action) => {
        return state.set("nmlzRule", {}).set("loading", true);
      },
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: CREATE_NMLZRULE,
      onSuccess: (state, action) => {
        if (action.payload.data === "C013") {
          return state.set("isExistDuplicate", true).set("loading", false);
        } else {
          return state.set("isExistDuplicate", false).set("loading", false);
        }
      },
      onPending: (state, action) => state.set("loading", true),
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: UPDATE_NMLZRULE,
      onSuccess: (state, action) => {
        if (action.payload.data === "C013") {
          return state.set("isExistDuplicate", true).set("loading", false);
        } else {
          return state.set("isExistDuplicate", false).set("loading", false);
        }
      },
      onPending: (state, action) => state.set("loading", true),
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_NMLZRULES_HISTS,
      onSuccess: (state, action) =>
        state.set("nmlzRuleHists", action.payload.data).set("loading", false),
      onPending: (state, action) => {
        return state.set("nmlzRuleHist", {}).set("loading", true);
      },
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_NMLZRULES_HIST,
      onSuccess: (state, action) =>
        state.set("nmlzRuleHist", action.payload.data).set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: REMOVE_NMLZRULE,
      onSuccess: (state, action) => state.set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_NMLZRULES_ROLLBACK,
      onSuccess: (state, action) => {
        if (action.payload.data === "C013") {
          return state.set("isExistDuplicate", true).set("loading", false);
        } else {
          return state.set("isExistDuplicate", false).set("loading", false);
        }
      },
      onPending: (state, action) => state.set("loading", true),
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: DEPLOY_RULE,
      onSuccess: (state, action) => state.set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: state => state.set("loading", false)
    }),

    [SELECT_ALL_NMLZRULES]: (state, action) => {
      const arr = Object.keys(state.get("nmlzRules").content).map(
        k => state.get("nmlzRules").content[k]
      );
      const selectedNmlzRuleSeqs = arr.map(nmlzRule => nmlzRule.nmlzRuleSeq);
      return state.set("selectedNmlzRuleSeqs", selectedNmlzRuleSeqs);
    },

    [DESELECT_ALL_NMLZRULES]: (state, action) => {
      return state.set("selectedNmlzRuleSeqs", []);
    },

    [TOGGLE_IN_SELECTED_NMLZRULES]: (state, action) => {
      const nmlzRuleSeq = action.payload;
      let selectedNmlzRuleSeqs = state.get("selectedNmlzRuleSeqs");
      if (selectedNmlzRuleSeqs.find(seq => seq === nmlzRuleSeq) !== undefined) {
        selectedNmlzRuleSeqs = selectedNmlzRuleSeqs.filter(
          seq => seq !== nmlzRuleSeq
        );
      } else {
        selectedNmlzRuleSeqs = [...selectedNmlzRuleSeqs, nmlzRuleSeq];
      }
      return state.set("selectedNmlzRuleSeqs", selectedNmlzRuleSeqs);
    },
    [SET_SEARCH_TEXT]: (state, action) => {
      return state.set("searchText", action.payload);
    },
    /** 정규화룰 초기화 (추가시) action - ok */
    [INIT_NMLZRULE]: (state, action) => {
      return state
        .set("nmlzRule", newNmlzRule)
        .set("origNmlzRule", newNmlzRule)
        .set("selectedVendors", [])
        .set("selectedSatcs", [])
        .set("selectedModels", []);
    },

    /** 정규화룰 값 변경 action - ok  */
    [EDIT_NMLZRULE]: (state, action) => {
      return state
        .setIn(["nmlzRule", action.payload.name], action.payload.value)
        .setIn(["nmlzRule", "status"], "U");
    },

    /** 정규화룰 컨텐츠 우선순위 값 변경 action - ok  */
    [EDIT_CONT_PRIORITY]: (state, action) => {
      const hdrIdx = state.get("currentHdrIdx");
      return state.setIn(
        [
          "nmlzRule",
          "nmlzRuleHdrs",
          hdrIdx,
          "nmlzRuleConts",
          action.payload.index,
          "contPriority"
        ],
        action.payload.value
      );
    },

    /** 정규화룰 컨텐츠 우선순위 값 변경 action - ok  */
    [EDIT_HDR_PRIORITY]: (state, action) => {
      return state.setIn(
        ["nmlzRule", "nmlzRuleHdrs", action.payload.index, "hdrPriority"],
        action.payload.value
      );
    },

    /** 정규화룰 취소 action - ok  */
    [CANCEL_NMLZRULE]: (state, action) => {
      return state.set("nmlzRule", state.get("origNmlzRule"));
    },

    /** 헤더 항목 삭제 */
    [REMOVE_NMLZRULE_HDR]: (state, action) => {
      const newHdrs = _.filter(
        state.getIn(["nmlzRule", "nmlzRuleHdrs"]),
        cont => {
          return cont.nmlzRuleHdrSeq !== action.payload;
        }
      );
      return state
        .setIn(["nmlzRule", "nmlzRuleHdrs"], newHdrs)
        .set("currentHdrIdx", -1);
    },

    [PROC_NMLZRULE_HDR]: (state, action) => {
      const { data, type } = action.payload;
      if (type === PAGE_ACTION_TYPE.ADD) {
        let merge = Object.assign(
          JSON.parse(JSON.stringify(getNewNmlzRuleHdr(data.nmlzRuleSeq))),
          JSON.parse(JSON.stringify(data))
        );

        return state
          .set("procNmlzRule", action.payload)
          .set("nmlzRuleHdr", merge)
          .set("origNmlzRuleHdr", merge);
      } else if (type === PAGE_ACTION_TYPE.EDIT) {
        return state
          .set("procNmlzRule", action.payload)
          .set("nmlzRuleHdr", data)
          .set("origNmlzRuleHdr", data);
      }
    },

    /** 헤더 규칙 값 변경 시 action  */
    [EDIT_NMLZRULE_HDR]: (state, action) => {
      return state.setIn(
        ["nmlzRuleHdr", action.payload.name],
        action.payload.value
      );
    },

    /** 컨텐츠 규칙 추가, 수정 저장 시 action (array 의 첫번째 항목에 추가 _.concat 이용)*/
    [SAVE_NMLZRULE_HDR]: (state, action) => {
      const { type, data } = state.get("procNmlzRule");
      if (type === PAGE_ACTION_TYPE.ADD) {
        let newOne = [];
        const prevNmlzRuleHdrs = state.getIn(["nmlzRule", "nmlzRuleHdrs"]);
        newOne = _.concat(prevNmlzRuleHdrs, state.get("nmlzRuleHdr"));
        return state
          .set("origNmlzRuleHdr", state.get("nmlzRuleHdr"))
          .setIn(["nmlzRule", "nmlzRuleHdrs"], newOne);
      } else if (type === PAGE_ACTION_TYPE.EDIT) {
        const currNmlzRuleHdrs = state.getIn(["nmlzRule", "nmlzRuleHdrs"]);
        const index = _.findIndex(currNmlzRuleHdrs, hdr => {
          return hdr.nmlzRuleHdrSeq === data.nmlzRuleHdrSeq;
        });
        return state
          .setIn(["nmlzRule", "nmlzRuleHdrs", index], state.get("nmlzRuleHdr"))
          .set("origNmlzRuleHdr", state.get("nmlzRuleHdr"));
      }
    },

    [CAN_BE_HDR_SAVE]: (state, action) => {
      return state.set(
        "canBeHdrSave",
        !_.isEqual(state.get("nmlzRuleHdr"), state.get("origNmlzRuleHdr"))
      );
    },

    [CANCEL_NMLZRULE_HDR]: (state, action) => {
      return state.set("nmlzRuleHdr", state.get("origNmlzRuleHdr"));
    },

    /** 컨텐츠 항목 삭제 */
    [REMOVE_NMLZRULE_CONT]: (state, action) => {
      const newConts = _.filter(
        state.getIn([
          "nmlzRule",
          "nmlzRuleHdrs",
          state.get("currentHdrIdx"),
          "nmlzRuleConts"
        ]),
        cont => {
          return cont.nmlzRuleContSeq !== action.payload;
        }
      );
      return state.setIn(
        [
          "nmlzRule",
          "nmlzRuleHdrs",
          state.get("currentHdrIdx"),
          "nmlzRuleConts"
        ],
        newConts
      );
    },

    [PROC_NMLZRULE_CONT]: (state, action) => {
      const { data, type } = action.payload;

      if (type === PAGE_ACTION_TYPE.ADD) {
        let merge = Object.assign(
          JSON.parse(JSON.stringify(getNewNmlzRuleCont(data.nmlzRuleHdrSeq))),
          JSON.parse(JSON.stringify(data))
        );
        return state
          .set("regexResult", {})
          .set("procNmlzRuleHdr", action.payload)
          .set("nmlzRuleCont", merge)
          .set("origNmlzRuleCont", merge);
      } else if (type === PAGE_ACTION_TYPE.EDIT) {
        return state
          .set("regexResult", {})
          .set("procNmlzRuleHdr", action.payload)
          .set("nmlzRuleCont", data)
          .set("origNmlzRuleCont", data);
      }
    },

    [CAN_BE_ADD_CONT]: (state, action) => {
      return state.set("canBeAddCont", action.payload);
    },

    [GET_NMLZRULE_CONTS]: (state, action) => {
      return state
        .set("currentHdrIdx", action.payload.hdrIndex)
        .set("canBeAddCont", true);
    },

    [GET_NMLZRULE_CONT_HISTS]: (state, action) => {
      return state.set("currentHdrHistIdx", action.payload.hdrIndex);
    },
    /** 컨텐츠 규칙 값 변경 시 action  */
    [EDIT_NMLZRULE_CONT]: (state, action) => {
      return state.setIn(
        ["nmlzRuleCont", action.payload.name],
        action.payload.value
      );
    },

    /** 컨텐츠 규칙 추가, 수정 저장 시 action (array 의 첫번째 항목에 추가 _.concat 이용)*/
    [SAVE_NMLZRULE_CONT]: (state, action) => {
      const { type } = state.get("procNmlzRuleHdr");

      if (type === PAGE_ACTION_TYPE.ADD) {
        const defaultUseYn = state.get("nmlzRuleCont").defaultUseYn;
        let newOne = [];
        if (defaultUseYn === "Y") {
          const prevNmlzRuleConts = state.getIn([
            "nmlzRuleHdr",
            "nmlzRuleConts"
          ]);
          const newPrev = prevNmlzRuleConts.map(cont => {
            cont.defaultUseYn = "N";
            return cont;
          });
          newOne = _.concat(newPrev, state.get("nmlzRuleCont"));
        } else {
          const currNmlzRuleHdr = state.getIn([
            "nmlzRule",
            "nmlzRuleHdrs",
            state.get("currentHdrIdx")
          ]);
          newOne = _.concat(
            currNmlzRuleHdr.nmlzRuleConts,
            state.get("nmlzRuleCont")
          );
        }
        // const existItem = _.filter(newOne, item => {
        //   return item.defaultUseYn === 'Y';
        // });
        return state
          .set("origNmlzRuleCont", state.get("nmlzRuleCont"))
          .setIn(
            [
              "nmlzRule",
              "nmlzRuleHdrs",
              state.get("currentHdrIdx"),
              "nmlzRuleConts"
            ],
            newOne
          );
      } else if (type === PAGE_ACTION_TYPE.EDIT) {
        const defaultUseYn = state.get("nmlzRuleCont").defaultUseYn;
        let newNmlzRuleConts = [];
        if (defaultUseYn === "Y") {
          const hdrIdx = state.get("currentHdrIdx");
          const currNmlzRuleConts = [];
          var afterRemoveNmlzRuleConts = _.filter(currNmlzRuleConts, cont => {
            return (
              cont.nmlzRuleContSeq !== state.get("nmlzRuleCont").nmlzRuleContSeq
            );
          });
          newNmlzRuleConts = afterRemoveNmlzRuleConts.map(cont => {
            cont.defaultUseYn = "N";
            return cont;
          });
          newNmlzRuleConts.push(state.get("nmlzRuleCont"));
          return state
            .set("origNmlzRuleCont", state.get("nmlzRuleCont"))
            .setIn(
              ["nmlzRule", "nmlzRuleHdrs", hdrIdx, "nmlzRuleConts"],
              newNmlzRuleConts
            );
        } else {
          const hdrIdx = state.get("currentHdrIdx");
          newNmlzRuleConts = state.getIn([
            "nmlzRule",
            "nmlzRuleHdrs",
            hdrIdx,
            "nmlzRuleConts"
          ]);
          const index = _.findIndex(newNmlzRuleConts, cont => {
            return (
              cont.nmlzRuleContSeq === state.get("nmlzRuleCont").nmlzRuleContSeq
            );
          });
          return state
            .set("origNmlzRuleCont", state.get("nmlzRuleCont"))
            .setIn(
              ["nmlzRule", "nmlzRuleHdrs", hdrIdx, "nmlzRuleConts", index],
              state.get("nmlzRuleCont")
            );
        }
      }
    },

    [CAN_BE_CONT_SAVE]: (state, action) => {
      return state.set(
        "canBeContSave",
        !_.isEqual(state.get("nmlzRuleCont"), state.get("origNmlzRuleCont"))
      );
    },

    [CANCEL_NMLZRULE_CONT]: (state, action) => {
      return state.set("nmlzRuleCont", state.get("origNmlzRuleCont"));
    },

    /**필드 함수 삭제 시 action */
    [REMOVE_NMLZRULE_FIELD_FUNCS]: (state, action) => {
      const newFieldFuncs = _.filter(
        state.getIn(["nmlzRuleCont", "nmlzRuleFieldFuncs"]),
        cont => {
          return (
            cont.nmlzRuleFieldFuncSeq !== action.payload.nmlzRuleFieldFuncSeq
          );
        }
      );

      return state.setIn(["nmlzRuleCont", "nmlzRuleFieldFuncs"], newFieldFuncs);
    },

    /**필드 함수 추가, 수정 저장 시 action (array 의 첫번째 항목에 추가 _.concat 이용) */
    [SAVE_NMLZRULE_FIELD_FUNCS]: (state, action) => {
      const { type, data } = action.payload;
      if (type === "new") {
        const newOne = _.concat(
          data,
          state.getIn(["nmlzRuleCont", "nmlzRuleFieldFuncs"])
        );
        return state.setIn(["nmlzRuleCont", "nmlzRuleFieldFuncs"], newOne);
      } else if (type === "edit") {
        const index = _.findIndex(
          state.getIn(["nmlzRuleCont", "nmlzRuleFieldFuncs"]),
          cont => {
            return cont.nmlzRuleFieldFuncSeq === data.nmlzRuleFieldFuncSeq;
          }
        );
        return state.setIn(["nmlzRuleCont", "nmlzRuleFieldFuncs", index], data);
      }
    },

    /**Merge 규칙 삭제 시 action */
    [REMOVE_NMLZRULE_FIELD_MERGES]: (state, action) => {
      const newFieldMerges = _.filter(
        state.getIn(["nmlzRuleCont", "nmlzRuleFieldMerges"]),
        cont => {
          return (
            cont.nmlzRuleFieldMergeSeq !== action.payload.nmlzRuleFieldMergeSeq
          );
        }
      );

      return state.setIn(
        ["nmlzRuleCont", "nmlzRuleFieldMerges"],
        newFieldMerges
      );
    },

    [SAVE_NMLZRULE_FIELD_MERGES]: (state, action) => {
      const { type, data } = action.payload;
      if (type === "new") {
        const newOne = _.concat(
          data,
          state.getIn(["nmlzRuleCont", "nmlzRuleFieldMerges"])
        );
        return state.setIn(["nmlzRuleCont", "nmlzRuleFieldMerges"], newOne);
      } else if (type === "edit") {
        const index = _.findIndex(
          state.getIn(["nmlzRuleCont", "nmlzRuleFieldMerges"]),
          cont => {
            return cont.nmlzRuleFieldMergeSeq === data.nmlzRuleFieldMergeSeq;
          }
        );
        return state.setIn(
          ["nmlzRuleCont", "nmlzRuleFieldMerges", index],
          data
        );
      }
    },

    [REMOVE_NMLZRULE_FIELD_REPLACES]: (state, action) => {
      const newFieldMerges = _.filter(
        state.getIn(["nmlzRuleCont", "nmlzRuleFieldReplaces"]),
        cont => {
          return (
            cont.nmlzRuleFieldReplaceSeq !==
            action.payload.nmlzRuleFieldReplaceSeq
          );
        }
      );

      return state.setIn(
        ["nmlzRuleCont", "nmlzRuleFieldReplaces"],
        newFieldMerges
      );
    },
    [SAVE_NMLZRULE_FIELD_REPLACES]: (state, action) => {
      const { type, data } = action.payload;
      if (type === "new") {
        const newOne = _.concat(
          data,
          state.getIn(["nmlzRuleCont", "nmlzRuleFieldReplaces"])
        );
        return state.setIn(["nmlzRuleCont", "nmlzRuleFieldReplaces"], newOne);
      } else if (type === "edit") {
        const index = _.findIndex(
          state.getIn(["nmlzRuleCont", "nmlzRuleFieldReplaces"]),
          cont => {
            return (
              cont.nmlzRuleFieldReplaceSeq === data.nmlzRuleFieldReplaceSeq
            );
          }
        );
        return state.setIn(
          ["nmlzRuleCont", "nmlzRuleFieldReplaces", index],
          data
        );
      }
    },
    [ADD_NMLZRULE_FIELDS]: (state, action) => {
      const newOne = _.concat(
        state.getIn(["nmlzRuleCont", "nmlzRuleFields"]),
        getNewNmlzRuleField(action.payload)
      );
      return state.setIn(["nmlzRuleCont", "nmlzRuleFields"], newOne);
    },
    [ADD_NMLZRULE_FIELD]: (state, action) => {
      const newOne = _.concat(
        state.getIn(["nmlzRuleCont", "nmlzRuleFields"]),
        getNewNmlzRuleField(action.payload)
      );
      return state.setIn(["nmlzRuleCont", "nmlzRuleFields"], newOne);
    },
    [REMOVE_NMLZRULE_FIELD]: (state, action) => {
      const newFields = _.filter(
        state.getIn(["nmlzRuleCont", "nmlzRuleFields"]),
        cont => {
          return cont.nmlzRuleFieldSeq !== action.payload.nmlzRuleFieldSeq;
        }
      );

      return state.setIn(["nmlzRuleCont", "nmlzRuleFields"], newFields);
    },
    [EDIT_NMLZRULE_FIELD]: (state, action) => {
      const { index, name, value } = action.payload;

      return state.setIn(
        ["nmlzRuleCont", "nmlzRuleFields", index, name],
        value
      );
    },

    [CAN_BE_SUBMIT]: (state, action) => {
      return state.set(
        "canBeSubmit",
        !_.isEqual(state.get("nmlzRule"), state.get("origNmlzRule"))
      );
    },
    [OPEN_DELETE_POPUP]: (state, action) => {
      return state.set("deletePopup", {
        type: "delete",
        props: {
          from: action.payload.from,
          open: true
        },
        data: action.payload.data
      });
    },
    [CLOSE_DELETE_POPUP]: (state, action) => {
      return state.set("deletePopup", {
        type: "delete",
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_ROLLBACK_POPUP]: (state, action) => {
      return state.set("rollbackPopup", {
        type: "rollback",
        props: {
          open: true
        },
        data: action.payload
      });
    },
    [CLOSE_ROLLBACK_POPUP]: (state, action) => {
      return state.set("rollbackPopup", {
        type: "rollback",
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_FIELD_SELECT_POPUP]: (state, action) => {
      return state.set("fieldSelectPopup", {
        type: "field",
        props: {
          from: action.payload.from,
          open: true
        },
        data: action.payload.data
      });
    },
    [CLOSE_FIELD_SELECT_POPUP]: (state, action) => {
      return state.set("fieldSelectPopup", {
        type: "field",
        props: {
          from: action.payload.props.from,
          open: false
        },
        data: action.payload.data
      });
    },

    [OPEN_NEW_FIELD_FUNC_POPUP]: (state, action) => {
      return state.set("fieldFuncPopup", {
        type: "new",
        props: {
          open: true
        },
        data: action.payload
      });
    },
    [CLOSE_NEW_FIELD_FUNC_POPUP]: (state, action) => {
      return state.set("fieldFuncPopup", {
        type: "new",
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_EDIT_FIELD_FUNC_POPUP]: (state, action) => {
      return state.set("fieldFuncPopup", {
        type: "edit",
        props: {
          open: true
        },
        data: action.payload
      });
    },
    [CLOSE_EDIT_FIELD_FUNC_POPUP]: (state, action) => {
      return state.set("fieldFuncPopup", {
        type: "edit",
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_NEW_MERGE_POPUP]: (state, action) => {
      return state.set("mergePopup", {
        type: "new",
        props: {
          open: true
        },
        data: null
      });
    },
    [CLOSE_NEW_MERGE_POPUP]: (state, action) => {
      return state.set("mergePopup", {
        type: "new",
        props: {
          open: false
        },
        data: null
      });
    },
    [OPEN_EDIT_MERGE_POPUP]: (state, action) => {
      return state.set("mergePopup", {
        type: "edit",
        props: {
          open: true
        },
        data: action.payload
      });
    },
    [CLOSE_EDIT_MERGE_POPUP]: (state, action) => {
      return state.set("mergePopup", {
        type: "edit",
        props: {
          open: false
        },
        data: null
      });
    },
    [OPEN_NEW_REPLACE_POPUP]: (state, action) => {
      return state.set("replacePopup", {
        type: "new",
        props: {
          open: true
        },
        data: null
      });
    },

    [CLOSE_NEW_REPLACE_POPUP]: (state, action) => {
      return state.set("replacePopup", {
        type: "new",
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_EDIT_REPLACE_POPUP]: (state, action) => {
      return state.set("replacePopup", {
        type: "edit",
        props: {
          open: true
        },
        data: action.payload
      });
    },
    [CLOSE_EDIT_REPLACE_POPUP]: (state, action) => {
      return state.set("replacePopup", {
        type: "edit",
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_CANCEL_CONFIRM_POPUP]: (state, action) => {
      return state.set("cancelConfirmPopup", {
        type: "cancel",
        props: {
          from: action.payload.from,
          open: true,
          action: null
        },
        data: action.payload.data
      });
    },
    [CLOSE_CANCEL_CONFIRM_POPUP]: (state, action) => {
      return state.set("cancelConfirmPopup", {
        type: "cancel",
        props: {
          from: action.payload.props.from,
          open: false,
          action: action.payload.props.action
        },
        data: action.payload.data
      });
    },
    [OPEN_NEW_HDR_POPUP]: (state, action) => {
      return state.set("hdrPopup", {
        type: PAGE_ACTION_TYPE.ADD,
        props: {
          open: true
        },
        data: null
      });
    },

    [CLOSE_NEW_HDR_POPUP]: (state, action) => {
      return state.set("hdrPopup", {
        type: PAGE_ACTION_TYPE.ADD,
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_EDIT_HDR_POPUP]: (state, action) => {
      return state.set("hdrPopup", {
        type: PAGE_ACTION_TYPE.EDIT,
        props: {
          open: true
        },
        data: action.payload.data
      });
    },

    [CLOSE_EDIT_HDR_POPUP]: (state, action) => {
      return state.set("hdrPopup", {
        type: PAGE_ACTION_TYPE.EDIT,
        props: {
          open: false
        },
        data: action.payload.data
      });
    },
    [OPEN_NEW_CONT_POPUP]: (state, action) => {
      return state.set("contPopup", {
        type: PAGE_ACTION_TYPE.ADD,
        props: {
          open: true
        },
        data: null
      });
    },

    [CLOSE_NEW_CONT_POPUP]: (state, action) => {
      return state.set("contPopup", {
        type: PAGE_ACTION_TYPE.ADD,
        props: {
          open: false
        },
        data: null
      });
    },

    [OPEN_EDIT_CONT_POPUP]: (state, action) => {
      return state.set("contPopup", {
        type: PAGE_ACTION_TYPE.EDIT,
        props: {
          open: true
        },
        data: action.payload.data
      });
    },

    [CLOSE_EDIT_CONT_POPUP]: (state, action) => {
      return state.set("contPopup", {
        type: PAGE_ACTION_TYPE.EDIT,
        props: {
          open: false
        },
        data: action.payload.data
      });
    },
    [OPEN_DEFAULT_VALUE_APPLY_POPUP]: (state, action) => {
      return state.set("defaultValueApplyPopup", {
        type: "defaultValueApply",
        props: {
          open: true
        },
        data: action.payload
      });
    },

    [CLOSE_DEFAULT_VALUE_APPLY_POPUP]: (state, action) => {
      return state.set("defaultValueApplyPopup", {
        type: "defaultValueApply",
        props: {
          open: false
        },
        data: action.payload
      });
    },

    [OPEN_DUPLICATE_POPUP]: (state, action) => {
      return state.set("duplicatePopup", {
        type: "duplicate",
        props: {
          open: true
        },
        data: action.payload
      });
    },
    [CLOSE_DUPLICATE_POPUP]: (state, action) => {
      return state.set("duplicatePopup", {
        type: "duplicate",
        props: {
          open: false
        },
        data: null
      });
    },
    [NAV_PAGE_TARGET]: (state, action) => {
      const { type, detail, data } = action.payload;
      return state.set("navPageTarget", {
        type: type,
        detail: detail,
        data: data
      });
    },

    [SET_FIELD]: (state, action) => {
      return state.set("selectedField", action.payload);
    },
    [CLEAR_DATA]: (state, action) => {
      return initialState;
    },
    [SET_VENDORS]: (state, action) => {
      return state.set("selectedVendors", action.payload);
    },
    [SET_SATCS]: (state, action) => {
      return state.set("selectedSatcs", action.payload);
    },
    [SET_MODELS]: (state, action) => {
      return state.set("selectedModels", action.payload);
    },
    [SET_IS_EXIST_DEFAULT_USE_YN]: (state, action) => {
      return state.set("isExistDefaultUseYn", action.payload);
    },
    [SET_REGEX_RESULT]: (state, action) => {
      return state.set("regexResult", action.payload);
    },
    [SET_MATCH_INFO]: (state, action) => {
      return state.set("matchInfo", action.payload);
    }
  },
  initialState
);
