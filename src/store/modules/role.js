import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { pender } from 'redux-pender/lib/utils';

import * as roleApi from '../api/role';
import * as userApi from '../api/user';
import * as menuApi from '../api/menu';
import * as progApi from '../api/prog';

//action type
const GET_ROLES = 'roles/GET_ROLES';
const GET_ROLE_BY_SEQ = 'roles/GET_ROLE_BY_SEQ';
const CREATE_ROLE = 'roles/CREATE_ROLE';
const UPDATE_ROLE = 'roles/UPDATE_ROLE';
const REMOVE_ROLE_BY_SEQ = 'roles/REMOVE_ROLE_BY_SEQ';

const GET_USERS = 'users/GET_USERS';
const GET_USERS_BY_SEQ = 'roles/GET_USERS_BY_SEQ';
const REMOVE_USERS_BY_SEQ = 'roles/REMOVE_USERS_BY_SEQ';
const UPDATE_USER_ROLE_BY_SEQ = 'roles/UPDATE_USER_ROLE_BY_SEQ';

const GET_MENUS = 'menus/GET_MENUS';
const GET_MENUS_BY_SEQ = 'roles/GET_MENUS_BY_SEQ';
const UPDATE_MENU_ROLE_BY_SEQ = 'roles/UPDATE_MENU_ROLE_BY_SEQ';

const GET_PROGS = 'programs/GET_PROGS';
const GET_PROGS_BY_SEQ = 'roles/GET_PROGS_BY_SEQ';
const UPDATE_PROG_ROLE_BY_SEQ = 'roles/UPDATE_PROG_ROLE_BY_SEQ';

//action creator(action)
export const getRoles = createAction(GET_ROLES, roleApi.getRoles);
export const getRolesBySeq = createAction(
  GET_ROLE_BY_SEQ,
  roleApi.getRolesBySeq
);
export const createRole = createAction(CREATE_ROLE, roleApi.createRole);
export const updateRole = createAction(UPDATE_ROLE, roleApi.updateRole);
export const removeRoleBySeq = createAction(
  REMOVE_ROLE_BY_SEQ,
  roleApi.removeRole
);

export const getUsers = createAction(GET_USERS, userApi.getUsers);
export const getUsersBySeq = createAction(
  GET_USERS_BY_SEQ,
  roleApi.getUsersBySeq
);
export const removeUsersBySeq = createAction(
  REMOVE_USERS_BY_SEQ,
  roleApi.removeUsersBySeq
);
export const updateUserRoleBySeq = createAction(
  UPDATE_USER_ROLE_BY_SEQ,
  roleApi.updateUserRoleBySeq
);

export const getMenus = createAction(GET_MENUS, menuApi.getMenus);
export const getMenusBySeq = createAction(
  GET_MENUS_BY_SEQ,
  roleApi.getMenusBySeq
);
export const updateMenuRoleBySeq = createAction(
  UPDATE_MENU_ROLE_BY_SEQ,
  roleApi.updateMenuRoleBySeq
);

export const getProgs = createAction(GET_PROGS, progApi.getProgs);
export const getProgsBySeq = createAction(
  GET_PROGS_BY_SEQ,
  roleApi.getProgsBySeq
);
export const updateProgRoleBySeq = createAction(
  UPDATE_PROG_ROLE_BY_SEQ,
  roleApi.updateProgRoleBySeq
);

//inital state
const initialState = Map({
  roles: {
    content: [],
  },

  rolesBySeq: {
    roleId: '',
    roleNm: '',
    roleDesc: '',
    sort: '',
  },

  users: {
    content: [],
  },

  usersBySeq: {
    content: [],
  },

  menusBySeq: {
    content: [],
  },

  progsBySeq: {
    content: [],
  },

  isSuccess: 0,
  result: 0,
  loading: false,
});

//action handler(reducer)
export default handleActions(
  {
    // ROLE
    ...pender({
      type: GET_ROLES,
      onSuccess: (state, action) =>
        state.set('roles', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: GET_ROLE_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('rolesBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: CREATE_ROLE,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: UPDATE_ROLE,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: REMOVE_ROLE_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    // USERS
    ...pender({
      type: GET_USERS,
      onSuccess: (state, action) =>
        state.set('users', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: GET_USERS_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('usersBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: REMOVE_USERS_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: UPDATE_USER_ROLE_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    // MENUS

    ...pender({
      type: GET_MENUS_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('menusBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: UPDATE_MENU_ROLE_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    // PROGRAMS
    ...pender({
      type: GET_PROGS_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('progsBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: UPDATE_PROG_ROLE_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
  },
  initialState
);
