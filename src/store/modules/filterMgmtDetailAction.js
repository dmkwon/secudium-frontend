import { createAction, handleActions } from 'redux-actions';
import { pender } from 'redux-pender';
import { /*fromJS,*/ Map } from 'immutable';

import * as api from '../api/filter';

import { isDebug } from '../../components/FilterMgmt/const/filterConfig';

// action type 정의
const actionType = {
  GET_ONE_FILTER: 'filterMgmtDetail/GET_ONE_FILTER', // 필터 정보 하나

  GET_ALL_FILTER: 'filterMgmtDetail/GET_ALL_FILTER', // 필터 정보 하나
  GET_FILTER_HIST: 'filterMgmtDetail/GET_FILTER_HIST', // 필터의 히스토리
  GET_FILTER_CNDTN: 'filterMgmtDetail/GET_FILTER_CNDTN', // 필터의 필터 조건
  GET_FILTER_RULES: 'filterMgmtDetail/GET_FILTER_RULES', // 필터의 연관된 롤 리스트
};

// action 정의
const action = {
  getFilter: createAction(actionType.GET_ONE_FILTER, api.getFilterBySeq),

  allData: createAction(actionType.GET_ALL_FILTER, api.getFilterBySeq),
  getFilterHist: createAction(actionType.GET_FILTER_HIST, api.getFilterHist),
  getFilterCndtn: createAction(actionType.GET_FILTER_CNDTN, api.getFilterCndtn),
  getFilterRules: createAction(
    actionType.GET_FILTER_RULES,
    api.getRulesByFilterSeq
  ),
};

// 상태 초기값
const initialState = Map({
  filterInfo: {},
  filterHist: null,
  filterRules: null,
  filterCndtn: null,
  isSuccess: false,
});

// reducer 정의하기
export default handleActions(
  {
    ...pender({
      type: actionType.GET_ONE_FILTER,
      onSuccess: (state, action) => {
        isDebug && console.log('actionType.GET_ONE_FILTER: ', state, action);
        return state.set('filterInfo', action.payload.data);
      },
    }),
    ...pender({
      type: actionType.GET_ALL_FILTER,
      onSuccess: (state, action) => {
        isDebug && console.log('actionType.GET_ALL_FILTER: ', state, action);
        return state.set('filterInfo', action.payload.data);
      },
    }),
    ...pender({
      type: actionType.GET_FILTER_HIST,
      onSuccess: (state, action) => {
        isDebug && console.log('actionType.GET_FILTER_HIST: ', state, action);
        return state.set('filterHist', action.payload.data);
      },
    }),
    ...pender({
      type: actionType.GET_FILTER_CNDTN,
      onSuccess: (state, action) => {
        isDebug && console.log('actionType.GET_FILTER_CNDTN: ', state, action);
        return state.set('filterCndtn', action.payload.data);
      },
    }),
    ...pender({
      type: actionType.GET_FILTER_RULES,
      onSuccess: (state, action) => {
        isDebug && console.log('actionType.GET_FILTER_RULES: ', state, action);
        return state.set('filterRules', action.payload.data);
      },
    }),
  },
  initialState
);

export { action };
