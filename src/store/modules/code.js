import { createAction, handleActions } from 'redux-actions';
import { Map, fromJS } from 'immutable';
import { pender } from 'redux-pender/lib/utils';

import * as api from '../api/code';

//action type
const GET_COM_CODES = 'comcode/GET_COM_CODES';
const GET_COM_CODES_BY_SEQ = 'comcode/GET_COM_CODES_BY_SEQ';
const GET_CODES_BY_PARENT_CODE = 'comcode/GET_CODES_BY_PARENT_CODE';
const GET_CODES_BY_CODE = 'comcode/GET_CODES_BY_CODE';
const CREATE_CODES = 'comcode/CREATE_CODES';
const UPDATE_CODES = 'comcode/UPDATE_CODES';
const SET_CODE_BY_SEQ = 'comcode/SET_CODE_BY_SEQ';
const REMOVE_COM_CODE = 'comcode/REMOVE_COM_CODE';
const REMOVE_COM_CODES = 'comcode/REMOVE_COM_CODES';

//action creator(action)
export const getComCodes = createAction(GET_COM_CODES, api.getComCodes);
export const getComCodeBySeq = createAction(
  GET_COM_CODES_BY_SEQ,
  api.getComCodeBySeq
);
export const getCodesByParentCode = createAction(
  GET_CODES_BY_PARENT_CODE,
  api.getCodesByParentCode
);
export const getCodesByCode = createAction(
  GET_CODES_BY_CODE,
  api.getCodesByCode
);
export const createCodes = createAction(CREATE_CODES, api.createCode);
export const updateCodes = createAction(UPDATE_CODES, api.updateCode);
export const removeComCode = createAction(REMOVE_COM_CODE, api.removeComCode);
export const removeComCodes = createAction(
  REMOVE_COM_CODES,
  api.removeComCodes
);

export const setCodeBySeq = createAction(SET_CODE_BY_SEQ);

//inital state
const initialState = Map({
  comCodes: {
    content: [],
  },

  codesBySeq: Map({
    topCommCode: '',
    commCode: '',
    commCodeNmKo: '',
    commCodeNmEn: '',
    commCodeDesc: '',
    commCodeSortOrder: '',
    useYn: '',
  }),

  codesByParentCode: Map({
    topCommCode: '',
    commCode: '',
    commCodeNmKo: '',
    commCodeNmEn: '',
    commCodeDesc: '',
    commCodeSortOrder: '',
    useYn: '',
  }),

  isDisabled: 0,
  loading: false,
  result: 0,
});

export default handleActions(
  {
    ...pender({
      type: GET_COM_CODES,
      onSuccess: (state, action) =>
        state.set('comCodes', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: GET_COM_CODES_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('codesBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: GET_CODES_BY_PARENT_CODE,
      onSuccess: (state, action) =>
        state
          .set('codesByParentCode', fromJS(action.payload.data))
          .set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: GET_CODES_BY_CODE,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: CREATE_CODES,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: UPDATE_CODES,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: REMOVE_COM_CODE,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
    ...pender({
      type: REMOVE_COM_CODES,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
  },
  initialState
);
