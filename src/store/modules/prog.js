import { createAction, handleActions } from "redux-actions";
import { Map } from "immutable";
import { pender } from "redux-pender/lib/utils";

import * as api from "../api/prog";

//action type
const GET_PROGS = "programs/GET_PROGS";
const GET_PROG_BY_SEQ = "programs/GET_PROG_BY_SEQ";
const CREATE_PROG = "programs/CREATE_PROG";
const UPDATE_PROG = "programs/UPDATE_PROG";
const REMOVE_PROG = "programs/REMOVE_PROG";
const REMOVE_PROGS = "programs/REMOVE_PROGS";

//action creator(action)
export const getProgs = createAction(GET_PROGS, api.getProgs);
export const getProgBySeq = createAction(GET_PROG_BY_SEQ, api.getProgBySeq);
export const createProg = createAction(CREATE_PROG, api.createProg);
export const updateProg = createAction(UPDATE_PROG, api.updateProg);
export const removeProg = createAction(REMOVE_PROG, api.removeProg);
export const removeProgs = createAction(REMOVE_PROGS, api.removeProgs);

//inital state
const initialState = Map({
  progs: {
    content: []
  },

  progBySeq: {
    funcTypeCode: "",
    funcNmKo: "",
    funcNmEn: "",
    funcDesc: "",
    funcUrl: "",
    httpTrnsType: "",
    useYn: ""
  },

  isDisabled: "",
  isSuccess: 0,
  loading: false,
  result: 0
});

//action handler(reducer)
export default handleActions(
  {
    ...pender({
      type: GET_PROGS,
      onSuccess: (state, action) =>
        state.set("progs", action.payload.data).set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: (state, action) => state.set("loading", false)
    }),
    ...pender({
      type: GET_PROG_BY_SEQ,
      onSuccess: (state, action) =>
        state.set("progBySeq", action.payload.data).set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: (state, action) => state.set("loading", false)
    }),
    ...pender({
      type: CREATE_PROG,
      onSuccess: (state, action) =>
        state.set("result", action.payload.data).set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: (state, action) => state.set("loading", false)
    }),
    ...pender({
      type: UPDATE_PROG,
      onSuccess: (state, action) =>
        state.set("result", action.payload.data).set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: (state, action) => state.set("loading", false)
    }),

    ...pender({
      type: REMOVE_PROG,
      onSuccess: (state, action) =>
        state.set("result", action.payload.data).set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: (state, action) => state.set("loading", false)
    }),

    ...pender({
      type: REMOVE_PROGS,
      onSuccess: (state, action) =>
        state.set("result", action.payload.data).set("loading", false),
      onPending: (state, action) => state.set("loading", true),
      onFailure: (state, action) => state.set("loading", false)
    })
  },
  initialState
);
