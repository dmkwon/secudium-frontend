import { createAction, handleActions } from 'redux-actions';
import { Map, fromJS } from 'immutable';
import { pender } from 'redux-pender/lib/utils';

import * as api from '../api/menu';

//action type
const GET_MENUS = 'menus/GET_MENUS';
const GET_MENUS_BY_SEQ = 'menus/GET_MENUS_BY_SEQ';
const GET_MENUS_BY_STEP = 'menus/GET_MENUS_BY_STEP';
const CREATE_MENUS = 'menus/CREATE_MENUS';
const UPDATE_MENUS = 'menus/UPDATE_MENUS';
const REMOVE_MENUS = 'menus/REMOVE_MENUS';
const REMOVE_MENU = 'menus/REMOVE_MENU';

//action creator(action)
export const getMenus = createAction(GET_MENUS, api.getMenus);
export const getMenusBySeq = createAction(GET_MENUS_BY_SEQ, api.getMenusBySeq);
export const getMenusByStep = createAction(GET_MENUS_BY_STEP);
export const createMenus = createAction(CREATE_MENUS, api.createMenu);
export const updateMenus = createAction(UPDATE_MENUS, api.updateMenu);
export const removeMenus = createAction(REMOVE_MENUS, api.removeMenus);
export const removeMenu = createAction(REMOVE_MENU, api.removeMenu);

//inital state
const initialState = Map({
  menu: {
    content: [],
  },

  menuBySeq: Map({
    menuSeq: '',
    menuId: '',
    menuNm: '',
    url: '',
    sort: '',
    useYn: '',
    menuDesc: '',
  }),

  menuByStep: Map({
    parentSeq: '',
    menuSeq: '',
    menuId: '',
    menuNm: '',
    url: '',
    sort: '',
    useYn: '',
    menuDesc: '',
  }),

  result: 0,
  loading: false,
});

//action handler(reducer)
export default handleActions(
  {
    ...pender({
      type: GET_MENUS,
      onSuccess: (state, action) =>
        state.set('menu', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: GET_MENUS_BY_SEQ,
      onSuccess: (state, action) =>
        state.set('menuBySeq', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: GET_MENUS_BY_STEP,
      onSuccess: (state, action) =>
        state.set('menuByStep', fromJS(action.payload.data)),
      onFailure: state => state.set('loading', false),
    }),

    ...pender({
      type: CREATE_MENUS,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: UPDATE_MENUS,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: REMOVE_MENU,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),

    ...pender({
      type: REMOVE_MENUS,
      onSuccess: (state, action) =>
        state.set('result', action.payload.data).set('loading', false),
      onPending: (state, action) => state.set('loading', true),
      onFailure: (state, action) => state.set('loading', false),
    }),
  },
  initialState
);
