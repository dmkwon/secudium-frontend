import nmlzRule from './nmlzRule';
import rule from './rule';
import common from './common';
import blackcc from './blackcc';
import code from './code';
import auditlog from './auditlog';
import role from './role';
import threattaxonomy from './threattaxonomy';
import menu from './menu';
import prog from './prog';
import user from './user';
import field from './field';
import filterPopup from './filterPopup';
import ruleDist from './ruleDist';

import engineEnv from './engineEnv';

import filterMgmtList from './filterMgmtListAction';
import filterMgmtDetail from './filterMgmtDetailAction';
import zpNodeMgmt from './enginOptionMgmt';

import { combineReducers } from 'redux';

const reducers = combineReducers({
  common,
  nmlzRule,
  rule,
  blackcc,
  role,
  threattaxonomy,
  menu,
  prog,
  user,
  code,
  auditlog,
  field,
  engineEnv, // 엔진 환경 설정
  filterMgmtList,
  filterMgmtDetail,
  zpNodeMgmt,
  filterPopup,
  ruleDist,
});

export default reducers;
