import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { pender } from 'redux-pender/lib/utils';

import * as api from '../api/rule';

//action type
const GET_RULES = 'rule/GET_RULES';
const GET_RULES_BY_HISTSEQ = 'rule/GET_RULES_BY_HISTSEQ';

const GET_RULE = 'rule/GET_RULE';

const GET_RULE_HISTORIES = 'rule/GET_RULE_HISTORIES';
const GET_RULE_HISTORY = 'rule/GET_RULE_HISTORY';
const GET_LATEST_RULE_HISTORY = 'rule/GET_LATEST_RULE_HISTORY';

const INSERT_RULE = 'rule/INSERT_RULE';
const UPDATE_RULE = 'rule/UPDATE_RULE';
const REVERT_RULE = 'rule/REVERT_RULE';

const ROLLBACK_RULE = 'rule/ROLLBACK_RULE';

const DELETE_RULE = 'rule/DELETE_RULE';
const DELETE_RULE_BIN = 'rule/DELETE_RULE_BIN';

const INIT_RULE_STATE = 'rule/INIT_RULE_STATE';

const SET_THREAT_TAXO = 'rule/SET_THREAT_TAXO';

const DIST_RULES = 'rule/DIST_RULES';

//action creator(action)
export const getRules = createAction(GET_RULES, api.getRules);
export const getRulesByThreatClasSeq = createAction(
  GET_RULES_BY_HISTSEQ,
  api.getRulesByThreatClasSeq
);

export const getRule = createAction(GET_RULE, api.getRule);
export const revertRule = createAction(REVERT_RULE, api.revertRule);
export const rollbackRule = createAction(ROLLBACK_RULE, api.rollbackRule);

export const getRuleHistories = createAction(
  GET_RULE_HISTORIES,
  api.getRuleHistories
);
export const getRuleHistory = createAction(
  GET_RULE_HISTORY,
  api.getRuleHistory
);
export const getLatestRuleHistory = createAction(
  GET_LATEST_RULE_HISTORY,
  api.getLatestRuleHistory
);

export const insertRule = createAction(INSERT_RULE, api.insertRule);
export const updateRule = createAction(UPDATE_RULE, api.updateRule);
export const deleteRule = createAction(DELETE_RULE, api.deleteRule);
export const deleteRuleFromBin = createAction(
  DELETE_RULE_BIN,
  api.deleteRuleFromBin
);

export const setThreatTaxo = createAction(SET_THREAT_TAXO, api.setThreatTaxo);

export const distRules = createAction(DIST_RULES, api.distRules);

export const initRuleState = createAction(INIT_RULE_STATE);

//inital state
const initialState = Map({
  rules: [],
  ruleHists: [],
  ruleHist: {
    ruleChgHistSeq: '',
    ruleSeq: '',
    ruleNm: '',
    ruleType: '',
    ruleCommitStrDt: '',
    ruleCommitEndDt: '',
    ruleCommitStrTime: '',
    ruleCommitEndTime: '',
    ruleCommitWeek: '',
    thrsldGroupField: '',
    thrsldGroupPeriod: '',
    thrsldGroupCnt: '',
    thrsldDstnctField: '',
    thrsldDstnctCnt: '',
    thrsldOprtrPeriod: '',
    thrsldOprtr: '',
    detectCndtn: '',
    droolsCode: '',
    relshpRule: '',
    cntrlTargt: '',
    custmNm: '',
    ruleDesc: '',
    commitYn: '',
    ruleChgReason: '',
    filters: [],
  },
  loading: false,
});

//action handler(reducer)
export default handleActions(
  {
    ...pender({
      type: GET_RULES,
      onPending: state => state.set('loading', true),
      onSuccess: (state, action) =>
        state.set('rules', action.payload.data).set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_RULES_BY_HISTSEQ,
      onPending: state => state.set('loading', true),
      onSuccess: (state, action) =>
        state.set('rules', action.payload.data).set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: DELETE_RULE,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_RULE,
      onPending: state => state.set('loading', true),
      onSuccess: (state, action) =>
        state.set('rule', action.payload.data).set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_RULE_HISTORIES,
      onPending: state => state.set('loading', true),
      onSuccess: (state, action) =>
        state.set('ruleHists', action.payload.data).set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_RULE_HISTORY,
      onPending: state => state.set('loading', true),
      onSuccess: (state, action) =>
        state.set('ruleHist', action.payload.data).set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: GET_LATEST_RULE_HISTORY,
      onPending: state => state.set('loading', true),
      onSuccess: (state, action) =>
        state.set('ruleHist', action.payload.data).set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: INSERT_RULE,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: UPDATE_RULE,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: REVERT_RULE,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: ROLLBACK_RULE,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: SET_THREAT_TAXO,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: DELETE_RULE_BIN,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: DIST_RULES,
      onPending: state => state.set('loading', true),
      onSuccess: state => state.set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
    ...pender({
      type: INIT_RULE_STATE,
      onPending: state => state.set('loading', true),
      onSuccess: state =>
        state
          .set('ruleHist', initialState.get('ruleHist'))
          .set('loading', false),
      onFailure: state => state.set('loading', false),
    }),
  },
  initialState
);
