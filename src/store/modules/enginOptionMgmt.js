import { createAction, handleActions } from 'redux-actions';
import * as api from '../api/enginOptionMgmt';
import { pender } from 'redux-pender/lib/utils';
import { Map } from 'immutable';

// action type 정의
const actionType = {
  GET_NODELIST: 'enginOptionMgmt/GET_NODELIST',
  GET_CHILDNODE: 'enginOptionMgmt/GET_CHILDNODE',
  GET_NODEDATA: 'enginOptionMgmt/GET_NODEDATA',
  PUT_NODEDATA: 'enginOptionMgmt/PUT_NODEDATA',
  GET_INITDATA: 'enginOptionMgmt/GET_INITDATA',
  PUT_ZKDATA: 'enginOptionMgmt/PUT_ZKDATA',
};

// action 정의
const action = {
  getRootNodeList: createAction(actionType.GET_NODELIST, api.getNodeList),
  getChildNode: createAction(actionType.GET_CHILDNODE, api.getChildNodeList),
  getNodeData: createAction(actionType.GET_NODEDATA, api.getData),
  putNodeData: createAction(actionType.PUT_NODEDATA, api.putData),
  getInitData: createAction(actionType.GET_INITDATA, api.getInitData),
  pudZKData: createAction(actionType.PUT_ZKDATA, api.pudZKData),
};

const initState = Map({
  baseNodePath: '',
  nodeList: [
    {
      nodePath: 'app',
      hasHeader: false,
      userDefineHdr: '',
      hasData: false,
      hasSubNodes: false,
      subNode: [],
    },
  ],
  isSuccess: false,
});

// reducer 정의하기
export default handleActions(
  {
    ...pender({
      type: actionType.GET_INITDATA,
      onSuccess: (state, action) => state.set('isSuccess', true),
    }),
    ...pender({
      type: actionType.GET_CHILDNODE,
      onSuccess: (state, action) => state.set('isSuccess', true),
    }),
    ...pender({
      type: actionType.GET_NODEDATA,
      onSuccess: (state, action) => {
        return state.set('isSuccess', true);
      },
    }),
    ...pender({
      type: actionType.PUT_NODEDATA,
      onSuccess: (state, action) => state.set('isSuccess', true),
    }),
    ...pender({
      type: actionType.PUT_ZKDATA,
      onSuccess: (state, action) => state.set('isSuccess', true),
    }),
  },
  initState
);

export { action };
