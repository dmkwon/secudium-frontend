import { createAction, handleActions } from 'redux-actions';
import { pender } from 'redux-pender/lib/utils';

import * as api from '../api/auth';

//action type
const SIGN_IN = 'auditlog/SIGN_IN';

//action creator(action)
export const signIn = createAction(SIGN_IN, api.signIn);

//action handler(reducer)
export default handleActions(
  {
    ...pender({
      type: SIGN_IN,
      onSuccess: state => state.set('loading', false),
      onPending: state => state.set('loading', true),
      onFailure: state => state.set('loading', false),
    }),
  },
  {}
);
