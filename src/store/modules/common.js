import _ from "@lodash";
import { createAction, handleActions } from "redux-actions";
import { Map } from "immutable";
import { pender } from "redux-pender";

import * as authApi from "./../api/auth";
import * as commonApi from "./../api/common";

const SET_LANGUAGE = "common/SET_LANGUAGE";
const GET_MENUS = "common/GET_MENUS";
const GET_VENDORS = "common/GET_VENDORS";
const GET_SATC_OF_MODEL = "common/GET_SATC_OF_MODEL";
const GET_SATC_OF_MODEL_BY_VENDORIDS = "common/GET_SATC_OF_MODEL_BY_VENDORIDS";
const GET_MODELS_OF_SATC = "common/GET_MODELS_OF_SATC";
const GET_MODELS_OF_SATC_BY_VENDORIDS =
  "common/GET_MODELS_OF_SATC_BY_VENDORIDS";

const OPEN_COMMON_ALERT = "common/OPEN_COMMON_ALERT";
const CLOSE_COMMON_ALERT = "common/CLOSE_COMMON_ALERT";
const CLEAR_DATA = "common/CLEAR_DATA";
const CLEAR_SATCS = "common/CLEAR_SATCS";
const CLEAR_MODELS = "common/CLEAR_MODELS";

//action creator(action)
export const setLanguage = createAction(SET_LANGUAGE, commonApi.setLanguage);
export const getMenus = createAction(GET_MENUS, authApi.getMenus);

export const getVendors = createAction(GET_VENDORS, commonApi.getVendors);
export const getSatcOfModel = createAction(
  GET_SATC_OF_MODEL,
  commonApi.getSatcOfModel
);
export const getSatcOfModelByVendorIds = createAction(
  GET_SATC_OF_MODEL_BY_VENDORIDS,
  commonApi.getSatcOfModelByVendorIds
);

export const getModelsOfSatc = createAction(
  GET_MODELS_OF_SATC,
  commonApi.getModelsOfSatc
);
export const getModelsOfSatcByVendorIds = createAction(
  GET_MODELS_OF_SATC_BY_VENDORIDS,
  commonApi.getModelsOfSatcByVendorIds
);

export const openCommonAlert = createAction(OPEN_COMMON_ALERT);
export const closeCommonAlert = createAction(CLOSE_COMMON_ALERT);
export const clearData = createAction(CLEAR_DATA);
export const clearSatcs = createAction(CLEAR_SATCS);
export const clearModels = createAction(CLEAR_MODELS);

const initialState = Map({
  language:
    localStorage.getItem("LANG") === undefined ||
    localStorage.getItem("LANG") === null
      ? "ko"
      : localStorage.getItem("LANG"),
  languages: [
    { value: "ko", text: "Korean" },
    { value: "en", text: "English" }
  ],
  menu: [],
  code: [],
  vendors: [],
  satcOfModel: [],
  modelsOfSatc: [],
  satcOfModelByVendorIds: [],
  modelsOfSatcByVendorIds: [],
  commonAlertPopup: false,
  commonAlertCode: ""
});

export default handleActions(
  {
    ...pender({
      type: GET_MENUS,
      onSuccess: (state, action) => {
        return state.set("menu", action.payload.data);
      }
    }),
    ...pender({
      type: GET_VENDORS,
      onSuccess: (state, action) => {
        let newOne = [];
        const vendors = action.payload.data;
        newOne = _.concat({ value: "0", text: "ALL" }, vendors);
        return state.set("vendors", newOne);
      },
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_SATC_OF_MODEL,
      onSuccess: (state, action) => {
        return state.set("satcOfModel", action.payload.data);
      },
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_SATC_OF_MODEL_BY_VENDORIDS,
      onSuccess: (state, action) => {
        let newOne = [];
        const satcs = action.payload.data;
        newOne = _.concat({ value: "ALL", text: "ALL" }, satcs);
        return state.set("satcOfModelByVendorIds", newOne);
      },
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_MODELS_OF_SATC,
      onSuccess: (state, action) => {
        return state.set("modelsOfSatc", action.payload.data);
      },
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: GET_MODELS_OF_SATC_BY_VENDORIDS,
      onSuccess: (state, action) => {
        return state.set("modelsOfSatcByVendorIds", action.payload.data);
      },
      onFailure: state => state.set("loading", false)
    }),
    ...pender({
      type: SET_LANGUAGE,
      onSuccess: (state, action) => {
        localStorage.setItem("LANG", action.payload.data);
        return state.set("language", action.payload.data);
      },
      onFailure: state => state.set("loading", false)
    }),
    [OPEN_COMMON_ALERT]: (state, action) =>
      state
        .set("commonAlertPopup", true)
        .set("commonAlertCode", action.payload),
    [CLOSE_COMMON_ALERT]: state =>
      state
        .set("commonAlertPopup", false)
        .set("commonAlertCode", initialState.get("commonAlertCode")),
    [CLEAR_DATA]: (state, action) => {
      return state
        .set("vendors", [{ value: "0", text: "ALL" }])
        .set("satcOfModelByVendorIds", [{ value: "ALL", text: "ALL" }])
        .set("modelsOfSatcByVendorIds", []);
    },
    [CLEAR_SATCS]: (state, action) => {
      return state.set("satcOfModelByVendorIds", []);
    },
    [CLEAR_MODELS]: (state, action) => {
      return state.set("modelsOfSatcByVendorIds", []);
    }
  },
  initialState
);
