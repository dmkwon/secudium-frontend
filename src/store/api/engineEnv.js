import axios from "axios";
import {host} from "../../constants";

/** engine 환경 설정 정보  */
export const getEngineEnv = () => axios.get(`${host}/api/enginemgmt/engineenv`);
/** 접속 정보 수정 */
export const updateEngineEnv = (engineenv) => axios.put(`${host}/api/enginemgmt/engineenv`, engineenv);

/** command 목록 */
export const getEngineCommands = () => axios.get(`${host}/api/enginemgmt/engineenv/enginecommands`);
/** command 조회 */
export const getEngineCommand = (commandSeq) => axios.post(`${host}/api/enginemgmt/engineenv/enginecommandbycomseq`, {engineCmdSeq:commandSeq});
/** command 삽입 */
export const insertEngineCommand = (command) => axios.post(`${host}/api/enginemgmt/engineenv/enginecommand`, command);
/** command 수정 */
export const updateEngineCommand = (command) => axios.put(`${host}/api/enginemgmt/engineenv/enginecommand`, command);
/** command 삭제 */
export const deleteEngineCommand = (command) => axios.post(`${host}/api/enginemgmt/engineenv/enginecommand/delete`, {engineCmdSeq: command.engineCmdSeq});

/** userDefineHdr 목록 */
export const getUserDefineHdrs = () => axios.get(`${host}/api/enginemgmt/engineenv/userdefinehdr`);
/** userDefineHdr 삽입 */
export const insertUserDefineHdr = (userDefineHdr) => axios.post(`${host}/api/enginemgmt/engineenv/userdefinehdr`, userDefineHdr);
/** userDefineHdr 수정 */
export const updateUserDefineHdr = (userDefineHdr) => axios.put(`${host}/api/enginemgmt/engineenv/userdefinehdr`, userDefineHdr);
/** userDefineHdr 삭제 */
export const deleteUserDefineHdr = (userDefineHdr) => axios.post(`${host}/api/enginemgmt/engineenv/userdefinehdr/delete`, {userDefineHdrSeq : userDefineHdr.userDefineHdrSeq});