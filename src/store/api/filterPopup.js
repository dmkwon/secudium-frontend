import axios from "axios";
import {host} from "../../constants";

const base_url = `${host}/api/filtermgmt`;

export const getFilterFields = (query = "") => axios.get(`${base_url}/filterField?${query}`);

export const getFilterFieldTrees = (query = "") => axios.get(`${base_url}/filterFieldTree?${query}`);

export const getAgents = (query = "") => axios.get(`${base_url}/agent?${query}`);

export const getAgentModels = (query = "") => axios.get(`${base_url}/agentModel?${query}`);

export const getAgentModelTrees = (query = "") => axios.get(`${base_url}/agentModelTree?${query}`);

export const getAgentVendors = (query = "") => axios.get(`${base_url}/agentVendor?${query}`);

export const getCompanys = (query = "") => axios.get(`${base_url}/company?${query}`);

export const getCompanyTrees = (query = "") => axios.get(`${base_url}/companyTree?${query}`);
