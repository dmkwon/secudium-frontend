import axios from "axios";
import { host } from "./../../constants";

export const getFields = () => axios.get(`${host}/api/enginemgmt/field/all`);
export const getFieldHists = () => axios.get(`${host}/api/enginemgmt/field/history`);
export const rollback = (data) => axios.post(`${host}/api/enginemgmt/field/history/rollback`, data); 
export const getPrevFields = (seq) => axios.post(`${host}/api/enginemgmt/field`, {fieldHistSeq : seq});
export const insertFieldHist = (data) => axios.post(`${host}/api/enginemgmt/field/history/insert`, data);
export const updateFieldHist = (data) => axios.put(`${host}/api/enginemgmt/field/history/update`, data);
export const deleteFieldHist = (data) => axios.put(`${host}/api/enginemgmt/field/history/delete`, data);

export const isDuplicate = (value) => {
  let url = `${host}/api/enginemgmt/field/isduplicate?value=${value}`;
  var request = new XMLHttpRequest();
  request.open('GET', url, false);  // `false` makes the request synchronous
  request.withCredentials = true;
  request.send(null);
  return request;
}

export const isDuplicateFieldId = (value, fieldSeq) => { 
  let url = `${host}/api/enginemgmt/field/isDuplicateFieldId?value=${value}`;
  if(fieldSeq !== '') {
    url += `&fieldSeq=${fieldSeq}`;
  }
  var request = new XMLHttpRequest();
  request.open('GET', url, false);  // `false` makes the request synchronous
  request.withCredentials = true;
  request.send(null);
  return request;
}