import axios from "axios";
import {host} from "./../../constants";

export const signIn = (userInfo) => axios.post(`${host}/api/auth/signin`, userInfo);
export const getUserInfo = () => axios.get(`${host}/api/auth/me`);
export const getMenus = () => axios.get(`${host}/api/auth/menus`);