import axios from 'axios';
import { host } from './../../constants';

const base_url = `${host}/api/filtermgmt`;

// filter group -------------------------------------------
const getFilterGroup = obj => axios.get(`${base_url}/group`, { params: obj });
const getFilterSubGroupBySeq = filterGroupSeq =>
  axios.get(`${base_url}/subgroup`, { params: { filterGroupSeq } });
const addFilterGroup = dto => axios.post(`${base_url}/group`, dto);
const updateFilterGroup = dto => axios.post(`${base_url}/group/update`, dto);

// filter 여러개 ------------------------------------------
const getFilters = paramsObj =>
  axios.get(`${base_url}/filters`, { params: paramsObj });
// filter 하나 --------------------------------------------
const getFilterBySeq = filterSeq =>
  axios.get(`${base_url}/filter`, { params: { filterSeq } });
const addFilter = (filter, cndtn = null) =>
  axios.post(`${base_url}/filter`, { filter, cndtn });
const updateFilter = (filter, cndtn = null) =>
  axios.post(`${base_url}/filter/update`, { filter, cndtn });
const updateFilterList = dtoList =>
  axios.post(`${base_url}/filterlist/update`, dtoList);

// filter 조건 --------------------------------------------
const getFilterCndtn = filterSeq =>
  axios.get(`${base_url}/filter/cndtn`, { params: { filterSeq } });
//const addFilterCndtn = (dtoList) => axios.post(`${base_url}/cndtn`, dtoList);

// filter change history ----------------------------------
const getFilterHist = filterSeq =>
  axios.get(`${base_url}/filter/hist`, { params: { filterSeq } });

// filter 상세 rule 정보 ----------------------------------
const getRulesByFilterSeq = (filterSeq, filterChgHistSeq) =>
  axios.post(`${base_url}/filter/rule`, { filterSeq, filterChgHistSeq });

// multi --------------------------------------------------
// 초기화 데이터를 생성하기 위해서, 필터 그룹과 필터 리스트를 동시에 가져오도록 구현
const getInitFilterListData = (paramFg, paramFs) =>
  axios.all([getFilterGroup(paramFg), getFilters(paramFs)]);

// 삭제, 수정, 추가 후 데이터 가져오는 부분까지 같이 처리 할 수 있도록 구현
const addFilterGroupAndGetData = (
  dto,
  paramFg // add, 그룹 정보만
) => axios.all([addFilterGroup(dto), getFilterGroup(paramFg)]);

const updateFilterGroupAndGetData = (
  dto,
  paramFg,
  paramFs // edit, 그룹와 필터 같이
) =>
  axios.all([
    updateFilterGroup(dto),
    getFilterGroup(paramFg),
    getFilters(paramFs),
  ]);

const updateFilterListAndGetData = (dtoList, paramFs) =>
  axios.all([updateFilterList(dtoList), getFilters(paramFs)]);

const getFilterDetailAllData = filterSeq =>
  axios.all([getFilterHist(filterSeq), getFilterCndtn(filterSeq)]);
//
export {
  getInitFilterListData,
  getFilterSubGroupBySeq,
  addFilterGroup,
  updateFilterGroup,
  addFilterGroupAndGetData,
  updateFilterGroupAndGetData,
  addFilter,
  updateFilter,
  updateFilterList,
  updateFilterListAndGetData,
  getFilterBySeq,
  getFilterDetailAllData,
  getFilterHist,
  getFilterCndtn,
  getRulesByFilterSeq,
};
