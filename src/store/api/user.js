import axios from "axios";
import { host } from "./../../constants";

export const getUsers = () => axios.get(`${host}/api/systemoper/users`);
export const getUsersBySeq = (seq) => axios.post(`${host}/api/systemoper/userbyseq`, {seq:seq});
export const getCustmGroup = () => axios.get(`${host}/api/systemoper/user/custm/group`);
export const getCoNmByCustmGroupSeq = (seq) => axios.post(`${host}/api/systemoper/user/custm/company`,{seq:seq});
export const createUser = (code) => axios.post(`${host}/api/systemoper/user`, code);
export const updateUser = (code) => axios.put(`${host}/api/systemoper/user`, code);
export const changePwd = (seq, code) => axios.post(`${host}/api/systemoper/user/changePwd`, {seq:seq, changePwd:code});
export const removeUsers = (paramList) => axios.put(`${host}/api/systemoper/users/delete`, paramList);
export const removeUser = (seq) => axios.post(`${host}/api/systemoper/user/delete`, {seq:seq});

export const isDuplicate = (usrId) => {
    let url = `${host}/api/systemoper/user/isduplicate?usrId=${usrId}`;
    var request = new XMLHttpRequest();
    request.open('GET', url, false);  // `false` makes the request synchronous
    request.withCredentials = true;
    request.send(null);
    return request;
}
