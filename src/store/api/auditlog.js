import axios from "axios";
import qs from "qs";
import {host} from "./../../constants";

export const getAuditLogs = (selectParam) => axios.get(`${host}/api/systemoper/auditLogs`, {
  params: {
    ...selectParam
  },
  paramsSerializer: (params) => qs.stringify(params, { arrayFormat: 'repeat' })   // field: [1, 2, 3] => field=1&field=2&field=3 
});
export const getAuditLogsBySeq = (auditLogSeq) => axios.post(`${host}/api/systemoper/auditLog`, {auditHistSeq : auditLogSeq});
