import axios from 'axios';
import { host } from '../../constants';

/** 룰 목록 조회 */
export const getRules = ({ keyword }) =>
  axios.get(`${host}/api/rulemgmt/rules/all?keyword=${keyword}`);
/** 룰 목록 조회(위협체계코드) */
export const getRulesByThreatClasSeq = ({ selectedKey, keyword }) =>
  axios.post(
    `${host}/api/rulemgmt/rules`, {threatClasSeq:selectedKey, keyword:keyword}
  );
/** 룰 조회 */
export const getRule = ruleSeq =>
  axios.post(`${host}/api/rulemgmt/rulebyseq`, {ruleSeq:ruleSeq});
/** 룰 삭제(휴지통으로 이동) */
export const deleteRule = rule =>
  axios.put(`${host}/api/rulemgmt/rule/delete`, rule);
/** 룰 삭제 (휴지통에서 삭제, useYn -> N) */
export const deleteRuleFromBin = rule =>
  axios.post(`${host}/api/rulemgmt/rule/delete/bin`, rule);
/** 룰 추가 */
export const insertRule = rule => axios.post(`${host}/api/rulemgmt/rule`, rule);
/** 룰 수정 */
export const updateRule = rule => axios.put(`${host}/api/rulemgmt/rule`, rule);
/** 룰 변경이력 목록 조회 */
export const getRuleHistories = ruleSeq =>
  axios.post(`${host}/api/rulemgmt/rules/histories`, {ruleSeq:ruleSeq});
/** 룰 변경이력 조회 */
export const getRuleHistory = ruleHistSeq =>
  axios.post(`${host}/api/rulemgmt/rule/history`, {ruleHistSeq: ruleHistSeq});
/** 룰 최신 변경이력 조회 */
export const getLatestRuleHistory = ruleSeq =>
  axios.post(`${host}/api/rulemgmt/rule/history/latest`, {ruleSeq:ruleSeq});
/** 룰 롤백 */
export const rollbackRule = rule =>
  axios.post(`${host}/api/rulemgmt/rule/history/rollback`, rule);
/** 룰 복원(휴지통 -> 미분류) */
export const revertRule = rule =>
  axios.post(`${host}/api/rulemgmt/rule/revert`, rule);
/** 룰 위협체계 등록 */
export const setThreatTaxo = rule =>
  axios.post(`${host}/api/rulemgmt/rule/threattaxo`, rule);
/** 룰 배포 */
export const distRules = params =>
  axios.post(`${host}/api/rulemgmt/rule/zkdist`, params);
