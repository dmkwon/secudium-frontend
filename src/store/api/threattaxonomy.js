import axios from "axios";
import {host} from "../../constants";


/** 위협체계 초기데이터 조회 */
export const getTopNode = () => axios.get(`${host}/api/rulemgmt/threats/top`);
/** 위협체계 하위데이터 조회 */
export const getSubNodes = (parentSeq) => axios.post(`${host}/api/rulemgmt/threats/sub`, {tpThrtSeq:parentSeq});
/** 위협체계 단건 조회 */
export const getNodeData = (seq) => axios.post(`${host}/api/rulemgmt/threats/sel`, {thrtSeq:seq});
/** 룰 목록 조회 */
export const getRule = (seq) => axios.post(`${host}/api/rulemgmt/threats/rule`, {thrtSeq:seq});
/** 위협체계 등록 */
export const insertTaxo = (data) => axios.post(`${host}/api/rulemgmt/threats/isrt`, data);
/** 위협체계 수정 */
export const updateTaxo = (data) => axios.put(`${host}/api/rulemgmt/threats/updt`, data);

/** 위협체계 수정(분류체계 변경) */
export const updateTaxoHier = (data) => axios.put(`${host}/api/rulemgmt/threats/updt/hier`, data);

/** 위협체계 수정(분류체계 변경 시 원본노드 하위체계 확인) */
export const checkSubNodes = (data) => axios.post(`${host}/api/rulemgmt/threats/updt/hier/sub`, data);

/** 위협체계 수정(분류체계 변경 시 하위노드 수정) */
export const updateSubNodes = (data) => axios.put(`${host}/api/rulemgmt/threats/updt/hier/subupdt`, data);

/** 위협체계 삭제 */
export const deleteTaxo = (data) => axios.put(`${host}/api/rulemgmt/threats/delt`, data);

/** 룰 정보 조회 시 위협체계 조회 */
export const getTaxo = (ruleSeq) => axios.post(`${host}/api/rulemgmt/threats/taxo`, {ruleSeq:ruleSeq});