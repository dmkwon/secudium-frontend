import axios from "axios";
import {host} from "../../constants";

/** IP 목록 조회 */
export const getIpList = (selectParam) => axios.get(`${host}/api/enginemgmt/blackcc/ips`, {params: selectParam});
/** IP 단건 조회 */
// export const getIp = (seq) => axios.get(`${host}/api/enginemgmt/blackcc/ip?seq=${seq}`); //미사용
/** IP 등록 */
export const postIp = (postData) => axios.post(`${host}/api/enginemgmt/blackcc/ips`, postData );
/** IP 수정 */
export const putIp = (putData) => axios.put(`${host}/api/enginemgmt/blackcc/ip`, putData );
/** IP 선택 삭제 */
export const deleteIps = (paramList) => axios.put(`${host}/api/enginemgmt/blackcc/ips`, paramList );

/** IP 등록 범위 중복조회 */
export const getIpsDupl = ( selectParam ) => axios.get(`${host}/api/enginemgmt/blackcc/ips/isDuplicated`, {params: selectParam});
/** IP 수정 범위 중복조회 */
export const getIpsDuplUpdate = ( selectParam ) => axios.get(`${host}/api/enginemgmt/blackcc/ips/upDuplicated`, {params: selectParam} );
/** IP 등록 단건 중복조회 */
export const getIpDupl = ( selectParam ) => axios.get(`${host}/api/enginemgmt/blackcc/ip/isDuplicated`, {params: selectParam} );
/** IP 수정 단건 중복조회 */
export const getIpDuplUpdate = ( selectParam ) => axios.get(`${host}/api/enginemgmt/blackcc/ip/upDuplicated`, {params: selectParam});

/** URL 목록 조회 */
export const getUrlList = (selectParam) => axios.get(`${host}/api/enginemgmt/blackcc/urls`, {params: selectParam});
/** URL 단건 조회 */
// export const getUrl = () => axios.get(`${host}/api/enginemgmt/blackcc/url`);     // 미사용
/** URL 등록 */
export const postUrl = (postData) => axios.post(`${host}/api/enginemgmt/blackcc/urls`, postData );
/** URL 수정 */
export const putUrl = (putData) => axios.put(`${host}/api/enginemgmt/blackcc/url`, putData );
/** URL 삭제 */
export const deleteUrls = (paramList) => axios.put(`${host}/api/enginemgmt/blackcc/urls`, paramList );
/** Black/C2 배포 */
export const deployAll = () => axios.post(`${host}/api/enginemgmt/blackcc/deployAll`);