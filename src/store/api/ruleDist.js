import axios from "axios";
import {host} from "./../../constants";

/**
 * 룰 배포이력 조회
 */
export const getRuleDistHistories = () => axios.get(`${host}/api/rulemgmt/rule/disthist/all`);
/**
 * 룰 배포이력 상세
 * @param {*} histSeq 이력번호
 * @param {*} keyword 검색어
 * @param {*} showChgRule 변경룰조회유무
 */
export const getRuleDistDetail = (histSeq, keyword, diffYn) => 
  axios.post(`${host}/api/rulemgmt/rule/disthist/detail`, {ruleDstrbtHistSeq:histSeq, keyword:keyword, diffYn:diffYn});
export const getLatestRuleDistDetail = () => axios.get(`${host}/api/rulemgmt/rule/disthist/detail/latest`);
/**
 * 룰 롤백
 * @param {*} params RuleDistHistoryDto
 */
export const rollbackDistHistory = (params) => axios.post(`${host}/api/rulemgmt/rule/disthist/rollback`, params);
