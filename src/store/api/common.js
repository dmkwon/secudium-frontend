import axios from "axios";
import { host } from "./../../constants";

export const getMenu = () => axios.get(`${host}/api/menus`);
/**
 * 중복검사
 * @param {string} tableName
 * @param {string} colName
 * @param {string} value
 * @returns boolean
 */
export const isDuplicate = (tableName, colName, value) =>
  axios.get(`${host}/api/isduplicate`, {
    params: {
      tableName: tableName,
      colName: colName,
      value: value
    }
  });

/**
 * @param {string} parentCode
 * @returns List
 */
export const getCodeList = parentCode =>
  axios.get(`${host}/api/comcodes?parentCode=${parentCode}`);
export const getVendors = () => axios.get(`${host}/api/vendors`);
export const getSatcOfModel = vendorId =>
  axios.get(`${host}/api/satcOfModel`, { params: { vendorId: vendorId } });
export const getSatcOfModelByVendorIds = vendorIds =>
  axios.get(`${host}/api/satcOfModelByVendorIds`, {
    params: { vendorIds: vendorIds }
  });
export const getModelsOfSatc = (vendorId, satc) =>
  axios.get(`${host}/api/modelsOfSatc`, {
    params: { vendorId: vendorId, satc: satc }
  });
export const getModelsOfSatcByVendorIds = (vendorIds, satcs) =>
  axios.get(`${host}/api/modelsOfSatcByVendorIds`, {
    params: { vendorIds: vendorIds, satcs: satcs }
  });

export const setLanguage = lang =>
  axios.get(`${host}/api/change/language`, {
    params: { lang: lang }
  });
