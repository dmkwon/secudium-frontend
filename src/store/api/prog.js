import axios from "axios";
import { host } from "./../../constants";

export const getProgs = () => axios.get(`${host}/api/systemoper/programs`);
export const getProgBySeq = (seq) => axios.post(`${host}/api/systemoper/programbyseq`, {funcSeq : seq}); 
export const createProg = (prog) => axios.post(`${host}/api/systemoper/program`, prog);
export const updateProg = (prog) => axios.put(`${host}/api/systemoper/program`, prog);
export const removeProgs = (paramList) => axios.put(`${host}/api/systemoper/programs/delete`, paramList);
export const removeProg = (funcSeq) => axios.post(`${host}/api/systemoper/program/delete`, {funcSeq : funcSeq});
