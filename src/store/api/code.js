import axios from "axios";
import {host} from "../../constants";

export const getComCodes = () => axios.get(`${host}/api/systemoper/comcodes`);
export const getComCodeBySeq = (commCodeSeq) => axios.post(`${host}/api/systemoper/comcodebyseq`, {commCodeSeq : commCodeSeq}); 
export const getCodesByParentCode = (topCommCode) => axios.post(`${host}/api/systemoper/comcode/parentCode`, {topCommCode : topCommCode});  
export const getCodesByCode = (topCommCode, commCodeSeq) => axios.post(`${host}/api/systemoper/comcode/parentCode/codes`, {topCommCode:topCommCode, commCodeSeq:commCodeSeq});
export const createCode = (commCode) => axios.post(`${host}/api/systemoper/comcode`, commCode);
export const updateCode = (commCode) => axios.put(`${host}/api/systemoper/comcode`, commCode);
export const removeComCodes = (paramList) => axios.put(`${host}/api/systemoper/comcodes/delete`, paramList);
export const removeComCode = (commCodeSeq) => axios.post(`${host}/api/systemoper/comcode/delete`, {commCodeSeq:commCodeSeq});  
