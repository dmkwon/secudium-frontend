import axios from "axios";
import {host} from "../../constants";
import { isDebug } from "../../components/EngineOption/const/enginOptionConfig";

const base_url = `${host}/api/enginemgmt`;

const customTransformResponse = (data) => {
    isDebug && console.log("customTransformResponse", data, typeof data)
    // if (typeof data === 'string') { // 디폴트 파서에서 데이터 그래도 넘기도록 수정
    //   try {
    //     data = JSON.parse(data);
    //   } catch (e) {
    //     /* Ignore */
    //   }
    // }
    return data;
  };

const getNodeList = () => axios.get(`${base_url}/zkNode/root`);
const getChildNodeList = (obj) => axios.get(`${base_url}/zkNode/childrens`, {params: obj});
const getData = (obj) => axios.get(`${base_url}/zkNode/data`, {
    params: obj,
    headers: {
        'Content-Type': 'application/json'
      },
    transformResponse: customTransformResponse
});
const putData = (obj) => axios.put(`${base_url}/zkNode/data`, obj);

// engine 환경에서 rootpath를 얻어오기 위한
const getEngineEnv = () => axios.get(`${host}/api/enginemgmt/engineenv`);
// root 경로와 node 초기화 data를 가져오기 위한 
const getInitData = () => axios.all([getEngineEnv(), getNodeList()]);
const pudZKData = (obj1, obj2) => axios.all([putData(obj1), getData(obj2)]);

export { getNodeList, getChildNodeList, getData, putData, getEngineEnv, getInitData, pudZKData }