import axios from "axios";
import { host } from "./../../constants";

export const getMenus = () => axios.get(`${host}/api/systemoper/menus`);
// export const getMenusByStep = (step) => axios.get(`${host}/api/systemoper/step/${step}/menus`);  //미사용
export const getMenusBySeq = (seq) => axios.post(`${host}/api/systemoper/menubyseq`, {menuSeq:seq});
export const createMenu = (menu) => axios.post(`${host}/api/systemoper/menu`, menu);
export const updateMenu = (menu) => axios.put(`${host}/api/systemoper/menu`, menu);
export const removeMenu = (seq) => axios.post(`${host}/api/systemoper/menu/delete`, {menuSeq:seq});
export const removeMenus = (paramList) => axios.put(`${host}/api/systemoper/menus/delete`, paramList);
export const getMenusByParentSeq = (topMenuSeq) => axios.post(`${host}/api/systemoper/menus/parentMenu`, {topMenuSeq:topMenuSeq});
