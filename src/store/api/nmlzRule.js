import axios from 'axios';
import { host } from '../../constants';

/** 정규화룰 목록 조회 */
export function getNmlzRules(routeParams = '') {
  return axios.get(`${host}/api/rulemgmt/nmlzrules`, {
    params: routeParams,
  });
}

export function getNmlzRule(nmlzRuleSeq) {
  return axios.get(`${host}/api/rulemgmt/nmlzrule`, {
    params: { nmlzRuleSeq: nmlzRuleSeq },
  });
}
export function createNmlzRule(nmlzRule) {
  return axios.post(`${host}/api/rulemgmt/nmlzrules`, nmlzRule);
}

export function updateNmlzRule(nmlzRule) {
  return axios.put(`${host}/api/rulemgmt/nmlzrules`, nmlzRule);
}

export function removeNmlzRule(deleteNmlzRules) {
  return axios.post(`${host}/api/rulemgmt/nmlzrules/delete`, deleteNmlzRules);
}
export function rollBackNmlzRule(rollbackNmlzRule) {
  return axios.post(
    `${host}/api/rulemgmt/nmlzrules/rollback`,
    rollbackNmlzRule
  );
}

/** 정규화룰 이력 목록 조회 */
export function getNmlzRulesHists(nmlzRuleSeq) {
  return axios.get(`${host}/api/rulemgmt/nmlzrules/hists`, {
    params: { nmlzRuleSeq: nmlzRuleSeq },
  });
}

/** 정규화룰 이력 상세 내용 조회 */
export function getNmlzRulesHist(nmlzRuleHistSeq) {
  return axios.get(`${host}/api/rulemgmt/nmlzrules/hist`, {
    params: { nmlzRuleHistSeq: nmlzRuleHistSeq },
  });
}

/** 정규화룰 ZK 에 배포 */
export function deployRule() {
  return axios.post(`${host}/api/rulemgmt/nmlzrules/deploy`);
}
