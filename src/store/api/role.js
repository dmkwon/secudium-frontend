import axios from "axios";
import { host } from "./../../constants";

export const getRoles = () => axios.get(`${host}/api/systemoper/roles`);
export const getRolesBySeq = (seq) => axios.post(`${host}/api/systemoper/rolebyseq`, {roleSeq:seq});
export const createRole = (roleInsert) => axios.post(`${host}/api/systemoper/role`, roleInsert);
export const updateRole = (roleUpdate) => axios.put(`${host}/api/systemoper/role`, roleUpdate);
export const removeRole = (seq) => axios.post(`${host}/api/systemoper/role/delete`, {roleSeq:seq});
export const removeRoles = (paramList) => axios.put(`${host}/api/systemoper/roles/delete`, paramList);

export const getUsersBySeq = (seq) => axios.post(`${host}/api/systemoper/role/users`, {roleSeq: seq});
//export const createUsersBySeq = (seq, users) => axios.post(`${host}/api/systemoper/roles/${seq}/create/users`, users);
export const removeUsersBySeq = (seq, userSeq) => axios.post(`${host}/api/systemoper/role/delete/users`, {seq:seq, members:userSeq});
export const updateUserRoleBySeq = (seq, paramList) => axios.post(`${host}/api/systemoper/role/update/usersbyseq`, {seq:seq, members:paramList});

export const getMenusBySeq = (seq) => axios.post(`${host}/api/systemoper/role/menus`, {menuSeq : seq});
//export const removeRoleMenus = (paramList) => axios.put(`${host}/api/systemoper/roles/menus/list`, paramList);    // 미사용
export const updateMenuRoleBySeq = (seq, paramList) => axios.post(`${host}/api/systemoper/role/update/menus`, {seq:seq, menus: paramList});

export const getProgsBySeq = (seq) => axios.post(`${host}/api/systemoper/role/programs`, {funcSeq : seq}); 
//export const removeRoleProgs = (paramList) => axios.put(`${host}/api/systemoper/roles/programs/list`, paramList);     // 미사용
export const updateProgRoleBySeq = (seq, paramList) => axios.post(`${host}/api/systemoper/role/update/programs`, {seq:seq, progs:paramList}); 
